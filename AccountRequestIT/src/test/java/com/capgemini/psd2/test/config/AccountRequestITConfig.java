package com.capgemini.psd2.test.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = { "com.capgemini.psd2" })
@Configuration
@EnableAutoConfiguration
public class AccountRequestITConfig {
	
}
