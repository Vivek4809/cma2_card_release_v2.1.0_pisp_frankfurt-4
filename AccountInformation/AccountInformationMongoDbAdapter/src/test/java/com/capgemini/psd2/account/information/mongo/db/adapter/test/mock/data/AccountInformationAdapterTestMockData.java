/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.information.mongo.db.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.capgemini.psd2.aisp.domain.OBAccount2;
import com.capgemini.psd2.aisp.domain.OBCashAccount3;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification4;
import com.capgemini.psd2.aisp.domain.OBReadAccount2;
import com.capgemini.psd2.aisp.domain.OBReadAccount2Data;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountInformationCMA2;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountInformationResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.logger.LoggerAttribute;

/**
 * The Class AccountInformationAdapterTestMockData.
 */
public class AccountInformationAdapterTestMockData {

	/** The mock accounts GET response accounts. */
	public static OBAccount2 mockAccountsGETResponseAccounts;

	/** The mock account mapping. */
	public static AccountMapping mockAccountMapping;

	/** The mock accounts GET response. */
	public static OBReadAccount2 mockAccountsGETResponse;

	public static OBAccount2 Account;
	//public static MockAccount mockAccount;

	/**
	 * Gets the account GET response.
	 *
	 * @return the account GET response
	 */
	public static OBReadAccount2 getAccountGETResponse() {
		mockAccountsGETResponse = new OBReadAccount2();
		List<OBAccount2> accountsGETResponseAccountsList = new ArrayList<>();
		accountsGETResponseAccountsList.add(getAccountGETResponseData());
		OBReadAccount2Data mockData = new OBReadAccount2Data();
		mockData.setAccount(accountsGETResponseAccountsList);
		mockAccountsGETResponse.setData(mockData);
		return mockAccountsGETResponse;
	}

	/**
	 * Gets the account GET response data.
	 *
	 * @return the account GET response data
	 */
	public static OBAccount2 getAccountGETResponseData() {
		mockAccountsGETResponseAccounts = new OBAccount2();
		List<OBCashAccount3> accountList= new ArrayList<>();
		OBCashAccount3 mockData2Account = new OBCashAccount3();
		mockData2Account.setIdentification("47550843");
		mockData2Account.setSchemeName("UK.OBIE.IBAN");
		mockData2Account.setName("Lorem");
		mockAccountsGETResponseAccounts.setAccount(accountList);

		OBBranchAndFinancialInstitutionIdentification4 mockData2Servicer = new OBBranchAndFinancialInstitutionIdentification4();
		mockData2Servicer.setIdentification("901343");
		mockData2Servicer.setSchemeName("BICFI");
		mockAccountsGETResponseAccounts.setServicer(mockData2Servicer);
		return mockAccountsGETResponseAccounts;
	}

	/**
	 * Gets the account mapping.
	 *
	 * @return the account mapping
	 */
	public static AccountMapping getAccountMapping() {
		mockAccountMapping = new AccountMapping();
		mockAccountMapping.setTppCID("tpp123");
		mockAccountMapping.setPsuId("user123");
		mockAccountMapping.setCorrelationId("95212678-4d0c-450f-8268-25dcfc95bfa1");
		List<AccountDetails> selectedAccounts = new ArrayList<>();

		AccountDetails accountRequest1 = new AccountDetails();
		accountRequest1.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		accountRequest1.setAccountNSC("SC802001");
		accountRequest1.setAccountNumber("10203345");

		AccountDetails accountRequest2 = new AccountDetails();
		accountRequest2.setAccountId("bf3e700e-25c9-475c-ab37-f2069af8b79a");
		accountRequest2.setAccountNSC("SC802001");
		accountRequest2.setAccountNumber("25369621");

		selectedAccounts.add(accountRequest1);
		selectedAccounts.add(accountRequest2);

		mockAccountMapping.setAccountDetails(selectedAccounts);
		return mockAccountMapping;
	}

	/*public static MockAccount getAccount1() {
		mockAccount = new MockAccount();
		mockAccount.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		Data2Account mockData2Account = new Data2Account();
		mockData2Account.setIdentification("10203345");
		mockData2Account.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		mockData2Account.setName("Lorem");
		mockAccount.setAccount(mockData2Account);

		Data2Servicer mockData2Servicer = new Data2Servicer();
		mockData2Servicer.setIdentification("SC802001");
		mockData2Servicer.setSchemeName(Data2Servicer.SchemeNameEnum.BICFI);
		mockAccount.setServicer(mockData2Servicer);
		return mockAccount;
	}

	private static MockAccount getAccount2() {
		mockAccount = new MockAccount();
		Data2Account mockData2Account = new Data2Account();
		mockData2Account.setIdentification("76528776");
		mockData2Account.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		mockData2Account.setName("Mr Kevin");
		mockAccount.setAccount(mockData2Account);

		Data2Servicer mockData2Servicer = new Data2Servicer();
		mockData2Servicer.setIdentification("SC802001");
		mockData2Servicer.setSchemeName(Data2Servicer.SchemeNameEnum.BICFI);
		mockAccount.setServicer(mockData2Servicer);
		return mockAccount;
	}

	private static MockAccount getAccount3() {
		mockAccount = new MockAccount();
		Data2Account mockData2Account = new Data2Account();
		mockData2Account.setIdentification("25369621");
		mockData2Account.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		mockData2Account.setName("Mr Jack");
		mockAccount.setAccount(mockData2Account);

		Data2Servicer mockData2Servicer = new Data2Servicer();
		mockData2Servicer.setIdentification("SC802001");
		mockData2Servicer.setSchemeName(Data2Servicer.SchemeNameEnum.BICFI);
		mockAccount.setServicer(mockData2Servicer);
		return mockAccount;
	}

	private static MockAccount getAccount4() {
		mockAccount = new MockAccount();
		Data2Account mockData2Account = new Data2Account();
		mockData2Account.setIdentification("9999");
		mockData2Account.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		mockData2Account.setName("Mr Sam");
		mockAccount.setAccount(mockData2Account);

		Data2Servicer mockData2Servicer = new Data2Servicer();
		mockData2Servicer.setIdentification("SC802001");
		mockData2Servicer.setSchemeName(Data2Servicer.SchemeNameEnum.BICFI);
		mockAccount.setServicer(mockData2Servicer);
		return mockAccount;
	}

	private static MockAccount getAccount5() {
		mockAccount = new MockAccount();
		Data2Account mockData2Account = new Data2Account();
		mockData2Account.setIdentification("888");
		mockData2Account.setSchemeName(Data2Account.SchemeNameEnum.IBAN);
		mockData2Account.setName("Mr Ham");
		mockAccount.setAccount(mockData2Account);

		Data2Servicer mockData2Servicer = new Data2Servicer();
		mockData2Servicer.setIdentification("SC802001");
		mockData2Servicer.setSchemeName(Data2Servicer.SchemeNameEnum.BICFI);
		mockAccount.setServicer(mockData2Servicer);
		return mockAccount;
	}

	public static List<Account> getSingleAccountGETResponseDataList() {
		List<Account> mockAccountList = new ArrayList<>();
		mockAccountList.add(getAccount1());
		return mockAccountList;
	}

	public static List<MockAccount> getAccountGETResponseDataList() {
		List<MockAccount> mockAccountList = new ArrayList<>();
		mockAccountList.add(getAccount1());
		mockAccountList.add(getAccount2());
		mockAccountList.add(getAccount3());
		mockAccountList.add(getAccount4());
		mockAccountList.add(getAccount5());
		return mockAccountList;
	}
*/
	public static AccountMapping getAccountMapping1() {
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("88888888");
		List<AccountDetails> accountDetails = new ArrayList<>();
		AccountDetails obj1 = new AccountDetails();
		obj1.setAccountId("d1c3d9cb-1725-45c2-8c99-f061ec53b37e");
		obj1.setAccountNSC("901343");
		obj1.setAccountNumber("47550843");

		AccountDetails obj2 = new AccountDetails();
		obj2.setAccountId("d2192679-c26f-4af6-9102-2c9e8b8c207b");
		obj2.setAccountNSC("903779");
		obj2.setAccountNumber("25369621");

		accountDetails.add(obj1);
		accountDetails.add(obj2);
		accountMapping.setAccountDetails(accountDetails);
		accountMapping.setTppCID("123");
		return accountMapping;
	}

/*	public static List<MockAccount> getMockAccount() {
		List<MockAccount> mockAccountList = new ArrayList<>();

		MockAccount mockAccount = new MockAccount();
		mockAccount.setAccountType("savings");
		mockAccount.setCurrency("GBP");
		mockAccount.setNickname("james");
		mockAccount.setAccountId("d1c3d9cb-1725-45c2-8c99-f061ec53b37e");
		Data2Account data2account = new Data2Account();
		data2account.setIdentification("47550843");
		data2account.setName("franklin");
		data2account.setSchemeName(SchemeNameEnum.SORTCODEACCOUNTNUMBER);
		data2account.setSecondaryIdentification("0044");
		mockAccount.setAccount(data2account);

		Data2Servicer data2servicer = new Data2Servicer();
		data2servicer.setIdentification("901343");
		mockAccount.setServicer(data2servicer);
		mockAccountList.add(mockAccount);
		return mockAccountList;
	}
*/
	public static List<AccountInformationCMA2> getAccountInfoDetails() {
		List<AccountInformationCMA2> accInfoList = new ArrayList<>();
		return accInfoList;
	}

	public static LoggerAttribute getMockLoggerData() {
		LoggerAttribute x = new LoggerAttribute();
		x.setApiId("testApiID");
		return x;
	}

	public static OBReadAccount2 getMockExpectedAccountInformationResponse() {
		PlatformAccountInformationResponse platformAccountInformationRespnse = new PlatformAccountInformationResponse();
		OBReadAccount2Data obReadAccount2Data = new OBReadAccount2Data();
		OBReadAccount2 obReadAccount2 = new OBReadAccount2();
		//OBAccount2 acctInfoCma = new AccountInformationCMA2();
		List<AccountInformationCMA2> mockAccountInfoList = new ArrayList<>();
		//mockAccountInfoList.set(0,acctInfoCma.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1"));
		mockAccountInfoList.add(new AccountInformationCMA2());
		mockAccountInfoList.get(0).setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		
		List<OBAccount2> obAccount2List = new ArrayList<>();
		obAccount2List.addAll(mockAccountInfoList);

		obReadAccount2Data.setAccount(obAccount2List);
		obReadAccount2.setData(obReadAccount2Data);
		platformAccountInformationRespnse.setoBReadAccount2(obReadAccount2);

		return obReadAccount2;
	}
	
	public static OBReadAccount2 getMockExpectedMultiAccountInformationResponse() {
		PlatformAccountInformationResponse platformAccountInformationRespnse = new PlatformAccountInformationResponse();
		OBReadAccount2Data obReadAccount2Data = new OBReadAccount2Data();
		OBReadAccount2 obReadAccount2 = new OBReadAccount2();
		//OBAccount2 acctInfoCma = new AccountInformationCMA2();
		List<AccountInformationCMA2> mockAccountInfoList = new ArrayList<>();
		//mockAccountInfoList.set(0,acctInfoCma.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1"));
		mockAccountInfoList.add(new AccountInformationCMA2());
		mockAccountInfoList.get(0).setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		
		Map<String, String> accountNumberMap = new HashMap<>();
		
		/*AccountMapping accountmapping  = new AccountMapping();
		accountmapping.getAccountDetails()*/
		

		List<OBAccount2> obAccount2List = new ArrayList<>();
		obAccount2List.addAll(mockAccountInfoList);

		obReadAccount2Data.setAccount(obAccount2List);
		obReadAccount2.setData(obReadAccount2Data);
		platformAccountInformationRespnse.setoBReadAccount2(obReadAccount2);

		return obReadAccount2;
	}
}
