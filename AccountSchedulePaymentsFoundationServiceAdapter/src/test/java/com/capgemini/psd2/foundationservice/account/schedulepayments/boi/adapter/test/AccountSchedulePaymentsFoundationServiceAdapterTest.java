package com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.test;

import static org.mockito.Matchers.any;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.OBExternalAccountSubType1Code;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSchedulePaymentsResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.AccountSchedulePaymentsFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.client.AccountSchedulePaymentsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.delegate.AccountSchedulePaymentsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.raml.domain.PartiesOneOffPaymentInstructionsresponse;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Account;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Currency;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.CurrentAccount;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUser;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyBasicInformation;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.RetailNonRetailCode;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.SourceSystemAccountType;
import com.capgemini.psd2.logger.PSD2Constants;

public class AccountSchedulePaymentsFoundationServiceAdapterTest {
	
	@InjectMocks
	private AccountSchedulePaymentsFoundationServiceAdapter accountSchedulePaymentsFoundationServiceAdapter;
	/** The account information foundation service delegate. */
	@Mock
	private AccountSchedulePaymentsFoundationServiceDelegate accountSchedulePaymentsFoundationServiceDelegate;
	
	/** The account information foundation service client. */
	@Mock
	private AccountSchedulePaymentsFoundationServiceClient accountSchedulePaymentsFoundationServiceClient;
	
	@Mock
	private CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;
	
	@Mock
	private AdapterFilterUtility adapterFilterUtility;
	
	/** The rest client. */
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	@Test
	public void retrieveAccountSchedulePayments4(){
		PlatformAccountSchedulePaymentsResponse response = new PlatformAccountSchedulePaymentsResponse();
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("1234");
		List<AccountDetails> accList= new ArrayList();
		AccountDetails accDet = new AccountDetails();
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		params.put("account_subtype","account_subtype");
		accountMapping.setCorrelationId("1234");
		accDet.setAccountId("12345");
		accDet.setAccountNumber("1234");
		params.put("channelId","B365");
		accList.add(0, accDet);
		accountMapping.setAccountDetails(accList);
		Mockito.when(accountSchedulePaymentsFoundationServiceClient.restTransportForScheduleItems(any(), any(), any(), any())).thenReturn(new PartiesOneOffPaymentInstructionsresponse());
		Mockito.when(accountSchedulePaymentsFoundationServiceDelegate.createRequestHeaders(any(), any(), any())).thenReturn(null);
		accountSchedulePaymentsFoundationServiceAdapter.retrieveAccountSchedulePayments(accountMapping, params);
	}
	@Test
	public void retrieveAccountSchedulePayments(){
		PlatformAccountSchedulePaymentsResponse response = new PlatformAccountSchedulePaymentsResponse();
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("1234");
		List<AccountDetails> accList= new ArrayList();
		AccountDetails accDet = new AccountDetails();
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		params.put("account_subtype","account_subtype");
		accountMapping.setCorrelationId("1234");
		accDet.setAccountId("12345");
		accDet.setAccountNumber("1234");
		accountMapping.setAccountDetails(accList);
		accList.add(0, accDet);
		params.put("channelId","B365");
		accountMapping.setAccountDetails(accList);
		Mockito.when(accountSchedulePaymentsFoundationServiceClient.restTransportForScheduleItems(any(), any(), any(), any())).thenReturn(new PartiesOneOffPaymentInstructionsresponse());
		Mockito.when(accountSchedulePaymentsFoundationServiceDelegate.createRequestHeaders(any(), any(), any())).thenReturn(null);
		accountSchedulePaymentsFoundationServiceAdapter.retrieveAccountSchedulePayments(accountMapping, params);
	}
	@Test
	public void retrieveAccountSchedulePayments8(){
		
		PlatformAccountSchedulePaymentsResponse response = new PlatformAccountSchedulePaymentsResponse();
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("1234");
		List<AccountDetails> accList= new ArrayList();
		AccountDetails accDet = new AccountDetails();
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		params.put("account_subtype","account_subtype");
		//params.put("cmaVersion", "2.0");
		accountMapping.setCorrelationId("1234");
		accDet.setAccountId("12345");
		accDet.setAccountNumber("1234");
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
		
		accDet.setAccountType("CurrentAccount");
		accountMapping.setAccountDetails(accList);
		accList.add(0, accDet);
		accountMapping.setAccountDetails(accList);
		
		DigitalUser du = new DigitalUser();
		du.setDigitalUserLockedOutIndicator(false);
		DigitalUserProfile dup = new DigitalUserProfile();
		dup.setDigitalUser(du);
		PartyBasicInformation pi = new PartyBasicInformation();
		pi.setPartyActiveIndicator(true);
		dup.setPartyInformation(pi);
		List<AccountEntitlements> accountEntitlements = new ArrayList<AccountEntitlements>();
		AccountEntitlements ea = new AccountEntitlements();
		List<String> ent = new ArrayList<>();
		ent.add("Display Only Account");
		Account account = new Account();
		RetailNonRetailCode retail = RetailNonRetailCode.fromValue("Retail");
		SourceSystemAccountType sourceSystemAccountTypeCurrent = SourceSystemAccountType.fromValue("Current Account");
		account.setAccountNumber("24512786");
		Currency currency = new Currency();
		CurrentAccount cuacinfo = new CurrentAccount();
		currency.setIsoAlphaCode("GBP");
		account.setAccountCurrency(currency);
		account.setRetailNonRetailCode(retail);
		account.setSourceSystemAccountType(sourceSystemAccountTypeCurrent);
		cuacinfo.setParentNationalSortCodeNSCNumber("902127");
		cuacinfo.setInternationalBankAccountNumberIBAN("GB58BOFI90212724512786");
		cuacinfo.setSwiftBankIdentifierCodeBIC("BOFIGB2BXXX");
		account.setCurrentAccountInformation(cuacinfo);
		ea.setAccount(account);
		ea.setEntitlements(ent);
		accountEntitlements.add(ea);
		dup.setAccountEntitlements(accountEntitlements);
		params.put("channelId","B365");
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(), any())).thenReturn(dup);
		Mockito.when(adapterFilterUtility.prevalidateAccounts(dup, accountMapping)).thenReturn(accountMapping);
		Mockito.when(accountSchedulePaymentsFoundationServiceClient.restTransportForScheduleItems(any(), any(), any(),any())).thenReturn(new PartiesOneOffPaymentInstructionsresponse());
		Mockito.when(accountSchedulePaymentsFoundationServiceDelegate.createRequestHeaders(any(), any(), any())).thenReturn(null);
		accountSchedulePaymentsFoundationServiceAdapter.retrieveAccountSchedulePayments(accountMapping, params);
		
	}
	@Test
	public void retrieveAccountSchedulePayments1(){
		PlatformAccountSchedulePaymentsResponse response = new PlatformAccountSchedulePaymentsResponse();
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("1234");
		List<AccountDetails> accList= new ArrayList();
		AccountDetails accDet = new AccountDetails();
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		params.put("account_subtype","account_subtype");
		//params.put("cmaVersion", "2.0");
		accountMapping.setCorrelationId("1234");
		accDet.setAccountId("12345");
		accDet.setAccountNumber("1234");
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		
		accDet.setAccountType("CurrentAccount");
		accountMapping.setAccountDetails(accList);
		accList.add(0, accDet);
		params.put("channelId","B365");
		accountMapping.setAccountDetails(accList);
		Mockito.when(accountSchedulePaymentsFoundationServiceClient.restTransportForScheduleItems(any(), any(), any(),any())).thenReturn(new PartiesOneOffPaymentInstructionsresponse());
		Mockito.when(accountSchedulePaymentsFoundationServiceDelegate.createRequestHeaders(any(), any(), any())).thenReturn(null);
		accountSchedulePaymentsFoundationServiceAdapter.retrieveAccountSchedulePayments(accountMapping, params);
	}
	/*@Test(expected = AdapterException.class)
	public void retrieveAccountSchedulePayments5(){
		PlatformAccountSchedulePaymentsResponse response = new PlatformAccountSchedulePaymentsResponse();
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId(null);
		List<AccountDetails> accList= new ArrayList();
		AccountDetails accDet = new AccountDetails();
		Map<String, String> params = new HashMap<>();
		accountMapping.setCorrelationId("1234");
		accDet.setAccountId("12345");
		accDet.setAccountNumber("1234");
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		accDet.setAccountType("CurrentAccount");
		accList.add(0, accDet);
		params.put("channelId","B365");
		accountMapping.setAccountDetails(accList);
		Mockito.when(accountSchedulePaymentsFoundationServiceClient.restTransportForScheduleItems(any(), any(), any(),any())).thenReturn(new PartiesOneOffPaymentInstructionsresponse());
		Mockito.when(accountSchedulePaymentsFoundationServiceDelegate.createRequestHeaders(any(), any(), any())).thenReturn(null);
		accountSchedulePaymentsFoundationServiceAdapter.retrieveAccountSchedulePayments(accountMapping, params);
	}*/
/*	@Test(expected = AdapterException.class)
	public void retrieveAccountSchedulePayments6(){
		PlatformAccountSchedulePaymentsResponse response = new PlatformAccountSchedulePaymentsResponse();
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("12345");
		List<AccountDetails> accList= new ArrayList();
		AccountDetails accDet = new AccountDetails();
		accountMapping.setCorrelationId("1234");
		accDet.setAccountId("12345");
		accDet.setAccountNumber("1234");
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);	
		accDet.setAccountType("CurrentAccount");
		accList.add(0, accDet);
		Map<String, String> params = new HashMap<>();
		params.put("channelId","B365");
		accountMapping.setAccountDetails(accList);
		Mockito.when(accountSchedulePaymentsFoundationServiceClient.restTransportForScheduleItems(any(), any(), any(), any())).thenReturn(new PartiesOneOffPaymentInstructionsresponse());
		Mockito.when(accountSchedulePaymentsFoundationServiceDelegate.createRequestHeaders(any(), any(), any())).thenReturn(null);
		accountSchedulePaymentsFoundationServiceAdapter.retrieveAccountSchedulePayments(accountMapping, null);
	}*/
	@Test
	public void retrieveAccountSchedulePayments2(){
		PlatformAccountSchedulePaymentsResponse response = new PlatformAccountSchedulePaymentsResponse();
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("1234");
		List<AccountDetails> accList= new ArrayList();
		AccountDetails accDet = new AccountDetails();
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		params.put("account_subtype","account_subtype");
		//params.put("cmaVersion", "2.0");
		accountMapping.setCorrelationId("1234");
		accDet.setAccountId("12345");
		accDet.setAccountNumber("1234");
		params.put("channelId","B365");
		//accDet.setAccountSubType(AccountSubTypeEnum.SAVINGS);
		
		//accDet.setAccountType("CurrentAccount");
		accountMapping.setAccountDetails(accList);
		accList.add(0, accDet);
		accountMapping.setAccountDetails(accList);
		Mockito.when(accountSchedulePaymentsFoundationServiceClient.restTransportForScheduleItems(any(), any(), any(), any())).thenReturn(new PartiesOneOffPaymentInstructionsresponse());
		Mockito.when(accountSchedulePaymentsFoundationServiceDelegate.createRequestHeaders(any(), any(), any())).thenReturn(null);
		accountSchedulePaymentsFoundationServiceAdapter.retrieveAccountSchedulePayments(accountMapping, params);
	}
	@Test
	public void retrieveAccountSchedulePayments3(){
		PlatformAccountSchedulePaymentsResponse response = new PlatformAccountSchedulePaymentsResponse();
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("1234");
		List<AccountDetails> accList= new ArrayList();
		AccountDetails accDet = new AccountDetails();
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		params.put("account_subtype","account_subtype");
		params.put(PSD2Constants.CMAVERSION, "2.0");
		accountMapping.setCorrelationId("1234");
		accDet.setAccountId("12345");
		accDet.setAccountNumber("1234");
		accDet.setAccountSubType(OBExternalAccountSubType1Code.SAVINGS);
		params.put("channelId","B365");
		//accDet.setAccountType("CurrentAccount");
		accountMapping.setAccountDetails(accList);
		accList.add(0, accDet);
		accountMapping.setAccountDetails(accList);
		Mockito.when(accountSchedulePaymentsFoundationServiceClient.restTransportForScheduleItems(any(), any(), any(), any())).thenReturn(new PartiesOneOffPaymentInstructionsresponse());
		Mockito.when(accountSchedulePaymentsFoundationServiceDelegate.createRequestHeaders(any(), any(), any())).thenReturn(null);
		accountSchedulePaymentsFoundationServiceAdapter.retrieveAccountSchedulePayments(accountMapping, params);
	}
}
