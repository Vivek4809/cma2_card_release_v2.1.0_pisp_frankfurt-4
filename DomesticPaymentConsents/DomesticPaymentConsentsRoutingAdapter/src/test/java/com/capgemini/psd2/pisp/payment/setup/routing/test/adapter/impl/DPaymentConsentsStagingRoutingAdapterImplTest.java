package com.capgemini.psd2.pisp.payment.setup.routing.test.adapter.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyString;
import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.payment.setup.routing.adapter.impl.DPaymentConsentsStagingRoutingAdapterImpl;
import com.capgemini.psd2.pisp.payment.setup.routing.adapter.routing.DPaymentConsentsAdapterFactory;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticPaymentStagingAdapter;
import com.capgemini.psd2.token.Token;

@RunWith(SpringJUnit4ClassRunner.class)
public class DPaymentConsentsStagingRoutingAdapterImplTest {

	@Mock
	private DPaymentConsentsAdapterFactory factory;

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private LoggerUtils loggerUtils;

	@InjectMocks
	private DPaymentConsentsStagingRoutingAdapterImpl adapter;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testprocessStagingDomesticPaymentConsents() {
		CustomDPaymentConsentsPOSTRequest dPaymentSetupPOSTRequest = new CustomDPaymentConsentsPOSTRequest();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();

		Map<String, String> params = new HashMap<>();

		Token token = new Token();
		token.setSeviceParams(params);

		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);
		params.put(PSD2Constants.USER_IN_REQ_HEADER,
				reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));

		
		DomesticPaymentStagingAdapter stagingAdapter = new DomesticPaymentStagingAdapter() {

			@Override
			public CustomDPaymentConsentsPOSTResponse retrieveStagedDomesticPaymentConsents(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				return null;
			}

			@Override
			public CustomDPaymentConsentsPOSTResponse processDomesticPaymentConsents(
					CustomDPaymentConsentsPOSTRequest paymentConsentsRequest,
					CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
					OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
				return null;
			}
		};

		Mockito.when(factory.getDomesticPaymentSetupStagingInstance(anyString())).thenReturn(stagingAdapter);
		adapter.processDomesticPaymentConsents(dPaymentSetupPOSTRequest, stageIdentifiers, params,
				OBExternalConsentStatus1Code.AUTHORISED, OBExternalConsentStatus1Code.REJECTED);
		assertNotNull(stagingAdapter);
	}

	@Test
	public void testRetrieveStagedDomesticPaymentConsents() {
		CustomPaymentStageIdentifiers customPaymentSetupInfo = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();

		Token token = new Token();
		token.setSeviceParams(params);

		DomesticPaymentStagingAdapter paymentSetupStagingAdapter = new DomesticPaymentStagingAdapter() {

			@Override
			public CustomDPaymentConsentsPOSTResponse retrieveStagedDomesticPaymentConsents(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				return null;
			}

			@Override
			public CustomDPaymentConsentsPOSTResponse processDomesticPaymentConsents(
					CustomDPaymentConsentsPOSTRequest paymentConsentsRequest,
					CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
					OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
				return null;
			}
		};

		Mockito.when(factory.getDomesticPaymentSetupStagingInstance(anyString()))
				.thenReturn(paymentSetupStagingAdapter);

		adapter.retrieveStagedDomesticPaymentConsents(customPaymentSetupInfo, new HashMap<>());
		assertNotNull(paymentSetupStagingAdapter);
	}
	
	@Test
	public void testRetrieveStagedDomesticPaymentConsentsNullParams() {
		CustomPaymentStageIdentifiers customPaymentSetupInfo = new CustomPaymentStageIdentifiers();
		Map<String, String> params = null;

		DomesticPaymentStagingAdapter paymentSetupStagingAdapter = new DomesticPaymentStagingAdapter() {

			@Override
			public CustomDPaymentConsentsPOSTResponse retrieveStagedDomesticPaymentConsents(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				return null;
			}

			@Override
			public CustomDPaymentConsentsPOSTResponse processDomesticPaymentConsents(
					CustomDPaymentConsentsPOSTRequest paymentConsentsRequest,
					CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
					OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
				return null;
			}
		};

		Mockito.when(factory.getDomesticPaymentSetupStagingInstance(anyString()))
				.thenReturn(paymentSetupStagingAdapter);

		adapter.retrieveStagedDomesticPaymentConsents(customPaymentSetupInfo, params);
		assertNull(params);
		assertNotNull(paymentSetupStagingAdapter);
	}


	@After
	public void tearDown() throws Exception {
		factory = null;
		reqHeaderAtrributes = null;
		adapter = null;
		loggerUtils = null;
	}

}
