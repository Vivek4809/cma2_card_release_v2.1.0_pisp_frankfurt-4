
package com.capgemini.psd2.pisp.payment.setup.routing.adapter.routing;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticPaymentStagingAdapter;

@Component
public class DPaymentConsentsCoreSystemAdapterFactory
		implements ApplicationContextAware, DPaymentConsentsAdapterFactory {

	/** The application context. */
	private ApplicationContext applicationContext;

	@Override
	public DomesticPaymentStagingAdapter getDomesticPaymentSetupStagingInstance(String adapterName) {
		return (DomesticPaymentStagingAdapter) applicationContext.getBean(adapterName);
	}

	@Override
	public void setApplicationContext(ApplicationContext context) {
		this.applicationContext = context;
	}
}
