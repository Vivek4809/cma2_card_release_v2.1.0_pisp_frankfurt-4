package com.capgemini.psd2.pisp.payment.setup.test.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBWriteDomesticConsent1;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.payment.setup.controller.DPaymentConsentsController;
import com.capgemini.psd2.pisp.payment.setup.service.DPaymentConsentsService;
import com.capgemini.psd2.pisp.payment.setup.test.mock.data.DPaymentConsentsPOSTRequestResponseMockData;

@RunWith(SpringJUnit4ClassRunner.class)
public class DPaymentConsentsControllerTest {

	private MockMvc mockMvc;

	PSD2Exception obPSD2Exception;

	@Mock
	private DPaymentConsentsService dPaymentConsentsService;

	@Mock
	private OBPSD2ExceptionUtility util;

	@InjectMocks
	private DPaymentConsentsController dpaymentConsentController;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(dpaymentConsentController).dispatchOptions(true).build();
		Map<String, String> map = new HashMap<>();
		map.put("HEADER", "header error message");
		map.put("FREQUENCY", "freq ma");
		map.put("AFTER_CUTOFF_DATE", "freq ma");
		map.put("FIELD", "freq ma");
		map.put("SIGNATURE", "freq ma");
		map.put("INCOMPATIBLE", "freq ma");
		map.put("RES_NOTFOUND", "freq ma");
		map.put("INTERNAL", "freq ma");

		Map<String, String> map1 = new HashMap<>();
		map1.put("signature_missing", "drtyrty");
		
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(map1);
		

	}

	@Test
	public void testCreateDomesticPaymentConsentsResource() throws Exception {

		OBWriteDomesticConsent1 paymentConsentsRequest = new OBWriteDomesticConsent1();
		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		response.setId("123");
		Mockito.when(dPaymentConsentsService.createDomesticPaymentConsentsResource(anyObject())).thenReturn(response);
		this.mockMvc
				.perform(post("/domestic-payment-consents").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
						.content(DPaymentConsentsPOSTRequestResponseMockData.asJsonString(paymentConsentsRequest)))
				.andExpect(status().isCreated());
		dpaymentConsentController.createDomesticPaymentConsentsResource(paymentConsentsRequest);
	}
	

	@Test(expected = PSD2Exception.class)
	public void createDomesticPaymentConsentsResource_PSD2Exception() {
		OBWriteDomesticConsent1 paymentConsentsRequest = new OBWriteDomesticConsent1();
		ReflectionTestUtils.setField(dpaymentConsentController, "responseErrorMessageEnabled", "true");
		Mockito.when(dPaymentConsentsService.createDomesticPaymentConsentsResource(anyObject())).thenThrow(
				PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_DEBITOR_AGENT_IDENTIFICATION_INVALID));
		try {
			this.mockMvc
					.perform(post("/domestic-payment-consents").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
							.content(DPaymentConsentsPOSTRequestResponseMockData.asJsonString(paymentConsentsRequest)))
					.andExpect(status().isBadRequest());
		} catch (Exception e) {
			assertEquals(false, e.getMessage().contains("In Valid HTTP Request"));
		}
		dpaymentConsentController.createDomesticPaymentConsentsResource(paymentConsentsRequest);
	}

	@Test(expected = PSD2Exception.class)
	public void testCreateDomesticPaymentConsentsResource_OBPSD2Exception() {

		OBWriteDomesticConsent1 paymentConsentsRequest = new OBWriteDomesticConsent1();
		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		ReflectionTestUtils.setField(dpaymentConsentController, "responseErrorMessageEnabled", "true");
		response.setId("123");
		Mockito.when(dPaymentConsentsService.createDomesticPaymentConsentsResource(anyObject()))
				.thenThrow(PSD2Exception.class);

		try {
			this.mockMvc
					.perform(post("/domestic-payment-consents").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
							.content(DPaymentConsentsPOSTRequestResponseMockData.asJsonString(response)))
					.andExpect(status().isBadRequest());
		} catch (Exception e) {
			assertEquals(false, e.getMessage().contains("In Valid Request"));
		}
		dpaymentConsentController.createDomesticPaymentConsentsResource(paymentConsentsRequest);
	}
	

	@Test
	public void testRetrieveDomesticPaymentConsentsResource() throws Exception {

		PaymentRetrieveGetRequest paymentRetrieveRequest = new PaymentRetrieveGetRequest();
		paymentRetrieveRequest.setConsentId("58923");
		CustomDPaymentConsentsPOSTResponse paymentConsentsResponse = new CustomDPaymentConsentsPOSTResponse();
		Mockito.when(dPaymentConsentsService.retrieveDomesticPaymentConsentsResource(anyObject()))
				.thenReturn(paymentConsentsResponse);
		this.mockMvc
				.perform(get("/domestic-payment-consents/{ConsentId}", 58923)
						.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content(
								DPaymentConsentsPOSTRequestResponseMockData.asJsonString(paymentRetrieveRequest)))
				.andExpect(status().isOk());
		dpaymentConsentController.retrieveDomesticPaymentConsentsResource(paymentRetrieveRequest);
	}

	

	@Test(expected = PSD2Exception.class)
	public void retrieveDomesticPaymentConsentsResource_() {

		PaymentRetrieveGetRequest paymentRetrieveRequest = new PaymentRetrieveGetRequest();
		paymentRetrieveRequest.setConsentId("58923");
		ReflectionTestUtils.setField(dpaymentConsentController, "responseErrorMessageEnabled", "true");
		Mockito.when(dPaymentConsentsService.retrieveDomesticPaymentConsentsResource(anyObject())).thenThrow(
				PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_DEBITOR_AGENT_IDENTIFICATION_INVALID));
		try {
			this.mockMvc
					.perform(get("/domestic-payment-consents/{ConsentId}", 58923)
							.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content(
									DPaymentConsentsPOSTRequestResponseMockData.asJsonString(paymentRetrieveRequest)))
					.andExpect(status().isBadRequest());
		} catch (Exception e) {
			assertEquals(false, e.getMessage().contains("In Valid HTTP Request/DomesticConsentId"));
		}
		dpaymentConsentController.retrieveDomesticPaymentConsentsResource(paymentRetrieveRequest);
	}

	@Test(expected = PSD2Exception.class)
	public void retrieveDomesticPaymentConsentsResource_OBPSD2Exception() {

		PaymentRetrieveGetRequest paymentRetrieveRequest = new PaymentRetrieveGetRequest();
		paymentRetrieveRequest.setConsentId("58923");
		ReflectionTestUtils.setField(dpaymentConsentController, "responseErrorMessageEnabled", "true");
		Mockito.when(dPaymentConsentsService.retrieveDomesticPaymentConsentsResource(anyObject()))
				.thenThrow(PSD2Exception.class);
		try {
			this.mockMvc
					.perform(get("/domestic-payment-consents/{ConsentId}", 58923)
							.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content(
									DPaymentConsentsPOSTRequestResponseMockData.asJsonString(paymentRetrieveRequest)))
					.andExpect(status().isBadRequest());
		} catch (Exception e) {
			assertEquals(false, e.getMessage().contains("In Valid HTTP Request/DomesticConsentId"));
		}
		dpaymentConsentController.retrieveDomesticPaymentConsentsResource(paymentRetrieveRequest);
	}

	@After
	public void tearDown() {
		dPaymentConsentsService = null;
		dpaymentConsentController = null;
	}

}