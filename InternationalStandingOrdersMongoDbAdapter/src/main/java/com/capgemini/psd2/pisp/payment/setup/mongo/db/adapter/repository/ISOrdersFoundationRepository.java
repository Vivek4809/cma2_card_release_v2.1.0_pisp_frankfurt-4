package com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.pisp.domain.IStandingorderFoundationResource;

public interface ISOrdersFoundationRepository extends MongoRepository<IStandingorderFoundationResource, String> {

	public IStandingorderFoundationResource findOneByDataInternationalStandingOrderId(String submissionId);

}