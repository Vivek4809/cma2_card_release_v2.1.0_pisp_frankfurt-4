package com.capgemini.psd2.mock.data;

import java.util.ArrayList;

import com.capgemini.psd2.model.Authorisation;
import com.capgemini.psd2.model.OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations;
import com.capgemini.tpp.registration.model.RegistrationRequest;
import com.capgemini.tpp.registration.model.RegistrationResponse;
import com.capgemini.tpp.registration.model.SSAWrapper;
import com.capgemini.tpp.ssa.model.SSAModel;

public class DynamicClientMockData {

	public static SSAWrapper getSSAWrapper() {

		SSAWrapper ssaWrapper = new SSAWrapper();

		ssaWrapper.setSsaToken(
				"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c");

		SSAModel ssaModel = setSSAModel();
		ssaWrapper.setSsaModel(ssaModel);

		RegistrationRequest registrationRequest = setRegistrationRequest();
		ssaWrapper.setRegistrationRequest(registrationRequest);

		return ssaWrapper;
	}

	private static RegistrationRequest setRegistrationRequest() {

		ArrayList<String> redirect_uris = new ArrayList<>();
		redirect_uris.add("https://test.com");
		ArrayList<String> grant_types = new ArrayList<>();
		grant_types.add("authorization_code");
		grant_types.add("refresh_token");
		grant_types.add("client_credentials");

		RegistrationRequest registrationRequest = new RegistrationRequest(1537257843L, "1wwLDFoVlHfnlK0Dgu71lB",
				"1wwLDFoVlHfnlK0Dgu71lB", "7aac9164-eeac-47d6-8042-9a24610a984e", 1537257843L, "0015800000jfQ9aAAE",
				"4OOHG25bsZPvgx9mJfjKWN", redirect_uris, "accounts", "https://auth-test.apibank-cma2plus.in",
				"client_secret_basic", grant_types, null,
				"eyJhbGciOiJSUzI1NiIsImtpZCI6ImpfT1BYZTh0Y2hXdWhRM2dWTi1TT09PVHlEWSIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1MzczNjQyMjEsImlzcyI6Ik9wZW5CYW5raW5nIEx0ZCIsImp0aSI6IjZoaFRXclFCTlYzZWR5NGdRcEhhMjMiLCJvYl9yZWdpc3RyeV90b3MiOiJodHRwczovL3JlZ2lzdHJ5Lm9wZW5iYW5raW5ndGVzdC5vcmcudWsvdG9zLmh0bWwiLCJvcmdfY29udGFjdHMiOlt7ImVtYWlsIjoiT0JUZWNobmljYWxRdWVyaWVzQEJPSS5DT00iLCJuYW1lIjoiVGVjaG5pY2FsIiwicGhvbmUiOiIwODYwNjgxNzYyIiwidHlwZSI6IlRlY2huaWNhbCJ9LHsiZW1haWwiOiJPQkJ1c2luZXNzUXVlcmllc0BCT0kuQ09NIiwibmFtZSI6IkJ1c2luZXNzIiwicGhvbmUiOiIwNzU4NCAyMTQ4MzAiLCJ0eXBlIjoiQnVzaW5lc3MifV0sIm9yZ19pZCI6IjAwMTU4MDAwMDBqZlE5YUFBRSIsIm9yZ19qd2tzX2VuZHBvaW50IjoiaHR0cHM6Ly9rZXlzdG9yZS5vcGVuYmFua2luZ3Rlc3Qub3JnLnVrLzAwMTU4MDAwMDBqZlE5YUFBRS8wMDE1ODAwMDAwamZROWFBQUUuandrcyIsIm9yZ19qd2tzX3Jldm9rZWRfZW5kcG9pbnQiOiJodHRwczovL2tleXN0b3JlLm9wZW5iYW5raW5ndGVzdC5vcmcudWsvMDAxNTgwMDAwMGpmUTlhQUFFL3Jldm9rZWQvMDAxNTgwMDAwMGpmUTlhQUFFLmp3a3MiLCJvcmdfbmFtZSI6IkJhbmsgb2YgSXJlbGFuZCAoVUspIFBsYyIsIm9yZ19zdGF0dXMiOiJBY3RpdmUiLCJvcmdhbmlzYXRpb25fY29tcGV0ZW50X2F1dGhvcml0eV9jbGFpbXMiOnsiYXV0aG9yaXNhdGlvbnMiOlt7Im1lbWJlcl9zdGF0ZSI6IkdCIiwicm9sZXMiOlsiQUlTUCIsIlBJU1AiXX1dLCJhdXRob3JpdHlfaWQiOiJGQ0FHQlIiLCJyZWdpc3RyYXRpb25faWQiOiI1MTI5NTYiLCJzdGF0dXMiOiJBY3RpdmUifSwic29mdHdhcmVfY2xpZW50X2Rlc2NyaXB0aW9uIjoiQ0dfVEVTVF9EQ1IiLCJzb2Z0d2FyZV9jbGllbnRfaWQiOiI0T09IRzI1YnNaUHZneDltSmZqS1dOIiwic29mdHdhcmVfY2xpZW50X25hbWUiOiJDR19URVNUX0RDUiIsInNvZnR3YXJlX2NsaWVudF91cmkiOiJodHRwczovL3Rlc3QuY29tIiwic29mdHdhcmVfZW52aXJvbm1lbnQiOiJzYW5kYm94Iiwic29mdHdhcmVfaWQiOiI0T09IRzI1YnNaUHZneDltSmZqS1dOIiwic29mdHdhcmVfandrc19lbmRwb2ludCI6Imh0dHBzOi8va2V5c3RvcmUub3BlbmJhbmtpbmd0ZXN0Lm9yZy51ay8wMDE1ODAwMDAwamZROWFBQUUvNE9PSEcyNWJzWlB2Z3g5bUpmaktXTi5qd2tzIiwic29mdHdhcmVfandrc19yZXZva2VkX2VuZHBvaW50IjoiaHR0cHM6Ly9rZXlzdG9yZS5vcGVuYmFua2luZ3Rlc3Qub3JnLnVrLzAwMTU4MDAwMDBqZlE5YUFBRS9yZXZva2VkLzRPT0hHMjVic1pQdmd4OW1KZmpLV04uandrcyIsInNvZnR3YXJlX2xvZ29fdXJpIjoiaHR0cHM6Ly90ZXN0LmNvbSIsInNvZnR3YXJlX21vZGUiOiJMaXZlIiwic29mdHdhcmVfb25fYmVoYWxmX29mX29yZyI6IkJPSSIsInNvZnR3YXJlX3BvbGljeV91cmkiOiJodHRwczovL3Rlc3QuY29tIiwic29mdHdhcmVfcmVkaXJlY3RfdXJpcyI6WyJodHRwczovL3Rlc3QuY29tIiwiaHR0cHM6Ly90ZXN0MS5jb20iXSwic29mdHdhcmVfcm9sZXMiOlsiQUlTUCIsIlBJU1AiXSwic29mdHdhcmVfdG9zX3VyaSI6Imh0dHBzOi8vdGVzdC5jb20iLCJzb2Z0d2FyZV92ZXJzaW9uIjoxLjF9.o83r8Ie_O2kzosJwaIaZjZG3fxU-ZS5oVxih5xHhX04WICJIUSjcJuvvm3a0Wu8QKpPEXldmB8PplaML8yxHdWPGcAzQggOJk_R8s39VZAjFJ6ZlnNQE4xvhEglVcJCfwEy4SvqrCVo2WZ7p1lEgOOIxDYq88dWtumi5ncliDoU_rUhrmIISi0IAvDYHj2e9OUDMTUpgv8KunBwF4F1D0NJwmfw1nUHjIkXdwJsgu_gSbPzbBU2_SScAC8AJnmPCK5nOVtvokDC2XIjWdUNlrkQG8f7-iabivoiKvOJORhAN0pc7n8_cKrBco8Ufa7WjK32xiIxHmTKrZhIBfh6dRQ",
				"WEB", "RS256", "RS256","dn");

		return registrationRequest;
	}

	public static SSAModel setSSAModel() {

		SSAModel ssaModel = new SSAModel();
		ssaModel.setIat(1537257843);
		ssaModel.setIss("1wwLDFoVlHfnlK0Dgu71lB");
		ssaModel.setSoftware_id("1wwLDFoVlHfnlK0Dgu71lB");
		ssaModel.setJti("7aac9164-eeac-47d6-8042-9a24610a984e");
		ssaModel.setOrg_id("0015800000jfQ9aAAE");
		ssaModel.setOrg_jwks_endpoint("https://auth-test.apibank-cma2plus.in");
		ssaModel.setSoftware_jwks_endpoint("https://auth-test.apibank-cma2plus.in");
		ssaModel.setSoftware_client_id("1wwLDFoVlHfnlK0Dgu71lB");
		ssaModel.setSoftware_client_name("CG_TPP_DCR");
		ssaModel.setSoftware_client_uri("https://auth-test.apibank-cma2plus.in");
		ssaModel.setSoftware_logo_uri("https://auth-test.apibank-cma2plus.in");
		ArrayList<String> software_roles = new ArrayList<>();
		software_roles.add("aisp");
		software_roles.add("pisp");
		
		ssaModel.setSoftware_roles(software_roles);
		
		return ssaModel;
	}

	public static RegistrationResponse getRegistrationResponse() {
		RegistrationResponse registrationResponse = new RegistrationResponse();

		return registrationResponse;
	}
	
	public static OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations getAuthorisationsA1()
	{
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations a1=new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations();
		a1.setActive(true);
		a1.setMemberState("GB");
		a1.setPsd2Role("PISP");
		return a1;
	}
	
	public static OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations getAuthorisationsA2()
	{
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations a1=new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations();
		a1.setActive(true);
		a1.setMemberState("GB");
		a1.setPsd2Role("AISP");
		return a1;
	}
	
	
	public static OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations getAuthorisationsA3()
	{
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations a1=new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations();
		a1.setActive(true);
		a1.setMemberState("IR");
		a1.setPsd2Role("AISP");
		return a1;
	}
	
	public static OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations getAuthorisationsA4()
	{
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations a1=new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations();
		a1.setActive(false);
		a1.setMemberState("IR");
		a1.setPsd2Role("PISP");
		return a1;
	}
	
	public static OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations getAuthorisationsA5()
	{
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations a1=new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations();
		a1.setActive(true);
		a1.setMemberState("NR");
		a1.setPsd2Role("AISP");
		return a1;
	}
	
	
	public static Authorisation getAuthorisation1() {
	
		Authorisation C1=new Authorisation();
		C1.setMember_state("GB");
		ArrayList<String> roles=new ArrayList<>();
		roles.add("AISP");
		roles.add("PISP");
		//roles.add("CISP");
		C1.setRoles(roles);
		return C1;
	}
	
	public static Authorisation getAuthorisation2() {
		
		Authorisation C1=new Authorisation();
		C1.setMember_state("IR");
		ArrayList<String> roles=new ArrayList<>();
		roles.add("AISP");
		roles.add("PISP");
		C1.setRoles(roles);
		
		return C1;
		
	}
	
public static Authorisation getAuthorisation3() {
		
		Authorisation C1=new Authorisation();
		C1.setMember_state("NR");
		ArrayList<String> roles=new ArrayList<>();
		roles.add("AISP");
		roles.add("NISP");
		C1.setRoles(roles);
		
		return C1;
		
	}
}
