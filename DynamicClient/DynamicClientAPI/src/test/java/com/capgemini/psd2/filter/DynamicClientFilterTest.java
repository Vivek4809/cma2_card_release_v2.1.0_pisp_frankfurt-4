package com.capgemini.psd2.filter;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

@RunWith(SpringJUnit4ClassRunner.class)
public class DynamicClientFilterTest {

	@Mock
	private HttpServletRequest httpServletRequest;
	
	@Mock
	private HttpServletResponse httpServletResponse;
	
	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;
	
	
	@Mock
	private FilterChain filterChain;
	
	@Mock
	private LoggerUtils loggerUtils;

	@InjectMocks
	private DynamicClientFilter dynamicClientFilter;
	
	
	

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

	}

	@Test
	public void dynamicClientFilterSuccessTest() throws IOException, ServletException {
		
		Mockito.when(httpServletRequest.getHeader("DN")).thenReturn("ba4f73f89a60425baed82a7ef2509fea");
		Mockito.when(httpServletRequest.getHeader("OU")).thenReturn("ba4f73f89a60");
		Mockito.when(httpServletRequest.getMethod()).thenReturn("POST");
		
		dynamicClientFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);
	}
	
	@After
	public void tearDown() throws Exception {
	}

}
