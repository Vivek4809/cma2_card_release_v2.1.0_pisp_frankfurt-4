package com.capgemini.psd2.utilities;

import static org.junit.Assert.assertNotNull;

import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.exception.ClientRegistrationException;
import com.nimbusds.jose.util.X509CertUtils;

@RunWith(SpringJUnit4ClassRunner.class)
public class RolesUtilTest {

	@InjectMocks
	RolesUtil util;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
	}
	
	@Test
	public void testRoles() {
		List<String> ssaRoles=new ArrayList<>();
		ssaRoles.add("PISP");
		List<String> eIDASRoles=new ArrayList<>();
		eIDASRoles.add("PISP");
		List<String> ssaOrgRoles=new ArrayList<>();
		ssaOrgRoles.add("PISP");
		
		List<String> passportingRoles=new ArrayList<>();
		passportingRoles.add("PISP");
		util.extract(ssaRoles, null, ssaOrgRoles, passportingRoles);
	}
	
	@Test
	public void testRoleswithNULlEidasroles() {
		List<String> ssaRoles=new ArrayList<>();
		ssaRoles.add("PISP");
		//List<String> eIDASRoles=new ArrayList<>();
		//eIDASRoles.add("PISP");
		List<String> ssaOrgRoles=new ArrayList<>();
		ssaOrgRoles.add("PISP");
		
		List<String> passportingRoles=new ArrayList<>();
		passportingRoles.add("PISP");
		util.extract(ssaRoles, null, ssaOrgRoles, passportingRoles);
	}
	
	@Test
	public void testRolesfromCertificate() {
		
		X509Certificate cert = X509CertUtils.parse(
                "-----BEGIN CERTIFICATE-----\r\n\r\nMIIJ6zCCCNOgAwIBAgINPs68rPhniLTa66siCjANBgkqhkiG9w0BAQsFADBqMQsw\r\n\r\nCQYDVQQGEwJIVTERMA8GA1UEBwwIQnVkYXBlc3QxFjAUBgNVBAoMDU1pY3Jvc2Vj\r\n\r\nIEx0ZC4xFDASBgNVBAsMC2UtU3ppZ25vIENBMRowGAYDVQQDDBFlLVN6aWdubyBU\r\n\r\nZXN0IENBMzAeFw0xODA4MTUxNDQzMTZaFw0xODExMTMxNDQzMTZaMIHjMRMwEQYL\r\n\r\nKwYBBAGCNzwCAQMMAlVLMR0wGwYDVQQPDBRQcml2YXRlIE9yZ2FuaXphdGlvbjEO\r\n\r\nMAwGA1UEBRMFNDU1NTExCzAJBgNVBAYTAlVLMQ8wDQYDVQQHDAZMb25kb24xJjAk\r\n\r\nBgNVBAoMHU5hdGlvbmFsIFdlc3RtaW5zdGVyIEJhbmsgUGxjMRkwFwYDVQRhDBBQ\r\n\r\nU0RVSy1GQ0EtMTIxODc4MRQwEgYDVQQLDAtPcGVuQmFua2luZzEMMAoGA1UECwwD\r\n\r\nREVTMRgwFgYDVQQDDA93d3cubmF0d2VzdC5jb20wggEiMA0GCSqGSIb3DQEBAQUA\r\n\r\nA4IBDwAwggEKAoIBAQD0Q1hICMp6v+wZ/1mGH+t9R7g6qaPmBelWwbTSLAKzTO68\r\n\r\nWMbl7OSJvWsXgnqEDxpMJM3ViIbrntdZZcKvMdh6IjmWRLfiQwQQrcbZukU5aULR\r\n\r\nyi7JT84YsSiKBrXR0VI/I8Pmp14uLr2p5Qy8pzahniOblPMC7ZlNbfbnar335Uzh\r\n\r\n15ZkvNXR9FZ5mwQOW5Nj+zTL5yE3Qhuk6nrMUnaK7SUyQIK1NR3anQ8HR6eB/XXA\r\n\r\nhMEF2BKVoah/AJqmlWaubfAuwVu1/Lw1/BjKxajbRhK6yYiKMzpc7QCKUqH6IzCS\r\n\r\nmF45TAKYoBfwj4/WtX4rpo+ffQP+gDTP0q/pMOGPAgMBAAGjggYUMIIGEDAOBgNV\r\n\r\nHQ8BAf8EBAMCBaAwggEFBgorBgEEAdZ5AgQCBIH2BIHzAPEAdwCqHngEP4Bkgivl\r\n\r\nTLXBysLjLuRQ5nMwKki8JpcLmywIHQAAAWU+CrCBAAAEAwBIMEYCIQC+mIHdRY4U\r\n\r\n52+sPYHwODOd450fEd1RbLQVJvdck/VIhAIhAI8KMwHHjef45NR0lcJzV+hLp18s\r\n\r\nxitVf2RbFIPgAvLwAHYABVAhwruDpogYp0GO9LHwYCJ4asUHXakHkg6btr0tx04A\r\n\r\nAAFlPgqsUQAABAMARzBFAiEAo9Q7p+aqtYxqRDtPjKUZd8Id7Drm1GMBgo05boiD\r\n\r\nmVMCICLBhJZBzYxWBPmnhHsSOq8+BhfRwK0k3XJhweTcZY7wMBMGA1UdJQQMMAoG\r\n\r\nCCsGAQUFBwMBMIIC8AYDVR0gBIIC5zCCAuMwggLfBgwrBgEEAYGoGAIBAWQwggLN\r\n\r\nMCYGCCsGAQUFBwIBFhpodHRwOi8vY3AuZS1zemlnbm8uaHUvcWNwczCBpQYIKwYB\r\n\r\nBQUHAgIwgZgMgZVUZXN0IHF1YWxpZmllZCBjZXJ0aWZpY2F0ZSBmb3Igd2Vic2l0\r\n\r\nZSBhdXRoZW50aWNhdGlvbi4gVGhlIHByb3ZpZGVyIHByZXNlcnZlcyByZWdpc3Ry\r\n\r\nYXRpb24gZGF0YSBmb3IgMTAgeWVhcnMgYWZ0ZXIgdGhlIGV4cGlyYXRpb24gb2Yg\r\n\r\ndGhlIGNlcnRpZmljYXRlLjCBlQYIKwYBBQUHAgIwgYgMgYVURVNUIGNlcnRpZmlj\r\n\r\nYXRlIGlzc3VlZCBvbmx5IGZvciB0ZXN0aW5nIHB1cnBvc2VzLiBUaGUgaXNzdWVy\r\n\r\nIGlzIG5vdCBsaWFibGUgZm9yIGFueSBkYW1hZ2VzIGFyaXNpbmcgZnJvbSB0aGUg\r\n\r\ndXNlIG9mIHRoaXMgY2VydGlmaWNhdGUhMIGyBggrBgEFBQcCAjCBpQyBolRlc3p0\r\n\r\nIG1pbsWRc8OtdGV0dCB3ZWJvbGRhbC1oaXRlbGVzw610xZEgdGFuw7pzw610dsOh\r\n\r\nbnkuIEEgcmVnaXN6dHLDoWNpw7NzIGFkYXRva2F0IGEgc3pvbGfDoWx0YXTDsyBh\r\n\r\nIHRhbsO6c8OtdHbDoW55IGxlasOhcnTDoXTDs2wgc3rDoW3DrXRvdHQgMTAgw6l2\r\n\r\naWcgxZFyemkgbWVnLjCBrQYIKwYBBQUHAgIwgaAMgZ1UZXN6dGVsw6lzaSBjw6ls\r\n\r\ncmEga2lhZG90dCBURVNaVCB0YW7DunPDrXR2w6FueS4gQSBoYXN6bsOhbGF0w6F2\r\n\r\nYWwga2FwY3NvbGF0b3NhbiBmZWxtZXLDvGzFkSBrw6Fyb2vDqXJ0IGEgU3pvbGfD\r\n\r\noWx0YXTDsyBzZW1taWx5ZW4gZmVsZWzFkXNzw6lnZXQgbmVtIHbDoWxsYWwhMB0G\r\n\r\nA1UdDgQWBBT8tVeOG7j8MG99u/ucFURYUPKh4TAfBgNVHSMEGDAWgBTc5gIo7zcw\r\n\r\nj4k+oK0gVfPvNujwzTAaBgNVHREEEzARgg93d3cubmF0d2VzdC5jb20wMgYDVR0f\r\n\r\nBCswKTAnoCWgI4YhaHR0cDovL3Rlc3p0LmUtc3ppZ25vLmh1L1RDQTMuY3JsMG8G\r\n\r\nCCsGAQUFBwEBBGMwYTAwBggrBgEFBQcwAYYkaHR0cDovL3Rlc3p0LmUtc3ppZ25v\r\n\r\nLmh1L3Rlc3RjYTNvY3NwMC0GCCsGAQUFBzAChiFodHRwOi8vdGVzenQuZS1zemln\r\n\r\nbm8uaHUvVENBMy5jcnQwgeoGCCsGAQUFBwEDBIHdMIHaMAgGBgQAjkYBATALBgYE\r\n\r\nAI5GAQMCAQowUwYGBACORgEFMEkwJBYeaHR0cHM6Ly9jcC5lLXN6aWduby5odS9x\r\n\r\nY3BzX2VuEwJFTjAhFhtodHRwczovL2NwLmUtc3ppZ25vLmh1L3FjcHMTAkhVMBMG\r\n\r\nBgQAjkYBBjAJBgcEAI5GAQYDMFcGBgQAgZgnAjBNMCYwEQYHBACBmCcBAgwGUFNQ\r\n\r\nX1BJMBEGBwQAgZgnAQMMBlBTUF9BSQwbRmluYW5jaWFsIENvbmR1Y3QgQXV0aG9y\r\n\r\naXR5DAZVSy1GQ0EwDQYJKoZIhvcNAQELBQADggEBAFQ+Cs5owTyWtY68PAD5eqX+\r\n\r\nBpoySeKzs5tXsxr64zTjMXakbixU97nwxnAXLt/sScBgE4EZDpwWk85qxeCQDhgk\r\n\r\nXOcCdPsabTRPb22vaJpM6tClXiG89dgStSwxVSzpoq53VTUY5wa9nTkoUsmy5YMT\r\n\r\nASyzFCeK4JL4LPQ3Fvqvfyh4VsdbgVKslaWNgWvTfFvr8rBNNYZZCY/NNPwrPU/M\r\n\r\njI2TVwAaj2MHVtYFMuhdYvUMNLCsHlfECNjf3tBGccoylVguogB2uCse9MAU+xO3\r\n\r\nYW4mOTertht5uhTWPsL6fwGrFiRKiSvorR7FTGtc+y1L5snwnp7Y5ynny3Ygr54=\r\n\r\n-----END CERTIFICATE-----");
		assertNotNull(util.extractRolesFromCertificate(cert));
	
	
	}
	
	
}
