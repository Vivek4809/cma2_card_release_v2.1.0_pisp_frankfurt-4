/**
 * 
 */
package com.capgemini.psd2.service.impl;

import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.naming.NamingException;
import javax.naming.directory.BasicAttributes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.config.PFClientRegConfig;
import com.capgemini.psd2.constant.ClientRegistarionConstants;
import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.jwt.JWTReader;
import com.capgemini.psd2.jwt.JWTUtil;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.model.OBThirdPartyProviders;
import com.capgemini.psd2.service.DynamicClientService;
import com.capgemini.psd2.service.PingDirectoryService;
import com.capgemini.psd2.service.PortalService;
import com.capgemini.psd2.utilities.DCRUtil;
import com.capgemini.psd2.utilities.GetCertificateType;
import com.capgemini.psd2.utilities.JwtDecoder;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.utilities.RolesUtil;
import com.capgemini.psd2.utilities.TppStatusCheck;
import com.capgemini.psd2.validator.HeaderValidationContext;
import com.capgemini.psd2.validator.PayloadValidationContext;
import com.capgemini.psd2.validator.RegistrationRequestValidator;
import com.capgemini.psd2.validator.enums.CertificateTypes;
import com.capgemini.tpp.dtos.GrantScopes;
import com.capgemini.tpp.registration.model.RegistrationRequest;
import com.capgemini.tpp.registration.model.RegistrationResponse;
import com.capgemini.tpp.registration.model.SSAWrapper;
import com.capgemini.tpp.ssa.model.SSAModel;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.util.Base64;
import com.nimbusds.jose.util.X509CertUtils;
import com.nimbusds.jwt.JWT;

/**
 * @author nagoswam
 *
 */
@Service
public class DynamicClientServiceImpl implements DynamicClientService {
	
	@Autowired
	RegistrationRequestValidator registrationRequestValidator;
	
	@Autowired
	PortalService portalService;
	
	@Autowired
	private PingDirectoryService pingDirectoryService;
	
	@Autowired
	RequestHeaderAttributes requestHeaderAttributes;

	@Autowired
	PayloadValidationContext payloadValidationContext;
	
	@Autowired
	HeaderValidationContext headerValidationContext;
	
	@Autowired
	private PFClientRegConfig pfClientRegConfig;
	
	@Autowired
	private TppStatusCheck tppStatusCheck;
	
	@Autowired
	private GetCertificateType certUtil;	
	

	@Autowired
	private RolesUtil roleUtil;
	
	@Value("${openbanking.passportingcheck:false}")
	private boolean passportingcheck;
	

	@Value("${app.jwk.prefix}")
	private String jwkPrefix;
	
	@Value("${app.selfsignssa:false}")
	private boolean selfsignssa;
	
	@Override
	public SSAWrapper validateRegistrationRequest(String registerJWT,String clientId){
		
		RegistrationRequest registrationRequest=null;
		SSAModel ssaModel =  null;
		String ssaOriginalToken = null;
		SSAModel ssaModelOriginal = null;
		String jwks=null;
		JWT jwt=JWTUtil.checkJWTValidity(registerJWT);

		
		if(clientId != null && clientId.trim().length() > 0){
			ssaOriginalToken = fetchSSAForClient(clientId);
			
		}
		
		registrationRequest=JwtDecoder.decodeTokenIntoRegistrationRequest(jwt);
		ssaModel=JWTReader.decodeTokentoSSAModel(registrationRequest.getSoftware_statement());
		
		X509Certificate cert = X509CertUtils.parse(requestHeaderAttributes.getX_ssl_client_cert());
		List<String> eIDASroles=roleUtil.extractRolesFromCertificate(cert);
		certUtil.setCertType(jwt, ssaModel.getIss(),registrationRequest.getSoftware_statement(),eIDASroles);
		headerValidationContext.execute(ssaModel);
		
		if (CertificateTypes.NON_OB_EIDAS_CERT.name().equals(requestHeaderAttributes.getChannelId())) {
			
			if(!selfsignssa){
				throw ClientRegistrationException.populatePortalException(ClientRegistrationErrorCodeEnum.SELF_SIGNED_SSA_NOT_SUPPORTED);	
			}
			ssaModel.setSoftware_client_id(
					ssaModel.getSoftware_client_id().concat(requestHeaderAttributes.getX_ssl_client_ncaid()));
			List<Base64> listx5c=(List<Base64>)jwt.getHeader().toJSONObject().get(PSD2Constants.X5C);
			JWKSet jwkset=DCRUtil.generateJwk(listx5c.get(0));	
			jwks=jwkset.toString();
			ssaModel.setPublicKey(jwks);
			ssaModel.setSoftware_jwks_endpoint(jwkPrefix+ssaModel.getOrg_id()+PSD2Constants.SLASH+ssaModel.getSoftware_client_id()+".jwks?tenantId="+requestHeaderAttributes.getTenantId());
		}
		registrationRequestValidator.registrationRequestSignatureValidation(jwt,ssaModel.getSoftware_jwks_endpoint(),jwks);
		if(!CertificateTypes.NON_OB_EIDAS_CERT.name().equals(requestHeaderAttributes.getChannelId())){
			verifyOBSSA(registrationRequest.getSoftware_statement());
		}
		headerValidations();
		ssaModel.setClientCertSubjectDn(cert.getSubjectDN().toString());
		if(ssaOriginalToken != null && ssaOriginalToken.trim().length() > 0){
			ssaModelOriginal = updateClientFlow(registrationRequest, ssaOriginalToken);
		}		
		payloadValidationContext.execute(registrationRequest, ssaModel,ssaModelOriginal != null);
		if (passportingcheck && !CertificateTypes.NON_OB_EIDAS_CERT.name().equals(requestHeaderAttributes.getChannelId())) {
			OBThirdPartyProviders tppDetails=tppStatusCheck.isTppActive(ssaModel.getOrg_id());
			List<String> finalRoles=tppStatusCheck.passportingCheck(tppDetails, ssaModel.getSoftware_roles(), eIDASroles,
					ssaModel.getOrganisation_competent_authority_claims().getAuthorisations());
			ssaModel.setSoftware_roles((ArrayList<String>) finalRoles);
		}
		
		if (CertificateTypes.NON_OB_EIDAS_CERT.name().equals(requestHeaderAttributes.getChannelId()) && !eIDASroles.containsAll(ssaModel.getSoftware_roles())) {
				throw ClientRegistrationException.populatePortalException(ClientRegistrationErrorCodeEnum.SCOPES_NOT_SUPPORTED);
		}
		SSAWrapper ssaWrapper=new SSAWrapper();
		ssaWrapper.setSsaModel(ssaModel);
		ssaWrapper.setSsaToken(registrationRequest.getSoftware_statement());
		ssaWrapper.setRegistrationRequest(registrationRequest);
		return ssaWrapper;
	}


	/**
	 * @param registrationRequest
	 */
	private void verifyOBSSA(String ssaToken) {
		try {
			portalService.verifyToken(ssaToken);
		} catch (InvalidKeySpecException | NoSuchAlgorithmException | ParseException | JOSEException e) {
			throw ClientRegistrationException.populatePortalException(e.getMessage(), ClientRegistrationErrorCodeEnum.SSA_SIGNATURE_FAILED);
		}
	}


	/**
	 * @param registrationRequest
	 * @param ssaOriginalToken
	 * @return
	 */
	private SSAModel updateClientFlow(RegistrationRequest registrationRequest, String ssaOriginalToken) {
		SSAModel ssaModelOriginal;
		ssaModelOriginal = JWTReader.decodeTokentoSSAModel(ssaOriginalToken);
		SSAModel ssaCurrentModel=JWTReader.decodeTokentoSSAModel(registrationRequest.getSoftware_statement());
		
		boolean matchResult = ssaCurrentModel.equals(ssaModelOriginal);
		if(!matchResult){
			throw ClientRegistrationException
			.populatePortalException(ClientRegistrationErrorCodeEnum.CLIENT_UPDATE_OP_FAILED);				
		}
		return ssaModelOriginal;
	}

	
	@Override
	public RegistrationResponse clientRegistration(SSAWrapper ssaWrap,boolean updateFlag) {
		RegistrationResponse registrationResponse=new RegistrationResponse();
		try {
			setgrantTypesFromRequest(ssaWrap);
			if(updateFlag){
				portalService.updateTppApplication(ssaWrap.getSsaModel());
			}
			else {
				portalService.createTppApplication(ssaWrap.getSsaModel(), ssaWrap.getSsaModel().getOrg_id(), ssaWrap.getSsaToken());				
			}
			registrationResponse.setApplication_type(ssaWrap.getRegistrationRequest().getApplication_type());
			registrationResponse.setGrant_types(ssaWrap.getRegistrationRequest().getGrant_types());
			registrationResponse.setId_token_signed_response_alg(ssaWrap.getRegistrationRequest().getId_token_signed_response_alg());
			registrationResponse.setOrg_id(ssaWrap.getSsaModel().getOrg_id());
			registrationResponse.setRedirect_uris(ssaWrap.getRegistrationRequest().getRedirect_uris());
			registrationResponse.setScope(ssaWrap.getRegistrationRequest().getScope());
			registrationResponse.setSoftware_roles(ssaWrap.getSsaModel().getSoftware_roles());
			registrationResponse.setSoftware_id(ssaWrap.getRegistrationRequest().getSoftware_id());
			registrationResponse.setRequest_object_signing_alg(ssaWrap.getRegistrationRequest().getRequest_object_signing_alg());
			registrationResponse.setToken_endpoint_auth_method(ssaWrap.getRegistrationRequest().getToken_endpoint_auth_method());
			registrationResponse.setClient_id(ssaWrap.getSsaModel().getSoftware_client_id());
			if (!CertificateTypes.NON_OB_EIDAS_CERT.name().equals(requestHeaderAttributes.getChannelId())) {
			registrationResponse.setSoftware_jwks_endpoint(ssaWrap.getSsaModel().getSoftware_jwks_endpoint());
			}
			registrationResponse.setSoftware_logo_uri(ssaWrap.getSsaModel().getSoftware_logo_uri());
			registrationResponse.setSoftware_client_name(ssaWrap.getSsaModel().getSoftware_client_name());			
		
		} catch (Exception e) {
			throw ClientRegistrationException.populatePortalException(e.getMessage(), ClientRegistrationErrorCodeEnum.UNAPPROVED_SSA_ERROR);
		}
		
		return registrationResponse;
	}
	
	private void headerValidations() {
		if (CertificateTypes.OB_EXISTING_CERT.name().equals(requestHeaderAttributes.getChannelId())
				&& (NullCheckUtils.isNullOrEmpty(requestHeaderAttributes.getX_ssl_client_cn())
						|| NullCheckUtils.isNullOrEmpty(requestHeaderAttributes.getX_ssl_client_ou()))) {
			throw ClientRegistrationException
					.populatePortalException(ClientRegistrationErrorCodeEnum.INVALID_CERTIFICATE);
		}
	}
	
	public String fetchSSAForClient(String clientId){
		List<Object> listObj = pingDirectoryService.fetchTppAppMappingForClient(clientId);
		BasicAttributes object = ((BasicAttributes) listObj.get(0));
		try {
			return getAttributeValue(object, "ssaToken");
		} catch (NamingException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.NO_CLIENT_APP_DATA_FOUND);
		}		
	}
	
	
	private String getAttributeValue(BasicAttributes tppApplication, String ldapAttr) throws NamingException {
		String attributeValue = null;
		if (tppApplication.get(ldapAttr) != null && tppApplication.get(ldapAttr).get() != null) {
			attributeValue = tppApplication.get(ldapAttr).get().toString();
		}
		return attributeValue;
	}	
	private void setgrantTypesFromRequest(SSAWrapper ssaWrap){
		List<String> roles = ssaWrap.getSsaModel().getSoftware_roles();
		Map<String, GrantScopes> map=pfClientRegConfig.getGrantsandscopes();
		for (String role : roles) {
			List<String> getGranttypes=map.get(role).getGranttypes();
			getGranttypes.clear();
			getGranttypes.addAll(ssaWrap.getRegistrationRequest().getGrant_types().stream().map(String::toUpperCase).collect(Collectors.toList()));
			getGranttypes.add(ClientRegistarionConstants.IMPLICIT_GRANT_SCOPE);
		}
	}
}
