package com.capgemini.psd2.validator;

import java.util.Iterator;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.config.WellKnownConfig;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.tpp.registration.model.RegistrationRequest;
import com.capgemini.tpp.ssa.model.SSAModel;
/**
 * 
 * @author nagoswam
 *
 */
@Component
public class PayloadValidationContext {

	private static final Logger LOGGER = LoggerFactory.getLogger(PayloadValidationContext.class);
	@Autowired
	WellKnownConfig wellKnownConfig;
	
	@Autowired
	RequestHeaderAttributes headerAttributes;

	private Set<ValidationStrategy> insertFlowValidationStrategies;
	private Set<ValidationStrategy> updateFlowValidationStrategies;

	public PayloadValidationContext(Set<ValidationStrategy> payloadValidationStrategies,Set<ValidationStrategy> updateFlowValidationStrategies) {
		this.insertFlowValidationStrategies = payloadValidationStrategies;
		this.updateFlowValidationStrategies = updateFlowValidationStrategies;
	}

	/*
	 * This method performs validation for fields one by one and return the
	 * invalid one if found. Otherwise, it will continue validating remaining
	 * fields. If all the fields are valid then will return null.
	 */
	public void execute(RegistrationRequest regRequest, SSAModel ssaModel,boolean updateFlowFlag) {
		LOGGER.info("Enter payload validations");
		ValidationStrategy payloadValidation;
		for (Iterator<ValidationStrategy> iterator = (updateFlowFlag ? updateFlowValidationStrategies.iterator() : insertFlowValidationStrategies.iterator()); iterator.hasNext();) {
			payloadValidation = iterator.next();		
			payloadValidation.validate(regRequest, ssaModel, wellKnownConfig,headerAttributes);
		}
		LOGGER.info("Exit payload validations successfully");
	}
}
