package com.capgemini.psd2.utilities;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;
import javax.naming.ldap.Rdn;
import javax.xml.bind.DatatypeConverter;

import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.logger.PSD2Constants;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.util.Base64;
import com.nimbusds.jose.util.X509CertUtils;

public final class DCRUtil {
	
	private DCRUtil(){}
	
	public static String extractAttributesDN(String subDn, String attribute) throws InvalidNameException {
		LdapName ln = null;
		String dn = null;
		ln = new LdapName(subDn);
			for (Rdn rdn : ln.getRdns()) {
				if (rdn.getType().equalsIgnoreCase(attribute)) {
					dn = rdn.getValue().toString();
					break;
				}
			}
		return dn;
	}

	public static String extractPattren(String o, String v) {
			Pattern pattern = Pattern.compile(v,Pattern.CASE_INSENSITIVE);
			String woSpace=o.replaceAll("\\s+", "");
			Matcher matcher = pattern.matcher(woSpace);
			matcher.find();
			int i = matcher.end();
			return woSpace.substring(i + 1).split(",")[0];
		}

	
	public static JWKSet generateJwk(Base64 certificate) {
		List<Base64> lisBase = new ArrayList<>();

		X509Certificate x509cert = X509CertUtils.parse(formatCert(certificate.toString()));
		JWK jwk = null;

		try {
			lisBase.add(Base64.encode(x509cert.getEncoded()));
			jwk = new RSAKey.Builder((RSAPublicKey) x509cert.getPublicKey()).keyUse(KeyUse.SIGNATURE)
					.keyID(extractSha1Thumbprint(x509cert)).x509CertChain(lisBase).build();
		} catch (CertificateEncodingException | NoSuchAlgorithmException e) {
			throw ClientRegistrationException.populatePortalException(e.getMessage(),
					ClientRegistrationErrorCodeEnum.INVALID_X5C_CERT);
		}
		return new JWKSet(jwk);
	}
	
	public static String extractSha1Thumbprint(X509Certificate xCert)
			throws NoSuchAlgorithmException, CertificateEncodingException {
		MessageDigest md = MessageDigest.getInstance("SHA-1");
		byte[] encodedCert = xCert.getEncoded();
		md.update(encodedCert);
		byte[] digest = md.digest();
		String digestHex = DatatypeConverter.printHexBinary(digest);
		return digestHex.toLowerCase();
	}
	
	public static String formatCert(String cert) {
		String result = cert;
		if (result == null || result.isEmpty())
			return null;

		int markerStart = cert.indexOf(PSD2Constants.PEM_BEGIN_MARKER);
		int markerEnd = cert.indexOf(PSD2Constants.PEM_END_MARKER);

		if (markerStart < 0)
			result = PSD2Constants.PEM_BEGIN_MARKER + result;

		if (markerEnd < 0)
			result = result + PSD2Constants.PEM_END_MARKER;

		return result;

	}
}
