/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.exception.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

import com.capgemini.psd2.aspect.DynamicAspectUtils;
import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.rest.client.exception.handler.ExceptionHandler;
import com.capgemini.psd2.utilities.JSONUtilities;


/**
 * The Class ExceptionHandlerImpl.
 */
public class DynamicClientRestClientHandler implements ExceptionHandler{

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.rest.client.exception.handler.ExceptionHandler#handleException(org.springframework.web.client.HttpServerErrorException)
	 */
	@Autowired
	DynamicAspectUtils aspectutils;
	
	@Autowired
	LoggerAttribute loggerAttribute;
	
	@Override
	public void handleException(HttpServerErrorException e) {
		String responseBody  = e.getResponseBodyAsString();
		aspectutils.exceptionPayload(loggerAttribute, e,responseBody);
		if (responseBody == null || responseBody.isEmpty()) {
			throw ClientRegistrationException.populatePortalException(ClientRegistrationErrorCodeEnum.BAD_REQUEST);
		}
		
			ErrorInfo errorInfoObj = JSONUtilities.getObjectFromJSONString(responseBody, ErrorInfo.class); 
			errorInfoObj.setStatusCode(String.valueOf(e.getStatusCode().value()));
			throw new ClientRegistrationException(errorInfoObj.getDetailErrorMessage(), errorInfoObj);
	}

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.rest.client.exception.handler.ExceptionHandler#handleException(org.springframework.web.client.HttpClientErrorException)
	 */
	@Override
	public void handleException(HttpClientErrorException e) {
		String responseBody  = e.getResponseBodyAsString();
		aspectutils.exceptionPayload(loggerAttribute, e,responseBody);
		if (responseBody == null || responseBody.isEmpty()) {
			throw ClientRegistrationException.populatePortalException(ClientRegistrationErrorCodeEnum.BAD_REQUEST);
		}
		
		throw ClientRegistrationException.populatePortalException(e.getResponseBodyAsString(),ClientRegistrationErrorCodeEnum.BAD_REQUEST);
	}

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.rest.client.exception.handler.ExceptionHandler#handleException(org.springframework.web.client.ResourceAccessException)
	 */
	@Override
	public void handleException(ResourceAccessException e) {
		aspectutils.exceptionPayload(loggerAttribute, e,e.getMessage());
		throw ClientRegistrationException.populatePortalException(e.getMessage(), ClientRegistrationErrorCodeEnum.UNAPPROVED_SSA_ERROR);
	}
}