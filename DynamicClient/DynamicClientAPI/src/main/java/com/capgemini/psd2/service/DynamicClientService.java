package com.capgemini.psd2.service;

import com.capgemini.tpp.registration.model.RegistrationResponse;
import com.capgemini.tpp.registration.model.SSAWrapper;

public interface DynamicClientService {

	public RegistrationResponse clientRegistration(SSAWrapper ssaWrapper,boolean updateFlag);
	
	public SSAWrapper validateRegistrationRequest(String registerJWT,String clientId) ;
}
