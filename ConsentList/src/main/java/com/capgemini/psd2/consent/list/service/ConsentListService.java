package com.capgemini.psd2.consent.list.service;

import com.capgemini.psd2.consent.list.data.ConsentListResponse;

@FunctionalInterface
public interface ConsentListService {

	ConsentListResponse getConsentList(String userId, String status, String tenantId, String consentType);
	
}
