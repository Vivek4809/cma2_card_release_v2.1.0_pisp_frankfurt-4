package com.capgemini.psd2.pisp.domestic.standing.order.adapter.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.conditions.MongoDbMockCondition;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.DomesticStandingOrderAdapter;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.DStandingOrderPOST201Response;
import com.capgemini.psd2.pisp.domain.DStandingorderFoundationResource;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.domestic.standing.order.adapter.repository.DomesticStandingOrderFoundationRepository;
import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@Lazy
@Conditional(MongoDbMockCondition.class)
@Component("domesticStandingOrderMongoAdapter")
public class DomesticStandingOrderMongoDbAdapterImpl implements DomesticStandingOrderAdapter {

	@Autowired
	private SandboxValidationUtility utility;

	@Autowired
	private RequestHeaderAttributes reqAttributes;

	@Value("${app.isSandboxEnabled:false}")
	private boolean isSandboxEnabled;

	@Autowired
	private DomesticStandingOrderFoundationRepository repository;

	@Override
	public DStandingOrderPOST201Response processDomesticStandingOrder(CustomDStandingOrderPOSTRequest customRequest,
			Map<String, OBExternalStatus1Code> paymentStatusMap, Map<String, String> params) {

		String paymentRequest = JSONUtilities.getJSONOutPutFromObject(customRequest);
		DStandingorderFoundationResource bankResponse = JSONUtilities.getObjectFromJSONString(
				PispUtilities.getObjectMapper(), paymentRequest, DStandingorderFoundationResource.class);

		String currency = customRequest.getData().getInitiation().getFirstPaymentAmount().getCurrency();
		String initiationAmount = customRequest.getData().getInitiation().getFirstPaymentAmount().getAmount();
		OBChargeBearerType1Code reqChargeBearer = null;

		String submissionId = null;

		ProcessExecutionStatusEnum processStatusEnum = utility.getMockedProcessExecStatus(currency, initiationAmount);
		OBMultiAuthorisation1 multiAuthorisation = utility.getMockedMultiAuthBlock(currency, initiationAmount);

		if (processStatusEnum == ProcessExecutionStatusEnum.PASS
				|| processStatusEnum == ProcessExecutionStatusEnum.FAIL)
			submissionId = UUID.randomUUID().toString();

		bankResponse.getData().setDomesticStandingOrderId(submissionId);
		bankResponse.getData().setCreationDateTime(customRequest.getCreatedOn());

		// removing charges block as this is not returned for BOI on domestic payment
		// APIs
		if (!isSandboxEnabled) {
			List<OBCharge1> charges = utility.getMockedOBChargeList(currency, initiationAmount, reqChargeBearer);
			bankResponse.getData().setCharges(charges);
		}

		bankResponse.setProcessExecutionStatus(processStatusEnum);

		bankResponse.getData().setMultiAuthorisation(multiAuthorisation);
		if (submissionId != null) {
			OBExternalStatus1Code status = calculateCMAStatus(processStatusEnum, multiAuthorisation, paymentStatusMap);
			bankResponse.getData().setStatus(status);
			bankResponse.getData().setStatusUpdateDateTime(customRequest.getCreatedOn());
		}

		try {
			repository.save(bankResponse);
		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}

		return bankResponse;
	}

	@Override
	public DStandingOrderPOST201Response retrieveStagedDomesticStandingOrder(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
		DStandingorderFoundationResource bankResponse = null;

		try {
			bankResponse = repository
					.findOneByDataDomesticStandingOrderId(customPaymentStageIdentifiers.getPaymentSubmissionId());
		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception
					.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, e.getMessage()));
		}

		String initiationAmount = bankResponse.getData().getInitiation().getFirstPaymentAmount().getAmount();
		if (reqAttributes.getMethodType().equals("GET")
				&& utility.isValidAmount(initiationAmount, PSD2Constants.SANDBOX_PISP_GET_MOCKING)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
		}
		return bankResponse;

	}

	private OBExternalStatus1Code calculateCMAStatus(ProcessExecutionStatusEnum processExecutionStatusEnum,
			OBMultiAuthorisation1 multiAuthorisation, Map<String, OBExternalStatus1Code> paymentStatusMap) {

		OBExternalStatus1Code cmaStatus = paymentStatusMap.get(PaymentConstants.INITIAL);

		if (processExecutionStatusEnum == ProcessExecutionStatusEnum.FAIL)
			cmaStatus = paymentStatusMap.get(PaymentConstants.FAIL);
		else if (processExecutionStatusEnum == ProcessExecutionStatusEnum.PASS) {
			if (multiAuthorisation == null || multiAuthorisation.getStatus() == OBExternalStatus2Code.AUTHORISED)
				cmaStatus = paymentStatusMap.get(PaymentConstants.PASS_M_AUTH);
			else if (multiAuthorisation.getStatus() == OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION)
				cmaStatus = paymentStatusMap.get(PaymentConstants.PASS_M_AWAIT);
			else if (multiAuthorisation.getStatus() == OBExternalStatus2Code.REJECTED)
				cmaStatus = paymentStatusMap.get(PaymentConstants.PASS_M_REJECT);
		}
		return cmaStatus;
	}
}
