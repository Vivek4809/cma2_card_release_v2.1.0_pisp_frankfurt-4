package com.capgemini.psd2.pisp.domestic.standing.order.test.adapter.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.DStandingorderFoundationResource;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.domestic.standing.order.adapter.impl.DomesticStandingOrderMongoDbAdapterImpl;
import com.capgemini.psd2.pisp.domestic.standing.order.adapter.repository.DomesticStandingOrderFoundationRepository;
import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticStandingOrderMongoDbAdapterImplTest {

	@Mock
	private SandboxValidationUtility utility;

	@Mock
	private RequestHeaderAttributes reqAttributes;

	@Mock
	private DomesticStandingOrderFoundationRepository repository;

	@InjectMocks
	private List<OBCharge1> chargesList = new ArrayList<OBCharge1>();

	@InjectMocks
	private DomesticStandingOrderMongoDbAdapterImpl adapter;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testProcessDomesticStandingOrderMultiAuthorised() {
		CustomDStandingOrderPOSTRequest request = DomesticStandingOrderMongoDbAdapterImplTestMockData.getRequest();
		Map<String, String> params = new HashMap<>();
		Map<String, OBExternalStatus1Code> pStatusMap = new HashMap<>();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		ProcessExecutionStatusEnum processStatus = ProcessExecutionStatusEnum.PASS;

		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AUTHORISED);

		DStandingorderFoundationResource response = new DStandingorderFoundationResource();

		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);

		when(utility.getMockedProcessExecStatus(anyString(), anyString())).thenReturn(processStatus);

		when(repository.save(any(DStandingorderFoundationResource.class))).thenReturn(response);
		adapter.processDomesticStandingOrder(request, pStatusMap, params);
	}

	@Test
	public void testProcessDomesticStandingOrderAwaitingFurtherAuthorisation() {
		CustomDStandingOrderPOSTRequest request = DomesticStandingOrderMongoDbAdapterImplTestMockData.getRequest();
		Map<String, String> params = new HashMap<>();
		Map<String, OBExternalStatus1Code> pStatusMap = new HashMap<>();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		ProcessExecutionStatusEnum processStatus = ProcessExecutionStatusEnum.PASS;
		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION);

		DStandingorderFoundationResource response = new DStandingorderFoundationResource();

		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(utility.getMockedProcessExecStatus(anyString(), anyString())).thenReturn(processStatus);

		when(repository.save(any(DStandingorderFoundationResource.class))).thenReturn(response);
		adapter.processDomesticStandingOrder(request, pStatusMap, params);
	}

	@Test
	public void testProcessDomesticStandingOrderRejectedAuthorisation() {
		CustomDStandingOrderPOSTRequest request = DomesticStandingOrderMongoDbAdapterImplTestMockData.getRequest();
		Map<String, String> params = new HashMap<>();
		Map<String, OBExternalStatus1Code> pStatusMap = new HashMap<>();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("5900");
		charges.getAmount().setCurrency("USD");
		chargesList.add(charges);
		chargesList.set(0, charges);

		ProcessExecutionStatusEnum processStatus = ProcessExecutionStatusEnum.PASS;
		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.REJECTED);

		DStandingorderFoundationResource response = new DStandingorderFoundationResource();

		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(utility.getMockedProcessExecStatus(anyString(), anyString())).thenReturn(processStatus);

		when(repository.save(any(DStandingorderFoundationResource.class))).thenReturn(response);
		adapter.processDomesticStandingOrder(request, pStatusMap, params);
	}

	@Test
	public void testProcessDomesticStandingOrderNoAuthorisation() {
		CustomDStandingOrderPOSTRequest request = DomesticStandingOrderMongoDbAdapterImplTestMockData.getRequest();
		Map<String, String> params = new HashMap<>();
		Map<String, OBExternalStatus1Code> pStatusMap = new HashMap<>();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		ProcessExecutionStatusEnum processStatus = ProcessExecutionStatusEnum.FAIL;
		OBMultiAuthorisation1 multiAuthorisation = null;

		DStandingorderFoundationResource response = new DStandingorderFoundationResource();

		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(utility.getMockedProcessExecStatus(anyString(), anyString())).thenReturn(processStatus);

		when(repository.save(any(DStandingorderFoundationResource.class))).thenReturn(response);
		adapter.processDomesticStandingOrder(request, pStatusMap, params);
	}

	@Test
	public void testProcessDomesticStandingOrderWithNullSubmissionId() {
		CustomDStandingOrderPOSTRequest request = DomesticStandingOrderMongoDbAdapterImplTestMockData.getRequest();
		Map<String, String> params = new HashMap<>();
		Map<String, OBExternalStatus1Code> pStatusMap = new HashMap<>();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.REJECTED);

		DStandingorderFoundationResource response = new DStandingorderFoundationResource();

		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);

		when(repository.save(any(DStandingorderFoundationResource.class))).thenReturn(response);
		adapter.processDomesticStandingOrder(request, pStatusMap, params);
	}

	@Test(expected = PSD2Exception.class)
	public void testProcessDomesticStandingOrderDataAccessResourceFailureException() {
		CustomDStandingOrderPOSTRequest request = DomesticStandingOrderMongoDbAdapterImplTestMockData.getRequest();
		Map<String, String> params = new HashMap<>();
		Map<String, OBExternalStatus1Code> pStatusMap = new HashMap<>();

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.REJECTED);

		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(multiAuthorisation);
		when(utility.getMockedConsentProcessExecStatus(anyString(), anyString()))
				.thenThrow(new DataAccessResourceFailureException("Test"));

		when(repository.save(any(DStandingorderFoundationResource.class)))
				.thenThrow(new DataAccessResourceFailureException("Test"));
		adapter.processDomesticStandingOrder(request, pStatusMap, params);
	}

	@Test
	public void testRetrieveStagedDomesticStandingOrder() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();

		DStandingorderFoundationResource bankResponse = DomesticStandingOrderMongoDbAdapterImplTestMockData
				.getResource();

		when(reqAttributes.getMethodType()).thenReturn("GET");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(false);
		when(repository.findOneByDataDomesticStandingOrderId(anyString())).thenReturn(bankResponse);

		adapter.retrieveStagedDomesticStandingOrder(stageIdentifiers, params);
	}

	@Test(expected = PSD2Exception.class)
	public void testRetrieveStagedDomesticStandingOrderException() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();

		DStandingorderFoundationResource bankResponse = DomesticStandingOrderMongoDbAdapterImplTestMockData
				.getResource();

		when(reqAttributes.getMethodType()).thenReturn("GET");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(true);
		when(repository.findOneByDataDomesticStandingOrderId(anyString())).thenReturn(bankResponse);

		adapter.retrieveStagedDomesticStandingOrder(stageIdentifiers, params);
	}

	@Test(expected = PSD2Exception.class)
	public void testRetrieveStagedDomesticStandingOrderDataAccessResourceFailureException() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();

		when(reqAttributes.getMethodType()).thenReturn("GET");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(true);
		when(repository.findOneByDataDomesticStandingOrderId(anyString()))
				.thenThrow(new DataAccessResourceFailureException("Test"));

		adapter.retrieveStagedDomesticStandingOrder(stageIdentifiers, params);
	}

	@After
	public void tearDown() {
		utility = null;
		reqAttributes = null;
		repository = null;
		adapter = null;
	}
}
