package com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.test;

import static org.mockito.Matchers.any;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.aisp.domain.OBExternalAccountSubType1Code;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.constants.AccountBeneficiariesFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.transformer.AccountBeneficiariesFoundationServiceTransformer;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Account;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Currency;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.CurrentAccount;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUser;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyBasicInformation;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.RetailNonRetailCode;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.SourceSystemAccountType;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.Address;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.Beneficiary;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.BeneficiaryAccount;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.Country;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.ExternalPaymentBeneficiaryIndicator;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.OperationalOrganisationUnit;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.PartiesPaymentBeneficiariesresponse;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.PaymentBeneficiaryLocaleTypeCode;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountBeneficiariesFoundationServiceTransformerTest {
	@InjectMocks
	private AccountBeneficiariesFoundationServiceTransformer accountBeneficiariesFoundationServiceTransformer;

	@Mock
	@Qualifier("PSD2ResponseValidator")
	private PSD2Validator validator;
	
	private Map<String, String> params = new HashMap<String, String>();
	private AccountMapping accountMapping = new AccountMapping();
	private List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
	private AccountDetails accDet = new AccountDetails();
	private HttpHeaders httpHeaders = new HttpHeaders();
	
	@Mock
	private CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;

	@Mock
	private AdapterFilterUtility adapterFilterUtility;

	@Before
	public void setUp() {
		ReflectionTestUtils.setField(accountBeneficiariesFoundationServiceTransformer,
				"filteredBeneficiariesCountryCode1",
				"AT,BE,BG,HR,CY,CZ,DK,EE,FI,FR,DE,GR,HU,IS,IE,IT,LV,LI,LT,LU,MT,MC,NL,NO,PK,PL,PT,RO,SK,SI,ES,SE,CH,TR,AE,GB");
		ReflectionTestUtils.setField(accountBeneficiariesFoundationServiceTransformer,
				"filteredBeneficiariesCountryCode2", "AU,CA,ZA");
		ReflectionTestUtils.setField(accountBeneficiariesFoundationServiceTransformer,
				"filteredBeneficiariesCountryCode3", "US");
		ReflectionTestUtils.setField(accountBeneficiariesFoundationServiceTransformer,
				"filteredBeneficiariesCountryCode4", "CN,HK,IN,NZ,PH,RU,SG,TH");
		ReflectionTestUtils.setField(accountBeneficiariesFoundationServiceTransformer,
				"filteredBeneficiariesCountryCode5", "MX");
		
		DigitalUser du = new DigitalUser();
		du.setDigitalUserLockedOutIndicator(false);
		DigitalUserProfile dup = new DigitalUserProfile();
		dup.setDigitalUser(du);
		PartyBasicInformation pi = new PartyBasicInformation();
		pi.setPartyActiveIndicator(true);
		dup.setPartyInformation(pi);
		List<AccountEntitlements> accountEntitlements = new ArrayList<AccountEntitlements>();
		AccountEntitlements ea = new AccountEntitlements();
		List<String> ent = new ArrayList<>();
		ent.add("Display Only Account");
		Account account = new Account();
		RetailNonRetailCode retail = RetailNonRetailCode.fromValue("Retail");
		SourceSystemAccountType sourceSystemAccountTypeCurrent = SourceSystemAccountType.fromValue("Current Account");
		account.setAccountNumber("24512786");
		Currency currency = new Currency();
		CurrentAccount cuacinfo = new CurrentAccount();
		currency.setIsoAlphaCode("GBP");
		account.setAccountCurrency(currency);
		account.setRetailNonRetailCode(retail);
		account.setSourceSystemAccountType(sourceSystemAccountTypeCurrent);
		cuacinfo.setParentNationalSortCodeNSCNumber("902127");
		cuacinfo.setInternationalBankAccountNumberIBAN("GB58BOFI90212724512786");
		cuacinfo.setSwiftBankIdentifierCodeBIC("BOFIGB2BXXX");
		account.setCurrentAccountInformation(cuacinfo);
		ea.setAccount(account);
		ea.setEntitlements(ent);
		accountEntitlements.add(ea);
		dup.setAccountEntitlements(accountEntitlements);

		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("BOI999");
		accountMapping.setCorrelationId("23456778");

		Map<String, String> params = new HashMap<>();
		params.put("channelId", "BOL");
		params.put("consentFlowType", "AISP");
		params.put("x-channel-id", "BOL");
		params.put("x-user-id", "BOI999");
		params.put("X-BOI-PLATFORM", "platform");
		params.put("x-fapi-interaction-id", "12345678");
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(), any())).thenReturn(dup);
		Mockito.when(adapterFilterUtility.prevalidateAccounts(dup, accountMapping)).thenReturn(accountMapping);
	}

	@Test
	public void testAccountBeneficiaryFS1() {
		PartiesPaymentBeneficiariesresponse partiesPaymentBeneficiariesresponse = new PartiesPaymentBeneficiariesresponse();
		List<Beneficiary> paymentBeneficiaryList = new ArrayList<Beneficiary>();
		Beneficiary paymentBeneficiary = new Beneficiary();
		BeneficiaryAccount beneficiaryAccount = new BeneficiaryAccount();
		OperationalOrganisationUnit operationalOrganisationUnit = new OperationalOrganisationUnit();
		Address addressReference = new Address();
		Country addressCountry = new Country();

		addressReference.setGeoCodeBuildingName("New Century House");
		addressReference.setFirstAddressLine("Mayor Street Lower");
		addressReference.setSecondAddressLine("International Financial Services Centre");
		addressReference.setThirdAddressLine("Dublin 1");

		addressCountry.setIsoCountryAlphaTwoCode("IE");
		addressCountry.setCountryName("Republic of Ireland");
		addressReference.setAddressCountry(addressCountry);

		operationalOrganisationUnit.setAddressReference(addressReference);

		beneficiaryAccount.setAccountName("ABC Corporation");
		beneficiaryAccount.setAccountNumber("24512786");
		beneficiaryAccount.setParentNationalSortCodeNSCNumber("902127");
		beneficiaryAccount.setInternationalBankAccountNumberIBAN("GB58BOFI90212724512786");
		beneficiaryAccount.setSwiftBankIdentifierCodeBIC("BOFIGB2BXXX");
		beneficiaryAccount.setABARTNRoutingTransitNumber("091000190");
		beneficiaryAccount.setOperationalOrganisationUnit(operationalOrganisationUnit);

		paymentBeneficiary.setPaymentBeneficiaryIdentifier("1234");
		paymentBeneficiary.setPaymentBeneficiaryLabel("ACME Co. Invoices");
		paymentBeneficiary.setPaymentBeneficiaryNarrativeText("October Invoices");
		paymentBeneficiary.setPaymentBeneficiaryBankName("Bank of Ireland");
		paymentBeneficiary.setPaymentBeneficiaryLocaleTypeCode(PaymentBeneficiaryLocaleTypeCode.SEPA);
		paymentBeneficiary.setExternalPaymentBeneficiaryIndicator(ExternalPaymentBeneficiaryIndicator.N);
		paymentBeneficiary.setBeneficiaryAccount(beneficiaryAccount);

		paymentBeneficiaryList.add(paymentBeneficiary);

		partiesPaymentBeneficiariesresponse.setPaymentBeneficiaryList(paymentBeneficiaryList);
		partiesPaymentBeneficiariesresponse.setTotal(1d);

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");

		Map<String, String> params = new HashMap<>();
		params.put(AccountBeneficiariesFoundationServiceConstants.ACCOUNT_ID, "123");

		accountBeneficiariesFoundationServiceTransformer
				.transformAccountBeneficiaries(partiesPaymentBeneficiariesresponse, params);

	}

	@Test
	public void testAccountBeneficiaryFS2() {
		PartiesPaymentBeneficiariesresponse partiesPaymentBeneficiariesresponse = new PartiesPaymentBeneficiariesresponse();
		List<Beneficiary> paymentBeneficiaryList = new ArrayList<Beneficiary>();
		Beneficiary paymentBeneficiary = new Beneficiary();
		BeneficiaryAccount beneficiaryAccount = new BeneficiaryAccount();
		OperationalOrganisationUnit operationalOrganisationUnit = new OperationalOrganisationUnit();
		Address addressReference = new Address();
		Country addressCountry = new Country();

		addressReference.setGeoCodeBuildingName("New Century House");
		addressReference.setFirstAddressLine("Mayor Street Lower");
		addressReference.setSecondAddressLine("International Financial Services Centre");
		addressReference.setThirdAddressLine("Dublin 1");

		addressCountry.setIsoCountryAlphaTwoCode("IN");
		addressCountry.setCountryName("India");
		addressReference.setAddressCountry(addressCountry);

		operationalOrganisationUnit.setAddressReference(addressReference);

		beneficiaryAccount.setAccountName("ABC Corporation");
		beneficiaryAccount.setAccountNumber("24512786");
		beneficiaryAccount.setParentNationalSortCodeNSCNumber("902127");
		beneficiaryAccount.setInternationalBankAccountNumberIBAN("GB58BOFI90212724512786");
		beneficiaryAccount.setSwiftBankIdentifierCodeBIC("BOFIGB2BXXX");
		beneficiaryAccount.setABARTNRoutingTransitNumber("091000190");
		beneficiaryAccount.setOperationalOrganisationUnit(operationalOrganisationUnit);

		paymentBeneficiary.setPaymentBeneficiaryIdentifier("1234");
		paymentBeneficiary.setPaymentBeneficiaryLabel("ACME Co. Invoices");
		paymentBeneficiary.setPaymentBeneficiaryNarrativeText("October Invoices");
		paymentBeneficiary.setPaymentBeneficiaryBankName("Bank of Ireland");
		paymentBeneficiary.setPaymentBeneficiaryLocaleTypeCode(PaymentBeneficiaryLocaleTypeCode.SEPA);
		paymentBeneficiary.setExternalPaymentBeneficiaryIndicator(ExternalPaymentBeneficiaryIndicator.N);
		paymentBeneficiary.setBeneficiaryAccount(beneficiaryAccount);

		paymentBeneficiaryList.add(paymentBeneficiary);

		partiesPaymentBeneficiariesresponse.setPaymentBeneficiaryList(paymentBeneficiaryList);
		partiesPaymentBeneficiariesresponse.setTotal(1d);

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");

		Map<String, String> params = new HashMap<>();
		params.put(AccountBeneficiariesFoundationServiceConstants.ACCOUNT_ID, "123");

		accountBeneficiariesFoundationServiceTransformer
				.transformAccountBeneficiaries(partiesPaymentBeneficiariesresponse, params);

	}

	@Test
	public void testAccountBeneficiaryFS3() {
		PartiesPaymentBeneficiariesresponse partiesPaymentBeneficiariesresponse = new PartiesPaymentBeneficiariesresponse();
		List<Beneficiary> paymentBeneficiaryList = new ArrayList<Beneficiary>();
		Beneficiary paymentBeneficiary = new Beneficiary();
		BeneficiaryAccount beneficiaryAccount = new BeneficiaryAccount();
		OperationalOrganisationUnit operationalOrganisationUnit = new OperationalOrganisationUnit();
		Address addressReference = new Address();
		Country addressCountry = new Country();

		addressReference.setGeoCodeBuildingName("New Century House");
		addressReference.setFirstAddressLine("Mayor Street Lower");
		addressReference.setSecondAddressLine("International Financial Services Centre");
		addressReference.setThirdAddressLine("Dublin 1");

		addressCountry.setIsoCountryAlphaTwoCode("ZA");
		addressCountry.setCountryName("South Africa");
		addressReference.setAddressCountry(addressCountry);

		operationalOrganisationUnit.setAddressReference(addressReference);

		beneficiaryAccount.setAccountName("ABC Corporation");
		beneficiaryAccount.setAccountNumber("24512786");
		beneficiaryAccount.setParentNationalSortCodeNSCNumber("902127");
		beneficiaryAccount.setInternationalBankAccountNumberIBAN("GB58BOFI90212724512786");
		beneficiaryAccount.setSwiftBankIdentifierCodeBIC("BOFIGB2BXXX");
		beneficiaryAccount.setABARTNRoutingTransitNumber("091000190");
		beneficiaryAccount.setOperationalOrganisationUnit(operationalOrganisationUnit);

		paymentBeneficiary.setPaymentBeneficiaryIdentifier("1234");
		paymentBeneficiary.setPaymentBeneficiaryLabel("ACME Co. Invoices");
		paymentBeneficiary.setPaymentBeneficiaryNarrativeText("October Invoices");
		paymentBeneficiary.setPaymentBeneficiaryBankName("Bank of Ireland");
		paymentBeneficiary.setPaymentBeneficiaryLocaleTypeCode(PaymentBeneficiaryLocaleTypeCode.SEPA);
		paymentBeneficiary.setExternalPaymentBeneficiaryIndicator(ExternalPaymentBeneficiaryIndicator.N);
		paymentBeneficiary.setBeneficiaryAccount(beneficiaryAccount);

		paymentBeneficiaryList.add(paymentBeneficiary);

		partiesPaymentBeneficiariesresponse.setPaymentBeneficiaryList(paymentBeneficiaryList);
		partiesPaymentBeneficiariesresponse.setTotal(1d);

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");

		Map<String, String> params = new HashMap<>();
		params.put(AccountBeneficiariesFoundationServiceConstants.ACCOUNT_ID, "123");

		accountBeneficiariesFoundationServiceTransformer
				.transformAccountBeneficiaries(partiesPaymentBeneficiariesresponse, params);

	}

	@Test
	public void testAccountBeneficiaryFS4() {
		PartiesPaymentBeneficiariesresponse partiesPaymentBeneficiariesresponse = new PartiesPaymentBeneficiariesresponse();
		List<Beneficiary> paymentBeneficiaryList = new ArrayList<Beneficiary>();
		Beneficiary paymentBeneficiary = new Beneficiary();
		BeneficiaryAccount beneficiaryAccount = new BeneficiaryAccount();
		OperationalOrganisationUnit operationalOrganisationUnit = new OperationalOrganisationUnit();
		Address addressReference = new Address();
		Country addressCountry = new Country();

		addressReference.setGeoCodeBuildingName("New Century House");
		addressReference.setFirstAddressLine("Mayor Street Lower");
		addressReference.setSecondAddressLine("International Financial Services Centre");
		addressReference.setThirdAddressLine("Dublin 1");

		addressCountry.setIsoCountryAlphaTwoCode("US");
		addressCountry.setCountryName("United States");
		addressReference.setAddressCountry(addressCountry);

		operationalOrganisationUnit.setAddressReference(addressReference);

		beneficiaryAccount.setAccountName("ABC Corporation");
		beneficiaryAccount.setAccountNumber("24512786");
		beneficiaryAccount.setParentNationalSortCodeNSCNumber("902127");
		beneficiaryAccount.setInternationalBankAccountNumberIBAN("GB58BOFI90212724512786");
		beneficiaryAccount.setSwiftBankIdentifierCodeBIC("BOFIGB2BXXX");
		beneficiaryAccount.setABARTNRoutingTransitNumber("091000190");
		beneficiaryAccount.setOperationalOrganisationUnit(operationalOrganisationUnit);

		paymentBeneficiary.setPaymentBeneficiaryIdentifier("1234");
		paymentBeneficiary.setPaymentBeneficiaryLabel("ACME Co. Invoices");
		paymentBeneficiary.setPaymentBeneficiaryNarrativeText("October Invoices");
		paymentBeneficiary.setPaymentBeneficiaryBankName("Bank of Ireland");
		paymentBeneficiary.setPaymentBeneficiaryLocaleTypeCode(PaymentBeneficiaryLocaleTypeCode.SEPA);
		paymentBeneficiary.setExternalPaymentBeneficiaryIndicator(ExternalPaymentBeneficiaryIndicator.N);
		paymentBeneficiary.setBeneficiaryAccount(beneficiaryAccount);

		paymentBeneficiaryList.add(paymentBeneficiary);

		partiesPaymentBeneficiariesresponse.setPaymentBeneficiaryList(paymentBeneficiaryList);
		partiesPaymentBeneficiariesresponse.setTotal(1d);

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");

		Map<String, String> params = new HashMap<>();
		params.put(AccountBeneficiariesFoundationServiceConstants.ACCOUNT_ID, "123");

		accountBeneficiariesFoundationServiceTransformer
				.transformAccountBeneficiaries(partiesPaymentBeneficiariesresponse, params);

	}

	@Test
	public void testAccountBeneficiaryFS5() {
		PartiesPaymentBeneficiariesresponse partiesPaymentBeneficiariesresponse = new PartiesPaymentBeneficiariesresponse();
		List<Beneficiary> paymentBeneficiaryList = new ArrayList<Beneficiary>();
		Beneficiary paymentBeneficiary = new Beneficiary();
		BeneficiaryAccount beneficiaryAccount = new BeneficiaryAccount();
		OperationalOrganisationUnit operationalOrganisationUnit = new OperationalOrganisationUnit();
		Address addressReference = new Address();
		Country addressCountry = new Country();

		addressReference.setGeoCodeBuildingName("New Century House");
		addressReference.setFirstAddressLine("Mayor Street Lower");
		addressReference.setSecondAddressLine("International Financial Services Centre");
		addressReference.setThirdAddressLine("Dublin 1");

		addressCountry.setIsoCountryAlphaTwoCode("MX");
		addressCountry.setCountryName("Mexico");
		addressReference.setAddressCountry(addressCountry);

		operationalOrganisationUnit.setAddressReference(addressReference);

		beneficiaryAccount.setAccountName("ABC Corporation");
		beneficiaryAccount.setAccountNumber("24512786");
		beneficiaryAccount.setParentNationalSortCodeNSCNumber("902127");
		beneficiaryAccount.setInternationalBankAccountNumberIBAN("GB58BOFI90212724512786");
		beneficiaryAccount.setSwiftBankIdentifierCodeBIC("BOFIGB2BXXX");
		beneficiaryAccount.setABARTNRoutingTransitNumber("091000190");
		beneficiaryAccount.setOperationalOrganisationUnit(operationalOrganisationUnit);

		paymentBeneficiary.setPaymentBeneficiaryIdentifier("1234");
		paymentBeneficiary.setPaymentBeneficiaryLabel("ACME Co. Invoices");
		paymentBeneficiary.setPaymentBeneficiaryNarrativeText("October Invoices");
		paymentBeneficiary.setPaymentBeneficiaryBankName("Bank of Ireland");
		paymentBeneficiary.setPaymentBeneficiaryLocaleTypeCode(PaymentBeneficiaryLocaleTypeCode.SEPA);
		paymentBeneficiary.setExternalPaymentBeneficiaryIndicator(ExternalPaymentBeneficiaryIndicator.N);
		paymentBeneficiary.setBeneficiaryAccount(beneficiaryAccount);

		paymentBeneficiaryList.add(paymentBeneficiary);

		partiesPaymentBeneficiariesresponse.setPaymentBeneficiaryList(paymentBeneficiaryList);
		partiesPaymentBeneficiariesresponse.setTotal(1d);

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");

		Map<String, String> params = new HashMap<>();
		params.put(AccountBeneficiariesFoundationServiceConstants.ACCOUNT_ID, "123");

		accountBeneficiariesFoundationServiceTransformer
				.transformAccountBeneficiaries(partiesPaymentBeneficiariesresponse, params);

	}
	
	@Test
	public void testAccountBeneficiaryFS6() {
		PartiesPaymentBeneficiariesresponse partiesPaymentBeneficiariesresponse = new PartiesPaymentBeneficiariesresponse();
		List<Beneficiary> paymentBeneficiaryList = new ArrayList<Beneficiary>();
		Beneficiary paymentBeneficiary = new Beneficiary();
		BeneficiaryAccount beneficiaryAccount = new BeneficiaryAccount();
		OperationalOrganisationUnit operationalOrganisationUnit = new OperationalOrganisationUnit();
		Address addressReference = new Address();
		Country addressCountry = new Country();

		addressReference.setGeoCodeBuildingName("New Century House");
		addressReference.setFirstAddressLine("Mayor Street Lower");
		addressReference.setSecondAddressLine("International Financial Services Centre");
		addressReference.setThirdAddressLine("Dublin 1");

		addressCountry.setIsoCountryAlphaTwoCode("");
		addressCountry.setCountryName("");
		addressReference.setAddressCountry(addressCountry);

		operationalOrganisationUnit.setAddressReference(addressReference);

		beneficiaryAccount.setAccountName("ABC Corporation");
		beneficiaryAccount.setAccountNumber("24512786");
		beneficiaryAccount.setParentNationalSortCodeNSCNumber("902127");
		beneficiaryAccount.setInternationalBankAccountNumberIBAN("GB58BOFI90212724512786");
		beneficiaryAccount.setSwiftBankIdentifierCodeBIC("BOFIGB2BXXX");
		beneficiaryAccount.setABARTNRoutingTransitNumber("091000190");
		beneficiaryAccount.setOperationalOrganisationUnit(operationalOrganisationUnit);

		paymentBeneficiary.setPaymentBeneficiaryIdentifier("1234");
		paymentBeneficiary.setPaymentBeneficiaryLabel("ACME Co. Invoices");
		paymentBeneficiary.setPaymentBeneficiaryNarrativeText("October Invoices");
		paymentBeneficiary.setPaymentBeneficiaryBankName("Bank of Ireland");
		paymentBeneficiary.setPaymentBeneficiaryLocaleTypeCode(PaymentBeneficiaryLocaleTypeCode.SEPA);
		paymentBeneficiary.setExternalPaymentBeneficiaryIndicator(ExternalPaymentBeneficiaryIndicator.N);
		paymentBeneficiary.setBeneficiaryAccount(beneficiaryAccount);

		paymentBeneficiaryList.add(paymentBeneficiary);

		partiesPaymentBeneficiariesresponse.setPaymentBeneficiaryList(paymentBeneficiaryList);
		partiesPaymentBeneficiariesresponse.setTotal(1d);

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");

		Map<String, String> params = new HashMap<>();
		params.put(AccountBeneficiariesFoundationServiceConstants.ACCOUNT_ID, "123");

		accountBeneficiariesFoundationServiceTransformer
				.transformAccountBeneficiaries(partiesPaymentBeneficiariesresponse, params);

	}

	@Test
	public void testAccountBeneficiaryFS7() {
		PartiesPaymentBeneficiariesresponse partiesPaymentBeneficiariesresponse = new PartiesPaymentBeneficiariesresponse();
		List<Beneficiary> paymentBeneficiaryList = new ArrayList<Beneficiary>();
		Beneficiary paymentBeneficiary = new Beneficiary();
		BeneficiaryAccount beneficiaryAccount = new BeneficiaryAccount();
		OperationalOrganisationUnit operationalOrganisationUnit = new OperationalOrganisationUnit();
		Address addressReference = new Address();
		Country addressCountry = new Country();

		addressReference.setGeoCodeBuildingName("New Century House");
		addressReference.setFirstAddressLine("Mayor Street Lower");
		addressReference.setSecondAddressLine("International Financial Services Centre");
		addressReference.setThirdAddressLine("Dublin 1");

		addressCountry.setIsoCountryAlphaTwoCode("");
		addressCountry.setCountryName("");
		addressReference.setAddressCountry(addressCountry);

		operationalOrganisationUnit.setAddressReference(addressReference);

		beneficiaryAccount.setAccountName("ABC Corporation");
		beneficiaryAccount.setAccountNumber("24512786");
		beneficiaryAccount.setParentNationalSortCodeNSCNumber("902127");
		beneficiaryAccount.setOperationalOrganisationUnit(operationalOrganisationUnit);

		paymentBeneficiary.setPaymentBeneficiaryIdentifier("1234");
		paymentBeneficiary.setPaymentBeneficiaryLabel("ACME Co. Invoices");
		paymentBeneficiary.setPaymentBeneficiaryNarrativeText("October Invoices");
		paymentBeneficiary.setPaymentBeneficiaryBankName("Bank of Ireland");
		paymentBeneficiary.setPaymentBeneficiaryLocaleTypeCode(PaymentBeneficiaryLocaleTypeCode.SEPA);
		paymentBeneficiary.setExternalPaymentBeneficiaryIndicator(ExternalPaymentBeneficiaryIndicator.N);
		paymentBeneficiary.setBeneficiaryAccount(beneficiaryAccount);

		paymentBeneficiaryList.add(paymentBeneficiary);

		partiesPaymentBeneficiariesresponse.setPaymentBeneficiaryList(paymentBeneficiaryList);
		partiesPaymentBeneficiariesresponse.setTotal(1d);

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");

		Map<String, String> params = new HashMap<>();
		params.put(AccountBeneficiariesFoundationServiceConstants.ACCOUNT_ID, "123");

		accountBeneficiariesFoundationServiceTransformer
				.transformAccountBeneficiaries(partiesPaymentBeneficiariesresponse, params);

	}


}