package com.capgemini.psd2.pisp.payment.setup.service;

import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;

public interface InternationalPaymentConsentsService {

	public CustomIPaymentConsentsPOSTResponse createInternationalPaymentConsentsResource(
			CustomIPaymentConsentsPOSTRequest paymentRequest);

	public CustomIPaymentConsentsPOSTResponse retrieveInternationalPaymentConsentsResource(
			PaymentRetrieveGetRequest paymentRetrieveRequest);

}
