package com.capgemini.psd2.account.standingorder.mongo.db.adapter.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountStandingOrderCMA2;

public interface AccountStandingOrderRepository extends MongoRepository<AccountStandingOrderCMA2, String> {
	
	
	/**
	 * Find by account id.
	 *
	 * @param accountId the account id
	 * @return the balances GET response data
	 */
	public List<AccountStandingOrderCMA2> findByAccountNumberAndAccountNSC (String accountNumber, String accountNSC);

}
