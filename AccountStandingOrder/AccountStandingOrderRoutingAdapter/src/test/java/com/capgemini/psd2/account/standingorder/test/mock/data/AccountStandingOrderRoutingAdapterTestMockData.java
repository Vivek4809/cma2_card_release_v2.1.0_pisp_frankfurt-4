package com.capgemini.psd2.account.standingorder.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBReadStandingOrder3;
import com.capgemini.psd2.aisp.domain.OBReadStandingOrder3Data;
import com.capgemini.psd2.aisp.domain.OBStandingOrder3;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStandingOrdersResponse;

public class AccountStandingOrderRoutingAdapterTestMockData {
	
	public static PlatformAccountStandingOrdersResponse getMockStandingOrdersGETResponse() {
		
		PlatformAccountStandingOrdersResponse platformAccountStandingOrdersResponse = new PlatformAccountStandingOrdersResponse();
		OBReadStandingOrder3Data obReadStandingOrder2Data = new OBReadStandingOrder3Data();
		List<OBStandingOrder3> obStandingOrderList = new ArrayList<>();
		obReadStandingOrder2Data.setStandingOrder(obStandingOrderList);
		OBReadStandingOrder3 obReadStandingOrder2 = new OBReadStandingOrder3();
		obReadStandingOrder2.setData(obReadStandingOrder2Data);
		platformAccountStandingOrdersResponse.setoBReadStandingOrder3(obReadStandingOrder2);
		return platformAccountStandingOrdersResponse;
	}

}
