package com.capgemini.psd2.pisp.isp.test.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.isp.controller.InternationalScheduledPaymentsApiController;
import com.capgemini.psd2.pisp.isp.service.ISPaymentsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.ser.DefaultSerializerProvider;
import com.fasterxml.jackson.databind.ser.SerializerFactory;

@RunWith(SpringJUnit4ClassRunner.class)
public class InternationalScheduledPaymentsApiControllerTest {

	@Mock
	private ISPaymentsService service;

	@Mock
	ObjectMapper objectMapper;

	@InjectMocks
	private InternationalScheduledPaymentsApiController controller;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testObjectMapper() {
		controller.getObjectMapper();
	}

	@Test
	public void testRequest() {
		controller.getRequest();
	}

	@Test
	public void testCreateInternationalScheduledPayments() {
		CustomISPaymentsPOSTRequest request = new CustomISPaymentsPOSTRequest();
		CustomISPaymentsPOSTResponse response = new CustomISPaymentsPOSTResponse();

		@SuppressWarnings("serial")
		DefaultSerializerProvider p = new DefaultSerializerProvider() {

			@Override
			public DefaultSerializerProvider createInstance(SerializationConfig config, SerializerFactory jsf) {
				return null;
			}
		};
		objectMapper.setSerializerProvider(p);

		String xFapiFinancialId = "1";
		String authorization = "2";
		String xIdempotencyKey = "3";
		String xJwsSignature = "4";

		Mockito.when(service.createInternationalScheduledPaymentsResource(request)).thenReturn(response);
		controller.createInternationalScheduledPayments(request, xFapiFinancialId, authorization, xIdempotencyKey,
				xJwsSignature, null, null, null, null);

	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testCreateInternationalScheduledPaymentsException() {
		CustomISPaymentsPOSTRequest request = new CustomISPaymentsPOSTRequest();

		String xFapiFinancialId = "1";
		String authorization = "2";
		String xIdempotencyKey = "3";
		String xJwsSignature = "4";

		Mockito.when(service.createInternationalScheduledPaymentsResource(request)).thenThrow(PSD2Exception.class);
		controller.createInternationalScheduledPayments(request, xFapiFinancialId, authorization, xIdempotencyKey,
				xJwsSignature, null, null, null, null);
	}

	@Test
	public void testgetInternationalScheduledPaymentsInternationalScheduledPaymentId() {
		CustomISPaymentsPOSTResponse response = new CustomISPaymentsPOSTResponse();

		@SuppressWarnings("serial")
		DefaultSerializerProvider p = new DefaultSerializerProvider() {

			@Override
			public DefaultSerializerProvider createInstance(SerializationConfig config, SerializerFactory jsf) {
				return null;
			}
		};
		objectMapper.setSerializerProvider(p);

		String xFapiFinancialId = "1";
		String authorization = "2";
		String internationalScheduledPaymentId = "123456";
		Mockito.when(service.retrieveInternationalScheduledPaymentsResource(internationalScheduledPaymentId))
				.thenReturn(response);
		controller.getInternationalScheduledPaymentsInternationalScheduledPaymentId(internationalScheduledPaymentId,
				xFapiFinancialId, authorization, null, null, null, null);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testgetInternationalScheduledPaymentsInternationalScheduledPaymentIdException() {
		String xFapiFinancialId = "1";
		String authorization = "2";
		String internationalScheduledPaymentId = "123456";
		Mockito.when(service.retrieveInternationalScheduledPaymentsResource(internationalScheduledPaymentId))
				.thenThrow(PSD2Exception.class);
		controller.getInternationalScheduledPaymentsInternationalScheduledPaymentId(internationalScheduledPaymentId,
				xFapiFinancialId, authorization, null, null, null, null);
	}

	@After
	public void tearDown() {
		service = null;
		controller = null;
	}
}
