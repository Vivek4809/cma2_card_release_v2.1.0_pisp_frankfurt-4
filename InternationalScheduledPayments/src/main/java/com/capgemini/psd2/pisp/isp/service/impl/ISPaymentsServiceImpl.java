package com.capgemini.psd2.pisp.isp.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.HttpMethod;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.adapter.InternationalScheduledPaymentsAdapter;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPlatformDetails;
import com.capgemini.psd2.pisp.domain.GenericPaymentsResponse;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.isp.comparator.ISPaymentsPayloadComparator;
import com.capgemini.psd2.pisp.isp.service.ISPaymentsService;
import com.capgemini.psd2.pisp.processing.adapter.service.PaymentSubmissionProcessingAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalScheduledPaymentStagingAdapter;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Service
public class ISPaymentsServiceImpl implements ISPaymentsService {

	@Autowired
	private PaymentSubmissionProcessingAdapter<CustomISPaymentsPOSTRequest, CustomISPaymentsPOSTResponse> paymentsProcessingAdapter;

	@Autowired
	@Qualifier("isPaymentsRoutingAdapter")
	private InternationalScheduledPaymentsAdapter isPaymentsAdapter;

	@Autowired
	@Qualifier("isPaymentConsentsStagingRoutingAdapter")
	private InternationalScheduledPaymentStagingAdapter isPaymentConsentsAdapter;

	@Autowired
	private ISPaymentsPayloadComparator isPaymentsComparator;
	
	@Value("${cmaVersion}")
	private String cmaVersion;

	@Override
	public CustomISPaymentsPOSTResponse createInternationalScheduledPaymentsResource(
			CustomISPaymentsPOSTRequest customRequest) {

		CustomISPaymentsPOSTResponse paymentsFoundationResponse = null;
		CustomISPConsentsPOSTResponse consentsFoundationResource = null;
		PaymentConsentsPlatformResource paymentConsentPlatformResource = null;
		PaymentsPlatformResource paymentsPlatformResource = null;

		Map<String, Object> paymentsPlatformResourceMap = paymentsProcessingAdapter.preSubmissionProcessing(
				customRequest, populatePlatformDetails(), customRequest.getData().getConsentId().trim());

		paymentConsentPlatformResource = (PaymentConsentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.CONSENT);
		paymentsPlatformResource = (PaymentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.SUBMISSION);

		consentsFoundationResource = scheduledPayLoadCompare(customRequest, paymentConsentPlatformResource,
				customRequest.getData().getConsentId().trim());

		if (paymentsPlatformResource == null) {
			paymentsPlatformResource = paymentsProcessingAdapter.createInitialPaymentsPlatformResource(
					customRequest.getData().getConsentId(), cmaVersion,
					PaymentTypeEnum.INTERNATIONAL_SCH_PAY);
			paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);

		}

		if (paymentsPlatformResource.getProccessState().equals(PaymentConstants.INCOMPLETE)) {
			customRequest = populateScheduledStagedProcessingFields(customRequest, consentsFoundationResource);
			Map<String, OBExternalStatus1Code> paymentStatusMap = new HashMap<>();
			paymentStatusMap.put(PaymentConstants.INITIAL, OBExternalStatus1Code.INITIATIONPENDING);
			paymentStatusMap.put(PaymentConstants.FAIL, OBExternalStatus1Code.INITIATIONFAILED);
			paymentStatusMap.put(PaymentConstants.PASS_M_AUTH, OBExternalStatus1Code.INITIATIONCOMPLETED);
			paymentStatusMap.put(PaymentConstants.PASS_M_AWAIT, OBExternalStatus1Code.INITIATIONPENDING);
			paymentStatusMap.put(PaymentConstants.PASS_M_REJECT, OBExternalStatus1Code.INITIATIONFAILED);
			customRequest.setCreatedOn(paymentsPlatformResource.getCreatedAt());
			GenericPaymentsResponse paymentsGenericResponse = isPaymentsAdapter.processISPayments(customRequest,
					paymentStatusMap, null);
			paymentsFoundationResponse = transformIntoTppResponse(paymentsGenericResponse, customRequest);
		}

		if (paymentsPlatformResource.getProccessState().equals(PaymentConstants.COMPLETED)) {
			CustomPaymentStageIdentifiers identifiers = new CustomPaymentStageIdentifiers();
			identifiers.setPaymentSetupVersion(cmaVersion);
			identifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_SCH_PAY);
			identifiers.setPaymentConsentId(paymentsPlatformResource.getPaymentConsentId());
			identifiers.setPaymentSubmissionId(paymentsPlatformResource.getSubmissionId());
			paymentsFoundationResponse = isPaymentsAdapter.retrieveStagedISPaymentsResponse(identifiers, null);

		}

		if (paymentsFoundationResponse == null) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.PISP_NO_PAYMENT_SETUP_RESOURCES_FOUND));
		}

		OBExternalStatus2Code multiAuthStatus = OBExternalStatus2Code.AUTHORISED;
		if (paymentsFoundationResponse.getData().getMultiAuthorisation() != null)
			multiAuthStatus = paymentsFoundationResponse.getData().getMultiAuthorisation().getStatus();

		return paymentsProcessingAdapter.postPaymentProcessFlows(paymentsPlatformResourceMap,
				paymentsFoundationResponse, paymentsFoundationResponse.getProcessExecutionStatus(),
				paymentsFoundationResponse.getData().getInternationalScheduledPaymentId(), multiAuthStatus,
				HttpMethod.POST.toString());
	}

	private CustomISPaymentsPOSTResponse transformIntoTppResponse(GenericPaymentsResponse paymentsGenericResponse,
			CustomISPaymentsPOSTRequest customRequest) {
		String paymentFoundationRequest = JSONUtilities.getJSONOutPutFromObject(customRequest);
		CustomISPaymentsPOSTResponse transformedResponse = JSONUtilities.getObjectFromJSONString(
				PispUtilities.getObjectMapper(), paymentFoundationRequest, CustomISPaymentsPOSTResponse.class);
		transformedResponse.getData().setCharges(paymentsGenericResponse.getCharges());
		transformedResponse.getData().setInternationalScheduledPaymentId(paymentsGenericResponse.getSubmissionId());
		transformedResponse.getData()
				.setExpectedExecutionDateTime(paymentsGenericResponse.getExpectedExecutionDateTime());
		transformedResponse.getData()
				.setExpectedSettlementDateTime(paymentsGenericResponse.getExpectedSettlementDateTime());
		transformedResponse.getData().setMultiAuthorisation(paymentsGenericResponse.getMultiAuthorisation());
		transformedResponse.setProcessExecutionStatus(paymentsGenericResponse.getProcessExecutionStatusEnum());
		transformedResponse.getData().setExchangeRateInformation(paymentsGenericResponse.getExchangeRateInformation());
		return transformedResponse;
	}

	@Override
	public CustomISPaymentsPOSTResponse retrieveInternationalScheduledPaymentsResource(
			String internationalScheduledPaymentId) {

		Map<String, Object> paymentsPlatformResourceMap = paymentsProcessingAdapter
				.prePaymentProcessGETFlows(internationalScheduledPaymentId, PaymentTypeEnum.INTERNATIONAL_SCH_PAY);

		PaymentsPlatformResource paymentsPlatformResource = (PaymentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.SUBMISSION);

		CustomPaymentStageIdentifiers identifiers = new CustomPaymentStageIdentifiers();
		identifiers.setPaymentSetupVersion(cmaVersion);
		identifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_SCH_PAY);
		identifiers.setPaymentConsentId(paymentsPlatformResource.getPaymentConsentId());
		identifiers.setPaymentSubmissionId(internationalScheduledPaymentId);

		CustomISPaymentsPOSTResponse paymentsFoundationResponse = isPaymentsAdapter
				.retrieveStagedISPaymentsResponse(identifiers, null);

		if (paymentsFoundationResponse == null) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.PISP_NO_PAYMENT_SETUP_RESOURCES_FOUND));
		}

		OBExternalStatus2Code multiAuthStatus = OBExternalStatus2Code.AUTHORISED;
		if (paymentsFoundationResponse.getData().getMultiAuthorisation() != null)
			multiAuthStatus = paymentsFoundationResponse.getData().getMultiAuthorisation().getStatus();

		return paymentsProcessingAdapter.postPaymentProcessFlows(paymentsPlatformResourceMap,
				paymentsFoundationResponse, paymentsFoundationResponse.getProcessExecutionStatus(),
				paymentsFoundationResponse.getData().getInternationalScheduledPaymentId(), multiAuthStatus,
				HttpMethod.GET.toString());
	}

	private CustomPaymentSetupPlatformDetails populatePlatformDetails() {
		CustomPaymentSetupPlatformDetails customPaymentSetupPlatformDetails = new CustomPaymentSetupPlatformDetails();
		customPaymentSetupPlatformDetails.setSetupCmaVersion(cmaVersion);
		customPaymentSetupPlatformDetails.setPaymentType(PaymentTypeEnum.INTERNATIONAL_SCH_PAY);
		return customPaymentSetupPlatformDetails;
	}

	private CustomISPConsentsPOSTResponse scheduledPayLoadCompare(CustomISPaymentsPOSTRequest customRequest,
			PaymentConsentsPlatformResource paymentConsentPlatformResource, String consentId) {

		CustomPaymentStageIdentifiers identifiers = new CustomPaymentStageIdentifiers();
		identifiers.setPaymentConsentId(consentId);
		identifiers.setPaymentSetupVersion(cmaVersion);
		identifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_SCH_PAY);

		CustomISPConsentsPOSTResponse paymentSetupFoundationResponse = isPaymentConsentsAdapter
				.retrieveStagedInternationalScheduledPaymentConsents(identifiers, null);

		if (isPaymentsComparator.comparePaymentDetails(paymentSetupFoundationResponse, customRequest,
				paymentConsentPlatformResource) > 0)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_CONSENTMISMATCH,
					ErrorMapKeys.RESOURCE_CONSENT_MISMATCH));
		return paymentSetupFoundationResponse;
	}

	private CustomISPaymentsPOSTRequest populateScheduledStagedProcessingFields(
			CustomISPaymentsPOSTRequest customRequest, CustomISPConsentsPOSTResponse consentsFoundationResource) {
		/*
		 * If TPP has not sent DebtorAccount.Name then product will set the name
		 * details from staging record which is sent by FS
		 */
		if (customRequest.getData().getInitiation().getDebtorAccount() != null
				&& NullCheckUtils.isNullOrEmpty(customRequest.getData().getInitiation().getDebtorAccount().getName())) {

			customRequest.getData().getInitiation().getDebtorAccount()
					.setName(consentsFoundationResource.getData().getInitiation().getDebtorAccount().getName());
		}
		/*
		 * If debtor details are not provided by TPP then Platform will set
		 * debtor details received from FS. Same will be sent to FS for
		 * Pre-SubmissionValidation and Account Permission
		 */

		if (customRequest.getData().getInitiation().getDebtorAccount() == null) {
			customRequest.getData().getInitiation()
					.setDebtorAccount(consentsFoundationResource.getData().getInitiation().getDebtorAccount());
		}
		return customRequest;
	}

}