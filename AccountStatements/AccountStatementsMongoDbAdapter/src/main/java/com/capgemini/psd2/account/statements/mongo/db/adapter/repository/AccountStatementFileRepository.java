package com.capgemini.psd2.account.statements.mongo.db.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountStatementFileCMA2;

public interface AccountStatementFileRepository extends MongoRepository<AccountStatementFileCMA2, String>{
	
	public AccountStatementFileCMA2 findByStatementId(String statementId);
	
}
