package com.capgemini.psd2.test.utilities.test;

import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.account.statements.utilities.AccountStatementTransactionValidation;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.test.mock.data.AccountStatementsApiTestData;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountStatementTransactionValidationTest {
	
	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	@InjectMocks
	AccountStatementTransactionValidation accountStatementTransactionValidation;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void validateStatementTransactionClaims() {
		Mockito.when(reqHeaderAtrributes.getClaims())
		.thenReturn(AccountStatementsApiTestData.getMockTokenWithALLFilterTransaction());
		ArrayList<String> transactionMandatoryClaims=new ArrayList<String>();
		transactionMandatoryClaims.add("READTRANSACTIONSBASIC");
		transactionMandatoryClaims.add("READTRANSACTIONSDETAIL");
		ReflectionTestUtils.setField(accountStatementTransactionValidation, "transactionMandatoryClaims", transactionMandatoryClaims);
		accountStatementTransactionValidation.validateStatementTransactionClaims(reqHeaderAtrributes);
		
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateStatementTransactionClaimsNull() {
		Mockito.when(reqHeaderAtrributes.getClaims())
		.thenReturn(AccountStatementsApiTestData.getMockTokenWithALLFilterTransaction());
		ArrayList<String> transactionMandatoryClaims=new ArrayList<String>();
		transactionMandatoryClaims.add("ReadTransactionsCredits");
		ReflectionTestUtils.setField(accountStatementTransactionValidation, "transactionMandatoryClaims", transactionMandatoryClaims);
		accountStatementTransactionValidation.validateStatementTransactionClaims(reqHeaderAtrributes);
		
	}


}
