package com.capgemini.psd2.test.service.impl;

import static org.hamcrest.CoreMatchers.any;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.account.statements.service.impl.AccountStatementsServiceImpl;
import com.capgemini.psd2.account.statements.utilities.AccountStatementDateUtilities;
import com.capgemini.psd2.account.statements.utilities.AccountStatementTransactionValidation;
import com.capgemini.psd2.account.statements.utilities.AccountStatementValidationUtilities;
import com.capgemini.psd2.aisp.account.mapping.adapter.AccountMappingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountStatementsAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.OBReadStatement1;
import com.capgemini.psd2.aisp.domain.OBReadTransaction3;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSatementsFileResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStatementsResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;
import com.capgemini.psd2.aisp.validation.adapter.impl.AccountStatementsValidatorImpl;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.response.validator.ResponseValidator;
import com.capgemini.psd2.test.mock.data.AccountStatementsApiTestData;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.netflix.discovery.shared.Pair;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountStatementsServiceImplTest {

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	// ** The account standing order adapter. *//*
	@Mock
	private AccountStatementsAdapter accountStatementsAdapter;

	// ** The account mapping adapter. *//*
	@Mock
	private AccountMappingAdapter accountMappingAdapter;

	@Mock
	private ResponseValidator responseValidator;

	// ** The aisp consent adapter. *//*
	@Mock
	private AispConsentAdapter aispConsentAdapter;

	@Mock
	AccountStatementTransactionValidation accountStatementTransactionValidation;

	@Mock
	AccountStatementValidationUtilities accountStatementvalUtilities;

	/*
	 * @Mock private String fileFilter;
	 */

	@Mock
	AccountStatementDateUtilities apiDateUtil;

	@Mock
	private AccountStatementsValidatorImpl accountsStatementsValidatorImpl;

	@InjectMocks
	private AccountStatementsServiceImpl service = new AccountStatementsServiceImpl();

	@Mock
	private LoggerUtils loggerUtils;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Mockito.when(reqHeaderAtrributes.getToken())
				.thenReturn(AccountStatementsApiTestData.getMockTokenWithALLFilter());
		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountStatementsApiTestData.getAispConsent());
		Mockito.when(aispConsentAdapter.retrieveConsent(anyString()))
				.thenReturn(AccountStatementsApiTestData.getAispConsent());
		ReflectionTestUtils.setField(service, "debitFilter", "ReadTransactionsDebits");
		ReflectionTestUtils.setField(service, "creditFilter", "ReadTransactionsCredits");
		Mockito.when(reqHeaderAtrributes.getSelfUrl())
				.thenReturn("/accounts/269c3ff5-d7f8-419b-a3b9-7136c5b4611a/statements/");
		Mockito.when(reqHeaderAtrributes.getSelfUrl())
				.thenReturn("/accounts/269c3ff5-d7f8-419b-a3b9-7136c5b4611a/statements/7136c5b4611a");
		Mockito.when(reqHeaderAtrributes.getSelfUrl())
				.thenReturn("/accounts/269c3ff5-d7f8-419b-a3b9-7136c5b4611a/statements/7136c5b4611a/transactions");
		Mockito.when(reqHeaderAtrributes.getSelfUrl())
				.thenReturn("/accounts/269c3ff5-d7f8-419b-a3b9-7136c5b4611a/statements/7136c5b4611a/file");
		Map<String, String> map = new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap = new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content",
				"Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}

	// statements
	@Test
	public void retrieveStatementsTestWithNullParams() {
		// doNothing().when(accountsStatementsValidatorImpl).validateResponseParams(anyObject());
		accountsStatementsValidatorImpl.validateResponseParams(anyObject());
		when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject()))
				.thenReturn(AccountStatementsApiTestData.getMockAccountMapping());
		LocalDateTime local = LocalDateTime.now();
		when(apiDateUtil.convertDateStringToLocalDateTime(anyString())).thenReturn(local);
		when(apiDateUtil.convertDateStringToLocalDateTime(anyString())).thenReturn(local);
		Pair<String, String> pair = new Pair<String, String>(null, null);
		Mockito.when(apiDateUtil.populateSetupDates(anyObject())).thenReturn(pair);

		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountStatementsApiTestData.getAispConsent());
		Mockito.when(
				accountStatementsAdapter.retrieveAccountStatements(anyObject(), anyMapOf(String.class, String.class)))
				.thenReturn(AccountStatementsApiTestData.getAccountStatementsGETResponseForService());
		OBReadStatement1 response = service.retrieveAccountStatements("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", null,
				null, null, null, null);
		String actualResponseJson = JSONUtilities.getJSONOutPutFromObject(response);
		String expectedResponseJson = "{\"Data\":{\"Statement\":[{\"AccountId\":\"269c3ff5-d7f8-419b-a3b9-7136c5b4611a\",\"StatementId\":\"269c3ff5-d7f8-419b-a3b9-7136\",\"StatementReference\":\"gdqwe67384\",\"Type\":\"RegularPeriodic\",\"StartDateTime\":\"2016-10-01T00:00:00+00:00\",\"EndDateTime\":\"2018-09-01T00:00:00+00:00\",\"CreationDateTime\":\"2017-09-01T00:00:00+00:00\",\"StatementDescription\":[],\"StatementBenefit\":null,\"StatementFee\":null,\"StatementInterest\":null,\"StatementDateTime\":null,\"StatementRate\":null,\"StatementValue\":null,\"StatementAmount\":null}]},\"Links\":{\"Self\":\"/accounts/269c3ff5-d7f8-419b-a3b9-7136c5b4611a/statements/7136c5b4611a/file\",\"First\":null,\"Prev\":\"/accounts/269c3ff5-d7f8-419b-a3b9-7136c5b4611a/statements/7136c5b4611a/file?page=3\",\"Next\":\"/accounts/269c3ff5-d7f8-419b-a3b9-7136c5b4611a/statements/7136c5b4611a/file?page=5\",\"Last\":\"/accounts/269c3ff5-d7f8-419b-a3b9-7136c5b4611a/statements/7136c5b4611a/file?page=10\"},\"Meta\":{\"TotalPages\":null,\"FirstAvailableDateTime\":null,\"LastAvailableDateTime\":null}}";
		JSONAssert.assertEquals(expectedResponseJson, actualResponseJson, Boolean.FALSE);
	}

	@Test(expected = PSD2Exception.class)
	public void TestBothConsentAndParamDatesPresentParamsOutOfRange() {
		Mockito.when(reqHeaderAtrributes.getToken())
				.thenReturn(AccountStatementsApiTestData.getMockTokenWithDebitFilter());
		Mockito.when(apiDateUtil.convertDateStringToLocalDateTime("2017-01-02T00:00:00"))
				.thenReturn(LocalDateTime.parse("2017-01-02T00:00:00", DateTimeFormatter.ISO_DATE_TIME));
		Mockito.when(apiDateUtil.convertDateStringToLocalDateTime("2015-01-02T00:00:00"))
				.thenReturn(LocalDateTime.parse("2015-01-02T00:00:00", DateTimeFormatter.ISO_DATE_TIME));
		Mockito.when(apiDateUtil.convertDateStringToLocalDateTime("2017-01-01T00:00:00"))
				.thenReturn(LocalDateTime.parse("2017-01-01T00:00:00", DateTimeFormatter.ISO_DATE_TIME));
		Mockito.when(apiDateUtil.convertDateStringToLocalDateTime("2015-01-01T00:00:00"))
				.thenReturn(LocalDateTime.parse("2015-01-01T00:00:00", DateTimeFormatter.ISO_DATE_TIME));
		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountStatementsApiTestData.getAispConsent2());
		Pair<String, String> pair = new Pair<String, String>(null, null);
		Mockito.when(apiDateUtil.populateSetupDates(anyObject())).thenReturn(pair);

		ReflectionTestUtils.setField(service, "minPageSize", "1");
		ReflectionTestUtils.setField(service, "maxPageSize", "25");
		service.retrieveAccountStatements("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "2015-01-01T00:00:00",
				"2017-01-01T00:00:00", 1, 1, "asdasd");
	}

	@Test
	public void retrieveStatementsTestWithNoRecord() {
		Mockito.when(
				accountStatementsAdapter.retrieveAccountStatements(anyObject(), anyMapOf(String.class, String.class)))
				.thenReturn(null);

		Pair<String, String> pair = new Pair<String, String>(null, null);
		Mockito.when(apiDateUtil.populateSetupDates(anyObject())).thenReturn(pair);
		try {
			service.retrieveAccountStatements("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "2015-01-01T00:00:00",
					"2016-12-31T23:59:59", 2, null, null);
		} catch (PSD2Exception e) {
			assertEquals(HttpStatus.NOT_FOUND.toString(), e.getErrorInfo().getStatusCode());

		}
	}

	// Test Links population different scenarios
	@Test
	public void retrieveStatementsTestWithNullLinksFromAdapter() {
		Mockito.when(
				accountStatementsAdapter.retrieveAccountStatements(anyObject(), anyMapOf(String.class, String.class)))
				.thenReturn(AccountStatementsApiTestData.getAccountStatementsGETResponseWithNullLinks());
		Pair<String, String> pair = new Pair<String, String>(null, null);
		Mockito.when(apiDateUtil.populateSetupDates(anyObject())).thenReturn(pair);

		OBReadStatement1 response = service.retrieveAccountStatements("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", null,
				null, null, null, null);
		String actualResponseJson = JSONUtilities.getJSONOutPutFromObject(response);
		String expectedResponseJson = "{\"Data\":{\"Statement\":[{\"AccountId\":\"269c3ff5-d7f8-419b-a3b9-7136c5b4611a\",\"StatementId\":\"269c3ff5-d7f8-419b-a3b9-7136\",\"StatementReference\":\"gdqwe67384\",\"Type\":\"RegularPeriodic\",\"StartDateTime\":\"2016-10-01T00:00:00+00:00\",\"EndDateTime\":\"2018-09-01T00:00:00+00:00\",\"CreationDateTime\":\"2017-09-01T00:00:00+00:00\",\"StatementDescription\":[],\"StatementBenefit\":null,\"StatementFee\":null,\"StatementInterest\":null,\"StatementDateTime\":null,\"StatementRate\":null,\"StatementValue\":null,\"StatementAmount\":null}]},\"Links\":{\"Prev\": null}}";
		JSONAssert.assertEquals(expectedResponseJson, actualResponseJson, Boolean.FALSE);
	}

	@Test
	public void retrieveStatementsTestWithNullLinksObjectFromAdapter() {
		Mockito.when(
				accountStatementsAdapter.retrieveAccountStatements(anyObject(), anyMapOf(String.class, String.class)))
				.thenReturn(AccountStatementsApiTestData.getAccountStatementsGETResponseWithNullLinksObject());
		Pair<String, String> pair = new Pair<String, String>(null, null);
		Mockito.when(apiDateUtil.populateSetupDates(anyObject())).thenReturn(pair);
		OBReadStatement1 response = service.retrieveAccountStatements("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", null,
				null, null, null, null);
		String actualResponseJson = JSONUtilities.getJSONOutPutFromObject(response);
		String expectedResponseJson = "{\"Data\":{\"Statement\":[{\"AccountId\":\"269c3ff5-d7f8-419b-a3b9-7136c5b4611a\",}]},\"Links\":{\"Prev\": null}}";
		JSONAssert.assertEquals(expectedResponseJson, actualResponseJson, Boolean.FALSE);
	}

	// Test Consent Dates Availability
	@Test(expected = PSD2Exception.class)
	public void TestOnlyParamDatesPresent() {
		Mockito.when(reqHeaderAtrributes.getToken())
				.thenReturn(AccountStatementsApiTestData.getMockTokenWithDebitFilter());
		Pair<String, String> pair = new Pair<String, String>(null, null);
		Mockito.when(apiDateUtil.populateSetupDates(anyObject())).thenReturn(pair);
		Mockito.when(apiDateUtil.convertDateStringToLocalDateTime("2017-01-01T00:00:00"))
				.thenReturn(LocalDateTime.parse("2017-01-01T00:00:00", DateTimeFormatter.ISO_DATE_TIME));
		Mockito.when(apiDateUtil.convertDateStringToLocalDateTime("2015-01-01T00:00:00"))
				.thenReturn(LocalDateTime.parse("2015-01-01T00:00:00", DateTimeFormatter.ISO_DATE_TIME));
		ReflectionTestUtils.setField(service, "minPageSize", "1");
		ReflectionTestUtils.setField(service, "maxPageSize", "25");
		service.retrieveAccountStatements("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "2015-01-01T00:00:00",
				"2017-01-01T00:00:00", 1, 1, "asdasd");
	}

	@Test(expected = PSD2Exception.class)
	public void TestOnlyConsentDatesPresent() {
		// Mockito.when(reqHeaderAtrributes.getToken())
		// .thenReturn(AccountStatementsApiTestData.getMockTokenWithDebitFilter());
		Pair<String, String> pair = new Pair<String, String>(null, null);
		Mockito.when(apiDateUtil.populateSetupDates(anyObject())).thenReturn(pair);
		Mockito.when(apiDateUtil.convertDateStringToLocalDateTime("2017-01-02T00:00:00"))
				.thenReturn(LocalDateTime.parse("2017-01-02T00:00:00", DateTimeFormatter.ISO_DATE_TIME));
		Mockito.when(apiDateUtil.convertDateStringToLocalDateTime("2015-01-02T00:00:00"))
				.thenReturn(LocalDateTime.parse("2015-01-02T00:00:00", DateTimeFormatter.ISO_DATE_TIME));
		Mockito.when(aispConsentAdapter.retrieveConsent(anyString()))
				.thenReturn(AccountStatementsApiTestData.getAispConsent2());
		ReflectionTestUtils.setField(service, "minPageSize", "1");
		ReflectionTestUtils.setField(service, "maxPageSize", "25");
		service.retrieveAccountStatements("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", null, null, 1, 1, "asdasd");
	}

	@Test(expected = PSD2Exception.class)
	public void TestBothConsentAndParamDatesPresentParamsInRange() {
		// Mockito.when(reqHeaderAtrributes.getToken())
		// .thenReturn(AccountStatementsApiTestData.getMockTokenWithDebitFilter());
		Pair<String, String> pair = new Pair<String, String>(null, null);
		Mockito.when(apiDateUtil.populateSetupDates(anyObject())).thenReturn(pair);
		Mockito.when(apiDateUtil.convertDateStringToLocalDateTime("2017-01-02T00:00:00"))
				.thenReturn(LocalDateTime.parse("2017-01-02T00:00:00", DateTimeFormatter.ISO_DATE_TIME));
		Mockito.when(apiDateUtil.convertDateStringToLocalDateTime("2015-01-02T00:00:00"))
				.thenReturn(LocalDateTime.parse("2015-01-02T00:00:00", DateTimeFormatter.ISO_DATE_TIME));
		Mockito.when(apiDateUtil.convertDateStringToLocalDateTime("2017-01-03T00:00:00"))
				.thenReturn(LocalDateTime.parse("2017-01-03T00:00:00", DateTimeFormatter.ISO_DATE_TIME));
		Mockito.when(apiDateUtil.convertDateStringToLocalDateTime("2015-01-03T00:00:00"))
				.thenReturn(LocalDateTime.parse("2015-01-03T00:00:00", DateTimeFormatter.ISO_DATE_TIME));
		Mockito.when(aispConsentAdapter.retrieveConsent(anyString()))
				.thenReturn(AccountStatementsApiTestData.getAispConsent2());
		ReflectionTestUtils.setField(service, "minPageSize", "1");
		ReflectionTestUtils.setField(service, "maxPageSize", "25");
		service.retrieveAccountStatements("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "2015-01-03T00:00:00",
				"2017-01-03T00:00:00", 1, 1, "asdasd");
	}

	@SuppressWarnings("unchecked")
	@Test
	public void tppStatementReponse_NotNull() {
		OBReadStatement1 tppStatementReponse = new OBReadStatement1();
		tppStatementReponse.setData(null);
		tppStatementReponse.setLinks(null);
		AispConsent aispConsent = AccountStatementsApiTestData.getAispConsent2();
		Mockito.when(aispConsentAdapter.retrieveConsent(anyString()))
				.thenReturn(aispConsent);
		Pair<String, String> consentDates = new Pair<String, String>(null, null);
		AccountMapping accountMapping = AccountStatementsApiTestData.getMockAccountMapping();
		accountMapping.setPsuId("123123");
		
		PlatformAccountStatementsResponse platformAccountStatementsResponse = new PlatformAccountStatementsResponse();
		platformAccountStatementsResponse.setoBReadStatement1(new OBReadStatement1());
		Mockito.when(accountStatementsAdapter.retrieveAccountStatements(anyObject(), anyMap())).thenReturn(platformAccountStatementsResponse);
		Mockito.when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(),anyObject())).thenReturn(accountMapping);
		Mockito.when(apiDateUtil.populateSetupDates(anyObject())).thenReturn(consentDates);
		service.retrieveAccountStatements("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "2015-01-03T00:00:00",
				"2017-01-03T00:00:00", 1, 1, "asdasd");
	}
	
	@Test
	public void tppStatementReponse_Null() {
		AispConsent aispConsent = AccountStatementsApiTestData.getAispConsent2();
		Mockito.when(aispConsentAdapter.retrieveConsent(anyString()))
				.thenReturn(aispConsent);
		Pair<String, String> consentDates = new Pair<String, String>(null, null);
		AccountMapping accountMapping = AccountStatementsApiTestData.getMockAccountMapping();
		accountMapping.setPsuId("123123");
		PlatformAccountStatementsResponse platformAccountStatementsResponse = new PlatformAccountStatementsResponse();
		platformAccountStatementsResponse.setoBReadStatement1(null);
		Mockito.when(accountStatementsAdapter.retrieveAccountStatements(anyObject(), anyMap())).thenReturn(platformAccountStatementsResponse);
		Mockito.when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(),anyObject())).thenReturn(accountMapping);
		Mockito.when(apiDateUtil.populateSetupDates(anyObject())).thenReturn(consentDates);
		service.retrieveAccountStatements("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "2015-01-03T00:00:00",
				"2017-01-03T00:00:00", 1, 1, "asdasd");
	}

	@Test
	public void retrieveStatementsTestLinksWhenDatesInParams() {
		Mockito.when(
				accountStatementsAdapter.retrieveAccountStatements(anyObject(), anyMapOf(String.class, String.class)))
				.thenReturn(AccountStatementsApiTestData.getAccountStatementsGETResponseForService());
		Pair<String, String> pair = new Pair<String, String>(null, null);
		Mockito.when(apiDateUtil.populateSetupDates(anyObject())).thenReturn(pair);
		Mockito.when(reqHeaderAtrributes.getSelfUrl())
				.thenReturn("/accounts/269c3ff5-d7f8-419b-a3b9-7136c5b4611a/statements/");

		OBReadStatement1 response = service.retrieveAccountStatements("269c3ff5-d7f8-419b-a3b9-7136c5b4611a",
				"2015-01-01T00:00:00", "2017-01-01T00:00:00", null, null, null);
		String actualResponseJson = JSONUtilities.getJSONOutPutFromObject(response);
		String expectedResponseJson = "{\"Data\": {\"Statement\":[{\"AccountId\": \"269c3ff5-d7f8-419b-a3b9-7136c5b4611a\"}]},\"Links\":{\"Next\":\"/accounts/269c3ff5-d7f8-419b-a3b9-7136c5b4611a/statements/?fromStatementDateTime=2015-01-01T00:00:00&toStatementDateTime=2017-01-01T00:00:00&page=5\"}}";
		JSONAssert.assertEquals(expectedResponseJson, actualResponseJson, Boolean.FALSE);
	}

	// statementId
	@Test
	public void retrieveAccountStatementIdSuccessWithLinksMetaTest() {
		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountStatementsApiTestData.getMockAispConsent());

		Mockito.when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject()))
				.thenReturn(AccountStatementsApiTestData.getMockAccountMapping());

		Mockito.when(accountStatementsAdapter.retrieveAccountStatementsByStatementId(anyObject(), anyObject()))
				.thenReturn(AccountStatementsApiTestData.getMockPlatformAccountStatementsResponseWithLinksMeta());

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountStatementsApiTestData.getMockLoggerData());

		accountsStatementsValidatorImpl.validateResponseParams(anyObject());
		// responseValidator.validateResponse(anyObject());
		// verify(responseValidator).validateResponse(anyObject());

		OBReadStatement1 actualResponse = service.retrieveAccountStatementsByStatementsId(
				"f4483fda-81be-4873-b4a6-20375b7f55c1", "kedhuei23123", null, null, null);

	}

	@Test
	public void retrieveStatementIdTestWithNullParams() {
		// doNothing().when(responseValidator).validateResponse(anyObject());
		accountsStatementsValidatorImpl.validateResponseParams(anyObject());
		when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject()))
				.thenReturn(AccountStatementsApiTestData.getMockAccountMapping());
		// LocalDateTime local=LocalDateTime.now();
		// when(apiDateUtil.convertDateStringToLocalDateTime(anyString())).thenReturn(local);
		// when(apiDateUtil.convertDateStringToLocalDateTime(anyString())).thenReturn(local);
		// Pair<String, String> pair = new Pair<String, String>(null, null);
		// Mockito.when(apiDateUtil.populateSetupDates(anyObject())).thenReturn(pair);
		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountStatementsApiTestData.getAispConsent());
		Mockito.when(accountStatementsAdapter.retrieveAccountStatementsByStatementId(anyObject(),
				anyMapOf(String.class, String.class)))
				.thenReturn(AccountStatementsApiTestData.getAccountStatementsGETResponseForService());
		OBReadStatement1 response = service.retrieveAccountStatementsByStatementsId(
				"269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "4246985", null, null, null);
		String actualResponseJson = JSONUtilities.getJSONOutPutFromObject(response);
		// String expectedResponseJson =
		// "{\"Data\":{\"Statement\":[{\"AccountId\":\"269c3ff5-d7f8-419b-a3b9-7136c5b4611a\",}]},\"Links\":{\"Prev\":
		// null}}";
		String expectedResponseJson = "{\"Data\":{\"Statement\":[{\"AccountId\":\"269c3ff5-d7f8-419b-a3b9-7136c5b4611a\",\"StatementId\":\"269c3ff5-d7f8-419b-a3b9-7136\",\"StatementReference\":\"gdqwe67384\",\"Type\":\"RegularPeriodic\",\"StartDateTime\":\"2016-10-01T00:00:00+00:00\",\"EndDateTime\":\"2018-09-01T00:00:00+00:00\",\"CreationDateTime\":\"2017-09-01T00:00:00+00:00\",\"StatementDescription\":[],\"StatementBenefit\":null,\"StatementFee\":null,\"StatementInterest\":null,\"StatementDateTime\":null,\"StatementRate\":null,\"StatementValue\":null,\"StatementAmount\":null}]},\"Links\":{\"Self\":\"/accounts/269c3ff5-d7f8-419b-a3b9-7136c5b4611a/statements/7136c5b4611a/file\",\"First\":\"1\",\"Prev\":\"3\",\"Next\":\"5\",\"Last\":\"10\"},\"Meta\":{\"TotalPages\":null,\"FirstAvailableDateTime\":null,\"LastAvailableDateTime\":null}}";
		JSONAssert.assertEquals(expectedResponseJson, actualResponseJson, Boolean.FALSE);
	}

	@Test(expected = PSD2Exception.class)
	public void retrieveStatementIdTestWithNullParamsEmptyData() {
		// doNothing().when(responseValidator).validateResponse(anyObject());
		accountsStatementsValidatorImpl.validateResponseParams(anyObject());
		when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject()))
				.thenReturn(AccountStatementsApiTestData.getMockAccountMapping());
		/*
		 * when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(),
		 * anyObject())).thenReturn(AccountStatementsApiTestData.getMockAccountMapping()
		 * );
		 */
		// LocalDateTime local=LocalDateTime.now();
		// when(apiDateUtil.convertDateStringToLocalDateTime(anyString())).thenReturn(local);
		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountStatementsApiTestData.getAispConsent());
		Mockito.when(accountStatementsAdapter.retrieveAccountStatementsByStatementId(anyObject(),
				anyMapOf(String.class, String.class)))
				.thenReturn(AccountStatementsApiTestData.getAccountStatementsGETResponseForServiceTesting());

		Pair<String, String> pair = new Pair<String, String>(null, null);
		Mockito.when(apiDateUtil.populateSetupDates(anyObject())).thenReturn(pair);
		service.retrieveAccountStatementsByStatementsId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "674562", null, null,
				null);
	}

	// Test Links population different scenarios
	@Test
	public void retrieveStatementsByStatementIdTestWithNullLinksFromAdapter() {
		Mockito.when(accountStatementsAdapter.retrieveAccountStatementsByStatementId(anyObject(),
				anyMapOf(String.class, String.class)))
				.thenReturn(AccountStatementsApiTestData.getAccountStatementsGETResponseWithNullLinks());
		Pair<String, String> pair = new Pair<String, String>(null, null);
		Mockito.when(apiDateUtil.populateSetupDates(anyObject())).thenReturn(pair);

		OBReadStatement1 response = service.retrieveAccountStatementsByStatementsId(
				"269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "24csd", null, null, null);
		String actualResponseJson = JSONUtilities.getJSONOutPutFromObject(response);
		// String expectedResponseJson =
		// "{\"Data\":{\"Statement\":[{\"AccountId\":\"269c3ff5-d7f8-419b-a3b9-7136c5b4611a\",\"StatementId\":\"269c3ff5-d7f8-419b-a3b9-7136\",\"StatementReference\":\"gdqwe67384\",\"Type\":\"RegularPeriodic\",\"StartDateTime\":\"2016-10-01T00:00:00+00:00\",\"EndDateTime\":\"2018-09-01T00:00:00+00:00\",\"CreationDateTime\":\"2017-09-01T00:00:00+00:00\",\"StatementDescription\":[],\"StatementBenefit\":null,\"StatementFee\":null,\"StatementInterest\":null,\"StatementDateTime\":null,\"StatementRate\":null,\"StatementValue\":null,\"StatementAmount\":null}]},\"Links\":{\"Prev\":
		// null}}";
		String expectedResponseJson = "{\"Data\":{\"Statement\":[{\"AccountId\":\"269c3ff5-d7f8-419b-a3b9-7136c5b4611a\",\"StatementId\":\"269c3ff5-d7f8-419b-a3b9-7136\",\"StatementReference\":\"gdqwe67384\",\"Type\":\"RegularPeriodic\",\"StartDateTime\":\"2016-10-01T00:00:00+00:00\",\"EndDateTime\":\"2018-09-01T00:00:00+00:00\",\"CreationDateTime\":\"2017-09-01T00:00:00+00:00\",\"StatementDescription\":[],\"StatementBenefit\":null,\"StatementFee\":null,\"StatementInterest\":null,\"StatementDateTime\":null,\"StatementRate\":null,\"StatementValue\":null,\"StatementAmount\":null}]},\"Links\":{\"Self\":\"/accounts/269c3ff5-d7f8-419b-a3b9-7136c5b4611a/statements/7136c5b4611a/file\",\"First\":null,\"Prev\":null,\"Next\":null,\"Last\":null},\"Meta\":{\"TotalPages\":1,\"FirstAvailableDateTime\":null,\"LastAvailableDateTime\":null}}";
		JSONAssert.assertEquals(expectedResponseJson, actualResponseJson, Boolean.FALSE);
	}

	@Test
	public void retrieveStatementsByStatementIdTestWithNullLinksObjectFromAdapter() {
		Mockito.when(accountStatementsAdapter.retrieveAccountStatementsByStatementId(anyObject(),
				anyMapOf(String.class, String.class)))
				.thenReturn(AccountStatementsApiTestData.getAccountStatementsGETResponseWithNullLinksObject());
		Pair<String, String> pair = new Pair<String, String>(null, null);
		Mockito.when(apiDateUtil.populateSetupDates(anyObject())).thenReturn(pair);
		OBReadStatement1 response = service.retrieveAccountStatementsByStatementsId(
				"269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "525sfsdf", null, null, null);
		String actualResponseJson = JSONUtilities.getJSONOutPutFromObject(response);
		String expectedResponseJson = "{\"Data\":{\"Statement\":[{\"AccountId\":\"269c3ff5-d7f8-419b-a3b9-7136c5b4611a\",}]},\"Links\":{\"Prev\": null}}";
		JSONAssert.assertEquals(expectedResponseJson, actualResponseJson, Boolean.FALSE);
	}

	// statementId/transactions
	@Test
	public void retrieveTransactionByStatementIdTestWithNullParams() {
		doNothing().when(responseValidator).validateResponse(anyObject());
		when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject()))
				.thenReturn(AccountStatementsApiTestData.getMockAccountMapping());
		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountStatementsApiTestData.getAispConsent());
		Mockito.when(accountStatementsAdapter.retrieveAccountTransactionsByStatementsId(anyObject(),
				anyMapOf(String.class, String.class)))
				.thenReturn(AccountStatementsApiTestData.getAccountTransactionsGETResponseForService());

		OBReadTransaction3 response = service.retrieveAccountTransactionsByStatementsId(
				"269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "4324234", null, null, null);
		String actualResponseJson = JSONUtilities.getJSONOutPutFromObject(response);
		String expectedResponseJson = "{\"Data\": {\"Transaction\": [{\"AccountId\": \"269c3ff5-d7f8-419b-a3b9-7136c5b4611a\",\"TransactionId\": \"123\",\"TransactionReference\": \"Ref123\",\"Amount\": {\"Amount\": \"10.00\",\"Currency\": \"GBP\"},\"CreditDebitIndicator\": \"Credit\",\"Status\": \"Booked\",\"BookingDateTime\": \"2017-04-05T10:43:07+00:00\",\"ValueDateTime\": \"2017-04-05T10:45:22+00:00\",\"TransactionInformation\": \"Cash from Aubrey\",\"AddressLine\": \"XYZ address Line\",\"BankTransactionCode\": {\"Code\": \"ReceivedCreditTransfer\",\"SubCode\": \"DomesticCreditTransfer\"},\"ProprietaryBankTransactionCode\": {\"Code\": \"Transfer\",\"Issuer\": \"AlphaBank\"},\"Balance\": {\"Amount\": {\"Amount\": \"230.00\",\"Currency\": \"GBP\"},\"CreditDebitIndicator\": \"Credit\",\"Type\": \"InterimBooked\"},\"MerchantDetails\": {\"MerchantName\": \"MerchantXYZ\",\"MerchantCategoryCode\": \"MerchantXYZcode\"}}]},\"Links\":{\"Prev\": \"/accounts/269c3ff5-d7f8-419b-a3b9-7136c5b4611a/statements/7136c5b4611a/file?page=3\"}}";
		JSONAssert.assertEquals(expectedResponseJson, actualResponseJson, Boolean.FALSE);
	}

	@Test(expected = PSD2Exception.class)
	public void retrieveTransactionByStatementIdTestWithNullParamsEmptyData() {
		doNothing().when(responseValidator).validateResponse(anyObject());
		// accountsStatementsValidatorImpl.validateResponseParams(anyObject());
		when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject()))
				.thenReturn(AccountStatementsApiTestData.getMockAccountMapping());
		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountStatementsApiTestData.getAispConsent());
		Mockito.when(accountStatementsAdapter.retrieveAccountTransactionsByStatementsId(anyObject(),
				anyMapOf(String.class, String.class)))
				.thenReturn(AccountStatementsApiTestData.getAccountTransactionsGETResponseForServiceTesting());

		service.retrieveAccountTransactionsByStatementsId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "23123132", null,
				null, null);
		/*
		 * String actualResponseJson = JSONUtilities.getJSONOutPutFromObject(response);
		 * String expectedResponseJson =
		 * "{\"Data\": {\"Transaction\": [{\"AccountId\": \"269c3ff5-d7f8-419b-a3b9-7136c5b4611a\",\"TransactionId\": \"123\",\"TransactionReference\": \"Ref123\",\"Amount\": {\"Amount\": \"10.00\",\"Currency\": \"GBP\"},\"CreditDebitIndicator\": \"Credit\",\"Status\": \"Booked\",\"BookingDateTime\": \"2017-04-05T10:43:07+00:00\",\"ValueDateTime\": \"2017-04-05T10:45:22+00:00\",\"TransactionInformation\": \"Cash from Aubrey\",\"AddressLine\": \"XYZ address Line\",\"BankTransactionCode\": {\"Code\": \"ReceivedCreditTransfer\",\"SubCode\": \"DomesticCreditTransfer\"},\"ProprietaryBankTransactionCode\": {\"Code\": \"Transfer\",\"Issuer\": \"AlphaBank\"},\"Balance\": {\"Amount\": {\"Amount\": \"230.00\",\"Currency\": \"GBP\"},\"CreditDebitIndicator\": \"Credit\",\"Type\": \"InterimBooked\"},\"MerchantDetails\": {\"MerchantName\": \"MerchantXYZ\",\"MerchantCategoryCode\": \"MerchantXYZcode\"}}]},\"Links\":{\"Prev\": \"/accounts/269c3ff5-d7f8-419b-a3b9-7136c5b4611a/transactions/?page=3\"}}"
		 * ; JSONAssert.assertEquals(expectedResponseJson, actualResponseJson,
		 * Boolean.FALSE);
		 */
	}

	// Test Links population different scenarios
	@Test
	public void retrieveTransactionByStatementIdTestWithNullLinksFromAdapter() {
		Mockito.when(accountStatementsAdapter.retrieveAccountTransactionsByStatementsId(anyObject(),
				anyMapOf(String.class, String.class)))
				.thenReturn(AccountStatementsApiTestData.getAccountTransactionsGETResponseWithNullLinks());

		OBReadTransaction3 response = service.retrieveAccountTransactionsByStatementsId(
				"269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "3434", null, null, null);
		String actualResponseJson = JSONUtilities.getJSONOutPutFromObject(response);
		String expectedResponseJson = "{\"Data\": {\"Transaction\": [{\"AccountId\": \"269c3ff5-d7f8-419b-a3b9-7136c5b4611a\",\"TransactionId\": \"123\",\"TransactionReference\": \"Ref123\",\"Amount\": {\"Amount\": \"10.00\",\"Currency\": \"GBP\"},\"CreditDebitIndicator\": \"Credit\",\"Status\": \"Booked\",\"BookingDateTime\": \"2017-04-05T10:43:07+00:00\",\"ValueDateTime\": \"2017-04-05T10:45:22+00:00\",\"TransactionInformation\": \"Cash from Aubrey\",\"AddressLine\": \"XYZ address Line\",\"BankTransactionCode\": {\"Code\": \"ReceivedCreditTransfer\",\"SubCode\": \"DomesticCreditTransfer\"},\"ProprietaryBankTransactionCode\": {\"Code\": \"Transfer\",\"Issuer\": \"AlphaBank\"},\"Balance\": {\"Amount\": {\"Amount\": \"230.00\",\"Currency\": \"GBP\"},\"CreditDebitIndicator\": \"Credit\",\"Type\": \"InterimBooked\"},\"MerchantDetails\": {\"MerchantName\": \"MerchantXYZ\",\"MerchantCategoryCode\": \"MerchantXYZcode\"}}]},\"Links\":{\"Prev\": null}}";
		JSONAssert.assertEquals(expectedResponseJson, actualResponseJson, Boolean.FALSE);
	}

	@Test
	public void retrieveTransactionByStatementIdTestWithNullLinksObjectFromAdapter() {
		Mockito.when(accountStatementsAdapter.retrieveAccountTransactionsByStatementsId(anyObject(),
				anyMapOf(String.class, String.class)))
				.thenReturn(AccountStatementsApiTestData.getAccountTransactionsGETResponseWithNullLinksObject());

		OBReadTransaction3 response = service.retrieveAccountTransactionsByStatementsId(
				"269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "55456", null, null, null);
		String actualResponseJson = JSONUtilities.getJSONOutPutFromObject(response);
		String expectedResponseJson = "{\"Data\": {\"Transaction\": [{\"AccountId\": \"269c3ff5-d7f8-419b-a3b9-7136c5b4611a\",}]},\"Links\":{\"Prev\": null}}";
		JSONAssert.assertEquals(expectedResponseJson, actualResponseJson, Boolean.FALSE);
	}

	// @Test(expected = PSD2Exception.class)
	@Test(expected = NullPointerException.class)
	public void retrieveTransactionByStatementIdTestWithNullDataObjectFromAdapter() {
		/*
		 * Mockito.when(
		 * accountStatementsAdapter.retrieveAccountTransactionsByStatementsId(anyObject(
		 * ), anyMapOf(String.class, String.class)
		 * )).thenReturn(AccountStatementsApiTestData.
		 * getAccountTransactionsGETResponseWithNullDataObject());
		 */
		PlatformAccountTransactionResponse plat = AccountStatementsApiTestData
				.getAccountTransactionsGETResponseWithNullDataObject();
		Mockito.when(accountStatementsAdapter.retrieveAccountTransactionsByStatementsId(anyObject(),
				anyMapOf(String.class, String.class))).thenReturn(plat);

		OBReadTransaction3 response = service.retrieveAccountTransactionsByStatementsId(
				"269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "55456", null, null, null);
		String actualResponseJson = JSONUtilities.getJSONOutPutFromObject(response);
		String expectedResponseJson = "{\"Data\": null,\"Links\":{\"Prev\": null}}";
		JSONAssert.assertEquals(expectedResponseJson, actualResponseJson, Boolean.FALSE);
	}

	@Test
	public void retrieveTransactionByStatementIdTestLinksWhenDatesInParams() {
		Mockito.when(accountStatementsAdapter.retrieveAccountTransactionsByStatementsId(anyObject(),
				anyMapOf(String.class, String.class)))
				.thenReturn(AccountStatementsApiTestData.getAccountTransactionsGETResponseForService());

		Mockito.when(reqHeaderAtrributes.getSelfUrl())
				.thenReturn("/accounts/269c3ff5-d7f8-419b-a3b9-7136c5b4611a/transactions/");

		OBReadTransaction3 response = service.retrieveAccountTransactionsByStatementsId(
				"269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "2015dwe", null, null, null);
		String actualResponseJson = JSONUtilities.getJSONOutPutFromObject(response);
		String expectedResponseJson = "{\"Data\": {\"Transaction\":[{\"AccountId\": \"269c3ff5-d7f8-419b-a3b9-7136c5b4611a\"}]},\"Links\":{\"Next\":\"/accounts/269c3ff5-d7f8-419b-a3b9-7136c5b4611a/transactions/?page=5\"}}";
		JSONAssert.assertEquals(expectedResponseJson, actualResponseJson, Boolean.FALSE);
	}

	// Test different filter permissions present in token
	@Test(expected = PSD2Exception.class)
	public void TestCreditFilter() {
		ReflectionTestUtils.setField(service, "minPageSize", "1");
		ReflectionTestUtils.setField(service, "maxPageSize", "25");
		Mockito.when(reqHeaderAtrributes.getToken())
				.thenReturn(AccountStatementsApiTestData.getMockTokenWithCreditFilter());
		service.retrieveAccountTransactionsByStatementsId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "2017rtur", 1, 1,
				"asdasd");
	}

	@Test(expected = PSD2Exception.class)
	public void TestDebitFilter() {
		Mockito.when(reqHeaderAtrributes.getToken())
				.thenReturn(AccountStatementsApiTestData.getMockTokenWithDebitFilter());
		ReflectionTestUtils.setField(service, "minPageSize", "1");
		ReflectionTestUtils.setField(service, "maxPageSize", "25");
		service.retrieveAccountTransactionsByStatementsId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "201sfdsd", 1, 1,
				"asdasd");
	}

	// file

	@Test(expected = PSD2Exception.class)
	public void TestStatementFilter() {
		Mockito.when(reqHeaderAtrributes.getToken())
				.thenReturn(AccountStatementsApiTestData.getMockTokenWithCreditFilter());

		ReflectionTestUtils.setField(service, "minPageSize", "1");
		ReflectionTestUtils.setField(service, "maxPageSize", "25");
		service.retrieveAccountTransactionsByStatementsId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "2017rtur", 1, 1,
				"asdasd");
	}

	@Test(expected = PSD2Exception.class)
	public void downloadStatementWithoutPermission() {

		PlatformAccountSatementsFileResponse accountSatementsFileResponse = null;

		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountStatementsApiTestData.getMockAispConsent());
		Mockito.when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject()))
				.thenReturn(AccountStatementsApiTestData.getMockAccountMapping());

		Mockito.when(accountStatementsAdapter.downloadStatementFileByStatementsId(anyObject(), anyObject()))
				.thenReturn(accountSatementsFileResponse);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountStatementsApiTestData.getMockLoggerData());

		// responseValidator.validateResponse(anyObject());
		accountsStatementsValidatorImpl.validateResponseParams(anyObject());
		PlatformAccountSatementsFileResponse actualResponse = service
				.downloadStatementFileByStatementsId("f4483fda-81be-4873-b4a6-20375b7f55c1", "kedhuei23123");

	}

	@Test(expected = PSD2Exception.class)
	public void downloadStatementNoStatementId() {

		PlatformAccountSatementsFileResponse accountSatementsFileResponse = null;

		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountStatementsApiTestData.getMockAispConsent());
		Mockito.when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject()))
				.thenReturn(AccountStatementsApiTestData.getMockAccountMapping());

		ReflectionTestUtils.setField(service, "fileFilter", "ReadTransactionsCredits");

		Mockito.when(accountStatementsAdapter.downloadStatementFileByStatementsId(anyObject(), anyObject()))
				.thenReturn(accountSatementsFileResponse);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountStatementsApiTestData.getMockLoggerData());

		// responseValidator.validateResponse(anyObject());
		accountsStatementsValidatorImpl.validateResponseParams(anyObject());
		PlatformAccountSatementsFileResponse actualResponse = service
				.downloadStatementFileByStatementsId("f4483fda-81be-4873-b4a6-20375b7f55c1", "kedhuei23123");

	}

	@Test
	public void downloadStatementSuccess() {

		PlatformAccountSatementsFileResponse accountSatementsFileResponse = new PlatformAccountSatementsFileResponse();

		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountStatementsApiTestData.getMockAispConsent());
		Mockito.when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject()))
				.thenReturn(AccountStatementsApiTestData.getMockAccountMapping());

		ReflectionTestUtils.setField(service, "fileFilter", "ReadTransactionsCredits");

		Mockito.when(accountStatementsAdapter.downloadStatementFileByStatementsId(anyObject(), anyObject()))
				.thenReturn(accountSatementsFileResponse);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountStatementsApiTestData.getMockLoggerData());

		// responseValidator.validateResponse(anyObject());
		accountsStatementsValidatorImpl.validateResponseParams(anyObject());
		PlatformAccountSatementsFileResponse actualResponse = service
				.downloadStatementFileByStatementsId("f4483fda-81be-4873-b4a6-20375b7f55c1", "kedhuei23123");

	}

	@Test(expected = PSD2Exception.class)
	public void testInvalidAccountIdLengthForStatementIdUrl() {
		service.retrieveAccountStatementsByStatementsId(null,
				"20erweruyewruiewyruewiriwerhuweirhiewhfewuiry789322648926347238647234y674yuierhjdfdsfhghewfguiweryewuiryiewury8923748972348927443iryhuieshjferifheruiy934875983475893454",
				3, 4, "dsd");
	}

	@Test(expected = PSD2Exception.class)
	public void testInvalidstatementIdLengthForStatementIdUrl() {
		service.retrieveAccountStatementsByStatementsId("f4483fda-81be-4873-b4a6-20375b7f55c1", null, 3, 4, "dsd");
	}

	@After
	public void tearDown() throws Exception {
		service = null;
	}
}