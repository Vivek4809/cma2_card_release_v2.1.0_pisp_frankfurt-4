package com.capgemini.psd2.foundationservice.raml.exceptions;

public class ErrorInfo {
	
    private int replayCount;

    private String lastError;

    public int getReplayCount() {
      return this.replayCount;
    }

    public void setReplayCount(int replayCount) {
      this.replayCount = replayCount;
    }

    public String getLastError() {
      return this.lastError;
    }

    public void setLastError(String lastError) {
      this.lastError = lastError;
    }

}
