package com.capgemini.psd2.foundationservice.exceptions;

public class MockFoundationServiceAuthException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private final AuthenticationResponse authenticationResponse;

	public MockFoundationServiceAuthException(String message, AuthenticationResponse authenticationResponse) {
		super(message);
		this.authenticationResponse = authenticationResponse;
	}

	public AuthenticationResponse getErrorInfo() {
		return authenticationResponse;
	}

	public static MockFoundationServiceAuthException populateMockFoundationServiceException(ErrorCodeEnum errorCodeEnum) {
		AuthenticationResponse authenticationResponse = new AuthenticationResponse();
		authenticationResponse.setResponseCode(errorCodeEnum.getErrorCode());
		authenticationResponse.setResponseMessage(errorCodeEnum.getErrorText());
		authenticationResponse.setRemainingAttempts(1);

		return new MockFoundationServiceAuthException(errorCodeEnum.getErrorText(), authenticationResponse);
	}
	
	public static MockFoundationServiceAuthException populateMockFoundationServiceException(ErrorCodeEnum errorCodeEnum, int remainingAttempts) {
		AuthenticationResponse authenticationResponse = new AuthenticationResponse();
		authenticationResponse.setResponseCode(errorCodeEnum.getErrorCode());
		authenticationResponse.setResponseMessage(errorCodeEnum.getErrorText());
		authenticationResponse.setRemainingAttempts(remainingAttempts);

		return new MockFoundationServiceAuthException(errorCodeEnum.getErrorText(), authenticationResponse);
	}

}
