package com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.constants;

public class CustomerAccountProfileFoundationServiceConstants {
	
	public static final String  TSYSCUTOMERID="TSYS Customer ID";
	public static final String  PLAPPLID="PLAPPLID";
	public static final String  CURRENTACCOUNT="Current Account";
	public static final String  SAVINGS="Savings Account";
	public static final String  CREDITCARD="Credit Card";
	public static final String  DISPLAYONLYACCOUNT="Display Only Account";
	public static final String  BOIROI="BOI-ROI";
	public static final String IBAN ="IBAN";
	public static final String UK_OBIE_IBAN_DOT = "UK.OBIE.IBAN";
	public static final String UK_OBIE_IBAN = "UK_OBIE_IBAN";
	public static final String SORTCODEACCOUNTNUMBER  ="SortCodeAccountNumber";
	public static final String PAN ="PAN";
	public static final String UK_OBIE_SortCodeAccountNumber = "UK_OBIE_SortCodeAccountNumber";
	public static final String UK_OBIE_PAN = "UK_OBIE_PAN";
	public static final String UK_OBIE_SortCodeAccountNumber_DOT = "UK.OBIE.SortCodeAccountNumber";
	public static final String UK_OBIE_PAN_DOT = "UK.OBIE.PAN";
	public static final String PARTY_PERMISSION = "PARTY-PERMISSION";
	public static final String BALANCE_CURRENCY = "balanceCurrency";
	public static final String BALANCE_AMOUNT = "balanceAmount";
}

