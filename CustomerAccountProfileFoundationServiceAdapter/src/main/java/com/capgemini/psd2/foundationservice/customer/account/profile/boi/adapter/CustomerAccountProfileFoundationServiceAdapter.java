package com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.aisp.domain.OBReadAccount2;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.client.CustomerAccountProfileFoundationServiceClient;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.delegate.CustomerAccountProfileFoundationServiceDelegate;
import com.capgemini.psd2.fraudsystem.domain.PSD2CustomerInfo;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * CustomerAccountProfileFoundationServiceAdapter This adapter is to get
 * Customer Account List
 */
@Component
public class CustomerAccountProfileFoundationServiceAdapter implements CustomerAccountListAdapter {

	/* Customer Account Profile service Delegate */
	@Autowired
	private CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;

	@Autowired
	private CustomerAccountProfileFoundationServiceDelegate customerAccountProfileFoundationServiceDelegate;

	@Autowired
	private CustomerAccountProfileFoundationServiceClient customerAccountProfileFoundationServiceClient;

	@Autowired
	private AdapterFilterUtility adapterFilterUtility;

	/* Jurisdiction Type */
	// @Value("${foundationService.jurisdictionType}")
	// private String jurisdictionType;

	@Override
	public OBReadAccount2 retrieveCustomerAccountList(String userId, Map<String, String> params) {
		/*
		 * RequestInfo requestInfo = new RequestInfo(); HttpHeaders httpHeaders
		 * = null;
		 */
		
		OBReadAccount2 oBReadAccount2 = new OBReadAccount2();
		
		if (params == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		if (NullCheckUtils.isNullOrEmpty(userId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}

		DigitalUserProfile digiUserProfile = new DigitalUserProfile();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.DigitalUserProfile digiUserProfilePISP = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.DigitalUserProfile();
		/* Remove in next sprint(18) */
		// params.put(PSD2Constants.TENANT_ID, jurisdictionType); - REMOVED.

		if ((params.get(PSD2Constants.CONSENT_FLOW_TYPE).toUpperCase()).equalsIgnoreCase("AISP")) {
			digiUserProfile = commonFilterUtility.retrieveCustomerAccountList(userId, params);
			
			oBReadAccount2 =  customerAccountProfileFoundationServiceDelegate.transformResponseFromFDToAPI(digiUserProfile, params);
		}

		else if ((params.get(PSD2Constants.CONSENT_FLOW_TYPE).toUpperCase()).equalsIgnoreCase("PISP")) {
			digiUserProfilePISP = commonFilterUtility.retrieveCustomerAccountListPISP(userId, params);
			
			oBReadAccount2 =  customerAccountProfileFoundationServiceDelegate.transformResponseFromFDToAPIPISP(digiUserProfilePISP, params);
		}

		/* Disable Jurisdiction Filter */
		// if (!NullCheckUtils.isNullOrEmpty(jurisdictionType))
		// digiUserProfile =
		// adapterFilterUtility.prevalidateJurisdiction(digiUserProfile,
		// jurisdictionType);

		/*
		 * httpHeaders = customerAccountProfileFoundationServiceDelegate.
		 * createRequestMuleHeaders(params); requestInfo.setUrl(
		 * customerAccountProfileFoundationServiceDelegate.
		 * getFoundationServiceMuleURL(params, userId)); DigitalUserProfile
		 * digiUserProfile =
		 * customerAccountProfileFoundationServiceClient.restTransportForParty(
		 * requestInfo, DigitalUserProfile.class, httpHeaders);
		 */

		return oBReadAccount2;
	}

	@Override
	public PSD2CustomerInfo retrieveCustomerInfo(String userId, Map<String, String> params) {

		return null;
	}
}
