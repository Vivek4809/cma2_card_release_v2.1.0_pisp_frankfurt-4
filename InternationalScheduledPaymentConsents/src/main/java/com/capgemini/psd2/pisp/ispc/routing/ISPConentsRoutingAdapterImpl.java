package com.capgemini.psd2.pisp.ispc.routing;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.GenericPaymentConsentResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalScheduledPaymentStagingAdapter;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component("ispConsentsStagingRoutingAdapter")
public class ISPConentsRoutingAdapterImpl
		implements InternationalScheduledPaymentStagingAdapter, ApplicationContextAware {

	private InternationalScheduledPaymentStagingAdapter beanInstance;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Value("${app.paymentSetupStagingAdapter}")
	private String beanName;

	/** The application context. */
	private ApplicationContext applicationContext;

	public InternationalScheduledPaymentStagingAdapter getRoutingInstance(String adapterName) {
		if (beanInstance == null)
			beanInstance = (InternationalScheduledPaymentStagingAdapter) applicationContext.getBean(adapterName);
		return beanInstance;

	}

	@Override
	public void setApplicationContext(ApplicationContext context) {
		this.applicationContext = context;
	}

	@Override
	public GenericPaymentConsentResponse processInternationalScheduledPaymentConsents(
			CustomISPConsentsPOSTRequest internationalScheduledPaymentRequest,
			CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
			OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
		return getRoutingInstance(beanName).processInternationalScheduledPaymentConsents(
				internationalScheduledPaymentRequest, customStageIdentifiers, addHeaderParams(params), successStatus,
				failureStatus);
	}

	@Override
	public CustomISPConsentsPOSTResponse retrieveStagedInternationalScheduledPaymentConsents(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
		return getRoutingInstance(beanName).retrieveStagedInternationalScheduledPaymentConsents(
				customPaymentStageIdentifiers, addHeaderParams(params));
	}

	private Map<String, String> addHeaderParams(Map<String, String> mapParam) {
		/*
		 * Below code will be used when update service will be called from
		 * payment submission API. For submission flow -> update, PSUId and
		 * ChannelName are mandatory fields for FS
		 */
		if (NullCheckUtils.isNullOrEmpty(mapParam))
			mapParam = new HashMap<>();

		if (null != reqHeaderAtrributes.getToken() && null != reqHeaderAtrributes.getToken().getSeviceParams()) {
			mapParam.put(PSD2Constants.CHANNEL_IN_REQ_HEADER,
					reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));
		}
		mapParam.put(PSD2Constants.USER_IN_REQ_HEADER, reqHeaderAtrributes.getPsuId());
		mapParam.put(PSD2Constants.CORRELATION_REQ_HEADER, reqHeaderAtrributes.getCorrelationId());
		mapParam.put(PSD2Constants.TENANT_ID, reqHeaderAtrributes.getTenantId());
		return mapParam;
	}

}
