package com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.delegate.InternationalPaymentConsentsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.transformer.InternationalPaymentConsentsFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

public class InternationalPaymentConsentsFoundationServiceDelegateTest {
	@InjectMocks
	InternationalPaymentConsentsFoundationServiceDelegate delegate;
	
	
	@Mock
	InternationalPaymentConsentsFoundationServiceTransformer transformer;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	
	
	@Test
	public void testcreatePaymentRequestHeadersPostNIGB(){
		ReflectionTestUtils.setField(delegate,"transactioReqHeader","transactioReqHeader");
		ReflectionTestUtils.setField(delegate,"correlationMuleReqHeader","correlation");
		ReflectionTestUtils.setField(delegate,"sourceUserReqHeader","sorceuser");
		ReflectionTestUtils.setField(delegate,"sourcesystem","source");
		ReflectionTestUtils.setField(delegate,"apiChannelCode","channel");
		ReflectionTestUtils.setField(delegate,"internationalPaymentConsentPostBaseURL","baseUrl");
		ReflectionTestUtils.setField(delegate,"systemApiVersion","baseUrl");
		ReflectionTestUtils.setField(delegate,"channelBrand","channelBrand");
		ReflectionTestUtils.setField(delegate,"internationalPaymentConsentSetupVersion","internationalPaymentConsentSetupVersion");
		
		Map<String, String> params=new HashMap<>();
		CustomPaymentStageIdentifiers stageIden=new CustomPaymentStageIdentifiers();
		params.put("transactioReqHeader", "boi");
		params.put("tenant_id", "BOIUK");
		params.put("sourceUserReqHeader", "sourceuser");
		params.put("apiChannelCode", "channel");
		params.put("X-BOI-CHANNEL", "channel");
		params.put("X-BOI-USER", "BOI");
		params.put("X-CORRELATION-ID", "Id");
		
		stageIden.setPaymentSetupVersion("3.0");
		HttpHeaders header=delegate.createPaymentRequestHeadersPost(stageIden, params);
		assertNotNull(header);
		
	
	}
	
	
	@Test
	public void testcreatePaymentRequestHeadersPostROI(){
		ReflectionTestUtils.setField(delegate,"transactioReqHeader","transactioReqHeader");
		ReflectionTestUtils.setField(delegate,"correlationMuleReqHeader","correlation");
		ReflectionTestUtils.setField(delegate,"sourceUserReqHeader","sorceuser");
		ReflectionTestUtils.setField(delegate,"sourcesystem","source");
		ReflectionTestUtils.setField(delegate,"apiChannelCode","channel");
		ReflectionTestUtils.setField(delegate,"internationalPaymentConsentPostBaseURL","baseUrl");
		ReflectionTestUtils.setField(delegate,"systemApiVersion","baseUrl");
		ReflectionTestUtils.setField(delegate,"channelBrand","channelBrand");
		ReflectionTestUtils.setField(delegate,"internationalPaymentConsentSetupVersion","internationalPaymentConsentSetupVersion");
		
		Map<String, String> params=new HashMap<>();
		
		CustomPaymentStageIdentifiers stageIden=new CustomPaymentStageIdentifiers();
		params.put("tenant_id", "BOIROI");
		stageIden.setPaymentSetupVersion("1.0");
		HttpHeaders header=delegate.createPaymentRequestHeadersPost(stageIden, params);
		assertNotNull(header);
	}
	
	@Test
	public void testgetPaymentFoundationServiceURL(){
		ReflectionTestUtils.setField(delegate,"internationalPaymentConsentPostBaseURL","baseUrl");
		ReflectionTestUtils.setField(delegate,"internationalPaymentConsentSetupVersion","internationalPaymentConsentSetupVersion");
		String paymentInstuctionProposalId="415641612";
		delegate.getPaymentFoundationServiceURL(paymentInstuctionProposalId);
	}
	
	@Test
	public void testpostPaymentFoundationServiceURL(){
		ReflectionTestUtils.setField(delegate,"internationalPaymentConsentPostBaseURL","baseUrl");
		ReflectionTestUtils.setField(delegate,"internationalPaymentConsentSetupVersion","internationalPaymentConsentSetupVersion");
		delegate.postPaymentFoundationServiceURL();
	}
	
	@Test
	public void testtransformInternationalConsentResponseFromAPIToFDForInsert(){
		CustomIPaymentConsentsPOSTRequest paymentConsentsRequest=new CustomIPaymentConsentsPOSTRequest();
		Map<String, String> params=new HashMap<>();
		params.put("transactioReqHeader", "boi");
		params.put("tenant_id", "BOIUK");
		delegate.transformInternationalConsentResponseFromAPIToFDForInsert(paymentConsentsRequest, params);
		
		params.put("tenant_id", "BOIROI");
		delegate.transformInternationalConsentResponseFromAPIToFDForInsert(paymentConsentsRequest, params);
	}
	
	@Test
	public void testtransformInternationalConsentResponseFromFDToAPIForInsert(){
		PaymentInstructionProposalInternational paymentInstructionProposalResponse=new PaymentInstructionProposalInternational();
		delegate.transformInternationalConsentResponseFromFDToAPIForInsert(paymentInstructionProposalResponse);
	}
}
