package com.capgemini.psd2.payment.setup.insert.update.mock.foundationservice.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Adapter to marshal and unmarshall data entered in xs:date field, as defined by XSD, in to java.util.Date and vice versa.
 * 
 * @author A970560
 *
 */
public class DateAdapter extends XmlAdapter<String, Date>{

	private static final String DATE_FORMAT = "yyyy-MM-dd";
	
	//@Loggable(LogLevel.INFO)
	public Date parseDate(String s) throws ParseException{
		SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
		return format.parse(s);
	}

	//@Loggable(LogLevel.INFO)
	public String printDate(Date dt) {
		SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
		return format.format(dt);
	}

	
	@Override
	public String marshal(Date v) throws Exception {
		return printDate(v);
	}

	@Override
	public Date unmarshal(String v) throws Exception {
		return parseDate(v);
	}
}
