package com.capgemini.psd2.revoke.consent.routing.impl;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.internal.apis.adapter.RevokeConsentAdapter;

@Component("revokeConsentRoutingAdapter")
public class RevokeConsentRoutingAdapter implements ApplicationContextAware, RevokeConsentAdapter {

	private ApplicationContext context;

	private RevokeConsentAdapter beanInstance;

	/** The default adapter. */
	@Value("${app.defaultAdapter}")
	private String defaultAdapter;

	@Override
	public void revokeConsentRequest(String consentId, String tppCID) {
		getRoutingInstance().revokeConsentRequest(consentId, tppCID);
	}

	private RevokeConsentAdapter getRoutingInstance() {
		if (beanInstance == null) {
			beanInstance = (RevokeConsentAdapter) context.getBean(defaultAdapter);
		}
		return beanInstance;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.context = applicationContext;
	}

}
