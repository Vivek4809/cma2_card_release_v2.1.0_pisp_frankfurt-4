package com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.capgemini.psd2.pisp.domain.OBCharge1;

public interface IPaymentConsentsChargesDataRepository extends MongoRepository<OBCharge1,String>{
	public List<OBCharge1> findAll();
}
