/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.rest.client.model;

import com.capgemini.psd2.logger.RequestHeaderAttributes;

/**
 * The Class RequestInfo.
 */
public class RequestInfo {
	
	/** The url. */
	private String url;
	
	/** The request header attributes. */
	private RequestHeaderAttributes requestHeaderAttributes;
	
	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	
	/**
	 * Sets the url.
	 *
	 * @param url the new url
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	
	/**
	 * Gets the request header attributes.
	 *
	 * @return the request header attributes
	 */
	public RequestHeaderAttributes getRequestHeaderAttributes() {
		return requestHeaderAttributes;
	}
	
	/**
	 * Sets the request header attributes.
	 *
	 * @param requestHeaderAttributes the new request header attributes
	 */
	public void setRequestHeaderAttributes(RequestHeaderAttributes requestHeaderAttributes) {
		this.requestHeaderAttributes = requestHeaderAttributes;
	}
	
}
