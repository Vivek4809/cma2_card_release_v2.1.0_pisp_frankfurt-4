package com.capgemini.psd2.customer.account.profile.mock.foundationservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.customer.account.profile.mock.foundationservice.domain.DigitalUserProfile;


public interface CustomerAccountInformationRepository extends MongoRepository<DigitalUserProfile,String> {

	public DigitalUserProfile findByDigitalUserProfileID(String Id);

}
