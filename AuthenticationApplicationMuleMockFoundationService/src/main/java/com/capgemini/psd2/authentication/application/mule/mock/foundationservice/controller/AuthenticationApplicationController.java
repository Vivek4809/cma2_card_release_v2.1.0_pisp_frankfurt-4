package com.capgemini.psd2.authentication.application.mule.mock.foundationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.Login;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.LoginResponse;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.service.AuthenticationApplicationService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.utilities.NullCheckUtils;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;

@RestController
public class AuthenticationApplicationController {

	@Autowired
	private AuthenticationApplicationService authenticationApplicationService;
	
	@Autowired
	private ValidationUtility validationUtility;


	/* New BOL */
	@RequestMapping(value = "/mocking/api/v1/links/29932b82-4b1a-43e4-8208-5c5349f9738e/it-boi/p/authentication/v2.0/digital-user/authentication", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public ResponseEntity<LoginResponse> authenticatenewBOLUser(@RequestBody Login login,
			@RequestHeader(required = false, value = "x-api-source-user") String boiUser,
			@RequestHeader(required = false, value = "x-api-source-system") String boiPlatform,
			@RequestHeader(required = false, value = "x-api-correlation-id") String correlationID,
			@RequestHeader(required = false, value = "x-api-channel-brand") String apichannelBrand,
			@RequestHeader(required = false, value = "x-api-channel-code") String apiChannelCode,
			@RequestHeader(required = false, value = "x-api-transaction-id") String transanctionID) throws Exception {

		if (NullCheckUtils.isNullOrEmpty(boiPlatform)) {
			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PPR_AUT);
		}

		if (NullCheckUtils.isNullOrEmpty(login.getDigitalUser().getCustomerAuthenticationSession())) {
			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PPR_AUT);
		}
		validationUtility.sca_authentication_errorcode_Validation(login.getDigitalUser().getDigitalUserIdentifier()); 
		LoginResponse loginResponse = authenticationApplicationService.validatenewBolUser(login);
		return new ResponseEntity<>(loginResponse, HttpStatus.CREATED);

	}

}
