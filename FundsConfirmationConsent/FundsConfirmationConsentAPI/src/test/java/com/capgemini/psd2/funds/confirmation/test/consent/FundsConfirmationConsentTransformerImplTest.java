package com.capgemini.psd2.funds.confirmation.test.consent;


import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.cisp.adapter.FundsConfirmationConsentAdapter;
import com.capgemini.psd2.cisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsent1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentData1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentDataResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.funds.confirmation.consent.FundsConfirmationConsentApplication;
import com.capgemini.psd2.funds.confirmation.transformer.FundsConfirmationConsentTransformerImpl;
import com.capgemini.psd2.token.TPPInformation;
import com.capgemini.psd2.token.Token;


@RunWith(SpringJUnit4ClassRunner.class)
public class FundsConfirmationConsentTransformerImplTest {

	@InjectMocks
	public FundsConfirmationConsentTransformerImpl FundsConfirmationConsentTrans;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	
	
	@Test
	public void test() {
		OBFundsConfirmationConsentResponse1 OBFundsConfirmationConsentResponse=FundsConfirmationConsentTrans.fundsConfirmationConsentResponseTransformer(getFundConfirm());
		assertEquals("123456",OBFundsConfirmationConsentResponse.getData().getConsentId());
		}
	
	@Test
	public void test1() {
		
		TPPInformation tppInfo = new TPPInformation();
		tppInfo.setTppLegalEntityName("entityname");
		tppInfo.setTppRegisteredId("registeredId");
		tppInfo.setTppBlock("false");
		Token token = new Token();
		token.setTppInformation(tppInfo);
		OBFundsConfirmationConsent1 fundsConfirmationConsentPOSTRequest = new OBFundsConfirmationConsent1();
		OBFundsConfirmationConsentData1 data = new OBFundsConfirmationConsentData1();
		//fundsConfirmationConsentPOSTRequest = commonCardValidations.validateRequestParams(fundsConfirmationConsentPOSTRequest);
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setIdentification("12345");
		debtorAccount.setSchemeName("hj");
		debtorAccount.setName("pqr");
		data.setDebtorAccount(debtorAccount);
		data.setExpirationDateTime("2020-01-19T00:00:00+05:30");
		 fundsConfirmationConsentPOSTRequest.setData(data);
		 
		 
		 OBFundsConfirmationConsentResponse1 obFundsConfirmationConsentResponse1= new OBFundsConfirmationConsentResponse1();
		 OBFundsConfirmationConsentDataResponse1 obFundsConfirmationConsentDataResponse1=new OBFundsConfirmationConsentDataResponse1();
		 obFundsConfirmationConsentResponse1.setData(obFundsConfirmationConsentDataResponse1);
		 obFundsConfirmationConsentDataResponse1.setConsentId("123456");
		 obFundsConfirmationConsentDataResponse1.setCreationDateTime("kjkjn");
		 obFundsConfirmationConsentDataResponse1.setDebtorAccount(debtorAccount);
		OBFundsConfirmationConsentResponse1 OBFundsConfirmationConsentResponse=FundsConfirmationConsentTrans.fundsConfirmationConsentResponseTransformer(getFundConfirm());
		assertEquals("123456",OBFundsConfirmationConsentResponse.getData().getConsentId());
		}
	
	
	@Test
	public void test2() {
		
		TPPInformation tppInfo = new TPPInformation();
		tppInfo.setTppLegalEntityName("entityname");
		tppInfo.setTppRegisteredId("registeredId");
		tppInfo.setTppBlock("false");
		Token token = new Token();
		token.setTppInformation(tppInfo);
		OBFundsConfirmationConsent1 fundsConfirmationConsentPOSTRequest = new OBFundsConfirmationConsent1();
		OBFundsConfirmationConsentData1 data = new OBFundsConfirmationConsentData1();
		//fundsConfirmationConsentPOSTRequest = commonCardValidations.validateRequestParams(fundsConfirmationConsentPOSTRequest);
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setIdentification("12345");
		debtorAccount.setSchemeName("hj");
		debtorAccount.setName("pqr");
		data.setDebtorAccount(debtorAccount);
		data.setExpirationDateTime("2020-01-19T00:00:00+05:30");
		 fundsConfirmationConsentPOSTRequest.setData(data);
		 
		 
		 OBFundsConfirmationConsentResponse1 obFundsConfirmationConsentResponse1= new OBFundsConfirmationConsentResponse1();
		 OBFundsConfirmationConsentDataResponse1 obFundsConfirmationConsentDataResponse1=new OBFundsConfirmationConsentDataResponse1();
		 obFundsConfirmationConsentResponse1.setData(obFundsConfirmationConsentDataResponse1);
		 obFundsConfirmationConsentDataResponse1.setConsentId("123456");
		 obFundsConfirmationConsentDataResponse1.setCreationDateTime("kjkjn");
		 obFundsConfirmationConsentDataResponse1.setDebtorAccount(null);
		OBFundsConfirmationConsentResponse1 OBFundsConfirmationConsentResponse=FundsConfirmationConsentTrans.fundsConfirmationConsentResponseTransformer(obFundsConfirmationConsentResponse1);
		assertEquals("123456",OBFundsConfirmationConsentResponse.getData().getConsentId());
		}
	
	
	
	public OBFundsConfirmationConsentResponse1 getFundConfirm()
	{
		TPPInformation tppInfo = new TPPInformation();
		tppInfo.setTppLegalEntityName("entityname");
		tppInfo.setTppRegisteredId("registeredId");
		tppInfo.setTppBlock("false");
		Token token = new Token();
		token.setTppInformation(tppInfo);
		OBFundsConfirmationConsent1 fundsConfirmationConsentPOSTRequest = new OBFundsConfirmationConsent1();
		OBFundsConfirmationConsentData1 data = new OBFundsConfirmationConsentData1();
		//fundsConfirmationConsentPOSTRequest = commonCardValidations.validateRequestParams(fundsConfirmationConsentPOSTRequest);
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setIdentification("12345");
		debtorAccount.setSchemeName("IBAN");
		debtorAccount.setName("pqr");
		data.setDebtorAccount(debtorAccount);
		data.setExpirationDateTime("2020-01-19T00:00:00+05:30");
		 fundsConfirmationConsentPOSTRequest.setData(data);
		 
		 
		 OBFundsConfirmationConsentResponse1 obFundsConfirmationConsentResponse1= new OBFundsConfirmationConsentResponse1();
		 OBFundsConfirmationConsentDataResponse1 obFundsConfirmationConsentDataResponse1=new OBFundsConfirmationConsentDataResponse1();
		 obFundsConfirmationConsentResponse1.setData(obFundsConfirmationConsentDataResponse1);
		 obFundsConfirmationConsentDataResponse1.setConsentId("123456");
		 obFundsConfirmationConsentDataResponse1.setCreationDateTime("kjkjn");
		 obFundsConfirmationConsentDataResponse1.setDebtorAccount(debtorAccount);
		 
		 return obFundsConfirmationConsentResponse1;
	}
}
