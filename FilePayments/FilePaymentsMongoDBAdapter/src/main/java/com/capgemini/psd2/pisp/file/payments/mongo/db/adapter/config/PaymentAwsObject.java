package com.capgemini.psd2.pisp.file.payments.mongo.db.adapter.config;
import org.springframework.stereotype.Component;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

@Component
public class PaymentAwsObject {
	
	public S3Object getPaymentAwsObjectS3Object(AmazonS3 s3client,String awsS3BucketName,String fileName){
			
		S3Object fullObject = s3client.getObject(new GetObjectRequest(awsS3BucketName, fileName));
		return fullObject;
	}
	
	public AmazonS3 getPaymentAwsClient(BasicAWSCredentials awsCredentials,ClientConfiguration clientConfig,String awsRegion){
			
		 AmazonS3 s3client=AmazonS3ClientBuilder.standard()
					.withCredentials(DefaultAWSCredentialsProviderChain.getInstance())
					.withClientConfiguration(clientConfig).withRegion(awsRegion).build();
		return s3client;
	}
	
}
