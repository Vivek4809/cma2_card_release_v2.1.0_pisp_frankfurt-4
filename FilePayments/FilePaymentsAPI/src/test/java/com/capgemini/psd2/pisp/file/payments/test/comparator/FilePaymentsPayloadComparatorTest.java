package com.capgemini.psd2.pisp.file.payments.test.comparator;

import java.math.BigDecimal;

import org.aspectj.lang.annotation.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBFile1;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBWriteDataFile1;
import com.capgemini.psd2.pisp.domain.OBWriteDataFileConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataFileConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.file.payments.comparator.FilePaymentsPayloadComparator;

@RunWith(SpringJUnit4ClassRunner.class)
public class FilePaymentsPayloadComparatorTest {

	private MockMvc mockMvc;

	@InjectMocks
	private FilePaymentsPayloadComparator comparator;

	@Before(value = "")
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(comparator).dispatchOptions(true).build();
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testcompare() throws Exception {
		CustomFilePaymentConsentsPOSTResponse response1 = new CustomFilePaymentConsentsPOSTResponse();
		CustomFilePaymentConsentsPOSTResponse response2 = new CustomFilePaymentConsentsPOSTResponse();
		OBWriteDataFileConsentResponse1 data = new OBWriteDataFileConsentResponse1();
		BigDecimal controlSum = new BigDecimal("7.00");
		OBFile1 initiation = new OBFile1();
		OBWriteDataFileConsentResponse1 data1 = new OBWriteDataFileConsentResponse1();
		OBFile1 initiation1 = new OBFile1();
		
		initiation.setControlSum(controlSum);
		initiation1.setControlSum(controlSum);
		data.setConsentId("123455");
		data.setInitiation(initiation);
		initiation.setLocalInstrument("12");
		response1.setData(data);
		response2.setData(data1);
		data1.setConsentId("123455");
		data1.setInitiation(initiation1);
		initiation1.setLocalInstrument("12");

		comparator.compare(response1, response2);
	}

	@Test
	public void testComparePaymentDetails() throws Exception {
		CustomFilePaymentConsentsPOSTResponse response = new CustomFilePaymentConsentsPOSTResponse();
		CustomFilePaymentSetupPOSTRequest request = new CustomFilePaymentSetupPOSTRequest();
		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		CustomFilePaymentConsentsPOSTResponse adaptedPaymentConsentsResponse = new CustomFilePaymentConsentsPOSTResponse();
		OBWriteDataFileConsent1 dataSetup = new OBWriteDataFileConsent1();
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		OBWriteDataFileConsentResponse1 data = new OBWriteDataFileConsentResponse1();
		OBFile1 initiation = new OBFile1();
		
		BigDecimal controlSum=new BigDecimal("234.00");
		initiation.setControlSum(controlSum);
		data.setConsentId("1234");
		initiation.setRemittanceInformation(remittanceInformation);
		dataSetup.setInitiation(initiation);
		data.setInitiation(initiation);
		request.setData(dataSetup);
		response.setData(data);
		adaptedPaymentConsentsResponse.setData(data);

		comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
	}

	@Test
	public void testComparePaymentDetailsRequest() throws Exception {
		CustomFilePaymentConsentsPOSTResponse response = new CustomFilePaymentConsentsPOSTResponse();
		CustomFilePaymentsPOSTRequest request = new CustomFilePaymentsPOSTRequest();
		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		CustomFilePaymentConsentsPOSTResponse adaptedPaymentConsentsResponse = new CustomFilePaymentConsentsPOSTResponse();

		OBWriteDataFileConsentResponse1 data = new OBWriteDataFileConsentResponse1();
		data.setConsentId("1234");
		OBFile1 initiation = new OBFile1();
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setName("test");
		initiation.setDebtorAccount(debtorAccount);
		data.setInitiation(initiation);
		response.setData(data);
		paymentSetupPlatformResource.setTppDebtorDetails("Test1");
		OBWriteDataFile1 dataSetup = new OBWriteDataFile1();
		dataSetup.setInitiation(initiation);
		request.setData(dataSetup);
		adaptedPaymentConsentsResponse.setData(data);

		comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
	}

	@Test
	public void testComparePaymentDetailsDebitorEqual() throws Exception {
		CustomFilePaymentConsentsPOSTResponse response = new CustomFilePaymentConsentsPOSTResponse();
		CustomFilePaymentSetupPOSTRequest request = new CustomFilePaymentSetupPOSTRequest();
		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		CustomFilePaymentConsentsPOSTResponse adaptedPaymentConsentsResponse = new CustomFilePaymentConsentsPOSTResponse();
		OBWriteDataFileConsent1 dataSetup = new OBWriteDataFileConsent1();
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		OBWriteDataFileConsentResponse1 data = new OBWriteDataFileConsentResponse1();
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		OBFile1 initiation = new OBFile1();
		BigDecimal controlSum = new BigDecimal("7.00");

		initiation.setControlSum(controlSum);
		data.setConsentId("1234");
		initiation.setRemittanceInformation(remittanceInformation);
		initiation.setDebtorAccount(debtorAccount);
		dataSetup.setInitiation(initiation);
		data.setInitiation(initiation);
		request.setData(dataSetup);
		response.setData(data);
		adaptedPaymentConsentsResponse.setData(data);
		paymentSetupPlatformResource.setTppDebtorDetails(Boolean.toString(true));
		paymentSetupPlatformResource.setTppDebtorNameDetails(Boolean.toString(false));
		comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
	}

	@Test
	public void testComparePaymentDetailsDebitorTrue() throws Exception {
		CustomFilePaymentConsentsPOSTResponse response = new CustomFilePaymentConsentsPOSTResponse();
		CustomFilePaymentSetupPOSTRequest request = new CustomFilePaymentSetupPOSTRequest();
		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		CustomFilePaymentConsentsPOSTResponse adaptedPaymentConsentsResponse = new CustomFilePaymentConsentsPOSTResponse();
		OBWriteDataFileConsent1 dataSetup = new OBWriteDataFileConsent1();
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		OBWriteDataFileConsentResponse1 data = new OBWriteDataFileConsentResponse1();
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		OBFile1 initiation1 = new OBFile1();
		OBFile1 initiation2 = new OBFile1();
		
		BigDecimal controlSum=new BigDecimal("23.00");
		initiation2.setControlSum(controlSum);
		initiation1.setControlSum(controlSum);
		data.setConsentId("1234");
		initiation1.setRemittanceInformation(remittanceInformation);
		initiation2.setDebtorAccount(debtorAccount);
		initiation2.setRemittanceInformation(remittanceInformation);
		dataSetup.setInitiation(initiation1);
		data.setInitiation(initiation2);
		request.setData(dataSetup);
		response.setData(data);
		adaptedPaymentConsentsResponse.setData(data);
		paymentSetupPlatformResource.setTppDebtorDetails(Boolean.toString(false));

		comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
	}
}
