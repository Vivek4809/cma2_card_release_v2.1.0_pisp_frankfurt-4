package com.capgemini.psd2.payment.validation.mock.foundationservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.payment.validation.mock.foundationservice.domain.PaymentInstruction;

public interface PaymentValidationRepository extends MongoRepository<PaymentInstruction,String> {

	public PaymentInstruction findByPaymentPaymentId(String paymentId);
	
}
