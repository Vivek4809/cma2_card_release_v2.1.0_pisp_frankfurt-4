
package com.capgemini.psd2.foundationservice.authentication.application.service.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServicePostRequest;
import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServicePostResponse;
import com.capgemini.psd2.adapter.security.domain.Login;
import com.capgemini.psd2.adapter.security.domain.LoginResponse;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.foundationservice.authentication.application.service.boi.adapter.client.SCAAuthenticationServiceFoundationServiceClient;
import com.capgemini.psd2.foundationservice.authentication.application.service.boi.adapter.delegate.SCAAuthenticationServiceFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.authentication.application.service.boi.adapter.transformer.SCAAuthenticationServiceFoundationServiceTransformer;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@Component
public class SCAAuthenticationServiceFoundationServiceAdapter{

	@Autowired
	private AdapterUtility adapterUtility;

	@Autowired
	private SCAAuthenticationServiceFoundationServiceDelegate authenticationApplicationFoundationServiceDelegate;

	@Autowired
	private SCAAuthenticationServiceFoundationServiceClient authenticationApplicationFoundationServiceClient;
	
	@Autowired
	private SCAAuthenticationServiceFoundationServiceTransformer scaAuthenticationServiceFoundationServiceTransformer;
	
	
	public CustomAuthenticationServicePostResponse retrieveAuthenticationService(CustomAuthenticationServicePostRequest request, Map<String, Object> params){
		
		RequestInfo requestInfo = new RequestInfo();
		String finalURL = authenticationApplicationFoundationServiceDelegate
				.postAuthenticationFoundationServiceURL();
		requestInfo.setUrl(finalURL);
		HttpHeaders httpHeaders = authenticationApplicationFoundationServiceDelegate.createRequestHeaders(requestInfo, params);
		Login loginRequest = scaAuthenticationServiceFoundationServiceTransformer.transformAuthenticationServiceRequest(request,params);
		LoginResponse loginResponse = authenticationApplicationFoundationServiceClient.restTransportForAuthenticationServicePOST(requestInfo, loginRequest, LoginResponse.class, httpHeaders);
		
		return scaAuthenticationServiceFoundationServiceTransformer.transformAuthenticationServiceResponse(loginResponse);
	}
}
