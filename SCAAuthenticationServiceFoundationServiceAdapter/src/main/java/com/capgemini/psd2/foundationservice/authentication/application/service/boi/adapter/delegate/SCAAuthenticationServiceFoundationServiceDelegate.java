
package com.capgemini.psd2.foundationservice.authentication.application.service.boi.adapter.delegate;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import com.capgemini.psd2.adapter.security.constants.AdapterSecurityConstants;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class SCAAuthenticationServiceFoundationServiceDelegate {

	@Value("${foundationService.sourceUserInReqHeader:#{X-API-SOURCE-USER}}")
	private String sourceUserInReqHeader;

	@Value("${foundationService.sourceSystemInReqHeader:#{X-API-SOURCE-SYSTEM}}")
	private String sourceSystemInReqHeader;

	@Value("${foundationService.trasanctionIdInReqHeader:#{X-API-TRANSANCTION-ID}}")
	private String trasanctionIdInReqHeader;

	@Value("${foundationService.apiCorrelationIdInReqHeader:#{X-API-CORRELATION-ID}}")
	private String apiCorrelationIdInReqHeader;
	
	@Value("${foundationService.authenticationServicePostBaseURL}")
	private String authenticationServicePostBaseURL;

	@Value("${foundationService.authenticationServicePostVersion}")
	private String authenticationServicePostVersion;
	
	@Value("${foundationService.apichannelBrand:#{x-api-channel-brand}}")
	private String apichannelBrand;
	
	@Value("${foundationService.apichannelCode:#{x-api-channel-code}}")
	private String apichannelCode;

	public HttpHeaders createRequestHeaders(RequestInfo requestInfo, Map<String, Object> params) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(sourceUserInReqHeader, String.valueOf(params.get(AdapterSecurityConstants.USER_HEADER)));
		httpHeaders.add(sourceSystemInReqHeader, AdapterSecurityConstants.SOURCE_SYSTEM_HEADER_VALUE);
		httpHeaders.add(trasanctionIdInReqHeader, String.valueOf(params.get(PSD2Constants.CORRELATION_ID)));
		httpHeaders.add(apiCorrelationIdInReqHeader, "");
		httpHeaders.add(apichannelBrand, String.valueOf(params.get(AdapterSecurityConstants.CHANNEL_BRAND)));
		if(!NullCheckUtils.isNullOrEmpty(params.get(AdapterSecurityConstants.CHANNELCODE)))
			httpHeaders.add(apichannelCode, String.valueOf(params.get(AdapterSecurityConstants.CHANNELCODE)));
		httpHeaders.add("Content-Type", "application/json");
		return httpHeaders;
	}
	
	public String postAuthenticationFoundationServiceURL() {
		return authenticationServicePostBaseURL + "/" + authenticationServicePostVersion + "/"
				+ "digital-user/authentication";
	}
}
