package com.capgemini.psd2.revoke.consent.mongo.db.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.aisp.domain.OBReadDataResponse1;

public interface RevokeAccountRequestRepository extends MongoRepository<OBReadDataResponse1, String> {

	public OBReadDataResponse1 findByAccountRequestId(String accountId);
	
}
