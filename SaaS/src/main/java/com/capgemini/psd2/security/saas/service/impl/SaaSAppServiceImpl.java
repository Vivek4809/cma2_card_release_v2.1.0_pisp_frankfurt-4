package com.capgemini.psd2.security.saas.service.impl;

import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.naming.directory.BasicAttributes;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.adapter.exceptions.security.AdapterAuthenticationException;
import com.capgemini.psd2.adapter.exceptions.security.SecurityAdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.security.constants.AdapterSecurityConstants;
import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServiceGetResponse;
import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServicePostRequest;
import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServicePostResponse;
import com.capgemini.psd2.adapter.security.domain.AuthenticationParameters;
import com.capgemini.psd2.adapter.security.domain.BrandCode3;
import com.capgemini.psd2.adapter.security.domain.EventType1;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.foundationservice.authentication.application.retrieve.boi.adapter.SCAAuthenticationRetrieveFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.authentication.application.service.boi.adapter.SCAAuthenticationServiceFoundationServiceAdapter;
import com.capgemini.psd2.fraudsystem.constants.FraudSystemConstants;
import com.capgemini.psd2.fraudsystem.helper.FraudSystemHelper;
import com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestMapping;
import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.sca.consent.operations.adapter.PispScaConsentOperationsAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.exceptions.PSD2AuthenticationException;
import com.capgemini.psd2.security.exceptions.SCAConsentErrorCodeEnum;
import com.capgemini.psd2.security.saas.constants.SaaSAppConstants;
import com.capgemini.psd2.security.saas.model.IntentTypeEnum;
import com.capgemini.psd2.security.saas.service.SaaSAppService;
import com.capgemini.psd2.security.saas.utility.SaaSAppUtility;
import com.capgemini.psd2.tppinformation.adaptor.ldap.constants.TPPInformationConstants;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.utilities.SandboxConfig;

@Service
public class SaaSAppServiceImpl implements SaaSAppService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SaaSAppServiceImpl.class);

	@Autowired
	private AispConsentAdapter aispConsentAdapter;
	
	@Autowired
	private TPPInformationAdaptor tppInformationAdaptor;

	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;

	@Autowired
	private SCAAuthenticationServiceFoundationServiceAdapter authenticationPOSTAdapter;

	@Autowired
	private SCAAuthenticationRetrieveFoundationServiceAdapter authenticationGETAdapter;

	@Autowired
	private SaaSAppUtility saaSAppUtility;

	@Autowired
	private SandboxConfig sandboxConfig;

	@Autowired
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;

	@Autowired
	@Qualifier("pispScaConsentOperationsRoutingAdapter")
	private PispScaConsentOperationsAdapter pispStageOperationsAdapter;
	
	@Autowired
	private FraudSystemHelper fraudSystemHelper;

	@Autowired
	private HttpServletRequest request;

	private PickupDataModel pickUpDataModel;

	@Override
	public CustomAuthenticationServiceGetResponse authenticateDOB(
			CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest) {

		pickUpDataModel = (PickupDataModel) request.getAttribute(SCAConsentHelperConstants.INTENT_DATA);

		LOGGER.info("Entering service for submission of date of birth details"
				+ customAuthenticationServicePostRequest.toString());
		CustomAuthenticationServicePostResponse customAuthenticationServicePostResponse = new CustomAuthenticationServicePostResponse();
		CustomAuthenticationServiceGetResponse customAuthenticationServiceGetResponse = new CustomAuthenticationServiceGetResponse();
		
			if (Boolean.valueOf(sandboxConfig.isSandboxEnabled())) {
				if (NullCheckUtils.isNullOrEmpty(
						customAuthenticationServicePostRequest.getDigitalUser().getDigitalUserIdentifier())) {
					throw PSD2Exception.populatePSD2Exception("UserName is blank or invalid",
							ErrorCodeEnum.TECHNICAL_ERROR);
				}
			} else {
				if (NullCheckUtils.isNullOrEmpty(
						customAuthenticationServicePostRequest.getDigitalUser().getDigitalUserIdentifier())
						|| NullCheckUtils.isNullOrEmpty(customAuthenticationServicePostRequest.getDigitalUser()
								.getCustomerAuthenticationSession().get(0).getSecureAccessKeyUsed().get(0).getValue())
				/*
				 * || !saaSAppUtility.validUserAndPassword(customDOBPostRequest.
				 * getUserid())
				 */) {
					throw PSD2Exception.populatePSD2Exception("UserName is blank or invalid",
							ErrorCodeEnum.TECHNICAL_ERROR);
				}
			}
			AispConsent consent = aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(
					requestHeaderAttributes.getIntentId(), ConsentStatusEnum.AUTHORISED);
			if (consent != null && !consent.getPsuId().equalsIgnoreCase(
					customAuthenticationServicePostRequest.getDigitalUser().getDigitalUserIdentifier())) {
				throw PSD2AuthenticationException.populateAuthenticationFailedException(
						SCAConsentErrorCodeEnum.UN_AUTHORIZED_USER_REFRESH_TOKEN_FLOW);
			}
			requestHeaderAttributes
					.setPsuId(customAuthenticationServicePostRequest.getDigitalUser().getDigitalUserIdentifier());

			Map<String, Object> params = new HashMap<String, Object>();
			params.put(PSD2Constants.CORRELATION_ID, requestHeaderAttributes.getCorrelationId());
			params.put(AdapterSecurityConstants.USER_HEADER,
					customAuthenticationServicePostRequest.getDigitalUser().getDigitalUserIdentifier());
			params.put(AdapterSecurityConstants.CHANNELCODE, requestHeaderAttributes.getChannelId().toUpperCase());
			if(pickUpDataModel.getTenant_id().equalsIgnoreCase("BOIROI"))
				params.put(AdapterSecurityConstants.CHANNEL_BRAND,BrandCode3.ROI.getValue());
			else
				params.put(AdapterSecurityConstants.CHANNEL_BRAND,BrandCode3.NIGB.getValue());
			
			customAuthenticationServicePostResponse = authenticationPOSTAdapter
					.retrieveAuthenticationService(customAuthenticationServicePostRequest, params);
			
			if (NullCheckUtils.isNullOrEmpty(customAuthenticationServicePostResponse)){
				throw PSD2Exception.populatePSD2Exception("User data not found",
						ErrorCodeEnum.TECHNICAL_ERROR);
			}

			if (!customAuthenticationServicePostResponse.getLoginResponse().getDigitalUserSession()
					.isSessionInitiationFailureIndicator()) {
				customAuthenticationServiceGetResponse = retrievePINandListOfDevices(
						customAuthenticationServicePostRequest.getDigitalUser().getDigitalUserIdentifier());
				customAuthenticationServiceGetResponse
						.setSessionInitiationFailureIndicator(customAuthenticationServicePostResponse.getLoginResponse()
								.getDigitalUserSession().isSessionInitiationFailureIndicator());
			} else {
				customAuthenticationServiceGetResponse
						.setSessionInitiationFailureIndicator(customAuthenticationServicePostResponse.getLoginResponse()
								.getDigitalUserSession().isSessionInitiationFailureIndicator());
				AuthenticationParameters authenticationParameters = new AuthenticationParameters();
				authenticationParameters
						.setDigitalUser(customAuthenticationServicePostResponse.getLoginResponse().getDigitalUser());
				customAuthenticationServiceGetResponse.setAuthenticationParameters(authenticationParameters);
			}
		

		LOGGER.info("Exiting service for submission of date of birth details"
				+ customAuthenticationServiceGetResponse.toString());
		return customAuthenticationServiceGetResponse;
	}

	@Override
	public CustomAuthenticationServiceGetResponse retrievePINandListOfDevices(String username) {

		LOGGER.info("Entering service for retrieval of pin and list of devices" + username);

		CustomAuthenticationServiceGetResponse customAuthenticationServiceGetResponse = new CustomAuthenticationServiceGetResponse();
		
			if (Boolean.valueOf(sandboxConfig.isSandboxEnabled())) {
				if (NullCheckUtils.isNullOrEmpty(username)) {
					throw PSD2AuthenticationException.populateAuthenticationFailedException(
							SCAConsentErrorCodeEnum.VALIDATION_ERROR, "UserName is blank or invalid");
				}
			} else {
				if (NullCheckUtils.isNullOrEmpty(username)
				/* || !saaSAppUtility.validUserAndPassword(username,dob) */) {
					throw PSD2AuthenticationException.populateAuthenticationFailedException(
							SCAConsentErrorCodeEnum.VALIDATION_ERROR, "UserName or Password is blank or invalid");
				}
			}

			requestHeaderAttributes.setPsuId(username);

			// Call for retrieval of Payment Type
			EventType1 eventType = null;
			if (pickUpDataModel.getIntentTypeEnum().getIntentType()
					.equals(IntentTypeEnum.PISP_INTENT_TYPE.getIntentType())) {
				CustomPaymentStageIdentifiers stageIdentifiers = paymentSetupPlatformAdapter
						.populateStageIdentifiers(pickUpDataModel.getIntentId());

				if (stageIdentifiers.getPaymentTypeEnum().getPaymentType().toString()
						.equalsIgnoreCase("DomesticPayments")) {
					if (pickUpDataModel.getTenant_id().equalsIgnoreCase("BOIROI"))
						eventType = EventType1.SEPA_PAYMENT_NON_TRUSTED_PAYEE;
					else
						eventType = EventType1.UK_DOMESTIC_PAYMENT_NON_TRUSTED_PAYEE;

				} else if (stageIdentifiers.getPaymentTypeEnum().getPaymentType().toString()
						.equalsIgnoreCase("DomesticScheduledPayments")) {
					if (pickUpDataModel.getTenant_id().equalsIgnoreCase("BOIROI"))
						eventType = EventType1.SCHEDULED_SEPA_PAYMENT_NON_TRUSTED_PAYEE;
					else
						eventType = EventType1.SCHEDULED_UK_DOMESTIC_PAYMENT_NON_TRUSTED_PAYEE;
				}
			} else if (pickUpDataModel.getIntentTypeEnum().getIntentType()
					.equals(IntentTypeEnum.AISP_INTENT_TYPE.getIntentType())) {
				eventType = EventType1.ACCOUNT_INFORMATION_CONSENT;

			}


			Map<String, Object> params = new HashMap<String, Object>();
			params.put(PSD2Constants.CORRELATION_ID, requestHeaderAttributes.getCorrelationId());
			params.put(AdapterSecurityConstants.USER_HEADER, username);
			params.put(AdapterSecurityConstants.CHANNELCODE, requestHeaderAttributes.getChannelId().toUpperCase());
			params.put(AdapterSecurityConstants.EVENTTYPE, eventType.toString());
			
			if(pickUpDataModel.getTenant_id().equalsIgnoreCase("BOIROI"))
				params.put(AdapterSecurityConstants.CHANNEL_BRAND,BrandCode3.ROI.getValue());
			else
				params.put(AdapterSecurityConstants.CHANNEL_BRAND,BrandCode3.NIGB.getValue());
			customAuthenticationServiceGetResponse = authenticationGETAdapter.getAuthenticate(params);
		

		LOGGER.info(
				"Exiting service for retrieval of pin and list of devices" + customAuthenticationServiceGetResponse);
		return customAuthenticationServiceGetResponse;

	}

	@Override
	public CustomAuthenticationServicePostResponse submitPINDetailsandSelectedDevices(
			CustomAuthenticationServicePostRequest postRequest)  {

		LOGGER.info("Entering service for submission of device and pin" + postRequest.toString());

		CustomAuthenticationServicePostResponse customAuthenticationServicePostResponse = new CustomAuthenticationServicePostResponse();
		
			if (Boolean.valueOf(sandboxConfig.isSandboxEnabled())) {
				if (NullCheckUtils.isNullOrEmpty(postRequest.getDigitalUser().getDigitalUserIdentifier())) {
					throw PSD2AuthenticationException.populateAuthenticationFailedException(
							SCAConsentErrorCodeEnum.VALIDATION_ERROR, "UserName is blank or invalid");
				}
			} else {
				if (NullCheckUtils.isNullOrEmpty(postRequest.getDigitalUser().getDigitalUserIdentifier())
				/* || !saaSAppUtility.validUserAndPassword(username,dob) */) {
					throw PSD2AuthenticationException.populateAuthenticationFailedException(
							SCAConsentErrorCodeEnum.VALIDATION_ERROR, "UserName or Password is blank or invalid");
				}
			}

			requestHeaderAttributes.setPsuId(postRequest.getDigitalUser().getDigitalUserIdentifier());

			Map<String, Object> params = new HashMap<String, Object>();
			params.put(PSD2Constants.CORRELATION_ID, requestHeaderAttributes.getCorrelationId());
			params.put(AdapterSecurityConstants.USER_HEADER, postRequest.getDigitalUser().getDigitalUserIdentifier());
			params.put(AdapterSecurityConstants.CHANNELCODE, requestHeaderAttributes.getChannelId().toUpperCase());
			params.put("intentType", pickUpDataModel.getIntentTypeEnum().getIntentType());
			if(pickUpDataModel.getTenant_id().equalsIgnoreCase("BOIROI"))
				params.put(AdapterSecurityConstants.CHANNEL_BRAND,BrandCode3.ROI.getValue());
			else
				params.put(AdapterSecurityConstants.CHANNEL_BRAND,BrandCode3.NIGB.getValue());
			
		/*
		 * if (pickUpDataModel.getIntentTypeEnum().getIntentType()
		 * .equals(IntentTypeEnum.AISP_INTENT_TYPE.getIntentType())) {
		 * 
		 * String tppApplicationName =
		 * tppInformationAdaptor.fetchApplicationName(pickUpDataModel.getClientId());
		 * Object tppInformationObj =
		 * tppInformationAdaptor.fetchTPPInformation(pickUpDataModel.getClientId());
		 * 
		 * 
		 * if (!NullCheckUtils.isNullOrEmpty(tppApplicationName)) {
		 * params.put(SaaSAppConstants.TPP_NAME, tppApplicationName);
		 * LOGGER.info("Application_NAME" + tppApplicationName); }
		 * 
		 * else { throw
		 * PSD2Exception.populatePSD2Exception("Could not fetch TPP name or details.",
		 * ErrorCodeEnum.TECHNICAL_ERROR); }
		 * 
		 * 
		 * if (tppInformationObj != null && tppInformationObj instanceof
		 * BasicAttributes) { BasicAttributes tppInfoAttributes = (BasicAttributes)
		 * tppInformationObj; String legalEntityName =
		 * returnLegalEntityName(tppInfoAttributes);
		 * params.put(SaaSAppConstants.TPP_OBJECT, legalEntityName);
		 * LOGGER.info("TPP_NAME" + legalEntityName); } else { throw
		 * PSD2Exception.populatePSD2Exception("Could not fetch TPP name or details.",
		 * ErrorCodeEnum.TECHNICAL_ERROR); } }
		 */
			
			Map<String, String> params1 = new HashMap<String, String>();
			params1.put(PSD2Constants.TENANT_ID, pickUpDataModel.getTenant_id());

			if (pickUpDataModel.getIntentTypeEnum().getIntentType()
					.equals(IntentTypeEnum.PISP_INTENT_TYPE.getIntentType())) {
				CustomPaymentStageIdentifiers stageIdentifiers = paymentSetupPlatformAdapter
						.populateStageIdentifiers(pickUpDataModel.getIntentId());
				
				if (NullCheckUtils.isNullOrEmpty(stageIdentifiers)){
					throw PSD2Exception.populatePSD2Exception("Retrieved Payment details are empty or invalid for the requested user",
							ErrorCodeEnum.TECHNICAL_ERROR);
				}
				
				
				CustomConsentAppViewData customConsentAppViewData = pispStageOperationsAdapter
						.retrieveConsentAppStagedViewData(stageIdentifiers, params1);
				
				if (NullCheckUtils.isNullOrEmpty(customConsentAppViewData)){
					throw PSD2Exception.populatePSD2Exception("Retrieved Payment details are empty or invalid for the requested user",
							ErrorCodeEnum.TECHNICAL_ERROR);
				}
				params.put("custObj", customConsentAppViewData);
			}
			customAuthenticationServicePostResponse = authenticationPOSTAdapter.retrieveAuthenticationService(postRequest, params);
			Map<String, String> fraudHelperParam = new HashMap<>();
			fraudHelperParam.put(PSD2Constants.FLOWTYPE, pickUpDataModel.getIntentTypeEnum().getIntentType());
			fraudHelperParam.put(PSD2Constants.USERNAME,postRequest.getDigitalUser().getDigitalUserIdentifier());
			fraudHelperParam.put(FraudSystemConstants.CHANNEL_ID, requestHeaderAttributes.getChannelId().toUpperCase());
			if(postRequest.getHeaders() != null){
				fraudHelperParam.put(FraudSystemRequestMapping.FS_HEADERS, new String(Base64.getDecoder().decode(postRequest.getHeaders())));
			} 
			if(!customAuthenticationServicePostResponse.getLoginResponse().getDigitalUserSession().isSessionInitiationFailureIndicator()) {
				fraudHelperParam.put(PSD2Constants.OUTCOME, FraudSystemConstants.OUTCOME_SUCCESS);
				fraudSystemHelper.captureFraudEvent(fraudHelperParam);
			}
			// to be added after list of devices
			SimpleDateFormat sdf = new SimpleDateFormat(PFConstants.DATE_FORMAT);
			String currentDate = sdf.format(new Date());
			customAuthenticationServicePostResponse.setDropOffRef(saaSAppUtility.dropOff(postRequest.getDigitalUser().getDigitalUserIdentifier(), currentDate));

		

		LOGGER.info("Entering service for submission of device and pin"
				+ customAuthenticationServicePostResponse.toString());
		return customAuthenticationServicePostResponse;

	}
	
	private String returnLegalEntityName(BasicAttributes basicAttributes) throws NamingException {
		return getAttributeValue(basicAttributes, TPPInformationConstants.LEGAL_ENTITY_NAME);
	}

	private String getAttributeValue(BasicAttributes tppApplication, String ldapAttr) throws NamingException {
		String attributeValue = null;
		if (tppApplication.get(ldapAttr) != null && tppApplication.get(ldapAttr).get() != null) {
			attributeValue = tppApplication.get(ldapAttr).get().toString();
		}
		return attributeValue;
	}
}
