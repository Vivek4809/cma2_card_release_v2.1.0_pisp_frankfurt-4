/*
 * Payment Process API
 * Payments Process API designed for the PISP Orchestration Layer as part of the PSD2 solution.
 *
 * OpenAPI spec version: v1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.capgemini.psd2.domestic.scheduled.payments.submission.mock.foundationservice.domain;

import java.util.Objects;

import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
//import org.threeten.bp.OffsetDateTime;

/**
 * This is generic common response to an API method invocation and can be used to report technical/ business errors, information where the business transaction has been accepted but not processed and successful where the business transaction has been successfully processed.
 */
@ApiModel(description = "This is generic common response to an API method invocation and can be used to report technical/ business errors, information where the business transaction has been accepted but not processed and successful where the business transaction has been successfully processed.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-01-16T12:10:46.138+05:30")
public class CommonResponse {
  @SerializedName("api")
  private String api = null;

  @SerializedName("uri")
  private String uri = null;

  @SerializedName("timestamp")
  private String timestamp = null;

  @SerializedName("code")
  private String code = null;

  /**
   * The type of the response
   */
  @JsonAdapter(TypeEnum.Adapter.class)
  public enum TypeEnum {
    CRITICAL("CRITICAL"),
    
    ERROR("ERROR"),
    
    WARNING("WARNING"),
    
    INFORMATION("INFORMATION"),
    
    AUDIT("AUDIT"),
    
    SUCCESSFUL("SUCCESSFUL");

    private String value;

    TypeEnum(String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    public static TypeEnum fromValue(String text) {
      for (TypeEnum b : TypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    public static class Adapter extends TypeAdapter<TypeEnum> {
      @Override
      public void write(final JsonWriter jsonWriter, final TypeEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      @Override
      public TypeEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return TypeEnum.fromValue(String.valueOf(value));
      }
    }
  }

  @SerializedName("type")
  private TypeEnum type = null;

  @SerializedName("summary")
  private String summary = null;

  @SerializedName("additionalInformation")
  private CommonResponseAdditionalInformation additionalInformation = null;

  public CommonResponse api(String api) {
    this.api = api;
    return this;
  }

   /**
   * The name of the API which returned the response
   * @return api
  **/
  @ApiModelProperty(required = true, value = "The name of the API which returned the response")
  public String getApi() {
    return api;
  }

  public void setApi(String api) {
    this.api = api;
  }

  public CommonResponse uri(String uri) {
    this.uri = uri;
    return this;
  }

   /**
   * The URI including query/ uri params for the API from which returned the response
   * @return uri
  **/
  @ApiModelProperty(required = true, value = "The URI including query/ uri params for the API from which returned the response")
  public String getUri() {
    return uri;
  }

  public void setUri(String uri) {
    this.uri = uri;
  }

  public CommonResponse timestamp(String timestamp) {
    this.timestamp = timestamp;
    return this;
  }

   /**
   * The timestamp of the response
   * @return timestamp
  **/
  @ApiModelProperty(required = true, value = "The timestamp of the response")
  public String getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public CommonResponse code(String code) {
    this.code = code;
    return this;
  }

   /**
   * The unique code for the response
   * @return code
  **/
  @ApiModelProperty(value = "The unique code for the response")
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public CommonResponse type(TypeEnum type) {
    this.type = type;
    return this;
  }

   /**
   * The type of the response
   * @return type
  **/
  @ApiModelProperty(required = true, value = "The type of the response")
  public TypeEnum getType() {
    return type;
  }

  public void setType(TypeEnum type) {
    this.type = type;
  }

  public CommonResponse summary(String summary) {
    this.summary = summary;
    return this;
  }

   /**
   * A summary of the response
   * @return summary
  **/
  @ApiModelProperty(required = true, value = "A summary of the response")
  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public CommonResponse additionalInformation(CommonResponseAdditionalInformation additionalInformation) {
    this.additionalInformation = additionalInformation;
    return this;
  }

   /**
   * Get additionalInformation
   * @return additionalInformation
  **/
  @ApiModelProperty(value = "")
  public CommonResponseAdditionalInformation getAdditionalInformation() {
    return additionalInformation;
  }

  public void setAdditionalInformation(CommonResponseAdditionalInformation additionalInformation) {
    this.additionalInformation = additionalInformation;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CommonResponse commonResponse = (CommonResponse) o;
    return Objects.equals(this.api, commonResponse.api) &&
        Objects.equals(this.uri, commonResponse.uri) &&
        Objects.equals(this.timestamp, commonResponse.timestamp) &&
        Objects.equals(this.code, commonResponse.code) &&
        Objects.equals(this.type, commonResponse.type) &&
        Objects.equals(this.summary, commonResponse.summary) &&
        Objects.equals(this.additionalInformation, commonResponse.additionalInformation);
  }

  @Override
  public int hashCode() {
    return Objects.hash(api, uri, timestamp, code, type, summary, additionalInformation);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CommonResponse {\n");
    
    sb.append("    api: ").append(toIndentedString(api)).append("\n");
    sb.append("    uri: ").append(toIndentedString(uri)).append("\n");
    sb.append("    timestamp: ").append(toIndentedString(timestamp)).append("\n");
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    summary: ").append(toIndentedString(summary)).append("\n");
    sb.append("    additionalInformation: ").append(toIndentedString(additionalInformation)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

