/*
 * Payment Process API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.capgemini.psd2.domestic.standing.order.mock.foundationservice.raml.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Schedule object
 */
@ApiModel(description = "Schedule object")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-04-08T14:18:56.944+05:30")
public class Schedule {
  @SerializedName("scheduleDayOfMonthNumber")
  private Double scheduleDayOfMonthNumber = null;

  @SerializedName("scheduleNextDate")
  private LocalDate scheduleNextDate = null;

  @SerializedName("scheduleStartDate")
  private LocalDate scheduleStartDate = null;

  @SerializedName("scheduleEndDate")
  private LocalDate scheduleEndDate = null;

  @SerializedName("scheduleRegularityTypeCode")
  private ScheduleRegularityTypeCode scheduleRegularityTypeCode = null;

  @SerializedName("scheduleItems")
  private List<ScheduleItemBasic> scheduleItems = null;

  public Schedule scheduleDayOfMonthNumber(Double scheduleDayOfMonthNumber) {
    this.scheduleDayOfMonthNumber = scheduleDayOfMonthNumber;
    return this;
  }

   /**
   * The day of month number for schedule
   * @return scheduleDayOfMonthNumber
  **/
  @ApiModelProperty(value = "The day of month number for schedule")
  public Double getScheduleDayOfMonthNumber() {
    return scheduleDayOfMonthNumber;
  }

  public void setScheduleDayOfMonthNumber(Double scheduleDayOfMonthNumber) {
    this.scheduleDayOfMonthNumber = scheduleDayOfMonthNumber;
  }

  public Schedule scheduleNextDate(LocalDate scheduleNextDate) {
    this.scheduleNextDate = scheduleNextDate;
    return this;
  }

   /**
   * Next date for schedule
   * @return scheduleNextDate
  **/
  @ApiModelProperty(value = "Next date for schedule")
  public LocalDate getScheduleNextDate() {
    return scheduleNextDate;
  }

  public void setScheduleNextDate(LocalDate scheduleNextDate) {
    this.scheduleNextDate = scheduleNextDate;
  }

  public Schedule scheduleStartDate(LocalDate scheduleStartDate) {
    this.scheduleStartDate = scheduleStartDate;
    return this;
  }

   /**
   * Start date for schedule
   * @return scheduleStartDate
  **/
  @ApiModelProperty(value = "Start date for schedule")
  public LocalDate getScheduleStartDate() {
    return scheduleStartDate;
  }

  public void setScheduleStartDate(LocalDate scheduleStartDate) {
    this.scheduleStartDate = scheduleStartDate;
  }

  public Schedule scheduleEndDate(LocalDate scheduleEndDate) {
    this.scheduleEndDate = scheduleEndDate;
    return this;
  }

   /**
   * End date for schedule
   * @return scheduleEndDate
  **/
  @ApiModelProperty(value = "End date for schedule")
  public LocalDate getScheduleEndDate() {
    return scheduleEndDate;
  }

  public void setScheduleEndDate(LocalDate scheduleEndDate) {
    this.scheduleEndDate = scheduleEndDate;
  }

  public Schedule scheduleRegularityTypeCode(ScheduleRegularityTypeCode scheduleRegularityTypeCode) {
    this.scheduleRegularityTypeCode = scheduleRegularityTypeCode;
    return this;
  }

   /**
   * Get scheduleRegularityTypeCode
   * @return scheduleRegularityTypeCode
  **/
  @ApiModelProperty(value = "")
  public ScheduleRegularityTypeCode getScheduleRegularityTypeCode() {
    return scheduleRegularityTypeCode;
  }

  public void setScheduleRegularityTypeCode(ScheduleRegularityTypeCode scheduleRegularityTypeCode) {
    this.scheduleRegularityTypeCode = scheduleRegularityTypeCode;
  }

  public Schedule scheduleItems(List<ScheduleItemBasic> scheduleItems) {
    this.scheduleItems = scheduleItems;
    return this;
  }

  public Schedule addScheduleItemsItem(ScheduleItemBasic scheduleItemsItem) {
    if (this.scheduleItems == null) {
      this.scheduleItems = new ArrayList<ScheduleItemBasic>();
    }
    this.scheduleItems.add(scheduleItemsItem);
    return this;
  }

   /**
   * Get scheduleItems
   * @return scheduleItems
  **/
  @ApiModelProperty(value = "")
  public List<ScheduleItemBasic> getScheduleItems() {
    return scheduleItems;
  }

  public void setScheduleItems(List<ScheduleItemBasic> scheduleItems) {
    this.scheduleItems = scheduleItems;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Schedule schedule = (Schedule) o;
    return Objects.equals(this.scheduleDayOfMonthNumber, schedule.scheduleDayOfMonthNumber) &&
        Objects.equals(this.scheduleNextDate, schedule.scheduleNextDate) &&
        Objects.equals(this.scheduleStartDate, schedule.scheduleStartDate) &&
        Objects.equals(this.scheduleEndDate, schedule.scheduleEndDate) &&
        Objects.equals(this.scheduleRegularityTypeCode, schedule.scheduleRegularityTypeCode) &&
        Objects.equals(this.scheduleItems, schedule.scheduleItems);
  }

  @Override
  public int hashCode() {
    return Objects.hash(scheduleDayOfMonthNumber, scheduleNextDate, scheduleStartDate, scheduleEndDate, scheduleRegularityTypeCode, scheduleItems);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Schedule {\n");
    
    sb.append("    scheduleDayOfMonthNumber: ").append(toIndentedString(scheduleDayOfMonthNumber)).append("\n");
    sb.append("    scheduleNextDate: ").append(toIndentedString(scheduleNextDate)).append("\n");
    sb.append("    scheduleStartDate: ").append(toIndentedString(scheduleStartDate)).append("\n");
    sb.append("    scheduleEndDate: ").append(toIndentedString(scheduleEndDate)).append("\n");
    sb.append("    scheduleRegularityTypeCode: ").append(toIndentedString(scheduleRegularityTypeCode)).append("\n");
    sb.append("    scheduleItems: ").append(toIndentedString(scheduleItems)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

