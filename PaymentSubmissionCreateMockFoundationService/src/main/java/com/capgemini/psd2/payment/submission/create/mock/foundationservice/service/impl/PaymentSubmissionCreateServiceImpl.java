package com.capgemini.psd2.payment.submission.create.mock.foundationservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.utilities.NullCheckUtils;
import com.capgemini.psd2.foundationservice.validator.SuccesCodeEnum;
import com.capgemini.psd2.foundationservice.validator.ValidationPassed;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;
import com.capgemini.psd2.payment.submission.create.mock.foundationservice.domain.PaymentInstruction;
import com.capgemini.psd2.payment.submission.create.mock.foundationservice.repository.PaymentSubmissionCreateRepository;
import com.capgemini.psd2.payment.submission.create.mock.foundationservice.service.PaymentSubmissionCreateService;



@Service
public class PaymentSubmissionCreateServiceImpl implements PaymentSubmissionCreateService{

	@Autowired
	ValidationUtility validationUtility;
	
	@Autowired
	PaymentSubmissionCreateRepository repository;
	
	@Override
	public ValidationPassed validatePaymentInstruction( PaymentInstruction paymentInstruction) {
		
		String endToEndIdentification = paymentInstruction.getEndToEndIdentification();
		ValidationPassed validationPassed =null;
		
		PaymentInstruction returnedPaymentInstruction = repository.findByPaymentPaymentId(paymentInstruction.getPayment().getPaymentId());
		if (NullCheckUtils.isNullOrEmpty(returnedPaymentInstruction)) {
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.NOT_FOUND_PME);
		}
				
		try {
			validationPassed = validationUtility.validateMockBusinessValidations(endToEndIdentification, SuccesCodeEnum.SUCCESS_EXECUTE_PAYMENT.getSuccessCode(), SuccesCodeEnum.SUCCESS_EXECUTE_PAYMENT.getSuccessMessage());
		} catch (MockFoundationServiceException e) {
			e.getErrorInfo().setErrorText(returnedPaymentInstruction.getPayment().getPaymentInformation().getSubmissionID()+ "^" + e.getErrorInfo().getErrorText());
			throw e;
		}
		
/*		if(NullCheckUtils.isNullOrEmpty(returnedPaymentSubmissionId)){
			paymentSubmissionId = UUID.randomUUID().toString().replace("-", "");
			Payment payment = paymentInstruction.getPayment();
			returnedPaymentInstruction.getPayment().getPaymentInformation().setInitiatingPartyName(payment.getPaymentInformation().getInitiatingPartyName());
			returnedPaymentInstruction.getPayment().getPaymentInformation().setCreatedDateTime(payment.getPaymentInformation().getCreatedDateTime());
			returnedPaymentInstruction.getPayment().getPayerInformation().setPayerCurrency(payment.getPayerInformation().getPayerCurrency());
			returnedPaymentInstruction.getPayment().setPaymentSubmissionId(paymentSubmissionId);
			returnedPaymentInstruction.getPayment().setRequestType(payment.getRequestType());
			returnedPaymentInstruction.getPayment().setChannelUserID(payment.getChannelUserID());
			returnedPaymentInstruction.getPayment().setCreatedOn(payment.getCreatedOn());
			repository.save(returnedPaymentInstruction);
			validationPassed.setSuccessMessage(paymentSubmissionId);
			return validationPassed;
		}*/
		
		validationPassed.setSuccessMessage(returnedPaymentInstruction.getPayment().getPaymentInformation().getSubmissionID());
		return validationPassed;		
	}	
}
