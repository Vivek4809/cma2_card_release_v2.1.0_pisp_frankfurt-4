package com.capgemini.psd2.payment.submission.create.mock.foundationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.utilities.NullCheckUtils;
import com.capgemini.psd2.foundationservice.validator.ValidationPassed;
import com.capgemini.psd2.payment.submission.create.mock.foundationservice.domain.PaymentInstruction;
import com.capgemini.psd2.payment.submission.create.mock.foundationservice.service.PaymentSubmissionCreateService;

@RestController
@RequestMapping("/fs-payment-web-service/services")
public class PaymentSubmissionCreateController {
	
	@Autowired
	PaymentSubmissionCreateService submissionService;
	
	@RequestMapping(value = "/executePayment", method = RequestMethod.POST, produces = "application/xml", consumes = "application/xml")
	public ResponseEntity<ValidationPassed> executePayment(
	@RequestHeader(required = false, value= "X-BOI-USER") String boiUser,
	@RequestHeader(required = false, value= "X-BOI-CHANNEL") String boiChannel,
	@RequestHeader(required = false, value = "X-BOI-PLATFORM") String boiPlatform,
	@RequestHeader(required = false, value = "X-CORRELATION-ID") String correlationID,
	@RequestHeader(required = false, value = "client_id") String clientID,
	@RequestHeader(required = false, value = "client_secret") String clientSecret,
	@RequestBody PaymentInstruction paymentInstruction){
		/*
		 *  Validations
		 */
		if (NullCheckUtils.isNullOrEmpty(boiUser) || NullCheckUtils.isNullOrEmpty(boiChannel)
				|| NullCheckUtils.isNullOrEmpty(boiPlatform) || NullCheckUtils.isNullOrEmpty(correlationID)) {
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.AUTHENTICATION_FAILURE_PME);
		}
		
		if(NullCheckUtils.isNullOrEmpty(paymentInstruction.getPayment()) || NullCheckUtils.isNullOrEmpty(paymentInstruction.getPayment().getPaymentId())
				|| NullCheckUtils.isNullOrEmpty(paymentInstruction.getEndToEndIdentification())){
			
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PME);
		}

		ValidationPassed validationPassed = submissionService.validatePaymentInstruction(paymentInstruction);
		return new ResponseEntity<ValidationPassed>(validationPassed, HttpStatus.CREATED);
	
	}

}
