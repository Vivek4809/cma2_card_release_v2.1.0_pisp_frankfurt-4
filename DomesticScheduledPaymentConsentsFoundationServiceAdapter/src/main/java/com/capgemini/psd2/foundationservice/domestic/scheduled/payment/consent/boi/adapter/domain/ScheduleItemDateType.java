/*
 * Payment Process API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain;

import java.util.Objects;
import io.swagger.annotations.ApiModel;
import com.google.gson.annotations.SerializedName;

import java.io.IOException;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

/**
 * Defines if the schedule Item Date is the arrival date of the payment or execution date
 */
@JsonAdapter(ScheduleItemDateType.Adapter.class)
public enum ScheduleItemDateType {
  
  EXECUTION("Execution"),
  
  ARRIVAL("Arrival");

  private String value;

  ScheduleItemDateType(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return String.valueOf(value);
  }

  public static ScheduleItemDateType fromValue(String text) {
    for (ScheduleItemDateType b : ScheduleItemDateType.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }

  public static class Adapter extends TypeAdapter<ScheduleItemDateType> {
    @Override
    public void write(final JsonWriter jsonWriter, final ScheduleItemDateType enumeration) throws IOException {
      jsonWriter.value(enumeration.getValue());
    }

    @Override
    public ScheduleItemDateType read(final JsonReader jsonReader) throws IOException {
      String value = jsonReader.nextString();
      return ScheduleItemDateType.fromValue(String.valueOf(value));
    }
  }
}

