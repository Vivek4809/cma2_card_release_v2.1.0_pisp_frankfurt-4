package com.capgemini.psd2.authentication.application.service.mule.mock.foundationservice.domain;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class CustomLoginResponse {

	
	@SerializedName("digitalUserIdentifier")
	  private String digitalUserIdentifier = null;

	  @SerializedName("authenticationSystemCode")
	  private AuthenticationSystemCode authenticationSystemCode = null;

	  @SerializedName("channelCode")
	  private ChannelCode channelCode = null;

	  @SerializedName("digitalUserLockedOutIndicator")
	  private Boolean digitalUserLockedOutIndicator = null;

	  @SerializedName("secureKeyAttemptsRemainingCount")
	  private Double secureKeyAttemptsRemainingCount = null;

	  @SerializedName("personInformation")
	  private PersonBasicInformation personInformation = null;

	  @SerializedName("authenticationProtocolCode")
	  private AuthenticationProtocolCode authenticationProtocolCode = null;
	  
	  
	  @SerializedName("customerAuthenticationSession")
	  private List<CustomerAuthenticationSession> customerAuthenticationSession = new ArrayList<CustomerAuthenticationSession>();


	@Override
	public String toString() {
		return "CustomLoginResponse [digitalUserIdentifier=" + digitalUserIdentifier + ", authenticationSystemCode="
				+ authenticationSystemCode + ", channelCode=" + channelCode + ", digitalUserLockedOutIndicator="
				+ digitalUserLockedOutIndicator + ", secureKeyAttemptsRemainingCount=" + secureKeyAttemptsRemainingCount
				+ ", personInformation=" + personInformation + ", authenticationProtocolCode="
				+ authenticationProtocolCode + ", customerAuthenticationSession=" + customerAuthenticationSession + "]";
	}


	public String getDigitalUserIdentifier() {
		return digitalUserIdentifier;
	}


	public void setDigitalUserIdentifier(String digitalUserIdentifier) {
		this.digitalUserIdentifier = digitalUserIdentifier;
	}


	public AuthenticationSystemCode getAuthenticationSystemCode() {
		return authenticationSystemCode;
	}


	public void setAuthenticationSystemCode(AuthenticationSystemCode authenticationSystemCode) {
		this.authenticationSystemCode = authenticationSystemCode;
	}


	public ChannelCode getChannelCode() {
		return channelCode;
	}


	public void setChannelCode(ChannelCode channelCode) {
		this.channelCode = channelCode;
	}


	public Boolean getDigitalUserLockedOutIndicator() {
		return digitalUserLockedOutIndicator;
	}


	public void setDigitalUserLockedOutIndicator(Boolean digitalUserLockedOutIndicator) {
		this.digitalUserLockedOutIndicator = digitalUserLockedOutIndicator;
	}


	public Double getSecureKeyAttemptsRemainingCount() {
		return secureKeyAttemptsRemainingCount;
	}


	public void setSecureKeyAttemptsRemainingCount(Double secureKeyAttemptsRemainingCount) {
		this.secureKeyAttemptsRemainingCount = secureKeyAttemptsRemainingCount;
	}


	public PersonBasicInformation getPersonInformation() {
		return personInformation;
	}


	public void setPersonInformation(PersonBasicInformation personInformation) {
		this.personInformation = personInformation;
	}


	public AuthenticationProtocolCode getAuthenticationProtocolCode() {
		return authenticationProtocolCode;
	}


	public void setAuthenticationProtocolCode(AuthenticationProtocolCode authenticationProtocolCode) {
		this.authenticationProtocolCode = authenticationProtocolCode;
	}


	public List<CustomerAuthenticationSession> getCustomerAuthenticationSession() {
		return customerAuthenticationSession;
	}


	public void setCustomerAuthenticationSession(List<CustomerAuthenticationSession> customerAuthenticationSession) {
		this.customerAuthenticationSession = customerAuthenticationSession;
	}
}
