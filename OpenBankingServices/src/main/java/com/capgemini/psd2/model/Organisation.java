package com.capgemini.psd2.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Account details for an open banking organisation which are used for SCIM access to the Directory on behalf of an accredited organisation.
 */
@ApiModel(description = "Account details for an open banking organisation which are used for SCIM access to the Directory on behalf of an accredited organisation.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")
@JsonInclude(Include.NON_NULL)
public class Organisation   {
  @JsonProperty("CreateTimestamp")
  private String createTimestamp = null;

  @JsonProperty("EmailAddresses")
  private List<OrganisationEmailAddresses> emailAddresses = null;

  @JsonProperty("ModifyTimestamp")
  private String modifyTimestamp = null;

  @JsonProperty("OBOrganisationId")
  private String obOrganisationId = null;

  @JsonProperty("OrganisationCommonName")
  private String organisationCommonName = null;

  @JsonProperty("PersonalAccountRoles")
  private List<OrganisationPersonalAccountRoles> personalAccountRoles = null;

  @JsonProperty("PhoneNumbers")
  private List<OrganisationPhoneNumbers> phoneNumbers = null;

  @JsonProperty("PostalAddress")
  private List<OrganisationPostalAddress> postalAddress = null;
  
  @JsonProperty("status")
  private String status = null;

  public Organisation createTimestamp(String createTimestamp) {
    this.createTimestamp = createTimestamp;
    return this;
  }

   /**
   * Creation Timestamp
   * @return createTimestamp
  **/
  @ApiModelProperty(readOnly = true, value = "Creation Timestamp")


  public String getCreateTimestamp() {
    return createTimestamp;
  }

  public void setCreateTimestamp(String createTimestamp) {
    this.createTimestamp = createTimestamp;
  }

  public Organisation emailAddresses(List<OrganisationEmailAddresses> emailAddresses) {
    this.emailAddresses = emailAddresses;
    return this;
  }

  public Organisation addEmailAddressesItem(OrganisationEmailAddresses emailAddressesItem) {
    if (this.emailAddresses == null) {
      this.emailAddresses = new ArrayList<OrganisationEmailAddresses>();
    }
    this.emailAddresses.add(emailAddressesItem);
    return this;
  }

   /**
   * Email addresses associated with the organisation
   * @return emailAddresses
  **/
  @ApiModelProperty(value = "Email addresses associated with the organisation")

  @Valid

  public List<OrganisationEmailAddresses> getEmailAddresses() {
    return emailAddresses;
  }

  public void setEmailAddresses(List<OrganisationEmailAddresses> emailAddresses) {
    this.emailAddresses = emailAddresses;
  }

  public Organisation modifyTimestamp(String modifyTimestamp) {
    this.modifyTimestamp = modifyTimestamp;
    return this;
  }

   /**
   * Modification Timestamp
   * @return modifyTimestamp
  **/
  @ApiModelProperty(readOnly = true, value = "Modification Timestamp")


  public String getModifyTimestamp() {
    return modifyTimestamp;
  }

  public void setModifyTimestamp(String modifyTimestamp) {
    this.modifyTimestamp = modifyTimestamp;
  }

  public Organisation obOrganisationId(String obOrganisationId) {
    this.obOrganisationId = obOrganisationId;
    return this;
  }

   /**
   * Get obOrganisationId
   * @return obOrganisationId
  **/
  @ApiModelProperty(value = "")


  public String getObOrganisationId() {
    return obOrganisationId;
  }

  public void setObOrganisationId(String obOrganisationId) {
    this.obOrganisationId = obOrganisationId;
  }

  public Organisation organisationCommonName(String organisationCommonName) {
    this.organisationCommonName = organisationCommonName;
    return this;
  }

   /**
   * The common name of the organisation
   * @return organisationCommonName
  **/
  @ApiModelProperty(value = "The common name of the organisation")


  public String getOrganisationCommonName() {
    return organisationCommonName;
  }

  public void setOrganisationCommonName(String organisationCommonName) {
    this.organisationCommonName = organisationCommonName;
  }

  public Organisation personalAccountRoles(List<OrganisationPersonalAccountRoles> personalAccountRoles) {
    this.personalAccountRoles = personalAccountRoles;
    return this;
  }

  public Organisation addPersonalAccountRolesItem(OrganisationPersonalAccountRoles personalAccountRolesItem) {
    if (this.personalAccountRoles == null) {
      this.personalAccountRoles = new ArrayList<OrganisationPersonalAccountRoles>();
    }
    this.personalAccountRoles.add(personalAccountRolesItem);
    return this;
  }

   /**
   * Get personalAccountRoles
   * @return personalAccountRoles
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<OrganisationPersonalAccountRoles> getPersonalAccountRoles() {
    return personalAccountRoles;
  }

  public void setPersonalAccountRoles(List<OrganisationPersonalAccountRoles> personalAccountRoles) {
    this.personalAccountRoles = personalAccountRoles;
  }

  public Organisation phoneNumbers(List<OrganisationPhoneNumbers> phoneNumbers) {
    this.phoneNumbers = phoneNumbers;
    return this;
  }

  public Organisation addPhoneNumbersItem(OrganisationPhoneNumbers phoneNumbersItem) {
    if (this.phoneNumbers == null) {
      this.phoneNumbers = new ArrayList<OrganisationPhoneNumbers>();
    }
    this.phoneNumbers.add(phoneNumbersItem);
    return this;
  }

   /**
   * Phone numbers by which an organisation can be contacted
   * @return phoneNumbers
  **/
  @ApiModelProperty(value = "Phone numbers by which an organisation can be contacted")

  @Valid

  public List<OrganisationPhoneNumbers> getPhoneNumbers() {
    return phoneNumbers;
  }

  public void setPhoneNumbers(List<OrganisationPhoneNumbers> phoneNumbers) {
    this.phoneNumbers = phoneNumbers;
  }

  public Organisation postalAddress(List<OrganisationPostalAddress> postalAddress) {
    this.postalAddress = postalAddress;
    return this;
  }

  public Organisation addPostalAddressItem(OrganisationPostalAddress postalAddressItem) {
    if (this.postalAddress == null) {
      this.postalAddress = new ArrayList<OrganisationPostalAddress>();
    }
    this.postalAddress.add(postalAddressItem);
    return this;
  }

   /**
   * Postal Address of the organisation
   * @return postalAddress
  **/
  @ApiModelProperty(value = "Postal Address of the organisation")

  @Valid

  public List<OrganisationPostalAddress> getPostalAddress() {
    return postalAddress;
  }

  public void setPostalAddress(List<OrganisationPostalAddress> postalAddress) {
    this.postalAddress = postalAddress;
  }


  public String getStatus() {
	return status;
}

public void setStatus(String status) {
	this.status = status;
}

@Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Organisation organisation = (Organisation) o;
    return Objects.equals(this.createTimestamp, organisation.createTimestamp) &&
        Objects.equals(this.emailAddresses, organisation.emailAddresses) &&
        Objects.equals(this.modifyTimestamp, organisation.modifyTimestamp) &&
        Objects.equals(this.obOrganisationId, organisation.obOrganisationId) &&
        Objects.equals(this.organisationCommonName, organisation.organisationCommonName) &&
        Objects.equals(this.personalAccountRoles, organisation.personalAccountRoles) &&
        Objects.equals(this.phoneNumbers, organisation.phoneNumbers) &&
        Objects.equals(this.postalAddress, organisation.postalAddress);
  }

  @Override
  public int hashCode() {
    return Objects.hash(createTimestamp, emailAddresses, modifyTimestamp, obOrganisationId, organisationCommonName, personalAccountRoles, phoneNumbers, postalAddress);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Organisation {\n");
    
    sb.append("    createTimestamp: ").append(toIndentedString(createTimestamp)).append("\n");
    sb.append("    emailAddresses: ").append(toIndentedString(emailAddresses)).append("\n");
    sb.append("    modifyTimestamp: ").append(toIndentedString(modifyTimestamp)).append("\n");
    sb.append("    obOrganisationId: ").append(toIndentedString(obOrganisationId)).append("\n");
    sb.append("    organisationCommonName: ").append(toIndentedString(organisationCommonName)).append("\n");
    sb.append("    personalAccountRoles: ").append(toIndentedString(personalAccountRoles)).append("\n");
    sb.append("    phoneNumbers: ").append(toIndentedString(phoneNumbers)).append("\n");
    sb.append("    postalAddress: ").append(toIndentedString(postalAddress)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

