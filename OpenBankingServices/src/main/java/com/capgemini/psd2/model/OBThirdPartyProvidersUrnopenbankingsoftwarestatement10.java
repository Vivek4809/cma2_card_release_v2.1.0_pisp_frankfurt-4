package com.capgemini.psd2.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * The definition of a software statement as defined by OpenBanking ecosystem
 */
@ApiModel(description = "The definition of a software statement as defined by OpenBanking ecosystem")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class OBThirdPartyProvidersUrnopenbankingsoftwarestatement10   {
  @JsonProperty("SoftwareStatements")
  private List<OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements> softwareStatements = null;

  public OBThirdPartyProvidersUrnopenbankingsoftwarestatement10 softwareStatements(List<OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements> softwareStatements) {
    this.softwareStatements = softwareStatements;
    return this;
  }

  public OBThirdPartyProvidersUrnopenbankingsoftwarestatement10 addSoftwareStatementsItem(OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements softwareStatementsItem) {
    if (this.softwareStatements == null) {
      this.softwareStatements = new ArrayList<OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements>();
    }
    this.softwareStatements.add(softwareStatementsItem);
    return this;
  }

   /**
   * Get softwareStatements
   * @return softwareStatements
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements> getSoftwareStatements() {
    return softwareStatements;
  }

  public void setSoftwareStatements(List<OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements> softwareStatements) {
    this.softwareStatements = softwareStatements;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OBThirdPartyProvidersUrnopenbankingsoftwarestatement10 obThirdPartyProvidersUrnopenbankingsoftwarestatement10 = (OBThirdPartyProvidersUrnopenbankingsoftwarestatement10) o;
    return Objects.equals(this.softwareStatements, obThirdPartyProvidersUrnopenbankingsoftwarestatement10.softwareStatements);
  }

  @Override
  public int hashCode() {
    return Objects.hash(softwareStatements);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OBThirdPartyProvidersUrnopenbankingsoftwarestatement10 {\n");
    
    sb.append("    softwareStatements: ").append(toIndentedString(softwareStatements)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

