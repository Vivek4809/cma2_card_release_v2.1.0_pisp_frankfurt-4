package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.time.OffsetDateTime;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModelProperty;

/**
 * UserEvent2
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-03-19T13:42:54.062+05:30")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserEvent2 {
	@SerializedName("source")
	private Source source = null;

	@SerializedName("type")
	private Type type = null;

	@SerializedName("action")
	private Action action = null;

	@SerializedName("outcome")
	private Outcome outcome = null;

	@SerializedName("time")
	private OffsetDateTime time = null;

	@SerializedName("channelClassificationCode")
	private ChannelClassificationCode channelClassificationCode = null;

	@SerializedName("eventData")
	private EventData1 eventData = null;

	@SerializedName("eventRiskAssessment")
	private FraudServiceResponseType eventRiskAssessment = null;

	public UserEvent2 source(Source source) {
		this.source = source;
		return this;
	}

	/**
	 * Get source
	 * 
	 * @return source
	 **/
	@ApiModelProperty(required = true, value = "")
	public Source getSource() {
		return source;
	}

	public void setSource(Source source) {
		this.source = source;
	}

	public UserEvent2 type(Type type) {
		this.type = type;
		return this;
	}

	/**
	 * Get type
	 * 
	 * @return type
	 **/
	@ApiModelProperty(required = true, value = "")
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public UserEvent2 action(Action action) {
		this.action = action;
		return this;
	}

	/**
	 * Get action
	 * 
	 * @return action
	 **/
	@ApiModelProperty(required = true, value = "")
	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public UserEvent2 outcome(Outcome outcome) {
		this.outcome = outcome;
		return this;
	}

	/**
	 * Get outcome
	 * 
	 * @return outcome
	 **/
	@ApiModelProperty(required = true, value = "")
	public Outcome getOutcome() {
		return outcome;
	}

	public void setOutcome(Outcome outcome) {
		this.outcome = outcome;
	}

	public UserEvent2 time(OffsetDateTime time) {
		this.time = time;
		return this;
	}

	/**
	 * The time when the event occurred
	 * 
	 * @return time
	 **/
	@ApiModelProperty(required = true, value = "The time when the event occurred")
	public OffsetDateTime getTime() {
		return time;
	}

	public void setTime(OffsetDateTime time) {
		this.time = time;
	}

	public UserEvent2 channelClassificationCode(ChannelClassificationCode channelClassificationCode) {
		this.channelClassificationCode = channelClassificationCode;
		return this;
	}

	/**
	 * Get channelClassificationCode
	 * 
	 * @return channelClassificationCode
	 **/
	@ApiModelProperty(required = true, value = "")
	public ChannelClassificationCode getChannelClassificationCode() {
		return channelClassificationCode;
	}

	public void setChannelClassificationCode(ChannelClassificationCode channelClassificationCode) {
		this.channelClassificationCode = channelClassificationCode;
	}

	public UserEvent2 eventData(EventData1 eventData) {
		this.eventData = eventData;
		return this;
	}

	/**
	 * Get eventData
	 * 
	 * @return eventData
	 **/
	@ApiModelProperty(required = true, value = "")
	public EventData1 getEventData() {
		return eventData;
	}

	public void setEventData(EventData1 eventData) {
		this.eventData = eventData;
	}

	public UserEvent2 eventRiskAssessment(FraudServiceResponseType eventRiskAssessment) {
		this.eventRiskAssessment = eventRiskAssessment;
		return this;
	}

	/**
	 * Get eventRiskAssessment
	 * 
	 * @return eventRiskAssessment
	 **/
	@ApiModelProperty(value = "")
	public FraudServiceResponseType getEventRiskAssessment() {
		return eventRiskAssessment;
	}

	public void setEventRiskAssessment(FraudServiceResponseType eventRiskAssessment) {
		this.eventRiskAssessment = eventRiskAssessment;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		UserEvent2 userEvent2 = (UserEvent2) o;
		return Objects.equals(this.source, userEvent2.source) && Objects.equals(this.type, userEvent2.type)
				&& Objects.equals(this.action, userEvent2.action) && Objects.equals(this.outcome, userEvent2.outcome)
				&& Objects.equals(this.time, userEvent2.time)
				&& Objects.equals(this.channelClassificationCode, userEvent2.channelClassificationCode)
				&& Objects.equals(this.eventData, userEvent2.eventData)
				&& Objects.equals(this.eventRiskAssessment, userEvent2.eventRiskAssessment);
	}

	@Override
	public int hashCode() {
		return Objects.hash(source, type, action, outcome, time, channelClassificationCode, eventData,
				eventRiskAssessment);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class UserEvent2 {\n");

		sb.append("    source: ").append(toIndentedString(source)).append("\n");
		sb.append("    type: ").append(toIndentedString(type)).append("\n");
		sb.append("    action: ").append(toIndentedString(action)).append("\n");
		sb.append("    outcome: ").append(toIndentedString(outcome)).append("\n");
		sb.append("    time: ").append(toIndentedString(time)).append("\n");
		sb.append("    channelClassificationCode: ").append(toIndentedString(channelClassificationCode)).append("\n");
		sb.append("    eventData: ").append(toIndentedString(eventData)).append("\n");
		sb.append("    eventRiskAssessment: ").append(toIndentedString(eventRiskAssessment)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
