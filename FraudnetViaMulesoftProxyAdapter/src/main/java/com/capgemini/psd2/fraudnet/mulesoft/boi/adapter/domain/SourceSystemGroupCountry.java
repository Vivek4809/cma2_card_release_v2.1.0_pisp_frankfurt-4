package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

/**
 * The source system group country branding
 */
@JsonAdapter(SourceSystemGroupCountry.Adapter.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public enum SourceSystemGroupCountry {

	IE("IE"),

	UK("UK");

	private String value;

	SourceSystemGroupCountry(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return String.valueOf(value);
	}

	public static SourceSystemGroupCountry fromValue(String text) {
		for (SourceSystemGroupCountry b : SourceSystemGroupCountry.values()) {
			if (String.valueOf(b.value).equals(text)) {
				return b;
			}
		}
		return null;
	}

	public static class Adapter extends TypeAdapter<SourceSystemGroupCountry> {
		@Override
		public void write(final JsonWriter jsonWriter, final SourceSystemGroupCountry enumeration) throws IOException {
			jsonWriter.value(enumeration.getValue());
		}

		@Override
		public SourceSystemGroupCountry read(final JsonReader jsonReader) throws IOException {
			String value = jsonReader.nextString();
			return SourceSystemGroupCountry.fromValue(String.valueOf(value));
		}
	}
}
