package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets replayInterval
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public enum ReplayInterval {

	IMMEDIATE("Immediate"),

	DELAYED_1("Delayed_1"),

	DELAYED_15("Delayed_15"),

	DELAYED_CUSTOM("Delayed_Custom");

	private String value;

	ReplayInterval(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	@Override
	@JsonValue
	public String toString() {
		return String.valueOf(value);
	}

	@JsonCreator
	public static ReplayInterval fromValue(String text) {
		for (ReplayInterval b : ReplayInterval.values()) {
			if (String.valueOf(b.value).equals(text)) {
				return b;
			}
		}
		return null;
	}

}
