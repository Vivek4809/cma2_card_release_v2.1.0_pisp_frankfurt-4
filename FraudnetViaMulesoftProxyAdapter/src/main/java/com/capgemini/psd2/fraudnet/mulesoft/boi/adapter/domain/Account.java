package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

/**
 * Account
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-03-19T13:42:54.062+05:30")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Account {
	@SerializedName("accountInfo")
	private AccountInfo accountInfo;

	@SerializedName("sourceSystemAccountType")
	private String sourceSystemAccountType;

	@SerializedName("boiCurrencyCode")
	private String boiCurrencyCode;

	public Account accountInfo(AccountInfo accountInfo) {
		this.accountInfo = accountInfo;
		return this;
	}

	/**
	 * Get accountInfo
	 * 
	 * @return accountInfo
	 **/
	@ApiModelProperty(value = "")
	public AccountInfo getAccountInfo() {
		return accountInfo;
	}

	public void setAccountInfo(AccountInfo accountInfo) {
		this.accountInfo = accountInfo;
	}

	public Account sourceSystemAccountType(String sourceSystemAccountType) {
		this.sourceSystemAccountType = sourceSystemAccountType;
		return this;
	}

	/**
	 * Account Type from source system (valid for all systems)
	 * 
	 * @return sourceSystemAccountType
	 **/
	@ApiModelProperty(value = "Account Type from source system (valid for all systems)")
	public String getSourceSystemAccountType() {
		return sourceSystemAccountType;
	}

	public void setSourceSystemAccountType(String sourceSystemAccountType) {
		this.sourceSystemAccountType = sourceSystemAccountType;
	}

	public Account boiCurrencyCode(String boiCurrencyCode) {
		this.boiCurrencyCode = boiCurrencyCode;
		return this;
	}

	/**
	 * Get boiCurrencyCode
	 * 
	 * @return boiCurrencyCode
	 **/
	@ApiModelProperty(value = "")
	public String getBoiCurrencyCode() {
		return boiCurrencyCode;
	}

	public void setBoiCurrencyCode(String boiCurrencyCode) {
		this.boiCurrencyCode = boiCurrencyCode;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Account account = (Account) o;
		return Objects.equals(this.accountInfo, account.accountInfo)
				&& Objects.equals(this.sourceSystemAccountType, account.sourceSystemAccountType)
				&& Objects.equals(this.boiCurrencyCode, account.boiCurrencyCode);
	}

	@Override
	public int hashCode() {
		return Objects.hash(accountInfo, sourceSystemAccountType, boiCurrencyCode);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Account {\n");

		sb.append("    accountInfo: ").append(toIndentedString(accountInfo)).append("\n");
		sb.append("    sourceSystemAccountType: ").append(toIndentedString(sourceSystemAccountType)).append("\n");
		sb.append("    boiCurrencyCode: ").append(toIndentedString(boiCurrencyCode)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
