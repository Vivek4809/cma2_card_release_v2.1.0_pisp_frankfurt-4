package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModelProperty;

/**
 * DeviceData
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-03-19T13:42:54.062+05:30")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeviceData {
  @SerializedName("deviceFieldName")
  private String deviceFieldName = null;

  @SerializedName("deviceFieldType")
  private DeviceFieldType deviceFieldType = null;

  @SerializedName("deviceFieldValue")
  private String deviceFieldValue = null;

  public DeviceData deviceFieldName(String deviceFieldName) {
    this.deviceFieldName = deviceFieldName;
    return this;
  }

   /**
   * Name of the metadata field
   * @return deviceFieldName
  **/
  @ApiModelProperty(required = true, value = "Name of the metadata field")
  public String getDeviceFieldName() {
    return deviceFieldName;
  }

  public void setDeviceFieldName(String deviceFieldName) {
    this.deviceFieldName = deviceFieldName;
  }

  public DeviceData deviceFieldType(DeviceFieldType deviceFieldType) {
    this.deviceFieldType = deviceFieldType;
    return this;
  }

   /**
   * Get deviceFieldType
   * @return deviceFieldType
  **/
  @ApiModelProperty(required = true, value = "")
  public DeviceFieldType getDeviceFieldType() {
    return deviceFieldType;
  }

  public void setDeviceFieldType(DeviceFieldType deviceFieldType) {
    this.deviceFieldType = deviceFieldType;
  }

  public DeviceData deviceFieldValue(String deviceFieldValue) {
    this.deviceFieldValue = deviceFieldValue;
    return this;
  }

   /**
   * The value of the metadata field
   * @return deviceFieldValue
  **/
  @ApiModelProperty(required = true, value = "The value of the metadata field")
  public String getDeviceFieldValue() {
    return deviceFieldValue;
  }

  public void setDeviceFieldValue(String deviceFieldValue) {
    this.deviceFieldValue = deviceFieldValue;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DeviceData deviceData = (DeviceData) o;
    return Objects.equals(this.deviceFieldName, deviceData.deviceFieldName) &&
        Objects.equals(this.deviceFieldType, deviceData.deviceFieldType) &&
        Objects.equals(this.deviceFieldValue, deviceData.deviceFieldValue);
  }

  @Override
  public int hashCode() {
    return Objects.hash(deviceFieldName, deviceFieldType, deviceFieldValue);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DeviceData {\n");
    
    sb.append("    deviceFieldName: ").append(toIndentedString(deviceFieldName)).append("\n");
    sb.append("    deviceFieldType: ").append(toIndentedString(deviceFieldType)).append("\n");
    sb.append("    deviceFieldValue: ").append(toIndentedString(deviceFieldValue)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

