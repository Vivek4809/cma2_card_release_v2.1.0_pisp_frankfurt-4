package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * The subsystem / APIs from which the event originated from
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public enum SourceSubSystem {

	AUTHENTICATION_UI("Authentication UI"),

	CONSENT_UI("Consent UI"),

	BOL_WEB("BOL Web");

	private String value;

	SourceSubSystem(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	@Override
	@JsonValue
	public String toString() {
		return String.valueOf(value);
	}

	@JsonCreator
	public static SourceSubSystem fromValue(String text) {
		for (SourceSubSystem b : SourceSubSystem.values()) {
			if (String.valueOf(b.value).equals(text)) {
				return b;
			}
		}
		return null;
	}

}
