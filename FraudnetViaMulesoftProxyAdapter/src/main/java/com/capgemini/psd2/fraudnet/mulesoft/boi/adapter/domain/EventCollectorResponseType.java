package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * This is response structure returned from the event collector
 */
@ApiModel(description = "This is response structure returned from the event collector")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-03-19T13:42:54.062+05:30")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventCollectorResponseType {
	@SerializedName("eventIdentifier")
	private String eventIdentifier = null;

	public EventCollectorResponseType eventIdentifier(String eventIdentifier) {
		this.eventIdentifier = eventIdentifier;
		return this;
	}

	/**
	 * A unique identifier of the event
	 * 
	 * @return eventIdentifier
	 **/
	@ApiModelProperty(required = true, value = "A unique identifier of the event")
	public String getEventIdentifier() {
		return eventIdentifier;
	}

	public void setEventIdentifier(String eventIdentifier) {
		this.eventIdentifier = eventIdentifier;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		EventCollectorResponseType eventCollectorResponseType = (EventCollectorResponseType) o;
		return Objects.equals(this.eventIdentifier, eventCollectorResponseType.eventIdentifier);
	}

	@Override
	public int hashCode() {
		return Objects.hash(eventIdentifier);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class EventCollectorResponseType {\n");

		sb.append("    eventIdentifier: ").append(toIndentedString(eventIdentifier)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
