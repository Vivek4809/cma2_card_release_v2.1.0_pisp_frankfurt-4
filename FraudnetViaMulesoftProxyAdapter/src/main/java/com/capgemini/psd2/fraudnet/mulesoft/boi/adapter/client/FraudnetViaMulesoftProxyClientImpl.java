
package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.adapter.fraudnet.domain.FraudServiceResponse;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.FraudServiceRequest;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@Service
public class FraudnetViaMulesoftProxyClientImpl implements FraudnetViaMulesoftProxyClient {

	/** The rest client. */
	@Autowired
	@Qualifier("restClientFoundation")
	private RestClientSync restClient;

	@Override
	public FraudServiceResponse fraudnetViaMulesoftCall(RequestInfo requestInfo, FraudServiceRequest fraudServiceRequest, Class<FraudServiceResponse> response, HttpHeaders httpHeaders) {

		return restClient.callForPost(requestInfo, fraudServiceRequest, response, httpHeaders);

	}

}
