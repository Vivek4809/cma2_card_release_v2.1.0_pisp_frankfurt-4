package com.capgemini.psd2.account.beneficiaries.mock.foundationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories("com.capgemini.psd2")
@ComponentScan(basePackages = { "com.capgemini.psd2" })
public class AccountBeneficiariesApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountBeneficiariesApplication.class, args);
	}
}
