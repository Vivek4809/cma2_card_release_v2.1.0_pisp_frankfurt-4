package com.capgemini.psd2.account.beneficiaries.mock.foundationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.account.beneficiaries.mock.foundationservice.domain.PartiesPaymentBeneficiariesresponse;
import com.capgemini.psd2.account.beneficiaries.mock.foundationservice.service.AccountBeneficiariesService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.utilities.NullCheckUtils;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;

@RestController
@RequestMapping("/fs-abt-service/services")
public class AccountBeneficiariesController {

	@Autowired
	private AccountBeneficiariesService accountBeneficiariesService;
	
	@Autowired
	private ValidationUtility validationUtility;

	@RequestMapping(value = "/user/{version}/parties/{userId}/payment-beneficiaries", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public PartiesPaymentBeneficiariesresponse retrieveBeneficiaries(@PathVariable("userId") String userId,
			@RequestHeader(required = false, value = "x-api-source-user") String boiUser,
			@RequestHeader(required = false, value = "x-api-channel-Code") String boiChannel,
			@RequestHeader(required = false, value = "x-api-source-system") String boiPlatform,
			@RequestHeader(required = false, value = "x-api-transaction-id") String correlationID) throws Exception {

		validationUtility.validateErrorCode(correlationID);
		if (NullCheckUtils.isNullOrEmpty(boiUser) || NullCheckUtils.isNullOrEmpty(boiChannel)
				|| NullCheckUtils.isNullOrEmpty(boiPlatform) || NullCheckUtils.isNullOrEmpty(correlationID)) {
			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.AUTHENTICATION_FAILURE_ESBE);
		}

		if (NullCheckUtils.isNullOrEmpty(userId)) {
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_ESBE);
		}

		// PartiesPaymentBeneficiariesresponse beneficiaries =
		// accountBeneficiariesService.getBeneficiaries(userId);
		return accountBeneficiariesService.getBeneficiaries(userId);
	}

}
