/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.domestic.payment.mock.foundationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.domestic.payment.mock.foundationservice.exception.handler.InvalidParameterRequestException;
import com.capgemini.psd2.domestic.payment.mock.foundationservice.exception.handler.MissingAuthenticationHeaderException;
import com.capgemini.psd2.domestic.payment.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.domestic.payment.mock.foundationservice.service.DomesticPaymentsService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.pisp3.domain.PaymentInstructionProposalComposite;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;

@RestController
@RequestMapping("/group-payments/p/payments-service/{version}")
public class DomesticPaymentsController {

	@Autowired
	private DomesticPaymentsService domesticPaymentService;
	
	/** The validation utility. */
	@Autowired
    private ValidationUtility validationUtility;

	@RequestMapping(value = "/domestic/payment-instructions/{payment-instruction-number}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public PaymentInstructionProposalComposite channelRetrieveInformation(
			@PathVariable("payment-instruction-number")String paymentInstructionProposalId,
			@RequestHeader(required = false, value = "X-API-CHANNEL-CODE") String apiChannelCode,
			@RequestHeader(required = false, value = "X-API-SOURCE-USER") String sourceUserReqHeader,
			@RequestHeader(required = false, value = "X-API-TRANSACTION-ID") String transactioReqHeader,
			@RequestHeader(required = false, value = "X-API-CORRELATION-ID") String correlationMuleReqHeader,
			@RequestHeader(required = false, value = "X-API-PARTY-SOURCE-ID-NUMBER") String partySourceId,
			@RequestHeader(required = false, value = "X-API-SOURCE-SYSTEM") String sourcesystem, 
			@RequestHeader(required = false, value = "X-API-CHANNEL-BRAND") String channelBrand) throws Exception{
		if(sourcesystem == null  || channelBrand == null) {
			throw new MissingAuthenticationHeaderException("Header Missing");
		}
		if (paymentInstructionProposalId == null) {
			throw new InvalidParameterRequestException("Bad request");
		}
		validationUtility.validateErrorCode(transactioReqHeader);
		return domesticPaymentService.retrieveDocumentPaymentInformation(paymentInstructionProposalId);

	}
	
	@RequestMapping(value = "/domestic/payment-instructions", method = RequestMethod.POST ,produces = {MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseBody
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<PaymentInstructionProposalComposite> channelExecuteInformation(
			@RequestBody PaymentInstructionProposalComposite domesticPaymentRequest,
			@RequestHeader(required = false, value = "X-API-CHANNEL-CODE") String apiChannelCode,
			@RequestHeader(required = false, value = "X-API-SOURCE-USER") String sourceUserReqHeader,
			@RequestHeader(required = false, value = "X-API-TRANSACTION-ID") String transactioReqHeader,
			@RequestHeader(required = false, value = "X-API-CORRELATION-ID") String correlationMuleReqHeader,
			@RequestHeader(required = false, value = "X-API-PARTY-SOURCE-ID-NUMBER") String partySourceId,
			@RequestHeader(required = false, value = "X-API-SOURCE-SYSTEM") String sourcesystem, 
			@RequestHeader(required = false, value = "X-API-CHANNEL-BRAND") String channelBrand) throws MissingAuthenticationHeaderException, RecordNotFoundException,Exception {
		
		if(sourcesystem == null  || channelBrand == null) {
			
			throw new MissingAuthenticationHeaderException("Header Missing");
		}
		
		
		validationUtility.validateMockBusinessValidationsForSchedulePayment(domesticPaymentRequest.getPaymentInstructionProposal().getInstructionReference());
		
		
		PaymentInstructionProposalComposite domesticPaymentResponse = new PaymentInstructionProposalComposite();
		
		domesticPaymentResponse = domesticPaymentService.createDomesticPaymentSubmissionResource(domesticPaymentRequest);
		
		if (null == domesticPaymentResponse) {

			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.RESOURCE_NOT_FOUND_PPD_PIP);
		}
		
		return new ResponseEntity<>(domesticPaymentResponse, HttpStatus.CREATED);
		
		
	}


}
