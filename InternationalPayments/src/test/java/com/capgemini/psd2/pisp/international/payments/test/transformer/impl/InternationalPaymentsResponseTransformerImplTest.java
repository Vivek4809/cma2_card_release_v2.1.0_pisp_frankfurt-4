package com.capgemini.psd2.pisp.international.payments.test.transformer.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomIPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentInternationalSubmitPOST201Response;
import com.capgemini.psd2.pisp.international.payments.test.mockdata.IPResponseTransformerImplTestMockdata;
import com.capgemini.psd2.pisp.international.payments.transformer.impl.InternationalPaymentsResponseTransformerImpl;
import com.capgemini.psd2.pisp.payment.submission.platform.adapter.PaymentsPlatformAdapter;
import com.capgemini.psd2.pisp.status.PaymentConstants;

@RunWith(SpringJUnit4ClassRunner.class)
public class InternationalPaymentsResponseTransformerImplTest {

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private PaymentsPlatformAdapter adapter;

	@InjectMocks
	private InternationalPaymentsResponseTransformerImpl transformer;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	/*
	 * * @Test public void testPaymentTransformerPOST() {
	 * PaymentInternationalSubmitPOST201Response paymentSubmissionResponse =
	 * IPResponseTransformerImplTestMockdata .getPOST201response(); Map<String,
	 * Object> paymentsPlatformResourceMap = IPResponseTransformerImplTestMockdata
	 * .getpaymentsPlatformResourceMap(); PaymentInternationalSubmitPOST201Response
	 * object = transformer
	 * .paymentSubmissionResponseTransformer(paymentSubmissionResponse,
	 * paymentsPlatformResourceMap, "POST"); assertNotNull(object); }
	 * 
	 * 
	 * @Test public void testPaymentTransformer() {
	 * PaymentInternationalSubmitPOST201Response paymentSubmissionResponse =
	 * IPResponseTransformerImplTestMockdata .getPOST201response(); Map<String,
	 * Object> paymentsPlatformResourceMap = IPResponseTransformerImplTestMockdata
	 * .getpaymentsPlatformResourceMap(); PaymentInternationalSubmitPOST201Response
	 * object = transformer
	 * .paymentSubmissionResponseTransformer(paymentSubmissionResponse,
	 * paymentsPlatformResourceMap, "POST"); assertNotNull(object); }
	 */

	@Test
	public void testPaymentTransformerAUTHORISED() {
		PaymentInternationalSubmitPOST201Response paymentSubmissionResponse = IPResponseTransformerImplTestMockdata
				.getPOST201response();
		Map<String, Object> paymentsPlatformResourceMap = IPResponseTransformerImplTestMockdata
				.getpaymentsPlatformResourceMap2();
		PaymentConsentsPlatformResource paymentConsentsPlatformResource = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResource.setPaymentConsentId("q121");
		paymentConsentsPlatformResource.setTppDebtorDetails("False");
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, paymentConsentsPlatformResource);
		PaymentInternationalSubmitPOST201Response object = transformer
				.paymentSubmissionResponseTransformer(paymentSubmissionResponse, paymentsPlatformResourceMap, "GET");
		assertNotNull(object);
	}

	@Test
	public void testPaymentTransformerAUTHORISED1() {
		PaymentInternationalSubmitPOST201Response paymentSubmissionResponse = IPResponseTransformerImplTestMockdata
				.getPOST201responseDetbtandCredScehemmismatch();
		Map<String, Object> paymentsPlatformResourceMap = IPResponseTransformerImplTestMockdata
				.getpaymentsPlatformResourceMap2();
		PaymentConsentsPlatformResource paymentConsentsPlatformResource = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResource.setPaymentConsentId("q121");
		paymentConsentsPlatformResource.setTppDebtorDetails("False");
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, paymentConsentsPlatformResource);
		PaymentInternationalSubmitPOST201Response object = transformer
				.paymentSubmissionResponseTransformer(paymentSubmissionResponse, paymentsPlatformResourceMap, "GET");
		assertNotNull(object);
	}

	@Test
	public void testPaymentTransformerAUTHORISED2() {
		PaymentInternationalSubmitPOST201Response paymentSubmissionResponse = IPResponseTransformerImplTestMockdata
				.getPOST201responseMetaandLinks();
		Map<String, Object> paymentsPlatformResourceMap = IPResponseTransformerImplTestMockdata
				.getpaymentsPlatformResourceMap2();
		PaymentConsentsPlatformResource paymentConsentsPlatformResource = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResource.setPaymentConsentId("q121");
		paymentConsentsPlatformResource.setTppDebtorDetails("False");
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, paymentConsentsPlatformResource);
		PaymentInternationalSubmitPOST201Response object = transformer
				.paymentSubmissionResponseTransformer(paymentSubmissionResponse, paymentsPlatformResourceMap, "GET");
		assertNotNull(object);
	}

	@Test
	public void testPaymentTransformerPending() {
		PaymentInternationalSubmitPOST201Response paymentSubmissionResponse = IPResponseTransformerImplTestMockdata
				.getPOST201response();
		Map<String, Object> paymentsPlatformResourceMap = IPResponseTransformerImplTestMockdata
				.getpaymentsPlatformResourceMap();
		PaymentConsentsPlatformResource paymentConsentsPlatformResource = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResource.setPaymentConsentId("q121");
		paymentConsentsPlatformResource.setTppDebtorDetails("True");
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, paymentConsentsPlatformResource);
		PaymentInternationalSubmitPOST201Response object = transformer
				.paymentSubmissionResponseTransformer(paymentSubmissionResponse, paymentsPlatformResourceMap, "GET");
		assertNotNull(object);
	}

	@Test
	public void testPaymentTransformerPending1() {
		PaymentInternationalSubmitPOST201Response paymentSubmissionResponse = IPResponseTransformerImplTestMockdata
				.getPOST201response1();
		Map<String, Object> paymentsPlatformResourceMap = IPResponseTransformerImplTestMockdata
				.getpaymentsPlatformResourceMap();
		PaymentConsentsPlatformResource paymentConsentsPlatformResource = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResource.setPaymentConsentId("q121");
		paymentConsentsPlatformResource.setTppDebtorDetails("False");
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, paymentConsentsPlatformResource);
		PaymentInternationalSubmitPOST201Response object = transformer
				.paymentSubmissionResponseTransformer(paymentSubmissionResponse, paymentsPlatformResourceMap, "GET");
		assertNotNull(object);
	}

	@Test
	public void testPaymentTransformerAUTHORISEDPOST() {
		PaymentInternationalSubmitPOST201Response paymentSubmissionResponse = IPResponseTransformerImplTestMockdata
				.getPOST201response();
		Map<String, Object> paymentsPlatformResourceMap = IPResponseTransformerImplTestMockdata
				.getpaymentsPlatformResourceMap3();
		PaymentInternationalSubmitPOST201Response object = transformer
				.paymentSubmissionResponseTransformer(paymentSubmissionResponse, paymentsPlatformResourceMap, "POST");
		assertNotNull(object);
	}

	@Test
	public void testPaymentTransformerAWAITINGFURTHERAUTHORISATION() {
		PaymentInternationalSubmitPOST201Response paymentSubmissionResponse = IPResponseTransformerImplTestMockdata
				.getPOST201response();
		paymentSubmissionResponse.getData().getMultiAuthorisation()
				.setStatus(OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION);
		Map<String, Object> paymentsPlatformResourceMap = IPResponseTransformerImplTestMockdata
				.getpaymentsPlatformResourceMap();
		PaymentConsentsPlatformResource paymentConsentsPlatformResource = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResource.setPaymentConsentId("q121");
		paymentConsentsPlatformResource.setTppDebtorDetails("False");
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, paymentConsentsPlatformResource);
		transformer.paymentSubmissionResponseTransformer(paymentSubmissionResponse, paymentsPlatformResourceMap, "GET");

	}

	@Test
	public void testPaymentTransformerREJECTED() {
		PaymentInternationalSubmitPOST201Response paymentSubmissionResponse = IPResponseTransformerImplTestMockdata
				.getPOST201response();
		paymentSubmissionResponse.getData().getMultiAuthorisation().setStatus(OBExternalStatus2Code.REJECTED);
		Map<String, Object> paymentsPlatformResourceMap = IPResponseTransformerImplTestMockdata
				.getpaymentsPlatformResourceMap();

		PaymentConsentsPlatformResource paymentConsentsPlatformResource = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResource.setPaymentConsentId("q121");
		paymentConsentsPlatformResource.setTppDebtorDetails("False");
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, paymentConsentsPlatformResource);
		transformer.paymentSubmissionResponseTransformer(paymentSubmissionResponse, paymentsPlatformResourceMap, "GET");

	}

	@Test
	public void testPaymentTransformerACCEPTEDSETTLEMENTCOMPLETED() {
		PaymentInternationalSubmitPOST201Response paymentSubmissionResponse = IPResponseTransformerImplTestMockdata
				.getPOST201response();
		paymentSubmissionResponse.getData().setStatus(OBTransactionIndividualStatus1Code.ACCEPTEDSETTLEMENTCOMPLETED);
		Map<String, Object> paymentsPlatformResourceMap = IPResponseTransformerImplTestMockdata
				.getpaymentsPlatformResourceMap1();
		PaymentConsentsPlatformResource paymentConsentsPlatformResource = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResource.setPaymentConsentId("q121");
		paymentConsentsPlatformResource.setTppDebtorDetails("False");
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, paymentConsentsPlatformResource);
		PaymentInternationalSubmitPOST201Response object = transformer
				.paymentSubmissionResponseTransformer(paymentSubmissionResponse, paymentsPlatformResourceMap, "GET");

		assertNotNull(object);
	}

	@Test
	public void testpaymentSubmissionRequestTransformer() {
		CustomIPaymentsPOSTRequest request = new CustomIPaymentsPOSTRequest();
		transformer.paymentSubmissionRequestTransformer(request);
	}

	@After
	public void tearDown() throws Exception {
		reqHeaderAtrributes = null;
		adapter = null;
		transformer = null;
	}

}
