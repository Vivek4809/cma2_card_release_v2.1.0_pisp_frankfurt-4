/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.beneficiaries.routing.adapter.impl;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import com.capgemini.psd2.account.beneficiaries.routing.adapter.routing.AccountBeneficiariesAdapterFactory;
import com.capgemini.psd2.aisp.adapter.AccountBeneficiariesAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBeneficiariesResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The Class AccountBeneficiariesRoutingAdapter.
 */
@Component("accountBeneficiariesRoutingAdapter")
public class AccountBeneficiariesRoutingAdapter implements AccountBeneficiariesAdapter {

	/** The account beneficiaries adapter factory. */
	@Autowired
	private AccountBeneficiariesAdapterFactory factory;
	
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The default adapter. */
	@Value("${app.defaultAccountBeneficiariesAdapter}")
	private String defaultAdapter;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.aisp.adapter.AccountInformationAdapter#
	 * retrieveAccountInformation(com.capgemini.psd2.aisp.domain.AccountMapping,
	 * java.util.Map)
	 */
	@Override
	public PlatformAccountBeneficiariesResponse retrieveAccountBeneficiaries(AccountMapping accountMapping,
			Map<String, String> params) {
		
		AccountBeneficiariesAdapter accountBeneficiariesAdapter = factory
				.getAdapterInstance(defaultAdapter);
		return accountBeneficiariesAdapter.retrieveAccountBeneficiaries(accountMapping, addHeaderParams(params));
	}
	
	private Map<String, String> addHeaderParams(Map<String, String> mapParam) {
		if (NullCheckUtils.isNullOrEmpty(mapParam))
			mapParam = new HashMap<>();

		if (null != reqHeaderAtrributes.getToken() && null != reqHeaderAtrributes.getToken().getSeviceParams()) {
			mapParam.put(PSD2Constants.CHANNEL_IN_REQ_HEADER,
					reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));
		}
		mapParam.put(PSD2Constants.USER_IN_REQ_HEADER, reqHeaderAtrributes.getPsuId());
		mapParam.put(PSD2Constants.CORRELATION_REQ_HEADER, reqHeaderAtrributes.getCorrelationId());
		mapParam.put(PSD2Constants.TENANT_ID, reqHeaderAtrributes.getTenantId());
		return mapParam;
	}
}