package com.capgemini.psd2.pisp.international.standing.order.consent.transformer.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.processing.adapter.PaymentConsentsTransformer;
import com.capgemini.psd2.pisp.status.PaymentStatusCompatibilityMapEnum;
import com.capgemini.psd2.pisp.utilities.PispDateUtility;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.pisp.validation.adapter.constants.PaymentSetupConstants;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component("internationalStandingOrderTransformer")
public class IStandingOrderConsentsResponseTransformerImpl implements
		PaymentConsentsTransformer<CustomIStandingOrderConsentsPOSTResponse, CustomIStandingOrderConsentsPOSTRequest> {

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Autowired
	private PispDateUtility pispDateUtility;

	@Override
	public CustomIStandingOrderConsentsPOSTResponse paymentConsentsResponseTransformer(
			CustomIStandingOrderConsentsPOSTResponse standingOrderConsentsTppResponse,
			PaymentConsentsPlatformResource paymentSetupPlatformResource, String methodType) {
		/*
		 * These fields are expected to already be populated in
		 * domesticStandingOrderTppResponse from Domestic Stage Service : Charges,
		 * CutOffDateTime
		 */

		/* Fields from platform Resource */
		standingOrderConsentsTppResponse.getData().setCreationDateTime(paymentSetupPlatformResource.getCreatedAt());
		String setupCompatibleStatus = PaymentStatusCompatibilityMapEnum
				.valueOf(paymentSetupPlatformResource.getStatus().toUpperCase()).getCompatibleStatus();
		standingOrderConsentsTppResponse.getData()
				.setStatus(OBExternalConsentStatus1Code.fromValue(setupCompatibleStatus));
		standingOrderConsentsTppResponse.getData().setStatusUpdateDateTime(
				NullCheckUtils.isNullOrEmpty(paymentSetupPlatformResource.getStatusUpdateDateTime())
						? paymentSetupPlatformResource.getUpdatedAt()
						: paymentSetupPlatformResource.getStatusUpdateDateTime());

		String creditorAccountScheme = standingOrderConsentsTppResponse.getData().getInitiation().getCreditorAccount()
				.getSchemeName();
		if (PaymentSetupConstants.prefixToAddSchemeNameList().contains(creditorAccountScheme)) {
			standingOrderConsentsTppResponse.getData().getInitiation().getCreditorAccount()
					.setSchemeName(PSD2Constants.OB_SCHEME_PREFIX.concat(creditorAccountScheme));
		}

		String debtorAccountScheme = standingOrderConsentsTppResponse.getData().getInitiation()
				.getDebtorAccount() != null
						? standingOrderConsentsTppResponse.getData().getInitiation().getDebtorAccount().getSchemeName()
						: null;

		if (debtorAccountScheme != null
				&& PaymentSetupConstants.prefixToAddSchemeNameList().contains(debtorAccountScheme)) {
			standingOrderConsentsTppResponse.getData().getInitiation().getDebtorAccount()
					.setSchemeName(PSD2Constants.OB_SCHEME_PREFIX.concat(debtorAccountScheme));
		}
		/*
		 * If debtorFlag is 'False' then DebtorAccount & DebtorAgent details do not have
		 * to send in response. debtorFlag would be set as Null DebtorAgent is not
		 * available in CMA3
		 */

		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails()))
			standingOrderConsentsTppResponse.getData().getInitiation().setDebtorAccount(null);

		/*
		 * If tppDebtorNameFlag is 'False' then DebtorAccount.name should not be sent to
		 * TPP.
		 */
		if (Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& !Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorNameDetails()))
			standingOrderConsentsTppResponse.getData().getInitiation().getDebtorAccount().setName(null);

		if (standingOrderConsentsTppResponse.getLinks() == null)
			standingOrderConsentsTppResponse.setLinks(new PaymentSetupPOSTResponseLinks());

		standingOrderConsentsTppResponse.getLinks().setSelf(PispUtilities.populateLinks(
				paymentSetupPlatformResource.getPaymentConsentId(), methodType, reqHeaderAtrributes.getSelfUrl()));

		if (standingOrderConsentsTppResponse.getMeta() == null)
			standingOrderConsentsTppResponse.setMeta(new PaymentSetupPOSTResponseMeta());

		return standingOrderConsentsTppResponse;
	}

	@Override
	public CustomIStandingOrderConsentsPOSTRequest paymentConsentRequestTransformer(
			CustomIStandingOrderConsentsPOSTRequest paymentConsentRequest) {

		if (!NullCheckUtils.isNullOrEmpty(paymentConsentRequest.getData().getAuthorisation())) {
			paymentConsentRequest.getData().getAuthorisation()
					.setCompletionDateTime(pispDateUtility.transformDateTimeInRequest(
							paymentConsentRequest.getData().getAuthorisation().getCompletionDateTime()));
		}

		paymentConsentRequest.getData().getInitiation().setFirstPaymentDateTime(pispDateUtility
				.transformDateTimeInRequest(paymentConsentRequest.getData().getInitiation().getFirstPaymentDateTime()));
		paymentConsentRequest.getData().getInitiation().setFinalPaymentDateTime(pispDateUtility
				.transformDateTimeInRequest(paymentConsentRequest.getData().getInitiation().getFinalPaymentDateTime()));
		return paymentConsentRequest;
	}

}
