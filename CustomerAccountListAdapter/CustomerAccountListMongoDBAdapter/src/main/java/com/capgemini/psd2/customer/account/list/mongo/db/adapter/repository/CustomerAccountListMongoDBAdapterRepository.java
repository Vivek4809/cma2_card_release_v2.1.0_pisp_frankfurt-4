package com.capgemini.psd2.customer.account.list.mongo.db.adapter.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.aisp.domain.OBAccount2;
import com.capgemini.psd2.cisp.mock.domain.MockChannelProfile;

public interface CustomerAccountListMongoDBAdapterRepository extends MongoRepository<MockChannelProfile, String> {
	public List<OBAccount2> findByPsuId(String userId);
}
