package com.capgemini.psd2.security.saas.services;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.scaconsenthelper.config.helpers.MainPickupDataService;
import com.capgemini.psd2.scaconsenthelper.constants.OIDCConstants;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;

@Service
public class SaaSPickupDataService extends MainPickupDataService {

	public PickupDataModel populateIntentData(String jsonResponse) throws ParseException {

		PickupDataModel intentData = new PickupDataModel();

		JSONParser parser = new JSONParser();

		String intentId = null;

		JSONObject jsonObject = (JSONObject) parser.parse(jsonResponse);
		String claims = (String) jsonObject.get(OIDCConstants.REQUEST);
		String channelId = (String) jsonObject.get(OIDCConstants.CHANNEL_ID);
		String correlationId = (String) jsonObject.get(PSD2Constants.CORRELATION_ID);

		if (correlationId == null || correlationId.trim().length() == 0) {
			correlationId = (String) jsonObject.get(PSD2Constants.CO_RELATION_ID);
		}

		Jwt jwt = JwtHelper.decode(claims);
		JSONObject claimsJsonObject = (JSONObject) parser.parse(jwt.getClaims());
		String scopes = (String) claimsJsonObject.get(OIDCConstants.SCOPE);
		String clientId = (String) claimsJsonObject.get(OIDCConstants.CLIENT_ID);

		if (clientId == null) {
			clientId = (String) jsonObject.get(OIDCConstants.CLIENT_ID);
		}

		if (scopes != null && !scopes.isEmpty() && scopes.contains(OIDCConstants.OPENID)) {
			scopes = scopes.replace(OIDCConstants.OPENID, StringUtils.EMPTY).trim();
		}
		JSONObject intentJsonObject = (JSONObject) claimsJsonObject.get(OIDCConstants.CLAIMS);

		if (intentJsonObject.get(OIDCConstants.ID_TOKEN) != null) {
			JSONObject intentObj = (JSONObject) ((JSONObject) intentJsonObject.get(OIDCConstants.ID_TOKEN))
					.get(OIDCConstants.OPENBANKING_INTENT_ID);
			if (intentObj != null && intentObj.get(OIDCConstants.VALUE) != null) {
				intentId = ((String) intentObj.get(OIDCConstants.VALUE)).substring(
						((String) intentObj.get(OIDCConstants.VALUE)).lastIndexOf(':') + 1,
						((String) intentObj.get(OIDCConstants.VALUE)).length());

			}
		}
		if(null!=jsonObject.get("tenant_id")){
			String tenantid = (String) jsonObject.get("tenant_id");
			intentData.setTenant_id(tenantid);
		}
		intentData.setChannelId(channelId);
		intentData.setClientId(clientId);
		intentData.setScope(scopes);
		intentData.setIntentId(intentId);
		intentData.setCorrelationId(correlationId);
		return intentData;
	}
}
