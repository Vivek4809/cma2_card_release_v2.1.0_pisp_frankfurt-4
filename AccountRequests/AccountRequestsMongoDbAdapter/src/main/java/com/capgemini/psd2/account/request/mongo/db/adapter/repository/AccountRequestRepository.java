package com.capgemini.psd2.account.request.mongo.db.adapter.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.aisp.domain.OBReadDataResponse1;

public interface AccountRequestRepository extends MongoRepository<OBReadDataResponse1, String> {

	public OBReadDataResponse1 findByAccountRequestIdAndCmaVersionIn(String accountId,List<String> cmaVersion);
	
}
