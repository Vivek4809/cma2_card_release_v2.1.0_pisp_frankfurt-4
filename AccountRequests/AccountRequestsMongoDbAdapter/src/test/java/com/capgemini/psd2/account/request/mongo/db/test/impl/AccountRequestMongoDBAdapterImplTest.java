package com.capgemini.psd2.account.request.mongo.db.test.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.request.mongo.db.adapter.impl.AccountRequestMongoDbAdaptorImpl;
import com.capgemini.psd2.account.request.mongo.db.adapter.repository.AccountRequestCMA3Repository;
import com.capgemini.psd2.account.request.mongo.db.adapter.repository.AccountRequestRepository;
import com.capgemini.psd2.account.request.mongo.db.test.mock.data.AccountRequestMongoDBAdapterTestMockData;
import com.capgemini.psd2.aisp.consent.adapter.impl.AispConsentAdapterImpl;
import com.capgemini.psd2.aisp.domain.OBExternalRequestStatus1Code;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.aisp.domain.OBReadDataResponse1;
import com.capgemini.psd2.aisp.domain.OBReadDataResponse1.StatusEnum;
import com.capgemini.psd2.aisp.transformer.AccountRequestTransformer;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.product.common.CompatibleVersionList;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountRequestMongoDBAdapterImplTest {

	@Mock
	private AccountRequestCMA3Repository accountRequestRepository;

	@Mock
	private AccountRequestRepository accountRequestRepositoryV2;

	@Mock
	private AccountRequestTransformer accountRequestTransformer;

	@InjectMocks
	private AccountRequestMongoDbAdaptorImpl accountRequestMongoDbAdaptorImpl;

	@Mock
	private AispConsentAdapterImpl aispConsentAdapter;

	@Mock
	private CompatibleVersionList compatibleVersionList;

	@Mock
	private RequestHeaderAttributes requestHeaderAttribute;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Map<String, String> map = new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap = new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content",
				"Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}

	@Test
	public void testAccountRequestPostRequestMongoDBAdapterImplSuccess() {

		OBReadConsentResponse1Data data = AccountRequestMongoDBAdapterTestMockData.getData();
		Mockito.when(accountRequestRepository.save(data)).thenReturn(data);
		OBReadConsentResponse1 accountRequestPOSTResponse = accountRequestMongoDbAdaptorImpl
				.createAccountRequestPOSTResponse(data);
		assertNotNull(accountRequestPOSTResponse);
		assertTrue(accountRequestPOSTResponse.getData().getStatus().equals(data.getStatus()));
	}

	@Test(expected = PSD2Exception.class)
	public void testAccountRequestPostRequestMongoDBAdapterImplException() {

		OBReadConsentResponse1Data data = AccountRequestMongoDBAdapterTestMockData.getData();
		Mockito.when(accountRequestRepository.save(data)).thenThrow(new DataAccessResourceFailureException("error"));
		accountRequestMongoDbAdaptorImpl.createAccountRequestPOSTResponse(data);
	}

	@Test
	public void testAccountRequestGetRequestMongoDBAdapterImplSuccess() {
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		OBReadConsentResponse1Data data = AccountRequestMongoDBAdapterTestMockData.getData();
		OBReadDataResponse1 data1 = AccountRequestMongoDBAdapterTestMockData.getDataResponse();
		Mockito.when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(), any())).thenReturn(data);
		Mockito.when(accountRequestRepositoryV2.findByAccountRequestIdAndCmaVersionIn(anyString(), any()))
				.thenReturn(data1);
		when(requestHeaderAttribute.getTppCID()).thenReturn("12345");
		when(accountRequestTransformer.transformAccountInformation(any(), any(), any())).thenReturn(data);
		Map<String, String> map = new HashMap<>();
		map.put(PSD2Constants.TPP_CID, data.getTppCID());
		OBReadConsentResponse1 accountRequestPOSTResponse = accountRequestMongoDbAdaptorImpl
				.getAccountRequestGETResponse("1222345");
		assertNotNull(accountRequestPOSTResponse);
	}

	@Test(expected = Exception.class)
	public void testAccountRequestGetRequestMongoDBAdapterImplException() {

		Mockito.when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(), any()))
				.thenThrow(new DataAccessResourceFailureException("error"));
		accountRequestMongoDbAdaptorImpl.getAccountRequestGETResponse("12345");
	}

	@Test(expected = Exception.class)
	public void testAccountRequestGetRequestMongoDBAdapterImplNoDataFoundError() {

		Mockito.when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(), any())).thenReturn(null);
		accountRequestMongoDbAdaptorImpl.getAccountRequestGETResponse("123355");
	}

	@Test
	public void testAccountRequestGetRequestMongoDBAdapterImplNoDataFoundError1() {

		Mockito.when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(), any())).thenReturn(null);
		OBReadDataResponse1 dataV2 = new OBReadDataResponse1();
		Mockito.when(accountRequestRepositoryV2.findByAccountRequestIdAndCmaVersionIn(anyString(), any()))
				.thenReturn(dataV2);
		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		Mockito.when(accountRequestTransformer.transformAccountInformation(any(), any(), any())).thenReturn(data);
		accountRequestMongoDbAdaptorImpl.getAccountRequestGETResponse("123355");
	}
	
	@Test(expected = PSD2Exception.class)
	public void testAccountRequestGetRequestMongoDBAdapterImpl_cidNotNull() {

		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		Mockito.when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(), any())).thenReturn(data );
		String cid= "TestCID";
		data.setTppCID("CID");
		Mockito.when(requestHeaderAttribute.getTppCID()).thenReturn(cid);
		accountRequestMongoDbAdaptorImpl.getAccountRequestGETResponse("123355");
	}

	@Test
	public void testAccountRequestUpateRequestMongoDBAdapterImplSuccess() {

		OBReadConsentResponse1Data data = AccountRequestMongoDBAdapterTestMockData.getData();
		Map<String, String> map = new HashMap<>();
		map.put(PSD2Constants.TPP_CID, data.getTppCID());
		OBReadConsentResponse1Data updatedData = AccountRequestMongoDBAdapterTestMockData.getData();
		updatedData.setStatus(OBExternalRequestStatus1Code.AUTHORISED);
		Mockito.when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(), any())).thenReturn(data);
		Mockito.when(accountRequestRepository.save(data)).thenReturn(updatedData);
		OBReadConsentResponse1 accountRequestPOSTResponse = accountRequestMongoDbAdaptorImpl
				.updateAccountRequestResponse("123455", OBExternalRequestStatus1Code.AUTHORISED);
		assertNotNull(accountRequestPOSTResponse);
		assertTrue(accountRequestPOSTResponse.getData().getStatus().equals(updatedData.getStatus()));
	}

	@Test(expected = PSD2Exception.class)
	public void testAccountRequestUpdateRequestMongoDBAdapterImplException() {

		OBReadConsentResponse1Data data = AccountRequestMongoDBAdapterTestMockData.getData();
		Map<String, String> map = new HashMap<>();
		map.put(PSD2Constants.TPP_CID, data.getTppCID());
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		Mockito.when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(), any())).thenReturn(data);
		Mockito.when(accountRequestRepository.save(data)).thenThrow(new DataAccessResourceFailureException("error"));
		accountRequestMongoDbAdaptorImpl.updateAccountRequestResponse("12345", OBExternalRequestStatus1Code.AUTHORISED);
	}

	@Test(expected = PSD2Exception.class)
	public void testAccountRequestUpdateRequestMongoDBAdapterImplException2() {
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		OBReadConsentResponse1Data data = AccountRequestMongoDBAdapterTestMockData.getData();
		Mockito.when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(), any()))
				.thenThrow(new DataAccessResourceFailureException("error"));
		Mockito.when(accountRequestRepository.save(data)).thenReturn(data);
		accountRequestMongoDbAdaptorImpl.updateAccountRequestResponse("12345", OBExternalRequestStatus1Code.AUTHORISED);
	}

	@Test
	public void testAccountRequestUpdateRequestMongoDBAdapterImplNoDataFoundException() {
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		Mockito.when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(), any())).thenReturn(null);
		OBReadDataResponse1 dataV2 = new OBReadDataResponse1();
		Mockito.when(accountRequestRepositoryV2.findByAccountRequestIdAndCmaVersionIn(anyString(), any()))
				.thenReturn(dataV2);
		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		Mockito.when(accountRequestTransformer.transformAccountInformation(any(), any(), any())).thenReturn(data);

		accountRequestMongoDbAdaptorImpl.updateAccountRequestResponse("12345", OBExternalRequestStatus1Code.AUTHORISED);
	}

	@Test
	public void testAccountRequestDeleteRequestMongoDBAdapterImplSuccess() {

		OBReadConsentResponse1Data data = AccountRequestMongoDBAdapterTestMockData.getData();
		Map<String, String> map = new HashMap<>();
		map.put(PSD2Constants.TPP_CID, data.getTppCID());
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		Mockito.when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(), any())).thenReturn(data);
		data.setStatus(OBExternalRequestStatus1Code.AUTHORISED);
		Mockito.when(accountRequestRepository.save(data)).thenReturn(data);
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(data.getConsentId(),
				ConsentStatusEnum.AUTHORISED)).thenReturn(new AispConsent());
		Mockito.doNothing().when(aispConsentAdapter).updateConsentStatus(any(), any());

		AispConsent aispConsent = new AispConsent();
		aispConsent.setAccountRequestId("623e9088-6855-4ce6-8fe6-a7ff906ce477");
		aispConsent.setConsentId("44483470");
		aispConsent.setStatus(ConsentStatusEnum.AUTHORISED);

		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(anyString(), any()))
				.thenReturn(aispConsent);
		Mockito.doNothing().when(aispConsentAdapter).updateConsentStatus(aispConsent.getConsentId(),
				ConsentStatusEnum.REVOKED);

		accountRequestMongoDbAdaptorImpl.removeAccountRequest("12345", data.getTppCID());
	}

	@Test
	public void testAccountRequestDeleteRequestMongoDBAdapterImplSuccessNullAccessToken() {

		OBReadConsentResponse1Data data = AccountRequestMongoDBAdapterTestMockData.getData();
		Map<String, String> map = new HashMap<>();
		map.put(PSD2Constants.TPP_CID, data.getTppCID());
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		Mockito.when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(), any())).thenReturn(data);
		data.setStatus(OBExternalRequestStatus1Code.AUTHORISED);
		Mockito.when(accountRequestRepository.save(data)).thenReturn(data);
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(data.getConsentId(),
				ConsentStatusEnum.AUTHORISED)).thenReturn(new AispConsent());
		Mockito.doNothing().when(aispConsentAdapter).updateConsentStatus(any(), any());

		AispConsent aispConsent = new AispConsent();
		aispConsent.setAccountRequestId("623e9088-6855-4ce6-8fe6-a7ff906ce477");
		aispConsent.setConsentId("44483470");
		aispConsent.setStatus(ConsentStatusEnum.AUTHORISED);

		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(anyString(), any()))
				.thenReturn(aispConsent);
		Mockito.doNothing().when(aispConsentAdapter).updateConsentStatus(aispConsent.getConsentId(),
				ConsentStatusEnum.REVOKED);

		accountRequestMongoDbAdaptorImpl.removeAccountRequest("12345", data.getTppCID());
	}

	@Test(expected = PSD2Exception.class)
	public void testAccountRequestDeleteRequestMongoDBAdapterImplRevokeAccessTokenFailure() {

		OBReadConsentResponse1Data data = AccountRequestMongoDBAdapterTestMockData.getData();
		OBReadDataResponse1 data1 = AccountRequestMongoDBAdapterTestMockData.getDataResponse();
		Map<String, String> map = new HashMap<>();
		map.put(PSD2Constants.TPP_CID, data.getTppCID());
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		Mockito.when(accountRequestRepositoryV2.findByAccountRequestIdAndCmaVersionIn(anyString(), any()))
				.thenReturn(data1);
		data1.setStatus(StatusEnum.REVOKED);
		Mockito.when(accountRequestRepository.save(data)).thenReturn(data);
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(anyString(), any()))
				.thenReturn(new AispConsent());
		Mockito.doNothing().when(aispConsentAdapter).updateConsentStatus(any(), any());

		AispConsent aispConsent = new AispConsent();
		aispConsent.setAccountRequestId("623e9088-6855-4ce6-8fe6-a7ff906ce477");
		aispConsent.setConsentId("44483470");
		aispConsent.setStatus(ConsentStatusEnum.AUTHORISED);

		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString())).thenReturn(aispConsent);
		Mockito.doNothing().when(aispConsentAdapter).updateConsentStatus(aispConsent.getConsentId(),
				ConsentStatusEnum.REVOKED);

		accountRequestMongoDbAdaptorImpl.removeAccountRequest("44483470", data.getTppCID());
	}

	@Test(expected = PSD2Exception.class)
	public void testAccountRequestRemoveRequestMongoDBAdapterImplNoDataFoundException() {

		Mockito.when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(), any()))
				.thenThrow(new DataAccessResourceFailureException("error"));
		accountRequestMongoDbAdaptorImpl.removeAccountRequest("12345", null);
	}

	@Test(expected = PSD2Exception.class)
	public void testAccountRequestRemoveRequestMongoDBAdapterImplException() {

		OBReadConsentResponse1Data data = AccountRequestMongoDBAdapterTestMockData.getData();
		Map<String, String> map = new HashMap<>();
		map.put(PSD2Constants.TPP_CID, data.getTppCID());
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		Mockito.when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(), any())).thenReturn(data);
		data.setStatus(OBExternalRequestStatus1Code.REVOKED);
		Mockito.when(accountRequestRepository.save(data)).thenThrow(new DataAccessResourceFailureException("error"));
		accountRequestMongoDbAdaptorImpl.removeAccountRequest("12345", data.getTppCID());
	}

	@Test(expected = Exception.class)
	public void testAccountRequestRemoveRequestMongoDBAdapterImplNullDataException() {
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		Mockito.when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(), any())).thenReturn(null);
		accountRequestMongoDbAdaptorImpl.removeAccountRequest("12345", null);
	}

	@Test(expected = PSD2Exception.class)
	public void testAccountRequestRemoveRequestMongoDBAdapterInvalidTppAccess() {
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		OBReadConsentResponse1Data data = AccountRequestMongoDBAdapterTestMockData.getData();
		OBReadDataResponse1 data1 = AccountRequestMongoDBAdapterTestMockData.getDataResponse();
		Mockito.when(accountRequestRepositoryV2.findByAccountRequestIdAndCmaVersionIn(anyString(), any()))
				.thenReturn(data1);
		when(accountRequestTransformer.transformAccountInformation(any(), any(), any())).thenReturn(data);
		Map<String, String> map = new HashMap<>();
		map.put(PSD2Constants.TPP_CID, AccountRequestMongoDBAdapterTestMockData.getDataInvalidTppAccess().getTppCID());
		accountRequestMongoDbAdaptorImpl.removeAccountRequest("12345", "1234");
	}

}
