package com.capgemini.psd2.account.request.mongo.db.transaformer.test.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.request.mongo.db.adapter.transformer.impl.AccountRequestTransformerImpl;
import com.capgemini.psd2.account.request.mongo.db.test.mock.data.AccountRequestMongoDBAdapterTestMockData;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.aisp.domain.OBReadDataResponse1;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountRequestTransaformerImplTest {
	
	@InjectMocks
	private AccountRequestTransformerImpl accountRequestTransformerImpl;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}
	
	@Test
	public void testTransformAccountInformation(){
		OBReadDataResponse1 sourceData = AccountRequestMongoDBAdapterTestMockData.getDataResponse();
		OBReadConsentResponse1Data destinationData = new OBReadConsentResponse1Data();
		Map<String, String> params = new HashMap<>();
		OBReadConsentResponse1Data data = accountRequestTransformerImpl.
				transformAccountInformation(sourceData, destinationData, params);
		assertNotNull(data);
		assertTrue(data.getStatus().toString().toUpperCase().equals(sourceData.getStatus().toString().toUpperCase()));
	}

}
