package com.capgemini.psd2.account.request.service;

import com.capgemini.psd2.aisp.domain.OBReadConsent1;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;

public interface AccountRequestService {
	

	public OBReadConsentResponse1 createAccountRequest(OBReadConsent1 accountRequestPOSTRequest);

	public OBReadConsentResponse1 retrieveAccountRequest(String consentId);

	public void removeAccountRequest(String consentId);

}
