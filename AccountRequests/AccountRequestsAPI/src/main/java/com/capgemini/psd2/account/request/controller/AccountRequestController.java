package com.capgemini.psd2.account.request.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.account.request.service.AccountRequestService;
import com.capgemini.psd2.aisp.domain.OBReadConsent1;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;

@RestController
public class AccountRequestController {

	@Autowired
	private AccountRequestService accountRequestService;
	
	@RequestMapping(value = "/account-access-consents", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseStatus(HttpStatus.CREATED)
	public OBReadConsentResponse1 createAccountRequest(
			@RequestBody OBReadConsent1 accountRequestPOSTRequest) {
		return accountRequestService.createAccountRequest(accountRequestPOSTRequest);
	}

	@RequestMapping(value = "/account-access-consents/{ConsentId}", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public OBReadConsentResponse1 getAccountRequest(@PathVariable("ConsentId") String consentId) {
		return accountRequestService.retrieveAccountRequest(consentId);
	}

	@RequestMapping(value = {"/account-access-consents/{ConsentId}" , "/internal/account-access-consents/{ConsentId}"}, method = RequestMethod.DELETE, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteAccountRequest(@PathVariable("ConsentId") String consentId) {
		accountRequestService.removeAccountRequest(consentId);
		
	}
}
