/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.transaction.routing.adapter.test.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.account.transaction.routing.adapter.impl.AccountTransactionRoutingAdapter;
import com.capgemini.psd2.account.transaction.routing.adapter.routing.AccountTransactionAdapterFactory;
import com.capgemini.psd2.account.transcation.routing.adapter.test.adapter.AccountTransactionRoutingTestAdapter;
import com.capgemini.psd2.aisp.adapter.AccountTransactionAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.token.Token;

/**
 * The Class AccountTransactionRoutingAdapterTest.
 */
public class AccountTransactionRoutingAdapterTest{

	/** The account transaction adapter factory. */
	@Mock
	private AccountTransactionAdapterFactory accountTransactionAdapterFactory;

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock 
	AccountMapping accountMapping;
	/** The account transaction routing adapter. */

	@InjectMocks
	private AccountTransactionAdapter accountTransactionAdapter = new AccountTransactionRoutingAdapter();

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Retrieve account transaction test.
	 */
	@Test(expected= Exception.class)
	public void retrieveAccountTransactiontest() {



		AccountTransactionAdapter accountTransactionAdapterTest =  new AccountTransactionRoutingTestAdapter();
		Mockito.when(accountTransactionAdapterFactory.getAdapterInstance(anyString()))
		.thenReturn(accountTransactionAdapterTest);

		Token token = new Token();
		Map<String, String> map  = new HashMap<>();
		token.setSeviceParams(map);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);

		PlatformAccountTransactionResponse platformAccountTransactionResponse = accountTransactionAdapter
				.retrieveAccountTransaction(null, null);

		assertTrue(
				platformAccountTransactionResponse.getoBReadTransaction3().getData().getTransaction().isEmpty());


	}



}
