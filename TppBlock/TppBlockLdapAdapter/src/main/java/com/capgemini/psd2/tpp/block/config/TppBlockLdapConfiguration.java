package com.capgemini.psd2.tpp.block.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.ContextSource;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;

@Configuration
@ConfigurationProperties("ldap")
public class TppBlockLdapConfiguration {

	@Value("${ldap.url}")
	private String url;

	@Value("${ldap.userDn}")
	private String userDn;

	@Value("${ldap.readwrite.password}")
	private String ldapPassword;
	
	Map<String,String> tppgroupbasedn = new HashMap<>();
	

	@Bean
	public ContextSource contextSource() {
		LdapContextSource contextSource = new LdapContextSource();
		contextSource.setUrl(url);
		contextSource.setUserDn(userDn);
		contextSource.setPassword(ldapPassword);
		return contextSource;
	}

	@Bean
	public LdapTemplate ldapTemplate() {
		return new LdapTemplate(contextSource());
	}
	

	public String getTenantSpecificTppBaseDn(String tenantId) {
		if(tppgroupbasedn.containsKey(tenantId))
			return tppgroupbasedn.get(tenantId);
		else
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,ErrorMapKeys.FIELD));
	}

	

	public Map<String, String> getTppgroupbasedn() {
		return tppgroupbasedn;
	}

	public void setTppgroupbasedn(Map<String, String> tppgroupbasedn) {
		this.tppgroupbasedn = tppgroupbasedn;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUserDn() {
		return userDn;
	}

	public void setUserDn(String userDn) {
		this.userDn = userDn;
	}

	public String getLdapPassword() {
		return ldapPassword;
	}

	public void setLdapPassword(String ldapPassword) {
		this.ldapPassword = ldapPassword;
	}

}