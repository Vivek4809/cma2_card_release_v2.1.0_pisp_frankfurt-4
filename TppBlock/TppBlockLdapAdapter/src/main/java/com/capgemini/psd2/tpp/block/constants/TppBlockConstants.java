package com.capgemini.psd2.tpp.block.constants;

public class TppBlockConstants {

	public static final String ASTRICK = "*";
	public static final String PLUS = "+";
	public static final String CN = "cn=";
	public static final String BLOCK_ATTRIBUTE = "x-block";
	public static final String COMMA = ",";

	private TppBlockConstants() {
		throw new IllegalAccessError("Constant Class");
	}
}
