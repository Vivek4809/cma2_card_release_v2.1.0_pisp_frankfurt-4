package com.capgemini.psd2.tpp.block.test.utilities;

import org.junit.Test;

import com.capgemini.psd2.internal.apis.domain.ActionEnum;
import com.capgemini.psd2.internal.apis.domain.TppBlockInput;
import com.capgemini.psd2.tpp.block.domain.TppBlockPathVariables;
import com.capgemini.psd2.tpp.block.utilities.TppBlockDataValidator;

public class TppBlockDataValidatorTest {

	@Test
	public void testValidateData() {
		TppBlockInput obj = new TppBlockInput();
		obj.setAction(ActionEnum.BLOCK);
		obj.setDescription("dummy desc");
		String tppId = "123456";
		TppBlockPathVariables pathVariables = new TppBlockPathVariables();
		pathVariables.setTppId(tppId);
		TppBlockDataValidator.validateData(obj, pathVariables);
	}

	@Test(expected = Exception.class)
	public void testValidateData1() {
		TppBlockInput obj = new TppBlockInput();
		obj.setAction(ActionEnum.BLOCK);
		obj.setDescription("dummy desc");
		String tppId = "123456";
		TppBlockPathVariables pathVariables = new TppBlockPathVariables();
		pathVariables.setTppId(tppId);
		TppBlockDataValidator.validateData(null, pathVariables);
	}

	@Test(expected = Exception.class)
	public void testValidateData2() {
		TppBlockInput obj = new TppBlockInput();
		obj.setAction(ActionEnum.BLOCK);
		obj.setDescription("dummy desc");
		TppBlockDataValidator.validateData(obj, null);
	}

	@Test(expected = Exception.class)
	public void testValidateData3() {
		TppBlockInput obj = new TppBlockInput();
		obj.setAction(null);
		obj.setDescription("dummy desc");
		String tppId = "123456";
		TppBlockPathVariables pathVariables = new TppBlockPathVariables();
		pathVariables.setTppId(tppId);
		TppBlockDataValidator.validateData(obj, pathVariables);
	}

	@Test(expected = Exception.class)
	public void testValidateData4() {
		TppBlockInput obj = new TppBlockInput();
		obj.setAction(ActionEnum.BLOCK);
		obj.setDescription(null);
		String tppId = "123456";
		TppBlockPathVariables pathVariables = new TppBlockPathVariables();
		pathVariables.setTppId(tppId);
		TppBlockDataValidator.validateData(obj, pathVariables);
	}
	
	
	@Test(expected = Exception.class)
	public void testValidateData5() {
		TppBlockInput obj = new TppBlockInput();
		obj.setAction(ActionEnum.BLOCK);
		obj.setDescription(null);
		TppBlockPathVariables pathVariables = new TppBlockPathVariables();
		TppBlockDataValidator.validateData(obj, pathVariables);
	}
}
