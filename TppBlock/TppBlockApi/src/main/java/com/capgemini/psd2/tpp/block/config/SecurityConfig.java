/**
 * 
 */
package com.capgemini.psd2.tpp.block.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Value("${auth.ldap.enabled}")
	private boolean authenticationEnabled;

	@Value("${auth.ldap.url}")
	private String ldapUrls;

	@Value("${auth.ldap.username}")
	private String ldapSecurityPrincipal;

	@Value("${ldap.read.password}")
	private String ldapPrincipalPassword;

	@Value("${auth.ldap.iuser.searchbase}")
	private String userSearchBase;

	@Value("${auth.ldap.iuser.searchfilter}")
	private String userSearchFilter;

	@Override
	protected void configure(HttpSecurity http) {
		try {
			if (authenticationEnabled) {
				http.csrf().disable().requestMatchers().antMatchers("/tpp/**").and().authorizeRequests()
						.antMatchers("/tpp/**").authenticated().and().httpBasic().and().sessionManagement()
						.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
			} else {
				http.csrf().disable().authorizeRequests().antMatchers("/").permitAll();
			}
		} catch (Exception e) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.TECHNICAL_ERROR);
		}
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) {
		try {
			auth.ldapAuthentication().contextSource().url(ldapUrls).managerDn(ldapSecurityPrincipal)
					.managerPassword(ldapPrincipalPassword).and().userSearchBase(userSearchBase)
					.userSearchFilter(userSearchFilter);
		} catch (Exception e) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.TECHNICAL_ERROR);
		}
	}

}
