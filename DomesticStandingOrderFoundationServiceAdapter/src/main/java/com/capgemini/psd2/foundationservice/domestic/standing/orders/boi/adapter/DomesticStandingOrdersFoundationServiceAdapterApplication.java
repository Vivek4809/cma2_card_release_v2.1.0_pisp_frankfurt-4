package com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter;



import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.client.DomesticStandingOrdersFoundationServiceClient;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.delegate.DomesticStandingOrdersFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.StandingOrderInstructionComposite;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.transformer.DomesticStandingOrdersFoundationServiceTransformer;
import com.capgemini.psd2.pisp.adapter.DomesticStandingOrderAdapter;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.DStandingOrderPOST201Response;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.rest.client.model.RequestInfo;



@Component("domesticStandingOrdersFoundationServiceAdapter")
public class DomesticStandingOrdersFoundationServiceAdapterApplication implements DomesticStandingOrderAdapter  {

	@Autowired
	private DomesticStandingOrdersFoundationServiceDelegate domStandingOrdersDelegate;

	@Autowired
	private DomesticStandingOrdersFoundationServiceClient domStandingOrdersClient;
	
	@Autowired
	private DomesticStandingOrdersFoundationServiceTransformer domPayTransformer;

	@Value("${foundationService.domesticStandingOrdersVersion}")
	private String domesticStandingOrdersVersion;
	
	@Override
	public DStandingOrderPOST201Response processDomesticStandingOrder(CustomDStandingOrderPOSTRequest request,
			Map<String, OBExternalStatus1Code> paymentStatusMap, Map<String, String> params) {

		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = domStandingOrdersDelegate.createPaymentRequestHeadersForProcess(params);
		String domesticPaymentPostURL = domStandingOrdersDelegate.postPaymentFoundationServiceURL(domesticStandingOrdersVersion);
		requestInfo.setUrl(domesticPaymentPostURL);
		StandingOrderInstructionComposite standingOrderInstructionCompositeRequest=domStandingOrdersDelegate.transformDomesticStandingOrdersResponseFromAPIToFDForInsert(request);
		StandingOrderInstructionComposite standingOrderInstructionCompositeResponse =domStandingOrdersClient.restTransportForDomesticStandingOrdersServicePost(requestInfo, standingOrderInstructionCompositeRequest,StandingOrderInstructionComposite.class , httpHeaders);
		return domPayTransformer.transformDomesticStandingOrdersResponseFromFDToAPIForInsert(standingOrderInstructionCompositeResponse);

	}

	@Override
	public DStandingOrderPOST201Response retrieveStagedDomesticStandingOrder(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
		
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = domStandingOrdersDelegate.createPaymentRequestHeadersForGet(params);
		String domStandingOrdersURL = domStandingOrdersDelegate.getPaymentFoundationServiceURL(customPaymentStageIdentifiers.getPaymentSubmissionId(),customPaymentStageIdentifiers.getPaymentSetupVersion());
        
		requestInfo.setUrl(domStandingOrdersURL);
	
		StandingOrderInstructionComposite standingInsProposal = domStandingOrdersClient.restTransportForDomesticStandingOrderServiceGet(requestInfo, StandingOrderInstructionComposite.class, httpHeaders);
		return domPayTransformer.transformDomesticStandingOrderResponse(standingInsProposal);
		
	}
}

