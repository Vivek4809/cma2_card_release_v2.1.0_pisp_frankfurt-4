/*************************************************************************
 * 
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 */
package com.capgemini.psd2.security.saas.controllers;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriUtils;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestMapping;
import com.capgemini.psd2.fraudsystem.utilities.FraudSystemUtilities;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.config.CdnConfig;
import com.capgemini.psd2.scaconsenthelper.constants.OIDCConstants;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.security.exceptions.PSD2AuthenticationException;
import com.capgemini.psd2.security.exceptions.PSD2SecurityException;
import com.capgemini.psd2.security.exceptions.SCAConsentErrorCodeEnum;
import com.capgemini.psd2.ui.content.utility.controller.UIStaticContentUtilityController;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.SandboxConfig;
import com.capgemini.psd2.utilities.ValidationUtility;

/**
 * ConsentView Controller
 * 
 * @author Capgemini
 *
 */
@Controller
public class SaaSController {

	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;

	@Autowired
	@Qualifier(value = "accountRequestMongoDbAdaptor")
	private AccountRequestAdapter accountRequestAdapter;

	@Value("${app.fraudSystem.resUrl}")
	private String resUrl;

	@Value("${app.fraudSystem.hdmUrl}")
	private String hdmUrl;

	@Value("${app.fraudSystem.hdmInputName}")
	private String hdmInputName;

	@Value("${app.fraudSystem.jsc-file-path}")
	private String jscFilePath;

	@Value("${spring.application.name}")
	private String applicationName;

	@Value("${foundationService.newB365SCAFlow}")
	private String newB365SCAFlow;
	
	@Autowired
	private UIStaticContentUtilityController uiController;	
	
	@Autowired
	private SandboxConfig sandboxConfig;
	


	@Autowired
	private HttpServletRequest request;

	@RequestMapping(value = "/errors", method = RequestMethod.PUT)
	@ResponseBody
	public String handleSessionTimeOutOnCancel(Model model) {
		error(model);
		return JSONUtilities.getJSONOutPutFromObject(model);
	}

	@RequestMapping("/sessionerrors")
	public String viewErrors(Map<String, Object> model){
		model.putAll(CdnConfig.populateCdnAttributes());
		model.put(PSD2SecurityConstants.LOGO_URL,uiController.retrieveUiParamValue(PSD2SecurityConstants.SCA_LOGO_URL_PATH));
		model.put(PSD2SecurityConstants.ERROR_GENERIC_MSG,uiController.retrieveUiParamValue(PSD2SecurityConstants.GENERIC_ERROR_PATH));
		model.put(PSD2SecurityConstants.ERROR_GENERIC_DESCRIPTION,uiController.retrieveUiParamValue(PSD2SecurityConstants.GENERIC_ERROR_DESCRIPTION_PATH));
		return "viewerrors";
	}


	@RequestMapping("/errors")
	public String error(Model model) {
		String statusCode = null;
		AuthenticationException exception = (AuthenticationException) request
				.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);

		ErrorInfo errorInfo = (ErrorInfo) request.getAttribute(SCAConsentHelperConstants.EXCEPTION);

		String resumePath = (String) request.getAttribute(PFConstants.RESUME_PATH);

		boolean serverError = Boolean.FALSE;

		if (resumePath == null || resumePath.isEmpty()) {
			if (request.getParameter(PFConstants.RESUME_PATH) != null
					&& !request.getParameter(PFConstants.RESUME_PATH).isEmpty()) {
				resumePath = request.getParameter(PFConstants.RESUME_PATH);
			} else if (request.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM) != null
					&& !request.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM).isEmpty()) {
				resumePath = request.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM);
			}
		}

		String currentCorrId =  requestHeaderAttributes.getCorrelationId();

		if(currentCorrId == null || currentCorrId.isEmpty()){
			currentCorrId = request.getParameter(PSD2Constants.CO_RELATION_ID);
			ValidationUtility.isValidUUID(currentCorrId);			
		}

		if (exception != null && exception instanceof PSD2AuthenticationException) {
			ErrorInfo authenticationErrorInfo = ((PSD2AuthenticationException) exception).getErrorInfo();
			statusCode = authenticationErrorInfo.getStatusCode();
		}

		else if (errorInfo != null) {
			model.addAttribute(SCAConsentHelperConstants.EXCEPTION, errorInfo);
			errorInfo.setDetailErrorMessage(null);
			statusCode = errorInfo.getStatusCode();
		}

		if (statusCode != null && !statusCode.isEmpty()) {
			serverError = statusCode.equalsIgnoreCase(Integer.toString(HttpStatus.INTERNAL_SERVER_ERROR.value()))
					|| Integer.parseInt(statusCode) > HttpStatus.INTERNAL_SERVER_ERROR.value();
		}

		model.addAttribute(PSD2Constants.SERVER_ERROR_FLAG_ATTR, serverError);

		model.addAttribute(PSD2Constants.CO_RELATION_ID, currentCorrId);

		model.addAttribute(PSD2SecurityConstants.REDIRECT_URI_MODEL_ATTRIBUTE, resumePath);
		model.addAttribute(PSD2Constants.JS_MSG,
				uiController.retrieveUiParamValue(PSD2Constants.ERROR_MESSAGE, PSD2Constants.JS_MSG));
		model.addAttribute(OIDCConstants.CHANNEL_ID, requestHeaderAttributes.getChannelId());
		model.addAttribute(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME, request.getHeader(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME));
		model.addAttribute(PSD2Constants.APPLICATION_NAME,applicationName);
		model.addAttribute(PSD2Constants.IS_SANDBOX_ENABLED,sandboxConfig.isSandboxEnabled());
		model.addAttribute(PSD2Constants.UICONTENT, uiController.getConfigVariable());
		model.addAllAttributes(CdnConfig.populateCdnAttributes());
		return "index";
	}

	@RequestMapping(value = "/signin")
	public ModelAndView signIn(@RequestParam("REF") String ref) throws JSONException {
		ModelAndView mv = new ModelAndView();
		mv.addObject("newB365SCAFlow", newB365SCAFlow);
		mv.addObject(PSD2Constants.JS_MSG,
				uiController.retrieveUiParamValue(PSD2Constants.ERROR_MESSAGE, PSD2Constants.JS_MSG));
		mv.addObject(PSD2Constants.CO_RELATION_ID, requestHeaderAttributes.getCorrelationId());

		JSONObject fraudHdmInfo = new JSONObject();
		try {
			fraudHdmInfo.put(PSD2SecurityConstants.RES_URL, UriUtils.encode(resUrl, StandardCharsets.UTF_8.toString()));
			fraudHdmInfo.put(PSD2SecurityConstants.HDM_URL, UriUtils.encode(hdmUrl, StandardCharsets.UTF_8.toString()));
		} catch (UnsupportedEncodingException e) {
			throw PSD2SecurityException.populatePSD2SecurityException(e.getMessage(),
					SCAConsentErrorCodeEnum.TECHNICAL_ERROR);
		}
		fraudHdmInfo.put(PSD2SecurityConstants.HDM_INPUT_NAME, hdmInputName);
		mv.addObject(PSD2SecurityConstants.FRAUD_HDM_INFO, fraudHdmInfo.toString());
		mv.addObject(PSD2SecurityConstants.JSC_FILE_PATH, jscFilePath);
		mv.addObject(OIDCConstants.CHANNEL_ID, requestHeaderAttributes.getChannelId());
		mv.addObject(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME, request.getHeader(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME));
		if(null==request.getHeader(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)){
			mv.addObject(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME, requestHeaderAttributes.getTenantId());
			}
		mv.addObject(PSD2Constants.APPLICATION_NAME,applicationName);
		mv.addObject(FraudSystemRequestMapping.FS_HEADERS, (Base64.getEncoder().encodeToString(FraudSystemUtilities.populateFraudSystemHeaders(request).getBytes())));
		mv.addObject(PSD2Constants.UICONTENT, uiController.getConfigVariable());
		mv.addAllObjects(CdnConfig.populateCdnAttributes());
		mv.addObject(PSD2Constants.IS_SANDBOX_ENABLED,sandboxConfig.isSandboxEnabled());
		mv.setViewName("index");
		return mv;
	}

	


}