package com.capgemini.psd2.security.saas.service;

import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServiceGetResponse;
import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServicePostRequest;
import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServicePostResponse;

public interface SaaSAppService {

	public CustomAuthenticationServiceGetResponse authenticateDOB(CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest);

	public CustomAuthenticationServiceGetResponse retrievePINandListOfDevices(String username);

	public CustomAuthenticationServicePostResponse submitPINDetailsandSelectedDevices(CustomAuthenticationServicePostRequest postRequest);
}
