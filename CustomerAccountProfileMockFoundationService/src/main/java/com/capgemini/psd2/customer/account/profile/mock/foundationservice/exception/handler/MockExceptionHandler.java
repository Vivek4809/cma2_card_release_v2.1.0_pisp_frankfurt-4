package com.capgemini.psd2.customer.account.profile.mock.foundationservice.exception.handler;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.capgemini.psd2.customer.account.profile.mock.foundationservice.domain.ValidationViolation;
import com.capgemini.psd2.customer.account.profile.mock.foundationservice.domain.ValidationViolations;
import com.capgemini.psd2.customer.account.profile.mock.foundationservice.exception.handler.InvalidParameterRequestException;
import com.capgemini.psd2.customer.account.profile.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.customer.account.profile.mock.foundationservice.exception.handler.MissingAuthenticationHeaderException;



@ControllerAdvice
public class MockExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(InvalidParameterRequestException.class)
	public final ResponseEntity<ValidationViolations> invalidParameterRequestException(Exception ex) {

		ValidationViolations validationViolations = new ValidationViolations();
		ValidationViolation exceptionResponce = new ValidationViolation();

		exceptionResponce.setErrorCode("FS_CAP_001");
		exceptionResponce.setErrorText("Bad Request. Please check your request");
		exceptionResponce.setErrorField("Bad Request. Please check your request");
		exceptionResponce.setErrorValue("Bad request");

		validationViolations.getValidationViolation().add(exceptionResponce);

		return new ResponseEntity<ValidationViolations>(validationViolations, new HttpHeaders(), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(MissingAuthenticationHeaderException.class)
	public final ResponseEntity<ValidationViolations> missingAuthenticationHeaderException(Exception ex) {

		ValidationViolations validationViolations = new ValidationViolations();
		ValidationViolation exceptionResponce = new ValidationViolation();

		exceptionResponce.setErrorCode("FS_CAP_002");
		exceptionResponce.setErrorText("You are not authorised to use this service");
		exceptionResponce.setErrorField("You are not authorised to use this service");
		exceptionResponce.setErrorValue("Header Missing");

		validationViolations.getValidationViolation().add(exceptionResponce);

		return new ResponseEntity<ValidationViolations>(validationViolations, new HttpHeaders(), HttpStatus.UNAUTHORIZED);
	}

	@ExceptionHandler(RecordNotFoundException.class)
	public final ResponseEntity<ValidationViolations> recordNotFound(Exception ex) {

		ValidationViolations validationViolations = new ValidationViolations();
		ValidationViolation exceptionResponce = new ValidationViolation();
        String user_id[]=ex.getMessage().split(":");
		exceptionResponce.setErrorCode("FS_CAP_003");
		exceptionResponce.setErrorText ("This request can not be processed.No accounts linked to the profile used for login.Profile used-"+user_id[1]);
		exceptionResponce.setErrorField("This request can not be processed.No accounts linked to the profile used for login.Profile used-"+user_id[1]);
		exceptionResponce.setErrorValue("Not Found");

		validationViolations.getValidationViolation().add(exceptionResponce);

		return new ResponseEntity<ValidationViolations>(validationViolations, new HttpHeaders(), HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ValidationViolations> internalServerError(Exception ex) {

		ValidationViolations validationViolations = new ValidationViolations();
		ValidationViolation exceptionResponce = new ValidationViolation();

		exceptionResponce.setErrorCode("FS_CAP_004");
		exceptionResponce.setErrorText("Technical Error. Please try again later");
		exceptionResponce.setErrorField("Technical Error. Please try again later");
		exceptionResponce.setErrorValue("Server Error");

		validationViolations.getValidationViolation().add(exceptionResponce);

		return new ResponseEntity<ValidationViolations>(validationViolations, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(UserBlockedRecordNotFoundException.class)
	public final ResponseEntity<ValidationViolations> userStatusBlocked(Exception ex) {

		ValidationViolations validationViolations = new ValidationViolations();
		ValidationViolation exceptionResponce = new ValidationViolation();

		exceptionResponce.setErrorCode("FS_CAP_006");
		exceptionResponce.setErrorText("The Customer Account Profile is blocked");
		exceptionResponce.setErrorField("The Customer Account Profile is blocked");
		exceptionResponce.setErrorValue("Not Found");

		validationViolations.getValidationViolation().add(exceptionResponce);

		return new ResponseEntity<ValidationViolations>(validationViolations, new HttpHeaders(), HttpStatus.NOT_FOUND);
	}
	
	

}
