package com.capgemini.psd2.customer.account.profile.mock.foundationservice.exception.handler;

public class UserBlockedRecordNotFoundException extends Exception {

	public UserBlockedRecordNotFoundException (String i) {
		super(i);
	}
}
