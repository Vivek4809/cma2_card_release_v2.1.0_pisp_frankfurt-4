package com.capgemini.psd2.foundationservice.sca.authentication.retrieve.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.swing.event.HyperlinkEvent.EventType;

import org.apache.commons.collections.map.HashedMap;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.security.constants.AdapterSecurityConstants;
import com.capgemini.psd2.adapter.security.domain.AuthenticationMethodCode1;
import com.capgemini.psd2.adapter.security.domain.AuthenticationParameterTextTypeValue;
import com.capgemini.psd2.adapter.security.domain.AuthenticationParameters;
import com.capgemini.psd2.adapter.security.domain.AuthenticationProtocolCode;
import com.capgemini.psd2.adapter.security.domain.AuthenticationSystemCode;
import com.capgemini.psd2.adapter.security.domain.ChannelCode;
import com.capgemini.psd2.adapter.security.domain.CustomerAuthenticationSession;
import com.capgemini.psd2.adapter.security.domain.DeviceStatusCode;
import com.capgemini.psd2.adapter.security.domain.DeviceType;
import com.capgemini.psd2.adapter.security.domain.DigitalUser;
import com.capgemini.psd2.adapter.security.domain.ElectronicDevices;
import com.capgemini.psd2.adapter.security.domain.EventType1;
import com.capgemini.psd2.adapter.security.domain.PersonBasicInformation;
import com.capgemini.psd2.adapter.security.domain.SecureAccessKeyPositionValue;
import com.capgemini.psd2.adapter.security.domain.Type2;
import com.capgemini.psd2.foundationservice.authentication.application.retrieve.boi.adapter.transformer.SCAAuthenticationRetrieveFoundationServiceTransformer;

public class SCAAuthenticationRetrieveFoundationServiceTransformerTest {

	@InjectMocks
	private SCAAuthenticationRetrieveFoundationServiceTransformer scaAuthenticationRetrieveFoundationServiceTransformer;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testtransformAuthenticationResponse() {
		
		Map<String, Object> params = new HashedMap();

		DigitalUser digitalUser = new DigitalUser();
		ElectronicDevices electronicDevices = new ElectronicDevices();
		AuthenticationParameters authParams = new AuthenticationParameters();

		List<ElectronicDevices> electronicDevices2 = new ArrayList<>();

		List<AuthenticationParameterTextTypeValue> authenticationParameterTextTypeValue = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterText = new AuthenticationParameterTextTypeValue();
		authenticationParameterText.setType(Type2.AMOUNT);
		authenticationParameterText.setValue("gukgi");
		authenticationParameterTextTypeValue.add(authenticationParameterText);

		List<SecureAccessKeyPositionValue> secureAccessKeyPositionValue = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue123 = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue123.setPosition(5.0);
		secureAccessKeyPositionValue123.setValue("yy896");
		secureAccessKeyPositionValue.add(secureAccessKeyPositionValue123);

		List<CustomerAuthenticationSession> customerAuthenticationSessions = new ArrayList<CustomerAuthenticationSession>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.DATE_OF_BIRTH);
		customerAuthenticationSession.setEncryptedCustomerObject("jhiojoi");
		customerAuthenticationSession.setAuthenticationParameterText(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(secureAccessKeyPositionValue);
		customerAuthenticationSessions.add(customerAuthenticationSession);
		digitalUser.setDigitalUserIdentifier("jhg86");
		digitalUser.authenticationProtocolCode(AuthenticationProtocolCode.SCA);
		digitalUser.setDigitalUserLockedOutIndicator(true);
		digitalUser.setChannelCode(ChannelCode.B365);
		digitalUser.setAuthenticationSystemCode(AuthenticationSystemCode.B365);
		digitalUser.setSecureKeyAttemptsRemainingCount(5.0);
		PersonBasicInformation personInformation = new PersonBasicInformation();
		Date date = new Date();
	//	personInformation.setBirthDate(date);
		personInformation.setFirstName("AFSDG");
		personInformation.setMothersMaidenName("DSGDFH");
		personInformation.setSurname("asfg");
		personInformation.setTitleCode("sdvdfh");
		digitalUser.setPersonInformation(personInformation);

		assertNotNull(digitalUser);

		electronicDevices.setCustomerAuthenticationAppIdentifier("dsgvdfhd");
		electronicDevices.setDeviceNameText("fdsgefh");
		electronicDevices.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		electronicDevices.setDeviceType(DeviceType.HARD_TOKEN);
		electronicDevices2.add(electronicDevices);

		authParams.setCustomerAuthenticationSessions(customerAuthenticationSessions);
		authParams.setDigitalUser(digitalUser);
		authParams.setElectronicDevices(electronicDevices2);
		scaAuthenticationRetrieveFoundationServiceTransformer.transformAuthenticationResponse(authParams,params);

	}

	@Test
	public void Testcase2() {
		Map<String, Object> params = new HashedMap();

		DigitalUser digitalUser = new DigitalUser();
		ElectronicDevices electronicDevices = new ElectronicDevices();
		AuthenticationParameters authParams = new AuthenticationParameters();

		List<ElectronicDevices> electronicDevices2 = new ArrayList<>();

		List<AuthenticationParameterTextTypeValue> authenticationParameterTextTypeValue = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterText = new AuthenticationParameterTextTypeValue();
		authenticationParameterText.setType(Type2.AMOUNT);
		authenticationParameterText.setValue("gukgi");
		authenticationParameterTextTypeValue.add(authenticationParameterText);

		List<SecureAccessKeyPositionValue> secureAccessKeyPositionValue = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue123 = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue123.setPosition(5.0);
		secureAccessKeyPositionValue123.setValue("yy896");
		secureAccessKeyPositionValue.add(secureAccessKeyPositionValue123);

		List<CustomerAuthenticationSession> customerAuthenticationSessions = new ArrayList<CustomerAuthenticationSession>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.DATE_OF_BIRTH);
		customerAuthenticationSession.setEncryptedCustomerObject("jhiojoi");
		customerAuthenticationSession.setAuthenticationParameterText(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(secureAccessKeyPositionValue);
		customerAuthenticationSessions.add(customerAuthenticationSession);
		assertNotNull(customerAuthenticationSessions);

		PersonBasicInformation personInformation = new PersonBasicInformation();
		Date date = new Date();
	//	personInformation.setBirthDate(date);
		personInformation.setFirstName("AFSDG");
		personInformation.setMothersMaidenName("DSGDFH");
		personInformation.setSurname("asfg");
		personInformation.setTitleCode("sdvdfh");
		digitalUser.setPersonInformation(personInformation);

		electronicDevices.setCustomerAuthenticationAppIdentifier("dsgvdfhd");
		electronicDevices.setDeviceNameText("fdsgefh");
		electronicDevices.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		electronicDevices.setDeviceType(DeviceType.HARD_TOKEN);
		electronicDevices2.add(electronicDevices);

		authParams.setCustomerAuthenticationSessions(customerAuthenticationSessions);
		authParams.setDigitalUser(digitalUser);
		authParams.setElectronicDevices(electronicDevices2);

		scaAuthenticationRetrieveFoundationServiceTransformer.transformAuthenticationResponse(authParams,params);

	}

	@Test
	public void Testcase3() {
		Map<String, Object> params = new HashedMap();
		DigitalUser digitalUser = new DigitalUser();
		ElectronicDevices electronicDevices = new ElectronicDevices();
		AuthenticationParameters authParams = new AuthenticationParameters();

		List<ElectronicDevices> electronicDevices2 = new ArrayList<>();

		List<AuthenticationParameterTextTypeValue> authenticationParameterTextTypeValue = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterText = new AuthenticationParameterTextTypeValue();
		authenticationParameterText.setType(Type2.AMOUNT);
		authenticationParameterText.setValue("gukgi");
		authenticationParameterTextTypeValue.add(authenticationParameterText);

		List<SecureAccessKeyPositionValue> secureAccessKeyPositionValue = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue123 = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue123.setPosition(5.0);
		secureAccessKeyPositionValue123.setValue("yy896");
		secureAccessKeyPositionValue.add(secureAccessKeyPositionValue123);

		List<CustomerAuthenticationSession> customerAuthenticationSessions = new ArrayList<CustomerAuthenticationSession>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.DATE_OF_BIRTH);
		customerAuthenticationSession.setEncryptedCustomerObject("jhiojoi");
		customerAuthenticationSession.setAuthenticationParameterText(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(secureAccessKeyPositionValue);
		customerAuthenticationSessions.add(customerAuthenticationSession);
		digitalUser.setDigitalUserIdentifier(null);
		digitalUser.authenticationProtocolCode(AuthenticationProtocolCode.SCA);
		digitalUser.setChannelCode(ChannelCode.B365);
		digitalUser.setAuthenticationSystemCode(AuthenticationSystemCode.B365);
		digitalUser.setSecureKeyAttemptsRemainingCount(5.0);

		assertNotNull(digitalUser);
		PersonBasicInformation personInformation = new PersonBasicInformation();
		Date date = new Date();
	//	personInformation.setBirthDate(date);
		personInformation.setFirstName("AFSDG");
		personInformation.setMothersMaidenName("DSGDFH");
		personInformation.setSurname("asfg");
		personInformation.setTitleCode("sdvdfh");
		digitalUser.setPersonInformation(personInformation);

		electronicDevices.setCustomerAuthenticationAppIdentifier("dsgvdfhd");
		electronicDevices.setDeviceNameText("fdsgefh");
		electronicDevices.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		electronicDevices.setDeviceType(DeviceType.HARD_TOKEN);
		electronicDevices2.add(electronicDevices);

		authParams.setCustomerAuthenticationSessions(customerAuthenticationSessions);
		authParams.setDigitalUser(digitalUser);
		authParams.setElectronicDevices(electronicDevices2);

		scaAuthenticationRetrieveFoundationServiceTransformer.transformAuthenticationResponse(authParams,params);
	}

	@Test
	public void Testcase4() {
		Map<String, Object> params = new HashedMap();
		DigitalUser digitalUser = new DigitalUser();
		ElectronicDevices electronicDevices = new ElectronicDevices();
		AuthenticationParameters authParams = new AuthenticationParameters();

		List<ElectronicDevices> electronicDevices2 = new ArrayList<>();

		List<AuthenticationParameterTextTypeValue> authenticationParameterTextTypeValue = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterText = new AuthenticationParameterTextTypeValue();
		authenticationParameterText.setType(Type2.AMOUNT);
		authenticationParameterText.setValue("gukgi");
		authenticationParameterTextTypeValue.add(authenticationParameterText);

		assertNotNull(authenticationParameterTextTypeValue);

		List<SecureAccessKeyPositionValue> secureAccessKeyPositionValue = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue123 = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue123.setPosition(5.0);
		secureAccessKeyPositionValue123.setValue("yy896");
		secureAccessKeyPositionValue.add(secureAccessKeyPositionValue123);

		List<CustomerAuthenticationSession> customerAuthenticationSessions = new ArrayList<CustomerAuthenticationSession>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.DATE_OF_BIRTH);
		customerAuthenticationSession.setEncryptedCustomerObject("jhiojoi");
		customerAuthenticationSession.setAuthenticationParameterText(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(secureAccessKeyPositionValue);
		customerAuthenticationSessions.add(customerAuthenticationSession);

		digitalUser.setDigitalUserIdentifier("jhg86");
		digitalUser.authenticationProtocolCode(AuthenticationProtocolCode.SCA);
		digitalUser.setDigitalUserLockedOutIndicator(true);
		digitalUser.setChannelCode(null);
		digitalUser.setAuthenticationSystemCode(AuthenticationSystemCode.B365);
		digitalUser.setSecureKeyAttemptsRemainingCount(null);
		PersonBasicInformation personInformation = new PersonBasicInformation();
		Date date = new Date();
	//	personInformation.setBirthDate(date);
		personInformation.setFirstName("AFSDG");
		personInformation.setMothersMaidenName("DSGDFH");
		personInformation.setSurname("asfg");
		personInformation.setTitleCode("sdvdfh");
		digitalUser.setPersonInformation(personInformation);

		electronicDevices.setCustomerAuthenticationAppIdentifier("dsgvdfhd");
		electronicDevices.setDeviceNameText("fdsgefh");
		electronicDevices.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		electronicDevices.setDeviceType(DeviceType.HARD_TOKEN);
		electronicDevices2.add(electronicDevices);

		authParams.setCustomerAuthenticationSessions(customerAuthenticationSessions);
		authParams.setDigitalUser(digitalUser);
		authParams.setElectronicDevices(electronicDevices2);

		scaAuthenticationRetrieveFoundationServiceTransformer.transformAuthenticationResponse(authParams,params);

	}

	@Test
	public void Test6() {
		Map<String, Object> params = new HashedMap();
		DigitalUser digitalUser = new DigitalUser();
		ElectronicDevices electronicDevices = new ElectronicDevices();
		AuthenticationParameters authParams = new AuthenticationParameters();

		List<ElectronicDevices> electronicDevices2 = new ArrayList<>();

		List<AuthenticationParameterTextTypeValue> authenticationParameterTextTypeValue = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterText = new AuthenticationParameterTextTypeValue();
		authenticationParameterText.setType(Type2.AMOUNT);
		authenticationParameterText.setValue("gukgi");
		authenticationParameterTextTypeValue.add(authenticationParameterText);

		assertNotNull(authenticationParameterTextTypeValue);

		List<SecureAccessKeyPositionValue> secureAccessKeyPositionValue = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue123 = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue123.setPosition(5.0);
		secureAccessKeyPositionValue123.setValue("yy896");
		secureAccessKeyPositionValue.add(secureAccessKeyPositionValue123);

		List<CustomerAuthenticationSession> customerAuthenticationSessions = new ArrayList<CustomerAuthenticationSession>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PIN);
		customerAuthenticationSession.setEncryptedCustomerObject("jhiojoi");
		customerAuthenticationSession.setAuthenticationParameterText(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(secureAccessKeyPositionValue);
		customerAuthenticationSessions.add(customerAuthenticationSession);
		digitalUser.setDigitalUserIdentifier("jhg86");
		digitalUser.authenticationProtocolCode(AuthenticationProtocolCode.SCA);
		digitalUser.setDigitalUserLockedOutIndicator(true);
		digitalUser.setChannelCode(ChannelCode.B365);
		digitalUser.setAuthenticationSystemCode(AuthenticationSystemCode.B365);
		digitalUser.setSecureKeyAttemptsRemainingCount(5.0);
		PersonBasicInformation personInformation = new PersonBasicInformation();
		personInformation.setFirstName("AFSDG");
		personInformation.setMothersMaidenName("DSGDFH");
		personInformation.setSurname("asfg");
		personInformation.setTitleCode("sdvdfh");
		digitalUser.setPersonInformation(personInformation);

		electronicDevices.setCustomerAuthenticationAppIdentifier("dsgvdfhd");
		electronicDevices.setDeviceNameText("fdsgefh");
		electronicDevices.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		electronicDevices.setDeviceType(DeviceType.HARD_TOKEN);
		electronicDevices2.add(electronicDevices);

		authParams.setCustomerAuthenticationSessions(customerAuthenticationSessions);
		authParams.setDigitalUser(digitalUser);
		authParams.setElectronicDevices(electronicDevices2);

		scaAuthenticationRetrieveFoundationServiceTransformer.transformAuthenticationResponse(authParams,params);

	}

	@Test
	public void Test5() {
		Map<String, Object> params = new HashedMap();
		DigitalUser digitalUser = new DigitalUser();
		ElectronicDevices electronicDevices = new ElectronicDevices();
		AuthenticationParameters authParams = new AuthenticationParameters();

		List<ElectronicDevices> electronicDevices2 = new ArrayList<>();

		List<AuthenticationParameterTextTypeValue> authenticationParameterTextTypeValue = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterText = new AuthenticationParameterTextTypeValue();
		authenticationParameterText.setType(Type2.AMOUNT);
		authenticationParameterText.setValue("gukgi");
		authenticationParameterTextTypeValue.add(authenticationParameterText);

		assertNotNull(authenticationParameterTextTypeValue);

		List<SecureAccessKeyPositionValue> secureAccessKeyPositionValue = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue123 = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue123.setPosition(5.0);
		secureAccessKeyPositionValue123.setValue("yy896");
		secureAccessKeyPositionValue.add(secureAccessKeyPositionValue123);

		List<CustomerAuthenticationSession> customerAuthenticationSessions = new ArrayList<CustomerAuthenticationSession>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.CONTACT_NUMBER);
		customerAuthenticationSession.setEncryptedCustomerObject("jhiojoi");
		customerAuthenticationSession.setAuthenticationParameterText(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(secureAccessKeyPositionValue);
		customerAuthenticationSessions.add(customerAuthenticationSession);
		digitalUser.setDigitalUserIdentifier("jhg86");
		digitalUser.authenticationProtocolCode(AuthenticationProtocolCode.SCA);
		digitalUser.setDigitalUserLockedOutIndicator(true);
		digitalUser.setChannelCode(ChannelCode.B365);
		digitalUser.setAuthenticationSystemCode(AuthenticationSystemCode.B365);
		digitalUser.setSecureKeyAttemptsRemainingCount(5.0);
		PersonBasicInformation personInformation = new PersonBasicInformation();
		Date date = new Date();
	//	personInformation.setBirthDate(date);
		personInformation.setFirstName("AFSDG");
		personInformation.setMothersMaidenName("DSGDFH");
		personInformation.setSurname("asfg");
		personInformation.setTitleCode("sdvdfh");
		digitalUser.setPersonInformation(personInformation);

		electronicDevices.setCustomerAuthenticationAppIdentifier("dsgvdfhd");
		electronicDevices.setDeviceNameText("fdsgefh");
		electronicDevices.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		electronicDevices.setDeviceType(DeviceType.HARD_TOKEN);
		electronicDevices2.add(electronicDevices);

		authParams.setCustomerAuthenticationSessions(customerAuthenticationSessions);
		authParams.setDigitalUser(digitalUser);
		authParams.setElectronicDevices(electronicDevices2);

		scaAuthenticationRetrieveFoundationServiceTransformer.transformAuthenticationResponse(authParams,params);
	}

	@Test
	public void Test7() {
		Map<String, Object> params = new HashedMap();
		DigitalUser digitalUser = new DigitalUser();
		ElectronicDevices electronicDevices = new ElectronicDevices();
		AuthenticationParameters authParams = new AuthenticationParameters();

		List<ElectronicDevices> electronicDevices2 = new ArrayList<>();

		List<AuthenticationParameterTextTypeValue> authenticationParameterTextTypeValue = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterText = new AuthenticationParameterTextTypeValue();
		authenticationParameterText.setType(Type2.AMOUNT);
		authenticationParameterText.setValue("gukgi");
		authenticationParameterTextTypeValue.add(authenticationParameterText);
		assertNotNull(authenticationParameterTextTypeValue);

		List<SecureAccessKeyPositionValue> secureAccessKeyPositionValue = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue123 = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue123.setPosition(5.0);
		secureAccessKeyPositionValue123.setValue("yy896");
		secureAccessKeyPositionValue.add(secureAccessKeyPositionValue123);

		List<CustomerAuthenticationSession> customerAuthenticationSessions = new ArrayList<CustomerAuthenticationSession>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PASSWORD);
		customerAuthenticationSession.setEncryptedCustomerObject("jhiojoi");
		customerAuthenticationSession.setAuthenticationParameterText(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(secureAccessKeyPositionValue);
		customerAuthenticationSessions.add(customerAuthenticationSession);
		digitalUser.setDigitalUserIdentifier("jhg86");
		digitalUser.authenticationProtocolCode(AuthenticationProtocolCode.SCA);
		digitalUser.setDigitalUserLockedOutIndicator(true);
		digitalUser.setChannelCode(ChannelCode.B365);
		digitalUser.setAuthenticationSystemCode(AuthenticationSystemCode.B365);
		digitalUser.setSecureKeyAttemptsRemainingCount(5.0);
		PersonBasicInformation personInformation = new PersonBasicInformation();
		Date date = new Date();
//		personInformation.setBirthDate(date);
		personInformation.setFirstName("AFSDG");
		personInformation.setMothersMaidenName("DSGDFH");
		personInformation.setSurname("asfg");
		personInformation.setTitleCode("sdvdfh");
		digitalUser.setPersonInformation(personInformation);

		electronicDevices.setCustomerAuthenticationAppIdentifier("dsgvdfhd");
		electronicDevices.setDeviceNameText(null);
		electronicDevices.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		electronicDevices.setDeviceType(DeviceType.HARD_TOKEN);
		electronicDevices2.add(electronicDevices);

		authParams.setCustomerAuthenticationSessions(customerAuthenticationSessions);
		authParams.setDigitalUser(digitalUser);
		authParams.setElectronicDevices(electronicDevices2);

		scaAuthenticationRetrieveFoundationServiceTransformer.transformAuthenticationResponse(authParams,params);
	}

	@Test
	public void Test9() {
		Map<String, Object> params = new HashedMap();
		DigitalUser digitalUser = new DigitalUser();
		ElectronicDevices electronicDevices = new ElectronicDevices();
		AuthenticationParameters authParams = new AuthenticationParameters();

		List<ElectronicDevices> electronicDevices2 = new ArrayList<>();

		List<AuthenticationParameterTextTypeValue> authenticationParameterTextTypeValue = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterText = new AuthenticationParameterTextTypeValue();
		authenticationParameterText.setType(Type2.AMOUNT);
		authenticationParameterText.setValue("gukgi");
		authenticationParameterTextTypeValue.add(authenticationParameterText);
		assertNotNull(authenticationParameterTextTypeValue);

		List<SecureAccessKeyPositionValue> secureAccessKeyPositionValue = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue123 = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue123.setPosition(5.0);
		secureAccessKeyPositionValue123.setValue("yy896");
		secureAccessKeyPositionValue.add(secureAccessKeyPositionValue123);

		List<CustomerAuthenticationSession> customerAuthenticationSessions = new ArrayList<CustomerAuthenticationSession>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PUSH_NOTIFICATION);
		customerAuthenticationSession.setEncryptedCustomerObject("jhiojoi");
		params.put(AdapterSecurityConstants.EVENTTYPE, EventType1.INTERNATIONAL_PAYMENT_TRUSTED_PAYEE);
		customerAuthenticationSession.setAuthenticationParameterText(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(secureAccessKeyPositionValue);
		customerAuthenticationSessions.add(customerAuthenticationSession);
		digitalUser.setDigitalUserIdentifier("jhg86");
		digitalUser.authenticationProtocolCode(AuthenticationProtocolCode.SCA);
		digitalUser.setDigitalUserLockedOutIndicator(true);
		digitalUser.setChannelCode(ChannelCode.B365);
		digitalUser.setAuthenticationSystemCode(AuthenticationSystemCode.B365);
		digitalUser.setSecureKeyAttemptsRemainingCount(5.0);
		PersonBasicInformation personInformation = new PersonBasicInformation();
		Date date = new Date();
	//	personInformation.setBirthDate(date);
		personInformation.setFirstName("AFSDG");
		personInformation.setMothersMaidenName("DSGDFH");
		personInformation.setSurname("asfg");
		personInformation.setTitleCode("sdvdfh");
		digitalUser.setPersonInformation(personInformation);

		electronicDevices.setCustomerAuthenticationAppIdentifier("dsgvdfhd");
		electronicDevices.setDeviceNameText("fdsgefh");
		electronicDevices.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		electronicDevices.setDeviceType(DeviceType.HARD_TOKEN);
		electronicDevices2.add(electronicDevices);

		authParams.setCustomerAuthenticationSessions(customerAuthenticationSessions);
		authParams.setDigitalUser(digitalUser);
		authParams.setElectronicDevices(electronicDevices2);
		scaAuthenticationRetrieveFoundationServiceTransformer.transformAuthenticationResponse(authParams,params);

	}

	@Test
	public void Test11() {
		Map<String, Object> params = new HashedMap();
		DigitalUser digitalUser = new DigitalUser();
		ElectronicDevices electronicDevices = new ElectronicDevices();
		AuthenticationParameters authParams = new AuthenticationParameters();

		List<ElectronicDevices> electronicDevices2 = new ArrayList<>();

		List<AuthenticationParameterTextTypeValue> authenticationParameterTextTypeValue = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterText = new AuthenticationParameterTextTypeValue();
		authenticationParameterText.setType(Type2.AMOUNT);
		authenticationParameterText.setValue("gukgi");
		authenticationParameterTextTypeValue.add(authenticationParameterText);
		assertNotNull(authenticationParameterTextTypeValue);

		List<SecureAccessKeyPositionValue> secureAccessKeyPositionValue = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue123 = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue123.setPosition(5.0);
		secureAccessKeyPositionValue123.setValue("yy896");
		secureAccessKeyPositionValue.add(secureAccessKeyPositionValue123);

		List<CustomerAuthenticationSession> customerAuthenticationSessions = new ArrayList<CustomerAuthenticationSession>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PIN);
		customerAuthenticationSession.setEncryptedCustomerObject("jhiojoi");
		customerAuthenticationSession.setAuthenticationParameterText(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(secureAccessKeyPositionValue);
		customerAuthenticationSessions.add(customerAuthenticationSession);
		digitalUser.setDigitalUserIdentifier("jhg86");
		digitalUser.authenticationProtocolCode(AuthenticationProtocolCode.SCA);
		digitalUser.setDigitalUserLockedOutIndicator(true);
		digitalUser.setChannelCode(ChannelCode.B365);
		digitalUser.setAuthenticationSystemCode(AuthenticationSystemCode.B365);
		digitalUser.setSecureKeyAttemptsRemainingCount(5.0);
		PersonBasicInformation personInformation = new PersonBasicInformation();
		Date date = new Date();
	//	personInformation.setBirthDate(date);
		personInformation.setFirstName("AFSDG");
		personInformation.setMothersMaidenName("DSGDFH");
		personInformation.setSurname("asfg");
		personInformation.setTitleCode("sdvdfh");
		digitalUser.setPersonInformation(personInformation);

		electronicDevices.setCustomerAuthenticationAppIdentifier("dsgvdfhd");
		electronicDevices.setDeviceNameText("fdsgefh");

		electronicDevices.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		electronicDevices.setDeviceType(DeviceType.SOFT_TOKEN);
		electronicDevices2.add(electronicDevices);

		authParams.setCustomerAuthenticationSessions(customerAuthenticationSessions);
		authParams.setDigitalUser(digitalUser);
		authParams.setElectronicDevices(electronicDevices2);
		scaAuthenticationRetrieveFoundationServiceTransformer.transformAuthenticationResponse(authParams,params);

	}

}
