package com.capgemini.psd2.foundationservice.sca.authentication.retrieve.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.security.domain.AuthenticationSystemCode;
import com.capgemini.psd2.adapter.security.domain.ChannelCode;
import com.capgemini.psd2.adapter.security.domain.DigitalUser;
import com.capgemini.psd2.foundationservice.authentication.application.retrieve.boi.adapter.delegate.SCAAuthenticationRetrieveFoundationServiceDelegate;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@RunWith(SpringJUnit4ClassRunner.class)
public class SCAAuthenticationRetrieveFoundationServiceDelegateTest {

	
	@InjectMocks
	private SCAAuthenticationRetrieveFoundationServiceDelegate delegate;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testCreateRequestHeadersActual() {

		Map<String, Object> params = new HashMap<String, Object>();
		
		  params.put("X-API-SOURCE-USER", "ghghk"); 
		  params.put("X-API-SOURCE-SYSTEM", "dfg");
		  params.put("X-API-TRANSANCTION-ID", "ghghk");
		  params.put("X-API-CORRELATION-ID", "abghgc");
		  params.put("x-api-channel-brand", "abghgc");
		  params.put("x-api-channel-code", "abghgc");
		
		HttpHeaders httpHeaders = new HttpHeaders();
		ReflectionTestUtils.setField(delegate, "sourceUserInReqHeader", "X-API-SOURCE-USER");
		ReflectionTestUtils.setField(delegate, "sourceSystemInReqHeader", "X-API-SOURCE-SYSTEM");
		ReflectionTestUtils.setField(delegate, "trasanctionIdInReqHeader", "X-API-TRANSANCTION-ID");
		ReflectionTestUtils.setField(delegate, "apiCorrelationIdInReqHeader", "X-API-CORRELATION-ID");
		ReflectionTestUtils.setField(delegate, "apichannelCode", "X-API-Channel-Code");
		ReflectionTestUtils.setField(delegate, "apichannelBrand", "X-API-Channel-Brand");
				
		RequestInfo requestInfo = new RequestInfo();
		DigitalUser digitalUser = new DigitalUser();
		digitalUser.digitalUserIdentifier("hjgj78687");	
		digitalUser.authenticationSystemCode(AuthenticationSystemCode.B365);
		digitalUser.channelCode(ChannelCode.B365);
		digitalUser.digitalUserLockedOutIndicator(true);
		
		assertNotNull(digitalUser);
	
		httpHeaders = delegate.createRequestHeaders(requestInfo, params);	
		assertNotNull(httpHeaders);		
	}
	
	@Test
	public void testGetFoundationServiceURL() {
		Map<String, Object> params = new HashMap<String, Object>();
		String str=delegate.getAuthenticationFoundationServiceURL(params, "hjgj78687");
		assertNotNull(str);
	}
}