package com.capgemini.psd2.foundationservice.authentication.application.retrieve.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.security.constants.AdapterSecurityConstants;
import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServiceGetResponse;
import com.capgemini.psd2.adapter.security.domain.AuthenticationParameters;
import com.capgemini.psd2.foundationservice.authentication.application.retrieve.boi.adapter.client.SCAAuthenticationRetrieveFoundationServiceClient;
import com.capgemini.psd2.foundationservice.authentication.application.retrieve.boi.adapter.delegate.SCAAuthenticationRetrieveFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.authentication.application.retrieve.boi.adapter.transformer.SCAAuthenticationRetrieveFoundationServiceTransformer;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
@EnableConfigurationProperties
public class SCAAuthenticationRetrieveFoundationServiceAdapter{
	
	@Autowired
	SCAAuthenticationRetrieveFoundationServiceClient client;
	
	@Autowired
	SCAAuthenticationRetrieveFoundationServiceDelegate delegate;
	
	@Autowired
	SCAAuthenticationRetrieveFoundationServiceTransformer transformer;
	
	
	public CustomAuthenticationServiceGetResponse getAuthenticate(Map<String, Object> params) {

		String digitalUserId = String.valueOf(params.get(AdapterSecurityConstants.USER_HEADER));
		String eventType = String.valueOf(params.get(AdapterSecurityConstants.EVENTTYPE));
		
		if(NullCheckUtils.isNullOrEmpty(digitalUserId))
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		
		RequestInfo requestInfo = new RequestInfo();
		
		MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
		if(!NullCheckUtils.isNullOrEmpty(eventType))
			queryParams.add(AdapterSecurityConstants.EVENTTYPE,eventType);
		
		HttpHeaders httpHeaders = delegate.createRequestHeaders(requestInfo, params);
		String finalURL = delegate.getAuthenticationFoundationServiceURL(params, digitalUserId);
		requestInfo.setUrl(finalURL);
		
		AuthenticationParameters authenticationParameters = client.restTransportForRetrieveAuthenticationService(requestInfo, 
					AuthenticationParameters.class, queryParams, httpHeaders);
		return transformer.transformAuthenticationResponse(authenticationParameters,params);
	}

}
