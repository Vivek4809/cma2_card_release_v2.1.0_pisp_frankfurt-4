package com.capgemini.psd2.foundationservice.authentication.application.retrieve.boi.adapter.delegate;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.security.constants.AdapterSecurityConstants;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class SCAAuthenticationRetrieveFoundationServiceDelegate {
	
	@Value("${foundationService.sourceUserInReqHeader:#{X-API-SOURCE-USER}}")
	private String sourceUserInReqHeader;

	@Value("${foundationService.sourceSystemInReqHeader:#{X-API-SOURCE-SYSTEM}}")
	private String sourceSystemInReqHeader;

	@Value("${foundationService.trasanctionIdInReqHeader:#{X-API-TRANSANCTION-ID}}")
	private String trasanctionIdInReqHeader;

	@Value("${foundationService.apiCorrelationIdInReqHeader:#{X-API-CORRELATION-ID}}")
	private String apiCorrelationIdInReqHeader;
	
	@Value("${foundationService.authenticationServiceGetBaseURL}")
	private String authenticationServiceGetBaseURL;

	@Value("${foundationService.authenticationServiceGetVersion}")
	private String authenticationServiceGETVersion;
	
	@Value("${foundationService.apichannelBrand:#{x-api-channel-brand}}")
	private String apichannelBrand;
	
	@Value("${foundationService.apichannelCode:#{x-api-channel-code}}")
	private String apichannelCode;
	
	
	public HttpHeaders createRequestHeaders(RequestInfo requestInfo, Map<String, Object> params) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(sourceUserInReqHeader, String.valueOf(params.get(AdapterSecurityConstants.USER_HEADER)));
		httpHeaders.add(sourceSystemInReqHeader, AdapterSecurityConstants.SOURCE_SYSTEM_HEADER_VALUE);
		httpHeaders.add(trasanctionIdInReqHeader, String.valueOf(params.get(PSD2Constants.CORRELATION_ID)));
		httpHeaders.add(apichannelBrand, String.valueOf(params.get(AdapterSecurityConstants.CHANNEL_BRAND)));
		if(!NullCheckUtils.isNullOrEmpty(params.get(AdapterSecurityConstants.CHANNELCODE)))
			httpHeaders.add(apichannelCode, String.valueOf(params.get(AdapterSecurityConstants.CHANNELCODE)));
		httpHeaders.add(apiCorrelationIdInReqHeader, "");
		httpHeaders.add("Content-Type", "application/json");
		return httpHeaders;
	}
	
	public String getAuthenticationFoundationServiceURL(Map<String, Object> params, String digitalUserId) {
		return authenticationServiceGetBaseURL + "/" + authenticationServiceGETVersion + "/"
				+ "channels" + "/" + params.get(AdapterSecurityConstants.CHANNELCODE) 
				+ "/digital-users/" + digitalUserId + "/" + "authentication-parameters";
	}

}
