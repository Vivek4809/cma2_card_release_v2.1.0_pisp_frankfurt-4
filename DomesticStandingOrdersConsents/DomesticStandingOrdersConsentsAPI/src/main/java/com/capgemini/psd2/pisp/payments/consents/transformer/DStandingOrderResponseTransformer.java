package com.capgemini.psd2.pisp.payments.consents.transformer;

import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;

public interface DStandingOrderResponseTransformer {

	
	//Interfaces for DomesticStandingOrder Payments v3.0 API 
	public CustomDStandingOrderConsentsPOSTResponse domesticStandingOrderConsentsResponseTransformer(CustomDStandingOrderConsentsPOSTResponse standingOrderConsentsResponse, PaymentConsentsPlatformResource paymentSetupPlatformResource, String methodType);
}
