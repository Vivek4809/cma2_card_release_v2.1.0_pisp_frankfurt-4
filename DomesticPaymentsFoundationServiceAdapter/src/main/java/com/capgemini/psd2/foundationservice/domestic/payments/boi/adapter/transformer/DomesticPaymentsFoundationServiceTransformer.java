
package com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.transformer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.Address;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.AuthorisingPartyAccount;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.BrandCode3;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.Channel;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.ChannelCode;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.Country;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.Currency;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.FinancialEventAmount;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.Party2;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.PartyBasicInformation;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.PaymentInstructionPostalAddress;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.PaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.PaymentInstructionProposalComposite;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.PaymentInstructionStatusCode;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.PaymentInstrumentRiskFactor;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.ProposingPartyAccount;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBAddressTypeCode;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticResponse1;
import com.capgemini.psd2.pisp.domain.PaymentDomesticSubmitPOST201Response;
import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class DomesticPaymentsFoundationServiceTransformer {
	
	public static final String PARTY_NAME = "BOL Customer Id";

	public <T> PaymentDomesticSubmitPOST201Response transformDomesticPaymentResponse(T inputBalanceObj) {
		PaymentDomesticSubmitPOST201Response paymentDomesticSubmitPOST201Response = new PaymentDomesticSubmitPOST201Response();
		OBWriteDataDomesticResponse1 oBWriteDataDomesticResponse1 = new OBWriteDataDomesticResponse1();
		OBDomestic1 oBDomestic1 = new OBDomestic1();
		PaymentInstructionProposalComposite paymentInstructionProposal = (PaymentInstructionProposalComposite) inputBalanceObj;
				
		populateAPIToFDResponse(paymentInstructionProposal, oBWriteDataDomesticResponse1, oBDomestic1);
		

		if ((!NullCheckUtils.isNullOrEmpty(
				paymentInstructionProposal.getPaymentInstructionProposal().getAuthorisingPartyReference()))
				|| (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal()
						.getAuthorisingPartyUnstructuredReference()))) {
			OBRemittanceInformation1 oBRemittanceInformation1 = new OBRemittanceInformation1();
			boolean isInt=false;
			
			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getPaymentInstructionProposal().getAuthorisingPartyReference())) {

				oBRemittanceInformation1.setReference(
						paymentInstructionProposal.getPaymentInstructionProposal().getAuthorisingPartyReference());
				isInt=true;
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal()
					.getAuthorisingPartyUnstructuredReference())) {

				oBRemittanceInformation1.setUnstructured(paymentInstructionProposal.getPaymentInstructionProposal()
						.getAuthorisingPartyUnstructuredReference());
				isInt=true;
			}

			if(isInt)
			oBDomestic1.setRemittanceInformation(oBRemittanceInformation1);
		}

		oBWriteDataDomesticResponse1.setInitiation(oBDomestic1);
		paymentDomesticSubmitPOST201Response.setData(oBWriteDataDomesticResponse1);
		return paymentDomesticSubmitPOST201Response;

	}

	public PaymentInstructionProposalComposite transformDomesticSubmissionResponseFromAPIToFDForInsert(
			CustomDPaymentsPOSTRequest domesticPaymentsRequest, Map<String, String> params) {
		
		PaymentInstructionProposalComposite paymentInstructionProposalComposite = new PaymentInstructionProposalComposite();
		PaymentInstructionProposal paymentInstructionProposal = new PaymentInstructionProposal();

		if (!NullCheckUtils.isNullOrEmpty(domesticPaymentsRequest.getData().getConsentId())) {
			paymentInstructionProposal
					.setPaymentInstructionProposalId(domesticPaymentsRequest.getData().getConsentId());
		}
		
		//for channelcode
		
		Channel channel = new Channel();
		 if (!NullCheckUtils
		   .isNullOrEmpty(   params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))) {
		  
		  channel.setChannelCode(ChannelCode.fromValue((params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER)).toUpperCase()));
		 }
		 
			if (!NullCheckUtils
					.isNullOrEmpty(params.get(PSD2Constants.TENANT_ID))) {
				
				if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK")) {
					channel.setBrandCode(BrandCode3.NIGB);
				}else if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")){
					channel.setBrandCode(BrandCode3.ROI);
				}	
				
			}
		 
		paymentInstructionProposal.setChannel(channel);

		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))
				&& params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase(ChannelCode.BOL.toString())) {
			Party2 authorisingParty = new Party2();
			PartyBasicInformation partyInformation = new PartyBasicInformation();
			partyInformation.setPartySourceIdNumber(params.get(PSD2Constants.PARTY_IDENTIFIER));
			partyInformation.setPartyName(PARTY_NAME);
			authorisingParty.setPartyInformation(partyInformation);
			paymentInstructionProposal.setAuthorisingParty(authorisingParty);
		}

		if (!NullCheckUtils
				.isNullOrEmpty(domesticPaymentsRequest.getData().getInitiation().getInstructionIdentification())) {
			paymentInstructionProposal.setInstructionReference(
					domesticPaymentsRequest.getData().getInitiation().getInstructionIdentification());
		}

		if (!NullCheckUtils
				.isNullOrEmpty(domesticPaymentsRequest.getData().getInitiation().getEndToEndIdentification())) {
			paymentInstructionProposal.setInstructionEndToEndReference(
					domesticPaymentsRequest.getData().getInitiation().getEndToEndIdentification());
		}

		if (!NullCheckUtils.isNullOrEmpty(domesticPaymentsRequest.getData().getInitiation().getLocalInstrument())) {
			paymentInstructionProposal.setInstructionLocalInstrument(
					domesticPaymentsRequest.getData().getInitiation().getLocalInstrument());
		}

		if (!NullCheckUtils.isNullOrEmpty(domesticPaymentsRequest.getData().getInitiation().getInstructedAmount())) {
			if (!NullCheckUtils.isNullOrEmpty(
					domesticPaymentsRequest.getData().getInitiation().getInstructedAmount().getAmount())) {
				FinancialEventAmount financialEventAmount = new FinancialEventAmount();
				String amount = domesticPaymentsRequest.getData().getInitiation().getInstructedAmount().getAmount();
				Double amountDouble = Double.parseDouble(amount);
				financialEventAmount.setTransactionCurrency(amountDouble);
				paymentInstructionProposal.setFinancialEventAmount(financialEventAmount);
			}

			if (!NullCheckUtils.isNullOrEmpty(
					domesticPaymentsRequest.getData().getInitiation().getInstructedAmount().getCurrency())) {
				Currency transactionCurrency = new Currency();
				transactionCurrency.setIsoAlphaCode(
						domesticPaymentsRequest.getData().getInitiation().getInstructedAmount().getCurrency());
				paymentInstructionProposal.setTransactionCurrency(transactionCurrency);
			}
		}

		if (!NullCheckUtils.isNullOrEmpty(domesticPaymentsRequest.getData().getInitiation().getDebtorAccount())) {
			AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();
			if (!NullCheckUtils.isNullOrEmpty(
					domesticPaymentsRequest.getData().getInitiation().getDebtorAccount().getSchemeName())) {
				authorisingPartyAccount.setSchemeName(
						domesticPaymentsRequest.getData().getInitiation().getDebtorAccount().getSchemeName());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					domesticPaymentsRequest.getData().getInitiation().getDebtorAccount().getIdentification())) {
				authorisingPartyAccount.setAccountIdentification(
						domesticPaymentsRequest.getData().getInitiation().getDebtorAccount().getIdentification());
			}

			if (!NullCheckUtils
					.isNullOrEmpty(domesticPaymentsRequest.getData().getInitiation().getDebtorAccount().getName())) {
				authorisingPartyAccount
						.setAccountName(domesticPaymentsRequest.getData().getInitiation().getDebtorAccount().getName());
			}

			if (!NullCheckUtils.isNullOrEmpty(domesticPaymentsRequest.getData().getInitiation().getDebtorAccount()
					.getSecondaryIdentification())) {
				authorisingPartyAccount.setSecondaryIdentification(domesticPaymentsRequest.getData().getInitiation()
						.getDebtorAccount().getSecondaryIdentification());
			}
			paymentInstructionProposal.setAuthorisingPartyAccount(authorisingPartyAccount);
		}

		if (!NullCheckUtils.isNullOrEmpty(domesticPaymentsRequest.getData().getInitiation().getCreditorAccount())) {
			ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();
			if (!NullCheckUtils.isNullOrEmpty(
					domesticPaymentsRequest.getData().getInitiation().getCreditorAccount().getSchemeName())) {
				proposingPartyAccount.setSchemeName(
						domesticPaymentsRequest.getData().getInitiation().getCreditorAccount().getSchemeName());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					domesticPaymentsRequest.getData().getInitiation().getCreditorAccount().getIdentification())) {
				proposingPartyAccount.setAccountIdentification(
						domesticPaymentsRequest.getData().getInitiation().getCreditorAccount().getIdentification());
			}

			if (!NullCheckUtils
					.isNullOrEmpty(domesticPaymentsRequest.getData().getInitiation().getCreditorAccount().getName())) {
				proposingPartyAccount.setAccountName(
						domesticPaymentsRequest.getData().getInitiation().getCreditorAccount().getName());
			}

			if (!NullCheckUtils.isNullOrEmpty(domesticPaymentsRequest.getData().getInitiation().getCreditorAccount()
					.getSecondaryIdentification())) {
				proposingPartyAccount.setSecondaryIdentification(domesticPaymentsRequest.getData().getInitiation()
						.getCreditorAccount().getSecondaryIdentification());
			}
			paymentInstructionProposal.setProposingPartyAccount(proposingPartyAccount);
		}

		if (!NullCheckUtils
				.isNullOrEmpty(domesticPaymentsRequest.getData().getInitiation().getCreditorPostalAddress())) {
			PaymentInstructionPostalAddress proposingPartyPostalAddress = new PaymentInstructionPostalAddress();
			boolean isInit=false;
			if (!NullCheckUtils.isNullOrEmpty(
					domesticPaymentsRequest.getData().getInitiation().getCreditorPostalAddress().getAddressType())) {
				proposingPartyPostalAddress.setAddressType(domesticPaymentsRequest.getData().getInitiation()
						.getCreditorPostalAddress().getAddressType().toString());
				isInit=true;
			}

			if (!NullCheckUtils.isNullOrEmpty(
					domesticPaymentsRequest.getData().getInitiation().getCreditorPostalAddress().getDepartment())) {
				proposingPartyPostalAddress.setDepartment(
						domesticPaymentsRequest.getData().getInitiation().getCreditorPostalAddress().getDepartment());
				isInit=true;
			}

			if (!NullCheckUtils.isNullOrEmpty(
					domesticPaymentsRequest.getData().getInitiation().getCreditorPostalAddress().getSubDepartment())) {
				proposingPartyPostalAddress.setSubDepartment(domesticPaymentsRequest.getData().getInitiation()
						.getCreditorPostalAddress().getSubDepartment());
				isInit=true;
			}

			if (!NullCheckUtils.isNullOrEmpty(
					domesticPaymentsRequest.getData().getInitiation().getCreditorPostalAddress().getStreetName())) {
				proposingPartyPostalAddress.setGeoCodeBuildingName(
						domesticPaymentsRequest.getData().getInitiation().getCreditorPostalAddress().getStreetName());
				isInit=true;
			}

			if (!NullCheckUtils.isNullOrEmpty(
					domesticPaymentsRequest.getData().getInitiation().getCreditorPostalAddress().getBuildingNumber())) {
				proposingPartyPostalAddress.setGeoCodeBuildingNumber(domesticPaymentsRequest.getData().getInitiation()
						.getCreditorPostalAddress().getBuildingNumber());
				isInit=true;
			}

			if (!NullCheckUtils.isNullOrEmpty(
					domesticPaymentsRequest.getData().getInitiation().getCreditorPostalAddress().getPostCode())) {
				proposingPartyPostalAddress.setPostCodeNumber(
						domesticPaymentsRequest.getData().getInitiation().getCreditorPostalAddress().getPostCode());
				isInit=true;
			}

			if (!NullCheckUtils.isNullOrEmpty(
					domesticPaymentsRequest.getData().getInitiation().getCreditorPostalAddress().getTownName())) {
				proposingPartyPostalAddress.setTownName(
						domesticPaymentsRequest.getData().getInitiation().getCreditorPostalAddress().getTownName());
				isInit=true;
			}

			if (!NullCheckUtils.isNullOrEmpty(domesticPaymentsRequest.getData().getInitiation()
					.getCreditorPostalAddress().getCountrySubDivision())) {
				proposingPartyPostalAddress.setCountrySubDivision(domesticPaymentsRequest.getData().getInitiation()
						.getCreditorPostalAddress().getCountrySubDivision());
				isInit=true;
			}

			if (!NullCheckUtils.isNullOrEmpty(
					domesticPaymentsRequest.getData().getInitiation().getCreditorPostalAddress().getCountry())) {
				Country addressCountry = new Country();
				addressCountry.setIsoCountryAlphaTwoCode(
						domesticPaymentsRequest.getData().getInitiation().getCreditorPostalAddress().getCountry());
				proposingPartyPostalAddress.setAddressCountry(addressCountry);
				isInit=true;
			}

			List<String> addLine = domesticPaymentsRequest.getData().getInitiation().getCreditorPostalAddress()
					.getAddressLine();
			if (!NullCheckUtils.isNullOrEmpty(addLine) && !addLine.isEmpty()) {
				proposingPartyPostalAddress.setAddressLine(
						domesticPaymentsRequest.getData().getInitiation().getCreditorPostalAddress().getAddressLine());
				isInit=true;
			}
			if(isInit)
			paymentInstructionProposal.setProposingPartyPostalAddress(proposingPartyPostalAddress);
		}

		if (!NullCheckUtils
				.isNullOrEmpty(domesticPaymentsRequest.getData().getInitiation().getRemittanceInformation())) {
			if (!NullCheckUtils.isNullOrEmpty(
					domesticPaymentsRequest.getData().getInitiation().getRemittanceInformation().getUnstructured())) {
				paymentInstructionProposal.setAuthorisingPartyUnstructuredReference(
						domesticPaymentsRequest.getData().getInitiation().getRemittanceInformation().getUnstructured());
				
			}

			if (!NullCheckUtils.isNullOrEmpty(
					domesticPaymentsRequest.getData().getInitiation().getRemittanceInformation().getReference())) {
				paymentInstructionProposal.setAuthorisingPartyReference(
						domesticPaymentsRequest.getData().getInitiation().getRemittanceInformation().getReference());
			}
		}

		if (!NullCheckUtils.isNullOrEmpty(domesticPaymentsRequest.getRisk())) {

			PaymentInstrumentRiskFactor paymentInstructionRiskFactorReference = new PaymentInstrumentRiskFactor();

			if (!NullCheckUtils.isNullOrEmpty(domesticPaymentsRequest.getRisk().getPaymentContextCode())) {
				paymentInstructionRiskFactorReference
						.setPaymentContextCode(domesticPaymentsRequest.getRisk().getPaymentContextCode().toString());
			}

			if (!NullCheckUtils.isNullOrEmpty(domesticPaymentsRequest.getRisk().getMerchantCategoryCode())) {
				paymentInstructionRiskFactorReference
						.setMerchantCategoryCode(domesticPaymentsRequest.getRisk().getMerchantCategoryCode());
			}

			if (!NullCheckUtils.isNullOrEmpty(domesticPaymentsRequest.getRisk().getMerchantCustomerIdentification())) {
				paymentInstructionRiskFactorReference.setMerchantCustomerIdentification(
						domesticPaymentsRequest.getRisk().getMerchantCustomerIdentification());
			}

			if (!NullCheckUtils.isNullOrEmpty(domesticPaymentsRequest.getRisk().getDeliveryAddress())) {
				Address counterPartyAddress = new Address();
				if (!NullCheckUtils
						.isNullOrEmpty(domesticPaymentsRequest.getRisk().getDeliveryAddress().getBuildingNumber())) {
					counterPartyAddress.setGeoCodeBuildingNumber(
							domesticPaymentsRequest.getRisk().getDeliveryAddress().getBuildingNumber());
				}

				if (!NullCheckUtils
						.isNullOrEmpty(domesticPaymentsRequest.getRisk().getDeliveryAddress().getPostCode())) {
					counterPartyAddress
							.setPostCodeNumber(domesticPaymentsRequest.getRisk().getDeliveryAddress().getPostCode());
				}

				if (!NullCheckUtils
						.isNullOrEmpty(domesticPaymentsRequest.getRisk().getDeliveryAddress().getTownName())) {
					counterPartyAddress
							.setThirdAddressLine(domesticPaymentsRequest.getRisk().getDeliveryAddress().getTownName());
				}

				if (!NullCheckUtils.isNullOrEmpty(
						domesticPaymentsRequest.getRisk().getDeliveryAddress().getCountrySubDivision())) {
					counterPartyAddress.setFourthAddressLine(
							domesticPaymentsRequest.getRisk().getDeliveryAddress().getCountrySubDivision());
					counterPartyAddress.setFifthAddressLine(
							domesticPaymentsRequest.getRisk().getDeliveryAddress().getCountrySubDivision());
				}

				if (!NullCheckUtils
						.isNullOrEmpty(domesticPaymentsRequest.getRisk().getDeliveryAddress().getCountry())) {
					Country addressCountry = new Country();
					addressCountry.setIsoCountryAlphaTwoCode(
							domesticPaymentsRequest.getRisk().getDeliveryAddress().getCountry());
					counterPartyAddress.setAddressCountry(addressCountry);
				}

				if (!NullCheckUtils
						.isNullOrEmpty(domesticPaymentsRequest.getRisk().getDeliveryAddress().getAddressLine())
						&& !domesticPaymentsRequest.getRisk().getDeliveryAddress().getAddressLine().isEmpty()) {
					counterPartyAddress.setFirstAddressLine(
							domesticPaymentsRequest.getRisk().getDeliveryAddress().getAddressLine().get(0));
					if (!NullCheckUtils.isNullOrEmpty(
							domesticPaymentsRequest.getRisk().getDeliveryAddress().getAddressLine()) && 
							domesticPaymentsRequest.getRisk().getDeliveryAddress().getAddressLine().size()>1) {
						counterPartyAddress.setSecondAddressLine(
								domesticPaymentsRequest.getRisk().getDeliveryAddress().getAddressLine().get(1));
					}
				}
				

				if (!NullCheckUtils
						.isNullOrEmpty(domesticPaymentsRequest.getRisk().getDeliveryAddress().getStreetName())) {
					counterPartyAddress.setGeoCodeBuildingName(
							domesticPaymentsRequest.getRisk().getDeliveryAddress().getStreetName());
				}
				paymentInstructionRiskFactorReference.setCounterPartyAddress(counterPartyAddress);
			}
			paymentInstructionProposal.setPaymentInstructionRiskFactorReference(paymentInstructionRiskFactorReference);
		}

		paymentInstructionProposalComposite.setPaymentInstructionProposal(paymentInstructionProposal);
		return paymentInstructionProposalComposite;
	}

	public PaymentDomesticSubmitPOST201Response transformDomesticConsentResponseFromFDToAPIForInsert(
			PaymentInstructionProposalComposite paymentInstructionProposal) {
		
		PaymentDomesticSubmitPOST201Response paymentDomesticSubmitPOST201Response = new PaymentDomesticSubmitPOST201Response();
		OBWriteDataDomesticResponse1 oBWriteDataDomesticResponse1 = new OBWriteDataDomesticResponse1();
		OBDomestic1 oBDomestic1 = new OBDomestic1();

		populateAPIToFDResponse(paymentInstructionProposal, oBWriteDataDomesticResponse1, oBDomestic1);

		if ((!NullCheckUtils.isNullOrEmpty(
				paymentInstructionProposal.getPaymentInstructionProposal().getAuthorisingPartyReference()))
				|| (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal()
						.getAuthorisingPartyUnstructuredReference()))) {
			OBRemittanceInformation1 oBRemittanceInformation1 = new OBRemittanceInformation1();

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getPaymentInstructionProposal().getAuthorisingPartyReference())) {
				oBRemittanceInformation1.setReference(
						paymentInstructionProposal.getPaymentInstructionProposal().getAuthorisingPartyReference());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal()
					.getAuthorisingPartyUnstructuredReference())) {
				oBRemittanceInformation1.setUnstructured(paymentInstructionProposal.getPaymentInstructionProposal()
						.getAuthorisingPartyUnstructuredReference());
			}
			oBDomestic1.setRemittanceInformation(oBRemittanceInformation1);
		}

		oBWriteDataDomesticResponse1.setInitiation(oBDomestic1);
		paymentDomesticSubmitPOST201Response.setData(oBWriteDataDomesticResponse1);

		// Need to set Proposal status
		PaymentInstructionStatusCode status = PaymentInstructionStatusCode.fromValue(
				paymentInstructionProposal.getPaymentInstruction().getPaymentInstructionStatusCode().toString());
		if (((status.toString()).equalsIgnoreCase("AcceptedSettlementInProcess"))
				&& (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstruction().getPaymentInstructionNumber()))) {
			ProcessExecutionStatusEnum processExecutionStatusEnum = ProcessExecutionStatusEnum.PASS;
			paymentDomesticSubmitPOST201Response.setProcessExecutionStatus(processExecutionStatusEnum);
		} else if (((status.toString()).equalsIgnoreCase("Rejected"))
				&& (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstruction().getPaymentInstructionNumber()))) {
			ProcessExecutionStatusEnum processExecutionStatusEnum = ProcessExecutionStatusEnum.FAIL;
			paymentDomesticSubmitPOST201Response.setProcessExecutionStatus(processExecutionStatusEnum);
		}

		return paymentDomesticSubmitPOST201Response;
	}

	private void populateAPIToFDResponse(PaymentInstructionProposalComposite paymentInstructionProposal,
			OBWriteDataDomesticResponse1 oBWriteDataDomesticResponse1, OBDomestic1 oBDomestic1) {
		if (!NullCheckUtils
				.isNullOrEmpty(paymentInstructionProposal.getPaymentInstruction().getPaymentInstructionNumber())) {
			oBWriteDataDomesticResponse1.setDomesticPaymentId(
					paymentInstructionProposal.getPaymentInstruction().getPaymentInstructionNumber());
		}

		if (!NullCheckUtils.isNullOrEmpty(
				paymentInstructionProposal.getPaymentInstructionProposal().getPaymentInstructionProposalId())) {
			oBWriteDataDomesticResponse1.setConsentId(
					paymentInstructionProposal.getPaymentInstructionProposal().getPaymentInstructionProposalId());
		}

		if (!NullCheckUtils
				.isNullOrEmpty(paymentInstructionProposal.getPaymentInstruction().getInstructionIssueDate())) {

			oBWriteDataDomesticResponse1.setCreationDateTime(paymentInstructionProposal.getPaymentInstruction()
					.getInstructionIssueDate());
		}

		if (!NullCheckUtils.isNullOrEmpty(
				paymentInstructionProposal.getPaymentInstruction().getPaymentInstructionStatusCode())) {
			OBTransactionIndividualStatus1Code status = OBTransactionIndividualStatus1Code.fromValue(
					paymentInstructionProposal.getPaymentInstruction().getPaymentInstructionStatusCode().toString());
			oBWriteDataDomesticResponse1.setStatus(status);
		}

		if (!NullCheckUtils.isNullOrEmpty(
				paymentInstructionProposal.getPaymentInstruction().getInstructionStatusUpdateDateTime())) {

			oBWriteDataDomesticResponse1.setStatusUpdateDateTime(paymentInstructionProposal.getPaymentInstruction()
					.getInstructionStatusUpdateDateTime());
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal().getCharges())) {

			List<OBCharge1> charges = new ArrayList<>();
			OBCharge1 charge = new OBCharge1();
			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal().getCharges()
					.get(0).getChargeBearer())) {

				String chargeBearer = paymentInstructionProposal.getPaymentInstructionProposal().getCharges().get(0)
						.getChargeBearer().toString();
				OBChargeBearerType1Code oBChargeBearerType1Code = OBChargeBearerType1Code.fromValue(chargeBearer);
				charge.setChargeBearer(oBChargeBearerType1Code);
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getPaymentInstructionProposal().getCharges().get(0).getType())) {
				charge.setType(
						paymentInstructionProposal.getPaymentInstructionProposal().getCharges().get(0).getType());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal().getCharges()
					.get(0).getAmount().getTransactionCurrency())) {

				OBCharge1Amount amount = new OBCharge1Amount();
				Double chargeAmountMule = paymentInstructionProposal.getPaymentInstructionProposal().getCharges().get(0)
						.getAmount().getTransactionCurrency();
				amount.setAmount(String.valueOf(chargeAmountMule));
				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal()
						.getCharges().get(0).getCurrency().getIsoAlphaCode())) {
					amount.setCurrency(paymentInstructionProposal.getPaymentInstructionProposal().getCharges().get(0)
							.getCurrency().getIsoAlphaCode());
				}
				charge.setAmount(amount);
			}

			charges.add(charge);
			oBWriteDataDomesticResponse1.setCharges(charges);
		}

		if (!NullCheckUtils
				.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal().getInstructionReference())) {
			oBDomestic1.setInstructionIdentification(
					paymentInstructionProposal.getPaymentInstructionProposal().getInstructionReference());
		}

		if (!NullCheckUtils.isNullOrEmpty(
				paymentInstructionProposal.getPaymentInstructionProposal().getInstructionEndToEndReference())) {
			oBDomestic1.setEndToEndIdentification(
					paymentInstructionProposal.getPaymentInstructionProposal().getInstructionEndToEndReference());
		}

		if (!NullCheckUtils.isNullOrEmpty(
				paymentInstructionProposal.getPaymentInstructionProposal().getInstructionLocalInstrument())) {
			oBDomestic1.setLocalInstrument(
					paymentInstructionProposal.getPaymentInstructionProposal().getInstructionLocalInstrument());
		}

		if ((!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal()
				.getFinancialEventAmount().getTransactionCurrency()))
				&& (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal()
						.getTransactionCurrency().getIsoAlphaCode()))) {
			OBDomestic1InstructedAmount oBDomestic1InstructedAmount = new OBDomestic1InstructedAmount();
			Double amountMule = paymentInstructionProposal.getPaymentInstructionProposal().getFinancialEventAmount()
					.getTransactionCurrency();
			oBDomestic1InstructedAmount.setAmount(BigDecimal.valueOf(amountMule).toPlainString());
			oBDomestic1InstructedAmount.setCurrency(paymentInstructionProposal.getPaymentInstructionProposal()
					.getTransactionCurrency().getIsoAlphaCode());
			oBDomestic1.setInstructedAmount(oBDomestic1InstructedAmount);
		}

		if (!NullCheckUtils.isNullOrEmpty(
				paymentInstructionProposal.getPaymentInstructionProposal().getAuthorisingPartyAccount())) {

			OBCashAccountDebtor3 oBCashAccountDebtor3 = new OBCashAccountDebtor3();

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal()
					.getAuthorisingPartyAccount().getSchemeName())) {
				oBCashAccountDebtor3.setSchemeName(paymentInstructionProposal.getPaymentInstructionProposal()
						.getAuthorisingPartyAccount().getSchemeName());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal()
					.getAuthorisingPartyAccount().getAccountIdentification())) {
				oBCashAccountDebtor3.setIdentification(paymentInstructionProposal.getPaymentInstructionProposal()
						.getAuthorisingPartyAccount().getAccountIdentification());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal()
					.getAuthorisingPartyAccount().getAccountName())) {
				oBCashAccountDebtor3.setName(paymentInstructionProposal.getPaymentInstructionProposal()
						.getAuthorisingPartyAccount().getAccountName());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal()
					.getAuthorisingPartyAccount().getSecondaryIdentification())) {
				oBCashAccountDebtor3.setSecondaryIdentification(paymentInstructionProposal
						.getPaymentInstructionProposal().getAuthorisingPartyAccount().getSecondaryIdentification());
			}
			oBDomestic1.setDebtorAccount(oBCashAccountDebtor3);
		}

		if (!NullCheckUtils
				.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal().getProposingPartyAccount())) {

			OBCashAccountCreditor2 oBCashAccountCreditor2 = new OBCashAccountCreditor2();
			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal()
					.getProposingPartyAccount().getSchemeName())) {
				oBCashAccountCreditor2.setSchemeName(paymentInstructionProposal.getPaymentInstructionProposal()
						.getProposingPartyAccount().getSchemeName());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal()
					.getProposingPartyAccount().getAccountIdentification())) {
				oBCashAccountCreditor2.setIdentification(paymentInstructionProposal.getPaymentInstructionProposal()
						.getProposingPartyAccount().getAccountIdentification());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal()
					.getProposingPartyAccount().getAccountName())) {
				oBCashAccountCreditor2.setName(paymentInstructionProposal.getPaymentInstructionProposal()
						.getProposingPartyAccount().getAccountName());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal()
					.getProposingPartyAccount().getSecondaryIdentification())) {
				oBCashAccountCreditor2.setSecondaryIdentification(paymentInstructionProposal
						.getPaymentInstructionProposal().getProposingPartyAccount().getSecondaryIdentification());
			}

			oBDomestic1.setCreditorAccount(oBCashAccountCreditor2);
		}

		if (!NullCheckUtils.isNullOrEmpty(
				paymentInstructionProposal.getPaymentInstructionProposal().getProposingPartyPostalAddress())) {
			OBPostalAddress6 oBPostalAddress6 = new OBPostalAddress6();

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal()
					.getProposingPartyPostalAddress().getAddressType())) {
				OBAddressTypeCode addressType = OBAddressTypeCode.fromValue(paymentInstructionProposal
						.getPaymentInstructionProposal().getProposingPartyPostalAddress().getAddressType());
				oBPostalAddress6.setAddressType(addressType);
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal()
					.getProposingPartyPostalAddress().getDepartment())) {
				oBPostalAddress6.setDepartment(paymentInstructionProposal.getPaymentInstructionProposal()
						.getProposingPartyPostalAddress().getDepartment());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal()
					.getProposingPartyPostalAddress().getSubDepartment())) {
				oBPostalAddress6.setSubDepartment(paymentInstructionProposal.getPaymentInstructionProposal()
						.getProposingPartyPostalAddress().getSubDepartment());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal()
					.getProposingPartyPostalAddress().getGeoCodeBuildingName())) {
				oBPostalAddress6.setStreetName(paymentInstructionProposal.getPaymentInstructionProposal()
						.getProposingPartyPostalAddress().getGeoCodeBuildingName());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal()
					.getProposingPartyPostalAddress().getGeoCodeBuildingNumber())) {
				oBPostalAddress6.setBuildingNumber(paymentInstructionProposal.getPaymentInstructionProposal()
						.getProposingPartyPostalAddress().getGeoCodeBuildingNumber());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal()
					.getProposingPartyPostalAddress().getPostCodeNumber())) {
				oBPostalAddress6.setPostCode(paymentInstructionProposal.getPaymentInstructionProposal()
						.getProposingPartyPostalAddress().getPostCodeNumber());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal()
					.getProposingPartyPostalAddress().getTownName())) {
				oBPostalAddress6.setTownName(paymentInstructionProposal.getPaymentInstructionProposal()
						.getProposingPartyPostalAddress().getTownName());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal()
					.getProposingPartyPostalAddress().getCountrySubDivision())) {
				oBPostalAddress6.setCountrySubDivision(paymentInstructionProposal.getPaymentInstructionProposal()
						.getProposingPartyPostalAddress().getCountrySubDivision());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposal()
					.getProposingPartyPostalAddress().getAddressCountry())) {
				oBPostalAddress6.setCountry(paymentInstructionProposal.getPaymentInstructionProposal()
						.getProposingPartyPostalAddress().getAddressCountry().getIsoCountryAlphaTwoCode());
			}

			List<String> addLine = paymentInstructionProposal.getPaymentInstructionProposal()
					.getProposingPartyPostalAddress().getAddressLine();
			if (!NullCheckUtils.isNullOrEmpty(addLine) && !addLine.isEmpty()) {
				oBPostalAddress6.setAddressLine(paymentInstructionProposal.getPaymentInstructionProposal()
						.getProposingPartyPostalAddress().getAddressLine());
			}
			oBDomestic1.setCreditorPostalAddress(oBPostalAddress6);
		}
	}

}
