package com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.client.DomesticPaymentsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.delegate.DomesticPaymentsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.PaymentInstructionProposalComposite;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.transformer.DomesticPaymentsFoundationServiceTransformer;
import com.capgemini.psd2.pisp.adapter.DomesticPaymentsAdapter;
import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentDomesticSubmitPOST201Response;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@Component
public class DomesticPaymentsFoundationServiceAdapter implements DomesticPaymentsAdapter {

	@Autowired
	private DomesticPaymentsFoundationServiceDelegate domPayConsDelegate;
	
	@Autowired
	private DomesticPaymentsFoundationServiceClient domPayConsClient;

	@Autowired
	private DomesticPaymentsFoundationServiceTransformer domPayConsTransformer;
	
	@Value("${foundationService.domesticPaymentBaseURL}")
	private String domesticPaymentSubmissionBaseURL;
	
	///v1.0/domestic/payment-instructions/12
	public PaymentDomesticSubmitPOST201Response retrieveStagedDomesticPayments(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {

		RequestInfo requestInfo = new RequestInfo();

		HttpHeaders httpHeaders = domPayConsDelegate.createPaymentRequestHeaders(customPaymentStageIdentifiers,params);
		String domesticPaymentURL = domPayConsDelegate.getPaymentFoundationServiceURL(domesticPaymentSubmissionBaseURL,
				customPaymentStageIdentifiers.getPaymentSubmissionId());
		requestInfo.setUrl(domesticPaymentURL);
		PaymentInstructionProposalComposite paymentInstructionProposal = domPayConsClient
				.restTransportForDomesticPaymentFoundationService(requestInfo,
						PaymentInstructionProposalComposite.class, httpHeaders);
		return domPayConsTransformer.transformDomesticPaymentResponse(paymentInstructionProposal);

	}

	public PaymentDomesticSubmitPOST201Response processDomesticPayments(
			CustomDPaymentsPOSTRequest domesticPaymentsRequest,
			Map<String, OBTransactionIndividualStatus1Code> paymentStatusMap, Map<String, String> params) {
		
		RequestInfo requestInfo = new RequestInfo();
		
		HttpHeaders httpHeaders = domPayConsDelegate.createPaymentRequestHeadersPost(params);
		
		String domesticPaymentPostURL = domPayConsDelegate.postPaymentFoundationServiceURL(domesticPaymentSubmissionBaseURL);
		
		requestInfo.setUrl(domesticPaymentPostURL);
		
		PaymentInstructionProposalComposite paymentInstructionProposalCompositeRequest = domPayConsDelegate.transformDomesticSubmissionResponseFromAPIToFDForInsert(domesticPaymentsRequest,params);
		
		PaymentInstructionProposalComposite paymentInstructionProposalCompositeResponse = domPayConsClient.restTransportForDomesticPaymentFoundationServicePost(requestInfo , paymentInstructionProposalCompositeRequest, PaymentInstructionProposalComposite.class, httpHeaders);
		
		return domPayConsDelegate.transformDomesticConsentResponseFromFDToAPIForInsert(paymentInstructionProposalCompositeResponse);
	}
}
