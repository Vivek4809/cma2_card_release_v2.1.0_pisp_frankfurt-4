package com.capgemini.tpp.muleapi.schema;

import java.util.List;

public class ApisResponse {

	private int total;
	private List<Api> apis;

	public int getTotal() {
		return this.total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<Api> getApis() {
		return apis;
	}

	public void setApis(List<Api> apis) {
		this.apis = apis;
	}
}
