
package com.capgemini.tpp.muleapi.schema;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "identity_management"
})
public class Properties implements Serializable
{

    @JsonProperty("identity_management")
    private IdentityManagement identityManagement;
    private static final long serialVersionUID = -2768335850985692721L;

    @JsonProperty("identity_management")
    public IdentityManagement getIdentityManagement() {
        return identityManagement;
    }

    @JsonProperty("identity_management")
    public void setIdentityManagement(IdentityManagement identityManagement) {
        this.identityManagement = identityManagement;
    }

}
