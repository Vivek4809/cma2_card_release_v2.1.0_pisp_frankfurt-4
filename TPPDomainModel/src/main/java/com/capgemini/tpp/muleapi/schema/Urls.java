
package com.capgemini.tpp.muleapi.schema;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "sign_on",
    "sign_out"
})
public class Urls implements Serializable
{

    @JsonProperty("sign_on")
    private String signOn;
    @JsonProperty("sign_out")
    private String signOut;
    private static final long serialVersionUID = 6262935356817976494L;

    @JsonProperty("sign_on")
    public String getSignOn() {
        return signOn;
    }

    @JsonProperty("sign_on")
    public void setSignOn(String signOn) {
        this.signOn = signOn;
    }

    @JsonProperty("sign_out")
    public String getSignOut() {
        return signOut;
    }

    @JsonProperty("sign_out")
    public void setSignOut(String signOut) {
        this.signOut = signOut;
    }

}
