
package com.capgemini.tpp.muleapi.schema;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "base",
    "addOn"
})
public class ObjectStoreKeys implements Serializable
{

    @JsonProperty("base")
    private Integer base;
    @JsonProperty("addOn")
    private Integer addOn;
    private static final long serialVersionUID = -2148072122773427813L;

    @JsonProperty("base")
    public Integer getBase() {
        return base;
    }

    @JsonProperty("base")
    public void setBase(Integer base) {
        this.base = base;
    }

    @JsonProperty("addOn")
    public Integer getAddOn() {
        return addOn;
    }

    @JsonProperty("addOn")
    public void setAddOn(Integer addOn) {
        this.addOn = addOn;
    }

}
