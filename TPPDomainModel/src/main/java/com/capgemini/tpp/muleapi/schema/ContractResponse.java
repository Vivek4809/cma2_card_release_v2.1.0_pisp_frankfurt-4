package com.capgemini.tpp.muleapi.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "audit", "masterOrganizationId", "organizationId", "id", "status", "approvedDate", "rejectedDate",
		"revokedDate", "applicationId", "application", "tierId", "tier", "requestedTierId", "requestedTier", "terms",
		"apiVersionId", "apiVersion", "partyId", "partyName" })
public class ContractResponse{

	@JsonProperty("audit")
	private Audit audit;
	@JsonProperty("masterOrganizationId")
	private String masterOrganizationId;
	@JsonProperty("organizationId")
	private String organizationId;
	@JsonProperty("id")
	private int id;
	@JsonProperty("status")
	private String status;
	@JsonProperty("approvedDate")
	private String approvedDate;
	@JsonProperty("rejectedDate")
	private Object rejectedDate;
	@JsonProperty("revokedDate")
	private Object revokedDate;
	@JsonProperty("applicationId")
	private int applicationId;
	@JsonProperty("application")
	private Application application;
	@JsonProperty("tierId")
	private Object tierId;
	@JsonProperty("tier")
	private Object tier;
	@JsonProperty("requestedTierId")
	private Object requestedTierId;
	@JsonProperty("requestedTier")
	private Object requestedTier;
	@JsonProperty("terms")
	private Object terms;
	@JsonProperty("apiVersionId")
	private int apiVersionId;
	@JsonProperty("apiVersion")
	private ApiVersion apiVersion;
	@JsonProperty("partyId")
	private String partyId;
	@JsonProperty("partyName")
	private String partyName;

	private static final long serialVersionUID = -6978122425755580762L;

	@JsonProperty("audit")
	public Audit getAudit() {
		return audit;
	}

	@JsonProperty("audit")
	public void setAudit(Audit audit) {
		this.audit = audit;
	}

	

	@JsonProperty("masterOrganizationId")
	public String getMasterOrganizationId() {
		return masterOrganizationId;
	}

	@JsonProperty("masterOrganizationId")
	public void setMasterOrganizationId(String masterOrganizationId) {
		this.masterOrganizationId = masterOrganizationId;
	}

	

	@JsonProperty("organizationId")
	public String getOrganizationId() {
		return organizationId;
	}

	@JsonProperty("organizationId")
	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	
	@JsonProperty("id")
	public int getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(int id) {
		this.id = id;
	}

	

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	@JsonProperty("status")
	public void setStatus(String status) {
		this.status = status;
	}

	
	@JsonProperty("approvedDate")
	public String getApprovedDate() {
		return approvedDate;
	}

	@JsonProperty("approvedDate")
	public void setApprovedDate(String approvedDate) {
		this.approvedDate = approvedDate;
	}

	

	@JsonProperty("rejectedDate")
	public Object getRejectedDate() {
		return rejectedDate;
	}

	@JsonProperty("rejectedDate")
	public void setRejectedDate(Object rejectedDate) {
		this.rejectedDate = rejectedDate;
	}

	

	@JsonProperty("revokedDate")
	public Object getRevokedDate() {
		return revokedDate;
	}

	@JsonProperty("revokedDate")
	public void setRevokedDate(Object revokedDate) {
		this.revokedDate = revokedDate;
	}

	

	@JsonProperty("applicationId")
	public int getApplicationId() {
		return applicationId;
	}

	@JsonProperty("applicationId")
	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}

	

	@JsonProperty("application")
	public Application getApplication() {
		return application;
	}

	@JsonProperty("application")
	public void setApplication(Application application) {
		this.application = application;
	}

	

	@JsonProperty("tierId")
	public Object getTierId() {
		return tierId;
	}

	@JsonProperty("tierId")
	public void setTierId(Object tierId) {
		this.tierId = tierId;
	}

	

	@JsonProperty("tier")
	public Object getTier() {
		return tier;
	}

	@JsonProperty("tier")
	public void setTier(Object tier) {
		this.tier = tier;
	}

	
	@JsonProperty("requestedTierId")
	public Object getRequestedTierId() {
		return requestedTierId;
	}

	@JsonProperty("requestedTierId")
	public void setRequestedTierId(Object requestedTierId) {
		this.requestedTierId = requestedTierId;
	}

	
	@JsonProperty("requestedTier")
	public Object getRequestedTier() {
		return requestedTier;
	}

	@JsonProperty("requestedTier")
	public void setRequestedTier(Object requestedTier) {
		this.requestedTier = requestedTier;
	}

	
	@JsonProperty("terms")
	public Object getTerms() {
		return terms;
	}

	@JsonProperty("terms")
	public void setTerms(Object terms) {
		this.terms = terms;
	}

	
	@JsonProperty("apiVersionId")
	public int getApiVersionId() {
		return apiVersionId;
	}

	@JsonProperty("apiVersionId")
	public void setApiVersionId(int apiVersionId) {
		this.apiVersionId = apiVersionId;
	}

	@JsonProperty("apiVersion")
	public ApiVersion getApiVersion() {
		return apiVersion;
	}

	@JsonProperty("apiVersion")
	public void setApiVersion(ApiVersion apiVersion) {
		this.apiVersion = apiVersion;
	}

	@JsonProperty("partyId")
	public String getPartyId() {
		return partyId;
	}

	@JsonProperty("partyId")
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	@JsonProperty("partyName")
	public String getPartyName() {
		return partyName;
	}

	@JsonProperty("partyName")
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

}
