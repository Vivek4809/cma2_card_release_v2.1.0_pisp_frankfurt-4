
package com.capgemini.tpp.muleapi.schema;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "hideApiManagerDesigner",
    "environments",
    "hideFormerApiPlatform"
})
public class Crowd implements Serializable
{

    @JsonProperty("hideApiManagerDesigner")
    private Boolean hideApiManagerDesigner;
    @JsonProperty("environments")
    private Boolean environments;
    @JsonProperty("hideFormerApiPlatform")
    private Boolean hideFormerApiPlatform;
    private static final long serialVersionUID = -2186268520484262728L;

    @JsonProperty("hideApiManagerDesigner")
    public Boolean getHideApiManagerDesigner() {
        return hideApiManagerDesigner;
    }

    @JsonProperty("hideApiManagerDesigner")
    public void setHideApiManagerDesigner(Boolean hideApiManagerDesigner) {
        this.hideApiManagerDesigner = hideApiManagerDesigner;
    }

    @JsonProperty("environments")
    public Boolean getEnvironments() {
        return environments;
    }

    @JsonProperty("environments")
    public void setEnvironments(Boolean environments) {
        this.environments = environments;
    }

    @JsonProperty("hideFormerApiPlatform")
    public Boolean getHideFormerApiPlatform() {
        return hideFormerApiPlatform;
    }

    @JsonProperty("hideFormerApiPlatform")
    public void setHideFormerApiPlatform(Boolean hideFormerApiPlatform) {
        this.hideFormerApiPlatform = hideFormerApiPlatform;
    }

}
