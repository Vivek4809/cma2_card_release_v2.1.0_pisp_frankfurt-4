package com.capgemini.tpp.ldap.client.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientAttributes {
	private String clientId;
	private List<String> redirectUris;
	private List<String> grantTypes;
	private String clientSecret;
	private String name;
	private String description;
	private boolean refreshRolling;
	private int persistentGrantExpirationTime;
	private String persistentGrantExpirationTimeUnit;
	private boolean bypassApprovalPage;
	private boolean restrictScopes;
	private Object logoUrl;
	private Object clientCertIssuerDn;
	private Object clientCertSubjectDn;
	private SupplementalInfo supplementalInfo;
	private boolean pingAccessLogoutCapable;
	private List<Object> logoutUris;
	private List<String> restrictedScopes;
	
	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public List<String> getRedirectUris() {
		return redirectUris;
	}

	public void setRedirectUris(List<String> redirectUris) {
		this.redirectUris = redirectUris;
	}

	public List<String> getGrantTypes() {
		return grantTypes;
	}

	public void setGrantTypes(List<String> grantTypes) {
		this.grantTypes = grantTypes;
	}

	public String getClientSecret() {
		return this.clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Object getLogoUrl() {
		return this.logoUrl;
	}

	public void setLogoUrl(Object logoUrl) {
		this.logoUrl = logoUrl;
	}

	public boolean getRefreshRolling() {
		return this.refreshRolling;
	}

	public void setRefreshRolling(boolean refreshRolling) {
		this.refreshRolling = refreshRolling;
	}

	public int getPersistentGrantExpirationTime() {
		return this.persistentGrantExpirationTime;
	}

	public void setPersistentGrantExpirationTime(int persistentGrantExpirationTime) {
		this.persistentGrantExpirationTime = persistentGrantExpirationTime;
	}

	public String getPersistentGrantExpirationTimeUnit() {
		return this.persistentGrantExpirationTimeUnit;
	}

	public void setPersistentGrantExpirationTimeUnit(String persistentGrantExpirationTimeUnit) {
		this.persistentGrantExpirationTimeUnit = persistentGrantExpirationTimeUnit;
	}

	public boolean getBypassApprovalPage() {
		return this.bypassApprovalPage;
	}

	public void setBypassApprovalPage(boolean bypassApprovalPage) {
		this.bypassApprovalPage = bypassApprovalPage;
	}

	public boolean getRestrictScopes() {
		return this.restrictScopes;
	}

	public void setRestrictScopes(boolean restrictScopes) {
		this.restrictScopes = restrictScopes;
	}

	public List<Object> getLogoutUris() {
		return logoutUris;
	}

	public void setLogoutUris(List<Object> logoutUris) {
		this.logoutUris = logoutUris;
	}

	public List<String> getRestrictedScopes() {
		return restrictedScopes;
	}

	public void setRestrictedScopes(List<String> restrictedScopes) {
		this.restrictedScopes = restrictedScopes;
	}

	public Object getClientCertIssuerDn() {
		return this.clientCertIssuerDn;
	}

	public void setClientCertIssuerDn(Object clientCertIssuerDn) {
		this.clientCertIssuerDn = clientCertIssuerDn;
	}

	public Object getClientCertSubjectDn() {
		return this.clientCertSubjectDn;
	}

	public void setClientCertSubjectDn(Object clientCertSubjectDn) {
		this.clientCertSubjectDn = clientCertSubjectDn;
	}

	public SupplementalInfo getSupplementalInfo() {
		return this.supplementalInfo;
	}

	public void setSupplementalInfo(SupplementalInfo supplementalInfo) {
		this.supplementalInfo = supplementalInfo;
	}

	public boolean getPingAccessLogoutCapable() {
		return this.pingAccessLogoutCapable;
	}

	public void setPingAccessLogoutCapable(boolean pingAccessLogoutCapable) {
		this.pingAccessLogoutCapable = pingAccessLogoutCapable;
	}

}