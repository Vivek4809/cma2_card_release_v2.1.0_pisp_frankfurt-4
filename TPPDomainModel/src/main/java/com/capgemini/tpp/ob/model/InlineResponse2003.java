package com.capgemini.tpp.ob.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import io.swagger.annotations.ApiModelProperty;

/**
 * InlineResponse2003
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class InlineResponse2003   {
  @JsonProperty("Resources")
  private List<ExternalIdentity> resources = null;

  @JsonProperty("itemsPerPage")
  private Integer itemsPerPage = null;

  /**
   * Gets or Sets schemas
   */
  public enum SchemasEnum {
    LISTRESPONSE("urn:ietf:params:scim:api:messages:2.0:ListResponse");

    private String value;

    SchemasEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static SchemasEnum fromValue(String text) {
      for (SchemasEnum b : SchemasEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("schemas")
  private List<SchemasEnum> schemas = null;

  @JsonProperty("startIndex")
  private Integer startIndex = null;

  @JsonProperty("totalResults")
  private Integer totalResults = null;

  public InlineResponse2003 resources(List<ExternalIdentity> resources) {
    this.resources = resources;
    return this;
  }

  public InlineResponse2003 addResourcesItem(ExternalIdentity resourcesItem) {
    if (this.resources == null) {
      this.resources = new ArrayList<ExternalIdentity>();
    }
    this.resources.add(resourcesItem);
    return this;
  }

   /**
   * A multi-valued list of complex objects containing the requested resources.
   * @return resources
  **/
  @ApiModelProperty(value = "A multi-valued list of complex objects containing the requested resources.")

  @Valid

  public List<ExternalIdentity> getResources() {
    return resources;
  }

  public void setResources(List<ExternalIdentity> resources) {
    this.resources = resources;
  }

  public InlineResponse2003 itemsPerPage(Integer itemsPerPage) {
    this.itemsPerPage = itemsPerPage;
    return this;
  }

   /**
   * The number of resources returned in a list response page.
   * @return itemsPerPage
  **/
  @ApiModelProperty(value = "The number of resources returned in a list response page.")


  public Integer getItemsPerPage() {
    return itemsPerPage;
  }

  public void setItemsPerPage(Integer itemsPerPage) {
    this.itemsPerPage = itemsPerPage;
  }

  public InlineResponse2003 schemas(List<SchemasEnum> schemas) {
    this.schemas = schemas;
    return this;
  }

  public InlineResponse2003 addSchemasItem(SchemasEnum schemasItem) {
    if (this.schemas == null) {
      this.schemas = new ArrayList<SchemasEnum>();
    }
    this.schemas.add(schemasItem);
    return this;
  }

   /**
   * Contains a list of one or more URIs that indicate included SCIM schemas that are used to indicate the attributes contained within a resource.
   * @return schemas
  **/
  @ApiModelProperty(value = "Contains a list of one or more URIs that indicate included SCIM schemas that are used to indicate the attributes contained within a resource.")


  public List<SchemasEnum> getSchemas() {
    return schemas;
  }

  public void setSchemas(List<SchemasEnum> schemas) {
    this.schemas = schemas;
  }

  public InlineResponse2003 startIndex(Integer startIndex) {
    this.startIndex = startIndex;
    return this;
  }

   /**
   * The 1-based index of the first result in the current set of list results.
   * @return startIndex
  **/
  @ApiModelProperty(value = "The 1-based index of the first result in the current set of list results.")


  public Integer getStartIndex() {
    return startIndex;
  }

  public void setStartIndex(Integer startIndex) {
    this.startIndex = startIndex;
  }

  public InlineResponse2003 totalResults(Integer totalResults) {
    this.totalResults = totalResults;
    return this;
  }

   /**
   * The total number of results returned by the list or query operation.
   * @return totalResults
  **/
  @ApiModelProperty(value = "The total number of results returned by the list or query operation.")


  public Integer getTotalResults() {
    return totalResults;
  }

  public void setTotalResults(Integer totalResults) {
    this.totalResults = totalResults;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InlineResponse2003 inlineResponse2003 = (InlineResponse2003) o;
    return Objects.equals(this.resources, inlineResponse2003.resources) &&
        Objects.equals(this.itemsPerPage, inlineResponse2003.itemsPerPage) &&
        Objects.equals(this.schemas, inlineResponse2003.schemas) &&
        Objects.equals(this.startIndex, inlineResponse2003.startIndex) &&
        Objects.equals(this.totalResults, inlineResponse2003.totalResults);
  }

  @Override
  public int hashCode() {
    return Objects.hash(resources, itemsPerPage, schemas, startIndex, totalResults);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class InlineResponse2003 {\n");
    
    sb.append("    resources: ").append(toIndentedString(resources)).append("\n");
    sb.append("    itemsPerPage: ").append(toIndentedString(itemsPerPage)).append("\n");
    sb.append("    schemas: ").append(toIndentedString(schemas)).append("\n");
    sb.append("    startIndex: ").append(toIndentedString(startIndex)).append("\n");
    sb.append("    totalResults: ").append(toIndentedString(totalResults)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

