package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.constants.CustomerAccountsFilterFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.FilteredAccounts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.AccountEntitlements2;

public class AccountTypeFilter implements FilterationChain {

	private FilterationChain nextInChain;

	@Override
	public void setNext(FilterationChain next) {
		nextInChain = next;

	}

	@Override
	public List<AccountEntitlements> process(List<AccountEntitlements> accounts, PartyEntitlements partyEntitlements, String consentFlowType,
			Map<String, Map<String, List<String>>> accountFiltering) {

		FilteredAccounts filteredAccounts = new FilteredAccounts();
		List<AccountEntitlements> accountList = new ArrayList<>();

		if (!accounts.isEmpty() || accounts.size() > 0)

		// if (accounts.getAccount().size() >
		// 0||accounts.getCreditCardAccount().size() > 0)
		{
			Map<String, List<String>> accountTypeList = accountFiltering
					.get(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTTYPE);

			if (accountTypeList != null && !accountTypeList.isEmpty()) {
				List<String> validAccountTypeForFlow = (ArrayList<String>) accountTypeList.get(consentFlowType);
				validAccountTypeForFlow = validAccountTypeForFlow.stream().map(u -> u.toUpperCase())
						.collect(Collectors.toList());

				for (AccountEntitlements accnt : accounts) {

					if (accnt.getAccount().getSourceSystemAccountType() != null) {
						if (validAccountTypeForFlow
								.contains(accnt.getAccount().getSourceSystemAccountType().toString().toUpperCase().trim())) {

							AccountEntitlements accntEntitlment = new AccountEntitlements();
							accntEntitlment.setAccount(accnt.getAccount());
							accntEntitlment.setEntitlements(accnt.getEntitlements());
							accountList.add(accntEntitlment);
						}
					}
				}
				return nextInChain.process(accountList, partyEntitlements, consentFlowType, accountFiltering);

			} else {
				return nextInChain.process(accountList, partyEntitlements, consentFlowType, accountFiltering);
			}

		}
		return accountList;

	}
	
	@Override
	public List<AccountEntitlements2> processPISP(List<AccountEntitlements2> accounts, com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.PartyEntitlements partyEntitlements, String consentFlowType,
			Map<String, Map<String, List<String>>> accountFiltering) {

		FilteredAccounts filteredAccounts = new FilteredAccounts();
		List<AccountEntitlements2> accountList = new ArrayList<>();

		if (!accounts.isEmpty() || accounts.size() > 0)

		// if (accounts.getAccount().size() >
		// 0||accounts.getCreditCardAccount().size() > 0)
		{
			Map<String, List<String>> accountTypeList = accountFiltering
					.get(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTTYPE);

			if (accountTypeList != null && !accountTypeList.isEmpty()) {
				List<String> validAccountTypeForFlow = (ArrayList<String>) accountTypeList.get(consentFlowType);
				validAccountTypeForFlow = validAccountTypeForFlow.stream().map(u -> u.toUpperCase())
						.collect(Collectors.toList());

				for (AccountEntitlements2 accnt : accounts) {

					if (accnt.getAccount().getSourceSystemAccountType() != null) {
						if (validAccountTypeForFlow
								.contains(accnt.getAccount().getSourceSystemAccountType().toString().toUpperCase().trim())) {

							AccountEntitlements2 accntEntitlment = new AccountEntitlements2();
							accntEntitlment.setAccount(accnt.getAccount());
							accntEntitlment.setBalance(accnt.getBalance());
							accntEntitlment.setEntitlements(accnt.getEntitlements());
							accountList.add(accntEntitlment);
						}
					}
				}
				return nextInChain.processPISP(accountList, partyEntitlements, consentFlowType, accountFiltering);

			} else {
				return nextInChain.processPISP(accountList, partyEntitlements, consentFlowType, accountFiltering);
			}

		}
		return accountList;

	}

}
