package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@Component
public class CustomerAccountsFilterFoundationServiceClient {
	
	/** The rest client. */
	@Autowired
	@Qualifier("restClientFoundation")
	private RestClientSync restClient;

	
/**
 * Rest transport for Customer Account List 
 * @param reqInfo
 * @param responseType
 * @param headers
 * @return list of accounts 
 */
	public DigitalUserProfile restTransportForCustomerAccountProfileAISP(RequestInfo reqInfo, Class <DigitalUserProfile> responseType, MultiValueMap<String, String> params, HttpHeaders headers)
	{
		reqInfo.setUrl(UriComponentsBuilder.fromHttpUrl(reqInfo.getUrl()).queryParams(params).build().toString());
		DigitalUserProfile digitalUserProfile=restClient.callForGet(reqInfo, responseType, headers);
	    return digitalUserProfile;
	}
	
	public com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.DigitalUserProfile restTransportForCustomerAccountProfilePISP(RequestInfo reqInfo, Class <com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.DigitalUserProfile> responseType, MultiValueMap<String, String> params, HttpHeaders headers)
	{
		reqInfo.setUrl(UriComponentsBuilder.fromHttpUrl(reqInfo.getUrl()).queryParams(params).build().toString());
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.DigitalUserProfile digitalUserProfile=restClient.callForGet(reqInfo, responseType, headers);
	    return digitalUserProfile;
	}
	
	public Accnts restTransportForSingleAccountProfile(RequestInfo reqInfo, Class <Accounts> responseType, HttpHeaders headers)
	{
		Accounts accounts=restClient.callForGet(reqInfo, responseType, headers);
		Accnts accnts=new Accnts();
		accnts.getAccount().add(accounts.getAccount());
		return accnts;
	}

}
