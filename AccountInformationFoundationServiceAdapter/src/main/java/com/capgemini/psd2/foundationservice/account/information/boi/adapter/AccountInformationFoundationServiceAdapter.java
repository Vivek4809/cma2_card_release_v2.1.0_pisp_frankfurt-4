/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.information.boi.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.adapter.AccountInformationAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountInformationResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.client.AccountInformationFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.constants.AccountInformationFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.delegate.AccountInformationFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.Account;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The Class AccountInformationFoundationServiceAdapter.
 */
@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "foundationService")
public class AccountInformationFoundationServiceAdapter implements AccountInformationAdapter {

	private Map<String, List<String>> jurisdictions = new HashMap<>();

	public Map<String, List<String>> getJurisdictions() {
		return jurisdictions;
	}

	public void setJurisdictions(Map<String, List<String>> jurisdictions) {
		this.jurisdictions = jurisdictions;
	}

	/** The account information foundation service delegate. */
	@Autowired
	private AccountInformationFoundationServiceDelegate accountInformationFoundationServiceDelegate;

	/** The account information foundation service client. */
	@Autowired
	private AccountInformationFoundationServiceClient accountInformationFoundationServiceClient;
	
	/* Customer Account Profile service Delegate */
    @Autowired
    private CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;
    
	@Value("${foundationService.consentFlowType}")
	private String consentFlowType;

	@Value("${foundationService.channelId}")
	private String localChannelId;

//	@Value("${foundationService.jurisdictionType}")
//	private String jurisdictionType;
	
	@Value("${foundationService.version}")
	private String version;

	PlatformAccountInformationResponse platformAccountInformationResponse;

	@Override
	public PlatformAccountInformationResponse retrieveAccountInformation(AccountMapping accountMapping,
			Map<String, String> params) {

		if (accountMapping == null || accountMapping.getPsuId() == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		if (params == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, consentFlowType);
		params.put(PSD2Constants.USER_ID, accountMapping.getPsuId());
		/*To be removed in Next Sprint(18)*/
		//params.put(PSD2Constants.TENANT_ID, jurisdictionType);
		params.put(PSD2Constants.CHANNEL_ID, params.get(AccountInformationFoundationServiceConstants.CHANNEL_ID).toUpperCase());
		//params.put(AccountInformationFoundationServiceConstants.CHANNEL_ID, params.get(AccountInformationFoundationServiceConstants.CHANNEL_ID).toUpperCase());
		params.put(PSD2Constants.CORRELATION_ID, accountMapping.getCorrelationId());
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,
				AccountInformationFoundationServiceConstants.SINGLE_ACCOUNT);
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = null;
		AccountDetails accountDetails;
		if (accountMapping != null && accountMapping.getAccountDetails() != null
				&& !accountMapping.getAccountDetails().isEmpty()) {
			accountDetails = accountMapping.getAccountDetails().get(0);
		} else{
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		
		if ((NullCheckUtils.isNullOrEmpty(accountDetails.getAccountSubType()) && params.get(PSD2Constants.CMAVERSION) == null) 
				|| accountDetails.getAccountSubType().toString().contentEquals("CurrentAccount") 
				||  accountDetails.getAccountSubType().toString().contentEquals("Savings")) {
			params.put(AccountInformationFoundationServiceConstants.ACCOUNT_SUBTYPE,
					accountDetails.getAccountSubType().toString());
			httpHeaders = accountInformationFoundationServiceDelegate.createRequestHeaders(requestInfo, accountMapping,
					params);

			String finalURL = accountInformationFoundationServiceDelegate.getFoundationServiceURL(version,
					params.get(AccountInformationFoundationServiceConstants.CHANNEL_ID).toUpperCase(), accountDetails.getAccountNSC(),
					accountDetails.getAccountNumber());
			requestInfo.setUrl(finalURL);
			Account accountInfo = accountInformationFoundationServiceClient
					.restTransportForCreditCardInformation(requestInfo, Account.class, httpHeaders);
			if (NullCheckUtils.isNullOrEmpty(accountInfo)) {
				throw AdapterException
						.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND_FOUNDATION_SERVICE);
			}
			for (AccountDetails accdet : accountMapping.getAccountDetails()) {
				params.put(accdet.getAccountNumber() + "_" + accdet.getAccountNSC(), accdet.getAccountId());
			}
			platformAccountInformationResponse = accountInformationFoundationServiceDelegate
					.transformResponseFromFDToAPI(accountInfo, params);
		} else if (accountDetails.getAccountSubType().toString().contentEquals("CreditCard")) {
			params.put(AccountInformationFoundationServiceConstants.ACCOUNT_SUBTYPE,
					accountDetails.getAccountSubType().toString());
			params.put("maskedPan", accountDetails.getAccount().getIdentification().
					substring(accountDetails.getAccount().getIdentification().length() - 4));
			httpHeaders = accountInformationFoundationServiceDelegate.createCreditCardRequestHeaders(requestInfo,
					accountMapping, params);
			String finalURL = accountInformationFoundationServiceDelegate.getCreditCardFoundationServiceURL(version,
					accountDetails.getAccount().getSecondaryIdentification(), accountDetails.getAccountNumber(),
					params.get(AccountInformationFoundationServiceConstants.CHANNEL_ID).toUpperCase());
			requestInfo.setUrl(finalURL);
			Account accountInfo = accountInformationFoundationServiceClient
					.restTransportForCreditCardInformation(requestInfo, Account.class, httpHeaders);
			if (NullCheckUtils.isNullOrEmpty(accountInfo)) {
				throw AdapterException
						.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND_FOUNDATION_SERVICE);
			}
			for (AccountDetails accdet : accountMapping.getAccountDetails()) {

				params.put(accdet.getAccountNumber() + "_" + accdet.getAccount().getIdentification(),
						accdet.getAccountId());

			}
			platformAccountInformationResponse = accountInformationFoundationServiceDelegate
					.transformResponseFromFDToAPI(accountInfo, params);

		}
		return platformAccountInformationResponse;
	}

	@Override
	public PlatformAccountInformationResponse retrieveMultipleAccountsInformation(AccountMapping accountMapping,
			Map<String, String> params) {

		if (accountMapping == null || accountMapping.getPsuId() == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		if (params == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		params.put(PSD2Constants.USER_ID, accountMapping.getPsuId());
		params.put(PSD2Constants.CHANNEL_ID,params.get(AccountInformationFoundationServiceConstants.CHANNEL_ID).toUpperCase());
		//params.put(AccountInformationFoundationServiceConstants.CHANNEL_ID, params.get(AccountInformationFoundationServiceConstants.CHANNEL_ID).toUpperCase());
		params.put(PSD2Constants.CORRELATION_ID, accountMapping.getCorrelationId());
		/*To be removed in Next Sprint(18)*/
		//params.put(PSD2Constants.TENANT_ID, jurisdictionType);
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, consentFlowType);
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,
				AccountInformationFoundationServiceConstants.MULTI_ACCOUNT);

		DigitalUserProfile digiUserProfile = commonFilterUtility.retrieveCustomerAccountList(accountMapping.getPsuId(),params);
		
		if (NullCheckUtils.isNullOrEmpty(digiUserProfile)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.ACCOUNT_NOT_FOUND);
		}
		List<AccountEntitlements> listAccountEntitlement = new ArrayList<>();
		for(AccountEntitlements account : digiUserProfile.getAccountEntitlements()){
			if(!account.getEntitlements().contains(AccountInformationFoundationServiceConstants.DISPLAY_ONLY_ACCOUNT)){
				listAccountEntitlement.add(account);
			}
		}
		digiUserProfile.setAccountEntitlements(listAccountEntitlement);
		for (AccountDetails accdet : accountMapping.getAccountDetails()) {
			if ((NullCheckUtils.isNullOrEmpty(accdet.getAccountSubType()) && params.get(PSD2Constants.CMAVERSION) == null) 
					|| accdet.getAccountSubType().toString().contentEquals("CurrentAccount") 
					||  accdet.getAccountSubType().toString().contentEquals("Savings")) {
				params.put(accdet.getAccountNumber() + "_" + accdet.getAccountNSC(), accdet.getAccountId());
			}
			
			else if (accdet.getAccountSubType().toString().contentEquals("CreditCard")) {
				params.put(accdet.getAccountNumber() + "_" + accdet.getAccount().getIdentification(),
						accdet.getAccountId());
			}

		}
		return accountInformationFoundationServiceDelegate.transformResponseFromFDToAPI(digiUserProfile, params);
	}

}