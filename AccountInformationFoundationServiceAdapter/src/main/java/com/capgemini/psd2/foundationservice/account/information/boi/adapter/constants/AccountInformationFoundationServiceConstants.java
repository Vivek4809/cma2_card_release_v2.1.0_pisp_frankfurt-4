/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.information.boi.adapter.constants;

/**
 * The Class AccountInformationFoundationServiceConstants.
 */
public class AccountInformationFoundationServiceConstants {
	
	/** The Constant FOUNDATION_SERVICE. */
	public static final String FOUNDATION_SERVICE = "foundationService";
	
	/** The Constant BASE_URL. */
	public static final String BASE_URL = "baseURL";
	
	/** The Constant ACCOUNT_ID. */
	public static final String ACCOUNT_ID = "accountId";
	
	public static final String CHANNEL_ID = "channelId";
	
	/** The Constant CURRENCY_REGEX. */
	public static final String CURRENCY_REGEX = "^[A-Z]{3}$";
	
	public static final String SCHEMENAME="SchemeName";
	public static final String IDENTIFICATION ="Identification";
	public static final String SERVICERSCHEMENAME ="ServicerSchemeName";
	public static final String SERVICERIDENTIFICATION="ServicerIdentification";
	public static final String IBAN ="IBAN";
	public static final String ACCOUNTNSCNUMBER ="AccountNSCNumber";
	public static final String BIC ="BIC";
	public static final String SORTCODEACCOUNTNUMBER  ="SORTCODEACCOUNTNUMBER";
	public static final String BICFI ="BICFI";
	public static final String ACCOUNT_SUBTYPE = "account_subtype";
	public static final String CURRENT_ACCOUNT="CurrentAccount";
	public static final String SAVINGS= "Savings";
	public static final String CREDIT_CARD="CreditCard";
	public static final String ACCOUNT="Account";
	public static final String MULTI_ACCOUNT = "Multi_Account";
	public static final String SINGLE_ACCOUNT = "Single_Account";
	public static final String RETAILNONRETAILCODE = "Retail";
	public static final String BRANDCODE_BOI_NI="BOI-NI";
	public static final String BRANDCODE_BOI_GB="BOI-GB";
	public static final String BRANDCODE_BOIROI="BOIROI";
	public static final String BRANDCODE_BOI_NIGB="BOI-NIGB";
	public static final String BRANDCODE_BOIUK="BOIUK";
	public static final String SOURCESYSTEMACCOUNTTYPE_CURRENTACCOUNT="Current Account";
	public static final String SOURCESYSTEMACCOUNTTYPE_SAVINGSACCOUNT="Savings Account";
	public static final String SOURCESYSTEMACCOUNTTYPE_CREDITCARD="Credit Card";
	public static final String CURRENT_ACCOUNT_FLAG = "CurrentAccount";
	public static final String SAVINGS_ACCOUNT_FLAG = "Savings";
	public static final String CREDIT_CARD_FLAG ="CreditCard";
	public static final String CURRENT_ACCOUNT_KEY = "Current_Account_Key";
	public static final String SAVINGS_ACCOUNT_KEY = "Savings_Account_Key";
	public static final String CREDIT_CARD_KEY ="CreditCard_Account_Key";
	public static final String JURISDICTION ="jurisdiction";
	public static final String PAN ="PAN";
	public static final String DISPLAY_ONLY_ACCOUNT = "Display Only Account";
	public static final String UK_OBIE_BICFI = "UK.OBIE.BICFI";
	public static final String UK_OBIE_IBAN = "UK.OBIE.IBAN";
	public static final String UK_OBIE_PAN = "UK.OBIE.PAN";
	public static final String UK_OBIE_SortCodeAccountNumber = "UK.OBIE.SortCodeAccountNumber";
	
}
