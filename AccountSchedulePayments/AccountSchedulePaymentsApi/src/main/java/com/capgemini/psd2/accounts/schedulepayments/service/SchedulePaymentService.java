package com.capgemini.psd2.accounts.schedulepayments.service;

import com.capgemini.psd2.aisp.domain.OBReadScheduledPayment1;

@FunctionalInterface
public interface SchedulePaymentService {

	public OBReadScheduledPayment1 retrieveSchedulePayments(String accountId);
	
}
