package com.capgemini.psd2.authentication.application.retrieve.mock.foundationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;



@EnableMongoRepositories(basePackages = { "com.capgemini.psd2" })
@SpringBootApplication
@ComponentScan(basePackages = { "com.capgemini.psd2" })
public class SCAAuthenticationRetrieveMockFoundationServiceApplication{

	public static void main(String[] args) {
		SpringApplication.run(SCAAuthenticationRetrieveMockFoundationServiceApplication.class, args);
	}

	
}
