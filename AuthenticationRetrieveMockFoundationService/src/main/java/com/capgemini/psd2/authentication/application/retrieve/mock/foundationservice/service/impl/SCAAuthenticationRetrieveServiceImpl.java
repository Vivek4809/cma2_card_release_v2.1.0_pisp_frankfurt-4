package com.capgemini.psd2.authentication.application.retrieve.mock.foundationservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.authentication.application.retrieve.mock.foundationservice.domain.AuthenticationParameters;
import com.capgemini.psd2.authentication.application.retrieve.mock.foundationservice.repository.SCAAuthenticationRetrieveServiceRepository;
import com.capgemini.psd2.authentication.application.retrieve.mock.foundationservice.service.SCAAuthenticationRetrieveService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;


@Service
public class SCAAuthenticationRetrieveServiceImpl implements SCAAuthenticationRetrieveService {

	@Autowired
	private SCAAuthenticationRetrieveServiceRepository authenticationParametersRetrieveRepository;

	@Autowired
	private ValidationUtility validationUtility;

	@Override
	public AuthenticationParameters retrieveAuthentication( String digitalUserId) {
		AuthenticationParameters authParameters = authenticationParametersRetrieveRepository.findAuthenticationParametersByDigitalUserDigitalUserIdentifier(digitalUserId);
		if(authParameters == null)
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.AUTHENTICATION_FAILURE_LOGIN_AUTH);
		return authParameters;
	}

	


	

}
