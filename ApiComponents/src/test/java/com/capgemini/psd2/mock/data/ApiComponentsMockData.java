package com.capgemini.psd2.mock.data;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.web.context.request.WebRequest;

import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AispConsent;

public class ApiComponentsMockData {

	public static AispConsent getAccountMapping() {
		
		AispConsent aispConsent=new AispConsent();
		List<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails acctDetail=new AccountDetails();
		acctDetail.setAccountId("123456");
		acctDetail.setAccountNSC("901542");
		acctDetail.setAccountNumber("12345678");
		acctDetail.setIdentifier("123456");
		accountDetailsList.add(acctDetail);
		aispConsent.setAccountDetails(accountDetailsList);
		return aispConsent;
	}
	
	public static WebRequest getWebRequest() {
		WebRequest request=new WebRequest() {

			@Override
			public Object getAttribute(String name, int scope) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void setAttribute(String name, Object value, int scope) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void removeAttribute(String name, int scope) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public String[] getAttributeNames(int scope) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void registerDestructionCallback(String name, Runnable callback, int scope) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public Object resolveReference(String key) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getSessionId() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Object getSessionMutex() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getHeader(String headerName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String[] getHeaderValues(String headerName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Iterator<String> getHeaderNames() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getParameter(String paramName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String[] getParameterValues(String paramName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Iterator<String> getParameterNames() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Map<String, String[]> getParameterMap() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Locale getLocale() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getContextPath() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getRemoteUser() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Principal getUserPrincipal() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean isUserInRole(String role) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean isSecure() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean checkNotModified(long lastModifiedTimestamp) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean checkNotModified(String etag) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean checkNotModified(String etag, long lastModifiedTimestamp) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public String getDescription(boolean includeClientInfo) {
				// TODO Auto-generated method stub
				return null;
			}
			
		};
		return request;
	}
	
}
