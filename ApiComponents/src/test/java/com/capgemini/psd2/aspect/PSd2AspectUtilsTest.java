package com.capgemini.psd2.aspect;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exceptions.OBPSD2Exception;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.response.filteration.ResponseFilter;
import com.capgemini.psd2.utilities.ResponseSigningUtility;

public class PSd2AspectUtilsTest {
	
	@Mock
	private HttpServletResponse response;

	@Mock
	private LoggerUtils loggerUtils;
	
	@Mock
	private ResponseSigningUtility responseBodySigning;

	@Mock
	private LoggerAttribute loggerAttribute;

	@Mock
	private DataMask dataMask;

	@Mock
	private ResponseFilter responseFilterUtility;
	
	@Mock
	private ProceedingJoinPoint proceedingJoinPoint;
	
	@Mock
	private JoinPoint joinPoint;
		
	@Mock
	private MethodSignature signature;
	
	
	@InjectMocks
	private PSD2AspectUtils psd2AspectUtils = new PSD2AspectUtils();

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		when(loggerUtils.populateLoggerData(anyString())).thenReturn(loggerAttribute);
		when(proceedingJoinPoint.getSignature()).thenReturn(signature);
		when(signature.getName()).thenReturn("retrieveAccountBalance");
		when(signature.getDeclaringTypeName()).thenReturn("retrieveAccountBalance");
		when(proceedingJoinPoint.getArgs()).thenReturn(new Object[1]);
		when(joinPoint.getSignature()).thenReturn(signature);
		when(signature.getDeclaringType()).thenReturn(String.class);
		
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}
	
	@Test
	public void testPSD2AspectUtils() {
		when(responseBodySigning.signResponseBody(anyObject())).thenReturn(null);
		psd2AspectUtils.methodPayloadAdvice(proceedingJoinPoint);
	}
	
	@Test
	public void testPSD2AspectUtils1() {
		ReflectionTestUtils.setField(psd2AspectUtils,"payloadLog",true);
		ReflectionTestUtils.setField(psd2AspectUtils,"maskPayloadLog",true);
		psd2AspectUtils.methodPayloadAdvice(proceedingJoinPoint);
	}
	
	@Test
	public void testPSD2AspectUtils2() {
		ReflectionTestUtils.setField(psd2AspectUtils,"payloadLog",true);
		ReflectionTestUtils.setField(psd2AspectUtils,"maskPayload",true);
		ReflectionTestUtils.setField(psd2AspectUtils,"maskPayloadLog",false);
		psd2AspectUtils.methodPayloadAdvice(proceedingJoinPoint);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testPSD2AspectUtilsException() throws Throwable {
		ReflectionTestUtils.setField(psd2AspectUtils,"payloadLog",true);
		ReflectionTestUtils.setField(psd2AspectUtils,"maskPayload",true);
		ReflectionTestUtils.setField(psd2AspectUtils,"maskPayloadLog",false);
		doThrow(Exception.class).when(proceedingJoinPoint).proceed();
		psd2AspectUtils.methodPayloadAdvice(proceedingJoinPoint);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testPSD2AspectUtilsException1() throws Throwable {
		ReflectionTestUtils.setField(psd2AspectUtils,"payloadLog",true);
		ReflectionTestUtils.setField(psd2AspectUtils,"maskPayload",true);
		ReflectionTestUtils.setField(psd2AspectUtils,"maskPayloadLog",false);
		doThrow(PSD2Exception.class).when(proceedingJoinPoint).proceed();
		psd2AspectUtils.methodPayloadAdvice(proceedingJoinPoint);
	}
	
	@Test
	public void testMethodAdvice() {	
		psd2AspectUtils.methodAdvice(proceedingJoinPoint);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testMethodAdviceException() throws Throwable {
		doThrow(Exception.class).when(proceedingJoinPoint).proceed();
		psd2AspectUtils.methodAdvice(proceedingJoinPoint);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testMethodAdvicePSD2Exception() throws Throwable {
		doThrow(PSD2Exception.class).when(proceedingJoinPoint).proceed();
		psd2AspectUtils.methodAdvice(proceedingJoinPoint);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testThrowExceptionOnJsonBinding() {
		psd2AspectUtils.throwExceptionOnJsonBinding(joinPoint,new Exception("error"));
	}
}
