package com.capgemini.psd2.exceptions;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.HttpClientErrorException;

import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.mock.data.ApiComponentsMockData;
import com.capgemini.psd2.pisp.domain.OBError1;
import com.capgemini.psd2.pisp.domain.OBErrorResponse1;
import com.capgemini.psd2.utilities.ResponseSigningUtility;

public class ApiEndExceptionHandlerTest {
	
	@Mock
	private RequestHeaderAttributes requestAttributes;

	@Mock
	private LoggerUtils loggerUtils;
	
	@Mock
	private HttpServletRequest request;
	
	@Mock
	private HttpServletResponse response;
	
	@Mock
	private ResponseSigningUtility signingUtility;
	
	@InjectMocks
	ApiEndExceptionHandler obj=new ApiEndExceptionHandler();
	
	/** The request header attributes. */
	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;
	
	@Before
	public void setUp() {
	   MockitoAnnotations.initMocks(this);
	   Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}
	
	@Test
	public void testHandleControllerAdviceException() {
		HttpClientErrorException exception=new HttpClientErrorException(HttpStatus.BAD_REQUEST);
		obj.handleControllerAdviceException(exception, ApiComponentsMockData.getWebRequest());
	}
	
	@Test
	public void testHandleControllerAdviceExceptionPSD2() {
		OBErrorResponse1 obErrorResponse1 = new OBErrorResponse1();
		OBError1 obError1 = new OBError1();
		obError1.setErrorCode(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT.getObErrorCode());
		List<OBError1> errors = new ArrayList<>();
		errors.add(obError1);
		obErrorResponse1.setErrors(errors);
		obErrorResponse1.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
		requestHeaderAttributes.setCorrelationId("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
		ReflectionTestUtils.setField(obj, "requestHeaderAttributes", requestHeaderAttributes);
		PSD2Exception psd2ExceptionResponse = new PSD2Exception("error", obErrorResponse1);
		obj.handleControllerAdviceException(psd2ExceptionResponse, ApiComponentsMockData.getWebRequest());
	}
	
	@Test
	public void testHandleControllerAdviceExceptionErrorInfo() {
		OBErrorResponse1 obErrorResponse1 = new OBErrorResponse1();
		OBError1 obError1 = new OBError1();
		requestHeaderAttributes.setCorrelationId("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
		ReflectionTestUtils.setField(obj, "requestHeaderAttributes", requestHeaderAttributes);
		obError1.setErrorCode(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT.getObErrorCode());
		List<OBError1> errors = new ArrayList<>();
		errors.add(obError1);
		obErrorResponse1.setErrors(errors);
		obErrorResponse1.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
		ErrorInfo errorInfo = new ErrorInfo();
		errorInfo.setObErrorCode(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT.getObErrorCode());
		errorInfo.setStatusCode("400");
		PSD2Exception psd2ExceptionResponse = new PSD2Exception("error", errorInfo);
		obj.handleControllerAdviceException(psd2ExceptionResponse, ApiComponentsMockData.getWebRequest());
	}
	
	@Test
	public void testHandleControllerAdviceExceptionPSD2InvalidHttpStatus() {
		OBErrorResponse1 obErrorResponse1 = new OBErrorResponse1();
		OBError1 obError1 = new OBError1();
		obError1.setErrorCode(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT.getObErrorCode());
		List<OBError1> errors = new ArrayList<>();
		errors.add(obError1);
		obErrorResponse1.setErrors(errors);
		obErrorResponse1.setHttpStatus(HttpStatus.BAD_REQUEST);
		PSD2Exception psd2ExceptionResponse = new PSD2Exception("error", obErrorResponse1);
		obj.handleControllerAdviceException(psd2ExceptionResponse, ApiComponentsMockData.getWebRequest());
	}
	
	@Test
	public void testHandleControllerAdviceExceptionPSD2Null() {
		PSD2Exception psd2Exception=new PSD2Exception(null, new ErrorInfo("400"));
		when(signingUtility.signResponseBody(anyObject())).thenReturn(null);
		obj.handleControllerAdviceException(psd2Exception, ApiComponentsMockData.getWebRequest());
	}
	
	@Test
	public void testHandleControllerAdviceExceptionNull() {
		obj.handleControllerAdviceException(null, ApiComponentsMockData.getWebRequest());
	}
	
	@Test
	public void testHandleControllerAdviceExceptionPSD2ErrorInfoNull() {
		ErrorInfo errorInfo = null;
		PSD2Exception exception=new PSD2Exception("error", errorInfo);
		when(signingUtility.signResponseBody(anyObject())).thenReturn(null);
		obj.handleControllerAdviceException(exception, ApiComponentsMockData.getWebRequest());
	}
	
	@Test
	public void testHandleControllerAdviceExceptionPSD2ErrorInfo() {
		PSD2Exception exception=new PSD2Exception("error",new ErrorInfo("400"));
		when(signingUtility.signResponseBody(anyObject())).thenReturn(null);
		obj.handleControllerAdviceException(exception, ApiComponentsMockData.getWebRequest());
	}
	
	@Test
	public void testhandleAndLogPsd2FilterException() throws IOException {
		OBErrorResponse1 obErrorResponse1 = new OBErrorResponse1();
		OBError1 obError1 = new OBError1();
		obError1.setErrorCode(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT.getObErrorCode());
		List<OBError1> errors = new ArrayList<>();
		errors.add(obError1);
		obErrorResponse1.setErrors(errors);
		obErrorResponse1.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
		requestHeaderAttributes.setCorrelationId("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
		ReflectionTestUtils.setField(obj, "requestHeaderAttributes", requestHeaderAttributes);
		PSD2Exception psd2Exception=new PSD2Exception("error", obErrorResponse1);
		when(request.getRequestURI()).thenReturn("/accounts");
		when(response.getWriter()).thenReturn(new PrintWriter("string"));
		obj.handleAndLogPsd2FilterException(psd2Exception, request, response);
	}
	
	@Test
	public void testHandleAndLogPsd2FilterException2() throws IOException {
		Exception exception=new Exception("error");
		ReflectionTestUtils.setField(obj, "sendExternalApiErrorPayload", false);
		ReflectionTestUtils.setField(obj, "sendExternalApiDetailErrorMessage", false);
		when(response.getWriter()).thenReturn(new PrintWriter("string"));
		obj.handleAndLogPsd2FilterException(exception, request, response);
	}
	
	@Test
	public void testHandleAndLogPsd2FilterException3() throws IOException {
		Exception exception=new Exception("error");
		ReflectionTestUtils.setField(obj, "sendExternalApiErrorPayload", true);
		ReflectionTestUtils.setField(obj, "sendExternalApiDetailErrorMessage", true);
		when(response.getWriter()).thenReturn(new PrintWriter("string"));
		obj.handleAndLogPsd2FilterException(exception, request, response);
	}
	
	@Test
	public void testHandleAndLogPsd2FilterException4() throws IOException {
		Exception exception=new Exception("error");
		ReflectionTestUtils.setField(obj, "internalEndPoint", "/accounts");
		when(request.getRequestURI()).thenReturn("/accounts");
		when(response.getWriter()).thenReturn(new PrintWriter("string"));
		obj.handleAndLogPsd2FilterException(exception, request, response);
	}
	
	
	@Test
	public void testHandleControllerAdviceException2() throws IOException {
		ErrorInfo errorInfo = new ErrorInfo();
		errorInfo.setObErrorCode(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT.getObErrorCode());
		errorInfo.setStatusCode("400");
		PSD2Exception psd2ExceptionResponse = new PSD2Exception("error", errorInfo);
		ReflectionTestUtils.setField(obj, "internalEndPoint", null);
		ReflectionTestUtils.setField(obj, "sendExternalApiErrorPayload", null);
		when(request.getRequestURI()).thenReturn(null);
		when(response.getWriter()).thenReturn(null);
		obj.handleControllerAdviceException(psd2ExceptionResponse, ApiComponentsMockData.getWebRequest());
	}
	
	@Test
	public void testHandleControllerAdviceException3() throws IOException {
		ErrorInfo errorInfo = new ErrorInfo();
		errorInfo.setObErrorCode(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT.getObErrorCode());
		errorInfo.setStatusCode("400");
		PSD2Exception psd2ExceptionResponse = new PSD2Exception("error", errorInfo);
		ReflectionTestUtils.setField(obj, "sendExternalApiErrorPayload", false);
		ReflectionTestUtils.setField(obj, "sendExternalApiDetailErrorMessage", false);
		when(response.getWriter()).thenReturn(new PrintWriter("string"));
		obj.handleControllerAdviceException(psd2ExceptionResponse, ApiComponentsMockData.getWebRequest());
	}
	
	@Test
	public void testHandleControllerAdviceException4() throws IOException {
		ErrorInfo errorInfo = new ErrorInfo();
		errorInfo.setObErrorCode(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT.getObErrorCode());
		errorInfo.setStatusCode("400");
		PSD2Exception psd2ExceptionResponse = new PSD2Exception("error", errorInfo);
		ReflectionTestUtils.setField(obj, "sendExternalApiErrorPayload", true);
		ReflectionTestUtils.setField(obj, "sendExternalApiDetailErrorMessage", true);
		when(response.getWriter()).thenReturn(new PrintWriter("string"));
		obj.handleControllerAdviceException(psd2ExceptionResponse, ApiComponentsMockData.getWebRequest());
	}
	
}
