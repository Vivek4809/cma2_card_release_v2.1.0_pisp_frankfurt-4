/*
 * Payment Process API
 * Payments Process API designed for the PISP Orchestration Layer as part of the PSD2 solution.
 *
 * OpenAPI spec version: v1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.capgemini.psd2.domestic.scheduled.payments.consents.mock.foundationservice.domain;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * The financial value of the transaction
 */
@ApiModel(description = "The financial value of the transaction")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-01-16T12:10:46.138+05:30")
public class DomesticpaymentinstructionproposalsFinancialEventAmount {
  @SerializedName("localReportingCurrency")
  private BigDecimal localReportingCurrency = null;

  @SerializedName("transactionCurrency")
  private BigDecimal transactionCurrency = null;

  @SerializedName("groupReportingCurrency")
  private BigDecimal groupReportingCurrency = null;

  public DomesticpaymentinstructionproposalsFinancialEventAmount localReportingCurrency(BigDecimal localReportingCurrency) {
    this.localReportingCurrency = localReportingCurrency;
    return this;
  }

   /**
   * The value in Local reporting currency
   * maximum: 1.0E+15
   * @return localReportingCurrency
  **/
  @ApiModelProperty(value = "The value in Local reporting currency")
  public BigDecimal getLocalReportingCurrency() {
    return localReportingCurrency;
  }

  public void setLocalReportingCurrency(BigDecimal localReportingCurrency) {
    this.localReportingCurrency = localReportingCurrency;
  }

  public DomesticpaymentinstructionproposalsFinancialEventAmount transactionCurrency(BigDecimal transactionCurrency) {
    this.transactionCurrency = transactionCurrency;
    return this;
  }

   /**
   * The actual value in the transaction currency
   * maximum: 1.0E+15
   * @return transactionCurrency
  **/
  @ApiModelProperty(value = "The actual value in the transaction currency")
  public BigDecimal getTransactionCurrency() {
    return transactionCurrency;
  }

  public void setTransactionCurrency(BigDecimal transactionCurrency) {
    this.transactionCurrency = transactionCurrency;
  }

  public DomesticpaymentinstructionproposalsFinancialEventAmount groupReportingCurrency(BigDecimal groupReportingCurrency) {
    this.groupReportingCurrency = groupReportingCurrency;
    return this;
  }

   /**
   * The value in group reporting currency
   * maximum: 1.0E+15
   * @return groupReportingCurrency
  **/
  @ApiModelProperty(value = "The value in group reporting currency")
  public BigDecimal getGroupReportingCurrency() {
    return groupReportingCurrency;
  }

  public void setGroupReportingCurrency(BigDecimal groupReportingCurrency) {
    this.groupReportingCurrency = groupReportingCurrency;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DomesticpaymentinstructionproposalsFinancialEventAmount domesticpaymentinstructionproposalsFinancialEventAmount = (DomesticpaymentinstructionproposalsFinancialEventAmount) o;
    return Objects.equals(this.localReportingCurrency, domesticpaymentinstructionproposalsFinancialEventAmount.localReportingCurrency) &&
        Objects.equals(this.transactionCurrency, domesticpaymentinstructionproposalsFinancialEventAmount.transactionCurrency) &&
        Objects.equals(this.groupReportingCurrency, domesticpaymentinstructionproposalsFinancialEventAmount.groupReportingCurrency);
  }

  @Override
  public int hashCode() {
    return Objects.hash(localReportingCurrency, transactionCurrency, groupReportingCurrency);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DomesticpaymentinstructionproposalsFinancialEventAmount {\n");
    
    sb.append("    localReportingCurrency: ").append(toIndentedString(localReportingCurrency)).append("\n");
    sb.append("    transactionCurrency: ").append(toIndentedString(transactionCurrency)).append("\n");
    sb.append("    groupReportingCurrency: ").append(toIndentedString(groupReportingCurrency)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

