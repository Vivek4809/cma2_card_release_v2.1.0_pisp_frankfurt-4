package com.capgemini.psd2.service.qtsp;

import com.capgemini.psd2.model.qtsp.QTSPResource;

public interface UploadQTSPService {

	public boolean validateRequestBody(QTSPResource qtsp);

	public boolean uploadQTSP(QTSPResource qtsp);
}