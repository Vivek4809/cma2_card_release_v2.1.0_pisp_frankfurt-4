package com.capgemini.psd2.model.qtsp;

public class PFResponse {

	private String id;

	private String serialNumber;

	private String subjectDN;

	private String issuerDN;

	private String validFrom;

	private String expires;

	private String keyAlgorithm;

	private String keySize;

	private String signatureAlgorithm;

	private String version;

	private String md5Fingerprint;

	private String sha1Fingerprint;

	private String sha256Fingerprint;

	private String status;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getSubjectDN() {
		return subjectDN;
	}

	public void setSubjectDN(String subjectDN) {
		this.subjectDN = subjectDN;
	}

	public String getIssuerDN() {
		return issuerDN;
	}

	public void setIssuerDN(String issuerDN) {
		this.issuerDN = issuerDN;
	}

	public String getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}

	public String getExpires() {
		return expires;
	}

	public void setExpires(String expires) {
		this.expires = expires;
	}

	public String getKeyAlgorithm() {
		return keyAlgorithm;
	}

	public void setKeyAlgorithm(String keyAlgorithm) {
		this.keyAlgorithm = keyAlgorithm;
	}

	public String getKeySize() {
		return keySize;
	}

	public void setKeySize(String keySize) {
		this.keySize = keySize;
	}

	public String getSignatureAlgorithm() {
		return signatureAlgorithm;
	}

	public void setSignatureAlgorithm(String signatureAlgorithm) {
		this.signatureAlgorithm = signatureAlgorithm;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getMd5Fingerprint() {
		return md5Fingerprint;
	}

	public void setMd5Fingerprint(String md5Fingerprint) {
		this.md5Fingerprint = md5Fingerprint;
	}

	public String getSha1Fingerprint() {
		return sha1Fingerprint;
	}

	public void setSha1Fingerprint(String sha1Fingerprint) {
		this.sha1Fingerprint = sha1Fingerprint;
	}

	public String getSha256Fingerprint() {
		return sha256Fingerprint;
	}

	public void setSha256Fingerprint(String sha256Fingerprint) {
		this.sha256Fingerprint = sha256Fingerprint;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((sha256Fingerprint == null) ? 0 : sha256Fingerprint.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof PFResponse))
			return false;
		PFResponse other = (PFResponse) obj;
		if (sha256Fingerprint == null) {
			if (other.sha256Fingerprint != null)
				return false;
		} else if (!sha256Fingerprint.equalsIgnoreCase(other.sha256Fingerprint))
			return false;
		return true;
	}
}