package com.capgemini.psd2.model.qtsp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QTSPServiceExtension {

	@JsonProperty("Critical")
	private boolean critical;

	@JsonProperty("AdditionalInfoLang")
	private String additionalInfoLang;

	@JsonProperty("AdditionalInfoURI")
	private String additionalInfoURI;

	public boolean isCritical() {
		return critical;
	}

	public void setCritical(boolean critical) {
		this.critical = critical;
	}

	public String getAdditionalInfoLang() {
		return additionalInfoLang;
	}

	public void setAdditionalInfoLang(String additionalInfoLang) {
		this.additionalInfoLang = additionalInfoLang;
	}

	public String getAdditionalInfoURI() {
		return additionalInfoURI;
	}

	public void setAdditionalInfoURI(String additionalInfoURI) {
		this.additionalInfoURI = additionalInfoURI;
	}
}