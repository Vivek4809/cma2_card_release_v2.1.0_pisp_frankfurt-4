/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.party.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.party.service.AccountPartyService;
import com.capgemini.psd2.aisp.account.mapping.adapter.AccountMappingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountPartyAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBReadParty1;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountPartyResponse;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

/**
 * The Class AccountPartyServiceImpl.
 */
@Service
public class AccountPartyServiceImpl implements AccountPartyService {

	/** The adapter. */
	@Autowired
	@Qualifier("accountPartyRoutingAdapter")
	private AccountPartyAdapter adapter;

	/** The req header atrributes. */
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	/** The account mapping adapter. */
	@Autowired
	private AccountMappingAdapter accountMappingAdapter;

	/** The consent mapping adapter. */
	@Autowired
	private AispConsentAdapter aispConsentAdapter;
	
	@SuppressWarnings("rawtypes")
	@Autowired
	@Qualifier("accountsRoutingValidator")
	private AISPCustomValidator accountsCustomValidator;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.account.balance.service.AccountBalanceService#
	 * retrieveAccountBalance(java.lang.String)
	 */
	@Override
	public OBReadParty1 retrieveAccountParty(String accountId) {
		
		accountsCustomValidator.validateUniqueId(accountId);
		
		// Get consents by accountId and validate.
		AispConsent aispConsent = aispConsentAdapter.validateAndRetrieveConsentByAccountId(accountId,
				reqHeaderAtrributes.getToken().getConsentTokenData().getConsentId());
		
		// Get account mapping.
		AccountMapping accountMapping = accountMappingAdapter.retrieveAccountMappingDetails(accountId, aispConsent);

		Map<String,String> params=reqHeaderAtrributes.getToken().getSeviceParams();
		params.put(PSD2Constants.CMAVERSION, aispConsent.getCmaVersion());
	
		// Retrieve account balance.
		PlatformAccountPartyResponse platformPartyResponse = adapter
				.retrieveAccountParty(accountMapping, params);

		// Get CMA2 product response.
		OBReadParty1 tppPartyResponse = platformPartyResponse.getobReadParty1();

		// update platform response.
		updatePlatformResponse(tppPartyResponse);

		// Apply platform and CMA2-swagger validations.
		accountsCustomValidator.validateResponseParams(tppPartyResponse);

		return tppPartyResponse;
	}

	private void updatePlatformResponse(OBReadParty1 tppPartyResponse) {
		if (tppPartyResponse.getLinks() == null)
			tppPartyResponse.setLinks(new Links());
		tppPartyResponse.getLinks().setSelf(reqHeaderAtrributes.getSelfUrl());

		if (tppPartyResponse.getMeta() == null)
			tppPartyResponse.setMeta(new Meta());
		tppPartyResponse.getMeta().setTotalPages(1);
	}

}
