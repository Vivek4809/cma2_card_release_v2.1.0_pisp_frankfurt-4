package com.capgemini.psd2.adapter.datetime.utility;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;

@Component
public class TimeZoneDateTimeAdapter {

	private static final String DATE_TIME_FORMAT_1 = "yyyy-MM-dd HH:mm:ss";

	private static final String DATE_TIME_FORMAT_2 = "yyyy-MM-dd'T'HH:mm:ss";
	
	private static final String DATE_TIME_FORMAT_3 = "yyyy-MM-dd";
	
	private static final String DATE_TIME_FORMAT_4 = "yyyy-MM-dd'T'HH:mm:ssxxx";

	@Value("${foundationService.timeZone}")
	private String timeZone;

	@Value("${foundationService.timeZoneAPI:UTC}")
	private String timeZoneAPI;

	//InputFrom-Adapter, OutputTo-FS, InputType-Date, OutputType-Date, Note - Input only new date
	public Date parseNewDateTimeFS(Date newDate) {
		SimpleDateFormat format = new SimpleDateFormat(DATE_TIME_FORMAT_1);
		format.setTimeZone(TimeZone.getTimeZone(timeZone));
		String date = format.format(newDate);
		Date formatedDate = null;
		try {
			formatedDate = new SimpleDateFormat(DATE_TIME_FORMAT_1).parse(date);
		} catch (ParseException e) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.TECHNICAL_ERROR);
		}
		return formatedDate;
	}
	
	//InputFrom-API, OutputTo-FS, InputType-String, OutputType-Date
	public Date parseDateFS(String apiDateTime) {
		Date formatedDate = null;
		try {
			formatedDate = new SimpleDateFormat(DATE_TIME_FORMAT_3).parse(apiDateTime);
		} catch (ParseException e) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.TECHNICAL_ERROR);
		}
		return formatedDate;
	}
			
	//InputFrom-API, OutputTo-FS, InputType-String, OutputType-DateTime 
	public Date parseDateTimeFS(String apiDateTime) {
		Date formatedDate = null;
		try {
			formatedDate = new SimpleDateFormat(DATE_TIME_FORMAT_2).parse(apiDateTime);
		} catch (ParseException e) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.TECHNICAL_ERROR);
		}
		return formatedDate;
	}
	
	//InputFrom-FS, OutputTo-API, InputType-Date, OutputType-String
	//OutputFormat = yyyy-MM-dd'T'HH:mm:ss+HH:mm
	public String parseDateTimeCMA(Date fsDateTime) {
		TimeZone fromTimeZone = TimeZone.getTimeZone(timeZone);
		String timezoneOffset = getTimezoneOffset(fsDateTime, fromTimeZone);
		DateFormat df = new SimpleDateFormat(DATE_TIME_FORMAT_2);
		String dateString = df.format(fsDateTime);
		return dateString + timezoneOffset;
	}
	
	
	//InputFrom-FS, OutputTo-API, InputType-Date, OutputType-String
	//OutputFormat = yyyy-MM-dd'T'00:00:00+HH:mm
	public String parseDateCMA(Date fsDateTime){
		Calendar cal = Calendar.getInstance();  
        cal.setTime(fsDateTime);  
        cal.set(Calendar.HOUR_OF_DAY, 0);  
        cal.set(Calendar.MINUTE, 0);  
        cal.set(Calendar.SECOND, 0);  
        cal.set(Calendar.MILLISECOND, 0);
		DateFormat df = new SimpleDateFormat(DATE_TIME_FORMAT_2);
		TimeZone fromTimeZone = TimeZone.getTimeZone(timeZone);
		String timezoneOffset = getTimezoneOffset(fsDateTime, fromTimeZone);
		String dateString = df.format(cal.getTime());
		return dateString + timezoneOffset;
	}
	
	
	//InputFrom-FS, OutputTo-API, InputType-XMLGregorianCalendar, OutputType-String, Note - Input only date
	//OutputFormat = yyyy-MM-dd'T'HH:mm:ss+HH:mm
	public String parseDateCMA(XMLGregorianCalendar fsDate) {
		TimeZone fromTimeZone = TimeZone.getTimeZone(timeZone);
		fsDate.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
		GregorianCalendar gCalendar = fsDate.toGregorianCalendar();
		String timezoneOffset = getTimezoneOffset(gCalendar, fromTimeZone);
		DateFormat df = new SimpleDateFormat(DATE_TIME_FORMAT_2);
		Date date = gCalendar.getTime();
		String dateString = df.format(date);
		return dateString + timezoneOffset;
	}

	//InputFrom-FS, OutputTo-API, InputType-XMLGregorianCalendar, OutputType-String
	//OutputFormat = yyyy-MM-dd'T'HH:mm:ss+HH:mm
	public String parseDateTimeCMA(XMLGregorianCalendar fsDateTime) {
		TimeZone fromTimeZone = TimeZone.getTimeZone(timeZone);
		fsDateTime.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
		GregorianCalendar gCalendar = fsDateTime.toGregorianCalendar();
		Date date = gCalendar.getTime();
		String timezoneOffset = getTimezoneOffset(gCalendar, fromTimeZone);
		DateFormat df = new SimpleDateFormat(DATE_TIME_FORMAT_2);
		String dateString = df.format(date);
		return dateString + timezoneOffset;
	}


	private String getTimezoneOffset(GregorianCalendar gCalendar, TimeZone fromTimeZone) {
		int offsetInMillis = fromTimeZone.getOffset(gCalendar.getTimeInMillis());
		String offset = String.format("%02d:%02d", Math.abs(offsetInMillis / 3600000), Math.abs((offsetInMillis / 60000) % 60));
		offset = (offsetInMillis >= 0 ? "+" : "-") + offset;
		return offset;
	}

	private String getTimezoneOffset(Date date, TimeZone fromTimeZone) {
		int offsetInMillis = fromTimeZone.getOffset(date.getTime());
		String offset = String.format("%02d:%02d", Math.abs(offsetInMillis / 3600000), Math.abs((offsetInMillis / 60000) % 60));
		offset = (offsetInMillis >= 0 ? "+" : "-") + offset;
		return offset;
	}
	
	public String parseOffsetDateTimeToCMA(OffsetDateTime offsetDateTime){
		String offsetdatetime = offsetDateTime.format(DateTimeFormatter.ofPattern(DATE_TIME_FORMAT_4));
		return offsetdatetime;
	}
}
