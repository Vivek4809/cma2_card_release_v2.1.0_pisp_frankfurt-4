
package com.capgemini.psd2.adapter.exceptions.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "validationViolations"
})
public class ValidationWrapper {

    @JsonProperty("validationViolations")
    private ValidationViolations validationViolations;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ValidationWrapper() {
    }

    /**
     * 
     * @param validationViolations
     */
    public ValidationWrapper(ValidationViolations validationViolations) {
        super();
        this.validationViolations = validationViolations;
    }

    @JsonProperty("validationViolations")
    public ValidationViolations getValidationViolations() {
        return validationViolations;
    }

    @JsonProperty("validationViolations")
    public void setValidationViolations(ValidationViolations validationViolations) {
        this.validationViolations = validationViolations;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("validationViolations", validationViolations).toString();
    }

}
