package com.capgemini.psd2.foundationservice.account.statements.boi.adapter.constants;

public class AccountStatementsFoundationServiceConstants {
	
	public static final String ACCOUNT_ID = "accountId";

	public static final String ACCOUNT_CLASSIFIER = "accountClassifier";
	
	public static final String ACCOUNT_TYPE = "accountType";
	
	public static final String ACCOUNT_NSC = "accountNSC";
	
	public static final String ACCOUNT_NUMBER = "accountNumber";
	
	public static final String CREDIT_CARD = "CreditCard";
	
	public static final String STATEMENT_ID = "statementId";
	
	public static final String CHANNEL_ID = "channelId";
	
	public static final String SOURCE_SYSTEM_PSD2API = "PSD2API";
	
	public static final String ANNUAL_SUMMARY_STMT="Annual Summary Statement";
	
	public static final String REGULAR_MON_STMT="Regular Monthly Statement";
	
	public static final String COMM_SUMMARY_STMT="Commercial Summary Statement";
	
	public static final String COMM_MONTHLY_STMT="Commercial Monthly Statement";
	
	public static final String NO_STMT="No Statement Found";
	
	public static final String ACCOUNT_SUBTYPE = "account_subtype";
	
	public static final String CURRENT_ACCOUNT = "CurrentAccount";
	
	public static final String SAVINGS = "Savings";
	
	public static final String REQUESTED_FROM_DATETIME = "fromStatementDateTime";

	public static final String REQUESTED_TO_DATETIME = "toStatementDateTime";
	
    public static final String REQUESTED_FROM_CONSENT_DATETIME = "RequestedFromConsentDateTime";
	
	public static final String REQUESTED_TO_CONSENT_DATETIME = "RequestedToConsentDateTime";
	
	public static final String DATEFROM = "dateFrom";
	
	public static final String DATETO = "dateTo";
	
	public static final String CONSENT_EXPIRATION_DATETIME = "ConsentExpirationDateTime"; 
	
	public static final String FROM_DATE = "fromStatementDateTime";
	
	public static final String TO_DATE = "toStatementDateTime";
	
	
}
