package com.capgemini.psd2.foundationservice.account.statements.boi.adapter.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.constants.AccountStatementsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.raml.domain.StatementObject;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.raml.domain.StatementReasonDescription;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.raml.domain.Statementsresponse;
//import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.domain.StatementObject;
//import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.domain.StatementReasonDescription;
//import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.domain.StatementsResponse;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.transformer.AccountStatementsFoundationServiceTransformer;
import com.capgemini.psd2.validator.impl.PSD2ValidatorImpl;



@RunWith(SpringJUnit4ClassRunner.class)
public class AccountStatementsFoundationServiceTransformerTest {

	
	/** The psd 2 validator. */
	@Mock
	private PSD2ValidatorImpl psd2Validator;

	/** The account balance FS transformer. */
	@InjectMocks
	private AccountStatementsFoundationServiceTransformer accountStatementsFileFSTransformer;
	
	@Mock
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	/**
	 * Test account statements file FS.
	 */

	@Test
	public void transformAccountStatementRes()
	{
		
		Statementsresponse stmt1 = new Statementsresponse();
		StatementObject stmt = new StatementObject();
		List<StatementObject> statementsList = new ArrayList<StatementObject>();
		stmt.setDocumentIdentificationNumber("abcd");
		stmt.setDocumentIssuanceDate(new Date(2018-02-01));
		stmt.setStatementReasonDescription(StatementReasonDescription.ACCOUNT_STATEMENT);
		statementsList.add(stmt);
		statementsList.add(stmt);
		stmt1.setStatementsList(statementsList);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("accountId", "1234");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE,"CreditCard");
		accountStatementsFileFSTransformer.transformAccountStatementRes(stmt1, params);
	}
	
	@Test
	public void transformAccountStatementRes2()
	{
		
		Statementsresponse statements = new Statementsresponse();
		StatementObject stmt = new StatementObject();
		List<StatementObject> statementsList = new ArrayList<StatementObject>();
		stmt.setDocumentIssuanceDate(new Date(2018-02-01));
		statementsList.add(stmt);
		stmt.setDocumentIdentificationNumber("abcd");
		stmt.setStatementReasonDescription(StatementReasonDescription.COMMERCIAL_MONTHLY_STATEMENT);
		statements.setStatementsList(statementsList);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("accountId", "1234");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE,"CreditCard");
		accountStatementsFileFSTransformer.transformAccountStatementRes(statements, params);
	}
	
	
	@Test
	public void transformAccountStatementRes3()
	{
		
		Statementsresponse statements = new Statementsresponse();
		StatementObject stmt = new StatementObject();
		List<StatementObject> statementsList = new ArrayList<StatementObject>();
		stmt.setDocumentIssuanceDate(new Date(2018-02-01));
		stmt.setDocumentIdentificationNumber("abcd");
		stmt.setStatementReasonDescription(StatementReasonDescription.ANNUAL_SUMMARY_STATEMENT);
		statementsList.add(stmt);
		statements.setStatementsList(statementsList);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("accountId", "1234");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE,"CreditCard");
		accountStatementsFileFSTransformer.transformAccountStatementRes(statements, params);
	}
	
	
	@Test
	public void transformAccountStatementRes4()
	{
		
		Statementsresponse statements = new Statementsresponse();
		StatementObject stmt = new StatementObject();
		List<StatementObject> statementsList = new ArrayList<StatementObject>();
		stmt.setDocumentIssuanceDate(new Date(2018-02-01));
		statementsList.add(stmt);
		stmt.setDocumentIdentificationNumber("abcd");
		stmt.setStatementReasonDescription(StatementReasonDescription.REGULAR_MONTHLY_STATEMENT);
		statements.setStatementsList(statementsList);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("accountId", "1234");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE,"CreditCard");
		accountStatementsFileFSTransformer.transformAccountStatementRes(statements, params);
	}
	
	@Test
	public void transformAccountStatementRes5()
	{
		
		Statementsresponse statements = new Statementsresponse();
		StatementObject stmt = new StatementObject();
		List<StatementObject> statementsList = new ArrayList<StatementObject>();
		stmt.setDocumentIssuanceDate(new Date(2018-02-01));
		stmt.setDocumentIdentificationNumber("abcd");
		stmt.setStatementReasonDescription(StatementReasonDescription.COMMERCIAL_SUMMARY_STATEMENT);
		statementsList.add(stmt);
		statements.setStatementsList(statementsList);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("accountId", "1234");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE,"CreditCard");
		accountStatementsFileFSTransformer.transformAccountStatementRes(statements, params);
	}
	
	@Test
	public void transformAccountStatementRes6()
	{
		
		Statementsresponse statements = new Statementsresponse();
		StatementObject stmt = new StatementObject();
		List<StatementObject> statementsList = new ArrayList<StatementObject>();
		stmt.setDocumentIssuanceDate(new Date(2018-02-01));
		statementsList.add(stmt);
		stmt.setDocumentIdentificationNumber("abcd");
		stmt.setStatementReasonDescription(StatementReasonDescription.FEE_STATEMENT);
		statements.setStatementsList(statementsList);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("accountId", "1234");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE,"CreditCard");
		accountStatementsFileFSTransformer.transformAccountStatementRes(statements, params);
	}
	
	@Test
	public void transformAccountStatementRes7()
	{
		
		Statementsresponse stmt1 = new Statementsresponse();
		StatementObject stmt = new StatementObject();
		List<StatementObject> statementsList = new ArrayList<StatementObject>();
		stmt.setDocumentIdentificationNumber("abcd");
		stmt.setDocumentIssuanceDate(new Date(2018-02-01));
		stmt.setStatementReasonDescription(StatementReasonDescription.ACCOUNT_STATEMENT);
		statementsList.add(stmt);
		statementsList.add(stmt);
		stmt1.setStatementsList(statementsList);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("accountId", "1234");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE,"CurrentAccount");
		accountStatementsFileFSTransformer.transformAccountStatementRes(stmt1, params);
	}
	
	@Test
	public void transformAccountStatementRes8()
	{
		
		Statementsresponse statements = new Statementsresponse();
		StatementObject stmt = new StatementObject();
		List<StatementObject> statementsList = new ArrayList<StatementObject>();
		stmt.setDocumentIssuanceDate(new Date(2018-02-01));
		statementsList.add(stmt);
		stmt.setDocumentIdentificationNumber("abcd");
		stmt.setStatementReasonDescription(StatementReasonDescription.COMMERCIAL_MONTHLY_STATEMENT);
		statements.setStatementsList(statementsList);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("accountId", "1234");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE,"CurrentAccount");
		accountStatementsFileFSTransformer.transformAccountStatementRes(statements, params);
	}
	
	
	@Test
	public void transformAccountStatementRes9()
	{
		
		Statementsresponse statements = new Statementsresponse();
		StatementObject stmt = new StatementObject();
		List<StatementObject> statementsList = new ArrayList<StatementObject>();
		stmt.setDocumentIssuanceDate(new Date(2018-02-01));
		stmt.setDocumentIdentificationNumber("abcd");
		stmt.setStatementReasonDescription(StatementReasonDescription.ANNUAL_SUMMARY_STATEMENT);
		statementsList.add(stmt);
		statements.setStatementsList(statementsList);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("accountId", "1234");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE,"CurrentAccount");
		accountStatementsFileFSTransformer.transformAccountStatementRes(statements, params);
	}
	
	
	@Test
	public void transformAccountStatementRes10()
	{
		
		Statementsresponse statements = new Statementsresponse();
		StatementObject stmt = new StatementObject();
		List<StatementObject> statementsList = new ArrayList<StatementObject>();
		stmt.setDocumentIssuanceDate(new Date(2018-02-01));
		statementsList.add(stmt);
		stmt.setDocumentIdentificationNumber("abcd");
		stmt.setStatementReasonDescription(StatementReasonDescription.REGULAR_MONTHLY_STATEMENT);
		statements.setStatementsList(statementsList);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("accountId", "1234");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE,"CurrentAccount");
		accountStatementsFileFSTransformer.transformAccountStatementRes(statements, params);
	}
	
	@Test
	public void transformAccountStatementRes11()
	{
		
		Statementsresponse statements = new Statementsresponse();
		StatementObject stmt = new StatementObject();
		List<StatementObject> statementsList = new ArrayList<StatementObject>();
		stmt.setDocumentIssuanceDate(new Date(2018-02-01));
		stmt.setDocumentIdentificationNumber("abcd");
		stmt.setStatementReasonDescription(StatementReasonDescription.COMMERCIAL_SUMMARY_STATEMENT);
		statementsList.add(stmt);
		statements.setStatementsList(statementsList);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("accountId", "1234");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE,"CurrentAccount");
		accountStatementsFileFSTransformer.transformAccountStatementRes(statements, params);
	}
	
	@Test
	public void transformAccountStatementRes12()
	{
		
		Statementsresponse statements = new Statementsresponse();
		StatementObject stmt = new StatementObject();
		List<StatementObject> statementsList = new ArrayList<StatementObject>();
		stmt.setDocumentIssuanceDate(new Date(2018-02-01));
		statementsList.add(stmt);
		stmt.setDocumentIdentificationNumber("abcd");
		stmt.setStatementReasonDescription(StatementReasonDescription.FEE_STATEMENT);
		statements.setStatementsList(statementsList);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("accountId", "1234");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE,"CurrentAccount");
		accountStatementsFileFSTransformer.transformAccountStatementRes(statements, params);
	}
	
	
}
