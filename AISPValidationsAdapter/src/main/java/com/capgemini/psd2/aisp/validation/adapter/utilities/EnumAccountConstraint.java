package com.capgemini.psd2.aisp.validation.adapter.utilities;

public enum EnumAccountConstraint {	
	IBAN("2","IBAN"),
	SORTCODEACCOUNTNUMBER("3","SORTCODEACCOUNTNUMBER"),
	ADDRESSLINE_LENGTH(70),
	COUNTRYSUBDIVISION_LENGTH(35),
	IDEMPOTENCY_KEY_LENGTH(40),
	ADDITIONAL_PARAMETERS("ADDITIONAL_PARAMETERS"),
	PAYMENT_SETUP_VALIDATION_STATUS("PAYMENT_SETUP_VALIDATION_STATUS"),
	SECONDARYIDENTIFCATION_LENGTH(34),
	REFERENCE_LENGTH(18);
	

	private String value;
	private String code;
	private int size;
	private String status; 
	private EnumAccountConstraint(String code,String value){
		this.code = code;
		this.value = value;
	}
	private EnumAccountConstraint(int value){
		this.size = value;
	}
	
	private EnumAccountConstraint(String value){
		this.value = value;
	}
	
	public int getSize() {
		return size;
	}
		
	public String getStatus() {
		return status;
	}
	public String getValue() {
		return value;
	}
	
	public String getCode() {
		return code;
	}
		
}
