package com.capgemini.psd2.aisp.validation.test.adapter.utilities;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.validation.adapter.utilities.EnumAccountConstraint;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class EnumAccountConstraintTest {
	
	@Before
	public void setUp() {
		Map<String,String> genericErrorMessages=new HashMap<>();
		Map<String,String> specificErrorMessages=new HashMap<>();
		genericErrorMessages.put("INTERNAL", "INTERNAL Error");
		specificErrorMessages.put("Idempotency", "Idempotency duration is not configured in correct format.");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(genericErrorMessages);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessages);
	}
	
	@Test
	public void testEnumSetter(){
		EnumAccountConstraint.SORTCODEACCOUNTNUMBER.getCode();
		EnumAccountConstraint.SORTCODEACCOUNTNUMBER.getStatus();
		EnumAccountConstraint.ADDRESSLINE_LENGTH.getSize();
		EnumAccountConstraint.ADDITIONAL_PARAMETERS.getValue();
	}
}
