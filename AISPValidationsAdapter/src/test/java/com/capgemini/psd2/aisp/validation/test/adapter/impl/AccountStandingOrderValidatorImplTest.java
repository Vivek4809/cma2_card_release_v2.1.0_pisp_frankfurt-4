package com.capgemini.psd2.aisp.validation.test.adapter.impl;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.domain.OBExternalPermissions1Code;
import com.capgemini.psd2.aisp.validation.adapter.impl.AccountStandingOrdersValidatorImpl;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.aisp.validation.test.mock.data.AccountStandingOrderValidatorMockData;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountStandingOrderValidatorImplTest {
	
	@InjectMocks
	AccountStandingOrdersValidatorImpl accountStandingOrderValidatorImpl;
	
	@Mock
	CommonAccountValidations commonAccountValidation;
	
	@Mock
	private PSD2Validator psd2Validator;
	
	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	@Before
	public void setUp() {
		Map<String,String> genericErrorMessages=new HashMap<>();
		Map<String,String> specificErrorMessages=new HashMap<>();
		genericErrorMessages.put("INTERNAL", "INTERNAL Error");
		specificErrorMessages.put("Idempotency", "Idempotency duration is not configured in correct format.");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(genericErrorMessages);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessages);
	}
	
	@Test
	public void validateRequestParamsTest(){
		assertTrue(accountStandingOrderValidatorImpl.validateRequestParams(AccountStandingOrderValidatorMockData.getAccountStandingOrderGETResponse())
															.getData().getStandingOrder().get(0).getStandingOrderId().equals("TestStandingOrderId"));
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateResponseParamsTestTrue() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountStandingOrdersValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountStandingOrderValidatorImpl, true);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		Set<Object>claims=new HashSet<>();
		claims.add(OBExternalPermissions1Code.READSTANDINGORDERSDETAIL);
		Mockito.when(reqHeaderAtrributes.getClaims()).thenReturn(claims); 
		assertTrue(accountStandingOrderValidatorImpl.validateResponseParams(AccountStandingOrderValidatorMockData.getAccountStandingOrderCreditorAccountGETResponse()));
	}
	
	@Test
	public void validateResponseParamsClaimsNull() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountStandingOrdersValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountStandingOrderValidatorImpl, false);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		Mockito.when(reqHeaderAtrributes.getClaims()).thenReturn(null);
		assertTrue(accountStandingOrderValidatorImpl.validateResponseParams(AccountStandingOrderValidatorMockData.getAccountStandingOrderCreditorAccountGETResponse()));
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateResponseParamsCreditorAccountNull() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountStandingOrdersValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountStandingOrderValidatorImpl, false);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		Set<Object>claims=new HashSet<>();
		claims.add(OBExternalPermissions1Code.READSTANDINGORDERSDETAIL);
		Mockito.when(reqHeaderAtrributes.getClaims()).thenReturn(claims);
		assertTrue(accountStandingOrderValidatorImpl.validateResponseParams(AccountStandingOrderValidatorMockData.getAccountStandingOrderCreditorAccountGETResponse()));
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateResponseParamsNextPaymentAmountNull() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountStandingOrdersValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountStandingOrderValidatorImpl, false);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		Set<Object>claims=new HashSet<>();
		claims.add(OBExternalPermissions1Code.READSTANDINGORDERSDETAIL);
		Mockito.when(reqHeaderAtrributes.getClaims()).thenReturn(claims);
		assertTrue(accountStandingOrderValidatorImpl.validateResponseParams(AccountStandingOrderValidatorMockData.getAccountStandingOrderNextPaymentAmountGETResponse()));
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateResponseParamsNextPaymentAmountBlank() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountStandingOrdersValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountStandingOrderValidatorImpl, false);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		Set<Object>claims=new HashSet<>();
		claims.add(OBExternalPermissions1Code.READSTANDINGORDERSDETAIL);
		Mockito.when(reqHeaderAtrributes.getClaims()).thenReturn(claims);
		assertTrue(accountStandingOrderValidatorImpl.validateResponseParams(AccountStandingOrderValidatorMockData.getAccountStandingOrderNextPaymentAmountBlankGETResponse()));
	}
	
	@Test
	public void validateResponseParamsStandingOrderNull() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountStandingOrdersValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountStandingOrderValidatorImpl, false);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		Set<Object>claims=new HashSet<>();
		claims.add(OBExternalPermissions1Code.READSTANDINGORDERSDETAIL);
		Mockito.when(reqHeaderAtrributes.getClaims()).thenReturn(claims);
		assertTrue(accountStandingOrderValidatorImpl.validateResponseParams(AccountStandingOrderValidatorMockData.getAccountStandingOrderGETResponseNullData()));
	}
	
	@Test
	public void validateResponseParamsTestFalse() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountStandingOrdersValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountStandingOrderValidatorImpl, false);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		assertTrue(accountStandingOrderValidatorImpl.validateResponseParams(AccountStandingOrderValidatorMockData.getAccountStandingOrderGETResponse()));
	}
	
	@Test
	public void validateResponseParamsTestNull() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountStandingOrdersValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountStandingOrderValidatorImpl, true);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		assertTrue(accountStandingOrderValidatorImpl.validateResponseParams(null));
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateUniqueIdNull(){
		accountStandingOrderValidatorImpl.validateUniqueId(null);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateUniqueIdEmpty(){
		accountStandingOrderValidatorImpl.validateUniqueId("");
	}

	@Test
	public void testValidateUniqueIdNotNull(){
		accountStandingOrderValidatorImpl.validateUniqueId("c154ad51-4cd7-400f-9d84-532347f87360");
	}

}
