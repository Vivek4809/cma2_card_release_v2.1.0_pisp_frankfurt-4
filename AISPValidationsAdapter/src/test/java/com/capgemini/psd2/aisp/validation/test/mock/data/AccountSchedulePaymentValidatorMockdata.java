package com.capgemini.psd2.aisp.validation.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBActiveOrHistoricCurrencyAndAmount;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification4;
import com.capgemini.psd2.aisp.domain.OBCashAccount3;
import com.capgemini.psd2.aisp.domain.OBExternalScheduleType1Code;
import com.capgemini.psd2.aisp.domain.OBReadScheduledPayment1;
import com.capgemini.psd2.aisp.domain.OBReadScheduledPayment1Data;
import com.capgemini.psd2.aisp.domain.OBScheduledPayment1;

public class AccountSchedulePaymentValidatorMockdata {
	
	public static OBReadScheduledPayment1 getAccountSchedulePaymentGETResponse(){
		OBScheduledPayment1 oBScheduledPayment1 = new OBScheduledPayment1();
		oBScheduledPayment1.setAccountId("c154ad51-4cd7-400f-9d84-532347f87360");
		oBScheduledPayment1.setScheduledPaymentId("SP01");
		oBScheduledPayment1.setScheduledPaymentDateTime("2018-05-05T00:00:00+00:00");
		oBScheduledPayment1.setScheduledType(OBExternalScheduleType1Code.EXECUTION);
		oBScheduledPayment1.setReference("Ref123");
		OBActiveOrHistoricCurrencyAndAmount oBActiveOrHistoricCurrencyAndAmount = new OBActiveOrHistoricCurrencyAndAmount();
		oBActiveOrHistoricCurrencyAndAmount.setAmount("111.22");
		oBActiveOrHistoricCurrencyAndAmount.setCurrency("GBP");
		oBScheduledPayment1.setInstructedAmount(oBActiveOrHistoricCurrencyAndAmount);
		OBBranchAndFinancialInstitutionIdentification4 oBBranchAndFinancialInstitutionIdentification4 = new OBBranchAndFinancialInstitutionIdentification4();
		oBBranchAndFinancialInstitutionIdentification4.setSchemeName("BICFI");
		oBBranchAndFinancialInstitutionIdentification4.setIdentification("DE89370400440532013000");
		oBScheduledPayment1.setCreditorAgent(oBBranchAndFinancialInstitutionIdentification4);
		OBCashAccount3 oBCashAccount3 = new OBCashAccount3();
		oBCashAccount3.setSchemeName("PAN");
		oBCashAccount3.setIdentification("1234567890");
		oBCashAccount3.setName("Mr Tee");
		oBCashAccount3.setSecondaryIdentification("Teee");
		oBScheduledPayment1.setCreditorAccount(oBCashAccount3);
		List<OBScheduledPayment1> schedulePaymentList = new ArrayList<>();
		schedulePaymentList.add(oBScheduledPayment1);
		OBReadScheduledPayment1Data oBReadScheduledPayment1Data = new OBReadScheduledPayment1Data();
		oBReadScheduledPayment1Data.setScheduledPayment(schedulePaymentList);
		OBReadScheduledPayment1 oBReadScheduledPayment1 = new OBReadScheduledPayment1();
		oBReadScheduledPayment1.setData(oBReadScheduledPayment1Data);
		Links links = new Links();
		oBReadScheduledPayment1.setLinks(links);
		Meta meta = new Meta();
		meta.setTotalPages(1);
		oBReadScheduledPayment1.setMeta(meta);
		return oBReadScheduledPayment1;
	}
	
	public static OBReadScheduledPayment1 getAccountSchedulePaymentCreditorAccountGETResponse(){
		OBScheduledPayment1 oBScheduledPayment1 = new OBScheduledPayment1();
		oBScheduledPayment1.setAccountId("c154ad51-4cd7-400f-9d84-532347f87360");
		oBScheduledPayment1.setScheduledPaymentId(null);
		oBScheduledPayment1.setScheduledPaymentDateTime(null);
		oBScheduledPayment1.setScheduledType(OBExternalScheduleType1Code.EXECUTION);
		oBScheduledPayment1.setReference(null);
		OBActiveOrHistoricCurrencyAndAmount oBActiveOrHistoricCurrencyAndAmount = new OBActiveOrHistoricCurrencyAndAmount();
		oBActiveOrHistoricCurrencyAndAmount.setAmount(null);
		oBActiveOrHistoricCurrencyAndAmount.setCurrency(null);
		oBScheduledPayment1.setInstructedAmount(oBActiveOrHistoricCurrencyAndAmount);
		OBBranchAndFinancialInstitutionIdentification4 oBBranchAndFinancialInstitutionIdentification4 = new OBBranchAndFinancialInstitutionIdentification4();
		oBBranchAndFinancialInstitutionIdentification4.setSchemeName(null);
		oBBranchAndFinancialInstitutionIdentification4.setIdentification(null);
		oBScheduledPayment1.setCreditorAgent(oBBranchAndFinancialInstitutionIdentification4);
		OBCashAccount3 oBCashAccount3 = new OBCashAccount3();
		oBCashAccount3.setSchemeName(null);
		oBCashAccount3.setIdentification(null);
		oBCashAccount3.setName(null);
		oBCashAccount3.setSecondaryIdentification(null);
		oBScheduledPayment1.setCreditorAccount(null);
		List<OBScheduledPayment1> schedulePaymentList = new ArrayList<>();
		schedulePaymentList.add(oBScheduledPayment1);
		OBReadScheduledPayment1Data oBReadScheduledPayment1Data = new OBReadScheduledPayment1Data();
		oBReadScheduledPayment1Data.setScheduledPayment(schedulePaymentList);
		OBReadScheduledPayment1 oBReadScheduledPayment1 = new OBReadScheduledPayment1();
		oBReadScheduledPayment1.setData(oBReadScheduledPayment1Data);
		Links links = new Links();
		oBReadScheduledPayment1.setLinks(links);
		Meta meta = new Meta();
		meta.setTotalPages(1);
		oBReadScheduledPayment1.setMeta(meta);
		return oBReadScheduledPayment1;
	}
	
	public static OBReadScheduledPayment1 getAccountSchedulePaymentGETResponseBlank(){
		OBScheduledPayment1 oBScheduledPayment1 = new OBScheduledPayment1();
		oBScheduledPayment1.setAccountId("c154ad51-4cd7-400f-9d84-532347f87360");
		oBScheduledPayment1.setScheduledPaymentId("");
		oBScheduledPayment1.setScheduledPaymentDateTime("");
		oBScheduledPayment1.setScheduledType(OBExternalScheduleType1Code.EXECUTION);
		oBScheduledPayment1.setReference("");
		OBActiveOrHistoricCurrencyAndAmount oBActiveOrHistoricCurrencyAndAmount = new OBActiveOrHistoricCurrencyAndAmount();
		oBActiveOrHistoricCurrencyAndAmount.setAmount("");
		oBActiveOrHistoricCurrencyAndAmount.setCurrency("");
		oBScheduledPayment1.setInstructedAmount(oBActiveOrHistoricCurrencyAndAmount);
		OBBranchAndFinancialInstitutionIdentification4 oBBranchAndFinancialInstitutionIdentification4 = new OBBranchAndFinancialInstitutionIdentification4();
		oBBranchAndFinancialInstitutionIdentification4.setSchemeName("");
		oBBranchAndFinancialInstitutionIdentification4.setIdentification("");
		oBScheduledPayment1.setCreditorAgent(oBBranchAndFinancialInstitutionIdentification4);
		OBCashAccount3 oBCashAccount3 = new OBCashAccount3();
		oBCashAccount3.setSchemeName("");
		oBCashAccount3.setIdentification("");
		oBCashAccount3.setName("");
		oBCashAccount3.setSecondaryIdentification("");
		OBCashAccount3 creditorAccount = new OBCashAccount3();
		creditorAccount.setSchemeName("");
		creditorAccount.setIdentification("");
		creditorAccount.setName("");
		creditorAccount.setSecondaryIdentification("");
		oBScheduledPayment1.setCreditorAccount(creditorAccount);
		List<OBScheduledPayment1> schedulePaymentList = new ArrayList<>();
		schedulePaymentList.add(oBScheduledPayment1);
		OBReadScheduledPayment1Data oBReadScheduledPayment1Data = new OBReadScheduledPayment1Data();
		oBReadScheduledPayment1Data.setScheduledPayment(schedulePaymentList);
		OBReadScheduledPayment1 oBReadScheduledPayment1 = new OBReadScheduledPayment1();
		oBReadScheduledPayment1.setData(oBReadScheduledPayment1Data);
		Links links = new Links();
		oBReadScheduledPayment1.setLinks(links);
		Meta meta = new Meta();
		meta.setTotalPages(1);
		oBReadScheduledPayment1.setMeta(meta);
		return oBReadScheduledPayment1;
	}
	
	public static OBReadScheduledPayment1 getAccountScheduledPaymentGETResponseNullData(){
		OBScheduledPayment1 oBScheduledPayment1 = new OBScheduledPayment1();
		oBScheduledPayment1.setAccountId("c154ad51-4cd7-400f-9d84-532347f87360");
		oBScheduledPayment1.setScheduledPaymentId("SP01");
		oBScheduledPayment1.setScheduledPaymentDateTime("2018-05-05T00:00:00+00:00");
		oBScheduledPayment1.setScheduledType(OBExternalScheduleType1Code.EXECUTION);
		oBScheduledPayment1.setReference("Ref123");
		OBActiveOrHistoricCurrencyAndAmount oBActiveOrHistoricCurrencyAndAmount = new OBActiveOrHistoricCurrencyAndAmount();
		oBActiveOrHistoricCurrencyAndAmount.setAmount("111.22");
		oBActiveOrHistoricCurrencyAndAmount.setCurrency("GBP");
		oBScheduledPayment1.setInstructedAmount(oBActiveOrHistoricCurrencyAndAmount);
		OBBranchAndFinancialInstitutionIdentification4 oBBranchAndFinancialInstitutionIdentification4 = new OBBranchAndFinancialInstitutionIdentification4();
		oBBranchAndFinancialInstitutionIdentification4.setSchemeName("BICFI");
		oBBranchAndFinancialInstitutionIdentification4.setIdentification("DE89370400440532013000");
		oBScheduledPayment1.setCreditorAgent(oBBranchAndFinancialInstitutionIdentification4);
		OBCashAccount3 oBCashAccount3 = new OBCashAccount3();
		oBCashAccount3.setSchemeName("PAN");
		oBCashAccount3.setIdentification("1234567890");
		oBCashAccount3.setName("Mr Tee");
		oBCashAccount3.setSecondaryIdentification("Teee");
		oBScheduledPayment1.setCreditorAccount(oBCashAccount3);
		List<OBScheduledPayment1> schedulePaymentList = new ArrayList<>();
		schedulePaymentList.add(null);
		OBReadScheduledPayment1Data oBReadScheduledPayment1Data = new OBReadScheduledPayment1Data();
		oBReadScheduledPayment1Data.setScheduledPayment(schedulePaymentList);
		OBReadScheduledPayment1 oBReadScheduledPayment1 = new OBReadScheduledPayment1();
		oBReadScheduledPayment1.setData(oBReadScheduledPayment1Data);
		Links links = new Links();
		oBReadScheduledPayment1.setLinks(links);
		Meta meta = new Meta();
		meta.setTotalPages(1);
		oBReadScheduledPayment1.setMeta(meta);
		return oBReadScheduledPayment1;
	}
}
