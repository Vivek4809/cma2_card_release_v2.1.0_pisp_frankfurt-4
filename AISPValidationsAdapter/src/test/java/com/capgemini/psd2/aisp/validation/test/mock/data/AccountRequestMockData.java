package com.capgemini.psd2.aisp.validation.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBExternalPermissions1Code;
import com.capgemini.psd2.aisp.domain.OBReadData1;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.aisp.domain.OBReadConsent1;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.aisp.domain.OBRisk2;
import com.capgemini.psd2.aisp.domain.OBExternalRequestStatus1Code;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AccountRequestMockData {

	public static OBReadConsent1 getAccountRequestsParamatersMockData() {
		OBReadConsent1 accountRequestsParammeters = new OBReadConsent1();
		OBReadData1 data = new OBReadData1();
		//Data data = new Data();
		List<OBExternalPermissions1Code> permissions = new ArrayList<>();
		permissions.add(OBExternalPermissions1Code.READACCOUNTSDETAIL);
		permissions.add(OBExternalPermissions1Code.READBALANCES);
		permissions.add(OBExternalPermissions1Code.READACCOUNTSBASIC);
		permissions.add(OBExternalPermissions1Code.READTRANSACTIONSBASIC);
		permissions.add(OBExternalPermissions1Code.READTRANSACTIONSDETAIL);
		data.setPermissions(permissions);
		data.setExpirationDateTime("2019-05-02T00:00:00+00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00+00:00");
		data.setTransactionToDateTime("2017-05-03T00:00:00+00:00");

		accountRequestsParammeters.setData(data);
		accountRequestsParammeters.setRisk(null);
		return accountRequestsParammeters;
	}
		
	public static OBReadConsent1 getAccountRequestsParamatersMockDataOrConditions() {
		OBReadConsent1 accountRequestsParammeters = new OBReadConsent1();
		//Data data = new Data();
		OBReadData1 data = new OBReadData1();

		List<OBExternalPermissions1Code> permissions = new ArrayList<>();
		permissions.add(OBExternalPermissions1Code.READACCOUNTSDETAIL);
		permissions.add(OBExternalPermissions1Code.READBALANCES);
		permissions.add(OBExternalPermissions1Code.READTRANSACTIONSDEBITS);
		permissions.add(OBExternalPermissions1Code.READTRANSACTIONSDETAIL);
		permissions.add(OBExternalPermissions1Code.READTRANSACTIONSBASIC);

		data.setPermissions(permissions);
		data.setExpirationDateTime("2019-05-02T00:00:00+00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00+00:00");
		data.setTransactionToDateTime("2017-05-03T00:00:00+00:00");

		accountRequestsParammeters.setData(data);
		accountRequestsParammeters.setRisk(null);
		return accountRequestsParammeters;
	}
	
	public static OBReadConsent1 getAccountRequestsParamatersExceptionMockData() {
		OBReadConsent1 accountRequestsParammeters = new OBReadConsent1();
		//Data data = new Data();
		OBReadData1 data = new OBReadData1();

		List<OBExternalPermissions1Code> permissions = new ArrayList<>();
		permissions.add(OBExternalPermissions1Code.READACCOUNTSBASIC);
		
		data.setPermissions(permissions);
		data.setExpirationDateTime("2019-05-02T00:00:00+00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00+00:00");
		data.setTransactionToDateTime("2017-05-03T00:00:00+00:00");

		accountRequestsParammeters.setData(data);
		accountRequestsParammeters.setRisk(null);
		return accountRequestsParammeters;
	}
	
	public static OBReadConsentResponse1 getAccountRequestsMockResponseData() {

		OBReadConsentResponse1 accountRequestPOSTResponse = new OBReadConsentResponse1();

		//Data1 data = new Data1();
		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();

		List<OBExternalPermissions1Code> permissions = new ArrayList<>();
		permissions.add(OBExternalPermissions1Code.READACCOUNTSBASIC);
		permissions.add(OBExternalPermissions1Code.READACCOUNTSDETAIL);
		permissions.add(OBExternalPermissions1Code.READBALANCES);

		data.setPermissions(permissions);
		data.setCreationDateTime("2017-05-02T00:00:00+00:00");
		data.setExpirationDateTime("2019-05-02T00:00:00+00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00+00:00");
		data.setTransactionToDateTime("2017-12-03T00:00:00+00:00");
		data.setStatusUpdateDateTime("2019-01-24T08:59:52+00:00");

		data.setConsentId("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
		data.setStatus(OBExternalRequestStatus1Code.AWAITINGAUTHORISATION);
		
		data.setTppCID("6443e15975554bce8099e35b88b40465");

		accountRequestPOSTResponse.setData(data);
		accountRequestPOSTResponse.setRisk(null);

		return accountRequestPOSTResponse;
	}
	
	public static OBReadConsentResponse1 getAccountRequestsMockResponseDataNullDates() {

		OBReadConsentResponse1 accountRequestPOSTResponse = new OBReadConsentResponse1();

		//Data1 data = new Data1();
		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();

		List<OBExternalPermissions1Code> permissions = new ArrayList<>();
		permissions.add(OBExternalPermissions1Code.READACCOUNTSBASIC);
		permissions.add(OBExternalPermissions1Code.READACCOUNTSDETAIL);
		permissions.add(OBExternalPermissions1Code.READBALANCES);

		data.setPermissions(permissions);
		data.setCreationDateTime(null);
		data.setExpirationDateTime(null);
		data.setTransactionFromDateTime(null);
		data.setTransactionToDateTime(null);
		data.setStatusUpdateDateTime(null);

		data.setConsentId("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
		data.setStatus(OBExternalRequestStatus1Code.AWAITINGAUTHORISATION);
		
		data.setTppCID("6443e15975554bce8099e35b88b40465");

		accountRequestPOSTResponse.setData(data);
		accountRequestPOSTResponse.setRisk(null);

		return accountRequestPOSTResponse;
	}
	
	public static OBReadConsentResponse1 getAccountRequestsMockResponseDataBlankDates() {

		OBReadConsentResponse1 accountRequestPOSTResponse = new OBReadConsentResponse1();

		//Data1 data = new Data1();
		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();

		List<OBExternalPermissions1Code> permissions = new ArrayList<>();
		permissions.add(OBExternalPermissions1Code.READACCOUNTSBASIC);
		permissions.add(OBExternalPermissions1Code.READACCOUNTSDETAIL);
		permissions.add(OBExternalPermissions1Code.READBALANCES);

		data.setPermissions(permissions);
		data.setCreationDateTime("");
		data.setExpirationDateTime("");
		data.setTransactionFromDateTime("");
		data.setTransactionToDateTime("");
		data.setStatusUpdateDateTime("");

		data.setConsentId("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
		data.setStatus(OBExternalRequestStatus1Code.AWAITINGAUTHORISATION);
		
		data.setTppCID("6443e15975554bce8099e35b88b40465");

		accountRequestPOSTResponse.setData(data);
		accountRequestPOSTResponse.setRisk(null);

		return accountRequestPOSTResponse;
	}
	
	public static OBReadConsentResponse1 getAccountRequestsBranchCoverageMockResponseData() {

		OBReadConsentResponse1 accountRequestPOSTResponse = new OBReadConsentResponse1();
		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		//Data1 data = new Data1();

		List<OBExternalPermissions1Code> permissions = new ArrayList<>();
		permissions.add(OBExternalPermissions1Code.READACCOUNTSBASIC);
		permissions.add(OBExternalPermissions1Code.READACCOUNTSDETAIL);
		permissions.add(OBExternalPermissions1Code.READBALANCES);

		data.setPermissions(permissions);
		data.setCreationDateTime("2017-05-02T00:00:00+00:00");
		data.setExpirationDateTime("2019-05-02T00:00:00+00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00+00:00");
		data.setTransactionToDateTime("2017-12-03T00:00:00+00:00");

		data.setConsentId("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
		data.setStatus(OBExternalRequestStatus1Code.AWAITINGAUTHORISATION);
		
		data.setTppCID("6443e15975554bce8099e35b88b40465");
		
		accountRequestPOSTResponse.setData(data);
		OBRisk2 risk=new OBRisk2();
		accountRequestPOSTResponse.setRisk(risk);
		Links links = new Links();
		links.setSelf("http://localhost:8989/account-requests");
		accountRequestPOSTResponse.setLinks(links);
		Meta metaData = new Meta();
		metaData.setTotalPages(1);
		accountRequestPOSTResponse.setMeta(metaData);

		return accountRequestPOSTResponse;
	}
	
	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public static OBReadConsent1 getAccountRequestsParamatersMockData_NullDates() {
		OBReadConsent1 accountRequestsParammeters = new OBReadConsent1();
		OBReadData1 data = new OBReadData1();
		//Data data = new Data();
		List<OBExternalPermissions1Code> permissions = new ArrayList<>();
		permissions.add(OBExternalPermissions1Code.READACCOUNTSDETAIL);
		permissions.add(OBExternalPermissions1Code.READBALANCES);
		permissions.add(OBExternalPermissions1Code.READACCOUNTSBASIC);
		permissions.add(OBExternalPermissions1Code.READTRANSACTIONSBASIC);
		permissions.add(OBExternalPermissions1Code.READTRANSACTIONSDETAIL);
		data.setPermissions(permissions);
		data.setExpirationDateTime(null);
		data.setTransactionFromDateTime(null);
		data.setTransactionToDateTime(null);

		accountRequestsParammeters.setData(data);
		accountRequestsParammeters.setRisk(null);
		return accountRequestsParammeters;
	}
	
	public static OBReadConsent1 getAccountRequestsParamatersMockData_BlankDates() {
		OBReadConsent1 accountRequestsParammeters = new OBReadConsent1();
		OBReadData1 data = new OBReadData1();
		//Data data = new Data();
		List<OBExternalPermissions1Code> permissions = new ArrayList<>();
		permissions.add(OBExternalPermissions1Code.READACCOUNTSDETAIL);
		permissions.add(OBExternalPermissions1Code.READBALANCES);
		permissions.add(OBExternalPermissions1Code.READACCOUNTSBASIC);
		permissions.add(OBExternalPermissions1Code.READTRANSACTIONSBASIC);
		permissions.add(OBExternalPermissions1Code.READTRANSACTIONSDETAIL);
		data.setPermissions(permissions);
		data.setExpirationDateTime("");
		data.setTransactionFromDateTime("");
		data.setTransactionToDateTime("");

		accountRequestsParammeters.setData(data);
		accountRequestsParammeters.setRisk(null);
		return accountRequestsParammeters;
	}
	
	public static OBReadConsent1 getAccountRequestsParamatersMockData_NoOffsetInDates() {
		OBReadConsent1 accountRequestsParammeters = new OBReadConsent1();
		OBReadData1 data = new OBReadData1();
		//Data data = new Data();
		List<OBExternalPermissions1Code> permissions = new ArrayList<>();
		permissions.add(OBExternalPermissions1Code.READACCOUNTSDETAIL);
		permissions.add(OBExternalPermissions1Code.READBALANCES);
		permissions.add(OBExternalPermissions1Code.READACCOUNTSBASIC);
		permissions.add(OBExternalPermissions1Code.READTRANSACTIONSBASIC);
		permissions.add(OBExternalPermissions1Code.READTRANSACTIONSDETAIL);
		data.setPermissions(permissions);
		data.setExpirationDateTime("2019-05-02T00:00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00");
		data.setTransactionToDateTime("2017-05-03T00:00:00");

		accountRequestsParammeters.setData(data);
		accountRequestsParammeters.setRisk(null);
		return accountRequestsParammeters;
	}

}