package com.capgemini.psd2.constants;

public class SSAElements {

	public static final String SOFTWARE_CLIENT_ID = "software_client_id";
	public static final String SOFTWARE_CLIENT_NAME = "software_client_name";
	public static final String SOFTWARE_CLIENT_DESC = "software_client_description";
	public static final String SOFTWARE_CLIENT_URI = "software_client_uri";
	public static final String SOFTWARE_CONTRACTS = "software_contacts";
	public static final String SOFTWARE_ENV = "software_environment";
	public static final String SOFTWARE_ID = "software_id";
	public static final String SOFTWARE_JWKS_ENDPOINT = "software_jwks_endpoint";
	public static final String SOFTWARE_JWKS_REVOKED_ENDPOINT = "software_jwks_revoked_endpoint";
	public static final String SOFTWARE_LOGO_URI = "software_logo_uri";
	public static final String SOFTWARE_MODE = "software_mode";
	public static final String SOFTWARE_ON_BEHALF_ORG = "software_on_behalf_of_org";
	public static final String SOFTWARE_POLICY_URI = "software_policy_uri";
	public static final String SOFTWARE_REDIRECT_URIS = "software_redirect_uris";
	public static final String SOFTWARE_ROLES = "software_roles";
	public static final String SOFTWARE_TOS_URI = "software_tos_uri";
	public static final String SOFTWARE_VERSION = "software_version";
	public static final String SOFTWARE_ORG_ID = "org_id";
	public static final String SOFTWARE_ORG_STATUS = "org_status";

	private SSAElements() {

	}
}
