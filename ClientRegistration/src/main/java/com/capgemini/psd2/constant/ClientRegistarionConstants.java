/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.constant;

/**
 * The Class PortalConstants.
 */
public class ClientRegistarionConstants {

	private ClientRegistarionConstants() {
	}

	/** URL constants */
	public static final String LOGOUTURL = "logoutURL";
	public static final String MULELAUNCHURL = "muleLaunchURL";
	public static final String SAMLATTRIBUTE = "samlAttributes";
	public static final String ERRORMESSAGE = "errorMessage";
	public static final String ERRORCODE = "errorCode";
	public static final String PORTALBASEURL = "portalBaseURL";

	/** SAML Attribute name **/
	public static final String SAML_ATTRIBUTE_USER_NAME = "username";
	public static final String SAML_ATTRIBUTE_GIVEN_NAME = "given_name";
	public static final String SAML_ATTRIBUTE_FAMILY_NAME = "family_name";

	public static final String LDAP_ATTRIBUTE_UID = "uid";

	/** Error Types **/
	public static final String INFO = "2";
	public static final String WARN = "4";
	public static final String ERROR = "8";
	public static final String FATAL = "12";

	public static final String VALID = "VALID";
	public static final String INVALID = "INVALID";
	public static final String USER_NOT_FOUND = "USER_NOT_FOUND";
	public static final String FAIL = "FAIL";
	public static final String SAML_ATTRIBUTE_DISPLAY_NAME = "displayname";
	public static final String PF = "PF";
	public static final String JSESSIONID = "JSESSIONID";
	public static final String SAML_ATTRIBUTE_GROUP = "group";
	public static final String SAML_ATTRIBUTE_USER_TYPE = "userType";
	public static final String DEVPORTAL_USER = "devportal";
	public static final String SUPPORT_USER = "support";
	public static final String ROLE_ANONYMOUS = "ROLE_ANONYMOUS";

	public static final String UNABLE_TO_PROCESS_YOUR_REQUEST = "Unable to process your request. Please try again later.";
	public static final String ROLE_ANONYMOUS_EXECEPTION = "IDP session expired. Authorities is Anonymous Role.";
	public static final String LOCAL_SESSION_EXIPRED = "Application Local session expired";
	public static final String SESSION_EXIPRED = "Application OR IDP session expired";
	public static final String SAML_USER_NULL = "SAML user is null.";
	public static final String INVALID_USER_ID = "User Id is invalid";
	public static final String SESSION_EXPIRED = "Your session has expired. Please log in again.";
	public static final String APP_SESSION_EXPIRED = "Your application session has expired. Please log in again.";
	public static final String INVALID_USER_DETAILS = "User details are invalid";
	public static final String INVALID_FIRST_NAME = "First name is invalid in user details";
	public static final String INVALID_LAST_NAME = "Last name is invalid in user details";
	public static final String INVALID_ORGANIZATION = "Organization is invalid in user details";
	public static final String INVALID_CONTACT_NUMBER = "Contact number is invalid in user details";
	public static final String INVALID_COUNTRY_LOCATION = "Country location is invalid in user details";
	public static final String INVALID_EMAIL_ID = "Email id is invalid in user details";
	public static final String USER_ID_NULL = "User email id is null";

	public static final String SSA_NOT_OF_SELECTED_ORG = "SSA does not belong to selected Organisation.";
	public static final String INVALID_SSA = "Invalid SSA. Please regenerate a valid SSA.";
	public static final String SSA_EXPIRED = "SSA has expired. Please try with a fresh SSA.";
	public static final String TPP_ORG_NOT_ACTIVE = "This organization is not authorised on Open Banking. Please contact Open Banking.";
	public static final String INVALID_TIER = "Tier is greater than 1 for this api version";

	public static final String MULE_LOGIN_FAILURE = "Mule Gateway Login call failed";
	public static final String MULE_API_REGISTERATION_CALL_FAILURE = "Mule TPP Application Registeration call failed";
	public static final String MULE_FETCH_LIST_OF_ALL_ORG_APIS_FAILURE = "Mule call to fetch all the Organization Apis call failed";
	public static final String MULE_ORGANISATION_APIS_NOT_AVAILABLE = "No Apis exist for the associated organisation in Mule";
	public static final String MULE_FETCH_LIST_OF_TIERS_FOR_API_VERSIONS_FAILURE = "Mule call to fetch the list of tiers for all the Api Versions call failed";
	public static final String MULE_CREATE_APP_CONTRACT_FAILURE = "Mule create Application Contract for each Api Version call failed";
	public static final String PF_REGISTER_API_CALL_FAILURE = "Ping Federate Register Application call failed";
	public static final String PD_TPP_ORG_UPDATE_OR_INSERT_FAILURE = "Ping Directory TPP Organisation details updation or insertion call failed";
	public static final String PD_TPP_APP_ASSOCIATION_FAILURE = "Ping Directory TPP Application Association with TPP Organisation call failed";
	public static final String PD_CLIENT_APP_MAPPING_FAILURE = "Ping Directory Client Application Mapping call failed";
	public static final String PF_REGISTER_API_CALL_DELETE_FAILURE = "FATAL: Ping Federate TPP Application Registration delete call failed";
	public static final String MULE_API_REGISTERATION_CALL_DELETE_FAILURE = "FATAL: Mule Api Registration delete call failed";
	public static final String PD_TPP_ORG_UPDATE_OR_INSERT_DELETE_FAILURE = "FATAL: Ping Directory TPP Organisation details updation or insertion delete call failed";
	public static final String PD_TPP_APP_ASSOCIATION_DELETE_FAILURE = "FATAL: Ping Directory TPP Application Association with TPP Organisation delete call failed";
	public static final String LDAP_ATTRIBUTE_NOT_FOUND_IN_TPP = "No data found in the selected TPP";
	public static final String NO_TPP_DATA_FOUND = "No TPP Data Found";
	public static final String PING_DIRECTORY_CALL_FAILED = "Ping Directory Call Failed";
	public static final String LOGIN_ERROR = "Unable to login. Please try later.";
	public static final String NO_TPP_APPLICATION_DATA_FOUND = "No Tpp Application Data found for the selected organisation in Ping Directory";
	public static final String PD_IS_TPP_EXIST_CALL_FAILED = "Ping Directory call to check if TPP Exist Failed";
	public static final String CONTRACT_API_FLAG_INVALID = "Either set the createContractForAllApis flag as true or set flag createContractForAllApis as false and mention the Apis for which contract is to be created";
	/** The Constant CORRELATION_ID. */
	public static final String CORRELATION_ID = "x-fapi-interaction-id";

	/** The Constant ACCESS_TOKEN_NAME. */
	public static final String ACCESS_TOKEN_NAME = "Authorization";

	/** The Constant COLON. */
	public static final String COLON = " : ";

	/** The Constant SEMI_COLON. */
	public static final String SEMI_COLON = ";";

	/** The Constant COMMA. */
	public static final String COMMA = ",";

	/** The Constant CUSTOMER_IP_ADDRESS. */
	public static final String CUSTOMER_IP_ADDRESS = "x-fapi-customer-ip-address";

	/** The Constant CUSTOMER_LAST_LOGGED_TIME. */
	public static final String CUSTOMER_LAST_LOGGED_TIME = "x-fapi-customer-last-logged-time";

	/** The Constant FINANCIAL_ID. */
	public static final String FINANCIAL_ID = "x-fapi-financial-id";

	public static final String EQUAS = "=";

	public static final String QUESTIONMARK = "?";

	public static final String AMPERSAND = "&";

	public static final String OAUTH_ENC_URL = "oAuthEncUrl";

	public static final String CONSENT_FLOW_TYPE = "consentFlowType";

	public static final String SLASH = "/";

	public static final String IDEMPOTENCY_KEY = "x-idempotency-key";

	public static final String TPP_CID = "tppCID";

	public static final String USER_ID = "x-user-id";

	public static final String CHANNEL_ID = "x-channel-id";

	public static final String CORRELATION_REQ_HEADER = "X-CORRELATION-ID";

	public static final String PLATFORM_IN_REQ_HEADER = "X-BOI-PLATFORM";

	public static final String CHANNEL_IN_REQ_HEADER = "X-BOI-CHANNEL";

	public static final String USER_IN_REQ_HEADER = "X-BOI-USER";

	public static final String PAYMENT_CREATED_ON = "PAYMENT-CREATED-ON";

	public static final String PAYMENT_VALIDATION_STATUS = "PAYMENT-VALIDATION-STATUS";

	public static final String PAYMENT_SUBMISSION_STATUS = "PAYMENT-SUBMISSION-STATUS";

	public static final String PAYER_CURRENCY = "PAYER-CURRENCY";

	public static final String JS_MSG = "JAVASCRIPT_ENABLE_MSG";

	public static final String ERROR_MESSAGE = "ERROR_MESSAGES";

	public static final String TECHINICAL_ERROR = "Technical Error.";

	public static final String NOT_PRESENT = "NOT_PRESENT";

	public static final String EXIST = "EXIST";

	public static final String SUCCESS = "SUCCESS";

	public static final String EXCEPTION = "Exception";

	public static final String TECHNICAL_ERROR = "Technical_Error";
	public static final String AISP = "AISP";
	public static final String PISP = "PISP";
	public static final String EVENT_DASHBOARD_APPLICATION_VIEW = "EVENT_DASHBOARD_APPLICATION_VIEW";
	public static final String EVENT_DELETE_APPLICATION = "EVENT_DELETE_APPLICATION";
	public static final String EVENT_CREATE_APPLICATION = "EVENT_CREATE_APPLICATION";
	public static final String EVENT_GET_APPLICATION = "EVENT_GET_APPLICATION";
	public static final String EVENT_UPLOAD_JWT_TOKEN = "EVENT_UPLOAD_JWT_TOKEN";
	public static final String EVENT_CREATE_API_CONTRACTS = "EVENT_CREATE_API_CONTRACTS";

	public static final String EVENT_MULE_AUTHENTICATION = "EVENT_MULE_AUTHENTICATION";
	public static final String EVENT_LDAP_CREATE_APPLICATION = "EVENT_LDAP_CREATE_APPLICATION";
	public static final String EVENT_LDAP_UPDATE_TPP = "EVENT_LDAP_UPDATE_TPP";

	public static final String EVENT_MULE_CREATE_APPLICATION = "EVENT_MULE_CREATE_APPLICATION";
	public static final String EVENT_MULE_CREATE_CONTRACTS = "EVENT_MULE_CREATE_CONTRACTS";

	public static final String STEP_MULE_AUTHENTICATION = "STEP_MULE_AUTHENTICATION";
	public static final String STEP_MULE_CREATE_APPLIATION = "STEP_MULE_CREATE_APPLIATION";
	public static final String STEP_MULE_CREATE_CONTRACTS = "STEP_MULE_CREATE_CONTRACTS";
	public static final String STEP_MULE_DELETE_APPLICATION = "STEP_MULE_DELETE_APPLICATION";
	public static final String STEP_MULE_REVOKE_CONTRACTS = "STEP_MULE_REVOKE_CONTRACTS";
	public static final String STEP_MULE_REST_SECRET = "STEP_MULE_SECRET";

	public static final String STEP_LDAP_CREATE_APPLIATION = "STEP_LDAP_CREATE_APPLIATION";
	public static final String STEP_LDAP_UPDATE_TPP = "STEP_LDAP_UPDATE_TPP";
	public static final String STEP_LDAP_DELETE_APPLICATION = "STEP_LDAP_DELETE_APPLICATION";
	public static final String STEP_LDAP_UPDATE_SECRET = "STEP_LDAP_UPDATE_SECRET";
	public static final String STEP_LDAP_GET_APPLICATIONS = "STEP_LDAP_GET_APPLICATIONS";
	public static final String STEP_LDAP_GET_PTC_USER = "STEP_LDAP_GET_PTC_USER";
	public static final String STEP_LDAP_GET_TPP = "STEP_LDAP_GET_TPP";
	public static final String MULE_REVERT_APPLICATION_REQUEST = "MULE_REVERT_APPLICATION_REQUEST";
	public static final String MULE_REVERT_CONTRACT_REQUEST = "MULE_REVERT_CONTRACT_REQUEST";
	public static final String STEP_LDAP_UPDATE_TPP_APPLICATION = "STEP_LDAP_UPDATE_TPP_APPLICATION";
	public static final String STEP_MULE_REST_CLIENT_SECRET = "STEP_MULE_REST_CLIENT_SECRET";
	public static final String EVENT_RESET_SECRET = "EVENT_RESET_SECRET";
	public static final String STEP_POPULATE_TPP_APP_OBJ = "STEP_POPULATE_TPP_APP_OBJ";
	public static final String STEP_POPULATE_MULE_APP_REQ_OBJ = "STEP_POPULATE_MULE_APP_REQ_OBJ";
	public static final String TPP_ASSOCIATIONS_LIST = "tpp_associations_list";
	public static final String LOGOUT_URL = "logoutURL";
	public static final String INDEX_URL = "indexURL";
	public static final String UNABLE_TO_PARSE = "Unable to format the timestamp obtained from ping directory";

	public static final String TPPPORTAL_CUSTOM_SESSION_COOKIE = "TPPPORTAL_SESSION_COOKIE";

	public static final String TPPPORTAL_TEMP_COOKIE = "TPPPORTAL_TEMP_COOKIE";

	public static final String PTC_NAME = "username";

	public static final String ORGANIZATION_IDS = "tppNameList";

	public static final String PTC_ID = "subject";

	public static final String PTC_NAME_KEY = "ptcName";

	public static final String ORGANIZATION_IDS_KEY = "organizationIds";

	public static final String PTC_ID_KEY = "ptcId";

	public static final String OIDC_GIVEN_NAME = "given_name";

	public static final String OIDC_FAMILY_NAME = "family_name";

	public static final String OIDC_SUB = "sub";

	/** Index html page **/
	public static final String INDEX = "index";

	public static final String TPP_APPLICATION = "tppApplication";
	/** TPPs List **/
	public static final String TPP_LIST = "tppNameList";

	/** SAML Attribute name **/
	public static final String SAML_ATTRIBUTES = "samlAttributes";

	public static final String CORELATION_ID = "corelationId";

	public static final String PLUS = "+";

	public static final String ASTERISK = "*";

	public static final String CN = "cn=";

	public static final String CLIENTID = " clientId";

	public static final Integer SIXTY_SECONDS = 60;

	public static final String ISAUTHORIZED = "isAuthorized";
	public static final String UISTATICCONTENT = "uiStaticContent";

	public static final String CODE = "code";
	public static final String STATE = "state";

	public static final String INVALID_CLIENT_METADATA="invalid_client_metadata";
	public static final String IMPLICIT_GRANT_SCOPE="IMPLICIT";
	public static final String UNAPPROVED_SSA="unapproved_software_statement";
	public static final String INVALID_SSA_CERTIFICATE="Invalid SSA or Certificate";

}
