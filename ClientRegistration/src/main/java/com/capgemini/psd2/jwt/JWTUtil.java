package com.capgemini.psd2.jwt;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.JWTParser;
import com.nimbusds.jwt.SignedJWT;

public final class JWTUtil {
	/**
	 * Check expiry of JWT
	 * 
	 * @param claims
	 */
	
	private JWTUtil(){}
	
	public static JWT checkJWTValidity(String jwt) {
		SignedJWT signedJWT = null;
		JWTClaimsSet claims = null;
		try {
			signedJWT = (SignedJWT) JWTParser.parse(jwt);
			claims = signedJWT.getJWTClaimsSet();
		} catch (ParseException e) {
			throw ClientRegistrationException.populatePortalException(e.getMessage(),
					ClientRegistrationErrorCodeEnum.INVALID_JWT);
		}
		if (claims != null) {
			// token MUST NOT be accepted on or after any specified exp time:
			Date exp = claims.getExpirationTime();
			Date now = new Date();
			if (exp != null) {

				jwtExpiryCheck(now,exp);
			}

			// token MUST NOT be accepted before any specified nbf time:
			Date nbf = claims.getNotBeforeTime();
			if (nbf != null) {

				jwtNotBeforeCheck(now,nbf);
			}
		}
		return signedJWT;
	}

	/**
	 * @param now
	 * @param nbf
	 */
	private static void jwtNotBeforeCheck(Date now, Date nbf) {
		SimpleDateFormat sdf;
		if (now.before(nbf)) {
			sdf = new SimpleDateFormat();
			String nbfVal = sdf.format(nbf);
			String nowVal = sdf.format(now);

			String msg = "JWT must not be accepted before " + nbfVal + ". Current time: " + nowVal;
			throw ClientRegistrationException.populatePortalException(msg, ClientRegistrationErrorCodeEnum.INVALID_JWT);
		}
	}

	/**
	 * @param exp
	 * @return
	 */
	private static void jwtExpiryCheck(Date now,Date exp) {
		SimpleDateFormat sdf;
		if (now.equals(exp) || now.after(exp)) {
			sdf = new SimpleDateFormat();
			String expVal = sdf.format(exp);
			String nowVal = sdf.format(now);

			String msg = "JWT expired at " + expVal + ". Current time: " + nowVal;
			throw ClientRegistrationException.populatePortalException(msg, ClientRegistrationErrorCodeEnum.INVALID_JWT);
		}
	}
}
