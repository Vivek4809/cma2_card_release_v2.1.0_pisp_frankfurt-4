package com.capgemini.psd2.service;

import java.util.List;

import javax.naming.NamingException;

import com.capgemini.tpp.ldap.client.model.ClientModel;
import com.capgemini.tpp.ldap.client.model.TPPApplication;
import com.capgemini.tpp.ssa.model.SSAModel;

public interface PingDirectoryService {

	// Add Application Methods
	public boolean isTPPExist(String tppOrgId);

	public void registerOrUpdateTPPOrg(String tppOrgId, SSAModel ssaElements, boolean add);

	public void associateTPPApplicationWithTPP(String tppOrgId, ClientModel clientModel);

	public void mapClientApp(String clientId, String appId, String tppOrgId, String ssaToken, String softwareJwks, String publicKey);

	public List<TPPApplication> fetchListOfTPPApplications(String tppOrgId);

	public List<String> getClientID(String tppOrgId);

	public Boolean isTppBlockEnabled(String tppOrgId);

	// RollBack Methods
	public void deleteTppOrgRegistration(String tppOrgId);

	public void deleteTppOrgUpdation(String tppOrgId);

	public void deleteTPPApplicationAssociationWithTPP(String tppOrgId, String clientId);

	// Utility method
	public String getAttributeValuesFromClientAppMapping(String clientId, String attribute) throws NamingException;

	// Delete Application Methods
	public void deleteMapClientApp(String clientId);
	
	public List<Object> fetchTppAppMappingForClient(String clientId);
}
