package com.capgemini.psd2.service;

import com.capgemini.tpp.ldap.client.model.ClientModel;
import com.capgemini.tpp.muleapi.schema.ApisResponse;
import com.capgemini.tpp.muleapi.schema.Application;
import com.capgemini.tpp.muleapi.schema.LoginResponse;
import com.capgemini.tpp.muleapi.schema.NewApplicationMuleApiResponse;
import com.capgemini.tpp.muleapi.schema.ResetSecretResponse;
import com.capgemini.tpp.muleapi.schema.Tiers;
import com.capgemini.tpp.ssa.model.SSAModel;

public interface MuleService {

	public LoginResponse gatewayMuleAuthentication();

	public NewApplicationMuleApiResponse registerTPPApplicationInMule(ClientModel clientModel, SSAModel ssaDataElements,
			String clientSecret, LoginResponse loginResponse);

	public ApisResponse fetchListOfAllOrgApis(LoginResponse loginResponse);

	public Tiers fetchListOfTiersForApiVersions(int apiId, int apiVersionId, LoginResponse loginResponse);

	public void doApplicationContract(ApisResponse apisResponse, Integer applicationId, LoginResponse loginResponse);

	public Application fetchClientSecret(LoginResponse loginResponse, String applicationId);

	// RollBAck call
	public void deleteTppAppRegisterationInMule(LoginResponse loginResponse, String applicationId);

	public ResetSecretResponse resetClientSecret(LoginResponse loginResponse, String applicationId);

}
