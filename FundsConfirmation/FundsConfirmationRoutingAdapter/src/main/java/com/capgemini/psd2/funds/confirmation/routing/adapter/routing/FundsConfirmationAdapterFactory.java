package com.capgemini.psd2.funds.confirmation.routing.adapter.routing;

import com.capgemini.psd2.cisp.adapter.FundsConfirmationAdapter;

@FunctionalInterface
public interface FundsConfirmationAdapterFactory {
	/**
	 * Gets adapter instance.
	 *
	 * @param adapterName
	 *            the adapter name (i.e mongoDb/Client adpater)
	 * @return the adapter instance
	 */
	public FundsConfirmationAdapter getAdapterInstance(String adapterName);
}
