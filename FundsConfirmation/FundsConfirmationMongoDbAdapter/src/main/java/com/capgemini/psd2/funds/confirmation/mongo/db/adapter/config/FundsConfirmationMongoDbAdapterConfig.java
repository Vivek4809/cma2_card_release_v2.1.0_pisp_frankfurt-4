package com.capgemini.psd2.funds.confirmation.mongo.db.adapter.config;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.cisp.adapter.FundsConfirmationAdapter;
import com.capgemini.psd2.funds.confirmation.mongo.db.adapter.impl.FundsConfirmationMongoDbAdapterImpl;

@Component
public class FundsConfirmationMongoDbAdapterConfig {
	
	@Bean(name = "fundsConfirmationMongoDbAdapter")
	public FundsConfirmationAdapter fundsConfirmationMongoDBAdapter() {
		return new FundsConfirmationMongoDbAdapterImpl();
	}
}
