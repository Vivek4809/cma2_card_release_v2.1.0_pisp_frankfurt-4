
package com.capgemini.psd2.foundationservice.exceptions;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.capgemini.psd2.foundationservice.exceptions.domain.CommonResponse;
import com.capgemini.psd2.foundationservice.exceptions.domain.CommonResponseAdditionalInformation;
import com.capgemini.psd2.foundationservice.exceptions.domain.CommonResponseAdditionalInformation.TypeEnum1;
import com.capgemini.psd2.foundationservice.exceptions.domain.CommonResponse.TypeEnum;
import com.capgemini.psd2.foundationservice.utilities.MockServiceUtility;
import com.capgemini.psd2.foundationservice.utilities.NullCheckUtils;

@ControllerAdvice
public class MockFoundationServiceExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	private MockServiceUtility mockServiceUtility;
	
	@Value("${spring.application.name:#{API}}")
	private String appName;
	
	@ExceptionHandler({ Exception.class })
	public final ResponseEntity<?> handleInvalidRequest(Exception ex, WebRequest request) {

		HttpStatus status = null;
		CommonResponse commonResponse = null;
		
		
		if (ex != null && ex instanceof MockFoundationServiceException) {
			commonResponse = new CommonResponse();	
			commonResponse = ((MockFoundationServiceException) ex).getErrorInfo();
			String uri = request.getDescription(true);
			if (!NullCheckUtils.isNullOrEmpty(uri)) {
				String[] terms = uri.split("[=;]");
				commonResponse.setUri(terms[1]);
			}
			status = HttpStatus.valueOf(Integer.parseInt(ex.getMessage()));
		}
		
		
		if (commonResponse == null) {
			commonResponse = new CommonResponse();
			String defaultError = mockServiceUtility.getErrorCode().get("defaultError");
			defaultError = NullCheckUtils.isNullOrEmpty(defaultError) ? "Technical_Error" : defaultError;
			commonResponse.setCode(defaultError);
			commonResponse.setApi(appName);
			String uri = request.getDescription(true);
			if (!NullCheckUtils.isNullOrEmpty(uri)) {
				String[] terms = uri.split("[=;]");
				commonResponse.setUri(terms[1]);
			}
			CommonResponseAdditionalInformation additionalInformation = new CommonResponseAdditionalInformation();
			additionalInformation.setType(TypeEnum1.EXCEPTION);
			additionalInformation.setDetails(ex.getMessage());
			commonResponse.setAdditionalInformation(additionalInformation);
			commonResponse.setType(TypeEnum.ERROR);
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			commonResponse.setTimestamp(format.format(new Date()));
			commonResponse.setSummary(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
			status = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return new ResponseEntity<CommonResponse>(commonResponse, status);
	}
	
	@ExceptionHandler(MuleOutofBoxPolicyException.class)
	public final ResponseEntity<Object> muleOutofBoxPolicyException(Exception ex) {
		String detailedMessage = ex.getMessage();
		if(detailedMessage.equalsIgnoreCase("UNAUTHORIZED"))
			return new ResponseEntity<Object>(new HttpHeaders(), HttpStatus.UNAUTHORIZED);
		else if(detailedMessage.equalsIgnoreCase("UNSUPPORTED_MEDIA_TYPE"))
			return new ResponseEntity<Object>(new HttpHeaders(), HttpStatus.UNSUPPORTED_MEDIA_TYPE);
		else
			return new ResponseEntity<Object>(new HttpHeaders(), HttpStatus.TOO_MANY_REQUESTS);
	}
}
