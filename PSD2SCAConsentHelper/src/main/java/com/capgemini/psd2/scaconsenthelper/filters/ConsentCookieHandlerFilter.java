package com.capgemini.psd2.scaconsenthelper.filters;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.config.helpers.PickUpDataService;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentHelper;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.PFInstanceData;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.scaconsenthelper.models.RawAccessJwtToken;
import com.capgemini.psd2.security.exceptions.PSD2SecurityException;
import com.capgemini.psd2.security.exceptions.SCAConsentErrorCodeEnum;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.ValidationUtility;

public class ConsentCookieHandlerFilter extends CookieHandlerFilter {

	@Autowired
	private PickUpDataService pickupDataService;

	@Value("${spring.application.name}")
	private String applicationName;

	@Autowired
	private LoggerUtils loggerUtils;

	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;

	@Value("${app.jweDecryptionKey:#{null}}")
	private String jweDecryptionKey;
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ConsentCookieHandlerFilter.class);

	public ConsentCookieHandlerFilter(String flowType) {
		super(flowType);
	}

	@Override
	public void validateRefreshBackScenarios(Cookie customSessionControllCookie, PFInstanceData pfInstanceData,
			HttpServletRequest request, HttpServletResponse response)
			throws ParseException, ServletException, IOException {
		PickupDataModel intentData = null;
		RawAccessJwtToken token = null;		
		String requestUri = request.getRequestURI();
		String refId = request.getParameter(PFConstants.REF);
		
		request.setAttribute(PSD2Constants.APPLICATION_NAME, applicationName);
		try {
			if (customSessionControllCookie != null) {
				String tokenPayload = customSessionControllCookie.getValue();

				token = new RawAccessJwtToken(tokenPayload, requestHeaderAttributes);
				String intentDataStr = token.parseAndReturnClaims(jweDecryptionKey);
				intentData = JSONUtilities.getObjectFromJSONString(intentDataStr, PickupDataModel.class);
				if (intentData != null && intentData.getOriginatorRefId().equalsIgnoreCase(refId)) {
					requestHeaderAttributes.setCorrelationId(intentData.getCorrelationId());					
					requestHeaderAttributes.setIntentId(intentData.getIntentId());
					requestHeaderAttributes.setTppCID(intentData.getClientId());
					requestHeaderAttributes.setPsuId(intentData.getUserId());
					requestHeaderAttributes.setChannelId(intentData.getChannelId());
					requestHeaderAttributes.setSelfUrl(
							requestUri.endsWith("/") ? requestUri.substring(0, requestUri.length() - 1) : requestUri);
					requestHeaderAttributes.setMethodType(request.getMethod());
					
					throw PSD2SecurityException.populatePSD2SecurityException("Refresh Scenario..",
							SCAConsentErrorCodeEnum.VALIDATION_ERROR);
				}
			}
			if (intentData == null || !(intentData.getOriginatorRefId().equalsIgnoreCase(refId))) {
				intentData = pickupDataService.pickupIntent(refId, pfInstanceData);
				if (intentData == null) {
					throw PSD2SecurityException.populatePSD2SecurityException("Invalid Request..",
							SCAConsentErrorCodeEnum.INVALID_REQUEST);
				}
				Cookie cookie = createNewSessionCookie(intentData);
				cookie.setHttpOnly(true);
				cookie.setSecure(true);
				response.addCookie(cookie);
			}

			if (intentData != null) {
				request.setAttribute(SCAConsentHelperConstants.INTENT_DATA, intentData);
                ValidationUtility.isValidUUID(intentData.getCorrelationId());
				requestHeaderAttributes.setCorrelationId(intentData.getCorrelationId());
				requestHeaderAttributes.setIntentId(intentData.getIntentId());
				requestHeaderAttributes.setTppCID(intentData.getClientId());
				requestHeaderAttributes.setPsuId(intentData.getUserId());
				requestHeaderAttributes.setChannelId(intentData.getChannelId());				
				requestHeaderAttributes.setSelfUrl(
						requestUri.endsWith("/") ? requestUri.substring(0, requestUri.length() - 1) : requestUri);
				requestHeaderAttributes.setMethodType(request.getMethod());
			}
		} catch (PSD2Exception pe) {
			LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":{}}",
					"com.capgemini.psd2.scaconsenthelper.filters.ConsentCookieHandlerFilter.doFilterInternal()",
					loggerUtils.populateLoggerData("doFilterInternal"), pe.getErrorInfo());

			if (LOG.isDebugEnabled()) {
				LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":\"{}\"}",
						"com.capgemini.psd2.scaconsenthelper.filters.ConsentCookieHandlerFilter.doFilterInternal()",
						loggerUtils.populateLoggerData("doFilterInternal"), pe.getStackTrace(), pe);
			}
			request.setAttribute(SCAConsentHelperConstants.UNAUTHORIZED_ERROR, Boolean.TRUE);
			throw pe;
		}
	}
	
	
	public void populateSecurityHeaders(HttpServletResponse response){
		SCAConsentHelper.populateSecurityHeaders(response);
	}
}