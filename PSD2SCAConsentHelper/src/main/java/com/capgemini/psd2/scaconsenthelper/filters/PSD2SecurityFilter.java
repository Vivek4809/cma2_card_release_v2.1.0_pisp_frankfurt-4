package com.capgemini.psd2.scaconsenthelper.filters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.product.db.resources.locator.MultiTenancyRequestBean;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.SecurityRequestAttributes;
import com.capgemini.psd2.security.exceptions.PSD2SecurityException;
import com.capgemini.psd2.security.exceptions.SCAConsentErrorCodeEnum;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.ValidationUtility;

public class PSD2SecurityFilter extends OncePerRequestFilter {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(PSD2SecurityFilter.class);

	/** The logger utils. */
	@Autowired
	private LoggerUtils loggerUtils;

	/*
	 * @Value("${app.saas.security.tokenSigningKey}") private String
	 * tokenSigningKey;
	 */
	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;

	@Autowired
	private MultiTenancyRequestBean multiTenantReqBean;

	// @Autowired
	/*
	 * @Qualifier("tppValidationImpl") private TPPValidation tppValidation;
	 */
	@Autowired
	private SecurityRequestAttributes securityRequestAttributes;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		request.getServletPath();
		LOG.info(
				"{\"Enter\":\"{}\",\"preCorrelationId\":\" \",\"timestamp\":\"{}\",\"remoteAddress\":\"{}\",\"hostName\":\"{}\",\"correlationId\":\"{}\"}",
				"com.capgemini.psd2.security.filter.PSD2SecurityFilter.doFilterInternal()",
				DateUtilites.generateCurrentTimeStamp(), request.getRemoteAddr(), request.getRemoteHost(), null);
		try {

			String currentCorrId = requestHeaderAttributes.getCorrelationId();

			if (currentCorrId == null || currentCorrId.isEmpty()) {
				currentCorrId = request.getParameter(PSD2Constants.CO_RELATION_ID);
				ValidationUtility.isValidUUID(currentCorrId);
				requestHeaderAttributes.setCorrelationId(currentCorrId);
			}
			if ((request.getAttribute(SCAConsentHelperConstants.EXCEPTION) == null)
					&& (currentCorrId == null || currentCorrId.isEmpty())) {
				throw PSD2SecurityException.populatePSD2SecurityException("CorrelationId not found.",
						SCAConsentErrorCodeEnum.VALIDATION_ERROR);
			} else {
				LOG.info(
						"{\"Enter\":\"{}\",\"preCorrelationId\":\"Found\",\"timestamp\":\"{}\",\"remoteAddress\":\"{}\",\"hostName\":\"{}\",\"correlationId\":\"{}\"}",
						"com.capgemini.psd2.security.filter.PSD2SecurityFilter.doFilterInternal()",
						DateUtilites.generateCurrentTimeStamp(), request.getRemoteAddr(), request.getRemoteHost(),
						currentCorrId);
			}
			Map<String, String[]> paramsMap = request.getParameterMap();
			securityRequestAttributes.setParamMap(paramsMap);

			String tenantId = request.getHeader(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME);

			if (tenantId == null) {
				/*
				 * Case for JWKS end point triggered from PF, which sets
				 * tenantid in req param.
				 */
				tenantId = request.getParameter("tenantId");
			}
			requestHeaderAttributes.setTenantId(tenantId);
			multiTenantReqBean.setTenantId(tenantId);
			//

			requestHeaderAttributes.setTenantId(request.getHeader("tenantid"));
			LOG.info("{\"Exit\":\"{}\",\"timestamp\":\"{}\",\"correlationId\": \"{}\"}",
					"com.capgemini.psd2.security.filter.PSD2SecurityFilter.doFilterInternal()",
					DateUtilites.generateCurrentTimeStamp(), null);

			filterChain.doFilter(request, response);
		} catch (PSD2Exception e) {
			LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":{}}",
					"com.capgemini.psd2.logger.PSD2SecurityFilter.doFilterInternal()",
					loggerUtils.populateLoggerData("doFilterInternal"), e.getErrorInfo());
			if (LOG.isDebugEnabled()) {
				LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":\"{}\"}",
						"com.capgemini.psd2.logger.PSD2SecurityFilter.doFilterInternal()",
						loggerUtils.populateLoggerData("doFilterInternal"), e.getStackTrace(), e);
			}
			response.setContentType("application/json");
			response.setStatus(Integer.parseInt(e.getErrorInfo().getStatusCode()));
			request.setAttribute(SCAConsentHelperConstants.EXCEPTION, e.getErrorInfo());
			request.getRequestDispatcher("/errors").forward(request, response);
		} catch (Exception e) {
			PSD2Exception psd2Exception = PSD2Exception.populatePSD2Exception(e.getMessage(),
					ErrorCodeEnum.TECHNICAL_ERROR);
			LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":{}}",
					"com.capgemini.psd2.logger.PSD2SecurityFilter.doFilterInternal()",
					loggerUtils.populateLoggerData("doFilterInternal"), psd2Exception.getErrorInfo());
			if (LOG.isDebugEnabled()) {
				LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":\"{}\"}",
						"com.capgemini.psd2.logger.PSD2SecurityFilter.doFilterInternal()",
						loggerUtils.populateLoggerData("doFilterInternal"), e.getStackTrace(), e);
			}
			response.setContentType("application/json");
			response.setStatus(Integer.parseInt(psd2Exception.getErrorInfo().getStatusCode()));
			request.setAttribute(SCAConsentHelperConstants.EXCEPTION, psd2Exception.getErrorInfo());
			request.getRequestDispatcher("/errors").forward(request, response);
		}

	}

	/*
	 * private String populateResumePath(HttpServletRequest request){ String
	 * completeResumePath = null; String allowInteraction = null; String scope =
	 * null; String connectionId = null; String response_type = null; String
	 * state = null; String reauth = null; String nonce = null; String ref =
	 * null; String correlationId = null;
	 * 
	 * String resumePath = request.getParameter("resumePath"); if(resumePath ==
	 * null || resumePath.isEmpty()) { // throw } MultiValueMap<String,String>
	 * map =
	 * UriComponentsBuilder.fromHttpUrl(resumePath).build().getQueryParams();
	 * 
	 * if(map == null || map.isEmpty()) { allowInteraction =
	 * request.getParameter("allowInteraction"); scope =
	 * request.getParameter("scope"); connectionId =
	 * request.getParameter("connectionId"); state =
	 * request.getParameter("state"); ref = request.getParameter("REF"); nonce =
	 * request.getParameter("nonce"); response_type =
	 * request.getParameter("response_type"); reauth =
	 * request.getParameter("reauth"); correlationId =
	 * request.getParameter("allowInteraction");
	 * 
	 * map = new LinkedMultiValueMap<String,String>();
	 * 
	 * map.put("allowInteraction", Arrays.asList(allowInteraction));
	 * map.put("scope", Arrays.asList(scope)); map.put("connectionId",
	 * Arrays.asList(connectionId)); map.put("response_type",
	 * Arrays.asList(response_type)); map.put("state", Arrays.asList(state));
	 * map.put("REF", Arrays.asList(ref)); map.put("nonce",
	 * Arrays.asList(nonce)); map.put("reauth", Arrays.asList(reauth));
	 * 
	 * 
	 * completeResumePath =
	 * UriComponentsBuilder.newInstance().host(resumePath).queryParams(map).
	 * build().toUriString(); } else { completeResumePath =
	 * UriComponentsBuilder.fromHttpUrl(resumePath).build().toUriString(); }
	 * return completeResumePath; }
	 */

	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
		List<String> excludeUrlPatterns = new ArrayList<>();
		excludeUrlPatterns.add("/**/sca-ui/**");
		excludeUrlPatterns.add("/**/consent-ui/**");
		excludeUrlPatterns.add("/**/build/**");
		excludeUrlPatterns.add("/ui/staticContentGet");
		excludeUrlPatterns.add("/ui/staticContentGet");
		excludeUrlPatterns.add("/**/fonts/**");
		excludeUrlPatterns.add("/**/img/**");
		excludeUrlPatterns.add("/AISP.**");
		excludeUrlPatterns.add("/ext-libs/fraudnet/**");

		AntPathMatcher matcher = new AntPathMatcher();
		return excludeUrlPatterns.stream().anyMatch(p -> matcher.match(p, request.getServletPath()));
	}
}
