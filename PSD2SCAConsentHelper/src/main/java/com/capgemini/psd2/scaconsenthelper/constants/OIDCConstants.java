package com.capgemini.psd2.scaconsenthelper.constants;

//import com.capgemini.psd2.logger.PSD2Constants;

public interface OIDCConstants {
	
	public final static String REQUEST= "request";
	public final static String SCOPE ="scope";
	public final static String OPENID = "openid";
	public final static String CLAIMS = "claims";
	public final static String USER_INFO = "userinfo";
	public final static String OPENBANKING_INTENT_ID = "openbanking_intent_id";
	public final static String VALUE="value";
	public final static String ID_TOKEN = "id_token";
	public final static String SCA_ACR = "urn:openbanking:psd2:sca";
	public final static String USERNAME= "username";
	public final static String CHANNEL_ID = "channel_id";
	public final static String CLIENT_ID = "client_id";
	//public final static String CORRELATION_ID = PSD2Constants.CORRELATION_ID;
}
