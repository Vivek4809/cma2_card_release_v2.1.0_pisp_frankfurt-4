package com.capgemini.psd2.pisp.domestic.payments.routing.test.adapter.routing;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.pisp.adapter.DomesticPaymentsAdapter;
import com.capgemini.psd2.pisp.domestic.payments.routing.adapter.impl.DomesticPaymentsRoutingAdapterImpl;
import com.capgemini.psd2.pisp.domestic.payments.routing.adapter.routing.DomesticPaymentsCoreSystemAdapterFactory;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticPaymentsCoreSystemAdapterFactoryTest {
	
	@InjectMocks
	DomesticPaymentsCoreSystemAdapterFactory paymentSubmissionCoreSystemAdapterFactory;
	
	@Mock
	ApplicationContext applicationContext;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testGetPaymentSubmissionExecutionAdapterInstance() {
		DomesticPaymentsRoutingAdapterImpl adapterImpl = new DomesticPaymentsRoutingAdapterImpl();
		when(applicationContext.getBean(anyString())).thenReturn(adapterImpl);
		
		DomesticPaymentsAdapter adapter = paymentSubmissionCoreSystemAdapterFactory.getDomesticPaymentsAdapterInstance("test");
		assertNotNull(adapter);
	}
	
	/*@Test
	public void testGetPaymentAuthorizationValidationAdapterInstance() {
		PaymentAuthorizationValidationRoutingAdapterImpl adapterImpl = new PaymentAuthorizationValidationRoutingAdapterImpl();
		when(applicationContext.getBean(anyString())).thenReturn(adapterImpl);
		PaymentAuthorizationValidationAdapter adapter = paymentSubmissionCoreSystemAdapterFactory.getPaymentAuthorizationValidationAdapterInstance("test");
		assertNotNull(adapter);
	}*/
	
	@Test
	public void testSetApplicationContext() {
		paymentSubmissionCoreSystemAdapterFactory.setApplicationContext(new ApplicationContextMock());
	}
	
}
