package com.capgemini.psd2.pisp.domestic.payments.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentDomesticSubmitPOST201Response;
import com.capgemini.psd2.pisp.domestic.payments.service.DomesticPaymentsService;

@RestController
public class DomesticPaymentsController {

	@Autowired
	private DomesticPaymentsService submissionService;

	@Value("${app.responseErrorMessageEnabled}")
	private String responseErrorMessageEnabled;

	@RequestMapping(value = "/domestic-payments", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<PaymentDomesticSubmitPOST201Response> createDomesticPaymentSubmissionResource(
			@RequestBody CustomDPaymentsPOSTRequest domesticPaymentsRequest) {

		try {
			CustomDPaymentsPOSTRequest customDomesticPaymentsRequest = new CustomDPaymentsPOSTRequest();
			customDomesticPaymentsRequest.setData(domesticPaymentsRequest.getData());
			customDomesticPaymentsRequest.setRisk(domesticPaymentsRequest.getRisk());

			PaymentDomesticSubmitPOST201Response submissionResponse = submissionService
					.createDomesticPaymentsResource(customDomesticPaymentsRequest);

			return new ResponseEntity<>(submissionResponse, HttpStatus.CREATED);

		} catch (PSD2Exception psd2Exception) {

			if (Boolean.valueOf(responseErrorMessageEnabled))
				throw psd2Exception;

			return new ResponseEntity<>(
					HttpStatus.valueOf(Integer.parseInt(psd2Exception.getErrorInfo().getStatusCode())));

		}
	}

	@RequestMapping(value = "/domestic-payments/{DomesticPaymentId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<PaymentDomesticSubmitPOST201Response> retrieveDomesticPaymentPaymentSubmissionResource(
			@PathVariable("DomesticPaymentId") String submissionId) {

		try {
			PaymentDomesticSubmitPOST201Response submissionResponse = submissionService
					.retrieveDomesticPaymentsResource(submissionId);
			return new ResponseEntity<>(submissionResponse, HttpStatus.OK);

		} catch (PSD2Exception psd2Exception) {

			if (Boolean.valueOf(responseErrorMessageEnabled))
				throw psd2Exception;

			return new ResponseEntity<>(
					HttpStatus.valueOf(Integer.parseInt(psd2Exception.getErrorInfo().getStatusCode())));
		}
	}
}
