package com.capgemini.psd2.pisp.dspc.service;

import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTResponse;

public interface DSPConsentsService {

	public CustomDSPConsentsPOSTResponse createDomesticScheduledPaymentConsentsResource(
			CustomDSPConsentsPOSTRequest scheduledPaymentRequest);	
	 
	public CustomDSPConsentsPOSTResponse retrieveDomesticScheduledPaymentConsentsResource(
			String consentId);	
}
