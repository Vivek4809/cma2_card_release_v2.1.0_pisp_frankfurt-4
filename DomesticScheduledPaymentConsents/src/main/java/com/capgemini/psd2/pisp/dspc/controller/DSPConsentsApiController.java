package com.capgemini.psd2.pisp.dspc.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBWriteDomesticScheduledConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDomesticScheduledConsentResponse1;
import com.capgemini.psd2.pisp.dspc.service.DSPConsentsService;
import com.fasterxml.jackson.databind.ObjectMapper;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-12-03T16:46:14.584+05:30")

@Controller
public class DSPConsentsApiController implements DSPConsentsApi {

	@Autowired
	private DSPConsentsService service;

	@Value("${app.responseErrorMessageEnabled}")
	private String responseErrorMessageEnabled;
	
	private final ObjectMapper objectMapper;

	private final HttpServletRequest request;

	@org.springframework.beans.factory.annotation.Autowired
	public DSPConsentsApiController(ObjectMapper objectMapper, HttpServletRequest request) {
		this.objectMapper = objectMapper;
		this.request = request;
	}

	@Override
	public Optional<ObjectMapper> getObjectMapper() {
		return Optional.ofNullable(objectMapper);
	}

	@Override
	public Optional<HttpServletRequest> getRequest() {
		return Optional.ofNullable(request);
	}

	@Override
	public ResponseEntity<OBWriteDomesticScheduledConsentResponse1> createDomesticScheduledPaymentConsents(@RequestBody OBWriteDomesticScheduledConsent1 obWriteDomesticScheduledConsent1Param, String xFapiFinancialId,
			String authorization, String xIdempotencyKey, String xJwsSignature, String xFapiCustomerLastLoggedTime,
			String xFapiCustomerIpAddress, String xFapiInteractionId, String xCustomerUserAgent) {
		if (getObjectMapper().isPresent()) {

			CustomDSPConsentsPOSTRequest customRequest = new CustomDSPConsentsPOSTRequest();
			customRequest.setData(obWriteDomesticScheduledConsent1Param.getData());
			customRequest.setRisk(obWriteDomesticScheduledConsent1Param.getRisk());
			CustomDSPConsentsPOSTResponse customResponse = service
					.createDomesticScheduledPaymentConsentsResource(customRequest);
			OBWriteDomesticScheduledConsentResponse1 tppResponse = new OBWriteDomesticScheduledConsentResponse1();
			tppResponse.setData(customResponse.getData());
			tppResponse.setLinks(customResponse.getLinks());
			tppResponse.setMeta(customResponse.getMeta());
			tppResponse.setRisk(customResponse.getRisk());
			return new ResponseEntity<>(tppResponse, HttpStatus.CREATED);
		}

		throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT,
				ErrorMapKeys.RESOURCE_INVALIDFORMAT));
	}

	@Override
	public ResponseEntity<OBWriteDomesticScheduledConsentResponse1> getDomesticScheduledPaymentConsentsConsentId(
			@PathVariable("ConsentId") String consentId, String xFapiFinancialId, String authorization, String xFapiCustomerLastLoggedTime,
			String xFapiCustomerIpAddress, String xFapiInteractionId, String xCustomerUserAgent) {
		if (getObjectMapper().isPresent()) {

			CustomDSPConsentsPOSTResponse customResponse = service
					.retrieveDomesticScheduledPaymentConsentsResource(consentId);

			OBWriteDomesticScheduledConsentResponse1 tppResponse = new OBWriteDomesticScheduledConsentResponse1();
			tppResponse.setData(customResponse.getData());
			tppResponse.setLinks(customResponse.getLinks());
			tppResponse.setMeta(customResponse.getMeta());
			tppResponse.setRisk(customResponse.getRisk());

			return new ResponseEntity<>(tppResponse, HttpStatus.OK);
		}
		throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT,
				ErrorMapKeys.RESOURCE_INVALIDFORMAT));
	}

}
