package com.capgemini.psd2.adapter.security.test;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.exceptions.security.AdapterAuthenticationException;
import com.capgemini.psd2.adapter.exceptions.security.SecurityAdapterErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorInfo;

@RunWith(SpringJUnit4ClassRunner.class)
public class AdapterAuthenticationExceptionTest {

	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	
	@Test
	public void testPopulateAuthenticationFailedException(){
		AdapterAuthenticationException adapterAuthenticationException = AdapterAuthenticationException.populateAuthenticationFailedException("test", SecurityAdapterErrorCodeEnum.BAD_REQUEST);
		assertNotNull(adapterAuthenticationException);
	}
	
	@Test
	public void testPopulateAuthenticationFailedException2(){
		ErrorInfo errorInfo = new ErrorInfo();
		errorInfo.setDetailErrorMessage("test");
		AdapterAuthenticationException adapterAuthenticationException = AdapterAuthenticationException.populateAuthenticationFailedException(errorInfo);
		assertNotNull(adapterAuthenticationException);
	}
}
