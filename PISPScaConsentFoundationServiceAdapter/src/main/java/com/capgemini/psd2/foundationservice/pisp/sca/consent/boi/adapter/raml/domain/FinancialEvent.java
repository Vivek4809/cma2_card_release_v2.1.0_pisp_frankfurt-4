/*
 * Payment Process API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain;

import java.util.Objects;

import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.Currency;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.FinancialEventAmount;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.FinancialEventStatusCode;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.IOException;

/**
 * FinancialEvent
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-06-12T15:45:10.334+05:30")
public class FinancialEvent {
  @SerializedName("financialEventAmount")
  private FinancialEventAmount financialEventAmount = null;

  @SerializedName("currency")
  private Currency currency = null;

  @SerializedName("financialEventSubtype")
  private String financialEventSubtype = null;

  @SerializedName("financialEventStatusCode")
  private FinancialEventStatusCode financialEventStatusCode = null;

  public FinancialEvent financialEventAmount(FinancialEventAmount financialEventAmount) {
    this.financialEventAmount = financialEventAmount;
    return this;
  }

   /**
   * Get financialEventAmount
   * @return financialEventAmount
  **/
  @ApiModelProperty(required = true, value = "")
  public FinancialEventAmount getFinancialEventAmount() {
    return financialEventAmount;
  }

  public void setFinancialEventAmount(FinancialEventAmount financialEventAmount) {
    this.financialEventAmount = financialEventAmount;
  }

  public FinancialEvent currency(Currency currency) {
    this.currency = currency;
    return this;
  }

   /**
   * Get currency
   * @return currency
  **/
  @ApiModelProperty(required = true, value = "")
  public Currency getCurrency() {
    return currency;
  }

  public void setCurrency(Currency currency) {
    this.currency = currency;
  }

  public FinancialEvent financialEventSubtype(String financialEventSubtype) {
    this.financialEventSubtype = financialEventSubtype;
    return this;
  }

   /**
   * The Financial Event Subtype identifies the purpose of the Financial Event e.g. Lodgement; Withdrawal; Sale; Refund; Interest Capitalisation; Principal Repayment; Fee Charge;  Tax Charge or Mixed event.
   * @return financialEventSubtype
  **/
  @ApiModelProperty(required = true, value = "The Financial Event Subtype identifies the purpose of the Financial Event e.g. Lodgement; Withdrawal; Sale; Refund; Interest Capitalisation; Principal Repayment; Fee Charge;  Tax Charge or Mixed event.")
  public String getFinancialEventSubtype() {
    return financialEventSubtype;
  }

  public void setFinancialEventSubtype(String financialEventSubtype) {
    this.financialEventSubtype = financialEventSubtype;
  }

  public FinancialEvent financialEventStatusCode(FinancialEventStatusCode financialEventStatusCode) {
    this.financialEventStatusCode = financialEventStatusCode;
    return this;
  }

   /**
   * Get financialEventStatusCode
   * @return financialEventStatusCode
  **/
  @ApiModelProperty(required = true, value = "")
  public FinancialEventStatusCode getFinancialEventStatusCode() {
    return financialEventStatusCode;
  }

  public void setFinancialEventStatusCode(FinancialEventStatusCode financialEventStatusCode) {
    this.financialEventStatusCode = financialEventStatusCode;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FinancialEvent financialEvent = (FinancialEvent) o;
    return Objects.equals(this.financialEventAmount, financialEvent.financialEventAmount) &&
        Objects.equals(this.currency, financialEvent.currency) &&
        Objects.equals(this.financialEventSubtype, financialEvent.financialEventSubtype) &&
        Objects.equals(this.financialEventStatusCode, financialEvent.financialEventStatusCode);
  }

  @Override
  public int hashCode() {
    return Objects.hash(financialEventAmount, currency, financialEventSubtype, financialEventStatusCode);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class FinancialEvent {\n");
    
    sb.append("    financialEventAmount: ").append(toIndentedString(financialEventAmount)).append("\n");
    sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
    sb.append("    financialEventSubtype: ").append(toIndentedString(financialEventSubtype)).append("\n");
    sb.append("    financialEventStatusCode: ").append(toIndentedString(financialEventStatusCode)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

