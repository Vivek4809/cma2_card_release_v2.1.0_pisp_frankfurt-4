/*package com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.fraudnet.domain.DecisionType;
import com.capgemini.psd2.adapter.fraudnet.domain.FraudServiceResponse;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.Amount;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.AuthorisationType;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.AuthorisingPartyAccount;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.ChargeBearer;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.Currency;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.FinancialEventAmount;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.Frequency;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.PaymentInstructionCharge;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.PaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.PaymentTransaction;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.PaymentType;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.ProposalStatus;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.ProposingPartyAccount;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.ScheduledPaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.transformer.PispScaConsentFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomFraudSystemPaymentData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;
import com.capgemini.psd2.pisp.stage.domain.CustomPreAuthorizeAdditionalInfo;
import com.capgemini.psd2.validator.impl.PSD2ValidatorImpl;



@RunWith(SpringJUnit4ClassRunner.class)
public class PispScaConsentFoundationServiceTransformerTest {

	@Mock
	private PSD2ValidatorImpl psd2Validator;

	@InjectMocks
	private PispScaConsentFoundationServiceTransformer pispScaConsentFoundationServiceTransformer;

	@Mock
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	public static final String TENANT_ID = "tenant_id";
	@Test
	public void contextLoads() {
	}

	@Test
	public void testtransformPispScaConsentResponseScheduled() {

		ScheduledPaymentInstructionProposal paymentIns = new ScheduledPaymentInstructionProposal();
		Map<String, String> params = new HashMap<>();

		AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();
		authorisingPartyAccount.setAccountIdentification("eiei");
		authorisingPartyAccount.setAccountName("psd2");
		authorisingPartyAccount.setAccountNumber("1234");
		authorisingPartyAccount.setSchemeName("abs");
		authorisingPartyAccount.setSecondaryIdentification("wer");
		paymentIns.setAuthorisingPartyAccount(authorisingPartyAccount);

		ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();

		proposingPartyAccount.setAccountIdentification("wiwiw");
		proposingPartyAccount.setAccountName("abc");
		

		proposingPartyAccount.setAccountNumber("1234");
		

		proposingPartyAccount.setSchemeName("qoqo");

		proposingPartyAccount.setSecondaryIdentification("1234");
		Currency transactionCurrency = new Currency();
		transactionCurrency.setCurrencyName("ei");
		transactionCurrency.setIsoAlphaCode("dk");
		paymentIns.setTransactionCurrency(transactionCurrency);
		paymentIns.setRequestedExecutionDateTime("2018-10-05T15:15:13+00:00");
		paymentIns.setProposingPartyAccount(proposingPartyAccount);
		PaymentInstructionCharge paymentInstructionCharge = new PaymentInstructionCharge();
		OBCharge1 oBCharge1 = new OBCharge1();
		OBCharge1Amount oBCharge1Amount = new OBCharge1Amount();
		oBCharge1Amount.setAmount("12");
		oBCharge1Amount.setCurrency("12");

		oBCharge1.amount(oBCharge1Amount);
		Amount amount = new Amount();
		amount.setTransactionCurrency(12.50);
		amount.setAccountCurrency(34.67);
		amount.setGroupReportingCurrency(45.67);
		amount.setLocalReportingCurrency(21.3);
		paymentInstructionCharge.setAmount(amount);
		paymentInstructionCharge.setType("sjsj");

		paymentInstructionCharge.setChargeBearer(ChargeBearer.BorneByCreditor);
		Currency currency = new Currency();
		currency.setCurrencyName("qwr");
		currency.setIsoAlphaCode("qwe");

		paymentInstructionCharge.setCurrency(currency);
		paymentIns.setAuthorisingPartyReference("sjsj");
		paymentIns.setAuthorisingPartyUnstructuredReference("djdjdjj");
		FinancialEventAmount financialEventAmount = new FinancialEventAmount();
		financialEventAmount.setAccountCurrency(12.34);
		financialEventAmount.setGroupReportingCurrency(23.45);
		financialEventAmount.setLocalReportingCurrency(12.34);
		financialEventAmount.setTransactionCurrency(20.0);
		paymentIns.setFinancialEventAmount(financialEventAmount);

		List<PaymentInstructionCharge> charges = new ArrayList<PaymentInstructionCharge>();
		charges.add(paymentInstructionCharge);
		paymentIns.setCharges(charges);
		pispScaConsentFoundationServiceTransformer.transformPispScaConsentResponseScheduled(paymentIns);

	}

	@Test
	public void testPispScaConsentFoundationServiceTransformer() {

		CustomConsentAppViewData customConsentAppViewData = new CustomConsentAppViewData();
		PaymentInstructionProposal paymentIns = new PaymentInstructionProposal();
		Map<String, String> params = new HashMap<>();
		AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();
		paymentIns.setPaymentType(PaymentType.Domestic);
		paymentIns.setAuthorisationDatetime("4747");
		paymentIns.authorisingPartyReference("gjgj");
		paymentIns.setAuthorisingPartyUnstructuredReference("tikiu");
		paymentIns.setAuthorisationType(AuthorisationType.Any);

		authorisingPartyAccount.setAccountName("sai");
		authorisingPartyAccount.setAccountNumber("464");
		authorisingPartyAccount.setSchemeName("tfu");
		authorisingPartyAccount.setSecondaryIdentification("ffj");
		authorisingPartyAccount.setAccountIdentification("sdxsdx5412152");
		paymentIns.setAuthorisingPartyAccount(authorisingPartyAccount);

		ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();
		proposingPartyAccount.setAccountName("namu");
		proposingPartyAccount.setAccountNumber("5687");
		proposingPartyAccount.setSchemeName("fuyjg");
		proposingPartyAccount.setSecondaryIdentification("djhd");
		proposingPartyAccount.setAccountIdentification("dcdc454521");
		paymentIns.setProposingPartyAccount(proposingPartyAccount);

		FinancialEventAmount financialEventAmount = new FinancialEventAmount();
		financialEventAmount.setGroupReportingCurrency(67646.00);
		financialEventAmount.setLocalReportingCurrency(435.00);
		financialEventAmount.setTransactionCurrency(131.00);
		paymentIns.setFinancialEventAmount(financialEventAmount);

		Currency transactionCurrency = new Currency();
		transactionCurrency.setCurrencyName("fguy");
		transactionCurrency.setIsoAlphaCode("gweui");
		paymentIns.setTransactionCurrency(transactionCurrency);

		List<PaymentInstructionCharge> charges = new ArrayList<>();
		PaymentInstructionCharge paymentInstructionCharge = new PaymentInstructionCharge();
		Amount amount = new Amount();
		amount.setGroupReportingCurrency(69.00);
		amount.setLocalReportingCurrency(68979.00);
		amount.setTransactionCurrency(6896.00);
		paymentInstructionCharge.setAmount(amount);
		Currency currency = new Currency();
		currency.setCurrencyName("yiu");
		currency.setIsoAlphaCode("68");
		paymentInstructionCharge.setCurrency(currency);
		paymentInstructionCharge.setType("utguy");
		paymentInstructionCharge.setChargeBearer(ChargeBearer.BorneByCreditor);
		charges.add(paymentInstructionCharge);
		paymentIns.setCharges(charges);

		pispScaConsentFoundationServiceTransformer.transformPispScaConsentResponse(paymentIns);
	}

	@Test
	public void testPispScaConsentFoundationServiceTransformerforNull() {

		PaymentInstructionProposal paymentIns = new PaymentInstructionProposal();
		Map<String, String> params = new HashMap<>();
		pispScaConsentFoundationServiceTransformer.transformPispScaConsentResponse(paymentIns);
	}

	@Test
	public void testPispScaConsentFoundationServiceTransformerforNull1() {

		CustomConsentAppViewData customConsentAppViewData = new CustomConsentAppViewData();
		PaymentInstructionProposal paymentIns = new PaymentInstructionProposal();
		Map<String, String> params = new HashMap<>();
		AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();
		paymentIns.setPaymentType(PaymentType.Domestic);
		paymentIns.setAuthorisationDatetime("4747");
		paymentIns.authorisingPartyReference("gjgj");
		paymentIns.setAuthorisingPartyUnstructuredReference("tikiu");
		paymentIns.setAuthorisationType(AuthorisationType.Any);

		authorisingPartyAccount.setAccountName("sai");
		authorisingPartyAccount.setAccountNumber("464");
		authorisingPartyAccount.setSchemeName("tfu");
		authorisingPartyAccount.setSecondaryIdentification(null);
		paymentIns.setAuthorisingPartyAccount(authorisingPartyAccount);

		ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();
		proposingPartyAccount.setAccountName("namu");
		proposingPartyAccount.setAccountNumber("5687");
		proposingPartyAccount.setSchemeName("fuyjg");
		proposingPartyAccount.setSecondaryIdentification(null);
		paymentIns.setProposingPartyAccount(proposingPartyAccount);

		paymentIns.setFinancialEventAmount(null);
		paymentIns.setTransactionCurrency(null);

		List<PaymentInstructionCharge> charges = new ArrayList<>();
		PaymentInstructionCharge paymentInstructionCharge = new PaymentInstructionCharge();
		Amount amount = new Amount();
		amount.setGroupReportingCurrency(69.00);
		amount.setLocalReportingCurrency(68979.00);
		amount.setTransactionCurrency(6896.00);
		paymentInstructionCharge.setAmount(amount);
		Currency currency = new Currency();
		currency.setCurrencyName("yiu");
		currency.setIsoAlphaCode("68");
		paymentInstructionCharge.setCurrency(currency);
		paymentInstructionCharge.setType("utguy");
		paymentInstructionCharge.setChargeBearer(ChargeBearer.BorneByCreditor);
		charges.add(paymentInstructionCharge);
		paymentIns.setCharges(charges);

		pispScaConsentFoundationServiceTransformer.transformPispScaConsentResponse(paymentIns);
	}

	@Test
	public void testPispScaConsentFoundationServiceTransformerForPut() {
		CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
		PaymentInstructionProposal paymentInstructionProposal = new PaymentInstructionProposal();
		Map<String, String> params = new HashMap<>();
		params.put(TENANT_ID, "BOIUK");
		params.put("X-BOI-CHANNEL", "B365");
		ProposingPartyAccount account = new ProposingPartyAccount();
		updateData.setFraudScoreUpdated(true);
		FraudServiceResponse fraudsystem = new FraudServiceResponse();
		fraudsystem.setDecisionType(DecisionType.APPROVE);
		fraudsystem.setDeviceIdentifier("15689");
		updateData.setFraudScore(fraudsystem);
		updateData.setSetupStatus("AcceptedCustomerProfile");
		updateData.setSetupStatusUpdated(true);
		OBCashAccountDebtor3 debtorDetails = new OBCashAccountDebtor3();
		debtorDetails.setIdentification("kfgjkg");
		debtorDetails.setName("fjk");
		debtorDetails.setSchemeName("fg");
		debtorDetails.setSecondaryIdentification("ry");
		updateData.setDebtorDetails(debtorDetails);
		updateData.setDebtorDetailsUpdated(true);
		account.setAccountName("ertye");
		account.setAccountNumber("35");
		account.setSchemeName("drthq2");
		account.setSecondaryIdentification("gfje");
		paymentInstructionProposal.setProposingPartyAccount(account);
		pispScaConsentFoundationServiceTransformer.transformPispScaConsentResponseForUpdate(updateData,
				paymentInstructionProposal,params);
	}

	@Test
	public void testPispScaConsentFoundationServiceTransformerForPut1() {
		CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
		PaymentInstructionProposal paymentInstructionProposal = new PaymentInstructionProposal();
		Map<String, String> params = new HashMap<>();
		params.put(TENANT_ID, "BOIROI");
		params.put("X-BOI-CHANNEL", "BOL");
		ProposingPartyAccount account = new ProposingPartyAccount();
		updateData.setFraudScoreUpdated(true);
		FraudServiceResponse fraudsystem = new FraudServiceResponse();
		fraudsystem.setDecisionType(DecisionType.INVESTIGATE_APPROVE);
		fraudsystem.setDeviceIdentifier("15689");
		updateData.setFraudScore(fraudsystem);
		updateData.setSetupStatus("fukg");
		updateData.setSetupStatusUpdated(true);
		OBCashAccountDebtor3 debtorDetails = new OBCashAccountDebtor3();
		debtorDetails.setIdentification("kfgjkg");
		debtorDetails.setName("fjk");
		debtorDetails.setSchemeName("fg");
		debtorDetails.setSecondaryIdentification("ry");
		updateData.setDebtorDetails(debtorDetails);
		updateData.setDebtorDetailsUpdated(true);
		account.setAccountName("ertye");
		account.setAccountNumber("35");
		account.setSchemeName("drthq2");
		account.setSecondaryIdentification("gfje");
		paymentInstructionProposal.setProposingPartyAccount(account);
		pispScaConsentFoundationServiceTransformer.transformPispScaConsentResponseForUpdate(updateData,
				paymentInstructionProposal,params);
	}
	@Test
	public void testPispScaConsentFoundationServiceTransformerForPut2() {
		CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
		PaymentInstructionProposal paymentInstructionProposal = new PaymentInstructionProposal();
		Map<String, String> params = new HashMap<>();
		params.put(TENANT_ID, "BOIUK");
		ProposingPartyAccount account = new ProposingPartyAccount();
		updateData.setFraudScoreUpdated(true);
		FraudServiceResponse fraudsystem = new FraudServiceResponse();
		fraudsystem.setDecisionType(DecisionType.INVESTIGATE_CANCEL);
		fraudsystem.setDeviceIdentifier("15689");
		updateData.setFraudScore(fraudsystem);
		updateData.setSetupStatus("fukg");
		updateData.setSetupStatusUpdated(true);
		OBCashAccountDebtor3 debtorDetails = new OBCashAccountDebtor3();
		debtorDetails.setIdentification("kfgjkg");
		debtorDetails.setName("fjk");
		debtorDetails.setSchemeName("fg");
		debtorDetails.setSecondaryIdentification("ry");
		updateData.setDebtorDetails(debtorDetails);
		updateData.setDebtorDetailsUpdated(true);
		account.setAccountName("ertye");
		account.setAccountNumber("35");
		account.setSchemeName("drthq2");
		account.setSecondaryIdentification("gfje");
		paymentInstructionProposal.setProposingPartyAccount(account);
		pispScaConsentFoundationServiceTransformer.transformPispScaConsentResponseForUpdate(updateData,
				paymentInstructionProposal,params);
	}
	@Test
	public void testPispScaConsentFoundationServiceTransformerForPut3() {
		CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
		PaymentInstructionProposal paymentInstructionProposal = new PaymentInstructionProposal();
		Map<String, String> params = new HashMap<>();
		params.put(TENANT_ID, "BOIUK");
		ProposingPartyAccount account = new ProposingPartyAccount();
		updateData.setFraudScoreUpdated(true);
		FraudServiceResponse fraudsystem = new FraudServiceResponse();
		fraudsystem.setDecisionType(DecisionType.INVESTIGATE_WAIT);
		fraudsystem.setDeviceIdentifier("15689");
		updateData.setFraudScore(fraudsystem);
		updateData.setSetupStatus("fukg");
		updateData.setSetupStatusUpdated(true);
		OBCashAccountDebtor3 debtorDetails = new OBCashAccountDebtor3();
		debtorDetails.setIdentification("kfgjkg");
		debtorDetails.setName("fjk");
		debtorDetails.setSchemeName("fg");
		debtorDetails.setSecondaryIdentification("ry");
		updateData.setDebtorDetails(debtorDetails);
		updateData.setDebtorDetailsUpdated(true);
		account.setAccountName("ertye");
		account.setAccountNumber("35");
		account.setSchemeName("drthq2");
		account.setSecondaryIdentification("gfje");
		paymentInstructionProposal.setProposingPartyAccount(account);
		pispScaConsentFoundationServiceTransformer.transformPispScaConsentResponseForUpdate(updateData,
				paymentInstructionProposal,params);
	}
	@Test
	public void testPispScaConsentFoundationServiceTransformerForPut4() {
		CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
		PaymentInstructionProposal paymentInstructionProposal = new PaymentInstructionProposal();
		Map<String, String> params = new HashMap<>();
		params.put(TENANT_ID, "BOIUK");
		ProposingPartyAccount account = new ProposingPartyAccount();
		updateData.setFraudScoreUpdated(true);
		FraudServiceResponse fraudsystem = new FraudServiceResponse();
		fraudsystem.setDecisionType(DecisionType.REJECT);
		fraudsystem.setDeviceIdentifier("15689");
		updateData.setFraudScore(fraudsystem);
		updateData.setSetupStatus("fukg");
		updateData.setSetupStatusUpdated(true);
		OBCashAccountDebtor3 debtorDetails = new OBCashAccountDebtor3();
		debtorDetails.setIdentification("kfgjkg");
		debtorDetails.setName("fjk");
		debtorDetails.setSchemeName("fg");
		debtorDetails.setSecondaryIdentification("ry");
		updateData.setDebtorDetails(debtorDetails);
		updateData.setDebtorDetailsUpdated(true);
		account.setAccountName("ertye");
		account.setAccountNumber("35");
		account.setSchemeName("drthq2");
		account.setSecondaryIdentification("gfje");
		paymentInstructionProposal.setProposingPartyAccount(account);
		pispScaConsentFoundationServiceTransformer.transformPispScaConsentResponseForUpdate(updateData,
				paymentInstructionProposal,params);
	}
	@Test
	public void testPispScaConsentFoundationServiceTransformerforValidate12() {

		OBCashAccountDebtor3 selectedDebtorDetails = new OBCashAccountDebtor3();
		CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo = new CustomPreAuthorizeAdditionalInfo();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		PaymentInstructionProposal paymentInstructionProposal = new PaymentInstructionProposal();
		Map<String, String> param = new HashMap<String, String>();
		param.put(TENANT_ID, "BOIROI");
		param.put("X-BOI-CHANNEL", "BOL");
		selectedDebtorDetails.setIdentification("asdf");
		selectedDebtorDetails.setSchemeName("abcd");
		selectedDebtorDetails.setName("adffg");
		selectedDebtorDetails.setSecondaryIdentification("acvgvg");

		pispScaConsentFoundationServiceTransformer.transformDomesticConsentResponseFromAPIToFDForInsert(
				selectedDebtorDetails,paymentInstructionProposal,param);
	}

	@Test
	public void testPispScaConsentFoundationServiceTransformerforValidate() {

		OBCashAccountDebtor3 selectedDebtorDetails = new OBCashAccountDebtor3();
		CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo = new CustomPreAuthorizeAdditionalInfo();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		PaymentInstructionProposal paymentInstructionProposal = new PaymentInstructionProposal();
		Map<String, String> param = new HashMap<String, String>();
		param.put(TENANT_ID, "BOIUK");
		param.put("X-BOI-CHANNEL", "B365");
		selectedDebtorDetails.setIdentification("asdf");
		selectedDebtorDetails.setSchemeName("abcd");
		selectedDebtorDetails.setName("adffg");
		selectedDebtorDetails.setSecondaryIdentification("acvgvg");

		pispScaConsentFoundationServiceTransformer.transformDomesticConsentResponseFromAPIToFDForInsert(
				selectedDebtorDetails,paymentInstructionProposal,param);
	}
	@Test
	public void testPispScaConsentFoundationServiceTransformerforValidate1() {

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		PaymentInstructionProposal paymentInstructionProposal = new PaymentInstructionProposal();
		Map<String, String> params = new HashMap<String, String>();
		stageIdentifiers.setPaymentSubmissionId("asdf");

		paymentInstructionProposal.setProposalStatus(ProposalStatus.Rejected);
		pispScaConsentFoundationServiceTransformer.transformDomesticConsentResponseFromFDToAPIForInsert(
				paymentInstructionProposal, stageIdentifiers);
	}
	
	
	@Test
	public void testtransformDomesticScheduledConsentResponseFromAPIToFDForInsert() {
		OBCashAccountDebtor3 selectedDebtorDetails= new OBCashAccountDebtor3();
		CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo= new CustomPreAuthorizeAdditionalInfo();
		CustomPaymentStageIdentifiers stageIdentifiers= new CustomPaymentStageIdentifiers();
		ScheduledPaymentInstructionProposal scheduledPaymentInstructionProposal=new ScheduledPaymentInstructionProposal();
		Map<String, String> params= new HashMap<String,String>();
		params.put(TENANT_ID, "BOIUK");
		selectedDebtorDetails.setIdentification("aaa");
		selectedDebtorDetails.setName("ram");
		selectedDebtorDetails.setSchemeName("ruru");
		selectedDebtorDetails.setSecondaryIdentification("eueu");
		
		pispScaConsentFoundationServiceTransformer.transformDomesticScheduledConsentResponseFromAPIToFDForInsert(selectedDebtorDetails,scheduledPaymentInstructionProposal,params);
		
	}
	@Test
	public void testtransformDomesticScheduledConsentResponseFromAPIToFDForInsert15() {
		OBCashAccountDebtor3 selectedDebtorDetails= new OBCashAccountDebtor3();
		CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo= new CustomPreAuthorizeAdditionalInfo();
		CustomPaymentStageIdentifiers stageIdentifiers= new CustomPaymentStageIdentifiers();
		ScheduledPaymentInstructionProposal scheduledPaymentInstructionProposal=new ScheduledPaymentInstructionProposal();
		scheduledPaymentInstructionProposal.setInstructionEndToEndReference("sdcsdcsd");
		Map<String, String> params= new HashMap<String,String>();
		params.put(TENANT_ID, "BOIUK");
		selectedDebtorDetails.setIdentification("aaa");
		selectedDebtorDetails.setName("ram");
		selectedDebtorDetails.setSchemeName("ruru");
		selectedDebtorDetails.setSecondaryIdentification("eueu");
		
		pispScaConsentFoundationServiceTransformer.transformDomesticScheduledConsentResponseFromAPIToFDForInsert(selectedDebtorDetails,scheduledPaymentInstructionProposal,params);
		
	}
	@Test
	public void testtransformDomesticScheduledConsentResponseFromFDToAPIForInsert() {
		
		CustomPaymentStageIdentifiers stageIdentifiers= new CustomPaymentStageIdentifiers();
		ScheduledPaymentInstructionProposal scheduledPaymentInstructionProposal = new ScheduledPaymentInstructionProposal ();
		Map<String, String> params= new HashMap<String,String>();
		stageIdentifiers.setPaymentSubmissionId("384");
		pispScaConsentFoundationServiceTransformer.transformDomesticScheduledConsentResponseFromFDToAPIForInsert(stageIdentifiers);
		
	}
	
	
	@Test
	public void testtransformPispScaConsentScheduledResponseForUpdate() {
		CustomPaymentStageUpdateData updateData =new CustomPaymentStageUpdateData();
		
		ScheduledPaymentInstructionProposal scheduledPaymentInstructionProposal=new ScheduledPaymentInstructionProposal();
		Map<String, String> params= new HashMap<String,String>();
		updateData.setDebtorDetailsUpdated(true);
		updateData.setSetupStatus("djdjd");
		params.put(TENANT_ID, "BOIUK");
		updateData.setSetupStatusUpdated(true);
		OBCashAccountDebtor3  oBCashAccountDebtor3 =new OBCashAccountDebtor3 ();
		oBCashAccountDebtor3.setIdentification("eeu");
		oBCashAccountDebtor3.setName("dhdh");
		oBCashAccountDebtor3.setSchemeName("ram");
		oBCashAccountDebtor3.setSecondaryIdentification("ddj");
		updateData.setDebtorDetails(oBCashAccountDebtor3);
		updateData.setFraudScoreUpdated(true);
		FraudServiceResponse fraudScore = new FraudServiceResponse();
		fraudScore.setDecisionType(DecisionType.APPROVE);
		updateData.setFraudScore(fraudScore);
		pispScaConsentFoundationServiceTransformer.transformPispScaConsentScheduledResponseForUpdate(updateData, scheduledPaymentInstructionProposal,params);
		
	}
	@Test
	public void testtransformPispScaConsentScheduledResponseForUpdate1() {
		CustomPaymentStageUpdateData updateData =new CustomPaymentStageUpdateData();
		
		ScheduledPaymentInstructionProposal scheduledPaymentInstructionProposal=new ScheduledPaymentInstructionProposal();
		Map<String, String> params= new HashMap<String,String>();
		updateData.setDebtorDetailsUpdated(true);
		updateData.setSetupStatus("djdjd");
		params.put(TENANT_ID, "BOIUK");
		updateData.setSetupStatusUpdated(true);
		OBCashAccountDebtor3  oBCashAccountDebtor3 =new OBCashAccountDebtor3 ();
		oBCashAccountDebtor3.setIdentification("eeu");
		oBCashAccountDebtor3.setName("dhdh");
		oBCashAccountDebtor3.setSchemeName("ram");
		oBCashAccountDebtor3.setSecondaryIdentification("ddj");
		updateData.setDebtorDetails(oBCashAccountDebtor3);
		updateData.setFraudScoreUpdated(true);
		FraudServiceResponse fraudScore = new FraudServiceResponse();
		fraudScore.setDecisionType(DecisionType.INVESTIGATE_APPROVE);
		updateData.setFraudScore(fraudScore);
		scheduledPaymentInstructionProposal.setInstructionEndToEndReference("vdfvdfvd");
		pispScaConsentFoundationServiceTransformer.transformPispScaConsentScheduledResponseForUpdate(updateData, scheduledPaymentInstructionProposal,params);
		
	}
	@Test
	public void testtransformPispScaConsentScheduledResponseForUpdate2() {
		CustomPaymentStageUpdateData updateData =new CustomPaymentStageUpdateData();
		
		ScheduledPaymentInstructionProposal scheduledPaymentInstructionProposal=new ScheduledPaymentInstructionProposal();
		Map<String, String> params= new HashMap<String,String>();
		updateData.setDebtorDetailsUpdated(true);
		updateData.setSetupStatus("djdjd");
		params.put(TENANT_ID, "BOIUK");
		updateData.setSetupStatusUpdated(true);
		OBCashAccountDebtor3  oBCashAccountDebtor3 =new OBCashAccountDebtor3 ();
		oBCashAccountDebtor3.setIdentification("eeu");
		oBCashAccountDebtor3.setName("dhdh");
		oBCashAccountDebtor3.setSchemeName("ram");
		oBCashAccountDebtor3.setSecondaryIdentification("ddj");
		updateData.setDebtorDetails(oBCashAccountDebtor3);
		updateData.setFraudScoreUpdated(true);
		FraudServiceResponse fraudScore = new FraudServiceResponse();
		fraudScore.setDecisionType(DecisionType.INVESTIGATE_CANCEL);
		updateData.setFraudScore(fraudScore);
		pispScaConsentFoundationServiceTransformer.transformPispScaConsentScheduledResponseForUpdate(updateData, scheduledPaymentInstructionProposal,params);
		
	}
	@Test
	public void testtransformPispScaConsentScheduledResponseForUpdate3() {
		CustomPaymentStageUpdateData updateData =new CustomPaymentStageUpdateData();
		
		ScheduledPaymentInstructionProposal scheduledPaymentInstructionProposal=new ScheduledPaymentInstructionProposal();
		Map<String, String> params= new HashMap<String,String>();
		updateData.setDebtorDetailsUpdated(true);
		updateData.setSetupStatus("djdjd");
		params.put(TENANT_ID, "BOIUK");
		updateData.setSetupStatusUpdated(true);
		OBCashAccountDebtor3  oBCashAccountDebtor3 =new OBCashAccountDebtor3 ();
		oBCashAccountDebtor3.setIdentification("eeu");
		oBCashAccountDebtor3.setName("dhdh");
		oBCashAccountDebtor3.setSchemeName("ram");
		oBCashAccountDebtor3.setSecondaryIdentification("ddj");
		updateData.setDebtorDetails(oBCashAccountDebtor3);
		updateData.setFraudScoreUpdated(true);
		FraudServiceResponse fraudScore = new FraudServiceResponse();
		fraudScore.setDecisionType(DecisionType.INVESTIGATE_WAIT);
		updateData.setFraudScore(fraudScore);
		pispScaConsentFoundationServiceTransformer.transformPispScaConsentScheduledResponseForUpdate(updateData, scheduledPaymentInstructionProposal,params);
		
	}
	@Test
	public void testtransformPispScaConsentScheduledResponseForUpdate4() {
		CustomPaymentStageUpdateData updateData =new CustomPaymentStageUpdateData();
		
		ScheduledPaymentInstructionProposal scheduledPaymentInstructionProposal=new ScheduledPaymentInstructionProposal();
		Map<String, String> params= new HashMap<String,String>();
		updateData.setDebtorDetailsUpdated(true);
		updateData.setSetupStatus("djdjd");
		params.put(TENANT_ID, "BOIUK");
		updateData.setSetupStatusUpdated(true);
		OBCashAccountDebtor3  oBCashAccountDebtor3 =new OBCashAccountDebtor3 ();
		oBCashAccountDebtor3.setIdentification("eeu");
		oBCashAccountDebtor3.setName("dhdh");
		oBCashAccountDebtor3.setSchemeName("ram");
		oBCashAccountDebtor3.setSecondaryIdentification("ddj");
		updateData.setDebtorDetails(oBCashAccountDebtor3);
		updateData.setFraudScoreUpdated(true);
		FraudServiceResponse fraudScore = new FraudServiceResponse();
		fraudScore.setDecisionType(DecisionType.REJECT);
		updateData.setFraudScore(fraudScore);
		pispScaConsentFoundationServiceTransformer.transformPispScaConsentScheduledResponseForUpdate(updateData, scheduledPaymentInstructionProposal,params);
		
	}
	
	
	@Test
	public void testtransformPispScaConsentResponseForStandingOrders() {
		
		StandingOrderInstructionProposal standingOrderInstructionProposal = new StandingOrderInstructionProposal ();
		
		Map<String, String> params= new HashMap<String,String>();
		
		
		AuthorisingPartyAccount authorisingPartyAccount =new AuthorisingPartyAccount();
		authorisingPartyAccount.setAccountIdentification("ddj");
		authorisingPartyAccount.setAccountName("Boi");
		authorisingPartyAccount.setAccountNumber("10220");
		authorisingPartyAccount.setSchemeName("dhdh");
		authorisingPartyAccount.setSecondaryIdentification("1qwe");
		
		standingOrderInstructionProposal.setAuthorisingPartyAccount(authorisingPartyAccount);
		ProposingPartyAccount proposingPartyAccount= new ProposingPartyAccount();
		proposingPartyAccount.setAccountIdentification("euu");
		proposingPartyAccount.setAccountName("euru");
		proposingPartyAccount.setAccountNumber("123");
		proposingPartyAccount.setSchemeName("ram");
		proposingPartyAccount.setSecondaryIdentification("tyrt");
		standingOrderInstructionProposal.setProposingPartyAccount(proposingPartyAccount);
		
		PaymentInstructionCharge paymentInstructionCharge = new PaymentInstructionCharge();
		List<PaymentInstructionCharge> paymChargeList = new ArrayList<PaymentInstructionCharge>();
		Amount amount = new Amount();
		amount.setAccountCurrency(23.3);
		amount.setGroupReportingCurrency(34.0);
		amount.setLocalReportingCurrency(12.0);
		amount.setTransactionCurrency(23.0);
		paymentInstructionCharge.setAmount(amount);
		
		paymentInstructionCharge.setChargeBearer(ChargeBearer.BorneByDebtor);
		Currency currency = new Currency();
		currency.setCurrencyName("wiwi");
		currency.setIsoAlphaCode("euue");
		paymentInstructionCharge.setCurrency(currency);
		paymentInstructionCharge.setType("dhdh");
		 paymChargeList.add(paymentInstructionCharge);
		standingOrderInstructionProposal.setCharges( paymChargeList);
		PaymentTransaction paymentTransaction = new PaymentTransaction();
		FinancialEventAmount financialEventAmount =new FinancialEventAmount();
		financialEventAmount.setAccountCurrency(23.0);
		financialEventAmount.setGroupReportingCurrency(30.20);
		financialEventAmount.setLocalReportingCurrency(40.5);
		financialEventAmount.setTransactionCurrency(43.0);
		paymentTransaction.setFinancialEventAmount(financialEventAmount);
		Currency transactionCurrency =new Currency();
		currency.setCurrencyName("dolar");
		currency.setIsoAlphaCode("1020");
		paymentTransaction.setTransactionCurrency(transactionCurrency);
		standingOrderInstructionProposal.setRecurringPaymentAmount(paymentTransaction);
		standingOrderInstructionProposal.setFirstPaymentAmount(paymentTransaction);
		standingOrderInstructionProposal.setFinalPaymentAmount(paymentTransaction);
		standingOrderInstructionProposal.setFrequency(Frequency.FORTNIGHTLY);
		pispScaConsentFoundationServiceTransformer.transformPispScaConsentResponseForStandingOrders(standingOrderInstructionProposal, params);	
	}
	
	
	@Test
	public void testtransformFraudSystemDomesticPaymentStagedData() {
		CustomPaymentStageUpdateData updateData =new CustomPaymentStageUpdateData();
		
		
		Map<String, String> params= new HashMap<String,String>();
		
		
		 PaymentInstructionProposal  paymentInstructionProposal =new PaymentInstructionProposal();
		 FinancialEventAmount financialEventAmount =new FinancialEventAmount();
		 financialEventAmount.setAccountCurrency(30.0);
		 financialEventAmount.setGroupReportingCurrency(23.89);
		 financialEventAmount.setLocalReportingCurrency(56.89);
		 financialEventAmount.setTransactionCurrency(56.78);
		 
		 paymentInstructionProposal.setFinancialEventAmount(financialEventAmount);
		Currency currency =new Currency();
		currency.setCurrencyName("dkdk");
		currency.setIsoAlphaCode("wiwi");
		paymentInstructionProposal.setTransactionCurrency(currency);
		paymentInstructionProposal.setAuthorisingPartyReference("sjsj");
		ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();
		proposingPartyAccount.setAccountIdentification("skk");
		proposingPartyAccount.setAccountName("dkd");
		proposingPartyAccount.setAccountNumber("djdj");
		proposingPartyAccount.setSchemeName("skk");
	proposingPartyAccount.setSecondaryIdentification("sksk");
		paymentInstructionProposal.setProposingPartyAccount(proposingPartyAccount);
		
		pispScaConsentFoundationServiceTransformer.transformFraudSystemDomesticPaymentStagedData(paymentInstructionProposal);
		
	}
	
	@Test
	public void testtransformFraudSystemDomesticScheduledPaymentStagedData() {
		CustomFraudSystemPaymentData customFraudSystemPaymentData = new CustomFraudSystemPaymentData();
		Map<String, String> params= new HashMap<String,String>();
		
		ScheduledPaymentInstructionProposal scheduledPaymentInstructionProposal= new ScheduledPaymentInstructionProposal();
		FinancialEventAmount financialEventAmount=new FinancialEventAmount();
		financialEventAmount.setAccountCurrency(40.56);
		financialEventAmount.setGroupReportingCurrency(12.34);
		financialEventAmount.setLocalReportingCurrency(40.34);
		financialEventAmount.setTransactionCurrency(23.56);
		scheduledPaymentInstructionProposal.setFinancialEventAmount(financialEventAmount);
		
		Currency transactionCurrency = new Currency();
		transactionCurrency.setCurrencyName("ei");
		transactionCurrency.setIsoAlphaCode("dk");
		scheduledPaymentInstructionProposal.setTransactionCurrency(transactionCurrency);
		scheduledPaymentInstructionProposal.setAuthorisingPartyReference("djd");
		ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();
		proposingPartyAccount.setAccountIdentification("dkk");
		proposingPartyAccount.setAccountName("djdj");
		proposingPartyAccount.setAccountNumber("124");
		proposingPartyAccount.setSchemeName("sjs");
		proposingPartyAccount.setSecondaryIdentification("djdj");
	
		scheduledPaymentInstructionProposal.setProposingPartyAccount(proposingPartyAccount);
		pispScaConsentFoundationServiceTransformer.transformFraudSystemDomesticScheduledPaymentStagedData(scheduledPaymentInstructionProposal);
	}
	
	@Test
	public void testtransformFraudSystemDomesticStandingOrderPaymentStagedData() {
		CustomFraudSystemPaymentData customFraudSystemPaymentData = new CustomFraudSystemPaymentData();
		Map<String, String> params= new HashMap<String,String>();
		
		StandingOrderInstructionProposal standingOrderInstructionProposal = new StandingOrderInstructionProposal();
		PaymentTransaction firstPaymentAmount =new PaymentTransaction();
		FinancialEventAmount financialEventAmount=new FinancialEventAmount();
		financialEventAmount.setAccountCurrency(40.56);
		financialEventAmount.setGroupReportingCurrency(12.34);
		financialEventAmount.setLocalReportingCurrency(40.34);
		financialEventAmount.setTransactionCurrency(23.56);
		Currency transactionCurrency = new Currency();
		transactionCurrency.setCurrencyName("ei");
		transactionCurrency.setIsoAlphaCode("dk");
		firstPaymentAmount.setTransactionCurrency(transactionCurrency);
		
		firstPaymentAmount.setFinancialEventAmount(financialEventAmount);
		standingOrderInstructionProposal.setFirstPaymentAmount(firstPaymentAmount);
		standingOrderInstructionProposal.setReference("djdj");
		ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();
		proposingPartyAccount.setAccountIdentification("dkk");
		proposingPartyAccount.setAccountName("djdj");
		proposingPartyAccount.setAccountNumber("124");
		proposingPartyAccount.setSchemeName("sjs");
		proposingPartyAccount.setSecondaryIdentification("djdj");
	
		standingOrderInstructionProposal.setProposingPartyAccount(proposingPartyAccount);
		
		
		pispScaConsentFoundationServiceTransformer.transformFraudSystemDomesticStandingOrderPaymentStagedData(standingOrderInstructionProposal);
	}
	
	@Test
	public void testtransformDomesticStandingOrderConsentResponseFromAPIToFDForInsert( ){
		CustomPaymentStageIdentifiers stageIdentifiers =new CustomPaymentStageIdentifiers();
		Map<String, String> params= new HashMap<String,String>();
		CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo =new CustomPreAuthorizeAdditionalInfo();
		StandingOrderInstructionProposal standingOrderInstructionProposal = new StandingOrderInstructionProposal();
		OBCashAccountDebtor3 selectedDebtorDetails =new OBCashAccountDebtor3();
		params.put(TENANT_ID, "BOIUK");
		selectedDebtorDetails.setIdentification("dld");
		selectedDebtorDetails.setName("dldl");
		selectedDebtorDetails.setSchemeName("rel");
		selectedDebtorDetails.setSecondaryIdentification("dldl");
		
		pispScaConsentFoundationServiceTransformer.transformDomesticStandingOrderConsentResponseFromAPIToFDForInsert(selectedDebtorDetails, standingOrderInstructionProposal);
	}
	
	@Test
	public void testtransformDomesticStandingOrderConsentResponseFromFDToAPIForInsert(){
		CustomPaymentStageIdentifiers stageIdentifiers =new CustomPaymentStageIdentifiers();
		Map<String, String> params= new HashMap<String,String>();
		
		StandingOrderInstructionProposal standingOrderInstructionProposal = new StandingOrderInstructionProposal();
		
		stageIdentifiers.setPaymentSubmissionId("dld");
		pispScaConsentFoundationServiceTransformer.transformDomesticStandingOrderConsentResponseFromFDToAPIForInsert(stageIdentifiers);
	}
	@Test
	public void testtransformDomesticStandingOrderConsentResponseForUpdate(){
		
		Map<String, String> params= new HashMap<String,String>();
		params.put(TENANT_ID, "BOIUK");
		CustomPaymentStageUpdateData updateData =new CustomPaymentStageUpdateData();
		 StandingOrderInstructionProposal standingorderinstructionproposal= new StandingOrderInstructionProposal();
		 OBCashAccountDebtor3 debtorDetails = new OBCashAccountDebtor3();
		 debtorDetails.setIdentification("dl");
		 debtorDetails.setName("dld");
		 debtorDetails.setSchemeName("dll");
		 debtorDetails.setSecondaryIdentification("sksk");
		 updateData.setDebtorDetails(debtorDetails);
		 updateData.setSetupStatusUpdateDateTime("");
		 updateData.setSetupStatus("dkkd");
		 updateData.setSetupStatusUpdated(false);
		 updateData.setDebtorDetailsUpdated(false);
		updateData.setFraudScore(null);
		pispScaConsentFoundationServiceTransformer.transformDomesticStandingOrderConsentResponseForUpdate(updateData, standingorderinstructionproposal);
	}
	
	@Test
	public void transformDomesticStandingOrderConsentResponseForUpdate(){
		
		Map<String, String> params= new HashMap<String,String>();
		params.put(TENANT_ID, "BOIUK");
		CustomPaymentStageUpdateData updateData =new CustomPaymentStageUpdateData();
		 StandingOrderInstructionProposal standingorderinstructionproposal= new StandingOrderInstructionProposal();
		 OBCashAccountDebtor3 debtorDetails = new OBCashAccountDebtor3();
		 debtorDetails.setIdentification("dl");
		 debtorDetails.setName("dld");
		 debtorDetails.setSchemeName("dll");
		 debtorDetails.setSecondaryIdentification("sksk");
		 debtorDetails.setIdentification("dcdc1d2120");
		 updateData.setDebtorDetails(debtorDetails);
		 updateData.setSetupStatusUpdateDateTime("2018-10-20T00:00:00+01:00");
		 updateData.setSetupStatus(ProposalStatus.Authorised.toString());
		 updateData.setSetupStatusUpdated(true);
		 updateData.setDebtorDetailsUpdated(true);
		 FraudServiceResponse fraudScore = new FraudServiceResponse();
		 fraudScore.setDecisionType(DecisionType.APPROVE);
		updateData.setFraudScore(fraudScore);
		updateData.setFraudScoreUpdated(true);
		pispScaConsentFoundationServiceTransformer.transformDomesticStandingOrderConsentResponseForUpdate(updateData, standingorderinstructionproposal);
	}
	
	@Test
	public void transformDomesticStandingOrderConsentResponseForUpdate1(){
		
		Map<String, String> params= new HashMap<String,String>();
		params.put(TENANT_ID, "vbhvcb");
		CustomPaymentStageUpdateData updateData =new CustomPaymentStageUpdateData();
		 StandingOrderInstructionProposal standingorderinstructionproposal= new StandingOrderInstructionProposal();
		 OBCashAccountDebtor3 debtorDetails = new OBCashAccountDebtor3();
		 debtorDetails.setIdentification("dl");
		 debtorDetails.setName("dld");
		 debtorDetails.setSchemeName("dll");
		 debtorDetails.setSecondaryIdentification("sksk");
		 debtorDetails.setIdentification("dcdc1d2120");
		 updateData.setDebtorDetails(debtorDetails);
		 updateData.setSetupStatusUpdateDateTime("2018-10-20T00:00:00+01:00");
		 updateData.setSetupStatus(ProposalStatus.Authorised.toString());
		 updateData.setSetupStatusUpdated(true);
		 updateData.setDebtorDetailsUpdated(true);
		 FraudServiceResponse fraudScore = new FraudServiceResponse();
		 fraudScore.setDecisionType(DecisionType.INVESTIGATE_APPROVE);
		updateData.setFraudScore(fraudScore);
		updateData.setFraudScoreUpdated(true);
		pispScaConsentFoundationServiceTransformer.transformDomesticStandingOrderConsentResponseForUpdate(updateData, standingorderinstructionproposal);
	}
	@Test
	public void transformDomesticStandingOrderConsentResponseForUpdate12(){
		
		Map<String, String> params= new HashMap<String,String>();
		params.put(TENANT_ID, "BOIUK");
		CustomPaymentStageUpdateData updateData =new CustomPaymentStageUpdateData();
		 StandingOrderInstructionProposal standingorderinstructionproposal= new StandingOrderInstructionProposal();
		 OBCashAccountDebtor3 debtorDetails = new OBCashAccountDebtor3();
		 debtorDetails.setIdentification("dl");
		 debtorDetails.setName("dld");
		 debtorDetails.setSchemeName("dll");
		 debtorDetails.setSecondaryIdentification("sksk");
		 debtorDetails.setIdentification("dcdc1d2120");
		 updateData.setDebtorDetails(debtorDetails);
		 updateData.setSetupStatusUpdateDateTime("2018-10-20T00:00:00+01:00");
		 updateData.setSetupStatus(ProposalStatus.Authorised.toString());
		 updateData.setSetupStatusUpdated(true);
		 updateData.setDebtorDetailsUpdated(true);
		 FraudServiceResponse fraudScore = new FraudServiceResponse();
		 fraudScore.setDecisionType(DecisionType.REJECT);
		updateData.setFraudScore(fraudScore);
		updateData.setFraudScoreUpdated(true);
		pispScaConsentFoundationServiceTransformer.transformDomesticStandingOrderConsentResponseForUpdate(updateData, standingorderinstructionproposal);
	}
	@Test
	public void transformDomesticStandingOrderConsentResponseForUpdate2(){
		
		Map<String, String> params= new HashMap<String,String>();
		params.put(TENANT_ID, "BOIUK");
		CustomPaymentStageUpdateData updateData =new CustomPaymentStageUpdateData();
		 StandingOrderInstructionProposal standingorderinstructionproposal= new StandingOrderInstructionProposal();
		 OBCashAccountDebtor3 debtorDetails = new OBCashAccountDebtor3();
		 debtorDetails.setIdentification("dl");
		 debtorDetails.setName("dld");
		 debtorDetails.setSchemeName("dll");
		 debtorDetails.setSecondaryIdentification("sksk");
		 debtorDetails.setIdentification("dcdc1d2120");
		 updateData.setDebtorDetails(debtorDetails);
		 updateData.setSetupStatusUpdateDateTime("2018-10-20T00:00:00+01:00");
		 updateData.setSetupStatus(ProposalStatus.Authorised.toString());
		 updateData.setSetupStatusUpdated(true);
		 updateData.setDebtorDetailsUpdated(true);
		 FraudServiceResponse fraudScore = new FraudServiceResponse();
		 fraudScore.setDecisionType(DecisionType.INVESTIGATE_CANCEL);
		updateData.setFraudScore(fraudScore);
		updateData.setFraudScoreUpdated(true);
		pispScaConsentFoundationServiceTransformer.transformDomesticStandingOrderConsentResponseForUpdate(updateData, standingorderinstructionproposal);
	}
	
	@Test
	public void transformDomesticStandingOrderConsentResponseForUpdate3(){
		
		Map<String, String> params= new HashMap<String,String>();
		params.put(TENANT_ID, "BOIUK");
		CustomPaymentStageUpdateData updateData =new CustomPaymentStageUpdateData();
		 StandingOrderInstructionProposal standingorderinstructionproposal= new StandingOrderInstructionProposal();
		 OBCashAccountDebtor3 debtorDetails = new OBCashAccountDebtor3();
		 debtorDetails.setIdentification("dl");
		 debtorDetails.setName("dld");
		 debtorDetails.setSchemeName("dll");
		 debtorDetails.setSecondaryIdentification("sksk");
		 debtorDetails.setIdentification("dcdc1d2120");
		 updateData.setDebtorDetails(debtorDetails);
		 updateData.setSetupStatusUpdateDateTime("2018-10-20T00:00:00+01:00");
		 updateData.setSetupStatus(ProposalStatus.Authorised.toString());
		 updateData.setSetupStatusUpdated(true);
		 updateData.setDebtorDetailsUpdated(true);
		 FraudServiceResponse fraudScore = new FraudServiceResponse();
		 fraudScore.setDecisionType(DecisionType.INVESTIGATE_WAIT);
		updateData.setFraudScore(fraudScore);
		updateData.setFraudScoreUpdated(true);
		pispScaConsentFoundationServiceTransformer.transformDomesticStandingOrderConsentResponseForUpdate(updateData, standingorderinstructionproposal);
	}
}
*/
