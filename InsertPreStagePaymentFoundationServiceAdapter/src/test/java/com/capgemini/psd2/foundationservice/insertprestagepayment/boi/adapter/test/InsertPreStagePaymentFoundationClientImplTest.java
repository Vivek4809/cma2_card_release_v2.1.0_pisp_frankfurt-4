package com.capgemini.psd2.foundationservice.insertprestagepayment.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.client.InsertPreStagePaymentFoundationClientImpl;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;



@RunWith(SpringJUnit4ClassRunner.class)
public class InsertPreStagePaymentFoundationClientImplTest {

	@InjectMocks
	private InsertPreStagePaymentFoundationClientImpl insertPreStagePaymentFoundationClientImpl;
	
	@Mock
	@Qualifier("restClientFoundation")
	private RestClientSync restClient;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	@Test
	public void contextLoads() {
	}
	
	@Test(expected = RuntimeException.class)
	public void testRestTransportForInsertPreStagePayment(){
		
		ValidationPassed validationPassed = new ValidationPassed();
		RequestInfo requestInfo = new RequestInfo();
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		HttpHeaders headers = new HttpHeaders();
		
		when(restClient.callForPost(anyObject(), anyObject(), anyObject(), anyObject())).thenReturn(validationPassed);
		ValidationPassed passed = insertPreStagePaymentFoundationClientImpl.restTransportForInsertPreStagePayment(requestInfo, paymentInstruction, ValidationPassed.class, headers);
		assertNotNull(passed);
		
		ErrorInfo errorInfo = new ErrorInfo("503", "test", "Connection refused: connect", "500");
		errorInfo.setErrorCode("503");
		errorInfo.setErrorMessage("test");
		errorInfo.setStatusCode("test");
		errorInfo.setDetailErrorMessage("Connection refused: connect");
		when(restClient.callForPost(anyObject(), anyObject(), anyObject(), anyObject())).thenThrow(new AdapterException("test", errorInfo));
		insertPreStagePaymentFoundationClientImpl.restTransportForInsertPreStagePayment(requestInfo, paymentInstruction, ValidationPassed.class, headers);
	}
	
	@Test(expected = Exception.class)
	public void testRestTransportForInsertPreStagePaymentForException(){
		
		RequestInfo requestInfo = new RequestInfo();
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		HttpHeaders headers = new HttpHeaders();
		when(restClient.callForPost(anyObject(), anyObject(), anyObject(), anyObject())).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CONNECTION_ERROR));
		insertPreStagePaymentFoundationClientImpl.restTransportForInsertPreStagePayment(requestInfo, paymentInstruction, ValidationPassed.class, headers);
	}
	
	@Test(expected = RuntimeException.class)
	public void testRestTransportForUpdatePreStagePayment(){
		
		ValidationPassed validationPassed = new ValidationPassed();
		RequestInfo requestInfo = new RequestInfo();
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		HttpHeaders headers = new HttpHeaders();
		
		when(restClient.callForPost(anyObject(), anyObject(), anyObject(), anyObject())).thenReturn(validationPassed);
		ValidationPassed passed = insertPreStagePaymentFoundationClientImpl.restTransportForUpdatePreStagePayment(requestInfo, paymentInstruction, ValidationPassed.class, headers);
		assertNotNull(passed);
		
		ErrorInfo errorInfo = new ErrorInfo("503", "test", "Connection refused: connect", "500");
		errorInfo.setErrorCode("503");
		errorInfo.setErrorMessage("test");
		errorInfo.setStatusCode("test");
		errorInfo.setDetailErrorMessage("Connection refused: connect");
		when(restClient.callForPost(anyObject(), anyObject(), anyObject(), anyObject())).thenThrow(new AdapterException("test", errorInfo));
		insertPreStagePaymentFoundationClientImpl.restTransportForUpdatePreStagePayment(requestInfo, paymentInstruction, ValidationPassed.class, headers);
	}
	
	@Test(expected = Exception.class)
	public void testRestTransportForUpdatePreStagePaymentForException(){
		
		RequestInfo requestInfo = new RequestInfo();
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		HttpHeaders headers = new HttpHeaders();
		when(restClient.callForPost(anyObject(), anyObject(), anyObject(), anyObject())).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CONNECTION_ERROR));
		insertPreStagePaymentFoundationClientImpl.restTransportForUpdatePreStagePayment(requestInfo, paymentInstruction, ValidationPassed.class, headers);
	}
	
	@Test(expected = RuntimeException.class)
	public void testRestTransportForPaymentRetrieval(){
		
		RequestInfo requestInfo = new RequestInfo();
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		HttpHeaders headers = new HttpHeaders();
		
		when(restClient.callForGet(anyObject(), anyObject(), anyObject())).thenReturn(paymentInstruction);
		PaymentInstruction instruction = insertPreStagePaymentFoundationClientImpl.restTransportForPaymentRetrieval(requestInfo, PaymentInstruction.class, headers);
		assertNotNull(instruction);
		
		ErrorInfo errorInfo = new ErrorInfo("503", "test", "Connection refused: connect", "500");
		errorInfo.setErrorCode("503");
		errorInfo.setErrorMessage("test");
		errorInfo.setStatusCode("test");
		errorInfo.setDetailErrorMessage("Connection refused: connect");
		when(restClient.callForGet(anyObject(), anyObject(), anyObject())).thenThrow(new AdapterException("test", errorInfo));
		insertPreStagePaymentFoundationClientImpl.restTransportForPaymentRetrieval(requestInfo, PaymentInstruction.class, headers);
	}
	
	@Test(expected = RuntimeException.class)
	public void testRestTransportForPaymentRetrievalForRunTimeEx503(){
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders headers = new HttpHeaders();
		
		ErrorInfo errorInfo = new ErrorInfo("503", "test", "Please try again later", "500");
		errorInfo.setErrorCode("503");
		errorInfo.setErrorMessage("test");
		errorInfo.setStatusCode("test");
		errorInfo.setDetailErrorMessage("Please try again later");
		when(restClient.callForGet(anyObject(), anyObject(), anyObject())).thenThrow(new AdapterException("test", errorInfo));
		insertPreStagePaymentFoundationClientImpl.restTransportForPaymentRetrieval(requestInfo, PaymentInstruction.class, headers);
	}
	
	@Test(expected = AdapterException.class)
	
		public void testRestTransportForPaymentUpdateAdapterExp(){	
			RequestInfo requestInfo = new RequestInfo();
			HttpHeaders headers = new HttpHeaders();
			
			ErrorInfo errorInfo = new ErrorInfo("504", "tes", "Please try again later", "4");
			PaymentInstruction paymentInstruction = new PaymentInstruction();
			when(restClient.callForPost(anyObject(), anyObject(), anyObject(), anyObject())).thenThrow(new AdapterException("test",errorInfo));
			insertPreStagePaymentFoundationClientImpl.restTransportForUpdatePreStagePayment(requestInfo, paymentInstruction, ValidationPassed.class, headers);
			

	}
	@Test(expected = AdapterException.class)
	public void testRestTransportForPaymentInsertForAdapterExp(){
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders headers = new HttpHeaders();
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		
		ErrorInfo errorInfo = new ErrorInfo("504", "tes", "Please try again later", "4");
		
		when(restClient.callForPost(anyObject(), anyObject(), anyObject(), anyObject())).thenThrow(new AdapterException("test",errorInfo));
		insertPreStagePaymentFoundationClientImpl.restTransportForInsertPreStagePayment(requestInfo, paymentInstruction,  ValidationPassed.class, headers);
	}
	
	
//	here
	@Test(expected = Exception.class)
	public void testRestTransportForPaymentInsertForEx503(){
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders headers = new HttpHeaders();
		when(restClient.callForPost(anyObject(), anyObject(), anyObject(), anyObject())).thenThrow(new NullPointerException());
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		ValidationPassed validationPassed = insertPreStagePaymentFoundationClientImpl.restTransportForInsertPreStagePayment(requestInfo, paymentInstruction, ValidationPassed.class, headers);
		
	}
	
	
	@Test(expected = RuntimeException.class)
	public void testRestTransportForPaymentRetrievalForRunTimeEx515(){
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders headers = new HttpHeaders();
		
		ErrorInfo errorInfo = new ErrorInfo("515", "test", "Connection Error", "500");
		errorInfo.setErrorCode("515");
		errorInfo.setErrorMessage("test");
		errorInfo.setStatusCode("test");
		errorInfo.setDetailErrorMessage("Connection Error");
		when(restClient.callForGet(anyObject(), anyObject(), anyObject())).thenThrow(new AdapterException("test", errorInfo));
		insertPreStagePaymentFoundationClientImpl.restTransportForPaymentRetrieval(requestInfo, PaymentInstruction.class, headers);
	}
	
	@Test(expected = RuntimeException.class)
	public void testRestTransportForPaymentRetrievalForRunTimeEx500(){
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders headers = new HttpHeaders();
		
		ErrorInfo errorInfo = new ErrorInfo("500", "test", "You are not authorised", "500");
		errorInfo.setErrorCode("500");
		errorInfo.setErrorMessage("test");
		errorInfo.setStatusCode("test");
		errorInfo.setDetailErrorMessage("You are not authorised");
		when(restClient.callForGet(anyObject(), anyObject(), anyObject())).thenThrow(new AdapterException("test", errorInfo));
		insertPreStagePaymentFoundationClientImpl.restTransportForPaymentRetrieval(requestInfo, PaymentInstruction.class, headers);
	}
	
	@Test(expected = RuntimeException.class)
	public void testRestTransportForPaymentRetrievalForRunTimeEx544(){
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders headers = new HttpHeaders();
		
		ErrorInfo errorInfo = new ErrorInfo("544", "test", "You are not authorised", "544");
		errorInfo.setErrorCode("544");
		errorInfo.setErrorMessage("test");
		errorInfo.setStatusCode("test");
		errorInfo.setDetailErrorMessage("You are not authorised");
		when(restClient.callForGet(anyObject(), anyObject(), anyObject())).thenThrow(new AdapterException("test", errorInfo));
		insertPreStagePaymentFoundationClientImpl.restTransportForPaymentRetrieval(requestInfo, PaymentInstruction.class, headers);
	
	}
	
	@Test(expected = RuntimeException.class)
	public void testRestTransportForPaymentRetrievalForRunTimeEx541(){
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders headers = new HttpHeaders();
		
		ErrorInfo errorInfo = new ErrorInfo("541", "test", "You are not authorised", "541");
		errorInfo.setErrorCode("541");
		errorInfo.setErrorMessage("test");
		errorInfo.setStatusCode("test");
		errorInfo.setDetailErrorMessage("You are not authorised");
		when(restClient.callForGet(anyObject(), anyObject(), anyObject())).thenThrow(new AdapterException("test", errorInfo));
		insertPreStagePaymentFoundationClientImpl.restTransportForPaymentRetrieval(requestInfo, PaymentInstruction.class, headers);
	
	}
	@Test(expected = Exception.class)
	public void testRestTransportForPaymentRetrievalForRunTimeExeption(){
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders headers = new HttpHeaders();
		
		
		when(restClient.callForGet(anyObject(), anyObject(), anyObject())).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CONNECTION_ERROR));
		insertPreStagePaymentFoundationClientImpl.restTransportForPaymentRetrieval(requestInfo, PaymentInstruction.class, headers);
	}
}
