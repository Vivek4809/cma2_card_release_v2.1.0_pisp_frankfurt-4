package com.capgemini.psd2.fraudsystem.routing.adapter.test.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.fraudsystem.adapter.FraudSystemAdapter;
import com.capgemini.psd2.fraudsystem.routing.adapter.impl.FraudSystemRoutingAdapter;
import com.capgemini.psd2.fraudsystem.routing.adapter.routing.FraudSystemAdapterFactory;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

public class FraudSystemRoutingAdapterTest {

	@Mock
	private FraudSystemAdapterFactory fraudSystemAdapterFactory;
	
	@InjectMocks
	private FraudSystemRoutingAdapter adapter;
	
	@Mock
	private RequestHeaderAttributes reqAttributes;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testRetrieveFraudScore() {
		
		Map<String, Map<String, Object>> fraudSystemRequest = new HashMap<>();
		
		when(reqAttributes.getTenantId()).thenReturn("ROI");
		
		
		FraudSystemAdapter fraudSystemAdapter = new FraudSystemAdapter() {
			
			@Override
			public <T> T retrieveFraudScore(Map<String, Map<String, Object>> fraudSystemRequest) {
				return null;
			}
		};
		Mockito.when(fraudSystemAdapterFactory.getAdapterInstance(anyString())).thenReturn(fraudSystemAdapter);
		
		adapter.retrieveFraudScore(fraudSystemRequest);
		assertNotNull(fraudSystemAdapter);
	}

}