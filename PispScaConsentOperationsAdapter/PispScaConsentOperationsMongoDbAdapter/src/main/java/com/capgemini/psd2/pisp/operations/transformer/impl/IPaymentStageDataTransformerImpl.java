package com.capgemini.psd2.pisp.operations.transformer.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.conditions.MongoDbMockCondition;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.operations.transformer.PaymentStageDataTransformer;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository.IPaymentConsentsFoundationRepository;
import com.capgemini.psd2.pisp.stage.domain.AmountDetails;
import com.capgemini.psd2.pisp.stage.domain.ChargeDetails;
import com.capgemini.psd2.pisp.stage.domain.CreditorDetails;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomDebtorDetails;
import com.capgemini.psd2.pisp.stage.domain.CustomFraudSystemPaymentData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;
import com.capgemini.psd2.pisp.stage.domain.ExchangeRateDetails;
import com.capgemini.psd2.pisp.stage.domain.RemittanceDetails;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Conditional(MongoDbMockCondition.class)
@Component("iPaymentStageDataTransformer")
public class IPaymentStageDataTransformerImpl
		implements PaymentStageDataTransformer<CustomIPaymentConsentsPOSTResponse> {

	@Autowired
	private IPaymentConsentsFoundationRepository iPaymentConsentsBankRepository;

	@Override
	public CustomConsentAppViewData transformDStageToConsentAppViewData(
			CustomIPaymentConsentsPOSTResponse iPaymentStageResponse) {
		CustomConsentAppViewData response = new CustomConsentAppViewData();

		/* Payment Type needed on consent page */
		response.setPaymentType(PaymentTypeEnum.INTERNATIONAL_PAY.getPaymentType());

		/* Amount details needed on consent page */
		AmountDetails amountDetails = new AmountDetails();
		if (!NullCheckUtils.isNullOrEmpty(iPaymentStageResponse.getData().getInitiation().getInstructedAmount())) {
			amountDetails.setAmount(iPaymentStageResponse.getData().getInitiation().getInstructedAmount().getAmount());
			amountDetails
					.setCurrency(iPaymentStageResponse.getData().getInitiation().getInstructedAmount().getCurrency());
		}

		/* Debtor details needed on consent page */
		CustomDebtorDetails debtorDetails = null;
		if (!NullCheckUtils.isNullOrEmpty(iPaymentStageResponse.getData().getInitiation().getDebtorAccount())) {
			debtorDetails = new CustomDebtorDetails();
			debtorDetails.setIdentification(
					iPaymentStageResponse.getData().getInitiation().getDebtorAccount().getIdentification());
			debtorDetails.setName(iPaymentStageResponse.getData().getInitiation().getDebtorAccount().getName());
			String stagedDebtorSchemeName = iPaymentStageResponse.getData().getInitiation().getDebtorAccount()
					.getSchemeName();
			// Fix Scheme name validation failed. In MongoDB producing
			// exception: can't have. In field names [UK.OBIE.IBAN] while
			// creating test data. Hence, in mock data, we used "_" instead of
			// "." character
			// Replacing "_" to "." to convert valid Scheme Name into mongo db
			// adapter only. there is no impact on CMA api flow. Its specifc to
			// Sandbox functionality
			debtorDetails.setSchemeName(stagedDebtorSchemeName.replace("_", "."));
			debtorDetails.setSecondaryIdentification(
					iPaymentStageResponse.getData().getInitiation().getDebtorAccount().getSecondaryIdentification());
		}

		/* Creditor details needed on consent page */
		CreditorDetails creditorDetails = new CreditorDetails();
		if (!NullCheckUtils.isNullOrEmpty(iPaymentStageResponse.getData().getInitiation().getCreditorAccount())) {
			creditorDetails.setIdentification(
					iPaymentStageResponse.getData().getInitiation().getCreditorAccount().getIdentification());
			creditorDetails.setName(iPaymentStageResponse.getData().getInitiation().getCreditorAccount().getName());
			creditorDetails.setSchemeName(
					iPaymentStageResponse.getData().getInitiation().getCreditorAccount().getSchemeName());
			creditorDetails.setSecondaryIdentification(
					iPaymentStageResponse.getData().getInitiation().getCreditorAccount().getSecondaryIdentification());
		}

		/* Remittance details needed on consent page */
		RemittanceDetails remittanceDetails = new RemittanceDetails();
		if (!NullCheckUtils.isNullOrEmpty(iPaymentStageResponse.getData().getInitiation().getRemittanceInformation())) {
			remittanceDetails.setReference(
					iPaymentStageResponse.getData().getInitiation().getRemittanceInformation().getReference());
			remittanceDetails.setUnstructured(
					iPaymentStageResponse.getData().getInitiation().getRemittanceInformation().getUnstructured());
		}

		/* Charge details needed on consent page */
		ChargeDetails chargeDetails = new ChargeDetails();
		chargeDetails.setChargesList(iPaymentStageResponse.getData().getCharges());

		/* Exchange Rate for International Payment on consent page */
		ExchangeRateDetails exchangeRateDetails = new ExchangeRateDetails();
		if (!NullCheckUtils.isNullOrEmpty(iPaymentStageResponse.getData().getExchangeRateInformation())) {
			exchangeRateDetails
					.setExchangeRate(iPaymentStageResponse.getData().getExchangeRateInformation().getExchangeRate());
			exchangeRateDetails.setContractIdentification(
					iPaymentStageResponse.getData().getExchangeRateInformation().getContractIdentification());
			exchangeRateDetails.setExpirationDateTime(
					iPaymentStageResponse.getData().getExchangeRateInformation().getExpirationDateTime());
			exchangeRateDetails.setRateType(iPaymentStageResponse.getData().getExchangeRateInformation().getRateType());
			exchangeRateDetails
					.setUnitCurrency(iPaymentStageResponse.getData().getExchangeRateInformation().getUnitCurrency());
		}

		response.setAmountDetails(amountDetails);
		response.setChargeDetails(chargeDetails);
		response.setCreditorDetails(creditorDetails);
		response.setDebtorDetails(debtorDetails);
		response.setRemittanceDetails(remittanceDetails);
		response.setExchangeRateDetails(exchangeRateDetails);
		return response;
	}

	@Override
	public CustomFraudSystemPaymentData transformDStageToFraudData(
			CustomIPaymentConsentsPOSTResponse domesticStageResponse) {
		CustomFraudSystemPaymentData fraudSystemData = new CustomFraudSystemPaymentData();
		fraudSystemData.setPaymentType(PaymentTypeEnum.INTERNATIONAL_PAY.getPaymentType());
		fraudSystemData
				.setTransferAmount(domesticStageResponse.getData().getInitiation().getInstructedAmount().getAmount());
		fraudSystemData.setTransferCurrency(
				domesticStageResponse.getData().getInitiation().getInstructedAmount().getCurrency());
		if (!NullCheckUtils.isNullOrEmpty(domesticStageResponse.getData().getInitiation().getRemittanceInformation()))
			fraudSystemData.setTransferMemo(
					domesticStageResponse.getData().getInitiation().getRemittanceInformation().getReference());
		fraudSystemData.setTransferTime(domesticStageResponse.getData().getCreationDateTime());
		CreditorDetails fraudSystenCreditorInfo = new CreditorDetails();
		fraudSystenCreditorInfo.setIdentification(
				domesticStageResponse.getData().getInitiation().getCreditorAccount().getIdentification());
		fraudSystemData.setCreditorDetails(fraudSystenCreditorInfo);

		return fraudSystemData;
	}

	@Override
	public void updateStagedPaymentConsents(CustomPaymentStageIdentifiers customStageIdentifiers,
			CustomPaymentStageUpdateData stageUpdateData, Map<String, String> params) {

		CustomIPaymentConsentsPOSTResponse updatedStagedResource = iPaymentConsentsBankRepository
				.findOneByDataConsentId(customStageIdentifiers.getPaymentConsentId());
		if (stageUpdateData.getDebtorDetailsUpdated() == Boolean.TRUE) {
			updatedStagedResource.getData().getInitiation().setDebtorAccount(stageUpdateData.getDebtorDetails());
		}
		if (stageUpdateData.getFraudScoreUpdated() == Boolean.TRUE) {
			updatedStagedResource.setFraudScore(stageUpdateData.getFraudScore());
		}
		if (stageUpdateData.getSetupStatusUpdated() == Boolean.TRUE) {
			updatedStagedResource.getData()
					.setStatus(OBExternalConsentStatus1Code.fromValue(stageUpdateData.getSetupStatus()));
			updatedStagedResource.getData().setStatusUpdateDateTime(stageUpdateData.getSetupStatusUpdateDateTime());
		}
		try {
			iPaymentConsentsBankRepository.save(updatedStagedResource);
		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.PISP_TECHNICAL_ERROR_FS_PRE_STAGE_UPDATION));
		}

	}

}
