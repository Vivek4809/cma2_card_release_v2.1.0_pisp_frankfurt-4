package com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Status of the payment instruction proposal
 */
public enum ProposalStatus {
  
	AwaitingAuthorisation("AwaitingAuthorisation"),
  
	Authorised("Authorised"),
  
	Consumed("Consumed"),
  
	Rejected("Rejected");

  private String value;

  ProposalStatus(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static ProposalStatus fromValue(String text) {
    for (ProposalStatus b : ProposalStatus.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

