package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.util.Objects;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Amount of the payment instruction
 */
@ApiModel(description = "Amount of the payment instruction")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class PaymentAmount   {
  @JsonProperty("transactionCurrency")
  private Double transactionCurrency = null;

  @JsonProperty("accountCurrency")
  private Double accountCurrency = null;

  public PaymentAmount transactionCurrency(Double transactionCurrency) {
    this.transactionCurrency = transactionCurrency;
    return this;
  }

  /**
   * The actual value in the transaction currency
   * @return transactionCurrency
  **/
  @ApiModelProperty(value = "The actual value in the transaction currency")


  public Double getTransactionCurrency() {
    return transactionCurrency;
  }

  public void setTransactionCurrency(Double transactionCurrency) {
    this.transactionCurrency = transactionCurrency;
  }

  public PaymentAmount accountCurrency(Double accountCurrency) {
    this.accountCurrency = accountCurrency;
    return this;
  }

  /**
   * The actual value in the account currency
   * @return accountCurrency
  **/
  @ApiModelProperty(value = "The actual value in the account currency")


  public Double getAccountCurrency() {
    return accountCurrency;
  }

  public void setAccountCurrency(Double accountCurrency) {
    this.accountCurrency = accountCurrency;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PaymentAmount paymentAmount = (PaymentAmount) o;
    return Objects.equals(this.transactionCurrency, paymentAmount.transactionCurrency) &&
        Objects.equals(this.accountCurrency, paymentAmount.accountCurrency);
  }

  @Override
  public int hashCode() {
    return Objects.hash(transactionCurrency, accountCurrency);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaymentAmount {\n");
    
    sb.append("    transactionCurrency: ").append(toIndentedString(transactionCurrency)).append("\n");
    sb.append("    accountCurrency: ").append(toIndentedString(accountCurrency)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

