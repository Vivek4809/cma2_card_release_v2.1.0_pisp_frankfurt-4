package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Status of the payment instruction proposal
 */
public enum ProposalStatus {
  
  AWAITINGAUTHORISATION("AwaitingAuthorisation"),
  
  AUTHORISED("Authorised"),
  
  CONSUMED("Consumed"),
  
  REJECTED("Rejected");

  private String value;

  ProposalStatus(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static ProposalStatus fromValue(String text) {
    for (ProposalStatus b : ProposalStatus.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

