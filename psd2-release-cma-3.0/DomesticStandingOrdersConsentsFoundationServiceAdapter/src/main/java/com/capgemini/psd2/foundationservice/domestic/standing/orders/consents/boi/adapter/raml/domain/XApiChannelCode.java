package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * The short identifier by which the Channel is known (BOL or 365). User permissions will be validated using the database for the channel.
 */
public enum XApiChannelCode {
  
  BOL("BOL"),
  
  B365("B365");

  private String value;

  XApiChannelCode(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static XApiChannelCode fromValue(String text) {
    for (XApiChannelCode b : XApiChannelCode.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

