package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Payment Instruction Proposal ( aka Payment Order Consent ).
 */
@ApiModel(description = "Payment Instruction Proposal ( aka Payment Order Consent ).")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class PaymentInstructionProposal1   {
  @JsonProperty("paymentInstructionProposalId")
  private String paymentInstructionProposalId = null;

  @JsonProperty("proposalCreationDatetime")
  private OffsetDateTime proposalCreationDatetime = null;

  @JsonProperty("proposalStatus")
  private ProposalStatus proposalStatus = null;

  @JsonProperty("proposalStatusUpdateDatetime")
  private OffsetDateTime proposalStatusUpdateDatetime = null;

  @JsonProperty("authorisationType")
  private AuthorisationType authorisationType = null;

  @JsonProperty("authorisationDatetime")
  private OffsetDateTime authorisationDatetime = null;

  @JsonProperty("proposingParty")
  private Party proposingParty = null;

  @JsonProperty("proposingPartyPostalAddress")
  private PaymentInstructionPostalAddress proposingPartyPostalAddress = null;

  @JsonProperty("proposingPartyAccount")
  private ProposingPartyAccount proposingPartyAccount = null;

  @JsonProperty("authorisingParty")
  private Party authorisingParty = null;

  @JsonProperty("authorisingPartyAccount")
  private AuthorisingPartyAccount authorisingPartyAccount = null;

  @JsonProperty("paymentInstructionRiskFactorReference")
  private PaymentInstrumentRiskFactor paymentInstructionRiskFactorReference = null;

  @JsonProperty("paymentType")
  private PaymentType paymentType = null;

  @JsonProperty("charges")
  @Valid
  private List<PaymentInstructionCharge> charges = null;

  @JsonProperty("fraudSystemResponse")
  private FraudSystemResponse fraudSystemResponse = null;

  public PaymentInstructionProposal1 paymentInstructionProposalId(String paymentInstructionProposalId) {
    this.paymentInstructionProposalId = paymentInstructionProposalId;
    return this;
  }

  /**
   * The identifier of the Payment Instruction Proposal in the Banking System
   * @return paymentInstructionProposalId
  **/
  @ApiModelProperty(required = true, value = "The identifier of the Payment Instruction Proposal in the Banking System")
  @NotNull

@Size(min=0,max=35) 
  public String getPaymentInstructionProposalId() {
    return paymentInstructionProposalId;
  }

  public void setPaymentInstructionProposalId(String paymentInstructionProposalId) {
    this.paymentInstructionProposalId = paymentInstructionProposalId;
  }

  public PaymentInstructionProposal1 proposalCreationDatetime(OffsetDateTime proposalCreationDatetime) {
    this.proposalCreationDatetime = proposalCreationDatetime;
    return this;
  }

  /**
   * The risk profile associated
   * @return proposalCreationDatetime
  **/
  @ApiModelProperty(required = true, value = "The risk profile associated")
  @NotNull

  @Valid

  public OffsetDateTime getProposalCreationDatetime() {
    return proposalCreationDatetime;
  }

  public void setProposalCreationDatetime(OffsetDateTime proposalCreationDatetime) {
    this.proposalCreationDatetime = proposalCreationDatetime;
  }

  public PaymentInstructionProposal1 proposalStatus(ProposalStatus proposalStatus) {
    this.proposalStatus = proposalStatus;
    return this;
  }

  /**
   * Get proposalStatus
   * @return proposalStatus
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public ProposalStatus getProposalStatus() {
    return proposalStatus;
  }

  public void setProposalStatus(ProposalStatus proposalStatus) {
    this.proposalStatus = proposalStatus;
  }

  public PaymentInstructionProposal1 proposalStatusUpdateDatetime(OffsetDateTime proposalStatusUpdateDatetime) {
    this.proposalStatusUpdateDatetime = proposalStatusUpdateDatetime;
    return this;
  }

  /**
   * Reflects the last time proposalStatus is updated.
   * @return proposalStatusUpdateDatetime
  **/
  @ApiModelProperty(value = "Reflects the last time proposalStatus is updated.")

  @Valid

  public OffsetDateTime getProposalStatusUpdateDatetime() {
    return proposalStatusUpdateDatetime;
  }

  public void setProposalStatusUpdateDatetime(OffsetDateTime proposalStatusUpdateDatetime) {
    this.proposalStatusUpdateDatetime = proposalStatusUpdateDatetime;
  }

  public PaymentInstructionProposal1 authorisationType(AuthorisationType authorisationType) {
    this.authorisationType = authorisationType;
    return this;
  }

  /**
   * Get authorisationType
   * @return authorisationType
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public AuthorisationType getAuthorisationType() {
    return authorisationType;
  }

  public void setAuthorisationType(AuthorisationType authorisationType) {
    this.authorisationType = authorisationType;
  }

  public PaymentInstructionProposal1 authorisationDatetime(OffsetDateTime authorisationDatetime) {
    this.authorisationDatetime = authorisationDatetime;
    return this;
  }

  /**
   * Moment on which the proposal was authorised.
   * @return authorisationDatetime
  **/
  @ApiModelProperty(value = "Moment on which the proposal was authorised.")

  @Valid

  public OffsetDateTime getAuthorisationDatetime() {
    return authorisationDatetime;
  }

  public void setAuthorisationDatetime(OffsetDateTime authorisationDatetime) {
    this.authorisationDatetime = authorisationDatetime;
  }

  public PaymentInstructionProposal1 proposingParty(Party proposingParty) {
    this.proposingParty = proposingParty;
    return this;
  }

  /**
   * Get proposingParty
   * @return proposingParty
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Party getProposingParty() {
    return proposingParty;
  }

  public void setProposingParty(Party proposingParty) {
    this.proposingParty = proposingParty;
  }

  public PaymentInstructionProposal1 proposingPartyPostalAddress(PaymentInstructionPostalAddress proposingPartyPostalAddress) {
    this.proposingPartyPostalAddress = proposingPartyPostalAddress;
    return this;
  }

  /**
   * Get proposingPartyPostalAddress
   * @return proposingPartyPostalAddress
  **/
  @ApiModelProperty(value = "")

  @Valid

  public PaymentInstructionPostalAddress getProposingPartyPostalAddress() {
    return proposingPartyPostalAddress;
  }

  public void setProposingPartyPostalAddress(PaymentInstructionPostalAddress proposingPartyPostalAddress) {
    this.proposingPartyPostalAddress = proposingPartyPostalAddress;
  }

  public PaymentInstructionProposal1 proposingPartyAccount(ProposingPartyAccount proposingPartyAccount) {
    this.proposingPartyAccount = proposingPartyAccount;
    return this;
  }

  /**
   * Get proposingPartyAccount
   * @return proposingPartyAccount
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public ProposingPartyAccount getProposingPartyAccount() {
    return proposingPartyAccount;
  }

  public void setProposingPartyAccount(ProposingPartyAccount proposingPartyAccount) {
    this.proposingPartyAccount = proposingPartyAccount;
  }

  public PaymentInstructionProposal1 authorisingParty(Party authorisingParty) {
    this.authorisingParty = authorisingParty;
    return this;
  }

  /**
   * Get authorisingParty
   * @return authorisingParty
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Party getAuthorisingParty() {
    return authorisingParty;
  }

  public void setAuthorisingParty(Party authorisingParty) {
    this.authorisingParty = authorisingParty;
  }

  public PaymentInstructionProposal1 authorisingPartyAccount(AuthorisingPartyAccount authorisingPartyAccount) {
    this.authorisingPartyAccount = authorisingPartyAccount;
    return this;
  }

  /**
   * Get authorisingPartyAccount
   * @return authorisingPartyAccount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public AuthorisingPartyAccount getAuthorisingPartyAccount() {
    return authorisingPartyAccount;
  }

  public void setAuthorisingPartyAccount(AuthorisingPartyAccount authorisingPartyAccount) {
    this.authorisingPartyAccount = authorisingPartyAccount;
  }

  public PaymentInstructionProposal1 paymentInstructionRiskFactorReference(PaymentInstrumentRiskFactor paymentInstructionRiskFactorReference) {
    this.paymentInstructionRiskFactorReference = paymentInstructionRiskFactorReference;
    return this;
  }

  /**
   * Get paymentInstructionRiskFactorReference
   * @return paymentInstructionRiskFactorReference
  **/
  @ApiModelProperty(value = "")

  @Valid

  public PaymentInstrumentRiskFactor getPaymentInstructionRiskFactorReference() {
    return paymentInstructionRiskFactorReference;
  }

  public void setPaymentInstructionRiskFactorReference(PaymentInstrumentRiskFactor paymentInstructionRiskFactorReference) {
    this.paymentInstructionRiskFactorReference = paymentInstructionRiskFactorReference;
  }

  public PaymentInstructionProposal1 paymentType(PaymentType paymentType) {
    this.paymentType = paymentType;
    return this;
  }

  /**
   * Get paymentType
   * @return paymentType
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public PaymentType getPaymentType() {
    return paymentType;
  }

  public void setPaymentType(PaymentType paymentType) {
    this.paymentType = paymentType;
  }

  public PaymentInstructionProposal1 charges(List<PaymentInstructionCharge> charges) {
    this.charges = charges;
    return this;
  }

  public PaymentInstructionProposal1 addChargesItem(PaymentInstructionCharge chargesItem) {
    if (this.charges == null) {
      this.charges = new ArrayList<PaymentInstructionCharge>();
    }
    this.charges.add(chargesItem);
    return this;
  }

  /**
   * Get charges
   * @return charges
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<PaymentInstructionCharge> getCharges() {
    return charges;
  }

  public void setCharges(List<PaymentInstructionCharge> charges) {
    this.charges = charges;
  }

  public PaymentInstructionProposal1 fraudSystemResponse(FraudSystemResponse fraudSystemResponse) {
    this.fraudSystemResponse = fraudSystemResponse;
    return this;
  }

  /**
   * Get fraudSystemResponse
   * @return fraudSystemResponse
  **/
  @ApiModelProperty(value = "")

  @Valid

  public FraudSystemResponse getFraudSystemResponse() {
    return fraudSystemResponse;
  }

  public void setFraudSystemResponse(FraudSystemResponse fraudSystemResponse) {
    this.fraudSystemResponse = fraudSystemResponse;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PaymentInstructionProposal1 paymentInstructionProposal1 = (PaymentInstructionProposal1) o;
    return Objects.equals(this.paymentInstructionProposalId, paymentInstructionProposal1.paymentInstructionProposalId) &&
        Objects.equals(this.proposalCreationDatetime, paymentInstructionProposal1.proposalCreationDatetime) &&
        Objects.equals(this.proposalStatus, paymentInstructionProposal1.proposalStatus) &&
        Objects.equals(this.proposalStatusUpdateDatetime, paymentInstructionProposal1.proposalStatusUpdateDatetime) &&
        Objects.equals(this.authorisationType, paymentInstructionProposal1.authorisationType) &&
        Objects.equals(this.authorisationDatetime, paymentInstructionProposal1.authorisationDatetime) &&
        Objects.equals(this.proposingParty, paymentInstructionProposal1.proposingParty) &&
        Objects.equals(this.proposingPartyPostalAddress, paymentInstructionProposal1.proposingPartyPostalAddress) &&
        Objects.equals(this.proposingPartyAccount, paymentInstructionProposal1.proposingPartyAccount) &&
        Objects.equals(this.authorisingParty, paymentInstructionProposal1.authorisingParty) &&
        Objects.equals(this.authorisingPartyAccount, paymentInstructionProposal1.authorisingPartyAccount) &&
        Objects.equals(this.paymentInstructionRiskFactorReference, paymentInstructionProposal1.paymentInstructionRiskFactorReference) &&
        Objects.equals(this.paymentType, paymentInstructionProposal1.paymentType) &&
        Objects.equals(this.charges, paymentInstructionProposal1.charges) &&
        Objects.equals(this.fraudSystemResponse, paymentInstructionProposal1.fraudSystemResponse);
  }

  @Override
  public int hashCode() {
    return Objects.hash(paymentInstructionProposalId, proposalCreationDatetime, proposalStatus, proposalStatusUpdateDatetime, authorisationType, authorisationDatetime, proposingParty, proposingPartyPostalAddress, proposingPartyAccount, authorisingParty, authorisingPartyAccount, paymentInstructionRiskFactorReference, paymentType, charges, fraudSystemResponse);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaymentInstructionProposal1 {\n");
    
    sb.append("    paymentInstructionProposalId: ").append(toIndentedString(paymentInstructionProposalId)).append("\n");
    sb.append("    proposalCreationDatetime: ").append(toIndentedString(proposalCreationDatetime)).append("\n");
    sb.append("    proposalStatus: ").append(toIndentedString(proposalStatus)).append("\n");
    sb.append("    proposalStatusUpdateDatetime: ").append(toIndentedString(proposalStatusUpdateDatetime)).append("\n");
    sb.append("    authorisationType: ").append(toIndentedString(authorisationType)).append("\n");
    sb.append("    authorisationDatetime: ").append(toIndentedString(authorisationDatetime)).append("\n");
    sb.append("    proposingParty: ").append(toIndentedString(proposingParty)).append("\n");
    sb.append("    proposingPartyPostalAddress: ").append(toIndentedString(proposingPartyPostalAddress)).append("\n");
    sb.append("    proposingPartyAccount: ").append(toIndentedString(proposingPartyAccount)).append("\n");
    sb.append("    authorisingParty: ").append(toIndentedString(authorisingParty)).append("\n");
    sb.append("    authorisingPartyAccount: ").append(toIndentedString(authorisingPartyAccount)).append("\n");
    sb.append("    paymentInstructionRiskFactorReference: ").append(toIndentedString(paymentInstructionRiskFactorReference)).append("\n");
    sb.append("    paymentType: ").append(toIndentedString(paymentType)).append("\n");
    sb.append("    charges: ").append(toIndentedString(charges)).append("\n");
    sb.append("    fraudSystemResponse: ").append(toIndentedString(fraudSystemResponse)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

