package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets brandCode
 */
public enum BrandCode {
  
  AA("AA"),
  
  PO("PO"),
  
  BOI_ROI("BOI-ROI"),
  
  BOI_NI("BOI-NI"),
  
  BOI_GB("BOI-GB");

  private String value;

  BrandCode(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static BrandCode fromValue(String text) {
    for (BrandCode b : BrandCode.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

