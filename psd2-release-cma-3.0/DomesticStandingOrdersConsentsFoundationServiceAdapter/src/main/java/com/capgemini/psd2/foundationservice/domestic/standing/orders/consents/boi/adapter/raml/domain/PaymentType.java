package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Type of payment
 */
public enum PaymentType {
  
  DOMESTIC("Domestic"),
  
  SEPA("SEPA"),
  
  INTERNATIONAL("International"),
  
  NOTSUPPORTED("NotSupported");

  private String value;

  PaymentType(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static PaymentType fromValue(String text) {
    for (PaymentType b : PaymentType.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

