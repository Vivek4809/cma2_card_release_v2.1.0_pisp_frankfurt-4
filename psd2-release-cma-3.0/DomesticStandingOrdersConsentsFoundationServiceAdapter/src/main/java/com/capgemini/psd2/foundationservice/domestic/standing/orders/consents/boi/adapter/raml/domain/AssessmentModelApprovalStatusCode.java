package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets assessmentModelApprovalStatusCode
 */
public enum AssessmentModelApprovalStatusCode {
  
  ACCEPT("Accept"),
  
  REJECT("Reject"),
  
  REFER("Refer");

  private String value;

  AssessmentModelApprovalStatusCode(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static AssessmentModelApprovalStatusCode fromValue(String text) {
    for (AssessmentModelApprovalStatusCode b : AssessmentModelApprovalStatusCode.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

