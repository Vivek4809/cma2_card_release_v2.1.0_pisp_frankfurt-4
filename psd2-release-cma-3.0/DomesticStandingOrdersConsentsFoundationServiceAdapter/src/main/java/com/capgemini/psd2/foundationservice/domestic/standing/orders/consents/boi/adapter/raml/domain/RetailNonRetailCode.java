package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets retailNonRetailCode
 */
public enum RetailNonRetailCode {
  
  RETAIL("Retail"),
  
  NON_RETAIL("Non-Retail");

  private String value;

  RetailNonRetailCode(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static RetailNonRetailCode fromValue(String text) {
    for (RetailNonRetailCode b : RetailNonRetailCode.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

