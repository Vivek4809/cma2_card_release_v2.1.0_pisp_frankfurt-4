/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.constants;

/**
 * The Class AccountStandingOrderFoundationServiceConstants.
 */
public class AccountStandingOrderFoundationServiceConstants {
	public static final String ACCOUNT_ID = "account_ID";
	public static final String YEARLY = "Yearly";
	public static final String MONTHLY = "Monthly";
	public static final String FORTNIGHTLY = "Fortnightly";
	public static final String WEEKLY = "Weekly";
	public static final String DAILY = "Daily";
    public static final String ACCEPTEDSETTLEMENTCOMPLETED = "AcceptedSettlementCompleted";
	public static final String ACCEPTEDSETTLEMENTINPROCESS = "AcceptedSettlementInProcess";
	public static final String PENDING = "Pending";
	public static final String REJECTED = "Rejected";
	public static final String ACTIVE = "Active";
	public static final String INACTIVE = "Inactive";

}
