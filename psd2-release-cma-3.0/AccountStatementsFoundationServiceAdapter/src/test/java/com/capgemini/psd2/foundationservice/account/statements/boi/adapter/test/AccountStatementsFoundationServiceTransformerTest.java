package com.capgemini.psd2.foundationservice.account.statements.boi.adapter.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.constants.AccountStatementsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.domain.Statement;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.domain.StatementsResponse;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.transformer.AccountStatementsFoundationServiceTransformer;
import com.capgemini.psd2.validator.impl.PSD2ValidatorImpl;



@RunWith(SpringJUnit4ClassRunner.class)
public class AccountStatementsFoundationServiceTransformerTest {

	
	/** The psd 2 validator. */
	@Mock
	private PSD2ValidatorImpl psd2Validator;

	/** The account balance FS transformer. */
	@InjectMocks
	private AccountStatementsFoundationServiceTransformer accountStatementsFileFSTransformer;
	
	@Mock
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	/**
	 * Test account statements file FS.
	 */

	@Test
	public void transformAccountStatementRes()
	{
		
		StatementsResponse statements = new StatementsResponse();
		Statement stmt = new Statement();
		List<Statement> statementsList = new ArrayList<Statement>();
		stmt.setDocumentIdentificationNumber("abcd");
		stmt.setDocumentIssuanceDate(new Date(2018-02-01));
		stmt.setStatementReasonDescription("Annual Summary Statement");
		statementsList.add(stmt);
		statementsList.add(stmt);
		statements.setStatementsList(statementsList);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("accountId", "1234");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE,"CreditCard");
		accountStatementsFileFSTransformer.transformAccountStatementRes(statements, params);
	}
	
	@Test
	public void transformAccountStatementRes2()
	{
		
		StatementsResponse statements = new StatementsResponse();
		Statement stmt = new Statement();
		List<Statement> statementsList = new ArrayList<Statement>();
		stmt.setDocumentIssuanceDate(new Date(2018-02-01));
		statementsList.add(stmt);
		stmt.setDocumentIdentificationNumber("abcd");
		stmt.setStatementReasonDescription("Commercial Summary Statement");
		statements.setStatementsList(statementsList);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("accountId", "1234");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE,"CreditCard");
		accountStatementsFileFSTransformer.transformAccountStatementRes(statements, params);
	}
	
	
	@Test
	public void transformAccountStatementRes3()
	{
		
		StatementsResponse statements = new StatementsResponse();
		List<Statement> statementsList = new ArrayList<Statement>();
		Statement stmt = new Statement();
		stmt.setDocumentIssuanceDate(new Date(2018-02-01));
		stmt.setDocumentIdentificationNumber("abcd");
		stmt.setStatementReasonDescription("Commercial Monthly Statement");
		statementsList.add(stmt);
		statements.setStatementsList(statementsList);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("accountId", "1234");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE,"CreditCard");
		accountStatementsFileFSTransformer.transformAccountStatementRes(statements, params);
	}
	
	
	@Test
	public void transformAccountStatementRes4()
	{
		
		StatementsResponse statements = new StatementsResponse();
		Statement stmt = new Statement();
		List<Statement> statementsList = new ArrayList<Statement>();
		stmt.setDocumentIssuanceDate(new Date(2018-02-01));
		statementsList.add(stmt);
		stmt.setDocumentIdentificationNumber("abcd");
		stmt.setStatementReasonDescription("RegularPeriodic");
		statements.setStatementsList(statementsList);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("accountId", "1234");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE,"CreditCard");
		accountStatementsFileFSTransformer.transformAccountStatementRes(statements, params);
	}
	
	@Test
	public void transformAccountStatementRes5()
	{
		
		StatementsResponse statements = new StatementsResponse();
		List<Statement> statementsList = new ArrayList<Statement>();
		Statement stmt = new Statement();
		stmt.setDocumentIssuanceDate(new Date(2018-02-01));
		stmt.setDocumentIdentificationNumber("abcd");
		stmt.setStatementReasonDescription("Regular Monthly Statement");
		statementsList.add(stmt);
		statements.setStatementsList(statementsList);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("accountId", "1234");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE,"CreditCard");
		accountStatementsFileFSTransformer.transformAccountStatementRes(statements, params);
	}
	
	@Test
	public void transformAccountStatementRes6()
	{
		
		StatementsResponse statements = new StatementsResponse();
		Statement stmt = new Statement();
		List<Statement> statementsList = new ArrayList<Statement>();
		stmt.setDocumentIssuanceDate(new Date(2018-02-01));
		statementsList.add(stmt);
		stmt.setDocumentIdentificationNumber("abcd");
		stmt.setStatementReasonDescription("Annual Summary Statement");
		statements.setStatementsList(statementsList);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("accountId", "1234");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE,"CreditCard");
		accountStatementsFileFSTransformer.transformAccountStatementRes(statements, params);
	}
	
	@Test
	public void transformAccountStatementRes7()
	{
		
		StatementsResponse statements = new StatementsResponse();
		Statement stmt = new Statement();
		List<Statement> statementsList = new ArrayList<Statement>();
		stmt.setDocumentIdentificationNumber("abcd");
		stmt.setDocumentIssuanceDate(new Date(2018-02-01));
		stmt.setStatementReasonDescription("Regular Monthly Statement");
		statementsList.add(stmt);
		statementsList.add(stmt);
		statements.setStatementsList(statementsList);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("accountId", "1234");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE,"CurrentAccount");
		accountStatementsFileFSTransformer.transformAccountStatementRes(statements, params);
	}
	
	@Test
	public void transformAccountStatementRes8()
	{
		
		StatementsResponse statements = new StatementsResponse();
		Statement stmt = new Statement();
		List<Statement> statementsList = new ArrayList<Statement>();
		stmt.setDocumentIdentificationNumber("abcd");
		stmt.setDocumentIssuanceDate(new Date(2018-02-01));
		stmt.setStatementReasonDescription("Commercial Summary Statement");
		statementsList.add(stmt);
		statementsList.add(stmt);
		statements.setStatementsList(statementsList);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("accountId", "1234");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE,"CurrentAccount");
		accountStatementsFileFSTransformer.transformAccountStatementRes(statements, params);
	}
	@Test
	public void transformAccountStatementRes9()
	{
		
		StatementsResponse statements = new StatementsResponse();
		Statement stmt = new Statement();
		List<Statement> statementsList = new ArrayList<Statement>();
		stmt.setDocumentIdentificationNumber("abcd");
		stmt.setDocumentIssuanceDate(new Date(2018-02-01));
		stmt.setStatementReasonDescription("Commercial Monthly Statement");
		statementsList.add(stmt);
		statementsList.add(stmt);
		statements.setStatementsList(statementsList);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("accountId", "1234");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE,"CurrentAccount");
		accountStatementsFileFSTransformer.transformAccountStatementRes(statements, params);
	}
	
}
