
package com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter.client;

import org.springframework.http.HttpHeaders;

import com.capgemini.psd2.aisp.domain.OBDirectDebit1;
import com.capgemini.psd2.rest.client.model.RequestInfo;

public interface AccountDirectDebitsFoundationServiceClient {

	public OBDirectDebit1 restTransportForDirectDebitsFS(RequestInfo requestInfo, Class<OBDirectDebit1> response, HttpHeaders httpHeaders);

}
