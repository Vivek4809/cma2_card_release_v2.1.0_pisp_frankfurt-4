package com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain;

import java.util.Objects;
import io.swagger.annotations.ApiModel;
import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Specifies the status of the payment instruction resource.
 */
public enum PaymentInstructionStatusCode2 {
  
  INITIATIONCOMPLETED("InitiationCompleted"),
  
  INITIATIONFAILED("InitiationFailed"),
  
  INITIATIONPENDING("InitiationPending");

  private String value;

  PaymentInstructionStatusCode2(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static PaymentInstructionStatusCode2 fromValue(String text) {
    for (PaymentInstructionStatusCode2 b : PaymentInstructionStatusCode2.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

