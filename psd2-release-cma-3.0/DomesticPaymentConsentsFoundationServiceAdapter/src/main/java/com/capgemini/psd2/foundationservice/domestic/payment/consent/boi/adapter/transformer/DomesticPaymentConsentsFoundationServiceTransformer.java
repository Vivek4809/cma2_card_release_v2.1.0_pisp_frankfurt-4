package com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.transformer;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.Address;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.AuthorisationType;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.AuthorisingPartyAccount;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.Country;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.Currency;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.FinancialEventAmount;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.PaymentInstructionPostalAddress;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.PaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.PaymentInstrumentRiskFactor;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.ProposalStatus;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.ProposingPartyAccount;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBAddressTypeCode;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBExternalAuthorisation1Code;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalPaymentContext1Code;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticConsentResponse1;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

@Component
public class DomesticPaymentConsentsFoundationServiceTransformer {
	@Autowired
	private PSD2Validator psd2Validator;

	public <T> CustomDPaymentConsentsPOSTResponse transformDomesticPaymentResponse(T inputBalanceObj,
			Map<String, String> params) {
		/* Root Element */
		CustomDPaymentConsentsPOSTResponse customDPaymentConsentPostRes = new CustomDPaymentConsentsPOSTResponse();
		OBWriteDataDomesticConsentResponse1 obWriteDataDomesticConsentRes = new OBWriteDataDomesticConsentResponse1();
		PaymentInstructionProposal paymentIns = (PaymentInstructionProposal) inputBalanceObj;
		OBDomestic1 obDomestic1 = new OBDomestic1();
		OBRisk1 obRisk1 = new OBRisk1();

		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionProposalId())) {
			obWriteDataDomesticConsentRes.setConsentId(paymentIns.getPaymentInstructionProposalId());
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposalCreationDatetime())) {
			Date proposalCreationDatetime = paymentIns.getProposalCreationDatetime();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
			String creationDateTime = dateFormat.format(proposalCreationDatetime);
			obWriteDataDomesticConsentRes.setCreationDateTime(creationDateTime);
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposalStatus())) {
			OBExternalConsentStatus1Code status = OBExternalConsentStatus1Code
					.fromValue(paymentIns.getProposalStatus().toString());
			obWriteDataDomesticConsentRes.setStatus(status);
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposalStatusUpdateDatetime())) {
			Date statusUpdateDatetime = paymentIns.getProposalStatusUpdateDatetime();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
			String updateDateTime = dateFormat.format(statusUpdateDatetime);
			obWriteDataDomesticConsentRes.setStatusUpdateDateTime(updateDateTime);
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getInstructionReference())) {
			obDomestic1.setInstructionIdentification(paymentIns.getInstructionReference());
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getInstructionEndToEndReference())) {
			obDomestic1.setEndToEndIdentification(paymentIns.getInstructionEndToEndReference());
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getInstructionLocalInstrument())) {
			obDomestic1.setLocalInstrument(paymentIns.getInstructionLocalInstrument());
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getCharges())) {
			List<OBCharge1> charges = new ArrayList<OBCharge1>();
			OBCharge1 oBCharge1 = new OBCharge1();

			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getCharges().get(0))) {
				String chargeBearer = paymentIns.getCharges().get(0).getChargeBearer().toString();
				OBChargeBearerType1Code oBChargeBearerType1Code = OBChargeBearerType1Code.fromValue(chargeBearer);
				oBCharge1.setChargeBearer(oBChargeBearerType1Code);
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getCharges().get(0).getType())) {
				oBCharge1.setType(paymentIns.getCharges().get(0).getType());
			}

			if ((!NullCheckUtils.isNullOrEmpty(paymentIns.getCharges().get(0).getAmount()))
					&& (!NullCheckUtils.isNullOrEmpty(paymentIns.getCharges().get(0).getCurrency()))) {
				OBCharge1Amount chargeAmount = new OBCharge1Amount();
				Double chargeAmountMule = paymentIns.getCharges().get(0).getAmount().getTransactionCurrency();
				chargeAmount.setAmount(String.valueOf(chargeAmountMule));
				chargeAmount.setCurrency(paymentIns.getCharges().get(0).getCurrency().getIsoAlphaCode());
				oBCharge1.setAmount(chargeAmount);
			}

			charges.add(oBCharge1);
			obWriteDataDomesticConsentRes.setCharges(charges);
		}

		if ((!NullCheckUtils.isNullOrEmpty(paymentIns.getFinancialEventAmount().getTransactionCurrency()))
				&& (!NullCheckUtils.isNullOrEmpty(paymentIns.getTransactionCurrency().getIsoAlphaCode()))) {
			OBDomestic1InstructedAmount amountInstructed = new OBDomestic1InstructedAmount();
			Double amountMule = paymentIns.getFinancialEventAmount().getTransactionCurrency();
			amountInstructed.setAmount(BigDecimal.valueOf(amountMule).toPlainString());
			amountInstructed.setCurrency(paymentIns.getTransactionCurrency().getIsoAlphaCode());
			obDomestic1.setInstructedAmount(amountInstructed);
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getAuthorisingPartyAccount())) {
			OBCashAccountDebtor3 oBCashAccountDebtor3 = new OBCashAccountDebtor3();
			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getAuthorisingPartyAccount().getSchemeName())) {
				oBCashAccountDebtor3.setSchemeName(paymentIns.getAuthorisingPartyAccount().getSchemeName());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getAuthorisingPartyAccount().getAccountIdentification())) {
				oBCashAccountDebtor3.setIdentification(paymentIns.getAuthorisingPartyAccount().getAccountIdentification());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getAuthorisingPartyAccount().getAccountName())) {
				oBCashAccountDebtor3.setName(paymentIns.getAuthorisingPartyAccount().getAccountName());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getAuthorisingPartyAccount().getSecondaryIdentification())) {
				oBCashAccountDebtor3.setSecondaryIdentification(
						paymentIns.getAuthorisingPartyAccount().getSecondaryIdentification());
			}

			obDomestic1.setDebtorAccount(oBCashAccountDebtor3);
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyAccount())) {
			OBCashAccountCreditor2 oBCashAccountCreditor2 = new OBCashAccountCreditor2();
			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyAccount().getSchemeName())) {
				oBCashAccountCreditor2.setSchemeName(paymentIns.getProposingPartyAccount().getSchemeName());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyAccount().getAccountIdentification())) {
				oBCashAccountCreditor2.setIdentification(paymentIns.getProposingPartyAccount().getAccountIdentification());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyAccount().getAccountName())) {
				oBCashAccountCreditor2.setName(paymentIns.getProposingPartyAccount().getAccountName());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyAccount().getSecondaryIdentification())) {
				oBCashAccountCreditor2
						.setSecondaryIdentification(paymentIns.getProposingPartyAccount().getSecondaryIdentification());
			}

			obDomestic1.setCreditorAccount(oBCashAccountCreditor2);

		}

		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyPostalAddress())) {
			OBPostalAddress6 oBPostalAddress6 = null;
			boolean isInt=false;
				oBPostalAddress6 = new OBPostalAddress6();
				if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyPostalAddress().getAddressType())) {
					OBAddressTypeCode addressType = OBAddressTypeCode
							.fromValue(paymentIns.getProposingPartyPostalAddress().getAddressType());
					oBPostalAddress6.setAddressType(addressType);
					isInt=true;
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyPostalAddress().getDepartment())) {
					oBPostalAddress6.setDepartment(paymentIns.getProposingPartyPostalAddress().getDepartment());
					isInt=true;
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyPostalAddress().getSubDepartment())) {
					oBPostalAddress6.setSubDepartment(paymentIns.getProposingPartyPostalAddress().getSubDepartment());
					isInt=true;
				}

				if (!NullCheckUtils
						.isNullOrEmpty(paymentIns.getProposingPartyPostalAddress().getGeoCodeBuildingName())) {
					oBPostalAddress6
							.setStreetName(paymentIns.getProposingPartyPostalAddress().getGeoCodeBuildingName());
					isInt=true;
				}

				if (!NullCheckUtils
						.isNullOrEmpty(paymentIns.getProposingPartyPostalAddress().getGeoCodeBuildingNumber())) {
					oBPostalAddress6
							.setBuildingNumber(paymentIns.getProposingPartyPostalAddress().getGeoCodeBuildingNumber());
					isInt=true;
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyPostalAddress().getPostCodeNumber())) {
					oBPostalAddress6.setPostCode(paymentIns.getProposingPartyPostalAddress().getPostCodeNumber());
					isInt=true;
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyPostalAddress().getTownName())) {
					oBPostalAddress6.setTownName(paymentIns.getProposingPartyPostalAddress().getTownName());
					isInt=true;
				}

				if (!NullCheckUtils
						.isNullOrEmpty(paymentIns.getProposingPartyPostalAddress().getCountrySubDivision())) {
					oBPostalAddress6
							.setCountrySubDivision(paymentIns.getProposingPartyPostalAddress().getCountrySubDivision());
					isInt=true;
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyPostalAddress().getAddressCountry())) {
					if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyPostalAddress().getAddressCountry()
							.getIsoCountryAlphaTwoCode())) {
						oBPostalAddress6.setCountry(paymentIns.getProposingPartyPostalAddress().getAddressCountry()
								.getIsoCountryAlphaTwoCode());
						isInt=true;
					}
				}

				List<String> addLine = paymentIns.getProposingPartyPostalAddress().getAddressLine();
				if (!NullCheckUtils.isNullOrEmpty(addLine) && addLine.size() > 0) {
					oBPostalAddress6.setAddressLine(paymentIns.getProposingPartyPostalAddress().getAddressLine());
					isInt = true;
				}

				if(isInt)
				obDomestic1.setCreditorPostalAddress(oBPostalAddress6);
		}

		if ((!NullCheckUtils.isNullOrEmpty(paymentIns.getAuthorisingPartyReference()))
				|| (!NullCheckUtils.isNullOrEmpty(paymentIns.getAuthorisingPartyUnstructuredReference()))) {
			OBRemittanceInformation1 oBRemittanceInformation1 = new OBRemittanceInformation1();
			boolean isInt=false;
			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getAuthorisingPartyReference())) {
				oBRemittanceInformation1.setReference(paymentIns.getAuthorisingPartyReference());
				isInt=true;
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getAuthorisingPartyUnstructuredReference())) {
				oBRemittanceInformation1.setUnstructured(paymentIns.getAuthorisingPartyUnstructuredReference());
				isInt=true;
			}

			if(isInt)
			obDomestic1.setRemittanceInformation(oBRemittanceInformation1);
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getAuthorisationType())) {
			OBAuthorisation1 oBAuthorisation1 = new OBAuthorisation1();
			String authType = paymentIns.getAuthorisationType().toString();
			OBExternalAuthorisation1Code authCode = OBExternalAuthorisation1Code.fromValue(authType);
			oBAuthorisation1.setAuthorisationType(authCode);

			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getAuthorisationDatetime())) {
				Date authDateTime = paymentIns.getAuthorisationDatetime();
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
				String completionDateTime = dateFormat.format(authDateTime);
				oBAuthorisation1.setCompletionDateTime(completionDateTime);
			}

			obWriteDataDomesticConsentRes.setAuthorisation(oBAuthorisation1);
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionRiskFactorReference())) {
			OBRisk1DeliveryAddress oBRisk1DeliveryAddress = null;

			if (!NullCheckUtils
					.isNullOrEmpty(paymentIns.getPaymentInstructionRiskFactorReference().getPaymentContextCode())) {
				OBExternalPaymentContext1Code contextCode = OBExternalPaymentContext1Code
						.fromValue(paymentIns.getPaymentInstructionRiskFactorReference().getPaymentContextCode());
				obRisk1.setPaymentContextCode(contextCode);
			}

			if (!NullCheckUtils
					.isNullOrEmpty(paymentIns.getPaymentInstructionRiskFactorReference().getMerchantCategoryCode())) {
				obRisk1.setMerchantCategoryCode(
						paymentIns.getPaymentInstructionRiskFactorReference().getMerchantCategoryCode());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentIns.getPaymentInstructionRiskFactorReference().getMerchantCustomerIdentification())) {
				obRisk1.setMerchantCustomerIdentification(
						paymentIns.getPaymentInstructionRiskFactorReference().getMerchantCustomerIdentification());
			}

			if (!NullCheckUtils
					.isNullOrEmpty(paymentIns.getPaymentInstructionRiskFactorReference().getCounterPartyAddress())) {
					oBRisk1DeliveryAddress = new OBRisk1DeliveryAddress();
					if (!NullCheckUtils.isNullOrEmpty((paymentIns.getPaymentInstructionRiskFactorReference()
							.getCounterPartyAddress().getFirstAddressLine()))) {
						List<String> addressLine = new ArrayList<String>();
						if (!NullCheckUtils.isNullOrEmpty((paymentIns.getPaymentInstructionRiskFactorReference()
								.getCounterPartyAddress().getSecondAddressLine()))) {
							addressLine.add(paymentIns.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getFirstAddressLine());
							addressLine.add(paymentIns.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getSecondAddressLine());
						} else {
							addressLine.add(paymentIns.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getFirstAddressLine());
						}

						if(!NullCheckUtils.isNullOrEmpty(addressLine) && addressLine.size()>0)
						    oBRisk1DeliveryAddress.setAddressLine(addressLine);
					}

					if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionRiskFactorReference()
							.getCounterPartyAddress().getGeoCodeBuildingName())) {
						oBRisk1DeliveryAddress.setStreetName(paymentIns.getPaymentInstructionRiskFactorReference()
								.getCounterPartyAddress().getGeoCodeBuildingName());
					}

					if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionRiskFactorReference()
							.getCounterPartyAddress().getGeoCodeBuildingNumber())) {
						oBRisk1DeliveryAddress.setBuildingNumber(paymentIns.getPaymentInstructionRiskFactorReference()
								.getCounterPartyAddress().getGeoCodeBuildingNumber());
					}

					if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionRiskFactorReference()
							.getCounterPartyAddress().getPostCodeNumber())) {
						oBRisk1DeliveryAddress.setPostCode(paymentIns.getPaymentInstructionRiskFactorReference()
								.getCounterPartyAddress().getPostCodeNumber());
					}

					if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionRiskFactorReference()
							.getCounterPartyAddress().getThirdAddressLine())) {
						oBRisk1DeliveryAddress.setTownName(paymentIns.getPaymentInstructionRiskFactorReference()
								.getCounterPartyAddress().getThirdAddressLine());
					}

					if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionRiskFactorReference()
							.getCounterPartyAddress().getFourthAddressLine())) {
						String addressLine = null;
						if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionRiskFactorReference()
								.getCounterPartyAddress().getFifthAddressLine())) {
							addressLine = paymentIns.getPaymentInstructionRiskFactorReference().getCounterPartyAddress()
									.getFourthAddressLine()
									+ paymentIns.getPaymentInstructionRiskFactorReference().getCounterPartyAddress()
											.getFifthAddressLine();
						} else {
							addressLine = paymentIns.getPaymentInstructionRiskFactorReference().getCounterPartyAddress()
									.getFourthAddressLine();
						}
						oBRisk1DeliveryAddress.setCountrySubDivision(addressLine);
					}

					if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionRiskFactorReference()
							.getCounterPartyAddress().getAddressCountry().getIsoCountryAlphaTwoCode())) {
						oBRisk1DeliveryAddress.setCountry(paymentIns.getPaymentInstructionRiskFactorReference()
								.getCounterPartyAddress().getAddressCountry().getIsoCountryAlphaTwoCode());
					}
					obRisk1.setDeliveryAddress(oBRisk1DeliveryAddress);
				}
				
			}
		customDPaymentConsentPostRes.setRisk(obRisk1);
		obWriteDataDomesticConsentRes.setInitiation(obDomestic1);
		customDPaymentConsentPostRes.setData(obWriteDataDomesticConsentRes);
		return customDPaymentConsentPostRes;

	}

	public <T> CustomDPaymentConsentsPOSTResponse transformDomesticConsentResponseFromFDToAPIForInsert(
			T paymentInstructionProposalResponse, Map<String, String> params) {
		
		CustomDPaymentConsentsPOSTResponse customDPaymentConsentsPOSTResponse = new CustomDPaymentConsentsPOSTResponse();
		OBWriteDataDomesticConsentResponse1 oBWriteDataDomesticConsentResponse1 = new OBWriteDataDomesticConsentResponse1();
		PaymentInstructionProposal paymentInstructionProposal = (PaymentInstructionProposal) paymentInstructionProposalResponse;
		OBDomestic1 oBDomestic1 = new OBDomestic1();
		OBRisk1 oBRisk1 = new OBRisk1();

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalId())) {
			oBWriteDataDomesticConsentResponse1
					.setConsentId(paymentInstructionProposal.getPaymentInstructionProposalId());
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getProposalCreationDatetime())) {
			Date proposalCreationDatetime = paymentInstructionProposal.getProposalCreationDatetime();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
			String creationDateTime = dateFormat.format(proposalCreationDatetime);
			oBWriteDataDomesticConsentResponse1.setCreationDateTime(creationDateTime);
		}
	
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getProposalStatus())) {
			OBExternalConsentStatus1Code status = OBExternalConsentStatus1Code
					.fromValue(paymentInstructionProposal.getProposalStatus().toString());
			oBWriteDataDomesticConsentResponse1.setStatus(status);
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getProposalStatusUpdateDatetime())) {
			Date statusUpdateDatetime = paymentInstructionProposal.getProposalStatusUpdateDatetime();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
			String updateDateTime = dateFormat.format(statusUpdateDatetime);
			oBWriteDataDomesticConsentResponse1.setStatusUpdateDateTime(updateDateTime);
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getCharges())) {
			List<OBCharge1> charges = new ArrayList<OBCharge1>();
			OBCharge1 oBCharge1 = new OBCharge1();

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getCharges().get(0))) {
				String chargeBearer = paymentInstructionProposal.getCharges().get(0).getChargeBearer().toString();
				OBChargeBearerType1Code oBChargeBearerType1Code = OBChargeBearerType1Code.fromValue(chargeBearer);
				oBCharge1.setChargeBearer(oBChargeBearerType1Code);
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getCharges().get(0).getType())) {
				oBCharge1.setType(paymentInstructionProposal.getCharges().get(0).getType());
			}

			if ((!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getCharges().get(0).getAmount()))
					&& (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getCharges().get(0).getCurrency()))) {
				OBCharge1Amount chargeAmount = new OBCharge1Amount();
				Double chargeAmountMule = paymentInstructionProposal.getCharges().get(0).getAmount()
						.getTransactionCurrency();
				chargeAmount.setAmount(String.valueOf(chargeAmountMule));
				chargeAmount
						.setCurrency(paymentInstructionProposal.getCharges().get(0).getCurrency().getIsoAlphaCode());
				oBCharge1.setAmount(chargeAmount);
			}

			charges.add(oBCharge1);
			oBWriteDataDomesticConsentResponse1.setCharges(charges);
		}
	
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getInstructionReference())) {
			oBDomestic1.setInstructionIdentification(paymentInstructionProposal.getInstructionReference());
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getInstructionEndToEndReference())) {
			oBDomestic1.setEndToEndIdentification(paymentInstructionProposal.getInstructionEndToEndReference());
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getInstructionLocalInstrument())) {
			oBDomestic1.setLocalInstrument(paymentInstructionProposal.getInstructionLocalInstrument());
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getFinancialEventAmount().getTransactionCurrency())
				&& !NullCheckUtils
						.isNullOrEmpty(paymentInstructionProposal.getTransactionCurrency().getIsoAlphaCode())) {
			OBDomestic1InstructedAmount amountInstructed = new OBDomestic1InstructedAmount();
			Double amountMule = paymentInstructionProposal.getFinancialEventAmount().getTransactionCurrency();
			amountInstructed.setAmount(BigDecimal.valueOf(amountMule).toPlainString());
			amountInstructed.setCurrency(paymentInstructionProposal.getTransactionCurrency().getIsoAlphaCode());
			oBDomestic1.setInstructedAmount(amountInstructed);
		}
	
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getAuthorisingPartyAccount())) {
			OBCashAccountDebtor3 oBCashAccountDebtor3 = new OBCashAccountDebtor3();
			if (!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposal.getAuthorisingPartyAccount().getSchemeName())) {
				oBCashAccountDebtor3
						.setSchemeName(paymentInstructionProposal.getAuthorisingPartyAccount().getSchemeName());
			}

			if (!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposal.getAuthorisingPartyAccount().getAccountIdentification())) {
				oBCashAccountDebtor3
						.setIdentification(paymentInstructionProposal.getAuthorisingPartyAccount().getAccountIdentification());
			}

			if (!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposal.getAuthorisingPartyAccount().getAccountName())) {
				oBCashAccountDebtor3.setName(paymentInstructionProposal.getAuthorisingPartyAccount().getAccountName());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getAuthorisingPartyAccount().getSecondaryIdentification())) {
				oBCashAccountDebtor3.setSecondaryIdentification(
						paymentInstructionProposal.getAuthorisingPartyAccount().getSecondaryIdentification());
			}

			oBDomestic1.setDebtorAccount(oBCashAccountDebtor3);
		}
		
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getProposingPartyAccount())) {
			OBCashAccountCreditor2 oBCashAccountCreditor2 = new OBCashAccountCreditor2();
			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getProposingPartyAccount().getSchemeName())) {
				oBCashAccountCreditor2
						.setSchemeName(paymentInstructionProposal.getProposingPartyAccount().getSchemeName());
			}

			if (!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposal.getProposingPartyAccount().getAccountIdentification())) {
				oBCashAccountCreditor2
						.setIdentification(paymentInstructionProposal.getProposingPartyAccount().getAccountIdentification());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getProposingPartyAccount().getAccountName())) {
				oBCashAccountCreditor2.setName(paymentInstructionProposal.getProposingPartyAccount().getAccountName());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getProposingPartyAccount().getSecondaryIdentification())) {
				oBCashAccountCreditor2.setSecondaryIdentification(
						paymentInstructionProposal.getProposingPartyAccount().getSecondaryIdentification());
			}

			oBDomestic1.setCreditorAccount(oBCashAccountCreditor2);

		}
	
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getProposingPartyPostalAddress())) {
			OBPostalAddress6 oBPostalAddress6 = null;
		
				oBPostalAddress6 = new OBPostalAddress6();
				if (!NullCheckUtils
						.isNullOrEmpty(paymentInstructionProposal.getProposingPartyPostalAddress().getAddressType())) {
					OBAddressTypeCode addressType = OBAddressTypeCode
							.fromValue(paymentInstructionProposal.getProposingPartyPostalAddress().getAddressType());
					oBPostalAddress6.setAddressType(addressType);
				}

				if (!NullCheckUtils
						.isNullOrEmpty(paymentInstructionProposal.getProposingPartyPostalAddress().getDepartment())) {
					oBPostalAddress6
							.setDepartment(paymentInstructionProposal.getProposingPartyPostalAddress().getDepartment());
				}

				if (!NullCheckUtils.isNullOrEmpty(
						paymentInstructionProposal.getProposingPartyPostalAddress().getSubDepartment())) {
					oBPostalAddress6.setSubDepartment(
							paymentInstructionProposal.getProposingPartyPostalAddress().getSubDepartment());
				}

				if (!NullCheckUtils.isNullOrEmpty(
						paymentInstructionProposal.getProposingPartyPostalAddress().getGeoCodeBuildingName())) {
					oBPostalAddress6.setStreetName(
							paymentInstructionProposal.getProposingPartyPostalAddress().getGeoCodeBuildingName());
				}

				if (!NullCheckUtils.isNullOrEmpty(
						paymentInstructionProposal.getProposingPartyPostalAddress().getGeoCodeBuildingNumber())) {
					oBPostalAddress6.setBuildingNumber(
							paymentInstructionProposal.getProposingPartyPostalAddress().getGeoCodeBuildingNumber());
				}

				if (!NullCheckUtils.isNullOrEmpty(
						paymentInstructionProposal.getProposingPartyPostalAddress().getPostCodeNumber())) {
					oBPostalAddress6.setPostCode(
							paymentInstructionProposal.getProposingPartyPostalAddress().getPostCodeNumber());
				}

				if (!NullCheckUtils
						.isNullOrEmpty(paymentInstructionProposal.getProposingPartyPostalAddress().getTownName())) {
					oBPostalAddress6
							.setTownName(paymentInstructionProposal.getProposingPartyPostalAddress().getTownName());
				}

				if (!NullCheckUtils.isNullOrEmpty(
						paymentInstructionProposal.getProposingPartyPostalAddress().getCountrySubDivision())) {
					oBPostalAddress6.setCountrySubDivision(
							paymentInstructionProposal.getProposingPartyPostalAddress().getCountrySubDivision());
				}

				if (!NullCheckUtils.isNullOrEmpty(
						paymentInstructionProposal.getProposingPartyPostalAddress().getAddressCountry())) {
					if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getProposingPartyPostalAddress()
							.getAddressCountry().getIsoCountryAlphaTwoCode())) {
						oBPostalAddress6.setCountry(paymentInstructionProposal.getProposingPartyPostalAddress()
								.getAddressCountry().getIsoCountryAlphaTwoCode());
					}
				}

				List<String> addLine = paymentInstructionProposal.getProposingPartyPostalAddress().getAddressLine();
				if (!NullCheckUtils.isNullOrEmpty(addLine) && addLine.size() > 0) {
					oBPostalAddress6
							.setAddressLine(paymentInstructionProposal.getProposingPartyPostalAddress().getAddressLine());
				}

			if(!NullCheckUtils.isNullOrEmpty(oBPostalAddress6))	
			oBDomestic1.setCreditorPostalAddress(oBPostalAddress6);
			
		}
	
		if ((!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getAuthorisingPartyReference()))
				|| (!NullCheckUtils
						.isNullOrEmpty(paymentInstructionProposal.getAuthorisingPartyUnstructuredReference()))) {
			OBRemittanceInformation1 oBRemittanceInformation1 = new OBRemittanceInformation1();
			boolean isInit = false;
			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getAuthorisingPartyReference())) {
				oBRemittanceInformation1.setReference(paymentInstructionProposal.getAuthorisingPartyReference());
				isInit  = true;
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getAuthorisingPartyUnstructuredReference())) {
				oBRemittanceInformation1
						.setUnstructured(paymentInstructionProposal.getAuthorisingPartyUnstructuredReference());
				isInit  = true;
			}

			if (isInit)
				oBDomestic1.setRemittanceInformation(oBRemittanceInformation1);
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getAuthorisationType())) {
			OBAuthorisation1 oBAuthorisation1 = new OBAuthorisation1();
			String authType = paymentInstructionProposal.getAuthorisationType().toString();
			OBExternalAuthorisation1Code authCode = OBExternalAuthorisation1Code.fromValue(authType);
			oBAuthorisation1.setAuthorisationType(authCode);

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getAuthorisationDatetime())) {
				Date authDateTime = paymentInstructionProposal.getAuthorisationDatetime();
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
				String completionDateTime = dateFormat.format(authDateTime);
				oBAuthorisation1.setCompletionDateTime(completionDateTime);
			}

			oBWriteDataDomesticConsentResponse1.setAuthorisation(oBAuthorisation1);
		}
	
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionRiskFactorReference())) {
			OBRisk1DeliveryAddress oBRisk1DeliveryAddress = null;

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getPaymentInstructionRiskFactorReference().getPaymentContextCode())) {
				OBExternalPaymentContext1Code contextCode = OBExternalPaymentContext1Code.fromValue(
						paymentInstructionProposal.getPaymentInstructionRiskFactorReference().getPaymentContextCode());
				oBRisk1.setPaymentContextCode(contextCode);
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getPaymentInstructionRiskFactorReference().getMerchantCategoryCode())) {
				oBRisk1.setMerchantCategoryCode(paymentInstructionProposal.getPaymentInstructionRiskFactorReference()
						.getMerchantCategoryCode());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionRiskFactorReference()
					.getMerchantCustomerIdentification())) {
				oBRisk1.setMerchantCustomerIdentification(paymentInstructionProposal
						.getPaymentInstructionRiskFactorReference().getMerchantCustomerIdentification());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress())) {
					oBRisk1DeliveryAddress = new OBRisk1DeliveryAddress();
					if (!NullCheckUtils
							.isNullOrEmpty((paymentInstructionProposal.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getFirstAddressLine()))) {
						List<String> addressLine = new ArrayList<String>();
						if (!NullCheckUtils
								.isNullOrEmpty((paymentInstructionProposal.getPaymentInstructionRiskFactorReference()
										.getCounterPartyAddress().getSecondAddressLine()))) {
							addressLine.add(paymentInstructionProposal.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getFirstAddressLine());
							addressLine.add(paymentInstructionProposal.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getSecondAddressLine());
						} else {
							addressLine.add(paymentInstructionProposal.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getFirstAddressLine());
						}

						oBRisk1DeliveryAddress.setAddressLine(addressLine);
					}

					if (!NullCheckUtils
							.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getGeoCodeBuildingName())) {
						oBRisk1DeliveryAddress
								.setStreetName(paymentInstructionProposal.getPaymentInstructionRiskFactorReference()
										.getCounterPartyAddress().getGeoCodeBuildingName());
					}

					if (!NullCheckUtils
							.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getGeoCodeBuildingNumber())) {
						oBRisk1DeliveryAddress
								.setBuildingNumber(paymentInstructionProposal.getPaymentInstructionRiskFactorReference()
										.getCounterPartyAddress().getGeoCodeBuildingNumber());
					}

					if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal
							.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getPostCodeNumber())) {
						oBRisk1DeliveryAddress
								.setPostCode(paymentInstructionProposal.getPaymentInstructionRiskFactorReference()
										.getCounterPartyAddress().getPostCodeNumber());
					}

					if (!NullCheckUtils
							.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getThirdAddressLine())) {
						oBRisk1DeliveryAddress
								.setTownName(paymentInstructionProposal.getPaymentInstructionRiskFactorReference()
										.getCounterPartyAddress().getThirdAddressLine());
					}

					if (!NullCheckUtils
							.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getFourthAddressLine())) {
						String addressLine = null;
						if (!NullCheckUtils
								.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionRiskFactorReference()
										.getCounterPartyAddress().getFifthAddressLine())) {
							addressLine = paymentInstructionProposal.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getFourthAddressLine()
									+ paymentInstructionProposal.getPaymentInstructionRiskFactorReference()
											.getCounterPartyAddress().getFifthAddressLine();
						} else {
							addressLine = paymentInstructionProposal.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getFourthAddressLine();
						}

						oBRisk1DeliveryAddress.setCountrySubDivision(addressLine);
					}

					if (!NullCheckUtils
							.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getAddressCountry().getIsoCountryAlphaTwoCode())) {
						oBRisk1DeliveryAddress
								.setCountry(paymentInstructionProposal.getPaymentInstructionRiskFactorReference()
										.getCounterPartyAddress().getAddressCountry().getIsoCountryAlphaTwoCode());
					}

					oBRisk1.setDeliveryAddress(oBRisk1DeliveryAddress);
			}

		}
		
		oBWriteDataDomesticConsentResponse1.setInitiation(oBDomestic1);
		customDPaymentConsentsPOSTResponse.setData(oBWriteDataDomesticConsentResponse1);
		customDPaymentConsentsPOSTResponse.setRisk(oBRisk1);

		ProposalStatus status = ProposalStatus.fromValue(paymentInstructionProposal.getProposalStatus().toString());

		if (((status.toString()).equals("Rejected"))
				&& (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalId()))) {
			ProcessConsentStatusEnum consentStatus = ProcessConsentStatusEnum.FAIL;
			customDPaymentConsentsPOSTResponse.setConsentPorcessStatus(consentStatus);
		} else if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalId())) {
			ProcessConsentStatusEnum consentStatus = ProcessConsentStatusEnum.PASS;
			customDPaymentConsentsPOSTResponse.setConsentPorcessStatus(consentStatus);
		}
		
		return customDPaymentConsentsPOSTResponse;
	}

	public PaymentInstructionProposal transformDomesticConsentResponseFromAPIToFDForInsert(
			CustomDPaymentConsentsPOSTRequest paymentConsentsRequest, Map<String, String> params) {

		PaymentInstructionProposal paymentInstructionProposal = new PaymentInstructionProposal();
		paymentInstructionProposal.setPaymentInstructionProposalId("");

		if (!NullCheckUtils
				.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getInstructionIdentification())) {

			paymentInstructionProposal.setInstructionReference(
					paymentConsentsRequest.getData().getInitiation().getInstructionIdentification());
		}

		if (!NullCheckUtils
				.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getEndToEndIdentification())) {

			paymentInstructionProposal.setInstructionEndToEndReference(
					paymentConsentsRequest.getData().getInitiation().getEndToEndIdentification());
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getLocalInstrument())) {
			paymentInstructionProposal.setInstructionLocalInstrument(
					paymentConsentsRequest.getData().getInitiation().getLocalInstrument());
		}

		// Instructed Amount
		if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getInstructedAmount())) {

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getInstructedAmount().getAmount())) {
				FinancialEventAmount financialEventAmount = new FinancialEventAmount();
				String amount = paymentConsentsRequest.getData().getInitiation().getInstructedAmount().getAmount();
				Double amountDouble = Double.parseDouble(amount);
				financialEventAmount.setTransactionCurrency(amountDouble);
				paymentInstructionProposal.setFinancialEventAmount(financialEventAmount);
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getInstructedAmount().getCurrency())) {
				Currency transactionCurrency = new Currency();
				String currency = paymentConsentsRequest.getData().getInitiation().getInstructedAmount().getCurrency();
				transactionCurrency.setIsoAlphaCode(currency);
				paymentInstructionProposal.setTransactionCurrency(transactionCurrency);
			}
		}
		// Debtor Account

		if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getDebtorAccount())) {
			AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();
			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getSchemeName())) {
				authorisingPartyAccount.setSchemeName(
						paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getSchemeName());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getIdentification())) {
				authorisingPartyAccount.setAccountIdentification(
						paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getIdentification());
			}

			if (!NullCheckUtils
					.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getName())) {
				authorisingPartyAccount
						.setAccountName(paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getName());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getSecondaryIdentification())) {
				authorisingPartyAccount.setSecondaryIdentification(paymentConsentsRequest.getData().getInitiation()
						.getDebtorAccount().getSecondaryIdentification());
			}

			paymentInstructionProposal.setAuthorisingPartyAccount(authorisingPartyAccount);
		}
		// Creditor Account

		if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorAccount())) {
			ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();
			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getCreditorAccount().getSchemeName())) {

				proposingPartyAccount.setSchemeName(
						paymentConsentsRequest.getData().getInitiation().getCreditorAccount().getSchemeName());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getCreditorAccount().getIdentification())) {

				proposingPartyAccount.setAccountIdentification(
						paymentConsentsRequest.getData().getInitiation().getCreditorAccount().getIdentification());
			}

			if (!NullCheckUtils
					.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorAccount().getName())) {

				proposingPartyAccount.setAccountName(
						paymentConsentsRequest.getData().getInitiation().getCreditorAccount().getName());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorAccount()
					.getSecondaryIdentification())) {
				proposingPartyAccount.setSecondaryIdentification(paymentConsentsRequest.getData().getInitiation()
						.getCreditorAccount().getSecondaryIdentification());
			}

			paymentInstructionProposal.setProposingPartyAccount(proposingPartyAccount);
		}
		// Creditor Postal Address

		if (!NullCheckUtils
				.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress())) {
			PaymentInstructionPostalAddress proposingPartyPostalAddress = new PaymentInstructionPostalAddress();
			
			boolean isInit = false;
			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getAddressType())) {
				OBAddressTypeCode addressType = paymentConsentsRequest.getData().getInitiation()
						.getCreditorPostalAddress().getAddressType();
				proposingPartyPostalAddress.setAddressType(addressType.toString());
				isInit = true;
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getDepartment())) {
				proposingPartyPostalAddress.setDepartment(
						paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getDepartment());
				isInit = true;
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getSubDepartment())) {
				proposingPartyPostalAddress.setSubDepartment(
						paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getSubDepartment());
				isInit = true;
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getStreetName())) {
				proposingPartyPostalAddress.setGeoCodeBuildingName(
						paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getStreetName());
				isInit = true;
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getBuildingNumber())) {
				proposingPartyPostalAddress.setGeoCodeBuildingNumber(paymentConsentsRequest.getData().getInitiation()
						.getCreditorPostalAddress().getBuildingNumber());
				isInit = true;
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getPostCode())) {
				proposingPartyPostalAddress.setPostCodeNumber(
						paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getPostCode());
				isInit = true;
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getTownName())) {
				proposingPartyPostalAddress.setTownName(
						paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getTownName());
				isInit = true;
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation()
					.getCreditorPostalAddress().getCountrySubDivision())) {
				proposingPartyPostalAddress.setCountrySubDivision(paymentConsentsRequest.getData().getInitiation()
						.getCreditorPostalAddress().getCountrySubDivision());
				isInit = true;
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getCountry())) {
				Country addressCountryCreditor = new Country();
				addressCountryCreditor.setIsoCountryAlphaTwoCode(
						paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getCountry());
				proposingPartyPostalAddress.setAddressCountry(addressCountryCreditor);
				isInit = true;
			}

			List<String> addLine = paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress()
					.getAddressLine();
			if (!NullCheckUtils.isNullOrEmpty(addLine) && addLine.size() > 0) {
				proposingPartyPostalAddress.setAddressLine(
						paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getAddressLine());
				isInit = true;
			}
			if (isInit)
				paymentInstructionProposal.setProposingPartyPostalAddress(proposingPartyPostalAddress);
		}
		// Remittance Information

		if (!NullCheckUtils
				.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getRemittanceInformation())) {

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getRemittanceInformation().getUnstructured())) {
				paymentInstructionProposal.setAuthorisingPartyUnstructuredReference(
						paymentConsentsRequest.getData().getInitiation().getRemittanceInformation().getUnstructured());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getRemittanceInformation().getReference())) {
				paymentInstructionProposal.setAuthorisingPartyReference(
						paymentConsentsRequest.getData().getInitiation().getRemittanceInformation().getReference());
			}
		}

		// Authorization

		if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getAuthorisation())) {

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getAuthorisation().getAuthorisationType().toString())) {
				String authorisationType = paymentConsentsRequest.getData().getAuthorisation().getAuthorisationType()
						.toString();
				paymentInstructionProposal.setAuthorisationType(AuthorisationType.fromValue(authorisationType));
			}

			if (!NullCheckUtils
					.isNullOrEmpty(paymentConsentsRequest.getData().getAuthorisation().getCompletionDateTime())) {
				String authorisationDateTime = paymentConsentsRequest.getData().getAuthorisation()
						.getCompletionDateTime();
				Date authorisationDateTime1;
				try {
					authorisationDateTime1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX")
							.parse(authorisationDateTime);
					paymentInstructionProposal.setAuthorisationDatetime(authorisationDateTime1);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		}
		// Risk

		if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getRisk())) {

			PaymentInstrumentRiskFactor paymentInstructionRiskFactorReference = new PaymentInstrumentRiskFactor();

			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getRisk().getPaymentContextCode())) {
				paymentInstructionRiskFactorReference
				.setPaymentContextCode(String.valueOf(paymentConsentsRequest.getRisk().getPaymentContextCode()));
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getRisk().getMerchantCategoryCode())) {
				paymentInstructionRiskFactorReference
						.setMerchantCategoryCode(paymentConsentsRequest.getRisk().getMerchantCategoryCode());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getRisk().getMerchantCustomerIdentification())) {
				paymentInstructionRiskFactorReference.setMerchantCustomerIdentification(
						paymentConsentsRequest.getRisk().getMerchantCustomerIdentification());
			}

			// Delivery Address

			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getRisk().getDeliveryAddress())) {
				Address counterPartyAddress = new Address();
				if (!NullCheckUtils
						.isNullOrEmpty(paymentConsentsRequest.getRisk().getDeliveryAddress().getAddressLine()) && paymentConsentsRequest.getRisk().getDeliveryAddress().getAddressLine().size()>0 ) {
					counterPartyAddress.setFirstAddressLine(
							paymentConsentsRequest.getRisk().getDeliveryAddress().getAddressLine().get(0));

					if (!NullCheckUtils.isNullOrEmpty(
							paymentConsentsRequest.getRisk().getDeliveryAddress().getAddressLine()) && paymentConsentsRequest.getRisk().getDeliveryAddress().getAddressLine().size()>1) {
						counterPartyAddress.setSecondAddressLine(
								paymentConsentsRequest.getRisk().getDeliveryAddress().getAddressLine().get(1));
					}
				}

				if (!NullCheckUtils
						.isNullOrEmpty(paymentConsentsRequest.getRisk().getDeliveryAddress().getStreetName())) {
					counterPartyAddress.setGeoCodeBuildingName(
							paymentConsentsRequest.getRisk().getDeliveryAddress().getStreetName());
				}

				if (!NullCheckUtils
						.isNullOrEmpty(paymentConsentsRequest.getRisk().getDeliveryAddress().getBuildingNumber())) {
					counterPartyAddress.setGeoCodeBuildingNumber(
							paymentConsentsRequest.getRisk().getDeliveryAddress().getBuildingNumber());
				}

				if (!NullCheckUtils
						.isNullOrEmpty(paymentConsentsRequest.getRisk().getDeliveryAddress().getPostCode())) {
					counterPartyAddress
							.setPostCodeNumber(paymentConsentsRequest.getRisk().getDeliveryAddress().getPostCode());
				}

				if (!NullCheckUtils
						.isNullOrEmpty(paymentConsentsRequest.getRisk().getDeliveryAddress().getTownName())) {
					counterPartyAddress
							.setThirdAddressLine(paymentConsentsRequest.getRisk().getDeliveryAddress().getTownName());
				}

				if (!NullCheckUtils
						.isNullOrEmpty(paymentConsentsRequest.getRisk().getDeliveryAddress().getCountrySubDivision())) {
					counterPartyAddress.setFourthAddressLine(
							paymentConsentsRequest.getRisk().getDeliveryAddress().getCountrySubDivision());
					//counterPartyAddress.setFifthAddressLine(
						//	paymentConsentsRequest.getRisk().getDeliveryAddress().getCountrySubDivision());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getRisk().getDeliveryAddress().getCountry())) {
					Country addressCountryRisk = new Country();
					addressCountryRisk.setIsoCountryAlphaTwoCode(
							paymentConsentsRequest.getRisk().getDeliveryAddress().getCountry());
					counterPartyAddress.setAddressCountry(addressCountryRisk);
				}

				paymentInstructionRiskFactorReference.setCounterPartyAddress(counterPartyAddress);
			}

			paymentInstructionProposal.setPaymentInstructionRiskFactorReference(paymentInstructionRiskFactorReference);
		}

		return paymentInstructionProposal;
	}

}
