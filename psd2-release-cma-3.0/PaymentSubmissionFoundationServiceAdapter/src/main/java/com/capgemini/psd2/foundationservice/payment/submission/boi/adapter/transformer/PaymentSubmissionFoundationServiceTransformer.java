
package com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.transformer;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.fraudnet.domain.FraudServiceResponse;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.constants.PaymentSubmissionFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.domain.DeliveryAddress;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.domain.FraudInputs;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.domain.PayeeInformation;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.domain.PayerInformation;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.domain.Payment;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.domain.PaymentInformation;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomPaymentSubmissionPOSTRequest;
import com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponseInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionExecutionResponse;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionPOSTRequest;
import com.capgemini.psd2.pisp.domain.RiskDeliveryAddress;
import com.capgemini.psd2.utilities.NullCheckUtils;


@Component
public class PaymentSubmissionFoundationServiceTransformer  {
	
	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;
	
	public PaymentInstruction transformPaymentSubmissionPOSTRequest(CustomPaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest, Map<String, String> params){
		
		PaymentSetupResponseInitiation initiation = paymentSubmissionPOSTRequest.getData().getInitiation();
		
		//PaymentSetup/Data
		PaymentInformation paymentInformation = new PaymentInformation();
		paymentInformation.setPaymentDate(timeZoneDateTimeAdapter.parseNewDateTimeFS(new Date()));	
		Payment payment = new Payment();
		payment.setChannelUserID(params.get(PSD2Constants.USER_IN_REQ_HEADER));
		paymentInformation.setInitiatingPartyName(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER));
		payment.setRequestType("ExecutePayment");
		paymentInformation.setStatus(paymentSubmissionPOSTRequest.getPaymentStatus());
		payment.setCreatedOn(timeZoneDateTimeAdapter.parseDateTimeFS(paymentSubmissionPOSTRequest.getCreatedOn()));
		paymentInformation.setCreatedDateTime(timeZoneDateTimeAdapter.parseNewDateTimeFS(new Date()));
		paymentInformation.setJurisdiction(paymentSubmissionPOSTRequest.getPayerJurisdiction());
		PayerInformation payerInformation = new PayerInformation();
		payerInformation.setPayerCurrency(paymentSubmissionPOSTRequest.getPayerCurrency());
		payment.setPaymentId(paymentSubmissionPOSTRequest.getData().getPaymentId());	
		
		//PaymentSetup/Data/Initiation
		paymentInformation.setInstructionIdentification(initiation.getInstructionIdentification());
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		paymentInstruction.setEndToEndIdentification(initiation.getEndToEndIdentification());
		BigDecimal amount = new BigDecimal(initiation.getInstructedAmount().getAmount());
		paymentInformation.setAmount(amount);
		PayeeInformation payeeInformation = new PayeeInformation();
		payeeInformation.setBeneficiaryCurrency(initiation.getInstructedAmount().getCurrency());
		
	
		//PaymentSetup/Data/Initiation/DebtorAgent
		if(!NullCheckUtils.isNullOrEmpty(initiation.getDebtorAgent())){
			if(SchemeNameEnum.BICFI.toString().equals(initiation.getDebtorAgent().getSchemeName().toString())){
				payerInformation.setBic(initiation.getDebtorAgent().getIdentification());
			} 
		}
		
		//PaymentSetup/Data/Initiation/DebtorAccount
		if(!NullCheckUtils.isNullOrEmpty(initiation.getDebtorAccount())){
			if(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER.toString().equals(initiation.getDebtorAccount().getSchemeName().toString())){	
				payerInformation.setNsc(initiation.getDebtorAccount().getIdentification().substring(0, 6));
            	payerInformation.setAccountNumber(initiation.getDebtorAccount().getIdentification().substring(6, 14));
			}
			else if(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN.toString().equals(initiation.getDebtorAccount().getSchemeName().toString())){	
				payerInformation.setIban(initiation.getDebtorAccount().getIdentification());
			}
			payerInformation.setAccountName(initiation.getDebtorAccount().getName());
			payerInformation.setAccountSecondaryID(initiation.getDebtorAccount().getSecondaryIdentification());
			
		}
		
		//PaymentSetup/Data/Initiation/CreditorAgent
		if(!NullCheckUtils.isNullOrEmpty(initiation.getCreditorAgent())){
			if(com.capgemini.psd2.pisp.domain.CreditorAgent.SchemeNameEnum.BICFI.toString().equals(initiation.getCreditorAgent().getSchemeName().toString())){
				payeeInformation.setBic(initiation.getCreditorAgent().getIdentification());
			} 
		}
		
		//PaymentSetup/Data/Initiation/CreditorAccount
		if(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER.toString().equals(initiation.getCreditorAccount().getSchemeName().toString())){
			payeeInformation.setBeneficiaryNsc(initiation.getCreditorAccount().getIdentification().substring(0, 6));
        	payeeInformation.setBeneficiaryAccountNumber(initiation.getCreditorAccount().getIdentification().substring(6, 14));
        	payeeInformation.setBeneficiaryCountry(PaymentSubmissionFoundationServiceConstants.BENEFICIARY_COUNTRY);
        	
		} else if (com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN.toString().equals(initiation.getCreditorAccount().getSchemeName().toString())) {
			payeeInformation.setIban(initiation.getCreditorAccount().getIdentification());
			payeeInformation.setBeneficiaryCountry(initiation.getCreditorAccount().getIdentification().substring(0, 2));
		}
		payeeInformation.setBeneficiaryName(initiation.getCreditorAccount().getName());
		payeeInformation.setAccountSecondaryID(initiation.getCreditorAccount().getSecondaryIdentification());
		
		if(!NullCheckUtils.isNullOrEmpty(initiation.getRemittanceInformation())){
			paymentInformation.setUnstructured(initiation.getRemittanceInformation().getUnstructured());
			paymentInformation.setBeneficiaryReference(initiation.getRemittanceInformation().getReference());
		}
		
		//PaymentSetup/Risk
		if(!NullCheckUtils.isNullOrEmpty(paymentSubmissionPOSTRequest.getRisk().getPaymentContextCode())){
			paymentInformation.setPaymentContextCode(paymentSubmissionPOSTRequest.getRisk().getPaymentContextCode().toString());
		}
		
		if(!NullCheckUtils.isNullOrEmpty(paymentSubmissionPOSTRequest.getRisk().getMerchantCategoryCode())){
			payeeInformation.setMerchentCategoryCode(paymentSubmissionPOSTRequest.getRisk().getMerchantCategoryCode());
		}
		
		if(!NullCheckUtils.isNullOrEmpty(paymentSubmissionPOSTRequest.getRisk().getMerchantCustomerIdentification())){
			payerInformation.setMerchantCustomerIdentification(paymentSubmissionPOSTRequest.getRisk().getMerchantCustomerIdentification());
		}
		
		//PaymentSetup/Risk/DeliveryAddress
		setDeliveryAddress(paymentSubmissionPOSTRequest, payeeInformation);
		
		
		//fraud input data
		Object fnResponse =  paymentSubmissionPOSTRequest.getFraudSystemResponse();
		if(fnResponse != null ){
			if(fnResponse instanceof  FraudServiceResponse){
				FraudServiceResponse fraudnetResp = (FraudServiceResponse) fnResponse;
				FraudInputs fnInputs = new FraudInputs();
				fnInputs.setSystemResponse( fraudnetResp.getDecisionType());
				paymentInformation.setFraudInputs(fnInputs);
			}
			
		}
		
        payment.setPaymentInformation(paymentInformation);
        payment.setPayerInformation(payerInformation);
        payment.setPayeeInformation(payeeInformation);
		paymentInstruction.setPayment(payment);
		
		return paymentInstruction;
		
	}

	private void setDeliveryAddress(PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest, PayeeInformation payeeInformation) {
		
		if(!NullCheckUtils.isNullOrEmpty(paymentSubmissionPOSTRequest.getRisk().getDeliveryAddress())){
			DeliveryAddress deliveryAddress = new DeliveryAddress();
			RiskDeliveryAddress riskDeliveryAddress = paymentSubmissionPOSTRequest.getRisk().getDeliveryAddress();
			if(!NullCheckUtils.isNullOrEmpty(riskDeliveryAddress.getAddressLine())){
				int addressLineLength = riskDeliveryAddress.getAddressLine().size();
				if(addressLineLength>0)
					deliveryAddress.setAddressLine1(riskDeliveryAddress.getAddressLine().get(0));
		        if(addressLineLength > 1)
		        	deliveryAddress.setAddressLine2(riskDeliveryAddress.getAddressLine().get(1));
			}
			deliveryAddress.setStreetName(riskDeliveryAddress.getStreetName());
			deliveryAddress.setBuildingNumber(riskDeliveryAddress.getBuildingNumber());
			deliveryAddress.setPostCode(riskDeliveryAddress.getPostCode());
			deliveryAddress.setTownName(riskDeliveryAddress.getTownName());
			
			if(!NullCheckUtils.isNullOrEmpty(riskDeliveryAddress.getCountrySubDivision())){
				int countrySubDivisionCount = riskDeliveryAddress.getCountrySubDivision().size();
				if(countrySubDivisionCount > 0)
					deliveryAddress.setCountrySubDivision1(riskDeliveryAddress.getCountrySubDivision().get(0));
			    if(countrySubDivisionCount > 1)
			    	deliveryAddress.setCountrySubDivision2(riskDeliveryAddress.getCountrySubDivision().get(1));
			}
			deliveryAddress.setCountry(riskDeliveryAddress.getCountry());
			payeeInformation.setDeliveryAddress(deliveryAddress);
		}
		
	}
	
	public PaymentSubmissionExecutionResponse transformPaymentSubmissionResponse(ValidationPassed validationPassed) {

		PaymentSubmissionExecutionResponse paymentSubmissionExecutionResponse = new PaymentSubmissionExecutionResponse();

		if (NullCheckUtils.isNullOrEmpty(validationPassed) || NullCheckUtils.isNullOrEmpty(validationPassed.getSuccessMessage())) {
			throw AdapterException.populatePSD2Exception("FS response is null for Stage Execute Payment.", AdapterErrorCodeEnum.TECHNICAL_ERROR);
		} else {
			paymentSubmissionExecutionResponse.setPaymentSubmissionId(validationPassed.getSuccessMessage());
			paymentSubmissionExecutionResponse.setPaymentSubmissionStatus(PaymentSubmissionFoundationServiceConstants.SUBMISSION_STATUS_PASSED);
			return paymentSubmissionExecutionResponse;
		}
	}
	
}
