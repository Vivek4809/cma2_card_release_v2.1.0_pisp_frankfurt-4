package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets primaryCardIndicator
 */
public enum PrimaryCardIndicator {
  
  Y("Y"),
  
  N("N");

  private String value;

  PrimaryCardIndicator(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static PrimaryCardIndicator fromValue(String text) {
    for (PrimaryCardIndicator b : PrimaryCardIndicator.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

