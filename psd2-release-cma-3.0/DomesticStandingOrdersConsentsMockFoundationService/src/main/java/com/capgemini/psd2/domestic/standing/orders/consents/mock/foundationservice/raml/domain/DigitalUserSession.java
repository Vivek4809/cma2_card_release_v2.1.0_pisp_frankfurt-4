package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain;

import java.time.OffsetDateTime;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Digital User Session object
 */
@ApiModel(description = "Digital User Session object")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class DigitalUserSession   {
  @JsonProperty("sessionInitiationFailureIndicator")
  private Boolean sessionInitiationFailureIndicator = null;

  @JsonProperty("sessionInitiationFailureReasonCode")
  private String sessionInitiationFailureReasonCode = null;

  @JsonProperty("sessionStartDateTime")
  private OffsetDateTime sessionStartDateTime = null;

  public DigitalUserSession sessionInitiationFailureIndicator(Boolean sessionInitiationFailureIndicator) {
    this.sessionInitiationFailureIndicator = sessionInitiationFailureIndicator;
    return this;
  }

  /**
   * Get sessionInitiationFailureIndicator
   * @return sessionInitiationFailureIndicator
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Boolean isSessionInitiationFailureIndicator() {
    return sessionInitiationFailureIndicator;
  }

  public void setSessionInitiationFailureIndicator(Boolean sessionInitiationFailureIndicator) {
    this.sessionInitiationFailureIndicator = sessionInitiationFailureIndicator;
  }

  public DigitalUserSession sessionInitiationFailureReasonCode(String sessionInitiationFailureReasonCode) {
    this.sessionInitiationFailureReasonCode = sessionInitiationFailureReasonCode;
    return this;
  }

  /**
   * Get sessionInitiationFailureReasonCode
   * @return sessionInitiationFailureReasonCode
  **/
  @ApiModelProperty(value = "")


  public String getSessionInitiationFailureReasonCode() {
    return sessionInitiationFailureReasonCode;
  }

  public void setSessionInitiationFailureReasonCode(String sessionInitiationFailureReasonCode) {
    this.sessionInitiationFailureReasonCode = sessionInitiationFailureReasonCode;
  }

  public DigitalUserSession sessionStartDateTime(OffsetDateTime sessionStartDateTime) {
    this.sessionStartDateTime = sessionStartDateTime;
    return this;
  }

  /**
   * Get sessionStartDateTime
   * @return sessionStartDateTime
  **/
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getSessionStartDateTime() {
    return sessionStartDateTime;
  }

  public void setSessionStartDateTime(OffsetDateTime sessionStartDateTime) {
    this.sessionStartDateTime = sessionStartDateTime;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DigitalUserSession digitalUserSession = (DigitalUserSession) o;
    return Objects.equals(this.sessionInitiationFailureIndicator, digitalUserSession.sessionInitiationFailureIndicator) &&
        Objects.equals(this.sessionInitiationFailureReasonCode, digitalUserSession.sessionInitiationFailureReasonCode) &&
        Objects.equals(this.sessionStartDateTime, digitalUserSession.sessionStartDateTime);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sessionInitiationFailureIndicator, sessionInitiationFailureReasonCode, sessionStartDateTime);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DigitalUserSession {\n");
    
    sb.append("    sessionInitiationFailureIndicator: ").append(toIndentedString(sessionInitiationFailureIndicator)).append("\n");
    sb.append("    sessionInitiationFailureReasonCode: ").append(toIndentedString(sessionInitiationFailureReasonCode)).append("\n");
    sb.append("    sessionStartDateTime: ").append(toIndentedString(sessionStartDateTime)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

