package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Device Fraud Response object
 */
@ApiModel(description = "Device Fraud Response object")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class DeviceFraudResponse   {
  @JsonProperty("eventIdentifier")
  private String eventIdentifier = null;

  @JsonProperty("electronicDevice")
  private ElectronicDevice electronicDevice = null;

  @JsonProperty("assessmentDecision")
  private AssessmentDecision assessmentDecision = null;

  @JsonProperty("assessmentModel")
  private AssessmentModel assessmentModel = null;

  @JsonProperty("assessmentRules")
  @Valid
  private List<AssessmentRule> assessmentRules = null;

  public DeviceFraudResponse eventIdentifier(String eventIdentifier) {
    this.eventIdentifier = eventIdentifier;
    return this;
  }

  /**
   * A unique identifier of the event
   * @return eventIdentifier
  **/
  @ApiModelProperty(required = true, value = "A unique identifier of the event")
  @NotNull


  public String getEventIdentifier() {
    return eventIdentifier;
  }

  public void setEventIdentifier(String eventIdentifier) {
    this.eventIdentifier = eventIdentifier;
  }

  public DeviceFraudResponse electronicDevice(ElectronicDevice electronicDevice) {
    this.electronicDevice = electronicDevice;
    return this;
  }

  /**
   * Get electronicDevice
   * @return electronicDevice
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ElectronicDevice getElectronicDevice() {
    return electronicDevice;
  }

  public void setElectronicDevice(ElectronicDevice electronicDevice) {
    this.electronicDevice = electronicDevice;
  }

  public DeviceFraudResponse assessmentDecision(AssessmentDecision assessmentDecision) {
    this.assessmentDecision = assessmentDecision;
    return this;
  }

  /**
   * Get assessmentDecision
   * @return assessmentDecision
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public AssessmentDecision getAssessmentDecision() {
    return assessmentDecision;
  }

  public void setAssessmentDecision(AssessmentDecision assessmentDecision) {
    this.assessmentDecision = assessmentDecision;
  }

  public DeviceFraudResponse assessmentModel(AssessmentModel assessmentModel) {
    this.assessmentModel = assessmentModel;
    return this;
  }

  /**
   * Get assessmentModel
   * @return assessmentModel
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public AssessmentModel getAssessmentModel() {
    return assessmentModel;
  }

  public void setAssessmentModel(AssessmentModel assessmentModel) {
    this.assessmentModel = assessmentModel;
  }

  public DeviceFraudResponse assessmentRules(List<AssessmentRule> assessmentRules) {
    this.assessmentRules = assessmentRules;
    return this;
  }

  public DeviceFraudResponse addAssessmentRulesItem(AssessmentRule assessmentRulesItem) {
    if (this.assessmentRules == null) {
      this.assessmentRules = new ArrayList<AssessmentRule>();
    }
    this.assessmentRules.add(assessmentRulesItem);
    return this;
  }

  /**
   * Get assessmentRules
   * @return assessmentRules
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<AssessmentRule> getAssessmentRules() {
    return assessmentRules;
  }

  public void setAssessmentRules(List<AssessmentRule> assessmentRules) {
    this.assessmentRules = assessmentRules;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DeviceFraudResponse deviceFraudResponse = (DeviceFraudResponse) o;
    return Objects.equals(this.eventIdentifier, deviceFraudResponse.eventIdentifier) &&
        Objects.equals(this.electronicDevice, deviceFraudResponse.electronicDevice) &&
        Objects.equals(this.assessmentDecision, deviceFraudResponse.assessmentDecision) &&
        Objects.equals(this.assessmentModel, deviceFraudResponse.assessmentModel) &&
        Objects.equals(this.assessmentRules, deviceFraudResponse.assessmentRules);
  }

  @Override
  public int hashCode() {
    return Objects.hash(eventIdentifier, electronicDevice, assessmentDecision, assessmentModel, assessmentRules);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DeviceFraudResponse {\n");
    
    sb.append("    eventIdentifier: ").append(toIndentedString(eventIdentifier)).append("\n");
    sb.append("    electronicDevice: ").append(toIndentedString(electronicDevice)).append("\n");
    sb.append("    assessmentDecision: ").append(toIndentedString(assessmentDecision)).append("\n");
    sb.append("    assessmentModel: ").append(toIndentedString(assessmentModel)).append("\n");
    sb.append("    assessmentRules: ").append(toIndentedString(assessmentRules)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

