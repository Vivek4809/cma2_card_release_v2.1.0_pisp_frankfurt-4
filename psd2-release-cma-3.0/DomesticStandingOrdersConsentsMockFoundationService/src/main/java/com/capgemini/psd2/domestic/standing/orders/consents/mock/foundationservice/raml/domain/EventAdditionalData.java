package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * EventAdditionalData
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class EventAdditionalData   {
  @JsonProperty("party")
  private PartyBasicInformation party = null;

  @JsonProperty("account")
  @Valid
  private List<Account> account = null;

  @JsonProperty("paymentInstruction")
  private Object paymentInstruction = null;

  public EventAdditionalData party(PartyBasicInformation party) {
    this.party = party;
    return this;
  }

  /**
   * Get party
   * @return party
  **/
  @ApiModelProperty(value = "")

  @Valid

  public PartyBasicInformation getParty() {
    return party;
  }

  public void setParty(PartyBasicInformation party) {
    this.party = party;
  }

  public EventAdditionalData account(List<Account> account) {
    this.account = account;
    return this;
  }

  public EventAdditionalData addAccountItem(Account accountItem) {
    if (this.account == null) {
      this.account = new ArrayList<Account>();
    }
    this.account.add(accountItem);
    return this;
  }

  /**
   * Get account
   * @return account
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<Account> getAccount() {
    return account;
  }

  public void setAccount(List<Account> account) {
    this.account = account;
  }

  public EventAdditionalData paymentInstruction(Object paymentInstruction) {
    this.paymentInstruction = paymentInstruction;
    return this;
  }

  /**
   * The payment instruction information from the account of the end user used in the originating event which can be a one off instruction or a standing order instruction\"
   * @return paymentInstruction
  **/
  @ApiModelProperty(value = "The payment instruction information from the account of the end user used in the originating event which can be a one off instruction or a standing order instruction\"")


  public Object getPaymentInstruction() {
    return paymentInstruction;
  }

  public void setPaymentInstruction(Object paymentInstruction) {
    this.paymentInstruction = paymentInstruction;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EventAdditionalData eventAdditionalData = (EventAdditionalData) o;
    return Objects.equals(this.party, eventAdditionalData.party) &&
        Objects.equals(this.account, eventAdditionalData.account) &&
        Objects.equals(this.paymentInstruction, eventAdditionalData.paymentInstruction);
  }

  @Override
  public int hashCode() {
    return Objects.hash(party, account, paymentInstruction);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class EventAdditionalData {\n");
    
    sb.append("    party: ").append(toIndentedString(party)).append("\n");
    sb.append("    account: ").append(toIndentedString(account)).append("\n");
    sb.append("    paymentInstruction: ").append(toIndentedString(paymentInstruction)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

