package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Type of schedule
 */
public enum ScheduleRegularityTypeCode {
  
  REGULAR("Regular"),
  
  ONCE_OFF("Once Off");

  private String value;

  ScheduleRegularityTypeCode(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static ScheduleRegularityTypeCode fromValue(String text) {
    for (ScheduleRegularityTypeCode b : ScheduleRegularityTypeCode.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

