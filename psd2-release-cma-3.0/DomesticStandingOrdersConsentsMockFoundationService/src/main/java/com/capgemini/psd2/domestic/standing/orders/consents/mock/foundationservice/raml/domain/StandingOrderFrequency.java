package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets standingOrderFrequency
 */
public enum StandingOrderFrequency {
  
  DAILY("Daily"),
  
  FORTNIGHTLY("Fortnightly"),
  
  WEEKLY("Weekly"),
  
  MONTHLY("Monthly"),
  
  YEARLY("Yearly");

  private String value;

  StandingOrderFrequency(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static StandingOrderFrequency fromValue(String text) {
    for (StandingOrderFrequency b : StandingOrderFrequency.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

