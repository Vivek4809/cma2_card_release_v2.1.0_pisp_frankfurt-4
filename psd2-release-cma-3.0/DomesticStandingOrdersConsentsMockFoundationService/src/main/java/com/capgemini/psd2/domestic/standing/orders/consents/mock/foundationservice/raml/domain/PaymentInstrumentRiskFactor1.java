package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain;
import java.util.Objects;

import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * The risk factor associated to this payment instrument.
 */
@ApiModel(description = "The risk factor associated to this payment instrument.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-01-16T12:10:46.138+05:30")
public class PaymentInstrumentRiskFactor1 {
  @SerializedName("counterPartyAddress")
  private Address counterPartyAddress = null;

  @SerializedName("merchantCustomerIdentification")
  private String merchantCustomerIdentification = null;

  @SerializedName("merchantCategoryCode")
  private String merchantCategoryCode = null;

  @SerializedName("paymentContextCode")
  private String paymentContextCode = null;

  public PaymentInstrumentRiskFactor1 counterPartyAddress(Address counterPartyAddress) {
    this.counterPartyAddress = counterPartyAddress;
    return this;
  }

   /**
   * Get counterPartyAddress
   * @return counterPartyAddress
  **/
  @ApiModelProperty(value = "")
  public Address getCounterPartyAddress() {
    return counterPartyAddress;
  }

  public void setCounterPartyAddress(Address counterPartyAddress) {
    this.counterPartyAddress = counterPartyAddress;
  }

  public PaymentInstrumentRiskFactor1 merchantCustomerIdentification(String merchantCustomerIdentification) {
    this.merchantCustomerIdentification = merchantCustomerIdentification;
    return this;
  }

   /**
   * Get merchantCustomerIdentification
   * @return merchantCustomerIdentification
  **/
  @ApiModelProperty(value = "")
  public String getMerchantCustomerIdentification() {
    return merchantCustomerIdentification;
  }

  public void setMerchantCustomerIdentification(String merchantCustomerIdentification) {
    this.merchantCustomerIdentification = merchantCustomerIdentification;
  }

  public PaymentInstrumentRiskFactor1 merchantCategoryCode(String merchantCategoryCode) {
    this.merchantCategoryCode = merchantCategoryCode;
    return this;
  }

   /**
   * Get merchantCategoryCode
   * @return merchantCategoryCode
  **/
  @ApiModelProperty(value = "")
  public String getMerchantCategoryCode() {
    return merchantCategoryCode;
  }

  public void setMerchantCategoryCode(String merchantCategoryCode) {
    this.merchantCategoryCode = merchantCategoryCode;
  }

  public PaymentInstrumentRiskFactor1 paymentContextCode(String paymentContextCode) {
    this.paymentContextCode = paymentContextCode;
    return this;
  }

   /**
   * Get paymentContextCode
   * @return paymentContextCode
  **/
  @ApiModelProperty(value = "")
  public String getPaymentContextCode() {
    return paymentContextCode;
  }

  public void setPaymentContextCode(String paymentContextCode) {
    this.paymentContextCode = paymentContextCode;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PaymentInstrumentRiskFactor1 paymentInstrumentRiskFactor1 = (PaymentInstrumentRiskFactor1) o;
    return Objects.equals(this.counterPartyAddress, paymentInstrumentRiskFactor1.counterPartyAddress) &&
        Objects.equals(this.merchantCustomerIdentification, paymentInstrumentRiskFactor1.merchantCustomerIdentification) &&
        Objects.equals(this.merchantCategoryCode, paymentInstrumentRiskFactor1.merchantCategoryCode) &&
        Objects.equals(this.paymentContextCode, paymentInstrumentRiskFactor1.paymentContextCode);
  }

  @Override
  public int hashCode() {
    return Objects.hash(counterPartyAddress, merchantCustomerIdentification, merchantCategoryCode, paymentContextCode);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaymentInstrumentRiskFactor1 {\n");
    
    sb.append("    counterPartyAddress: ").append(toIndentedString(counterPartyAddress)).append("\n");
    sb.append("    merchantCustomerIdentification: ").append(toIndentedString(merchantCustomerIdentification)).append("\n");
    sb.append("    merchantCategoryCode: ").append(toIndentedString(merchantCategoryCode)).append("\n");
    sb.append("    paymentContextCode: ").append(toIndentedString(paymentContextCode)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

