package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Scheduled Payment Instruction.
 */
@ApiModel(description = "Scheduled Payment Instruction.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class ScheduledPaymentInstructionProposal2   {
  @JsonProperty("paymentInstructionNumber")
  private String paymentInstructionNumber = null;

  @JsonProperty("paymentInstructionStatusCode")
  private PaymentInstructionStatusCode2 paymentInstructionStatusCode = null;

  @JsonProperty("originatingAccount")
  private Account originatingAccount = null;

  @JsonProperty("instructingParty")
  private Party instructingParty = null;

  @JsonProperty("instructionIssueDate")
  private OffsetDateTime instructionIssueDate = null;

  @JsonProperty("instructionStatusUpdateDateTime")
  private OffsetDateTime instructionStatusUpdateDateTime = null;

  @JsonProperty("paymentDirectionType")
  private String paymentDirectionType = null;

  @JsonProperty("paymentInstructionRiskFactor")
  private PaymentInstrumentRiskFactor paymentInstructionRiskFactor = null;

  @JsonProperty("paymentInstructionChannel")
  private Channel paymentInstructionChannel = null;

  @JsonProperty("paymentInstructionToCounterParty")
  @Valid
  private List<PaymentInstructionCounterparty> paymentInstructionToCounterParty = new ArrayList<PaymentInstructionCounterparty>();

  public ScheduledPaymentInstructionProposal2 paymentInstructionNumber(String paymentInstructionNumber) {
    this.paymentInstructionNumber = paymentInstructionNumber;
    return this;
  }

  /**
   * The identifying number of the Payment Instruction in the Banking System
   * @return paymentInstructionNumber
  **/
  @ApiModelProperty(required = true, value = "The identifying number of the Payment Instruction in the Banking System")
  @NotNull


  public String getPaymentInstructionNumber() {
    return paymentInstructionNumber;
  }

  public void setPaymentInstructionNumber(String paymentInstructionNumber) {
    this.paymentInstructionNumber = paymentInstructionNumber;
  }

  public ScheduledPaymentInstructionProposal2 paymentInstructionStatusCode(PaymentInstructionStatusCode2 paymentInstructionStatusCode) {
    this.paymentInstructionStatusCode = paymentInstructionStatusCode;
    return this;
  }

  /**
   * Get paymentInstructionStatusCode
   * @return paymentInstructionStatusCode
  **/
  @ApiModelProperty(value = "")

  @Valid

  public PaymentInstructionStatusCode2 getPaymentInstructionStatusCode() {
    return paymentInstructionStatusCode;
  }

  public void setPaymentInstructionStatusCode(PaymentInstructionStatusCode2 paymentInstructionStatusCode) {
    this.paymentInstructionStatusCode = paymentInstructionStatusCode;
  }

  public ScheduledPaymentInstructionProposal2 originatingAccount(Account originatingAccount) {
    this.originatingAccount = originatingAccount;
    return this;
  }

  /**
   * Get originatingAccount
   * @return originatingAccount
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public Account getOriginatingAccount() {
    return originatingAccount;
  }

  public void setOriginatingAccount(Account originatingAccount) {
    this.originatingAccount = originatingAccount;
  }

  public ScheduledPaymentInstructionProposal2 instructingParty(Party instructingParty) {
    this.instructingParty = instructingParty;
    return this;
  }

  /**
   * Get instructingParty
   * @return instructingParty
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Party getInstructingParty() {
    return instructingParty;
  }

  public void setInstructingParty(Party instructingParty) {
    this.instructingParty = instructingParty;
  }

  public ScheduledPaymentInstructionProposal2 instructionIssueDate(OffsetDateTime instructionIssueDate) {
    this.instructionIssueDate = instructionIssueDate;
    return this;
  }

  /**
   * The date on which the Instruction was created.
   * @return instructionIssueDate
  **/
  @ApiModelProperty(required = true, value = "The date on which the Instruction was created.")
  @NotNull

  @Valid

  public OffsetDateTime getInstructionIssueDate() {
    return instructionIssueDate;
  }

  public void setInstructionIssueDate(OffsetDateTime instructionIssueDate) {
    this.instructionIssueDate = instructionIssueDate;
  }

  public ScheduledPaymentInstructionProposal2 instructionStatusUpdateDateTime(OffsetDateTime instructionStatusUpdateDateTime) {
    this.instructionStatusUpdateDateTime = instructionStatusUpdateDateTime;
    return this;
  }

  /**
   * Timestamp of last time paymentInstructionStatusCode was updated.
   * @return instructionStatusUpdateDateTime
  **/
  @ApiModelProperty(required = true, value = "Timestamp of last time paymentInstructionStatusCode was updated.")
  @NotNull

  @Valid

  public OffsetDateTime getInstructionStatusUpdateDateTime() {
    return instructionStatusUpdateDateTime;
  }

  public void setInstructionStatusUpdateDateTime(OffsetDateTime instructionStatusUpdateDateTime) {
    this.instructionStatusUpdateDateTime = instructionStatusUpdateDateTime;
  }

  public ScheduledPaymentInstructionProposal2 paymentDirectionType(String paymentDirectionType) {
    this.paymentDirectionType = paymentDirectionType;
    return this;
  }

  /**
   * Determines if a payment is a repayment or a payment (relevant for payment schedule and irregular payment schedule)
   * @return paymentDirectionType
  **/
  @ApiModelProperty(value = "Determines if a payment is a repayment or a payment (relevant for payment schedule and irregular payment schedule)")


  public String getPaymentDirectionType() {
    return paymentDirectionType;
  }

  public void setPaymentDirectionType(String paymentDirectionType) {
    this.paymentDirectionType = paymentDirectionType;
  }

  public ScheduledPaymentInstructionProposal2 paymentInstructionRiskFactor(PaymentInstrumentRiskFactor paymentInstructionRiskFactor) {
    this.paymentInstructionRiskFactor = paymentInstructionRiskFactor;
    return this;
  }

  /**
   * Get paymentInstructionRiskFactor
   * @return paymentInstructionRiskFactor
  **/
  @ApiModelProperty(value = "")

  @Valid

  public PaymentInstrumentRiskFactor getPaymentInstructionRiskFactor() {
    return paymentInstructionRiskFactor;
  }

  public void setPaymentInstructionRiskFactor(PaymentInstrumentRiskFactor paymentInstructionRiskFactor) {
    this.paymentInstructionRiskFactor = paymentInstructionRiskFactor;
  }

  public ScheduledPaymentInstructionProposal2 paymentInstructionChannel(Channel paymentInstructionChannel) {
    this.paymentInstructionChannel = paymentInstructionChannel;
    return this;
  }

  /**
   * Get paymentInstructionChannel
   * @return paymentInstructionChannel
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Channel getPaymentInstructionChannel() {
    return paymentInstructionChannel;
  }

  public void setPaymentInstructionChannel(Channel paymentInstructionChannel) {
    this.paymentInstructionChannel = paymentInstructionChannel;
  }

  public ScheduledPaymentInstructionProposal2 paymentInstructionToCounterParty(List<PaymentInstructionCounterparty> paymentInstructionToCounterParty) {
    this.paymentInstructionToCounterParty = paymentInstructionToCounterParty;
    return this;
  }

  public ScheduledPaymentInstructionProposal2 addPaymentInstructionToCounterPartyItem(PaymentInstructionCounterparty paymentInstructionToCounterPartyItem) {
    this.paymentInstructionToCounterParty.add(paymentInstructionToCounterPartyItem);
    return this;
  }

  /**
   * Get paymentInstructionToCounterParty
   * @return paymentInstructionToCounterParty
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public List<PaymentInstructionCounterparty> getPaymentInstructionToCounterParty() {
    return paymentInstructionToCounterParty;
  }

  public void setPaymentInstructionToCounterParty(List<PaymentInstructionCounterparty> paymentInstructionToCounterParty) {
    this.paymentInstructionToCounterParty = paymentInstructionToCounterParty;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ScheduledPaymentInstructionProposal2 scheduledPaymentInstructionProposal2 = (ScheduledPaymentInstructionProposal2) o;
    return Objects.equals(this.paymentInstructionNumber, scheduledPaymentInstructionProposal2.paymentInstructionNumber) &&
        Objects.equals(this.paymentInstructionStatusCode, scheduledPaymentInstructionProposal2.paymentInstructionStatusCode) &&
        Objects.equals(this.originatingAccount, scheduledPaymentInstructionProposal2.originatingAccount) &&
        Objects.equals(this.instructingParty, scheduledPaymentInstructionProposal2.instructingParty) &&
        Objects.equals(this.instructionIssueDate, scheduledPaymentInstructionProposal2.instructionIssueDate) &&
        Objects.equals(this.instructionStatusUpdateDateTime, scheduledPaymentInstructionProposal2.instructionStatusUpdateDateTime) &&
        Objects.equals(this.paymentDirectionType, scheduledPaymentInstructionProposal2.paymentDirectionType) &&
        Objects.equals(this.paymentInstructionRiskFactor, scheduledPaymentInstructionProposal2.paymentInstructionRiskFactor) &&
        Objects.equals(this.paymentInstructionChannel, scheduledPaymentInstructionProposal2.paymentInstructionChannel) &&
        Objects.equals(this.paymentInstructionToCounterParty, scheduledPaymentInstructionProposal2.paymentInstructionToCounterParty);
  }

  @Override
  public int hashCode() {
    return Objects.hash(paymentInstructionNumber, paymentInstructionStatusCode, originatingAccount, instructingParty, instructionIssueDate, instructionStatusUpdateDateTime, paymentDirectionType, paymentInstructionRiskFactor, paymentInstructionChannel, paymentInstructionToCounterParty);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ScheduledPaymentInstructionProposal2 {\n");
    
    sb.append("    paymentInstructionNumber: ").append(toIndentedString(paymentInstructionNumber)).append("\n");
    sb.append("    paymentInstructionStatusCode: ").append(toIndentedString(paymentInstructionStatusCode)).append("\n");
    sb.append("    originatingAccount: ").append(toIndentedString(originatingAccount)).append("\n");
    sb.append("    instructingParty: ").append(toIndentedString(instructingParty)).append("\n");
    sb.append("    instructionIssueDate: ").append(toIndentedString(instructionIssueDate)).append("\n");
    sb.append("    instructionStatusUpdateDateTime: ").append(toIndentedString(instructionStatusUpdateDateTime)).append("\n");
    sb.append("    paymentDirectionType: ").append(toIndentedString(paymentDirectionType)).append("\n");
    sb.append("    paymentInstructionRiskFactor: ").append(toIndentedString(paymentInstructionRiskFactor)).append("\n");
    sb.append("    paymentInstructionChannel: ").append(toIndentedString(paymentInstructionChannel)).append("\n");
    sb.append("    paymentInstructionToCounterParty: ").append(toIndentedString(paymentInstructionToCounterParty)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

