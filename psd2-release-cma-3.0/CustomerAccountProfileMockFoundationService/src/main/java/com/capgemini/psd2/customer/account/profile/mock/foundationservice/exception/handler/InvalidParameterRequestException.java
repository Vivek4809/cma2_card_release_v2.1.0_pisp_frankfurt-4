package com.capgemini.psd2.customer.account.profile.mock.foundationservice.exception.handler;

public class InvalidParameterRequestException extends Exception {

	public InvalidParameterRequestException(String i) {
		super(i);

	}

}
