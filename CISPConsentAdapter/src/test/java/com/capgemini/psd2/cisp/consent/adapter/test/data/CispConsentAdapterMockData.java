/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.cisp.consent.adapter.test.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.CispConsent;

/**
 * The Class ConsentMappingAdapterMockData.
 */
public class CispConsentAdapterMockData {

	/** The mock consent. */
	private static CispConsent mockConsent;
	
	/** The mock account mapping. */
	private static AccountMapping mockAccountMapping;

	/**
	 * Gets the account mapping.
	 *
	 * @return the account mapping
	 */
	public static AccountMapping getAccountMapping() {
		mockAccountMapping = new AccountMapping();
		mockAccountMapping.setTppCID("tpp123");
		mockAccountMapping.setPsuId("user123");
		mockAccountMapping.setCorrelationId("95212678-4d0c-450f-8268-25dcfc95bfa1");
		List<AccountDetails> selectedAccounts = new ArrayList<>();

		AccountDetails accountRequest1 = new AccountDetails();
		accountRequest1.setAccountId("22289");
		accountRequest1.setAccountNSC("SC802001");
		accountRequest1.setAccountNumber("1022675");
		
		AccountDetails accountRequest2 = new AccountDetails();
		accountRequest2.setAccountId("89893");
		accountRequest2.setAccountNSC("LOYDGB21");
		accountRequest2.setAccountNumber("GB19LOYD30961799709943");
		selectedAccounts.add(accountRequest1);
		selectedAccounts.add(accountRequest2);

		mockAccountMapping.setAccountDetails(selectedAccounts);
		return mockAccountMapping;
	}

	/**
	 * Gets the consent mock data.
	 *
	 * @return the consent mock data
	 */
	public static CispConsent getConsentMockData() {
		mockConsent = new CispConsent();

		mockConsent.setTppCId("tpp123");
		mockConsent.setPsuId("user123");
		mockConsent.setConsentId("a1e590ad-73dc-453a-841e-2a2ef055e878");

		AccountDetails accountRequest1 = new AccountDetails();
		accountRequest1.setAccountId("22289");
		accountRequest1.setAccountNSC("SC802001");
		accountRequest1.setAccountNumber("1022675");

		mockConsent.setAccountDetails(accountRequest1);
		mockConsent.setFundsIntentId("123456");

		return mockConsent;
	}
	
	public static CispConsent getConsentMockDataWithOutAccountDetails() {
		mockConsent = new CispConsent();
		mockConsent.setTppCId("tpp123");
		mockConsent.setPsuId("user123");
		mockConsent.setConsentId("a1e590ad-73dc-453a-841e-2a2ef055e878");
		return mockConsent;
	}

	public static List<CispConsent> getConsentListMockData() {
		List<CispConsent> consentList = new ArrayList<>();
		consentList.add(getConsentMockData());
		return consentList;
	}
}
