package com.capgemini.psd2.pisp.dsp.test.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.dsp.controller.DSPaymentsApiController;
import com.capgemini.psd2.pisp.dsp.service.DSPaymentsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.ser.DefaultSerializerProvider;
import com.fasterxml.jackson.databind.ser.SerializerFactory;

@RunWith(SpringJUnit4ClassRunner.class)
public class DSPaymentsApiControllerTest {

	@Mock
	private DSPaymentsService service;

	@Mock
	ObjectMapper objectMapper;

	@InjectMocks
	private DSPaymentsApiController controller;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testObjectMapper() {
		controller.getObjectMapper();
	}

	@Test
	public void testRequest() {
		controller.getRequest();
	}

	@Test
	public void testCreateDomesticScheduledPayments() {
		CustomDSPaymentsPOSTRequest request = new CustomDSPaymentsPOSTRequest();
		CustomDSPaymentsPOSTResponse response = new CustomDSPaymentsPOSTResponse();

		@SuppressWarnings("serial")
		DefaultSerializerProvider p = new DefaultSerializerProvider() {

			@Override
			public DefaultSerializerProvider createInstance(SerializationConfig config, SerializerFactory jsf) {
				return null;
			}
		};
		objectMapper.setSerializerProvider(p);

		String xFapiFinancialId = "1";
		String authorization = "2";
		String xIdempotencyKey = "3";
		String xJwsSignature = "4";

		Mockito.when(service.createDomesticScheduledPaymentsResource(request)).thenReturn(response);
		controller.createDomesticScheduledPayments(request, xFapiFinancialId, authorization, xIdempotencyKey,
				xJwsSignature, null, null, null, null);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testCreateDomesticScheduledPaymentsException() {
		CustomDSPaymentsPOSTRequest request = new CustomDSPaymentsPOSTRequest();
		String xFapiFinancialId = "1";
		String authorization = "2";
		String xIdempotencyKey = "3";
		String xJwsSignature = "4";

		Mockito.when(service.createDomesticScheduledPaymentsResource(request)).thenThrow(PSD2Exception.class);
		controller.createDomesticScheduledPayments(request, xFapiFinancialId, authorization, xIdempotencyKey,
				xJwsSignature, null, null, null, null);
	}

	@Test
	public void testGetDomesticScheduledPaymentsDomesticScheduledPaymentId() {
		CustomDSPaymentsPOSTResponse response = new CustomDSPaymentsPOSTResponse();

		@SuppressWarnings("serial")
		DefaultSerializerProvider p = new DefaultSerializerProvider() {

			@Override
			public DefaultSerializerProvider createInstance(SerializationConfig config, SerializerFactory jsf) {
				return null;
			}
		};
		objectMapper.setSerializerProvider(p);

		String xFapiFinancialId = "1";
		String authorization = "2";
		String domesticScheduledPaymentId = "123456";

		Mockito.when(service.retrieveDomesticScheduledPaymentsResource(domesticScheduledPaymentId))
				.thenReturn(response);
		controller.getDomesticScheduledPaymentsDomesticScheduledPaymentId(domesticScheduledPaymentId, xFapiFinancialId,
				authorization, null, null, null, null);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testGetDomesticScheduledPaymentsDomesticScheduledPaymentIdException() {
		String xFapiFinancialId = "1";
		String authorization = "2";
		String domesticScheduledPaymentId = "123456";

		Mockito.when(service.retrieveDomesticScheduledPaymentsResource(domesticScheduledPaymentId))
				.thenThrow(PSD2Exception.class);
		controller.getDomesticScheduledPaymentsDomesticScheduledPaymentId(domesticScheduledPaymentId, xFapiFinancialId,
				authorization, null, null, null, null);
	}

	@After
	public void tearDown() {
		service = null;
		controller = null;
	}
}
