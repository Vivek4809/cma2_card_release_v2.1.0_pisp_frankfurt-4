package com.capgemini.psd2.account.offers.test.mock.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBOffer1;
import com.capgemini.psd2.aisp.domain.OBReadOffer1;
import com.capgemini.psd2.aisp.domain.OBReadOffer1Data;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountOffersResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.token.ConsentTokenData;
import com.capgemini.psd2.token.Token;

public class AccountOffersTestMockData {
	
	public static Token mockToken;
	
	public static Token getToken() {
		mockToken = new Token();
		ConsentTokenData consentTokenData = new ConsentTokenData();
		consentTokenData.setConsentExpiry("1509348259877L");
		consentTokenData.setConsentId("51915818");
		mockToken.setConsentTokenData(consentTokenData);
		Map<String,String> map=new HashMap<>();
		mockToken.setSeviceParams(map);
		return mockToken;
	}
	
	public static AccountMapping getMockAccountMapping() {
		AccountMapping mapping = new AccountMapping();
		mapping.setTppCID("6443e15975554bce8099e35b88b40465");
		mapping.setPsuId("88888888");
		List<AccountDetails> selectedAccounts = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();
		accountRequest.setAccountId("d9d9c3dc-1427-4f03-92a7-7916fcf4cc0d");
		accountRequest.setAccountNSC("11111111");
		accountRequest.setAccountNumber("111111");
		selectedAccounts.add(accountRequest);
		mapping.setAccountDetails(selectedAccounts);
		return mapping;
	}

	public static LoggerAttribute getMockLoggerData() {
		LoggerAttribute x = new LoggerAttribute();
		x.setApiId("testApiID");
		return x;
	}
	
	
	public static AispConsent getMockAispConsent() {
		AispConsent aispConsent = new AispConsent();
		aispConsent.setTppCId("6443e15975554bce8099e35b88b40465");
		aispConsent.setPsuId("88888888");
		aispConsent.setConsentId("51915818");
		aispConsent.setAccountRequestId("10693f4f-1a9c-4527-bf2c-19b23c6a6f61");
		aispConsent.setChannelId("B365");
		aispConsent.setStartDate("2017-07-31T11:25:26.874Z");
		aispConsent.setEndDate("2019-12-30T00:00:00.875");
		aispConsent.setTransactionFromDateTime("2014-01-01T00:00:00.800");
		aispConsent.setTransactionToDateTime("2019-12-30T00:00:00.345");
		aispConsent.setStatus(ConsentStatusEnum.AUTHORISED);

		List<AccountDetails> selectedAispConsentAccountDetails = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();

		accountRequest.setAccountId("d9d9c3dc-1427-4f03-92a7-7916fcf4cc0d");
		accountRequest.setAccountNSC("111111");
		accountRequest.setAccountNumber("11111111");

		selectedAispConsentAccountDetails.add(accountRequest);
		aispConsent.setAccountDetails(selectedAispConsentAccountDetails);

		return aispConsent;
	}

	public static PlatformAccountOffersResponse getAccountOffersMockPlatformResponseWithLinksMeta() {
		PlatformAccountOffersResponse platformAccountOffersResponse = new PlatformAccountOffersResponse();

		OBReadOffer1Data obReadOffer1Data = new OBReadOffer1Data();
		List<OBOffer1> offerList = new ArrayList<>();

		OBOffer1 offer1 = new OBOffer1();
		offer1.setAccountId("d9d9c3dc-1427-4f03-92a7-7916fcf4cc0d");
		offer1.setOfferId("Offer1");
		offerList.add(offer1);

		obReadOffer1Data.setOffer(offerList);
		OBReadOffer1 obReadOffer1 = new OBReadOffer1();
		obReadOffer1.setData(obReadOffer1Data);

		Links links = new Links();
		obReadOffer1.setLinks(links);
		Meta meta = new Meta();
		meta.setTotalPages(1);

		obReadOffer1.setMeta(meta);

		platformAccountOffersResponse.setoBReadOffer1(obReadOffer1);
		return platformAccountOffersResponse;
	}

	public static Object getMockExpectedAccountOffersWithLinksMeta() {

		OBReadOffer1Data obReadOffer1Data = new OBReadOffer1Data();
		List<OBOffer1> offerList = new ArrayList<>();

		OBOffer1 offer1 = new OBOffer1();
		offer1.setAccountId("d9d9c3dc-1427-4f03-92a7-7916fcf4cc0d");
		offer1.setOfferId("Offer1");
		offerList.add(offer1);

		obReadOffer1Data.setOffer(offerList);
		OBReadOffer1 obReadOffer1 = new OBReadOffer1();
		obReadOffer1.setData(obReadOffer1Data);

		Links links = new Links();
		obReadOffer1.setLinks(links);
		Meta meta = new Meta();
		meta.setTotalPages(1);

		obReadOffer1.setMeta(meta);
		return obReadOffer1;
	}

	public static PlatformAccountOffersResponse getAccountOffersMockPlatformResponseWithoutLinksMeta() {
		PlatformAccountOffersResponse platformAccountOffersResponse = new PlatformAccountOffersResponse();

		OBReadOffer1Data obReadOffer1Data = new OBReadOffer1Data();
		List<OBOffer1> offerList = new ArrayList<>();

		OBOffer1 offer1 = new OBOffer1();
		offer1.setAccountId("123");
		offer1.setOfferId("Offer1");
		offerList.add(offer1);

		obReadOffer1Data.setOffer(offerList);
		OBReadOffer1 obReadOffer1 = new OBReadOffer1();
		obReadOffer1.setData(obReadOffer1Data);

		platformAccountOffersResponse.setoBReadOffer1(obReadOffer1);
		return platformAccountOffersResponse;
	}

	public static Object getMockExpectedAccountOffersWithoutLinksMeta() {

		OBReadOffer1Data obReadOffer1Data = new OBReadOffer1Data();
		List<OBOffer1> offerList = new ArrayList<>();

		OBOffer1 offer1 = new OBOffer1();
		offer1.setAccountId("123");
		offer1.setOfferId("Offer1");
		offerList.add(offer1);

		obReadOffer1Data.setOffer(offerList);
		OBReadOffer1 obReadOffer1 = new OBReadOffer1();
		obReadOffer1.setData(obReadOffer1Data);

		Links links = new Links();
		obReadOffer1.setLinks(links);
		Meta meta = new Meta();
		meta.setTotalPages(1);

		obReadOffer1.setMeta(meta);
		return obReadOffer1;
	}

}
