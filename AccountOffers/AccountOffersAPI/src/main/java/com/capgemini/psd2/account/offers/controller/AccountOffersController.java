package com.capgemini.psd2.account.offers.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.account.offers.service.AccountOffersService;
import com.capgemini.psd2.aisp.domain.OBReadOffer1;

@RestController
public class AccountOffersController {

	@Autowired
	private AccountOffersService accountOffersService;

	@RequestMapping(value = "/accounts/{accountId}/offers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public OBReadOffer1 retriveAccountOffers(@PathVariable("accountId") String accountId) {

		return accountOffersService.retrieveAccountOffers(accountId);
	}
}
