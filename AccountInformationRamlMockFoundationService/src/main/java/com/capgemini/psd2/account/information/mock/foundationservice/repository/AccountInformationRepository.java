/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.information.mock.foundationservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.account.information.mock.foundationservice.raml.domain.Account;

/**
 * The Interface AccountInformationRepository.
 */
public interface AccountInformationRepository extends MongoRepository<Account, String> {

	/**
	 * Find by account nsc and account number.
	 *
	 * @param accountNsc the account nsc
	 * @param accountNumber the account number
	 * @return the accnt
	 */
	public Account findByCreditCardAccountInformationAndAccountNumber(String creditCardAccountInformation, String accountNumber);
	public Account findByAccountNumber( String accountNumber);

}
