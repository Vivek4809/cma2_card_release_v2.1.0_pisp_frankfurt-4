package com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.test;
/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************//*
package com.capgemini.psd2.foundationservice.account.balance.boi.adapter.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.aisp.domain.OBCashBalance1;
import com.capgemini.psd2.aisp.domain.OBReadBalance1;
import com.capgemini.psd2.aisp.domain.OBReadBalance1Data;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBalanceResponse;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.constants.FundsConfirmationFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.BalanceAmountBaseType;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.BalanceBaseType;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.BalanceCurrencyBaseType;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.BalanceResponse;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.BalanceTypeEnum;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.CreditcardsBalanceResponse;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.transformer.FundsConfirmationFoundationServiceTransformer;
import com.capgemini.psd2.validator.impl.PSD2ValidatorImpl;

*//**
 * The Class AccountBalanceFoundationServiceTransformerTest.
 *//*
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountBalanceFoundationServiceTransformerTest {

	@Mock
	private PSD2ValidatorImpl psd2Validator;

	*//** The account balance FS transformer. *//*
	@InjectMocks
	private FundsConfirmationFoundationServiceTransformer accountBalanceFSTransformer;

	@Mock
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;

	*//**
	 * Sets the up.
	 *//*
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testAccountBalanceFSTransformer1a() {
		Map<String, String> params = new HashMap<>();
		params.put(FundsConfirmationFoundationServiceConstants.ACCOUNT_SUBTYPE, "CurrentAccount");

		BalanceBaseType lobi = new BalanceBaseType();
		List<BalanceBaseType> balList = new ArrayList<BalanceBaseType>();
		balList.add(lobi);

		BalanceResponse balanceResponse = new BalanceResponse();
		balanceResponse.setBalancesList(balList);

		lobi.setBalanceType(BalanceTypeEnum.LEDGER_BALANCE);
		BalanceAmountBaseType babt = new BalanceAmountBaseType();
		Double amt = 5000d;
		babt.setLocalReportingCurrency(amt);
		lobi.setBalanceAmount(babt);
		BalanceCurrencyBaseType balanceCurrency = new BalanceCurrencyBaseType();
		lobi.setBalanceCurrency(balanceCurrency);
		balanceCurrency.setIsoAlphaCode("GBP");
		lobi.setBalanceCurrency(balanceCurrency);

		OBReadBalance1 obReadBalance1 = new OBReadBalance1();
		PlatformAccountBalanceResponse platformAccountBalanceResponse = new PlatformAccountBalanceResponse();
		OBReadBalance1Data obReadBalance1Data = new OBReadBalance1Data();
		List<OBCashBalance1> oBCashBalance1 = new ArrayList<>();
		oBCashBalance1.addAll(oBCashBalance1);
		obReadBalance1Data.setBalance(oBCashBalance1);
		obReadBalance1.setData(obReadBalance1Data);
		platformAccountBalanceResponse.setoBReadBalance1(obReadBalance1);

		accountBalanceFSTransformer.transformAccountBalance(balanceResponse, params);
	}

	@Test
	public void testAccountBalanceFSTransformer2a() {
		Map<String, String> params = new HashMap<>();
		params.put(FundsConfirmationFoundationServiceConstants.ACCOUNT_SUBTYPE, "Savings");

		BalanceBaseType lobi = new BalanceBaseType();
		List<BalanceBaseType> balList = new ArrayList<BalanceBaseType>();
		balList.add(lobi);

		BalanceResponse balanceResponse = new BalanceResponse();
		balanceResponse.setBalancesList(balList);

		lobi.setBalanceType(BalanceTypeEnum.SHADOW_BALANCE);
		BalanceAmountBaseType babt = new BalanceAmountBaseType();
		Double amt = 0d;
		babt.setLocalReportingCurrency(amt);
		lobi.setBalanceAmount(babt);
		BalanceCurrencyBaseType balanceCurrency = new BalanceCurrencyBaseType();
		lobi.setBalanceCurrency(balanceCurrency);
		balanceCurrency.setIsoAlphaCode("GBP");
		lobi.setBalanceCurrency(balanceCurrency);

		OBReadBalance1 obReadBalance1 = new OBReadBalance1();
		PlatformAccountBalanceResponse platformAccountBalanceResponse = new PlatformAccountBalanceResponse();
		OBReadBalance1Data obReadBalance1Data = new OBReadBalance1Data();
		List<OBCashBalance1> oBCashBalance1 = new ArrayList<>();
		oBCashBalance1.addAll(oBCashBalance1);
		obReadBalance1Data.setBalance(oBCashBalance1);
		obReadBalance1.setData(obReadBalance1Data);
		platformAccountBalanceResponse.setoBReadBalance1(obReadBalance1);

		accountBalanceFSTransformer.transformAccountBalance(balanceResponse, params);
	}

	@Test
	public void testAccountBalanceFSTransformer3a() {
		Map<String, String> params = new HashMap<>();
		params.put(FundsConfirmationFoundationServiceConstants.ACCOUNT_SUBTYPE, "");

		BalanceBaseType lobi = new BalanceBaseType();
		List<BalanceBaseType> balList = new ArrayList<BalanceBaseType>();
		balList.add(lobi);

		BalanceResponse balanceResponse = new BalanceResponse();
		balanceResponse.setBalancesList(balList);

		lobi.setBalanceType(BalanceTypeEnum.LEDGER_BALANCE);
		BalanceAmountBaseType babt = new BalanceAmountBaseType();
		Double amt = -5000d;
		babt.setLocalReportingCurrency(amt);
		lobi.setBalanceAmount(babt);
		BalanceCurrencyBaseType balanceCurrency = new BalanceCurrencyBaseType();
		lobi.setBalanceCurrency(balanceCurrency);
		balanceCurrency.setIsoAlphaCode("GBP");
		lobi.setBalanceCurrency(balanceCurrency);

		OBReadBalance1 obReadBalance1 = new OBReadBalance1();
		PlatformAccountBalanceResponse platformAccountBalanceResponse = new PlatformAccountBalanceResponse();
		OBReadBalance1Data obReadBalance1Data = new OBReadBalance1Data();
		List<OBCashBalance1> oBCashBalance1 = new ArrayList<>();
		oBCashBalance1.addAll(oBCashBalance1);
		obReadBalance1Data.setBalance(oBCashBalance1);
		obReadBalance1.setData(obReadBalance1Data);
		platformAccountBalanceResponse.setoBReadBalance1(obReadBalance1);

		accountBalanceFSTransformer.transformAccountBalance(balanceResponse, params);
	}

	@Test
	public void testAccountBalanceFSTransformer1b() {
		Map<String, String> params = new HashMap<>();
		params.put(FundsConfirmationFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");

		BalanceBaseType lobi = new BalanceBaseType();
		List<BalanceBaseType> balList = new ArrayList<BalanceBaseType>();
		balList.add(lobi);

		CreditcardsBalanceResponse balanceResponse = new CreditcardsBalanceResponse();
		balanceResponse.setBalancesList(balList);

		lobi.setBalanceType(BalanceTypeEnum.LEDGER_BALANCE);
		BalanceAmountBaseType babt = new BalanceAmountBaseType();
		Double amt = 5000d;
		babt.setTransactionCurrency(amt);
		lobi.setBalanceAmount(babt);
		BalanceCurrencyBaseType balanceCurrency = new BalanceCurrencyBaseType();
		lobi.setBalanceCurrency(balanceCurrency);
		balanceCurrency.setIsoAlphaCode("GBP");
		lobi.setBalanceCurrency(balanceCurrency);

		OBReadBalance1 obReadBalance1 = new OBReadBalance1();
		PlatformAccountBalanceResponse platformAccountBalanceResponse = new PlatformAccountBalanceResponse();
		OBReadBalance1Data obReadBalance1Data = new OBReadBalance1Data();
		List<OBCashBalance1> oBCashBalance1 = new ArrayList<>();
		oBCashBalance1.addAll(oBCashBalance1);
		obReadBalance1Data.setBalance(oBCashBalance1);
		obReadBalance1.setData(obReadBalance1Data);
		platformAccountBalanceResponse.setoBReadBalance1(obReadBalance1);

		accountBalanceFSTransformer.transformAccountBalance(balanceResponse, params);
	}

	@Test
	public void testAccountBalanceFSTransformer1bn() {
		Map<String, String> params = new HashMap<>();
		params.put(FundsConfirmationFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");

		BalanceBaseType lobi = new BalanceBaseType();
		List<BalanceBaseType> balList = new ArrayList<BalanceBaseType>();
		balList.add(lobi);

		CreditcardsBalanceResponse balanceResponse = new CreditcardsBalanceResponse();
		balanceResponse.setBalancesList(balList);

		lobi.setBalanceType(BalanceTypeEnum.LEDGER_BALANCE);
		BalanceAmountBaseType babt = new BalanceAmountBaseType();
		Double amt = -5000d;
		babt.setTransactionCurrency(amt);
		lobi.setBalanceAmount(babt);
		BalanceCurrencyBaseType balanceCurrency = new BalanceCurrencyBaseType();
		lobi.setBalanceCurrency(balanceCurrency);
		balanceCurrency.setIsoAlphaCode("GBP");
		lobi.setBalanceCurrency(balanceCurrency);

		OBReadBalance1 obReadBalance1 = new OBReadBalance1();
		PlatformAccountBalanceResponse platformAccountBalanceResponse = new PlatformAccountBalanceResponse();
		OBReadBalance1Data obReadBalance1Data = new OBReadBalance1Data();
		List<OBCashBalance1> oBCashBalance1 = new ArrayList<>();
		oBCashBalance1.addAll(oBCashBalance1);
		obReadBalance1Data.setBalance(oBCashBalance1);
		obReadBalance1.setData(obReadBalance1Data);
		platformAccountBalanceResponse.setoBReadBalance1(obReadBalance1);

		accountBalanceFSTransformer.transformAccountBalance(balanceResponse, params);
	}

	@Test
	public void testAccountBalanceFSTransformer2b() {
		Map<String, String> params = new HashMap<>();
		params.put(FundsConfirmationFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");

		BalanceBaseType lobi = new BalanceBaseType();
		List<BalanceBaseType> balList = new ArrayList<BalanceBaseType>();
		balList.add(lobi);

		CreditcardsBalanceResponse balanceResponse = new CreditcardsBalanceResponse();
		balanceResponse.setBalancesList(balList);

		lobi.setBalanceType(BalanceTypeEnum.STATEMENT_CLOSING_BALANCE);
		BalanceAmountBaseType babt = new BalanceAmountBaseType();
		Double amt = 0d;
		babt.setLocalReportingCurrency(amt);
		lobi.setBalanceAmount(babt);
		BalanceCurrencyBaseType balanceCurrency = new BalanceCurrencyBaseType();
		lobi.setBalanceCurrency(balanceCurrency);
		balanceCurrency.setIsoAlphaCode("GBP");
		lobi.setBalanceCurrency(balanceCurrency);

		OBReadBalance1 obReadBalance1 = new OBReadBalance1();
		PlatformAccountBalanceResponse platformAccountBalanceResponse = new PlatformAccountBalanceResponse();
		OBReadBalance1Data obReadBalance1Data = new OBReadBalance1Data();
		List<OBCashBalance1> oBCashBalance1 = new ArrayList<>();
		oBCashBalance1.addAll(oBCashBalance1);
		obReadBalance1Data.setBalance(oBCashBalance1);
		obReadBalance1.setData(obReadBalance1Data);
		platformAccountBalanceResponse.setoBReadBalance1(obReadBalance1);

		accountBalanceFSTransformer.transformAccountBalance(balanceResponse, params);
	}

	@Test
	public void testAccountBalanceFSTransformer2bn() {
		Map<String, String> params = new HashMap<>();
		params.put(FundsConfirmationFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");

		BalanceBaseType lobi = new BalanceBaseType();
		List<BalanceBaseType> balList = new ArrayList<BalanceBaseType>();
		balList.add(lobi);

		CreditcardsBalanceResponse balanceResponse = new CreditcardsBalanceResponse();
		balanceResponse.setBalancesList(balList);

		lobi.setBalanceType(BalanceTypeEnum.STATEMENT_CLOSING_BALANCE);
		BalanceAmountBaseType babt = new BalanceAmountBaseType();
		Double amt = -5000d;
		babt.setLocalReportingCurrency(amt);
		lobi.setBalanceAmount(babt);
		BalanceCurrencyBaseType balanceCurrency = new BalanceCurrencyBaseType();
		lobi.setBalanceCurrency(balanceCurrency);
		balanceCurrency.setIsoAlphaCode("GBP");
		lobi.setBalanceCurrency(balanceCurrency);

		OBReadBalance1 obReadBalance1 = new OBReadBalance1();
		PlatformAccountBalanceResponse platformAccountBalanceResponse = new PlatformAccountBalanceResponse();
		OBReadBalance1Data obReadBalance1Data = new OBReadBalance1Data();
		List<OBCashBalance1> oBCashBalance1 = new ArrayList<>();
		oBCashBalance1.addAll(oBCashBalance1);
		obReadBalance1Data.setBalance(oBCashBalance1);
		obReadBalance1.setData(obReadBalance1Data);
		platformAccountBalanceResponse.setoBReadBalance1(obReadBalance1);

		accountBalanceFSTransformer.transformAccountBalance(balanceResponse, params);
	}

	@Test
	public void testAccountBalanceFSTransformer3b() {
		Map<String, String> params = new HashMap<>();
		params.put(FundsConfirmationFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");

		BalanceBaseType lobi = new BalanceBaseType();
		List<BalanceBaseType> balList = new ArrayList<BalanceBaseType>();
		balList.add(lobi);

		CreditcardsBalanceResponse balanceResponse = new CreditcardsBalanceResponse();
		balanceResponse.setBalancesList(balList);

		lobi.setBalanceType(BalanceTypeEnum.AVAILABLE_LIMIT);
		BalanceAmountBaseType babt = new BalanceAmountBaseType();
		Double amt = -5000d;
		babt.setLocalReportingCurrency(amt);
		lobi.setBalanceAmount(babt);
		BalanceCurrencyBaseType balanceCurrency = new BalanceCurrencyBaseType();
		lobi.setBalanceCurrency(balanceCurrency);
		balanceCurrency.setIsoAlphaCode("GBP");
		lobi.setBalanceCurrency(balanceCurrency);

		OBReadBalance1 obReadBalance1 = new OBReadBalance1();
		PlatformAccountBalanceResponse platformAccountBalanceResponse = new PlatformAccountBalanceResponse();
		OBReadBalance1Data obReadBalance1Data = new OBReadBalance1Data();
		List<OBCashBalance1> oBCashBalance1 = new ArrayList<>();
		oBCashBalance1.addAll(oBCashBalance1);
		obReadBalance1Data.setBalance(oBCashBalance1);
		obReadBalance1.setData(obReadBalance1Data);
		platformAccountBalanceResponse.setoBReadBalance1(obReadBalance1);

		accountBalanceFSTransformer.transformAccountBalance(balanceResponse, params);
	}

	@Test
	public void testAccountBalanceFSTransformer3bg() {
		Map<String, String> params = new HashMap<>();
		params.put(FundsConfirmationFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");

		BalanceBaseType lobi = new BalanceBaseType();
		List<BalanceBaseType> balList = new ArrayList<BalanceBaseType>();
		balList.add(lobi);

		CreditcardsBalanceResponse balanceResponse = new CreditcardsBalanceResponse();
		balanceResponse.setBalancesList(balList);

		lobi.setBalanceType(BalanceTypeEnum.AVAILABLE_LIMIT);
		BalanceAmountBaseType babt = new BalanceAmountBaseType();
		Double amt = 5000d;
		babt.setLocalReportingCurrency(amt);
		lobi.setBalanceAmount(babt);
		BalanceCurrencyBaseType balanceCurrency = new BalanceCurrencyBaseType();
		lobi.setBalanceCurrency(balanceCurrency);
		balanceCurrency.setIsoAlphaCode("GBP");
		lobi.setBalanceCurrency(balanceCurrency);

		OBReadBalance1 obReadBalance1 = new OBReadBalance1();
		PlatformAccountBalanceResponse platformAccountBalanceResponse = new PlatformAccountBalanceResponse();
		OBReadBalance1Data obReadBalance1Data = new OBReadBalance1Data();
		List<OBCashBalance1> oBCashBalance1 = new ArrayList<>();
		oBCashBalance1.addAll(oBCashBalance1);
		obReadBalance1Data.setBalance(oBCashBalance1);
		obReadBalance1.setData(obReadBalance1Data);
		platformAccountBalanceResponse.setoBReadBalance1(obReadBalance1);

		accountBalanceFSTransformer.transformAccountBalance(balanceResponse, params);
	}

}
*/