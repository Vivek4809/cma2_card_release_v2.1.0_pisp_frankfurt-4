package com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain;

import java.math.BigDecimal;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMax;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * BalanceBalanceAmount
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-02-07T14:38:09.445+05:30")

public class BalanceBalanceAmount   {
  @JsonProperty("groupReportingCurrency")
  private BigDecimal groupReportingCurrency = null;

  @JsonProperty("localReportingCurrency")
  private BigDecimal localReportingCurrency = null;

  @JsonProperty("transactionCurrency")
  private BigDecimal transactionCurrency = null;

  @JsonProperty("accountCurrency")
  private BigDecimal accountCurrency = null;

  public BalanceBalanceAmount groupReportingCurrency(BigDecimal groupReportingCurrency) {
    this.groupReportingCurrency = groupReportingCurrency;
    return this;
  }

  /**
   * The value in group reporting currency
   * maximum: 1.0E+15
   * @return groupReportingCurrency
  **/
  @ApiModelProperty(value = "The value in group reporting currency")

  @Valid
 @DecimalMax("1.0E+15") 
  public BigDecimal getGroupReportingCurrency() {
    return groupReportingCurrency;
  }

  public void setGroupReportingCurrency(BigDecimal groupReportingCurrency) {
    this.groupReportingCurrency = groupReportingCurrency;
  }

  public BalanceBalanceAmount localReportingCurrency(BigDecimal localReportingCurrency) {
    this.localReportingCurrency = localReportingCurrency;
    return this;
  }

  /**
   * The value in Local reporting currency
   * maximum: 1.0E+15
   * @return localReportingCurrency
  **/
  @ApiModelProperty(value = "The value in Local reporting currency")

  @Valid
 @DecimalMax("1.0E+15") 
  public BigDecimal getLocalReportingCurrency() {
    return localReportingCurrency;
  }

  public void setLocalReportingCurrency(BigDecimal localReportingCurrency) {
    this.localReportingCurrency = localReportingCurrency;
  }

  public BalanceBalanceAmount transactionCurrency(BigDecimal transactionCurrency) {
    this.transactionCurrency = transactionCurrency;
    return this;
  }

  /**
   * The actual value in the transaction currency
   * maximum: 1.0E+15
   * @return transactionCurrency
  **/
  @ApiModelProperty(value = "The actual value in the transaction currency")

  @Valid
 @DecimalMax("1.0E+15") 
  public BigDecimal getTransactionCurrency() {
    return transactionCurrency;
  }

  public void setTransactionCurrency(BigDecimal transactionCurrency) {
    this.transactionCurrency = transactionCurrency;
  }

  public BalanceBalanceAmount accountCurrency(BigDecimal accountCurrency) {
    this.accountCurrency = accountCurrency;
    return this;
  }

  /**
   * The actual value in the account currency
   * maximum: 1.0E+15
   * @return accountCurrency
  **/
  @ApiModelProperty(value = "The actual value in the account currency")

  @Valid
 @DecimalMax("1.0E+15") 
  public BigDecimal getAccountCurrency() {
    return accountCurrency;
  }

  public void setAccountCurrency(BigDecimal accountCurrency) {
    this.accountCurrency = accountCurrency;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BalanceBalanceAmount balanceBalanceAmount = (BalanceBalanceAmount) o;
    return Objects.equals(this.groupReportingCurrency, balanceBalanceAmount.groupReportingCurrency) &&
        Objects.equals(this.localReportingCurrency, balanceBalanceAmount.localReportingCurrency) &&
        Objects.equals(this.transactionCurrency, balanceBalanceAmount.transactionCurrency) &&
        Objects.equals(this.accountCurrency, balanceBalanceAmount.accountCurrency);
  }

  @Override
  public int hashCode() {
    return Objects.hash(groupReportingCurrency, localReportingCurrency, transactionCurrency, accountCurrency);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BalanceBalanceAmount {\n");
    
    sb.append("    groupReportingCurrency: ").append(toIndentedString(groupReportingCurrency)).append("\n");
    sb.append("    localReportingCurrency: ").append(toIndentedString(localReportingCurrency)).append("\n");
    sb.append("    transactionCurrency: ").append(toIndentedString(transactionCurrency)).append("\n");
    sb.append("    accountCurrency: ").append(toIndentedString(accountCurrency)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

