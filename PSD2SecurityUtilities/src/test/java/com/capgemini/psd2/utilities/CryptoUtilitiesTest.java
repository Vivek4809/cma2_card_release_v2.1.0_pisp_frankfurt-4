/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************//*
package com.capgemini.psd2.utilities;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.capgemini.psd2.security.utilities.CryptoUtilities;

*//**
 * The Class CryptoUtilitiesTest.
 *//*
public class CryptoUtilitiesTest {

	*//**
	 * Test enc pay load.
	 *//*
	@Test
	public void testEncPayLoad() {
		assertEquals("d9zbU2XOeuQ494Vi1kEgVT1aSKgscy7gSvF9JV+m1vhnBPmPvtkl63J9aYD6qQFRxNsPWHqMNvp9Kz/rBrqNDA==", CryptoUtilities.encPayLoad("plain text to check encryption with tokenSigningKey", "0123456789abcdef"));
	}
	
	*//**
	 * Test decrypt pay load.
	 *//*
	@Test
	public void testDecryptPayLoad(){
		assertEquals("plain text to check encryption with tokenSigningKey", CryptoUtilities.decryptPayLoad("d9zbU2XOeuQ494Vi1kEgVT1aSKgscy7gSvF9JV+m1vhnBPmPvtkl63J9aYD6qQFRxNsPWHqMNvp9Kz/rBrqNDA==", "0123456789abcdef"));
	}

}
*/