/*package com.capgemini.psd2.security.test.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.GrantedAuthority;

import com.capgemini.psd2.security.models.UserContext;

public class UserContextTest {

	List<GrantedAuthority> authorities = null;

	@Before
	public void setUp() {
		authorities = new ArrayList<>();
		GrantedAuthority grantedAuthority = new GrantedAuthority() {

			@Override
			public String getAuthority() {
				return "authority";
			}
		};
		authorities.add(grantedAuthority);
		UserContext.create("boi123", authorities);
	}

	@Test
	public void testCreate() {
		assertNotNull(UserContext.create("boi123", authorities));
	}

	@Test
	public void testGetUsername() {
		assertEquals("boi123", UserContext.create("boi123", authorities).getUsername());
	}

	@Test
	public void testGetAuthorities() {
		assertNotNull(UserContext.create("boi123", authorities).getAuthorities().get(0));
	}
}*/