package com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import com.capgemini.psd2.aisp.domain.OBActiveOrHistoricCurrencyAndAmount;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.Address;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.Amount;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.AuthorisationType;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.AuthorisingPartyAccount;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.ChargeBearer;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.Country;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.Currency;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.FinancialEventAmount;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.Party;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.Party2;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.PaymentInstructionCharge;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.PaymentInstructionPostalAddress;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.PaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.PaymentInstrumentRiskFactor;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.ProposalStatus;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.ProposingPartyAccount;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.transformer.DomesticPaymentConsentsFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBExternalAuthorisation1Code;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalPaymentContext1Code;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticConsentResponse1;
import com.capgemini.psd2.pisp.stage.domain.Charge;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticPaymentConsentsFoundationServiceTransformerTest {
	
	@InjectMocks
	DomesticPaymentConsentsFoundationServiceTransformer transformer;
	
	@Mock
	private PSD2Validator psd2Validator;
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	 
	@Test
	public void contextLoads() {
	}
	@Test
	public void testtransformDomesticPaymentResponse11(){
	PaymentInstructionProposal inputBalanceObj=new PaymentInstructionProposal();
	inputBalanceObj.setPaymentInstructionProposalId("1234");
	inputBalanceObj.setInstructionEndToEndReference("user");
	inputBalanceObj.setInstructionReference("boi");
	inputBalanceObj.setInstructionLocalInstrument("card");
	inputBalanceObj.setProposalCreationDatetime("03/14/2019");
	inputBalanceObj.setProposalStatus(ProposalStatus.Authorised);
	inputBalanceObj.setProposalStatusUpdateDatetime("02/15/2019");
	List<PaymentInstructionCharge> charges1 = new ArrayList<>();
	PaymentInstructionCharge pa= new PaymentInstructionCharge();
	pa.setChargeBearer(ChargeBearer.BorneByCreditor);
	pa.setType("vgju");
	charges1.add(pa);
	inputBalanceObj.setCharges(charges1);
	Charge c = new Charge();
	c.setChargeBearer(OBChargeBearerType1Code.BORNEBYCREDITOR);
	c.setType("ABC");
	List<Charge> charges = new ArrayList<Charge>();
	charges.add(c);
	Amount amt = new Amount();
	amt.setTransactionCurrency(12.00);
	pa.setAmount(amt);
	Currency cr = new Currency();
	cr.setIsoAlphaCode("a1s2");
	pa.setCurrency(cr);
	FinancialEventAmount amount=new FinancialEventAmount();
	amount.setTransactionCurrency(23.00);
	inputBalanceObj.setFinancialEventAmount(amount);
	Currency currency= new Currency();
	currency.setIsoAlphaCode("iuhio");
	inputBalanceObj.setTransactionCurrency(currency);
	AuthorisingPartyAccount act=new AuthorisingPartyAccount();
	act.setAccountName("nam");
	act.setAccountNumber("345");
	act.setSchemeName("debit");
	act.setSecondaryIdentification("678");
	act.setAccountIdentification("ddc45d4c");
	inputBalanceObj.setAuthorisingPartyAccount(act);
	ProposingPartyAccount ppa=new ProposingPartyAccount();
	ppa.setAccountName("nam");
	ppa.setAccountNumber("987");
	ppa.setSchemeName("credit");
	ppa.setSecondaryIdentification("456");
	ppa.setAccountIdentification("c4d512");
	inputBalanceObj.setProposingPartyAccount(ppa);
	PaymentInstructionPostalAddress add=new PaymentInstructionPostalAddress();
	add.setTownName("pune");
	add.setDepartment("A");
	add.setCountrySubDivision("MH");
	add.setSubDepartment("ABC");
	add.setPostCodeNumber("600789");
	add.setGeoCodeBuildingName("capg");
	add.setGeoCodeBuildingNumber("1521");
	Country country=new Country();
	country.setIsoCountryAlphaTwoCode("IND");
	add.setAddressCountry(country);
	List<String> line= new ArrayList<>();
	line.add("Talwade");
	add.setAddressLine(line);
	add.setAddressType("POSTAL");
	inputBalanceObj.setProposingPartyPostalAddress(add);
	inputBalanceObj.setAuthorisingPartyReference("ABCDEF");
	inputBalanceObj.setAuthorisingPartyUnstructuredReference("ANY TYPE");
	inputBalanceObj.setAuthorisationType(AuthorisationType.Any);
	Date date=new Date();
	inputBalanceObj.setAuthorisationDatetime("12/12/2018");
	PaymentInstrumentRiskFactor ref = new PaymentInstrumentRiskFactor();
	ref .setMerchantCategoryCode("BUI");
	ref.setPaymentContextCode("BILLPAYMENT");
	ref.setMerchantCustomerIdentification("SBU");
	Address cpa=new Address();
	cpa.setFirstAddressLine("134");
	cpa.setSecondAddressLine("pradhikaran");
	cpa.setThirdAddressLine("sector27");
	cpa.setFourthAddressLine("nigdi");
	cpa.setFifthAddressLine("pune");
	cpa.setPostCodeNumber("600024");
	cpa.setGeoCodeBuildingNumber("1654");
	cpa.setGeoCodeBuildingName("Vinalay");
	cpa.setAddressCountry(country);
	Country country1=new Country();
	country1.setIsoCountryAlphaTwoCode("IND");
	cpa.setAddressCountry(country1);
	ref.setCounterPartyAddress(cpa);
	inputBalanceObj.setPaymentInstructionRiskFactorReference(ref);
	transformer.transformDomesticPaymentResponse(inputBalanceObj);
	}
	
	@Test
	public void testtransformDomesticPaymentResponseForNullAdressLines(){
	PaymentInstructionProposal inputBalanceObj=new PaymentInstructionProposal();
	inputBalanceObj.setPaymentInstructionProposalId("1234");
	inputBalanceObj.setInstructionEndToEndReference("user");
	inputBalanceObj.setInstructionReference("boi");
	inputBalanceObj.setInstructionLocalInstrument("card");
	inputBalanceObj.setProposalCreationDatetime("03/14/2019");
	inputBalanceObj.setProposalStatus(ProposalStatus.Authorised);
	inputBalanceObj.setProposalStatusUpdateDatetime("02/15/2019");
	List<PaymentInstructionCharge> charges1 = new ArrayList<>();
	PaymentInstructionCharge pa= new PaymentInstructionCharge();
	pa.setChargeBearer(ChargeBearer.BorneByCreditor);
	pa.setType("vgju");
	charges1.add(pa);
	inputBalanceObj.setCharges(charges1);
	Charge c = new Charge();
	c.setChargeBearer(OBChargeBearerType1Code.BORNEBYCREDITOR);
	c.setType("ABC");
	List<Charge> charges = new ArrayList<Charge>();
	charges.add(c);
	Amount amt = new Amount();
	amt.setTransactionCurrency(12.00);
	pa.setAmount(amt);
	Currency cr = new Currency();
	cr.setIsoAlphaCode("a1s2");
	pa.setCurrency(cr);
	FinancialEventAmount amount=new FinancialEventAmount();
	amount.setTransactionCurrency(23.00);
	inputBalanceObj.setFinancialEventAmount(amount);
	Currency currency= new Currency();
	currency.setIsoAlphaCode("iuhio");
	inputBalanceObj.setTransactionCurrency(currency);
	AuthorisingPartyAccount act=new AuthorisingPartyAccount();
	act.setAccountName("nam");
	act.setAccountNumber("345");
	act.setSchemeName("debit");
	act.setSecondaryIdentification("678");
	act.setAccountIdentification("ddc45d4c");
	inputBalanceObj.setAuthorisingPartyAccount(act);
	ProposingPartyAccount ppa=new ProposingPartyAccount();
	ppa.setAccountName("nam");
	ppa.setAccountNumber("987");
	ppa.setSchemeName("credit");
	ppa.setSecondaryIdentification("456");
	ppa.setAccountIdentification("c4d512");
	inputBalanceObj.setProposingPartyAccount(ppa);
	PaymentInstructionPostalAddress add=new PaymentInstructionPostalAddress();
	add.setTownName("pune");
	add.setDepartment("A");
	add.setCountrySubDivision("MH");
	add.setSubDepartment("ABC");
	add.setPostCodeNumber("600789");
	add.setGeoCodeBuildingName("capg");
	add.setGeoCodeBuildingNumber("1521");
	Country country=new Country();
	country.setIsoCountryAlphaTwoCode("IND");
	add.setAddressCountry(country);
	List<String> line= new ArrayList<>();
	line.add("Talwade");
	add.setAddressLine(line);
	add.setAddressType("POSTAL");
	inputBalanceObj.setProposingPartyPostalAddress(add);
	inputBalanceObj.setAuthorisingPartyReference("ABCDEF");
	inputBalanceObj.setAuthorisingPartyUnstructuredReference("ANY TYPE");
	inputBalanceObj.setAuthorisationType(AuthorisationType.Any);
	Date date=new Date();
	inputBalanceObj.setAuthorisationDatetime("12/03/2019");
	PaymentInstrumentRiskFactor ref = new PaymentInstrumentRiskFactor();
	ref .setMerchantCategoryCode("BUI");
	ref.setPaymentContextCode("BILLPAYMENT");
	ref.setMerchantCustomerIdentification("SBU");
	Address cpa=new Address();
	cpa.setFirstAddressLine("134");
	cpa.setSecondAddressLine(null);
	cpa.setThirdAddressLine("sector27");
	cpa.setFourthAddressLine("nigdi");
	cpa.setFifthAddressLine(null);
	cpa.setPostCodeNumber("600024");
	cpa.setGeoCodeBuildingNumber("1654");
	cpa.setGeoCodeBuildingName("Vinalay");
	cpa.setAddressCountry(country);
	Country country1=new Country();
	country1.setIsoCountryAlphaTwoCode("IND");
	cpa.setAddressCountry(country1);
	ref.setCounterPartyAddress(cpa);
	inputBalanceObj.setPaymentInstructionRiskFactorReference(ref);
	transformer.transformDomesticPaymentResponse(inputBalanceObj);
	}
	@Test
	public void testtransformDomesticPaymentResponse1(){
		PaymentInstructionProposal inputBalanceObj=new PaymentInstructionProposal();
		inputBalanceObj.setPaymentInstructionProposalId("1234");
		inputBalanceObj.setInstructionEndToEndReference("user");
		inputBalanceObj.setInstructionReference("boi");
		inputBalanceObj.setInstructionLocalInstrument("card");
		List<PaymentInstructionCharge> charges1 = new ArrayList<>();
		PaymentInstructionCharge pa= new PaymentInstructionCharge();
		pa.setChargeBearer(ChargeBearer.BorneByCreditor);
		pa.setType("vgju");
		charges1.add(pa);
		inputBalanceObj.setCharges(charges1);
		Charge c = new Charge();
		c.setChargeBearer(OBChargeBearerType1Code.BORNEBYCREDITOR);
		c.setType("ABC");
		List<Charge> charges = new ArrayList<Charge>();
		charges.add(c);
		Amount amt = new Amount();
		amt.setTransactionCurrency(12.00);
		pa.setAmount(amt);
		Currency cr = new Currency();
		cr.setIsoAlphaCode("a1s2");
		pa.setCurrency(cr);
		FinancialEventAmount amount=new FinancialEventAmount();
		amount.setTransactionCurrency(23.00);
		inputBalanceObj.setFinancialEventAmount(amount);
		Currency currency=new Currency();
		currency.setIsoAlphaCode("iuhio");
		inputBalanceObj.setTransactionCurrency(currency);
		AuthorisingPartyAccount act=new AuthorisingPartyAccount();
		act.setAccountName("nam");
		act.setAccountNumber("345");
		act.setSchemeName("debit");
		act.setSecondaryIdentification("678");
		inputBalanceObj.setAuthorisingPartyAccount(act);
		ProposingPartyAccount ppa=new ProposingPartyAccount();
		ppa.setAccountName("nam");
		ppa.setAccountNumber("987");
		ppa.setSchemeName("credit");
		ppa.setSecondaryIdentification("456");
		inputBalanceObj.setProposingPartyAccount(ppa);
		PaymentInstructionPostalAddress add=new PaymentInstructionPostalAddress();
		add.setTownName("pune");
		add.setDepartment("A");
		add.setCountrySubDivision("MH");
		add.setSubDepartment("ABC");
		add.setPostCodeNumber("600789");
		add.setGeoCodeBuildingName("capg");
		add.setGeoCodeBuildingNumber("1521");
		Country country=new Country();
		country.setIsoCountryAlphaTwoCode("IND");
		add.setAddressCountry(country);
		List<String> line= new ArrayList<>();
		line.add("Talwade");
		add.setAddressLine(line);
		add.setAddressType("POSTAL");
		inputBalanceObj.setProposingPartyPostalAddress(add);
		inputBalanceObj.setAuthorisingPartyReference("ABCDEF");
		inputBalanceObj.setAuthorisingPartyUnstructuredReference("ANY TYPE");
		inputBalanceObj.setAuthorisationType(AuthorisationType.Any);
		Date date=new Date();
		inputBalanceObj.setAuthorisationDatetime("04/04/2019");
		PaymentInstrumentRiskFactor ref = new PaymentInstrumentRiskFactor();
		ref.setMerchantCategoryCode("BUI");
		ref.setPaymentContextCode("BILLPAYMENT");
		ref.setMerchantCustomerIdentification("SBU");
		Address cpa=new Address();
		cpa.setFirstAddressLine("134");
		cpa.setSecondAddressLine("pradhikaran");
		cpa.setThirdAddressLine("sector27");
		cpa.setFourthAddressLine("nigdi");
		cpa.setFifthAddressLine("pune");
		cpa.setPostCodeNumber("600024");
		cpa.setGeoCodeBuildingNumber("1654");
		cpa.setGeoCodeBuildingName("Vinalay");
		cpa.setAddressCountry(country);
		Country country1 =new Country();
		country1.setIsoCountryAlphaTwoCode("IND");
		cpa.setAddressCountry(country1);
		ref.setCounterPartyAddress(cpa);
		inputBalanceObj.setPaymentInstructionRiskFactorReference(ref);
	Currency tc = new Currency();
	inputBalanceObj.setPaymentInstructionProposalId("1234");
	inputBalanceObj.setInstructionEndToEndReference("user");
	inputBalanceObj.setCharges(null);
	inputBalanceObj.setInstructionReference("boi");
	inputBalanceObj.setInstructionLocalInstrument(null);
	inputBalanceObj.setAuthorisingPartyAccount(null);
	inputBalanceObj.setProposingPartyAccount(null);
	inputBalanceObj.setProposingPartyPostalAddress(null);
	inputBalanceObj.setAuthorisationType(null);
	inputBalanceObj.setPaymentInstructionRiskFactorReference(null);
	inputBalanceObj.setPaymentInstructionRiskFactorReference(null);
	inputBalanceObj.authorisationDatetime(null);
	inputBalanceObj.setFinancialEventAmount(amount);
	transformer.transformDomesticPaymentResponse(inputBalanceObj);
	}
	
	@Test
	public void testtransformDomesticPaymentResponse(){
		PaymentInstructionProposal inputBalanceObj=new PaymentInstructionProposal();
		inputBalanceObj.setPaymentInstructionProposalId("1234");
		inputBalanceObj.setInstructionEndToEndReference("user");
		inputBalanceObj.setInstructionReference("boi");
		inputBalanceObj.setInstructionLocalInstrument("card");
		FinancialEventAmount amt=new FinancialEventAmount();
		amt.setTransactionCurrency(23.00);
		inputBalanceObj.setFinancialEventAmount(amt);
		Currency currency= new Currency();
		currency.isoAlphaCode("EU");
		inputBalanceObj.setTransactionCurrency(currency);
		AuthorisingPartyAccount act=new AuthorisingPartyAccount();
		act.setAccountName("nam");
		act.setAccountNumber("345");
		act.setSchemeName("debit");
		act.setSecondaryIdentification("678");
		inputBalanceObj.setAuthorisingPartyAccount(act);
		ProposingPartyAccount ppa=new ProposingPartyAccount();
		ppa.setAccountName("nam");
		ppa.setAccountNumber("987");
		ppa.setSchemeName("credit");
		ppa.setSecondaryIdentification("456");
		inputBalanceObj.setProposingPartyAccount(ppa);
		PaymentInstructionPostalAddress add=new PaymentInstructionPostalAddress();
		add.setTownName("pune");
		add.setDepartment("A");
		add.setCountrySubDivision("MH");
		add.setSubDepartment("ABC");
		add.setPostCodeNumber("600789");
		add.setGeoCodeBuildingName("capg");
		add.setGeoCodeBuildingNumber("1521");
		Country country =new Country();
		country.setIsoCountryAlphaTwoCode("IND");
		add.setAddressCountry(country);
		List<String> line= new ArrayList<>();
		line.add("Talwade");
		add.setAddressLine(line);
		add.setAddressType("POSTAL");
		inputBalanceObj.setProposingPartyPostalAddress(add);
		inputBalanceObj.setAuthorisationType(AuthorisationType.Any);
		Date date=new Date();
		inputBalanceObj.setAuthorisationDatetime("15/03/2019");
		
		PaymentInstrumentRiskFactor ref=new PaymentInstrumentRiskFactor();
		ref.setMerchantCategoryCode("BUI");
		ref.setPaymentContextCode("BILLPAYMENT");
		ref.setMerchantCustomerIdentification("SBU");
		Address cpa=new Address();
		cpa.setFirstAddressLine("134");
		cpa.setSecondAddressLine("pradhikaran");
		cpa.setThirdAddressLine("sector27");
		cpa.setFourthAddressLine("nigdi");
		cpa.setFifthAddressLine("pune");
		cpa.setPostCodeNumber("600024");
		cpa.setGeoCodeBuildingNumber("1654");
		cpa.setGeoCodeBuildingName("Vinalay");
		cpa.setAddressCountry(country);
		ref.setCounterPartyAddress(cpa);
		inputBalanceObj.setPaymentInstructionRiskFactorReference(ref);
		transformer.transformDomesticPaymentResponse(inputBalanceObj);
	}
	
	@Test
	public void testTransformDomesticConsentResponseFromFDToAPIForInsert1(){
		PaymentInstructionProposal inputBalanceObj=new PaymentInstructionProposal();
		PaymentInstructionCharge charge = new PaymentInstructionCharge();
		//inputBalanceObj.getProposingPartyAccount().accountIdentification("123456");
		Amount amount = new Amount();
		Currency currency = new Currency();
		FinancialEventAmount financialAmount = new FinancialEventAmount();
		Currency transactionCurrency = new Currency();
		AuthorisingPartyAccount authPartyAccount = new AuthorisingPartyAccount();
		ProposingPartyAccount propPartyAccount = new ProposingPartyAccount();
		PaymentInstructionPostalAddress propPartyProposalAdd  = new PaymentInstructionPostalAddress();
		Country addCountry = new Country();
		List<String> addLine = new ArrayList<String>();
		PaymentInstrumentRiskFactor ref=new PaymentInstrumentRiskFactor();
		OBCashAccountDebtor3  oBCashAccountDebtor3=new OBCashAccountDebtor3(); 
		OBCashAccountCreditor2 oBCashAccountCreditor2=new OBCashAccountCreditor2();
		oBCashAccountDebtor3.setSchemeName("ABC");
		oBCashAccountDebtor3.setIdentification("123456789");
		oBCashAccountCreditor2.setIdentification("123456");
		OBWriteDataDomesticConsentResponse1 oBWriteDataDomesticConsentResponse1 = new OBWriteDataDomesticConsentResponse1();
		OBRisk1 oBRisk1 = new OBRisk1();
		OBRisk1DeliveryAddress oBRisk1DeliveryAddress = new OBRisk1DeliveryAddress();
		
		oBRisk1DeliveryAddress.setAddressLine(addLine);
		oBRisk1DeliveryAddress.setStreetName("pata nai");
		oBRisk1DeliveryAddress.setTownName("pata nai");
		oBRisk1DeliveryAddress.setBuildingNumber("1");
		oBRisk1DeliveryAddress.setPostCode("3244");
		oBRisk1DeliveryAddress.setCountrySubDivision("ABCD");
		oBRisk1DeliveryAddress.setCountry("India");
		OBAuthorisation1 oBAuthorisation1 = new OBAuthorisation1();
		oBAuthorisation1.setCompletionDateTime("xcxc");
		
		oBAuthorisation1.setAuthorisationType(OBExternalAuthorisation1Code.fromValue("hdg"));
		Address counterPartyAddress=new Address();
		counterPartyAddress.setFifthAddressLine("abc");
		counterPartyAddress.secondAddressLine("xyz");
		counterPartyAddress.setGeoCodeBuildingName("pata nai");
		counterPartyAddress.setGeoCodeBuildingNumber("pata nai");
		counterPartyAddress.setPostCodeNumber("pata nai");
		counterPartyAddress.firstAddressLine("ATIUY");
		counterPartyAddress.setFourthAddressLine("pata nai");
		counterPartyAddress.setFifthAddressLine("abc");
		counterPartyAddress.setAddressCountry(addCountry);
		
		PaymentInstrumentRiskFactor riskFactor=new PaymentInstrumentRiskFactor();
		riskFactor.setPaymentContextCode("pata nai");
		riskFactor.setMerchantCategoryCode("abc");
		riskFactor.setMerchantCustomerIdentification("abc");
		riskFactor.setCounterPartyAddress(counterPartyAddress);
		
		addLine.add("add1");
		addLine.add("add2");
		addLine.add("add3");
		addCountry.setIsoCountryAlphaTwoCode("IND");
		propPartyProposalAdd.setAddressType("permanent");
		propPartyProposalAdd.setDepartment("pata nai");
		propPartyProposalAdd.setSubDepartment("pata nai");
		propPartyProposalAdd.setGeoCodeBuildingName("pata nai");
		propPartyProposalAdd.setGeoCodeBuildingNumber("pata nai");
		propPartyProposalAdd.setPostCodeNumber("pata nai");
		propPartyProposalAdd.setTownName("pata nai");
		propPartyProposalAdd.setCountrySubDivision("pata nai");
		propPartyProposalAdd.setAddressCountry(addCountry);
		propPartyProposalAdd.setAddressLine(addLine);
		
		propPartyAccount.setSchemeName("pata nai");
		propPartyAccount.setAccountName("pata nai");
		propPartyAccount.accountNumber("pata nai");
		propPartyAccount.setSecondaryIdentification("pata nai");
		authPartyAccount.setSchemeName("pata nai");
		authPartyAccount.setAccountName("pata nai");
		authPartyAccount.setAccountNumber("pata nai");
		authPartyAccount.setSecondaryIdentification("nai pata");
		transactionCurrency.setIsoAlphaCode("pata nai");
		financialAmount.setTransactionCurrency(98.00);
		currency.setIsoAlphaCode("NZD");
		amount.setTransactionCurrency(22.0);
		charge.setChargeBearer(ChargeBearer.BorneByCreditor);
		charge.setType("xyz");
		charge.setAmount(amount);
		charge.setCurrency(currency);
		List<PaymentInstructionCharge> chargeList = new ArrayList<PaymentInstructionCharge>();
		chargeList.add(charge);
		
		
		inputBalanceObj.setPaymentInstructionProposalId("1234");
		inputBalanceObj.setProposalCreationDatetime("03/08/2019");
		inputBalanceObj.setProposalStatus(ProposalStatus.AwaitingAuthorisation);
		inputBalanceObj.setProposalStatusUpdateDatetime("04/05/2019");
		inputBalanceObj.setCharges(chargeList);
		inputBalanceObj.setInstructionReference("aaa");
		inputBalanceObj.setInstructionEndToEndReference("bbb");
		inputBalanceObj.setInstructionLocalInstrument("cc");
		inputBalanceObj.setFinancialEventAmount(financialAmount);
		inputBalanceObj.setTransactionCurrency(transactionCurrency);
		inputBalanceObj.setAuthorisingPartyAccount(authPartyAccount);
		inputBalanceObj.setProposingPartyAccount(propPartyAccount);
		inputBalanceObj.setProposingPartyPostalAddress(propPartyProposalAdd);
		inputBalanceObj.setAuthorisingPartyReference("pata nai");
		inputBalanceObj.setAuthorisingPartyUnstructuredReference("pata nai");
		inputBalanceObj.setAuthorisationType(AuthorisationType.Single);
		inputBalanceObj.setAuthorisationDatetime("15/05/2019");
		inputBalanceObj.setPaymentInstructionRiskFactorReference(riskFactor);
		
		oBWriteDataDomesticConsentResponse1.setConsentId("pata nai");
		oBWriteDataDomesticConsentResponse1.setCreationDateTime("abc");
		oBWriteDataDomesticConsentResponse1.setStatus(OBExternalConsentStatus1Code.fromValue("abc"));
		oBWriteDataDomesticConsentResponse1.setStatusUpdateDateTime("abc");
		oBWriteDataDomesticConsentResponse1.setAuthorisation(oBAuthorisation1);
		
			oBRisk1.setPaymentContextCode(OBExternalPaymentContext1Code.ECOMMERCEGOODS);
			oBRisk1.setMerchantCategoryCode("596");
			oBRisk1.setMerchantCustomerIdentification("053598653254");
			List<String> addressLine = new ArrayList<String>();
			addressLine.add("Ireland");
			addressLine.add("UK");
			addressLine.add("add4");
			oBRisk1DeliveryAddress.setAddressLine(addressLine);
			oBRisk1DeliveryAddress.setStreetName("ftfftyf");
			oBRisk1DeliveryAddress.setBuildingNumber("talwade");
			oBRisk1DeliveryAddress.setPostCode("380061");
			oBRisk1DeliveryAddress.setTownName("ireland");
			oBRisk1DeliveryAddress.setCountrySubDivision("PQRST");
			oBRisk1DeliveryAddress.setCountry("GB");
			oBRisk1.setDeliveryAddress(oBRisk1DeliveryAddress);
			inputBalanceObj.setPaymentInstructionProposalId(null);
			inputBalanceObj.setProposalCreationDatetime(null);
			inputBalanceObj.setProposalStatusUpdateDatetime(null);
			inputBalanceObj.setCharges(null);
			inputBalanceObj.setInstructionReference(null);
			inputBalanceObj.setInstructionEndToEndReference(null);
			inputBalanceObj.setInstructionLocalInstrument(null);
			inputBalanceObj.setAuthorisingPartyAccount(null);
			inputBalanceObj.setProposingPartyAccount(null);
			inputBalanceObj.setProposingPartyPostalAddress(null);
			inputBalanceObj.setAuthorisingPartyReference(null);
			inputBalanceObj.setAuthorisingPartyUnstructuredReference(null);
			inputBalanceObj.setAuthorisationType(null);
			inputBalanceObj.setAuthorisationDatetime(null);
			inputBalanceObj.setPaymentInstructionRiskFactorReference(null);	
			transformer.transformDomesticConsentResponseFromFDToAPIForInsert(inputBalanceObj);
	}
	@Test
	public void testTransformDomesticConsentResponseFromFDToAPIForInsert(){
		PaymentInstructionProposal inputBalanceObj=new PaymentInstructionProposal();
		PaymentInstructionCharge charge = new PaymentInstructionCharge();
		Amount amount = new Amount();
		Currency currency = new Currency();
		FinancialEventAmount financialAmount = new FinancialEventAmount();
		Currency transactionCurrency = new Currency();
		AuthorisingPartyAccount authPartyAccount = new AuthorisingPartyAccount();
		ProposingPartyAccount propPartyAccount = new ProposingPartyAccount();
		PaymentInstructionPostalAddress propPartyProposalAdd  = new PaymentInstructionPostalAddress();
		Country addCountry = new Country();
		List<String> addLine = new ArrayList<String>();

		Address counterPartyAddress = new Address();
		OBWriteDataDomesticConsentResponse1 oBWriteDataDomesticConsentResponse1 = new OBWriteDataDomesticConsentResponse1();
		OBRisk1 oBRisk1 = new OBRisk1();
		OBRisk1DeliveryAddress oBRisk1DeliveryAddress = new OBRisk1DeliveryAddress();
		
		oBRisk1DeliveryAddress.setAddressLine(addLine);
		oBRisk1DeliveryAddress.setStreetName("pata nai");
		oBRisk1DeliveryAddress.setTownName("pata nai");
		oBRisk1DeliveryAddress.setBuildingNumber("1");
		oBRisk1DeliveryAddress.setPostCode("3244");
		oBRisk1DeliveryAddress.setCountrySubDivision("XYZ");
		oBRisk1DeliveryAddress.setCountry("India");
		OBAuthorisation1 oBAuthorisation1 = new OBAuthorisation1();
		oBAuthorisation1.setCompletionDateTime("xcxc");
		
		oBAuthorisation1.setAuthorisationType(OBExternalAuthorisation1Code.fromValue("hdg"));
		counterPartyAddress.setFirstAddressLine("abc");
		counterPartyAddress.secondAddressLine(null);
		counterPartyAddress.setGeoCodeBuildingName("pata nai");
		counterPartyAddress.setGeoCodeBuildingNumber("pata nai");
		counterPartyAddress.setPostCodeNumber("pata nai");
		counterPartyAddress.setThirdAddressLine("dc54cd5c1");
		counterPartyAddress.setFourthAddressLine("pata nai");
		counterPartyAddress.setFifthAddressLine("abc");
		counterPartyAddress.setAddressCountry(addCountry);
		
		PaymentInstrumentRiskFactor riskFactor=new PaymentInstrumentRiskFactor();
		riskFactor.setPaymentContextCode("pata nai");
		riskFactor.setMerchantCategoryCode("abc");
		riskFactor.setMerchantCustomerIdentification("abc");
		riskFactor.setCounterPartyAddress(counterPartyAddress);
		addLine.add("add1");
		addLine.add("add2");
		addCountry.setIsoCountryAlphaTwoCode("IND");
		propPartyProposalAdd.setAddressType("permanent");
		propPartyProposalAdd.setDepartment("pata nai");
		propPartyProposalAdd.setSubDepartment("pata nai");
		propPartyProposalAdd.setGeoCodeBuildingName("pata nai");
		propPartyProposalAdd.setGeoCodeBuildingNumber("pata nai");
		propPartyProposalAdd.setPostCodeNumber("pata nai");
		propPartyProposalAdd.setTownName("pata nai");
		propPartyProposalAdd.setCountrySubDivision("pata nai");
		propPartyProposalAdd.setAddressCountry(addCountry);
		propPartyProposalAdd.setAddressLine(addLine);
		
		propPartyAccount.setSchemeName("pata nai");
		propPartyAccount.setAccountName("pata nai");
		propPartyAccount.accountNumber("pata nai");
		propPartyAccount.setSecondaryIdentification("pata nai");
		propPartyAccount.setAccountIdentification("dcd54c5");
		authPartyAccount.setSchemeName("pata nai");
		authPartyAccount.setAccountName("pata nai");
		authPartyAccount.setAccountNumber("pata nai");
		authPartyAccount.setSecondaryIdentification("nai pata");
		authPartyAccount.setAccountIdentification("d54c541");
		transactionCurrency.setIsoAlphaCode("pata nai");
		financialAmount.setTransactionCurrency(98.00);
		currency.setIsoAlphaCode("NZD");
		amount.setTransactionCurrency(22.0);
		charge.setChargeBearer(ChargeBearer.BorneByCreditor);
		charge.setType("xyz");
		charge.setAmount(amount);
		charge.setCurrency(currency);
		List<PaymentInstructionCharge> chargeList = new ArrayList<PaymentInstructionCharge>();
		chargeList.add(charge);
		
		
		inputBalanceObj.setPaymentInstructionProposalId("1234");
		inputBalanceObj.setProposalCreationDatetime("07/05/2019");
		inputBalanceObj.setProposalStatus(ProposalStatus.Rejected);
		inputBalanceObj.setProposalStatusUpdateDatetime("08/05/2019");
		inputBalanceObj.setCharges(chargeList);
		inputBalanceObj.setInstructionReference("aaa");
		inputBalanceObj.setInstructionEndToEndReference("bbb");
		inputBalanceObj.setInstructionLocalInstrument("cc");
		inputBalanceObj.setFinancialEventAmount(financialAmount);
		inputBalanceObj.setTransactionCurrency(transactionCurrency);
		inputBalanceObj.setAuthorisingPartyAccount(authPartyAccount);
		inputBalanceObj.setProposingPartyAccount(propPartyAccount);
		inputBalanceObj.setProposingPartyPostalAddress(propPartyProposalAdd);
		inputBalanceObj.setAuthorisingPartyReference("pata nai");
		inputBalanceObj.setAuthorisingPartyUnstructuredReference("pata nai");
		inputBalanceObj.setAuthorisationType(AuthorisationType.Single);
		inputBalanceObj.setAuthorisationDatetime("20/05/2019");
		inputBalanceObj.setPaymentInstructionRiskFactorReference(riskFactor);
		
		oBWriteDataDomesticConsentResponse1.setConsentId("pata nai");
		oBWriteDataDomesticConsentResponse1.setCreationDateTime("abc");
		oBWriteDataDomesticConsentResponse1.setStatus(OBExternalConsentStatus1Code.fromValue("abc"));
		oBWriteDataDomesticConsentResponse1.setStatusUpdateDateTime("abc");
		oBWriteDataDomesticConsentResponse1.setAuthorisation(oBAuthorisation1);
		if(!NullCheckUtils.isNullOrEmpty(inputBalanceObj.getPaymentInstructionRiskFactorReference())) {
		oBRisk1.setPaymentContextCode(OBExternalPaymentContext1Code.ECOMMERCEGOODS);
		oBRisk1.setMerchantCategoryCode("596");
		oBRisk1.setMerchantCustomerIdentification("053598653254");
		List<String> addressLine = new ArrayList<String>();
		addressLine.add("Ireland");
		addressLine.add("UK");
		oBRisk1DeliveryAddress.setAddressLine(addressLine);
		oBRisk1DeliveryAddress.setStreetName("ftfftyf");
		oBRisk1DeliveryAddress.setBuildingNumber("talwade");
		oBRisk1DeliveryAddress.setPostCode("380061");
		oBRisk1DeliveryAddress.setTownName("ireland");
		oBRisk1DeliveryAddress.setCountrySubDivision("ABCD");
		oBRisk1DeliveryAddress.setCountry("GB");
		oBRisk1.setDeliveryAddress(oBRisk1DeliveryAddress);
		}else {
			oBRisk1.setPaymentContextCode(OBExternalPaymentContext1Code.ECOMMERCEGOODS);
			oBRisk1.setMerchantCategoryCode("596");
			oBRisk1.setMerchantCustomerIdentification("053598653254");
			List<String> addressLine = new ArrayList<String>();
			addressLine.add("Ireland");
			addressLine.add("UK");
			oBRisk1DeliveryAddress.setAddressLine(addressLine);
			oBRisk1DeliveryAddress.setStreetName("ftfftyf");
			oBRisk1DeliveryAddress.setBuildingNumber("talwade");
			oBRisk1DeliveryAddress.setPostCode("380061");
			oBRisk1DeliveryAddress.setTownName("ireland");
			oBRisk1DeliveryAddress.setCountrySubDivision("ABCD");
			oBRisk1DeliveryAddress.setCountry("GB");
			oBRisk1.setDeliveryAddress(oBRisk1DeliveryAddress);
		}
		transformer.transformDomesticConsentResponseFromFDToAPIForInsert(inputBalanceObj);
	}
	
	
	
	
	
	@Test
	public void testTransformDomesticConsentResponseFromAPIToFDForInsert1(){
		
	//	ReflectionTestUtils.setField(transformer,"apiChannelBrandValue","BOIUK");
		
		PaymentInstructionProposal paymentInstructionProposal = new PaymentInstructionProposal();
		CustomDPaymentConsentsPOSTRequest paymentConsentsRequest = new CustomDPaymentConsentsPOSTRequest();
		OBDomestic1 obDomestic1 = new OBDomestic1();
		OBWriteDataDomesticConsent1 obWriteDataDomesticConsent1 = new OBWriteDataDomesticConsent1();
		OBActiveOrHistoricCurrencyAndAmount amount = new OBActiveOrHistoricCurrencyAndAmount();
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		OBPostalAddress6 creditorPostalAddress = new OBPostalAddress6(); 
		List<String> addressLine = new ArrayList<String>();
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		OBAuthorisation1 authorisation = new OBAuthorisation1();
		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		PaymentInstrumentRiskFactor ref=new PaymentInstrumentRiskFactor();
		
		Country addressCountry = new Country();
		
		PaymentInstructionPostalAddress proposingPartyPostalAddress = new PaymentInstructionPostalAddress();
		ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();
		
		AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();
		Currency transactionCurrency = new Currency();
		FinancialEventAmount financialEventAmount = new FinancialEventAmount();
		
		financialEventAmount.setTransactionCurrency(22.0);
		transactionCurrency.setIsoAlphaCode("NZD");
		authorisingPartyAccount.setSchemeName("pata nai");
		authorisingPartyAccount.setAccountName("pata nai");
		authorisingPartyAccount.setAccountNumber("pata nai");
		authorisingPartyAccount.setSecondaryIdentification("pata nai");
		proposingPartyAccount.setSchemeName("pata nai");
		proposingPartyAccount.setAccountNumber("pata nai");
		proposingPartyAccount.setAccountName("pata nai");
		proposingPartyAccount.setSecondaryIdentification("pata nai");
		
		proposingPartyPostalAddress.setAddressType("permanent");
		proposingPartyPostalAddress.setSubDepartment("kuch bhi");
		proposingPartyPostalAddress.setDepartment("pata nai");
		proposingPartyPostalAddress.setGeoCodeBuildingName("pata nai");
		proposingPartyPostalAddress.setGeoCodeBuildingNumber("pata nai");
		proposingPartyPostalAddress.setTownName("pata nai");
		proposingPartyPostalAddress.setCountrySubDivision("pata nai");
		proposingPartyPostalAddress.setAddressCountry(addressCountry);
		proposingPartyPostalAddress.setAddressLine(addressLine);
		
		addressCountry.setIsoCountryAlphaTwoCode("pata nai");
		Address counterPartyAdress=new Address();
		counterPartyAdress.setAddressCountry(addressCountry);
		ref.setCounterPartyAddress(counterPartyAdress);
		deliveryAddress.setAddressLine(addressLine);
		deliveryAddress.setStreetName("abc");
		deliveryAddress.setBuildingNumber("1233");
		deliveryAddress.setPostCode("3111455");
		deliveryAddress.setTownName("pune");
		deliveryAddress.setCountrySubDivision("ABCD");
		deliveryAddress.setCountry("India");
		
		risk.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);
		risk.setMerchantCategoryCode("pata nai");
		risk.setMerchantCustomerIdentification("pata nai");
		risk.setDeliveryAddress(deliveryAddress);
		authorisation.setAuthorisationType(OBExternalAuthorisation1Code.ANY);
		authorisation.completionDateTime(""+new Date());
		remittanceInformation.setUnstructured("pata nai");
		remittanceInformation.setReference("pata nai");
		
		addressLine.add("abc");
		addressLine.add("xyz");
		creditorPostalAddress.setAddressType(com.capgemini.psd2.pisp.domain.OBAddressTypeCode.POSTAL);
		creditorPostalAddress.setDepartment("pata nai");
		creditorPostalAddress.setSubDepartment("pata nai");
		creditorPostalAddress.setStreetName("pata nai");
		creditorPostalAddress.setBuildingNumber("pata nai");
		creditorPostalAddress.setPostCode("pata nai");
		creditorPostalAddress.setTownName("pata nai");
		creditorPostalAddress.setCountrySubDivision("pata nai");
		creditorPostalAddress.setCountry("India");
		creditorPostalAddress.setAddressLine(addressLine);
		debtorAccount.setName("pata nai");
		debtorAccount.setIdentification("pata nai");
		debtorAccount.setName("pata nai");
		debtorAccount.setSecondaryIdentification("pata nai");
		debtorAccount.setIdentification("123456789");
		debtorAccount.setSchemeName("PISP");
		creditorAccount.setSchemeName("pata nai");
		creditorAccount.setIdentification("pata nai");
		creditorAccount.setName("pata nai");
		creditorAccount.setSecondaryIdentification("pata nai");
		creditorAccount.setIdentification("123456789");
		amount.setAmount("29.0");
		amount.setCurrency("NZD");
		
		obDomestic1.setInstructionIdentification("pata nai");
		obDomestic1.setEndToEndIdentification("pata nai");
		obDomestic1.setLocalInstrument("pata nai");
		OBDomestic1InstructedAmount amt = new OBDomestic1InstructedAmount();
		amt.setAmount("10.00");
		amt.setCurrency("dollar");
		obDomestic1.setInstructedAmount(amt );
		obDomestic1.setDebtorAccount(debtorAccount);
		obDomestic1.setCreditorAccount(creditorAccount);
		obDomestic1.setCreditorPostalAddress(creditorPostalAddress);
		obDomestic1.setRemittanceInformation(remittanceInformation);
		obWriteDataDomesticConsent1.setInitiation(obDomestic1);
		obWriteDataDomesticConsent1.setAuthorisation(authorisation);
		
		paymentConsentsRequest.setData(obWriteDataDomesticConsent1);
		paymentConsentsRequest.setRisk(risk);
		
		paymentInstructionProposal.setInstructionReference("pata nai");
		paymentInstructionProposal.setInstructionEndToEndReference("pata nai");
		paymentInstructionProposal.setInstructionLocalInstrument("pata nai");
		paymentInstructionProposal.setAuthorisingPartyUnstructuredReference("pata nai");
		paymentInstructionProposal.setAuthorisingPartyReference("pata nai");
		paymentInstructionProposal.setAuthorisationType(AuthorisationType.Any);
		paymentInstructionProposal.setAuthorisationDatetime("14/02/2019");
		paymentInstructionProposal.setPaymentInstructionRiskFactorReference(ref);
		Party2 proposingParty=new Party2();
		paymentInstructionProposal.setProposingParty(proposingParty);
		paymentInstructionProposal.setProposingPartyPostalAddress(proposingPartyPostalAddress);
		paymentInstructionProposal.setProposingPartyAccount(proposingPartyAccount);
		Party2 authorisingParty=new Party2();
		paymentInstructionProposal.setAuthorisingParty(authorisingParty);
		paymentInstructionProposal.setAuthorisingPartyAccount(authorisingPartyAccount);
		paymentInstructionProposal.setTransactionCurrency(transactionCurrency);
		paymentInstructionProposal.setFinancialEventAmount(financialEventAmount);
		Map<String, String> params = new HashMap<String,String>();
		params.put("tenant_id", "BOIUK");
		params.put("X-BOI-CHANNEL", "channel");
		transformer.transformDomesticConsentResponseFromAPIToFDForInsert(paymentConsentsRequest, params);
	}
	
	@Test
	public void testTransformDomesticConsentResponseFromAPIToFDForInsert(){
		
		//ReflectionTestUtils.setField(transformer,"apiChannelBrandValue","BOIROI");
		
		PaymentInstructionProposal paymentInstructionProposal = new PaymentInstructionProposal();
		CustomDPaymentConsentsPOSTRequest paymentConsentsRequest = new CustomDPaymentConsentsPOSTRequest();
		OBDomestic1 obDomestic1 = new OBDomestic1();
		OBWriteDataDomesticConsent1 obWriteDataDomesticConsent1 = new OBWriteDataDomesticConsent1();
		OBDomestic1InstructedAmount amount = new OBDomestic1InstructedAmount();
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		OBPostalAddress6 creditorPostalAddress = new OBPostalAddress6(); 
		List<String> addressLine = new ArrayList<String>();
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		OBAuthorisation1 authorisation = new OBAuthorisation1();
		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();

		
		Country addressCountry = new Country();

		PaymentInstructionPostalAddress proposingPartyPostalAddress = new PaymentInstructionPostalAddress();
		ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();

		AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();
		Currency transactionCurrency = new Currency();
		FinancialEventAmount financialEventAmount = new FinancialEventAmount();
		
		financialEventAmount.setTransactionCurrency(22.0);
		transactionCurrency.setIsoAlphaCode("NZD");
		authorisingPartyAccount.setSchemeName("pata nai");
		authorisingPartyAccount.setAccountName("pata nai");
		authorisingPartyAccount.setAccountNumber("pata nai");
		authorisingPartyAccount.setSecondaryIdentification("pata nai");
		proposingPartyAccount.setSchemeName("pata nai");
		proposingPartyAccount.setAccountNumber("pata nai");
		proposingPartyAccount.setAccountName("pata nai");
		proposingPartyAccount.setSecondaryIdentification("pata nai");
		addressLine.add("FirstAddressLine");
		addressLine.add("SecondAddressLine");
		proposingPartyPostalAddress.setAddressType("permanent");
		proposingPartyPostalAddress.setSubDepartment("kuch bhi");
		proposingPartyPostalAddress.setDepartment("pata nai");
		proposingPartyPostalAddress.setGeoCodeBuildingName("pata nai");
		proposingPartyPostalAddress.setGeoCodeBuildingNumber("pata nai");
		proposingPartyPostalAddress.setTownName("pata nai");
		proposingPartyPostalAddress.setCountrySubDivision("pata nai");
		proposingPartyPostalAddress.setAddressCountry(addressCountry);
		proposingPartyPostalAddress.setAddressLine(addressLine);
		
		addressCountry.setIsoCountryAlphaTwoCode("pata nai");
		Address counterPartyAdress=new Address();
		counterPartyAdress.setAddressCountry(addressCountry);
		PaymentInstrumentRiskFactor paymentInstructionRiskFactorReference=new PaymentInstrumentRiskFactor() ;
		paymentInstructionRiskFactorReference.setCounterPartyAddress(counterPartyAdress);
		deliveryAddress.setAddressLine(addressLine);
		deliveryAddress.setStreetName("abc");
		deliveryAddress.setBuildingNumber("1233");
		deliveryAddress.setPostCode("3111455");
		deliveryAddress.setTownName("pune");
		deliveryAddress.setCountrySubDivision("ABCD");
		deliveryAddress.setCountry("India");
		
		risk.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);
		risk.setMerchantCategoryCode("pata nai");
		risk.setMerchantCustomerIdentification("pata nai");
		risk.setDeliveryAddress(deliveryAddress);
		authorisation.setAuthorisationType(OBExternalAuthorisation1Code.ANY);
		authorisation.completionDateTime(""+new Date());
		remittanceInformation.setUnstructured("pata nai");
		remittanceInformation.setReference("pata nai");
		
		addressLine.add("abc");
		addressLine.add("xyz");
		creditorPostalAddress.setAddressType(com.capgemini.psd2.pisp.domain.OBAddressTypeCode.POSTAL);
		creditorPostalAddress.setDepartment("pata nai");
		creditorPostalAddress.setSubDepartment("pata nai");
		creditorPostalAddress.setStreetName("pata nai");
		creditorPostalAddress.setBuildingNumber("pata nai");
		creditorPostalAddress.setPostCode("pata nai");
		creditorPostalAddress.setTownName("pata nai");
		creditorPostalAddress.setCountrySubDivision("pata nai");
		creditorPostalAddress.setCountry("India");
		creditorPostalAddress.setAddressLine(addressLine);
		debtorAccount.setName("pata nai");
		debtorAccount.setIdentification("pata nai");
		debtorAccount.setName("pata nai");
		debtorAccount.setSecondaryIdentification("pata nai");
		creditorAccount.setSchemeName("pata nai");
		creditorAccount.setIdentification("pata nai");
		creditorAccount.setName("pata nai");
		creditorAccount.setSecondaryIdentification("pata nai");
		amount.setAmount("29.0");
		amount.setCurrency("NZD");

		obDomestic1.setInstructionIdentification("pata nai");
		obDomestic1.setEndToEndIdentification("pata nai");
		obDomestic1.setLocalInstrument("pata nai");
		obDomestic1.setInstructedAmount(amount);
		obDomestic1.setDebtorAccount(debtorAccount);
		obDomestic1.setCreditorAccount(creditorAccount);
		obDomestic1.setCreditorPostalAddress(creditorPostalAddress);
		obDomestic1.setRemittanceInformation(remittanceInformation);
		
		obWriteDataDomesticConsent1.setInitiation(obDomestic1);
		obWriteDataDomesticConsent1.setAuthorisation(authorisation);
		
		paymentConsentsRequest.setData(obWriteDataDomesticConsent1);
		paymentConsentsRequest.setRisk(risk);
		
		paymentInstructionProposal.setInstructionReference("pata nai");
		paymentInstructionProposal.setInstructionEndToEndReference("pata nai");
		paymentInstructionProposal.setInstructionLocalInstrument("pata nai");
		paymentInstructionProposal.setAuthorisingPartyUnstructuredReference("pata nai");
		paymentInstructionProposal.setAuthorisingPartyReference("pata nai");
		paymentInstructionProposal.setAuthorisationType(AuthorisationType.Any);
		paymentInstructionProposal.setAuthorisationDatetime("16/06/2019");
		paymentInstructionProposal.setPaymentInstructionRiskFactorReference(paymentInstructionRiskFactorReference);
		Party2 proposingParty=new Party2();
		paymentInstructionProposal.setProposingParty(proposingParty);
		paymentInstructionProposal.setProposingPartyPostalAddress(proposingPartyPostalAddress);
		paymentInstructionProposal.setProposingPartyAccount(proposingPartyAccount);
		Party2 authorisingParty=new Party2();
		paymentInstructionProposal.setAuthorisingParty(authorisingParty);
		paymentInstructionProposal.setAuthorisingPartyAccount(authorisingPartyAccount);
		paymentInstructionProposal.setTransactionCurrency(transactionCurrency);
		paymentInstructionProposal.setFinancialEventAmount(financialEventAmount);
		obDomestic1.setEndToEndIdentification(null);
		obDomestic1.setInstructionIdentification(null);
		obDomestic1.setLocalInstrument(null);
		obDomestic1.setInstructedAmount(null);
		obDomestic1.setDebtorAccount(null);
		debtorAccount.setSchemeName(null);
		debtorAccount.setIdentification(null);
		debtorAccount.setName(null);
		debtorAccount.setSecondaryIdentification(null);
		obDomestic1.setCreditorAccount(null);
		creditorAccount.setIdentification(null);
		creditorAccount.setName(null);
		creditorAccount.setSchemeName(null);
		creditorAccount.setSecondaryIdentification(null);
		obDomestic1.setCreditorPostalAddress(null);
		creditorPostalAddress.setAddressLine(null);
		creditorPostalAddress.setAddressType(null);
		creditorPostalAddress.setBuildingNumber(null);
		creditorPostalAddress.setCountry(null);
		obDomestic1.setRemittanceInformation(null);
		obWriteDataDomesticConsent1.setAuthorisation(null);
		deliveryAddress.setAddressLine(null);
		deliveryAddress.setStreetName(null);
		deliveryAddress.setBuildingNumber(null);
		deliveryAddress.setTownName(null);
		deliveryAddress.setPostCode(null);
		deliveryAddress.setCountry(null);
		deliveryAddress.setCountrySubDivision(null);
		risk.setDeliveryAddress(null);
		amount.setAmount(null);
		amount.setCurrency(null);
		Map<String, String> params = new HashMap<String,String>();
		params.put("tenant_id", "BOIROI");
		params.put("X-BOI-CHANNEL", "channel");
		transformer.transformDomesticConsentResponseFromAPIToFDForInsert(paymentConsentsRequest, params);
	}

}
