package com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.test;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.DomesticPaymentConsentsFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.client.DomesticPaymentConsentsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.delegate.DomesticPaymentConsentsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.transformer.DomesticPaymentConsentsFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticPaymentConsentsFoundationServiceAdapterTest {
	
	
	@InjectMocks
	DomesticPaymentConsentsFoundationServiceAdapter adapter;
	@Mock
	DomesticPaymentConsentsFoundationServiceDelegate delegate;
	@Mock
	DomesticPaymentConsentsFoundationServiceClient client;
	@Mock
	DomesticPaymentConsentsFoundationServiceTransformer transformer;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testupdateStagedDomesticPaymentConsents(){
		CustomPaymentStageIdentifiers customStageIdentifiers= new CustomPaymentStageIdentifiers();
		CustomPaymentStageUpdateData stageUpdateData= new CustomPaymentStageUpdateData();
		Map<String, String> params= new HashMap<>();
		adapter.updateStagedDomesticPaymentConsents(customStageIdentifiers, stageUpdateData, params);
	}
	
	@Test
	public void testretrieveStagedDomesticPaymentConsents(){
		CustomPaymentStageIdentifiers customStageIdentifiers= new CustomPaymentStageIdentifiers();
		Map<String, String> params= new HashMap<>();
		customStageIdentifiers.setPaymentSetupVersion("2.0");
		customStageIdentifiers.setPaymentConsentId("1344");
		adapter.retrieveStagedDomesticPaymentConsents(customStageIdentifiers, params);
	}
	
	@Test
	public void testcreateStagingDomesticPaymentConsents(){
		CustomDPaymentConsentsPOSTRequest paymentConsentsRequest= new CustomDPaymentConsentsPOSTRequest();
		CustomPaymentStageIdentifiers customStageIdentifiers= new CustomPaymentStageIdentifiers();
		Map<String, String> params= new HashMap<>();
		adapter.createStagingDomesticPaymentConsents(paymentConsentsRequest, customStageIdentifiers, params);
	}
	
	@Test
	public void testProcessDomesticPaymentConsents (){
		CustomDPaymentConsentsPOSTRequest paymentConsentsRequest = new CustomDPaymentConsentsPOSTRequest();
		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentSetupVersion("2.0");
		Map<String, String> params= new HashMap<>();
		adapter.processDomesticPaymentConsents(paymentConsentsRequest, customStageIdentifiers, params, OBExternalConsentStatus1Code.AUTHORISED, OBExternalConsentStatus1Code.AWAITINGAUTHORISATION);
	}
}
