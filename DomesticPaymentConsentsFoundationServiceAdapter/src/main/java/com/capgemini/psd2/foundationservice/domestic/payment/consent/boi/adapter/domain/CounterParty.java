/*
 * Payment Process API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * CounterParty
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-06-12T15:45:10.334+05:30")
public class CounterParty {
  @SerializedName("counterPartyReferenceText")
  private String counterPartyReferenceText = null;

  @SerializedName("previousCounterPartyIndicator")
  private Boolean previousCounterPartyIndicator = null;

  @SerializedName("name")
  private String name = null;

  @SerializedName("country")
  private Country country = null;

  public CounterParty counterPartyReferenceText(String counterPartyReferenceText) {
    this.counterPartyReferenceText = counterPartyReferenceText;
    return this;
  }

   /**
   * Get counterPartyReferenceText
   * @return counterPartyReferenceText
  **/
  @ApiModelProperty(value = "")
  public String getCounterPartyReferenceText() {
    return counterPartyReferenceText;
  }

  public void setCounterPartyReferenceText(String counterPartyReferenceText) {
    this.counterPartyReferenceText = counterPartyReferenceText;
  }

  public CounterParty previousCounterPartyIndicator(Boolean previousCounterPartyIndicator) {
    this.previousCounterPartyIndicator = previousCounterPartyIndicator;
    return this;
  }

   /**
   * Indicates whether (Y) or not (N) a Transaction has been successfully made previously with this Counterparty and hence a certain level of verification has been achieved
   * @return previousCounterPartyIndicator
  **/
  @ApiModelProperty(required = true, value = "Indicates whether (Y) or not (N) a Transaction has been successfully made previously with this Counterparty and hence a certain level of verification has been achieved")
  public Boolean isPreviousCounterPartyIndicator() {
    return previousCounterPartyIndicator;
  }

  public void setPreviousCounterPartyIndicator(Boolean previousCounterPartyIndicator) {
    this.previousCounterPartyIndicator = previousCounterPartyIndicator;
  }

  public CounterParty name(String name) {
    this.name = name;
    return this;
  }

   /**
   * A label identifying the Counterparty
   * @return name
  **/
  @ApiModelProperty(required = true, value = "A label identifying the Counterparty")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public CounterParty country(Country country) {
    this.country = country;
    return this;
  }

   /**
   * Get country
   * @return country
  **/
  @ApiModelProperty(required = true, value = "")
  public Country getCountry() {
    return country;
  }

  public void setCountry(Country country) {
    this.country = country;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CounterParty counterParty = (CounterParty) o;
    return Objects.equals(this.counterPartyReferenceText, counterParty.counterPartyReferenceText) &&
        Objects.equals(this.previousCounterPartyIndicator, counterParty.previousCounterPartyIndicator) &&
        Objects.equals(this.name, counterParty.name) &&
        Objects.equals(this.country, counterParty.country);
  }

  @Override
  public int hashCode() {
    return Objects.hash(counterPartyReferenceText, previousCounterPartyIndicator, name, country);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CounterParty {\n");
    
    sb.append("    counterPartyReferenceText: ").append(toIndentedString(counterPartyReferenceText)).append("\n");
    sb.append("    previousCounterPartyIndicator: ").append(toIndentedString(previousCounterPartyIndicator)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    country: ").append(toIndentedString(country)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

