package com.capgemini.psd2.pisp.file.payment.routing.adapter.routing;

import com.capgemini.psd2.pisp.adapter.FilePaymentConsentsAdapter;

@FunctionalInterface
public interface FPaymentConsentsAdapterFactory {

	public FilePaymentConsentsAdapter getFilePaymentSetupStagingInstance(String coreSystemName);
}
