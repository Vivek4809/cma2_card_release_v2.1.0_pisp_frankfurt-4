package com.capgemini.psd2.pisp.file.payment.setup.mongo.db.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;

public interface FPaymentConsentsDataRepository extends MongoRepository<CustomFilePaymentConsentsPOSTResponse, String>{

	 public CustomFilePaymentConsentsPOSTResponse findOneByDataConsentId(String consentId );	
}
