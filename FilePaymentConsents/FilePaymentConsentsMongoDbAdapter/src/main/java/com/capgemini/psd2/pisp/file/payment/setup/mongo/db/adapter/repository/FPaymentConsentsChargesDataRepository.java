package com.capgemini.psd2.pisp.file.payment.setup.mongo.db.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.pisp.domain.OBCharge1;

public interface FPaymentConsentsChargesDataRepository extends MongoRepository<OBCharge1,String> {
	
}
