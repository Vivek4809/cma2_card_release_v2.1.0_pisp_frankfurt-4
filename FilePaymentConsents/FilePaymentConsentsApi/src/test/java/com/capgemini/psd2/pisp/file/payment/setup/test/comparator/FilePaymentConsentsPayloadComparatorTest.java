package com.capgemini.psd2.pisp.file.payment.setup.test.comparator;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBExternalAuthorisation1Code;
import com.capgemini.psd2.pisp.domain.OBFile1;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBWriteDataFileConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataFileConsentResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteFileConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.file.payment.setup.comparator.FilePaymentConsentsPayloadComparator;


@RunWith(SpringJUnit4ClassRunner.class)
public class FilePaymentConsentsPayloadComparatorTest {

	private MockMvc mockMvc;
	
	@InjectMocks
	private FilePaymentConsentsPayloadComparator comparator;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(comparator).dispatchOptions(true).build();
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testcompare() throws Exception {
		OBWriteFileConsentResponse1 response1 = new OBWriteFileConsentResponse1();
		OBWriteFileConsentResponse1 response2 = new OBWriteFileConsentResponse1();
		OBWriteDataFileConsentResponse1 data = new OBWriteDataFileConsentResponse1();
		OBWriteDataFileConsentResponse1 data1 = new OBWriteDataFileConsentResponse1();
		OBFile1 initiation = new OBFile1();
		OBFile1 initiation1 = new OBFile1();
		BigDecimal controlSum=new BigDecimal("7.00");
		initiation.setControlSum(controlSum);
		initiation1.setControlSum(controlSum);
		OBAuthorisation1 authorisation=new OBAuthorisation1();
		
		data.setConsentId("1234");
		data.setAuthorisation(authorisation);
		data.setInitiation(initiation);
		initiation.setLocalInstrument("111");
		
		initiation1.setLocalInstrument("111");
		data1.setConsentId("1234");
		data1.setAuthorisation(authorisation);
		data1.setInitiation(initiation1);
		
		response1.setData(data);
		response2.setData(data1);
		
		comparator.compare(response1, response2);
	}

	@Test
	public void testcompareAuth1() throws Exception {
		OBWriteFileConsentResponse1 response1 = new OBWriteFileConsentResponse1();
		OBWriteFileConsentResponse1 response2 = new OBWriteFileConsentResponse1();
		OBWriteDataFileConsentResponse1 data = new OBWriteDataFileConsentResponse1();
		OBWriteDataFileConsentResponse1 data1 = new OBWriteDataFileConsentResponse1();
		OBAuthorisation1 authorisation1=new OBAuthorisation1();
		OBAuthorisation1 authorisation2=new OBAuthorisation1();
		OBFile1 initiation1 = new OBFile1();
		OBFile1 initiation = new OBFile1();
		BigDecimal controlSum=new BigDecimal("7.00");
		
		initiation.setControlSum(controlSum);
		initiation1.setControlSum(controlSum);
		authorisation1.setAuthorisationType(OBExternalAuthorisation1Code.SINGLE);
		data.setAuthorisation(authorisation1);
		initiation.setLocalInstrument("111");
		data.setConsentId("1234");
		data1.setConsentId("1234");
		data1.setInitiation(initiation1);
		data1.setAuthorisation(authorisation2);
		initiation1.setLocalInstrument("111");
		data.setInitiation(initiation);
		response1.setData(data);
		response2.setData(data1);
		comparator.compare(response1, response2);
	}
	
	@Test
	public void testComparePaymentDetails() throws Exception {
		CustomFilePaymentConsentsPOSTResponse response = new CustomFilePaymentConsentsPOSTResponse();
		CustomFilePaymentSetupPOSTRequest request = new CustomFilePaymentSetupPOSTRequest();
		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		CustomFilePaymentConsentsPOSTResponse adaptedPaymentConsentsResponse=new CustomFilePaymentConsentsPOSTResponse();
		OBRemittanceInformation1 remittanceInformation=new OBRemittanceInformation1();
		
		OBWriteDataFileConsentResponse1 data = new OBWriteDataFileConsentResponse1();
		OBCashAccountDebtor3 debtorAccount=new OBCashAccountDebtor3();
		OBFile1 initiation = new OBFile1();
		data.setConsentId("1234");
		initiation.setRemittanceInformation(remittanceInformation);
		debtorAccount.setName("UK.OBIE.IBAN");
		initiation.setDebtorAccount(debtorAccount);
		data.setInitiation(initiation);
		response.setData(data);
		
		OBWriteDataFileConsent1 dataSetup=new OBWriteDataFileConsent1();
		dataSetup.setInitiation(initiation);
		request.setData(dataSetup);
		adaptedPaymentConsentsResponse.setData(data);
		paymentSetupPlatformResource.setTppDebtorDetails("true");
		paymentSetupPlatformResource.setTppDebtorNameDetails("false");
		
		comparator.compareFilePaymentDetails(response, request, paymentSetupPlatformResource);
	}
	
	@Test
	public void testComparePaymentDetailsAuth1() throws Exception {
		CustomFilePaymentConsentsPOSTResponse response = new CustomFilePaymentConsentsPOSTResponse();
		CustomFilePaymentSetupPOSTRequest request = new CustomFilePaymentSetupPOSTRequest();
		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		CustomFilePaymentConsentsPOSTResponse adaptedPaymentConsentsResponse=new CustomFilePaymentConsentsPOSTResponse();
		OBRemittanceInformation1 remittanceInformation=new OBRemittanceInformation1();
		OBAuthorisation1 authorisation=new OBAuthorisation1();
		OBWriteDataFileConsentResponse1 data = new OBWriteDataFileConsentResponse1();
		OBCashAccountDebtor3 debtorAccount=new OBCashAccountDebtor3();
		OBWriteDataFileConsent1 dataSetup=new OBWriteDataFileConsent1();
		OBFile1 initiation = new OBFile1();
		
		data.setConsentId("1234");
		initiation.setRemittanceInformation(remittanceInformation);
		debtorAccount.setName("UK.OBIE.IBAN");
		initiation.setDebtorAccount(debtorAccount);
		data.setInitiation(initiation);
		data.setAuthorisation(authorisation);
		response.setData(data);
		
		
		dataSetup.setInitiation(initiation);
		request.setData(dataSetup);
		adaptedPaymentConsentsResponse.setData(data);
		paymentSetupPlatformResource.setTppDebtorDetails("true");
		paymentSetupPlatformResource.setTppDebtorNameDetails("false");
		
		comparator.compareFilePaymentDetails(response, request, paymentSetupPlatformResource);
	}
	
	@Test
	public void testComparePaymentDetailsAuthorisation() throws Exception {
		CustomFilePaymentConsentsPOSTResponse response = new CustomFilePaymentConsentsPOSTResponse();
		CustomFilePaymentSetupPOSTRequest request = new CustomFilePaymentSetupPOSTRequest();
		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		CustomFilePaymentConsentsPOSTResponse adaptedPaymentConsentsResponse=new CustomFilePaymentConsentsPOSTResponse();
		OBRemittanceInformation1 remittanceInformation=new OBRemittanceInformation1();
		
		OBWriteDataFileConsentResponse1 data = new OBWriteDataFileConsentResponse1();
		OBCashAccountDebtor3 debtorAccount=new OBCashAccountDebtor3();
		OBFile1 initiation = new OBFile1();
		OBAuthorisation1 authorisation=new OBAuthorisation1();
		
		authorisation.setAuthorisationType(OBExternalAuthorisation1Code.ANY);
		data.setConsentId("1234");
		data.setAuthorisation(authorisation);
		initiation.setRemittanceInformation(remittanceInformation);
		debtorAccount.setName("UK.OBIE.IBAN");
		initiation.setDebtorAccount(debtorAccount);
		data.setInitiation(initiation);
		response.setData(data);
		OBWriteDataFileConsent1 dataSetup=new OBWriteDataFileConsent1();
		dataSetup.setInitiation(initiation);
		request.setData(dataSetup);
		adaptedPaymentConsentsResponse.setData(data);
		paymentSetupPlatformResource.setTppDebtorDetails("true");
		paymentSetupPlatformResource.setTppDebtorNameDetails("false");
		
		comparator.compareFilePaymentDetails(response, request, paymentSetupPlatformResource);
	}
	
	@Test
	public void testComparePaymentDetails2() throws Exception {
		CustomFilePaymentConsentsPOSTResponse response = new CustomFilePaymentConsentsPOSTResponse();
		CustomFilePaymentSetupPOSTRequest request = new CustomFilePaymentSetupPOSTRequest();
		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		CustomFilePaymentConsentsPOSTResponse adaptedPaymentConsentsResponse=new CustomFilePaymentConsentsPOSTResponse();
		OBRemittanceInformation1 remittanceInformation=new OBRemittanceInformation1();
		
		OBWriteDataFileConsentResponse1 data = new OBWriteDataFileConsentResponse1();
		OBCashAccountDebtor3 debtorAccount=new OBCashAccountDebtor3();
		OBFile1 initiation = new OBFile1();
		data.setConsentId("1234");
		initiation.setRemittanceInformation(remittanceInformation);
		debtorAccount.setName("UK.OBIE.IBAN");
		initiation.setDebtorAccount(debtorAccount);
		data.setInitiation(initiation);
		response.setData(data);
		
		OBWriteDataFileConsent1 dataSetup=new OBWriteDataFileConsent1();
		dataSetup.setInitiation(initiation);
		request.setData(dataSetup);
		adaptedPaymentConsentsResponse.setData(data);
		paymentSetupPlatformResource.setTppDebtorDetails("false");
		paymentSetupPlatformResource.setTppDebtorNameDetails("false");
		
		comparator.compareFilePaymentDetails(response, request, paymentSetupPlatformResource);
	}
	
	@Test
	public void testComparePaymentDetailsAuth() throws Exception {
		CustomFilePaymentConsentsPOSTResponse response = new CustomFilePaymentConsentsPOSTResponse();
		CustomFilePaymentSetupPOSTRequest request = new CustomFilePaymentSetupPOSTRequest();
		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		CustomFilePaymentConsentsPOSTResponse adaptedPaymentConsentsResponse=new CustomFilePaymentConsentsPOSTResponse();
		OBRemittanceInformation1 remittanceInformation=new OBRemittanceInformation1();
		
		OBWriteDataFileConsentResponse1 data = new OBWriteDataFileConsentResponse1();
		OBCashAccountDebtor3 debtorAccount=new OBCashAccountDebtor3();
		OBFile1 initiation = new OBFile1();
		data.setConsentId("1234");
		initiation.setRemittanceInformation(remittanceInformation);
		debtorAccount.setName("UK.OBIE.IBAN");
		data.setInitiation(initiation);
		response.setData(data);
		
		OBWriteDataFileConsent1 dataSetup=new OBWriteDataFileConsent1();
		dataSetup.setInitiation(initiation);
		OBAuthorisation1 authorisation=new OBAuthorisation1();
		dataSetup.setAuthorisation(authorisation);
		request.setData(dataSetup);
		adaptedPaymentConsentsResponse.setData(data);
		paymentSetupPlatformResource.setTppDebtorDetails("false");
		paymentSetupPlatformResource.setTppDebtorNameDetails("false");
		
		comparator.compareFilePaymentDetails(response, request, paymentSetupPlatformResource);
	}
	
	@Test
	public void testComparePaymentDetails4() throws Exception {
		CustomFilePaymentConsentsPOSTResponse response = new CustomFilePaymentConsentsPOSTResponse();
		CustomFilePaymentSetupPOSTRequest request = new CustomFilePaymentSetupPOSTRequest();
		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		CustomFilePaymentConsentsPOSTResponse adaptedPaymentConsentsResponse=new CustomFilePaymentConsentsPOSTResponse();
		OBRemittanceInformation1 remittanceInformation=new OBRemittanceInformation1();
		OBWriteDataFileConsent1 dataSetup=new OBWriteDataFileConsent1();
		OBWriteDataFileConsentResponse1 data = new OBWriteDataFileConsentResponse1();
		OBCashAccountDebtor3 debtorAccount=new OBCashAccountDebtor3();
		OBFile1 initiation = new OBFile1();
		OBFile1 initiationReq=new OBFile1();
		BigDecimal controlSum=new BigDecimal("7.00");
		
		initiation.setControlSum(controlSum);
		initiationReq.setControlSum(controlSum);
		data.setConsentId("1234");
		initiation.setRemittanceInformation(remittanceInformation);
		debtorAccount.setName("UK.OBIE.IBAN");
		initiation.setDebtorAccount(debtorAccount);
		dataSetup.setInitiation(initiationReq);
		request.setData(dataSetup);
		data.setInitiation(initiation);
		response.setData(data);
		adaptedPaymentConsentsResponse.setData(data);
		paymentSetupPlatformResource.setTppDebtorDetails("false");
		paymentSetupPlatformResource.setTppDebtorNameDetails("false");
		
		comparator.compareFilePaymentDetails(response, request, paymentSetupPlatformResource);
	}
	
	@Test
	public void testComparePaymentDetails5() throws Exception {
		CustomFilePaymentConsentsPOSTResponse response = new CustomFilePaymentConsentsPOSTResponse();
		CustomFilePaymentSetupPOSTRequest request = new CustomFilePaymentSetupPOSTRequest();
		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		CustomFilePaymentConsentsPOSTResponse adaptedPaymentConsentsResponse=new CustomFilePaymentConsentsPOSTResponse();
		OBRemittanceInformation1 remittanceInformation=new OBRemittanceInformation1();
		OBWriteDataFileConsentResponse1 data = new OBWriteDataFileConsentResponse1();
		OBCashAccountDebtor3 debtorAccount=new OBCashAccountDebtor3();
		OBFile1 initiation = new OBFile1();
		BigDecimal controlSum=new BigDecimal("7.00");
		
		initiation.setControlSum(controlSum);
		data.setConsentId("1234");
		initiation.setRemittanceInformation(remittanceInformation);
		debtorAccount.setName("UK.OBIE.IBAN");
		data.setInitiation(initiation);
		response.setData(data);
		
		OBWriteDataFileConsent1 dataSetup=new OBWriteDataFileConsent1();
		dataSetup.setInitiation(initiation);
		request.setData(dataSetup);
		adaptedPaymentConsentsResponse.setData(data);
		paymentSetupPlatformResource.setTppDebtorDetails("false");
		paymentSetupPlatformResource.setTppDebtorNameDetails("false");
		
		comparator.compareFilePaymentDetails(response, request, paymentSetupPlatformResource);
	}
	
	@Test
	public void testComparePaymentDetails6() throws Exception {
		CustomFilePaymentConsentsPOSTResponse response = new CustomFilePaymentConsentsPOSTResponse();
		CustomFilePaymentSetupPOSTRequest request = new CustomFilePaymentSetupPOSTRequest();
		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		CustomFilePaymentConsentsPOSTResponse adaptedPaymentConsentsResponse=new CustomFilePaymentConsentsPOSTResponse();
		OBRemittanceInformation1 remittanceInformation=new OBRemittanceInformation1();
		OBAuthorisation1 authorisation=new OBAuthorisation1();
		OBWriteDataFileConsentResponse1 data = new OBWriteDataFileConsentResponse1();
		OBCashAccountDebtor3 debtorAccount=new OBCashAccountDebtor3();
		OBWriteDataFileConsent1 dataSetup=new OBWriteDataFileConsent1();
		OBFile1 initiation = new OBFile1();
		
		data.setConsentId("1234");
		initiation.setRemittanceInformation(remittanceInformation);
		debtorAccount.setName("UK.OBIE.IBAN");
		data.setInitiation(initiation);
		
		dataSetup.setInitiation(initiation);
		data.setAuthorisation(authorisation);
		
		request.setData(dataSetup);
		response.setData(data);
		adaptedPaymentConsentsResponse.setData(data);
		paymentSetupPlatformResource.setTppDebtorDetails("false");
		paymentSetupPlatformResource.setTppDebtorNameDetails("false");
		
		comparator.compareFilePaymentDetails(response, request, paymentSetupPlatformResource);
	}
	
}
