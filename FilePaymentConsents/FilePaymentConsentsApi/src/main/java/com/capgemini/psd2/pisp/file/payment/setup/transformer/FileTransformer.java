package com.capgemini.psd2.pisp.file.payment.setup.transformer;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class FileTransformer {

	public boolean fileTypeChecker(MultipartFile file, String fileType) {

		if ("application/xml".equals(file.getContentType()) && ("UK.OBIE.pain.001.001.08").equals(fileType)
				|| ("application/json").equals(file.getContentType())
						&& ("UK.OBIE.PaymentInitiation.3.0").equals(fileType))
			return true;
		else
			return false;
	}

}
