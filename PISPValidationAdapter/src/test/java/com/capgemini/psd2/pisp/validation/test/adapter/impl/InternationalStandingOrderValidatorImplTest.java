package com.capgemini.psd2.pisp.validation.test.adapter.impl;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBBranchAndFinancialInstitutionIdentification3;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBInternationalStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalStandingOrderConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalStandingOrderConsentResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalStandingOrderResponse1;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.validation.adapter.constants.CommonPaymentValidations;
import com.capgemini.psd2.pisp.validation.adapter.impl.InternationalStandingOrderValidatorImpl;
import com.capgemini.psd2.validator.PSD2Validator;


public class InternationalStandingOrderValidatorImplTest {

	
	@InjectMocks
	private InternationalStandingOrderValidatorImpl internationalStandingOrderValidatorImpl;
	
	@Mock
	private CustomIStandingOrderConsentsPOSTRequest customIStandingOrderConsentsPOSTRequest;
	
	@Mock
	private CommonPaymentValidations commonPaymentValidations;
	
	@Mock
	private PSD2Validator psd2Validator;
	
	@Mock
	private PaymentRetrieveGetRequest paymentRetrieveGetRequest;
	
	@Mock
	private CustomIStandingOrderConsentsPOSTResponse customIStandingOrderConsentsPOSTResponse;
	
	@Mock
	private CustomIStandingOrderPOSTRequest request;
	
	@Mock
	private CustomIStandingOrderPOSTResponse response;
	
	@Before
	public void setUp() {
		
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void validateConsentRequestTest(){
		ReflectionTestUtils.setField(internationalStandingOrderValidatorImpl, "reqValidationEnabled", Boolean.TRUE);
		CustomIStandingOrderConsentsPOSTRequest customIStandingOrderConsentsPOSTRequest = new  CustomIStandingOrderConsentsPOSTRequest();
		OBRisk1 risk = new OBRisk1();
		
		risk.setMerchantCustomerIdentification("identification");
		OBRisk1DeliveryAddress address = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(address );
		customIStandingOrderConsentsPOSTRequest.setRisk(risk );
		OBWriteDataInternationalStandingOrderConsent1 data = new OBWriteDataInternationalStandingOrderConsent1();
		
		OBAuthorisation1 authorisation = new OBAuthorisation1();
		authorisation.setCompletionDateTime("2007-12-12T10:15:30+01:00");
		data.setAuthorisation(authorisation);
		OBInternationalStandingOrder1 initiation = new OBInternationalStandingOrder1();
		initiation.setFirstPaymentDateTime(OffsetDateTime.now().plusDays(1).toString());
		initiation.setFinalPaymentDateTime(OffsetDateTime.now().plusDays(2).toString());
		
		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("IBAN");
		creditorAgent.setIdentification("14sjokdl89sbbn");
		initiation.setCreditorAgent(creditorAgent);
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSchemeName("IBAN");
		creditorAccount.setIdentification("14sjokdl89sbbn");
		initiation.setCreditorAccount(creditorAccount );
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("IBAN");
		debtorAccount.setIdentification("14sjokdl89sbbn");
		debtorAccount.setSecondaryIdentification("003");
		initiation.setDebtorAccount(debtorAccount );
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setCurrency("INR");
		initiation.setInstructedAmount(instructedAmount );
		data.setInitiation(initiation );
		customIStandingOrderConsentsPOSTRequest.setData(data);
		internationalStandingOrderValidatorImpl.validateConsentRequest(customIStandingOrderConsentsPOSTRequest);
	}
	
	@Test
	public void validateConsentsGETRequestTest(){
		internationalStandingOrderValidatorImpl.validateConsentsGETRequest(paymentRetrieveGetRequest);
	}
	
	@Test
	public void validatePaymentConsentResponseTest(){
		ReflectionTestUtils.setField(internationalStandingOrderValidatorImpl, "resValidationEnabled", Boolean.TRUE);
		CustomIStandingOrderConsentsPOSTResponse customIStandingOrderConsentsPOSTResponse = new CustomIStandingOrderConsentsPOSTResponse();
		OBRisk1 risk = new OBRisk1();
		risk.setMerchantCategoryCode("CODE");
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress );
		customIStandingOrderConsentsPOSTResponse.setRisk(risk);
		OBWriteDataInternationalStandingOrderConsentResponse1 data = new OBWriteDataInternationalStandingOrderConsentResponse1();
		OBInternationalStandingOrder1 initiation = new OBInternationalStandingOrder1();
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("IBAN");
		debtorAccount.setIdentification("hrfgygryfgg78894");
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSchemeName("IBAN");
		creditorAccount.setIdentification("hrfgygryfgg78894");
		initiation.setCreditorAccount(creditorAccount );
		List<OBCharge1> charges = new ArrayList<>();
		data.setCharges(charges);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setCurrency("INR");
		initiation.setInstructedAmount(instructedAmount );
		
		initiation.setDebtorAccount(debtorAccount);
		data.setInitiation(initiation);
		customIStandingOrderConsentsPOSTResponse.setData(data);
		OBAuthorisation1 authorisation = new OBAuthorisation1();
		data.setAuthorisation(authorisation);
		authorisation.setCompletionDateTime("2019-05-03T10:15:30+01:00");
		initiation.setFirstPaymentDateTime("2019-05-03T10:15:30+01:00");
		initiation.setFinalPaymentDateTime("2019-05-09T10:15:30+01:00");
	    data.setCutOffDateTime("2019-05-09T10:15:30+01:00");
		
		internationalStandingOrderValidatorImpl.validatePaymentConsentResponse(customIStandingOrderConsentsPOSTResponse);
	}
	
	@Test
	public void validatePaymentsPOSTRequestTest(){
		ReflectionTestUtils.setField(internationalStandingOrderValidatorImpl, "paymentIdRegexValidator", "123456");
		CustomIStandingOrderPOSTRequest request  = new CustomIStandingOrderPOSTRequest();
		OBWriteDataInternationalStandingOrder1 data= new OBWriteDataInternationalStandingOrder1();
		data.setConsentId("123456");
		request.setData(data);
		internationalStandingOrderValidatorImpl.validatePaymentsPOSTRequest(request);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validatePaymentsResponseTest(){
		ReflectionTestUtils.setField(internationalStandingOrderValidatorImpl, "resValidationEnabled", Boolean.TRUE);
		ReflectionTestUtils.setField(internationalStandingOrderValidatorImpl, "paymentIdRegexValidator", "123456");
		CustomIStandingOrderPOSTResponse response = new CustomIStandingOrderPOSTResponse();
		OBWriteDataInternationalStandingOrderResponse1 data = new OBWriteDataInternationalStandingOrderResponse1();
		data.setConsentId("123456");
		data.setInternationalStandingOrderId("123456");
		OBInternationalStandingOrder1 initiation = new OBInternationalStandingOrder1();
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setIdentification("ehdedegd7788");
		debtorAccount.setSchemeName("IBAN");
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setIdentification("dhdggedgged78811");
		creditorAccount.setIdentification("IBAN");
		initiation.setCreditorAccount(creditorAccount );
		initiation.setDebtorAccount(debtorAccount );
		data.setInitiation(initiation);
		response.setData(data);
		internationalStandingOrderValidatorImpl.validatePaymentsResponse(response);
	}
}
