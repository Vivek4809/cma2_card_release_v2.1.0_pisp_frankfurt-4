package com.capgemini.psd2.pisp.validation.test.adapter.impl;

import static org.assertj.core.api.Assertions.in;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticScheduled1;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduled1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduledConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduledConsentResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduledResponse1;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.validation.adapter.constants.CommonPaymentValidations;
import com.capgemini.psd2.pisp.validation.adapter.impl.DomesticScheduledPaymentValidatorImpl;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticScheduledPaymentValidatorImplTest {

	@InjectMocks
	DomesticScheduledPaymentValidatorImpl domesticScheduledPaymentValidatorImpl;

	@Mock
	private PSD2Validator psd2Validator;

	@Mock
	private CommonPaymentValidations commonPaymentValidations;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Before
	public void initializeErrorMessages() {
		Map<String, String> genericErrorMessages = new HashMap<>();
		genericErrorMessages.put("INTERNAL", "Unexpeced error occured");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(genericErrorMessages);
	}

	@Test
	public void validateConsentsGETRequest() {
		doNothing().when(psd2Validator).validate(any(PaymentRetrieveGetRequest.class));
		assertTrue(domesticScheduledPaymentValidatorImpl.validateConsentsGETRequest(new PaymentRetrieveGetRequest()));
	}

	@Test
	public void validateConsentRequest() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = DomesticScheduledPaymentValidatorImpl.class.getDeclaredField("reqValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(domesticScheduledPaymentValidatorImpl, true);
		} catch (Exception e) {
		}
		doNothing().when(commonPaymentValidations).validateHeaders();
		assertTrue(domesticScheduledPaymentValidatorImpl.validateConsentRequest(getRequest()));
	}

	@Test
	public void validatePaymentConsentResponse() {
		Field resValidationEnabled = null;
		try {
			resValidationEnabled = DomesticScheduledPaymentValidatorImpl.class.getDeclaredField("resValidationEnabled");
			resValidationEnabled.setAccessible(true);
			resValidationEnabled.set(domesticScheduledPaymentValidatorImpl, true);
		} catch (Exception e) {
		}

		assertTrue(domesticScheduledPaymentValidatorImpl.validatePaymentConsentResponse(getResponse()));
	}

	@Test
	public void validatePaymentsPOSTRequest() {
		Field reqValidationEnabled = null;
		Field paymentIdRegexValidator=null;
		try {
			reqValidationEnabled = DomesticScheduledPaymentValidatorImpl.class.getDeclaredField("reqValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(domesticScheduledPaymentValidatorImpl, true);
			paymentIdRegexValidator= DomesticScheduledPaymentValidatorImpl.class.getDeclaredField("paymentIdRegexValidator");
			paymentIdRegexValidator.setAccessible(true);
			paymentIdRegexValidator.set(domesticScheduledPaymentValidatorImpl, "^[a-zA-Z0-9-_]{1,128}$");
		} catch (Exception e) {
		}
		doNothing().when(commonPaymentValidations).validateHeaders();
		domesticScheduledPaymentValidatorImpl.validatePaymentsPOSTRequest(getCustomDSPaymentsPOSTRequest());
	}

	private CustomDSPConsentsPOSTRequest getRequest() {
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setCurrency("currency");
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSchemeName("schemeName");
		creditorAccount.setIdentification("identification");
		creditorAccount.setSecondaryIdentification("secondaryIdentification");
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("schemeName");
		debtorAccount.setIdentification("identification");
		debtorAccount.setSecondaryIdentification("secondaryIdentification");
		OBPostalAddress6 creditorPostalAddress = new OBPostalAddress6();
		OBDomesticScheduled1 initiation = new OBDomesticScheduled1();
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		initiation.setInstructedAmount(instructedAmount);
		initiation.setCreditorAccount(creditorAccount);
		initiation.setDebtorAccount(debtorAccount);
		initiation.setCreditorPostalAddress(creditorPostalAddress);
		initiation.setLocalInstrument("localInstrument");
		initiation.setRemittanceInformation(remittanceInformation);
		OBAuthorisation1 authorisation = new OBAuthorisation1();
		authorisation.setCompletionDateTime("completionDateTime");
		OBWriteDataDomesticScheduledConsent1 data = new OBWriteDataDomesticScheduledConsent1();
		data.setInitiation(initiation);
		data.setAuthorisation(authorisation);
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		OBRisk1 risk = new OBRisk1();
		risk.setDeliveryAddress(deliveryAddress);
		CustomDSPConsentsPOSTRequest request = new CustomDSPConsentsPOSTRequest();
		request.setData(data);
		request.setRisk(risk);
		return request;
	}

	private CustomDSPConsentsPOSTResponse getResponse() {
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setCurrency("currency");
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSchemeName("schemeName");
		creditorAccount.setIdentification("identification");
		creditorAccount.setSecondaryIdentification("secondaryIdentification");
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("schemeName");
		debtorAccount.setIdentification("identification");
		debtorAccount.setSecondaryIdentification("secondaryIdentification");
		OBPostalAddress6 creditorPostalAddress = new OBPostalAddress6();
		OBDomesticScheduled1 initiation = new OBDomesticScheduled1();
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		initiation.setInstructedAmount(instructedAmount);
		initiation.setCreditorAccount(creditorAccount);
		initiation.setDebtorAccount(debtorAccount);
		initiation.setCreditorPostalAddress(creditorPostalAddress);
		initiation.setLocalInstrument("localInstrument");
		initiation.setRemittanceInformation(remittanceInformation);
		OBAuthorisation1 authorisation = new OBAuthorisation1();
		authorisation.setCompletionDateTime("completionDateTime");
		OBCharge1Amount amount = new OBCharge1Amount();
		amount.setCurrency("currency");
		OBCharge1 charge = new OBCharge1();
		charge.setAmount(amount);
		List<OBCharge1> charges = new ArrayList<>();
		charges.add(charge);
		OBWriteDataDomesticScheduledConsentResponse1 data = new OBWriteDataDomesticScheduledConsentResponse1();
		data.setInitiation(initiation);
		data.setAuthorisation(authorisation);
		data.setCharges(charges);
		data.setStatusUpdateDateTime("2017-04-05T10:43:07+00:00");
		data.setExpectedExecutionDateTime("2017-04-05T10:43:07+00:00");
		data.setExpectedSettlementDateTime("2017-04-05T10:43:07+00:00");
		data.setCutOffDateTime("2017-04-05T10:43:07+00:00");
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		OBRisk1 risk = new OBRisk1();
		risk.setDeliveryAddress(deliveryAddress);
		CustomDSPConsentsPOSTResponse response = new CustomDSPConsentsPOSTResponse();
		response.setData(data);
		response.setRisk(risk);
		return response;
	}

	public CustomDSPaymentsPOSTRequest getCustomDSPaymentsPOSTRequest() {
		CustomDSPaymentsPOSTRequest request = new CustomDSPaymentsPOSTRequest();
		OBWriteDataDomesticScheduled1 data = new OBWriteDataDomesticScheduled1();
		OBDomesticScheduled1 initiation = new OBDomesticScheduled1();
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setCurrency("currency");
		initiation.setInstructedAmount(instructedAmount);
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSchemeName("schemeName");
		creditorAccount.setIdentification("identification");
		creditorAccount.setSecondaryIdentification("secondaryIdentification");
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("schemeName");
		debtorAccount.setIdentification("identification");
		debtorAccount.setSecondaryIdentification("secondaryIdentification");
		initiation.setCreditorAccount(creditorAccount);
		initiation.setDebtorAccount(debtorAccount);
		OBPostalAddress6 creditorPostalAddress = new OBPostalAddress6();
		initiation.setCreditorPostalAddress(creditorPostalAddress);
		initiation.setLocalInstrument("localInstrument");
		initiation.setRemittanceInformation(remittanceInformation);
		OBAuthorisation1 authorisation = new OBAuthorisation1();
		authorisation.setCompletionDateTime("completionDateTime");
		OBCharge1Amount amount = new OBCharge1Amount();
		amount.setCurrency("currency");
		OBCharge1 charge = new OBCharge1();
		charge.setAmount(amount);
		List<OBCharge1> charges = new ArrayList<>();
		charges.add(charge);
		data.setInitiation(initiation);
		data.setConsentId("58ac46e3-113d-44c2-af37-5f0bbadf7141");
		data.setInitiation(initiation);
		request.setData(data);
		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress= new OBRisk1DeliveryAddress();
		deliveryAddress.setBuildingNumber("asdfasdf");
		risk.setDeliveryAddress(deliveryAddress);
		request.setRisk(risk);
		return request;
	}
	
	@Test
	public void validatePaymentsResponseTest(){
		ReflectionTestUtils.setField(domesticScheduledPaymentValidatorImpl, "resValidationEnabled", Boolean.TRUE);
		CustomDSPaymentsPOSTResponse paymentSubmissionResponse = new CustomDSPaymentsPOSTResponse();
		OBWriteDataDomesticScheduledResponse1 data = new OBWriteDataDomesticScheduledResponse1();
		OBDomesticScheduled1 initiation = new OBDomesticScheduled1();
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setCurrency("INR");
		initiation.setLocalInstrument("LocalInstrument");
		initiation.setInstructedAmount(instructedAmount );
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("IBAN");
		debtorAccount.setIdentification("12345gfhf78");
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setIdentification("31425627hjjjdfk");
		creditorAccount.setSchemeName("IBAN");
		initiation.setCreditorAccount(creditorAccount );
		initiation.setDebtorAccount(debtorAccount );
		data.setInitiation(initiation );
		List<OBCharge1> charges = new ArrayList<>();
		data.setCharges(charges );
		data.setStatusUpdateDateTime("2019-05-09T10:15:30+01:00");
		data.setExpectedExecutionDateTime("2019-05-17T10:15:30+01:00");
		data.setExpectedSettlementDateTime("2019-05-25T10:15:30+01:00");
		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		data.setMultiAuthorisation(multiAuthorisation );
		initiation.setRequestedExecutionDateTime("2019-05-29T10:15:30+01:00");
		paymentSubmissionResponse.setData(data);
		
		domesticScheduledPaymentValidatorImpl.validatePaymentsResponse(paymentSubmissionResponse );
	}

}
