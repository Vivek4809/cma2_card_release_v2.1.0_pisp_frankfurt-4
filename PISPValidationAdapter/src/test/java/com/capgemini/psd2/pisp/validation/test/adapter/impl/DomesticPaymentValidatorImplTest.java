package com.capgemini.psd2.pisp.validation.test.adapter.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.DPaymentsRetrieveGetRequest;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomestic1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticConsentResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteDomesticConsent1;
import com.capgemini.psd2.pisp.domain.PaymentDomesticSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.validation.adapter.constants.CommonPaymentValidations;
import com.capgemini.psd2.pisp.validation.adapter.impl.DomesticPaymentValidatorImpl;
import com.capgemini.psd2.validator.PSD2Validator;


@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticPaymentValidatorImplTest {

	/*
	 * Creating required objects
	 */

	List<String> addressLineList = new ArrayList<String>();
	List<String> countrySubDivisionList = new ArrayList<String>();
	OBWriteDataDomesticConsent1 data = new OBWriteDataDomesticConsent1();
	OBDomestic1 initiation = new OBDomestic1();
	OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
	OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
	OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
	OBWriteDomesticConsent1 paymentConsentsRequest = new OBWriteDomesticConsent1();
	OBRisk1 risk = new OBRisk1();
	OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
	OBPostalAddress6 creditorPostalAddress = new OBPostalAddress6();
	OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
	OBAuthorisation1 authorisation = new OBAuthorisation1();
	OBWriteDataDomestic1 paymentData = new OBWriteDataDomestic1();
	CustomDPaymentConsentsPOSTRequest req = new CustomDPaymentConsentsPOSTRequest();
	CustomDPaymentsPOSTRequest paymentSubmissionRequest = new CustomDPaymentsPOSTRequest();
	DPaymentsRetrieveGetRequest submissionRetrieveRequest = new DPaymentsRetrieveGetRequest();
	Field reqValidationEnabled = null;
	Field paymentIdRegexValidator = null;
	PaymentDomesticSubmitPOST201Response pResponse = new PaymentDomesticSubmitPOST201Response();
	OBWriteDataDomesticResponse1 domesticData = new OBWriteDataDomesticResponse1();
	CustomDPaymentConsentsPOSTResponse cResponse = new CustomDPaymentConsentsPOSTResponse();
	OBWriteDataDomesticConsentResponse1 domData = new OBWriteDataDomesticConsentResponse1();

	@InjectMocks
	private DomesticPaymentValidatorImpl domesticPaymentValidatorImpl;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		Map<String, String> genericErrorMessages = new HashMap<>();
		genericErrorMessages.put("FIELD", "freq ma");
		genericErrorMessages.put("INTERNAL", "freq ma");

		OBPSD2ExceptionUtility.genericErrorMessages.putAll(genericErrorMessages);

		try {
			reqValidationEnabled = DomesticPaymentValidatorImpl.class.getDeclaredField("reqValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(domesticPaymentValidatorImpl, true);
			paymentIdRegexValidator = DomesticPaymentValidatorImpl.class.getDeclaredField("paymentIdRegexValidator");
			paymentIdRegexValidator.setAccessible(true);
			paymentIdRegexValidator.set(domesticPaymentValidatorImpl, "[a-zA-Z]*");
		} catch (Exception e) {

		}
	}

	@Before
	public void setValues() {

		/*
		 * Creating required lists
		 */
		List<String> addressLineList = new ArrayList<>();
		addressLineList.add("fghfgh");
		addressLineList.add("bbfdff");

		String countrySubDivisionList = "hrgthrt";

		/*
		 * Setting data in the created objects
		 */

		creditorPostalAddress.setAddressLine(addressLineList);
		creditorPostalAddress.setBuildingNumber("dds");
		creditorPostalAddress.setCountry("IN");
		creditorPostalAddress.setCountrySubDivision("asd");
		creditorPostalAddress.setDepartment("dept");
		creditorPostalAddress.setPostCode("asda");
		creditorPostalAddress.setStreetName("hyufdbg");
		creditorPostalAddress.setSubDepartment("subDepartment");
		creditorPostalAddress.setTownName("townName");

		debtorAccount.setSchemeName("gdfgsdf");
		debtorAccount.setIdentification("hghghdfsd");
		debtorAccount.setName("ruiot ouo");
		debtorAccount.setSecondaryIdentification("klhjklhg");
		creditorAccount.setIdentification("asdasd");
		creditorAccount.setName("sdfjkghkf");
		creditorAccount.setSchemeName("ahgtrgegfdgsdasd");
		creditorAccount.setSecondaryIdentification("utyjkdfngui");
		instructedAmount.setCurrency("INR");
		remittanceInformation.setReference("asdasd");
		remittanceInformation.setUnstructured("unstructured");
		initiation.setLocalInstrument("ghdfgdfgdfg");
		initiation.setRemittanceInformation(remittanceInformation);
		initiation.setCreditorPostalAddress(creditorPostalAddress);
		initiation.setInstructedAmount(instructedAmount);
		initiation.setCreditorAccount(creditorAccount);
		initiation.setDebtorAccount(debtorAccount);
		authorisation.setCompletionDateTime("gdfgdfgdfgdf");
		data.setInitiation(initiation);
		authorisation.setCompletionDateTime("2019-05-03T10:15:30+01:00");
		data.setAuthorisation(authorisation);
		deliveryAddress.setBuildingNumber("asdfsdf");
		deliveryAddress.setStreetName("sdfsdf");
		deliveryAddress.setTownName("dfdfgsdfsdf");
		deliveryAddress.setAddressLine(addressLineList);
		deliveryAddress.setCountrySubDivision(countrySubDivisionList);
		deliveryAddress.setCountry("India");
		deliveryAddress.setPostCode("123453");
		risk.setDeliveryAddress(deliveryAddress);
		risk.setMerchantCategoryCode("B019");
		paymentConsentsRequest.setRisk(risk);
		paymentConsentsRequest.setData(data);
		paymentData.setConsentId("jkghghgh");
		paymentData.setInitiation(initiation);
		req.setData(data);
		req.setRisk(risk);
		
		domesticData.setInitiation(initiation);
		List<OBCharge1> charges1 = new ArrayList<>();
		domesticData.setCharges(charges1);
		domesticData.setStatusUpdateDateTime("2019-05-03T10:15:30+01:00");
		domesticData.setExpectedExecutionDateTime("2019-05-03T10:15:30+01:00");
		domesticData.setExpectedSettlementDateTime("2019-05-03T10:15:30+01:00");
		pResponse.setData(domesticData);
		paymentSubmissionRequest.setData(paymentData);
		paymentSubmissionRequest.setRisk(risk);
		domData.setConsentId("consent");
		domData.setInitiation(initiation);
		domData.setStatusUpdateDateTime("2019-05-03T10:15:30+01:00");
		domData.setExpectedExecutionDateTime("2019-05-08T10:15:30+01:00");
		domData.setExpectedSettlementDateTime("2019-05-09T10:15:30+01:00");
		domData.setCutOffDateTime("2019-05-11T10:15:30+01:00");
		List<OBCharge1> charges = new ArrayList<>();
		domData.setCharges(charges);
		cResponse.setData(domData);
		cResponse.setRisk(risk);
		

	}

	@Mock
	private CommonPaymentValidations commonPaymentValidations;

	@Mock
	private PSD2Validator psd2Validator;

	/*@Test
	public void validateConsentRequestTest() {

		doNothing().when(psd2Validator).validateRequestWithSwagger(req, "");
		doNothing().when(commonPaymentValidations).validateSchemeNameWithIdentification("asdas", "sddsd", null);
		doNothing().when(commonPaymentValidations).validateDomesticRiskDeliveryAddress(deliveryAddress, null);
		doNothing().when(commonPaymentValidations).validateDomesticCreditorPostalAddress(creditorPostalAddress, null);
		doNothing().when(commonPaymentValidations).validateLocalInstrument("ghkdfg", null);

		assertTrue(domesticPaymentValidatorImpl.validateConsentRequest(req));
	}*/

	@Test
	public void validatePaymentsPOSTRequestTest() {

		doNothing().when(commonPaymentValidations).validateDomesticPaymentContext(risk);
		doNothing().when(commonPaymentValidations).validateDomesticRiskDeliveryAddress(deliveryAddress, null);
		domesticPaymentValidatorImpl.validatePaymentsPOSTRequest(paymentSubmissionRequest);
		//verify(psd2Validator, times(1)).validate(any(CustomDPaymentsPOSTRequest.class));

	}

	@Test
	public void validateDomesticPaymentsGETRequestTest() {

		psd2Validator.validate(submissionRetrieveRequest);
		verify(psd2Validator, times(1)).validate(any(DPaymentsRetrieveGetRequest.class));
	}

	@Test
	public void validateConsentsGETRequest_valid() {
		assertTrue(domesticPaymentValidatorImpl.validateConsentsGETRequest(new PaymentRetrieveGetRequest()));
	}

	@Test
	public void validatePaymentConsentResponse_valid() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = DomesticPaymentValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(domesticPaymentValidatorImpl, true);
		} catch (Exception e) {
		}
		assertTrue(domesticPaymentValidatorImpl.validatePaymentConsentResponse(cResponse));
		// assertTrue(domesticPaymentValidatorImpl.validatePaymentConsentResponse(paymentConsentsResponse);
	}

	@Test
	public void validatePaymentsResponse_reqValidationEnabled() {
		Field resValidationEnabled = null;
		try {
			resValidationEnabled = DomesticPaymentValidatorImpl.class.getDeclaredField("resValidationEnabled");
			resValidationEnabled.setAccessible(true);
			resValidationEnabled.set(domesticPaymentValidatorImpl, true);
		} catch (Exception e) {
		}
		domesticPaymentValidatorImpl.validatePaymentsResponse(pResponse);
		verify(psd2Validator, times(1)).validate(any(PaymentDomesticSubmitPOST201Response.class));
	}

	@Test
	public void validatePaymentsResponse_reqValidationDisabled() {
		Field resValidationEnabled = null;
		try {
			resValidationEnabled = DomesticPaymentValidatorImpl.class.getDeclaredField("resValidationEnabled");
			resValidationEnabled.setAccessible(true);
			resValidationEnabled.set(domesticPaymentValidatorImpl, false);
		} catch (Exception e) {
		}
		domesticPaymentValidatorImpl.validatePaymentsResponse(pResponse);
		verify(psd2Validator, never()).validate(any(PaymentDomesticSubmitPOST201Response.class));
	}

	@Test
	public void validateConsentRequestTest(){
		ReflectionTestUtils.setField(domesticPaymentValidatorImpl, "reqValidationEnabled", Boolean.TRUE);
		OBWriteDomesticConsent1 consent1 = new OBWriteDomesticConsent1();
		consent1.setData(data);
		domesticPaymentValidatorImpl.validateConsentRequest(req);
	}
	
	@Test
	public void validateDomesticPaymentsGETRequest1Test(){
		domesticPaymentValidatorImpl.validateDomesticPaymentsGETRequest(submissionRetrieveRequest);
	}
	
	@Test(expected=PSD2Exception.class)
	public void validatePaymentsPOSTRequestExceptionTest(){
		OBWriteDataDomestic1 data1  = new OBWriteDataDomestic1();
		data1.setConsentId("12");
		paymentSubmissionRequest.setData(data1);
		domesticPaymentValidatorImpl.validatePaymentsPOSTRequest(paymentSubmissionRequest);
	}

}
