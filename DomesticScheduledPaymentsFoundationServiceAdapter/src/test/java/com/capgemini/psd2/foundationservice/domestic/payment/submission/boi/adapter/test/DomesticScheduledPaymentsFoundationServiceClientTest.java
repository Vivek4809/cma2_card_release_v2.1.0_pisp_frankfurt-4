package com.capgemini.psd2.foundationservice.domestic.payment.submission.boi.adapter.test;

import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.client.DomesticScheduledPaymentSubmissionFoundationServiceClient;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.ScheduledPaymentInstructionComposite;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticScheduledPaymentsFoundationServiceClientTest {
	
	@InjectMocks
	DomesticScheduledPaymentSubmissionFoundationServiceClient client;
	
	@Mock
	RestClientSync restClient;
	
	@Mock
	private RestTemplate restTemplate;
	
	@Before
	public void setUp() throws Exception{
		MockitoAnnotations.initMocks(this);
		Method postConstruct= DomesticScheduledPaymentSubmissionFoundationServiceClient.class.getDeclaredMethod("init", null);
		postConstruct.setAccessible(true);
		postConstruct.invoke(client);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testrestTransportForDomesticPaymentFoundationService()
	{
		RequestInfo reqInfo = new RequestInfo();
		HttpHeaders headers=new HttpHeaders();
		ScheduledPaymentInstructionComposite payment= new ScheduledPaymentInstructionComposite();
		payment=client.restTransportForDomesticScheduledPaymentFoundationService(reqInfo, ScheduledPaymentInstructionComposite.class, headers);
	}
	
	@Test
	public void testrestTransportForDomesticPaymentFoundationServicePost()
	{
		RequestInfo reqInfo = new RequestInfo();
		HttpHeaders headers=new HttpHeaders();
		ScheduledPaymentInstructionComposite payment= new ScheduledPaymentInstructionComposite();
		payment=client.restTransportForDomesticScheduledPaymentsFoundationServicePost(reqInfo,payment, ScheduledPaymentInstructionComposite.class, headers);
	}

}
