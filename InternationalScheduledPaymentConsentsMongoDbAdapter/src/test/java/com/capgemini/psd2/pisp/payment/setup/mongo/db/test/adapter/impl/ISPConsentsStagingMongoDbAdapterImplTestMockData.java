package com.capgemini.psd2.pisp.payment.setup.mongo.db.test.adapter.impl;

import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBInternationalScheduled1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalScheduledConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalScheduledConsentResponse1;

public class ISPConsentsStagingMongoDbAdapterImplTestMockData {

	public static CustomISPConsentsPOSTRequest getRequest() {

		CustomISPConsentsPOSTRequest request = new CustomISPConsentsPOSTRequest();
		
		request.setData(new OBWriteDataInternationalScheduledConsent1());
		request.getData().setInitiation(new OBInternationalScheduled1());
		request.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		request.getData().getInitiation().getInstructedAmount().setAmount("456.25");
		request.getData().getInitiation().getInstructedAmount().setCurrency("EUR");
		request.getData().getInitiation().setChargeBearer(OBChargeBearerType1Code.FOLLOWINGSERVICELEVEL);
		request.getData().getInitiation().setCreditorAccount(new OBCashAccountCreditor2());
		request.getData().getInitiation().getCreditorAccount().setSecondaryIdentification("SecondaryID");

		return request;

	}

	public static CustomISPConsentsPOSTResponse getResponse() {
		
		CustomISPConsentsPOSTResponse response = new CustomISPConsentsPOSTResponse();

		response.setData(new OBWriteDataInternationalScheduledConsentResponse1());
		response.getData().setInitiation(new OBInternationalScheduled1());
		response.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		response.getData().getInitiation().getInstructedAmount().setAmount("145.23");
		return response;

	}
}
