package com.capgemini.psd2.integration.service.impl.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.security.Key;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.service.impl.JWSVerificationServiceImpl;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.utilities.AbstractJWSVerifier;
import com.capgemini.psd2.utilities.CertificateGenerator;
import com.nimbusds.jose.JOSEException;

public class JWSVerificationServiceImplTest {
	
	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;

	@Mock
	private CertificateGenerator certificateGenerator;
	

	@Mock
	private AbstractJWSVerifier verifier;
	
	@InjectMocks
	private JWSVerificationServiceImpl jwsVerificationServiceImpl;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
				
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateSignature() throws JOSEException {
		jwsVerificationServiceImpl.getCritData();
		jwsVerificationServiceImpl.getSupportedAlgo();
		ReflectionTestUtils.setField(jwsVerificationServiceImpl, "critData", IntegrationOperationsAPIMockdata.getCritData());
		ReflectionTestUtils.setField(jwsVerificationServiceImpl,"supportedAlgo", IntegrationOperationsAPIMockdata.getSupportedAlgo());
		String jwsHeader="eyJiNjQiOmZhbHNlLCJodHRwOlwvXC9vcGVuYmFua2luZy5vcmcudWtcL2lhdCI6MTUzNjIxODMzNiwiY3JpdCI6WyJiNjQiLCJodHRwOlwvXC9vcGVuYmFua2luZy5vcmcudWtcL2lhdCIsImh0dHA6XC9cL29wZW5iYW5raW5nLm9yZy51a1wvaXNzIl0sImtpZCI6InlUWFpFY1QyUVYzakhlSnRLYU9mWTB2M3l0USIsImN0eSI6ImFwcGxpY2F0aW9uXC9qc29uIiwidHlwIjoiSk9TRSIsImh0dHA6XC9cL29wZW5iYW5raW5nLm9yZy51a1wvaXNzIjoiQ049NFA1Tm5FcE0zV0V5MXJXd21rVHJ1aSxPVT1uQlJjWUFjQUNuZ2hiR0ZPQmssTz1PcGVuIEJhbmtpbmcgTGltaXRlZCxDPUdCIiwiYWxnIjoiUlMyNTYifQ..EhD_eLsZPzo3e1AjOnpxiyN9DFkZ-jFI3z7JE7E-GvzQsx3Cfysgzeoy9EXfYpVjjEKczrWD4BsjUlnvygcyIRmL5XFVSRhDQTrskewne203Q9jCsVUozEzO0pt2OaszF3Nr8QqyP9vmljM8H9W_SEPcLe0HAkYtnmpyfASdPTajCcxGRdwTlxjtc64ib1GU1FQf3PCa4PyEkqMIX0w5bB_5klh8P1pPLjA-VmYVfSXlnPq6Z_Qn59_sBQmuAHAsuuuyhntCILoY_diCCAcxgNMW3jiugwAIzKtW5h7CE8wdDV0NjJIyrVltDZhHwvOnDxvbCE9G9wdgOmIevmtMHw";
		when(requestHeaderAttributes.getJwsHeader()).thenReturn(jwsHeader);
		when(requestHeaderAttributes.getX_ssl_client_ou()).thenReturn("nBRcYAcACnghbGFOBk");
		when(certificateGenerator.getSigningCertificate(anyString(), anyString())).thenReturn(IntegrationOperationsAPIMockdata.getCertifcate());
		when(verifier.verifyJWS(any(Key.class),anyObject())).thenReturn(true);
		jwsVerificationServiceImpl.validateSignature(IntegrationOperationsAPIMockdata.getRequestBody(), "4P5NnEpM3WEy1rWwmkTrui");	
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateSignature1() throws JOSEException {
		jwsVerificationServiceImpl.getCritData();
		jwsVerificationServiceImpl.getSupportedAlgo();
		ReflectionTestUtils.setField(jwsVerificationServiceImpl, "critData", IntegrationOperationsAPIMockdata.getCritData());
		ReflectionTestUtils.setField(jwsVerificationServiceImpl,"supportedAlgo", IntegrationOperationsAPIMockdata.getSupportedAlgo());
		String jwsHeader="not_defined";
		when(requestHeaderAttributes.getJwsHeader()).thenReturn(jwsHeader);
		when(requestHeaderAttributes.getX_ssl_client_ou()).thenReturn("nBRcYAcACnghbGFOBk");
		when(certificateGenerator.getSigningCertificate(anyString(), anyString())).thenReturn(IntegrationOperationsAPIMockdata.getCertifcate());
		when(verifier.verifyJWS(any(Key.class),anyObject())).thenReturn(true);
		jwsVerificationServiceImpl.validateSignature(IntegrationOperationsAPIMockdata.getRequestBody(), "4P5NnEpM3WEy1rWwmkTrui");	
	}
	
	
	@Test(expected=PSD2Exception.class)
	public void testValidateSignatureException1() throws JOSEException {
		Map<String, String> map=new HashMap<>();
		map.put("HEADER", "header error message");
		map.put("FREQUENCY", "freq ma");
		map.put("AFTER_CUTOFF_DATE", "freq ma");
		map.put("FIELD", "freq ma");
		map.put("SIGNATURE", "freq ma");
		map.put("INCOMPATIBLE", "freq ma");
		map.put("RES_NOTFOUND", "freq ma");
		map.put("INTERNAL", "freq ma");
		String jwsHeader="eyJiNjQiOmZhbHNlLCJodHRwOlwvXC9vcGVuYmFua2luZy5vcmcudWtcL2lhdCI6MTUzNjIxODMzNiwiY3JpdCI6WyJiNjQiLCJodHRwOlwvXC9vcGVuYmFua2luZy5vcmcudWtcL2lhdCIsImh0dHA6XC9cL29wZW5iYW5raW5nLm9yZy51a1wvaXNzIl0sImtpZCI6InlUWFpFY1QyUVYzakhlSnRLYU9mWTB2M3l0USIsImN0eSI6ImFwcGxpY2F0aW9uXC9qc29uIiwidHlwIjoiSk9TRSIsImh0dHA6XC9cL29wZW5iYW5raW5nLm9yZy51a1wvaXNzIjoiQ049NFA1Tm5FcE0zV0V5MXJXd21rVHJ1aSxPVT1uQlJjWUFjQUNuZ2hiR0ZPQmssTz1PcGVuIEJhbmtpbmcgTGltaXRlZCxDPUdCIiwiYWxnIjoiUlMyNTYifQ..EhD_eLsZPzo3e1AjOnpxiyN9DFkZ-jFI3z7JE7E-GvzQsx3Cfysgzeoy9EXfYpVjjEKczrWD4BsjUlnvygcyIRmL5XFVSRhDQTrskewne203Q9jCsVUozEzO0pt2OaszF3Nr8QqyP9vmljM8H9W_SEPcLe0HAkYtnmpyfASdPTajCcxGRdwTlxjtc64ib1GU1FQf3PCa4PyEkqMIX0w5bB_5klh8P1pPLjA-VmYVfSXlnPq6Z_Qn59_sBQmuAHAsuuuyhntCILoY_diCCAcxgNMW3jiugwAIzKtW5h7CE8wdDV0NjJIyrVltDZhHwvOnDxvbCE9G9wdgOmIevmtMHw";
		when(requestHeaderAttributes.getJwsHeader()).thenReturn(jwsHeader);
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		jwsVerificationServiceImpl.validateSignature(IntegrationOperationsAPIMockdata.getRequestBody(), "4P5NnEpM3WEy1rWwmkTrui");	
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateSignatureException2() throws JOSEException {
		when(requestHeaderAttributes.getJwsHeader()).thenReturn("token");
		jwsVerificationServiceImpl.validateSignature(IntegrationOperationsAPIMockdata.getRequestBody(), "4P5NnEpM3WEy1rWwmkTrui");	
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateSignatureException3() throws JOSEException {
		when(requestHeaderAttributes.getJwsHeader()).thenReturn("token");
		when(requestHeaderAttributes.getX_ssl_client_ou()).thenReturn("nBRcYAcACnghbGFOBk");
		jwsVerificationServiceImpl.validateSignature(IntegrationOperationsAPIMockdata.getRequestBody(), "4P5NnEpM3WEy1rWwmkTrui");	
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateSignatureException4() throws JOSEException {
		when(requestHeaderAttributes.getJwsHeader()).thenReturn("   ..adsig");
		when(requestHeaderAttributes.getX_ssl_client_ou()).thenReturn("nBRcYAcACnghbGFOBk");
		jwsVerificationServiceImpl.validateSignature(IntegrationOperationsAPIMockdata.getRequestBody(), "4P5NnEpM3WEy1rWwmkTrui");	
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateSignatureException5() throws JOSEException {
		when(requestHeaderAttributes.getJwsHeader()).thenReturn("ICAgICA=..adsig");
		when(requestHeaderAttributes.getX_ssl_client_ou()).thenReturn("nBRcYAcACnghbGFOBk");
		jwsVerificationServiceImpl.validateSignature(IntegrationOperationsAPIMockdata.getRequestBody(), "4P5NnEpM3WEy1rWwmkTrui");	
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateSignatureException6() throws JOSEException {
		when(requestHeaderAttributes.getJwsHeader()).thenReturn("eyJiNjQiOmZhbHNlLCJodHRwOlwvXC9vcGVuYmFua2luZy5vcmcudWtcL2lhdCI6MTUzNjIyNTk2OCwiY3JpdCI6WyJiNjQiLCJodHRwOlwvXC9vcGVuYmFua2luZy5vcmcudWtcL2lhdCIsImh0dHA6XC9cL29wZW5iYW5raW5nLm9yZy51a1wvaXNzIl0sImtpZCI6InlUWFpFY1QyUVYzakhlSnRLYU9mWTB2M3l0USIsImN0eSI6ImFwcGxpY2F0aW9uXC9qc29uIiwidHlwIjoiSldUIiwiaHR0cDpcL1wvb3BlbmJhbmtpbmcub3JnLnVrXC9pc3MiOiJDTj00UDVObkVwTTNXRXkxcld3bWtUcnVpLE9VPW5CUmNZQWNBQ25naGJHRk9CayxPPU9wZW4gQmFua2luZyBMaW1pdGVkLEM9R0IiLCJhbGciOiJSUzI1NiJ9..FEhHvnASjSirjfnWr-6mN8-f8pD4lp9kd9P_08NKBIWgTSLJGWJJLNcHf4Q96ura9bNi31JxBXjoFYSn3Z-QyE6el0Yp5la-pQNDsNvixbmUy7Kci_89iUU6z8e6nsuAymP-HwcBJlqHjEGFYKx3aB8fAHWyvBwboNrG0pPW3H3CMJ3JWs4KizxzybAVmCYxmosHqP7xXr2D67gp3DJydAAoNI0mv7oi5L6lwCqgItivoCA4eJ09Gw8hkPVpmojXJBlg0KHFu4CIUQzWkUV4bbfWHVCR7JCLXbNHEsEhoyLHk1U96fE1RORmmNdH7CXl90BzhSCFvSEHE6q9mMXBnQ");
		when(requestHeaderAttributes.getX_ssl_client_ou()).thenReturn("nBRcYAcACnghbGFOBk");
		jwsVerificationServiceImpl.validateSignature(IntegrationOperationsAPIMockdata.getRequestBody(), "4P5NnEpM3WEy1rWwmkTrui");	
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateSignatureException7() throws JOSEException {
		when(requestHeaderAttributes.getJwsHeader()).thenReturn("eyJiNjQiOmZhbHNlLCJodHRwOlwvXC9vcGVuYmFua2luZy5vcmcudWtcL2lhdCI6MTUzNjIyNjA5MywiY3JpdCI6WyJiNjQiLCJodHRwOlwvXC9vcGVuYmFua2luZy5vcmcudWtcL2lhdCIsImh0dHA6XC9cL29wZW5iYW5raW5nLm9yZy51a1wvaXNzIl0sImtpZCI6InlUWFpFY1QyUVYzakhlSnRLYU9mWTB2M3l0USIsImN0eSI6ImFwcGxpY2F0aW9uanNvbiIsInR5cCI6IkpPU0UiLCJodHRwOlwvXC9vcGVuYmFua2luZy5vcmcudWtcL2lzcyI6IkNOPTRQNU5uRXBNM1dFeTFyV3dta1RydWksT1U9bkJSY1lBY0FDbmdoYkdGT0JrLE89T3BlbiBCYW5raW5nIExpbWl0ZWQsQz1HQiIsImFsZyI6IlJTMjU2In0..YE1ZIDcYdHzT7reuPz01DB0FbQvnsArSqyIfQ22pftt4D19L_d52SZRCIIhn2naVZ0sKo7xdJcuI3MPShR4VonUTpyHIAduGibhUrnTk2tQYq3k5A-wNpc-XIhJCw47_tUoMXMw8aI7drhx2CfJNpFc9RK5X7wWH20zMp9MEba4AwIZDGaFgNon1XtVpL4mVtUAY2Q8I78yBvM054SWv91g0pAXRoqKefTamNwUVKyaMRhaYQe4_FKeuaeCUCGQrZEqWuzslhPYWIXhKfD2WUUXl8u8A7VaAK1FyLmu8OA9CE3rfm0uGYba_6QnyLDNJkoxp0u6BR89pruzh21CZlA");
		when(requestHeaderAttributes.getX_ssl_client_ou()).thenReturn("nBRcYAcACnghbGFOBk");
		jwsVerificationServiceImpl.validateSignature(IntegrationOperationsAPIMockdata.getRequestBody(), "4P5NnEpM3WEy1rWwmkTrui");	
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateSignatureException8() throws JOSEException {
		when(requestHeaderAttributes.getJwsHeader()).thenReturn("eyJiNjQiOmZhbHNlLCJodHRwOlwvXC9vcGVuYmFua2luZy5vcmcudWtcL2lhdCI6MTUzNjIyNjI1MSwiY3JpdCI6WyJiNjQiLCJodHRwOlwvXC9vcGVuYmFua2luZy5vcmcudWtcL2lhdCIsImh0dHA6XC9cL29wZW5iYW5raW5nLm9yZy51a1wvaXNzIl0sImtpZCI6InlUWFpFY1QyUVYzakhlSnRLYU9mWTB2M3l0USIsImN0eSI6ImFwcGxpY2F0aW9uXC9qc29uIiwidHlwIjoiSk9TRSIsImh0dHA6XC9cL29wZW5iYW5raW5nLm9yZy51a1wvaXNzIjoiQ049NFA1Tm5FcE0zV0V5MXJXd21rVHJ1aSxPVT1uQlJjWUFjQUNuZ2hiR0ZPQmssTz1PcGVuIEJhbmtpbmcgTGltaXRlZCxDPUdCIiwiYWxnIjoiUlMzODQifQ..OZPjbGKiTHieEitA9oXrXWF4iOdjjK5Cq1ucUMiJ0a5sCPXFI8Kll9SjMpZ2vxqZOZSdcwCQLyZjzR6aAhQIY4Kjb1qK8pLI2nkvJrrywHx3bTQykAq-aUV4d8pyvbNS7DKMY4ZRQ-eDBKDMvpUCUBq7s6OLYqKIH1g6wSu4s4-Ih4pZ55GTSdSL1aXAewMC9wJj52zOvDPGEbbkyB988UN6E-Gdk1IBaMmd8IOPnt7SCa3Z6eaZuk3LIa9SrbT7JcRsOmm4cn5t91zrPDVtTPCtU7EaX3590fr49_YXMxvHZ9m-RlY0OZxHRreGwY6L2BNXLTLSa9b_5c8x8gRUjA");
		when(requestHeaderAttributes.getX_ssl_client_ou()).thenReturn("nBRcYAcACnghbGFOBk");
		jwsVerificationServiceImpl.validateSignature(IntegrationOperationsAPIMockdata.getRequestBody(), "4P5NnEpM3WEy1rWwmkTrui");	
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateSignatureException9() throws JOSEException {
		ReflectionTestUtils.setField(jwsVerificationServiceImpl, "critData", IntegrationOperationsAPIMockdata.getCritData());
		ReflectionTestUtils.setField(jwsVerificationServiceImpl,"supportedAlgo", IntegrationOperationsAPIMockdata.getSupportedAlgo());
		when(requestHeaderAttributes.getJwsHeader()).thenReturn("eyJiNjQiOmZhbHNlLCJodHRwOlwvXC9vcGVuYmFua2luZy5vcmcudWtcL2lhdCI6MTUzNjIyNjUxNCwiY3JpdCI6WyJodHRwOlwvXC9vcGVuYmFua2luZy5vcmcudWtcL2lhdCIsImI2IiwiaHR0cDpcL1wvb3BlbmJhbmtpbmcub3JnLnVrXC9pc3MiXSwia2lkIjoieVRYWkVjVDJRVjNqSGVKdEthT2ZZMHYzeXRRIiwiY3R5IjoiYXBwbGljYXRpb25cL2pzb24iLCJ0eXAiOiJKT1NFIiwiaHR0cDpcL1wvb3BlbmJhbmtpbmcub3JnLnVrXC9pc3MiOiJDTj00UDVObkVwTTNXRXkxcld3bWtUcnVpLE9VPW5CUmNZQWNBQ25naGJHRk9CayxPPU9wZW4gQmFua2luZyBMaW1pdGVkLEM9R0IiLCJhbGciOiJSUzI1NiJ9..A1jcDbSBbSNpgt4psgR4vvfrw5ElWJJnOHnd6-tx0Vu8Oj4WNuKnbMBBQijnOlWdVpnrQMegy9wlj9k_JnDPfy3L6S5BMaEYRxAa5MyhS5u10-DHECnF6peTVk9qR-DYraq4pdRPqOE8gMVFYFfWeX5c9rbPFT8JW6BnoxqUk4JJDRl4PSe2h_PlVuC2SM0IMdQWKzRIVWoMJsIL9p4HPgwm5kJp_jFr_Pn4-pr-K-qX-jSxtFurGYAyAFKveBC4WkmG5Lu_pHvwQcdtxxhV6Sq1TDg_fCIDht-vWiUctVV4cvStJP3sVCDMBtLHMKslSETWS9Isyc3W-Zp5jdSVHQ");
		when(requestHeaderAttributes.getX_ssl_client_ou()).thenReturn("nBRcYAcACnghbGFOBk");
		jwsVerificationServiceImpl.validateSignature(IntegrationOperationsAPIMockdata.getRequestBody(), "4P5NnEpM3WEy1rWwmkTrui");	
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateSignatureException10() throws JOSEException {
		ReflectionTestUtils.setField(jwsVerificationServiceImpl, "critData", IntegrationOperationsAPIMockdata.getCritData());
		ReflectionTestUtils.setField(jwsVerificationServiceImpl,"supportedAlgo", IntegrationOperationsAPIMockdata.getSupportedAlgo());
		when(requestHeaderAttributes.getJwsHeader()).thenReturn("eyJiNjQiOnRydWUsImh0dHA6XC9cL29wZW5iYW5raW5nLm9yZy51a1wvaWF0IjoxNTM2MjI2OTY3LCJjcml0IjpbImI2NCIsImh0dHA6XC9cL29wZW5iYW5raW5nLm9yZy51a1wvaWF0IiwiaHR0cDpcL1wvb3BlbmJhbmtpbmcub3JnLnVrXC9pc3MiXSwia2lkIjoieVRYWkVjVDJRVjNqSGVKdEthT2ZZMHYzeXRRIiwiY3R5IjoiYXBwbGljYXRpb25cL2pzb24iLCJ0eXAiOiJKT1NFIiwiaHR0cDpcL1wvb3BlbmJhbmtpbmcub3JnLnVrXC9pc3MiOiJDTj00UDVObkVwTTNXRXkxcld3bWtUcnVpLE9VPW5CUmNZQWNBQ25naGJHRk9CayxPPU9wZW4gQmFua2luZyBMaW1pdGVkLEM9R0IiLCJhbGciOiJSUzI1NiJ9..G3D-whDXIAfSYjBx3w2vyMH-6yGRDpmyB7Ofge-hpDWNAswqsw29atBf1j8Hvmp-fJfrUzyRTger-CXgwvYrVBxOqCnUPh5ivFZ9KBYaPhQWB3DMp3C8dvFbRWI5AUck_tVtbcWGiMcKSUWVJqxi6ZfCiC64q0VM4uJ5a5LA_sVqwqhIrcJlh-g0_pJ5-sqSwWUpg52KaiD-LU0J8cCoe9rLnimfTa2mGXJcOIpzWliKaF-zgQbl9MA9VM9lGe4fmu8EEi5jhaPWc5vP8Eoct4ax4A9v2tnCi_tLrikKUKPNr4DNbLbuns6TWAXAhkHPU2eD-mJ7Uag6rhmGdjjq9Q");
		when(requestHeaderAttributes.getX_ssl_client_ou()).thenReturn("nBRcYAcACnghbGFOBk");
		jwsVerificationServiceImpl.validateSignature(IntegrationOperationsAPIMockdata.getRequestBody(), "4P5NnEpM3WEy1rWwmkTrui");	
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateSignatureException11(){
		ReflectionTestUtils.setField(jwsVerificationServiceImpl, "critData", IntegrationOperationsAPIMockdata.getCritData());
		ReflectionTestUtils.setField(jwsVerificationServiceImpl,"supportedAlgo", IntegrationOperationsAPIMockdata.getSupportedAlgo());
		when(requestHeaderAttributes.getJwsHeader()).thenReturn("eyJiNjQiOmZhbHNlLCJodHRwOlwvXC9vcGVuYmFua2luZy5vcmcudWtcL2lhdCI6MTUzNjIyNzU0NCwiY3JpdCI6WyJiNjQiLCJodHRwOlwvXC9vcGVuYmFua2luZy5vcmcudWtcL2lhdCIsImh0dHA6XC9cL29wZW5iYW5raW5nLm9yZy51a1wvaXNzIl0sImN0eSI6ImFwcGxpY2F0aW9uXC9qc29uIiwidHlwIjoiSk9TRSIsImh0dHA6XC9cL29wZW5iYW5raW5nLm9yZy51a1wvaXNzIjoiQ049NFA1Tm5FcE0zV0V5MXJXd21rVHJ1aSxPVT1uQlJjWUFjQUNuZ2hiR0ZPQmssTz1PcGVuIEJhbmtpbmcgTGltaXRlZCxDPUdCIiwiYWxnIjoiUlMyNTYifQ..DfSmlLSCm3Xz46TZEFi2L0D44jK4VetUVoKZd4wZmOecQaL005qaTClG7bv01ExE2ahxbDGAHB6x5gkxEBFhbPGEduGByCT0qPvi7blTm5bDKv5JgnNyJtyYKql-aMKzOzOakttait5BuEOgIkcggGi0IBUl1UNZGxpqZk89zMGipr-HA1gu3dVR9nG5CB8iL4ek4MImN8-8vcWCAoqMG2zFljM30WOrIpbF9WHCWs5jnQqvWjb36pjLl0M6j2iqQhQVscIyOHJDk6xEDeWvmpyxR9ipGWohFf2T9h7pAFcZ-CAMos3ssZNRmixdm2LLBW7a96gGgo6iSnvhCPnzmQ");
		when(requestHeaderAttributes.getX_ssl_client_ou()).thenReturn("nBRcYAcACnghbGFOBk");
		jwsVerificationServiceImpl.validateSignature(IntegrationOperationsAPIMockdata.getRequestBody(), "4P5NnEpM3WEy1rWwmkTrui");	
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateSignatureException12() {
		ReflectionTestUtils.setField(jwsVerificationServiceImpl, "critData", IntegrationOperationsAPIMockdata.getCritData());
		ReflectionTestUtils.setField(jwsVerificationServiceImpl,"supportedAlgo", IntegrationOperationsAPIMockdata.getSupportedAlgo());
		when(requestHeaderAttributes.getJwsHeader()).thenReturn("eyJiNjQiOmZhbHNlLCJodHRwOlwvXC9vcGVuYmFua2luZy5vcmcudWtcL2lhdCI6MTUzNjIyODA3MCwiY3JpdCI6WyJiNjQiLCJodHRwOlwvXC9vcGVuYmFua2luZy5vcmcudWtcL2lhdCIsImh0dHA6XC9cL29wZW5iYW5raW5nLm9yZy51a1wvaXNzIl0sImtpZCI6InlUWFpFY1QyUVYzakhlSnRLYU9mWTB2M3l0USIsImN0eSI6ImFwcGxpY2F0aW9uXC9qc29uIiwidHlwIjoiSk9TRSIsImh0dHA6XC9cL29wZW5iYW5raW5nLm9yZy51a1wvaXNzIjpudWxsLCJhbGciOiJSUzI1NiJ9..qaFw51zepixFYcQHGAbHayzGavUQqNIeQ-qbpQVbvQPZmoyoTiwYeuPb80nfeg4jG7RsVuDk_fO8Xvvn0xm4mE1GhZQocflYSgklBlz_y_52VM-8rkMUbIPuI1xAmIr-mqy3eiwNiigDgHVfCLCpD5hUJ5vm0yrxCI3DTbBn1eBXVR_bReguZPuMhMSwNaZ2MSPDba-KKaxkM5h0GdygmA2oSMdLj5M-5Qwyu-HK9Cht0GmffK7Yk4MMxvfs51pBhWJSgkumJdYSDibf1fLSyYdhHYUJqqwAX-fDTPeQ-JyyvrUhhwfIzHhnXjcPQ29jonSTfROW_ahKxRSMJc4tgw");
		when(requestHeaderAttributes.getX_ssl_client_ou()).thenReturn("nBRcYAcACnghbGFOBk");
		jwsVerificationServiceImpl.validateSignature(IntegrationOperationsAPIMockdata.getRequestBody(), "4P5NnEpM3WEy1rWwmkTrui");	
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateSignatureException13() throws JOSEException {
		ReflectionTestUtils.setField(jwsVerificationServiceImpl, "critData", IntegrationOperationsAPIMockdata.getCritData());
		ReflectionTestUtils.setField(jwsVerificationServiceImpl,"supportedAlgo", IntegrationOperationsAPIMockdata.getSupportedAlgo());
		when(requestHeaderAttributes.getJwsHeader()).thenReturn("eyJiNjQiOmZhbHNlLCJodHRwOlwvXC9vcGVuYmFua2luZy5vcmcudWtcL2lhdCI6MTUzNjIyODI3MSwiY3JpdCI6WyJiNjQiLCJodHRwOlwvXC9vcGVuYmFua2luZy5vcmcudWtcL2lhdCIsImh0dHA6XC9cL29wZW5iYW5raW5nLm9yZy51a1wvaXNzIl0sImtpZCI6InlUWFpFY1QyUVYzakhlSnRLYU9mWTB2M3l0USIsImN0eSI6ImFwcGxpY2F0aW9uXC9qc29uIiwidHlwIjoiSk9TRSIsImh0dHA6XC9cL29wZW5iYW5raW5nLm9yZy51a1wvaXNzIjoiQ049NFA1bkVwTTNXRXkxcld3bWtUcnVpLE9VPW5CUmNZQWNBQ25naGJHRk9CayxPPU9wZW4gQmFua2luZyBMaW1pdGVkLEM9R0IiLCJhbGciOiJSUzI1NiJ9..A5JL4o3xvHfafDvd3e92oGL5m1m3B1yeNhT5p6ncB7-9li3YvWh3KTsXNyFO99H1RmY0D8cGhjUBz3YQHXawJVnS2YSfP-yZGkR9k7n2Kg_h5iV4wCAMjsVo5A4vOE_SPgscEbgtZxksVjehTmWIiDgU_fsEGVnN92a5DNlKEdy3Pbk-kiVRLqnLSuKGIBIulZTeCQdYITtKW69R0JYf44OfnPQApqqOCS_ZAgGU9SzNPvIGVLUMHHdPD_nCUdHQzSNKOzweXXqa5HJCUo_XyxAXEG6F_CIIo3a9uxzMqLjV4euzYtJJWBMZgaFBlW3YyHr1jMWrB27FgdZ4Vku6Mw");
		when(requestHeaderAttributes.getX_ssl_client_ou()).thenReturn("nBRcYAcACnghbGFOBk");
		when(certificateGenerator.getSigningCertificate(anyString(), anyString())).thenReturn(IntegrationOperationsAPIMockdata.getCertifcate());
		jwsVerificationServiceImpl.validateSignature(IntegrationOperationsAPIMockdata.getRequestBody(), "4P5NnEpM3WEy1rWwmkTrui");	
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateSignatureException14() throws JOSEException {
		ReflectionTestUtils.setField(jwsVerificationServiceImpl, "critData", IntegrationOperationsAPIMockdata.getCritData());
		ReflectionTestUtils.setField(jwsVerificationServiceImpl,"supportedAlgo", IntegrationOperationsAPIMockdata.getSupportedAlgo());
		String jwsHeader="eyJiNjQiOmZhbHNlLCJodHRwOlwvXC9vcGVuYmFua2luZy5vcmcudWtcL2lhdCI6MTUzNjIxODMzNiwiY3JpdCI6WyJiNjQiLCJodHRwOlwvXC9vcGVuYmFua2luZy5vcmcudWtcL2lhdCIsImh0dHA6XC9cL29wZW5iYW5raW5nLm9yZy51a1wvaXNzIl0sImtpZCI6InlUWFpFY1QyUVYzakhlSnRLYU9mWTB2M3l0USIsImN0eSI6ImFwcGxpY2F0aW9uXC9qc29uIiwidHlwIjoiSk9TRSIsImh0dHA6XC9cL29wZW5iYW5raW5nLm9yZy51a1wvaXNzIjoiQ049NFA1Tm5FcE0zV0V5MXJXd21rVHJ1aSxPVT1uQlJjWUFjQUNuZ2hiR0ZPQmssTz1PcGVuIEJhbmtpbmcgTGltaXRlZCxDPUdCIiwiYWxnIjoiUlMyNTYifQ..EhD_eLsZPzo3e1AjOnpxiyN9DFkZ-jFI3z7JE7E-GvzQsx3Cfysgzeoy9EXfYpVjjEKczrWD4BsjUlnvygcyIRmL5XFVSRhDQTrskewne203Q9jCsVUozEzO0pt2OaszF3Nr8QqyP9vmljM8H9W_SEPcLe0HAkYtnmpyfASdPTajCcxGRdwTlxjtc64ib1GU1FQf3PCa4PyEkqMIX0w5bB_5klh8P1pPLjA-VmYVfSXlnPq6Z_Qn59_sBQmuAHAsuuuyhntCILoY_diCCAcxgNMW3jiugwAIzKtW5h7CE8wdDV0NjJIyrVltDZhHwvOnDxvbCE9G9wdgOmIevmtMHw";
		when(requestHeaderAttributes.getJwsHeader()).thenReturn(jwsHeader);
		when(requestHeaderAttributes.getX_ssl_client_ou()).thenReturn("nBRcYAcACnghbGFOBk");
		when(certificateGenerator.getSigningCertificate(anyString(), anyString())).thenReturn(IntegrationOperationsAPIMockdata.getCertifcate());
		when(verifier.verifyJWS(any(Key.class),anyObject())).thenReturn(false);
		jwsVerificationServiceImpl.validateSignature("dummybody", "4P5NnEpM3WEy1rWwmkTrui");	
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateSignatureException15() throws JOSEException {
		ReflectionTestUtils.setField(jwsVerificationServiceImpl, "critData", IntegrationOperationsAPIMockdata.getCritData());
		ReflectionTestUtils.setField(jwsVerificationServiceImpl,"supportedAlgo", IntegrationOperationsAPIMockdata.getSupportedAlgo());
		String jwsHeader="eyJiNjQiOmZhbHNlLCJodHRwOlwvXC9vcGVuYmFua2luZy5vcmcudWtcL2lhdCI6MTUzNjIxODMzNiwiY3JpdCI6WyJiNjQiLCJodHRwOlwvXC9vcGVuYmFua2luZy5vcmcudWtcL2lhdCIsImh0dHA6XC9cL29wZW5iYW5raW5nLm9yZy51a1wvaXNzIl0sImtpZCI6InlUWFpFY1QyUVYzakhlSnRLYU9mWTB2M3l0USIsImN0eSI6ImFwcGxpY2F0aW9uXC9qc29uIiwidHlwIjoiSk9TRSIsImh0dHA6XC9cL29wZW5iYW5raW5nLm9yZy51a1wvaXNzIjoiQ049NFA1Tm5FcE0zV0V5MXJXd21rVHJ1aSxPVT1uQlJjWUFjQUNuZ2hiR0ZPQmssTz1PcGVuIEJhbmtpbmcgTGltaXRlZCxDPUdCIiwiYWxnIjoiUlMyNTYifQ..EhD_eLsZPzo3e1AjOnpxiyN9DFkZ-jFI3z7JE7E-GvzQsx3Cfysgzeoy9EXfYpVjjEKczrWD4BsjUlnvygcyIRmL5XFVSRhDQTrskewne203Q9jCsVUozEzO0pt2OaszF3Nr8QqyP9vmljM8H9W_SEPcLe0HAkYtnmpyfASdPTajCcxGRdwTlxjtc64ib1GU1FQf3PCa4PyEkqMIX0w5bB_5klh8P1pPLjA-VmYVfSXlnPq6Z_Qn59_sBQmuAHAsuuuyhntCILoY_diCCAcxgNMW3jiugwAIzKtW5h7CE8wdDV0NjJIyrVltDZhHwvOnDxvbCE9G9wdgOmIevmtMHw";
		when(requestHeaderAttributes.getJwsHeader()).thenReturn(jwsHeader);
		when(requestHeaderAttributes.getX_ssl_client_ou()).thenReturn("nBRcYAcACnghbGFOBk");
		when(certificateGenerator.getSigningCertificate(anyString(), anyString())).thenReturn(IntegrationOperationsAPIMockdata.getCertifcate());
		doThrow(JOSEException.class).when(verifier).verifyJWS(any(Key.class),anyObject());
		jwsVerificationServiceImpl.validateSignature("dummybody", "4P5NnEpM3WEy1rWwmkTrui");	
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateSignatureException16() throws JOSEException {
		jwsVerificationServiceImpl.getCritData();
		jwsVerificationServiceImpl.getSupportedAlgo();
		ReflectionTestUtils.setField(jwsVerificationServiceImpl, "critData", IntegrationOperationsAPIMockdata.getCritData());
		ReflectionTestUtils.setField(jwsVerificationServiceImpl,"supportedAlgo", IntegrationOperationsAPIMockdata.getSupportedAlgo());
		String jwsHeader="eyJjdHkiOiJhcHBsaWNhdGlvblwvanNvbiIsInR5cCI6IkpPU0UiLCJodHRwOlwvXC9vcGVuYmFua2luZy5vcmcudWtcL2lzcyI6IkNOPTVwUlhuNEZKN2JTYkt3Yjd4NWRhS3UsT1U9MDAxNTgwMDAwMGpmUTlhQUFFLE89T3BlbkJhbmtpbmcsQz1HQiIsImFsZyI6IlJTMjU2Iiwia2lkIjoiU3Z6OVRiRXpYeFBlMmY3YndVbzF6NWdPT2FBIn0..LnFeAptpXSRR5FgjU8JAs-oAUbuWKNknalaasxKmjitpbtmJEcZfWRc_d9X4AF4ANxbKviMQtizFsIXr4AbhrzKnoxguFl0zBbCDwfdXyJV3zpJ2tG0-RhpPOJmmpjESNpzHMj_XBN5k8Gefq7zjPqAkC74kpmlCVyc375VGkd5b8Ts53jOFQOV4X7eMZj51OzhLI0N-29T3jhLg3-DvoexwLUS9LIFaqqSJ8VfTCse-pgLgSygNm_bXBy1vfsAoyANE2LGcg69q3-KkTJDYZrWuH3FIisMmT-7oyGEeMZmGcXY6bQUnL5DvSA7RE9AK7Umaxb2ngy_wx9T7zBxuxw";
		when(requestHeaderAttributes.getJwsHeader()).thenReturn(jwsHeader);
		when(requestHeaderAttributes.getX_ssl_client_ou()).thenReturn("nBRcYAcACnghbGFOBk");
		when(certificateGenerator.getSigningCertificate(anyString(), anyString())).thenReturn(IntegrationOperationsAPIMockdata.getCertifcate());
		when(verifier.verifyJWS(any(Key.class),anyObject())).thenReturn(true);
		jwsVerificationServiceImpl.validateSignature(IntegrationOperationsAPIMockdata.getRequestBody(), "4P5NnEpM3WEy1rWwmkTrui");	
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateSignatureException17() throws JOSEException {
		jwsVerificationServiceImpl.getCritData();
		jwsVerificationServiceImpl.getSupportedAlgo();
		ReflectionTestUtils.setField(jwsVerificationServiceImpl, "critData", IntegrationOperationsAPIMockdata.getCritData());
		ReflectionTestUtils.setField(jwsVerificationServiceImpl,"supportedAlgo", IntegrationOperationsAPIMockdata.getSupportedAlgo());
		String jwsHeader="eyJiNjQiOiJmYWxzZSIsImN0eSI6ImFwcGxpY2F0aW9uXC9qc29uIiwidHlwIjoiSk9TRSIsImh0dHA6XC9cL29wZW5iYW5raW5nLm9yZy51a1wvaXNzIjoiQ049NXBSWG40Rko3YlNiS3diN3g1ZGFLdSxPVT0wMDE1ODAwMDAwamZROWFBQUUsTz1PcGVuQmFua2luZyxDPUdCIiwiYWxnIjoiUlMyNTYiLCJraWQiOiJTdno5VGJFelh4UGUyZjdid1VvMXo1Z09PYUEifQ..fSEcvVcmGyi5l_DDfuEGX4TQyASesjWVDihlOEfsMpSthOZFjKqPM9xh2hshxQqRCpumWFr-M2BS0GTajObgw_6CsO-RipMismP5t_FntAdvu8bz6EQnGRL4pYL_7iIRUJtuorOok7Wi5ww_HgXdZ5kmOesDo6xwKOSFt8aWiPqzt3ry6VuApL25I_pt3AOVzad9Jd_6okJB1FC5G7qF11x7IgJmp7nX2jnaK0732Jc_-cxjBGoulsiqULyviPfy60S68sfffyCRs75G_-MNPEhYx4Vz_LfnXd1uhRwr0fbELx_5-Akpn3M6NRu0Ohg2juu-NwgTeVgjCpkYKzDdGw";
		when(requestHeaderAttributes.getJwsHeader()).thenReturn(jwsHeader);
		when(requestHeaderAttributes.getX_ssl_client_ou()).thenReturn("nBRcYAcACnghbGFOBk");
		when(certificateGenerator.getSigningCertificate(anyString(), anyString())).thenReturn(IntegrationOperationsAPIMockdata.getCertifcate());
		when(verifier.verifyJWS(any(Key.class),anyObject())).thenReturn(true);
		jwsVerificationServiceImpl.validateSignature(IntegrationOperationsAPIMockdata.getRequestBody(), "4P5NnEpM3WEy1rWwmkTrui");	
	}
}
