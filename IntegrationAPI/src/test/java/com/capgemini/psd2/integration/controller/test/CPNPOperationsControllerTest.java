package com.capgemini.psd2.integration.controller.test;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.integration.controller.CPNPOperationsController;
import com.capgemini.psd2.integration.service.CPNPOperationsService;
import com.capgemini.psd2.integration.service.impl.CPNPOperationsServiceImpl;

public class CPNPOperationsControllerTest {

	@Mock
	private CPNPOperationsService service;
	
	@InjectMocks
	private CPNPOperationsController controller;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testControllerMethod() {
		doNothing().when(service).verifyClient(anyString(), anyString());
		controller.verifyCPNP("99999999", "123456");
	}
}
