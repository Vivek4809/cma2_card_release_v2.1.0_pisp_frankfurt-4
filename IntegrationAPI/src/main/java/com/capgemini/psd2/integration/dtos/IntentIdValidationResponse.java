package com.capgemini.psd2.integration.dtos;

public class IntentIdValidationResponse {
	private String intentType;

	public String getIntentType() {
		return intentType;
	}

	public void setIntentType(String intentType) {
		this.intentType = intentType;
	}
	
}
