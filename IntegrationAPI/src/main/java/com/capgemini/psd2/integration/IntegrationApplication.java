package com.capgemini.psd2.integration;

import javax.servlet.Filter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.integration.utilities.RSAVerifierGenerator;
import com.capgemini.psd2.logger.InternalPSD2Filter;
import com.capgemini.psd2.utilities.AbstractJWSVerifier;

@EnableEurekaClient
@SpringBootApplication
@EnableMongoRepositories(basePackages={"com.capgemini.psd2"})
@ComponentScan(basePackages = {"com.capgemini.psd2"})
public class IntegrationApplication {

	static ConfigurableApplicationContext context = null;
	public static void main(String[] args) {
		context = SpringApplication.run(IntegrationApplication.class, args);
	}
	
	@Bean
	public RestTemplate restTemplate(){
		return new RestTemplate();
	}
	
	@Bean(name="internalPsd2Filter")
	public Filter internalPSD2Filter(){
		return new InternalPSD2Filter(); 
	}	
	
	@Bean
	public AbstractJWSVerifier abstractJWSVerifier() {
		return new RSAVerifierGenerator();
	}
	
}

