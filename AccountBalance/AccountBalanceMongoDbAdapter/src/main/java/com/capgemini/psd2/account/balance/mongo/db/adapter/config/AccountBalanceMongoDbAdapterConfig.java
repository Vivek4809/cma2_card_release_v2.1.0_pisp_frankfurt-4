/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.balance.mongo.db.adapter.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.capgemini.psd2.account.balance.mongo.db.adapter.impl.AccountBalanceMongoDbAdaptorImpl;
import com.capgemini.psd2.aisp.adapter.AccountBalanceAdapter;

/**
 * The Class AccountBalanceMongoDbAdapterConfig.
 */
@Configuration
public class AccountBalanceMongoDbAdapterConfig {
	
	/**
	 * Mongo DB adapter.
	 *
	 * @return the account balance adapter
	 */
	@Bean(name="accountBalanceMongoDbAdapter")
	public AccountBalanceAdapter mongoDBAdapter(){
		return new AccountBalanceMongoDbAdaptorImpl();
	}
}