/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.balance.mongo.db.adapter.test.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.balance.mongo.db.adapter.impl.AccountBalanceMongoDbAdaptorImpl;
import com.capgemini.psd2.account.balance.mongo.db.adapter.repository.AccountBalanceRepository;
import com.capgemini.psd2.account.balance.mongo.db.adapter.test.mock.data.AccountBalanceMockData;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountBalanceCMA2;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBalanceResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

/**
 * The Class AccountBalanceMongoDBAdapterImplTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountBalanceMongoDBAdapterImplTest {
	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		//specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		//specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		specificErrorMessageMap.put("NO_ACCOUNT_ID_FOUND", "signature_invalid_content");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}

	/** The account balance mongo DB adapter impl. */
	@InjectMocks
	private AccountBalanceMongoDbAdaptorImpl accountBalanceMongoDBAdaptorImpl;

	@Mock
	AccountBalanceRepository accountBalanceRepository;

	@Mock
	LoggerUtils loggerUtils;

	@Mock
	SandboxValidationUtility utility;
	/**
	 * Test retrieve blank response account direct debits success flow.
	 */

	@Test
	public void testRetrieveAccountDirectDebitsSuccessFlow() {
		AccountMapping accountMapping = new AccountMapping();
		ArrayList<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashedMap();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		Mockito.when(utility.isValidPsuId(accountMapping.getPsuId())).thenReturn(false);

		Mockito.when(accountBalanceRepository.findByAccountNumberAndAccountNSC(anyString(), anyString()))
				.thenReturn(AccountBalanceMockData.getMockAccountDetails());

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountBalanceMockData.getMockLoggerData());

		PlatformAccountBalanceResponse actualResponse = accountBalanceMongoDBAdaptorImpl
				.retrieveAccountBalance(accountMapping, params);

 		assertEquals(AccountBalanceMockData.getMockExpectedBalanceresponse().getData().getBalance().get(0).getAccountId(),
				actualResponse.getoBReadBalance1().getData().getBalance().get(0).getAccountId());

	}

	@Test(expected = PSD2Exception.class)
	public void testRetrieveAccountBalanceExceptionFlow() {
		AccountMapping accountMapping = new AccountMapping();
		ArrayList<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashedMap();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		
		Mockito.when(utility.isValidPsuId(accountMapping.getPsuId())).thenReturn(false);

		Mockito.when(accountBalanceRepository.findByAccountNumberAndAccountNSC(anyString(), anyString()))
				.thenThrow(DataAccessResourceFailureException.class);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountBalanceMockData.getMockLoggerData());

		PlatformAccountBalanceResponse actualResponse = accountBalanceMongoDBAdaptorImpl
				.retrieveAccountBalance(accountMapping, params);

		assertEquals(AccountBalanceMockData.getMockExpectedBalanceresponse(),
				actualResponse.getoBReadBalance1());

	}
	
	
	@Test
	public void testRetrieveAccountDirectDebitsNullFlow() {
		AccountMapping accountMapping = new AccountMapping();
		ArrayList<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashedMap();

		accountDetails.setAccountId("123");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		
		Mockito.when(utility.isValidPsuId(accountMapping.getPsuId())).thenReturn(false);

		Mockito.when(accountBalanceRepository.findByAccountNumberAndAccountNSC(anyString(), anyString()))
				.thenReturn(null);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountBalanceMockData.getMockLoggerData());

		PlatformAccountBalanceResponse actualResponse = accountBalanceMongoDBAdaptorImpl
				.retrieveAccountBalance(accountMapping, params);

		

	}
}
