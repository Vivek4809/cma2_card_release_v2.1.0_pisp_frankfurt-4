package com.capgemini.psd2.foundationservice.account.mock.schedulepayments.boi.adapter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableMongoRepositories(basePackages = { "com.capgemini.psd2" })
@SpringBootApplication
@ComponentScan(basePackages = { "com.capgemini.psd2" })

public class AccountScheduledPayments {

	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(AccountScheduledPayments.class, args);
	}

}
