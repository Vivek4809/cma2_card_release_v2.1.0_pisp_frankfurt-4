package com.capgemini.psd2.utilities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.iban4j.CountryCode;
import org.iban4j.Iban;
import org.iban4j.Iban4jException;
import org.iban4j.IbanFormat;
import org.iban4j.IbanFormatException;
import org.iban4j.InvalidCheckDigitException;
import org.iban4j.UnsupportedCountryException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

public class PSD2IBANUtilTest {

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getCountryCode() {
		assertEquals("IE", PSD2IBANUtil.getCountryCode("IE29AIBK93115212345678"));
	}

	@Test
	public void getBankCode() {
		assertEquals("AIBK", PSD2IBANUtil.getBankCode("IE29AIBK93115212345678"));
	}

	@Test
	public void getIBANLength() {
		assertEquals(22, PSD2IBANUtil.getIbanLength(CountryCode.IE));
	}

	@Test
	public void getCheckDigit() {
		assertEquals("29", PSD2IBANUtil.getCheckDigit("IE29AIBK93115212345678"));
	}

	@Test
	public void getBban() {
		assertEquals("AIBK93115212345678", PSD2IBANUtil.getBban("IE29AIBK93115212345678"));
	}

	@Test
	public void getAccountNumber() {
		assertEquals("12345678", PSD2IBANUtil.getAccountNumber("IE29AIBK93115212345678"));
	}

	@Test
	public void getCountryCodeAndCheckDigit() {
		assertEquals("IE29", PSD2IBANUtil.getCountryCodeAndCheckDigit("IE29AIBK93115212345678"));
	}

	@Test
	public void isSupportedCountry_Supported() {
		assertTrue(PSD2IBANUtil.isSupportedCountry(CountryCode.IE));
	}

	@Test
	public void isSupportedCountry_Unsupported() {
		assertFalse(PSD2IBANUtil.isSupportedCountry(CountryCode.IN));
	}

	@Test
	public void calculateCheckDigit() {
		assertEquals("29", PSD2IBANUtil.calculateCheckDigit(Iban.valueOf("IE29AIBK93115212345678")));
	}

	@Test
	public void validate() {
		try {
			PSD2IBANUtil.validate("IE29AIBK93115212345678");
			PSD2IBANUtil.validate("BY13NBRB3600900000002Z00AB00");
		} catch (IbanFormatException | InvalidCheckDigitException | UnsupportedCountryException e) {
			assertNull(e);
		}
	}

	@Test(expected = Iban4jException.class)
	public void validate_IBAN_Null() {
		PSD2IBANUtil.validate(null);
	}

	@Test(expected = Iban4jException.class)
	public void validate_IBAN_Empty() {
		PSD2IBANUtil.validate("");
	}

	@Test(expected = Iban4jException.class)
	public void validate_IBAN_IncompleteCountryCode() {
		PSD2IBANUtil.validate("I");
	}

	@Test(expected = Iban4jException.class)
	public void validate_IBAN_InvalidCountryCode1() {
		PSD2IBANUtil.validate("I#29AIBK93115212345678");
	}

	@Test(expected = Iban4jException.class)
	public void validate_IBAN_InvalidCountryCode2() {
		PSD2IBANUtil.validate("ZZ29AIBK93115212345678");
	}

	@Test(expected = Iban4jException.class)
	public void validate_IBAN_InvalidCountryCode3() {
		PSD2IBANUtil.validate("IN29AIBK93115212345678");
	}

	@Test(expected = Iban4jException.class)
	public void validate_IBAN_IncompleteCheckDigit() {
		PSD2IBANUtil.validate("IE2");
	}

	@Test(expected = Iban4jException.class)
	public void validate_IBAN_InvalidCheckDigit() {
		PSD2IBANUtil.validate("IE2#AIBK93115212345678");
	}

	@Test(expected = Iban4jException.class)
	public void validate_IBAN_InvalidBBANLength() {
		PSD2IBANUtil.validate("IE29AIBK931152123456");
	}

	@Test
	public void validate_IBANAndIBANFormat_Default() {
		try {
			PSD2IBANUtil.validate("IE29 AIBK 9311 5212 3456 78", IbanFormat.Default);
		} catch (IbanFormatException | InvalidCheckDigitException | UnsupportedCountryException e) {
			assertNull(e);
		}
	}

	@Test(expected = Iban4jException.class)
	public void validate_IBANAndIBANFormat_Default_InvalidFormat() {
		PSD2IBANUtil.validate("IE29 AIBK 931152 12345678", IbanFormat.Default);
	}

	@Test
	public void validate_IBANAndIBANFormat_None() {
		try {
			PSD2IBANUtil.validate("IE29AIBK93115212345678", IbanFormat.None);
		} catch (IbanFormatException | InvalidCheckDigitException | UnsupportedCountryException e) {
			assertNull(e);
		}
	}

	@Test
	public void getBranchCode() {
		assertNotNull(PSD2IBANUtil.getBranchCode("IE29AIBK93115212345678"));
	}

	@Test
	public void getNationalCheckDigit() {
		assertNull(PSD2IBANUtil.getNationalCheckDigit("IE29AIBK93115212345678"));
	}

	@Test
	public void getAccountType() {
		assertNull(PSD2IBANUtil.getAccountType("IE29AIBK93115212345678"));
	}

	@Test
	public void getOwnerAccountType() {
		assertNull(PSD2IBANUtil.getOwnerAccountType("IE29AIBK93115212345678"));
	}

	@Test
	public void getIdentificationNumber() {
		assertNull(PSD2IBANUtil.getIdentificationNumber("IE29AIBK93115212345678"));
	}

}
