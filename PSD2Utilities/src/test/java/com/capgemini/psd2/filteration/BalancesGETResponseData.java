/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.filteration;

/**
 * The Class BalancesGETResponseData.
 */
public class BalancesGETResponseData {
	
	/** The account id. */
	private String accountId = null;

	/** The amount. */
	private BalancesGETResponseAmount amount = null;

	/** The type. */
	private String type;

	/** The credit debit indicator. */
	private String creditDebitIndicator;

	/**
	 * Gets the account id.
	 *
	 * @return the account id
	 */
	public String getAccountId() {
		return accountId;
	}

	/**
	 * Sets the account id.
	 *
	 * @param accountId the new account id
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	/**
	 * Gets the amount.
	 *
	 * @return the amount
	 */
	public BalancesGETResponseAmount getAmount() {
		return amount;
	}

	/**
	 * Sets the amount.
	 *
	 * @param amount the new amount
	 */
	public void setAmount(BalancesGETResponseAmount amount) {
		this.amount = amount;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the credit debit indicator.
	 *
	 * @return the credit debit indicator
	 */
	public String getCreditDebitIndicator() {
		return creditDebitIndicator;
	}

	/**
	 * Sets the credit debit indicator.
	 *
	 * @param creditDebitIndicator the new credit debit indicator
	 */
	public void setCreditDebitIndicator(String creditDebitIndicator) {
		this.creditDebitIndicator = creditDebitIndicator;
	}

}
