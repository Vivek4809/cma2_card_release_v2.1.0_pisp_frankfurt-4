package com.capgemini.psd2.exceptions;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import com.capgemini.psd2.pisp.domain.OBErrorResponse1;

public class ExceptionAttributesTest {

	@InjectMocks
	ExceptionAttributes exceptionAttributes;

	@Before
	public void before() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void errorInfoTest(){
		ErrorInfo errorInfo = new ErrorInfo();
		exceptionAttributes.setErrorInfo(errorInfo);
		assertTrue(exceptionAttributes.getErrorInfo()== errorInfo);
	}
	
	@Test
	public void headersTest(){
		HttpHeaders headers = new HttpHeaders();
		exceptionAttributes.setHeaders(headers );
		assertTrue(exceptionAttributes.getHeaders()== headers);
	}
	
	@Test
	public void statusTest(){
		exceptionAttributes.setStatus(HttpStatus.ACCEPTED);
		assertTrue(exceptionAttributes.getStatus()== HttpStatus.ACCEPTED);
	}
	
	@Test
	public void ObErrorResponse1Test(){
		OBErrorResponse1 response = new OBErrorResponse1();
		response.setCode("400");
		exceptionAttributes.setObErrorResponse1(response);
		assertTrue(exceptionAttributes.getObErrorResponse1()== response);
	}


}
