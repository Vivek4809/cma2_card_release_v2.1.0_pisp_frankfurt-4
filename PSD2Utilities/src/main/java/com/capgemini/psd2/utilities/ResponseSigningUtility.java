package com.capgemini.psd2.utilities;

import java.io.IOException;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.jose4j.jwt.NumericDate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.bc.BouncyCastleProviderSingleton;
import com.nimbusds.jose.util.Base64URL;

@Component
@ConditionalOnExpression("${app.signature.signResponse:false}")
public class ResponseSigningUtility {

	@Value("${app.signature.signingAlgo:null}")
	private String signingAlgo;

	@Value("${app.signature.kid:null}")
	private String kid;

	@Value("${app.signature.signingKeyAlias:null}")
	private String signingKeyAlias;

	@Value("${app.signature.critData:b64,http://openbanking.org.uk/iat,http://openbanking.org.uk/iss}")
	private List<String> critData = new ArrayList<>();

	private KeyStore keyStore = null;

	@Value("${server.ssl.key-store:#{null}}")
	private String keystorePath;

	@Value("${server.ssl.key-store-password:#{null}}")
	private String keystorePassword;

	public void setSigningAlgo(String signingAlgo) {
		this.signingAlgo = signingAlgo;
	}

	public void setKid(String kid) {
		this.kid = kid;
	}

	public void setSigningKeyAlias(String signingKeyAlias) {
		this.signingKeyAlias = signingKeyAlias;
	}

	public List<String> getCritData() {
		return critData;
	}

	@PostConstruct
	public void populateKeyStore() {
		try {
			Resource resource = new ClassPathResource(keystorePath);
			InputStream in = resource.getInputStream();
			keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
			keyStore.load(in, keystorePassword.toCharArray());
		} catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException e) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, e.getMessage()));
		}
	}

	public String signResponseBody(Object responseObj) {
		ObjectMapper mapper=new ObjectMapper();
		ResponseEntity<Object> responseEnitity=null;
		JWSObject jwsObject=null;
		JWSHeader header=null;
		String jsonResponseString=null;
		PrivateKey key=null;
		JWSSigner signer=null;
		try {
			if (responseObj == null || responseObj instanceof AccountMapping) {
				return null;
			}
			validateInputs();
			mapper.setSerializationInclusion(Include.NON_NULL);
			if(responseObj instanceof ResponseEntity<?>) {
			responseEnitity=(ResponseEntity<Object>)responseObj;
			Object body=responseEnitity.getBody();
			jsonResponseString= mapper.writeValueAsString(body);
			}
			else if(responseObj instanceof String){
				jsonResponseString=(String)responseObj;
			}
			else {
				jsonResponseString=mapper.writeValueAsString(responseObj);
			}
			header = populateJWSHeader();
			jwsObject=createJWSObject(header, jsonResponseString);
			key = (PrivateKey) getPrivateKey();
			signer = new RSASSASigner(key);
			signer.getJCAContext().setProvider(BouncyCastleProviderSingleton.getInstance());
			jwsObject.sign(signer);
		} catch (JOSEException | JsonProcessingException e) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, e.getMessage()));
		}
		String token = jwsObject.serialize();
		String[] tokenParts=token.split("\\.");
		
		return tokenParts[0] + ".." + jwsObject.getSignature();
	}
	
	private void validateInputs() {
		if(NullCheckUtils.isNullOrEmpty(signingAlgo) || NullCheckUtils.isNullOrEmpty(signingKeyAlias)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, "invalid parameters configured"));
		}
		if(NullCheckUtils.isNullOrEmpty(keystorePassword) || NullCheckUtils.isNullOrEmpty(kid)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, "invalid parameters configured"));
		}
	}

	private JWSObject createJWSObject(JWSHeader header,String jsonResponseString) {
		Base64URL unencodedBody = new Base64URL(jsonResponseString);
		Payload payload = new Payload(unencodedBody);
		return new JWSObject(header, payload);
	}

	private String getSubjectFromCertificate() {
		X509Certificate certificate = null;
		try {
			certificate = (X509Certificate) keyStore.getCertificate(signingKeyAlias);
			return certificate.getSubjectX500Principal().getName();
		} catch (KeyStoreException e) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, e.getMessage()));
		}
	}

	private Key getPrivateKey() {
		try {
			return keyStore.getKey(signingKeyAlias, keystorePassword.toCharArray());
		} catch (UnrecoverableKeyException | NoSuchAlgorithmException | KeyStoreException e) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, e.getMessage()));
		}
	}

	private JWSHeader populateJWSHeader() {
		Set<String> critHeaders = new HashSet<>();
		critHeaders.addAll(critData);

		return new JWSHeader.Builder(JWSAlgorithm.parse(signingAlgo)).criticalParams(critHeaders)
				.customParam(PSD2Constants.JOSE_HEADER_B64, Boolean.FALSE)
				.customParam(PSD2Constants.JOSE_HEADER_ISSUER, getSubjectFromCertificate())
				.customParam(PSD2Constants.JOSE_HEADER_IAT, NumericDate.now().getValue())
				.type(JOSEObjectType.JOSE)
				.keyID(kid)
				.contentType(MediaType.APPLICATION_JSON_VALUE).build();
	}

}
