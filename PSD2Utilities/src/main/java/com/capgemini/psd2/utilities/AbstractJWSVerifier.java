/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.utilities;

import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.util.Map;

import javax.json.JsonObject;

import com.nimbusds.jose.Header;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jwt.SignedJWT;

/**
 * The Abstract class provides JWSVerifier.
 * @author nagoswam
 *
 */
public abstract class AbstractJWSVerifier {

	public boolean verifyJWS(String jwks, SignedJWT signedJWT)
			throws InvalidKeySpecException, NoSuchAlgorithmException, JOSEException, ParseException {
		JsonObject jsonObj = JWKSUtility.getJWKSJsonObject(jwks);
		Map<String, Key> keyMap = JWKSUtility.buildKeyMap(jsonObj);
		String kid = signedJWT.getHeader().getKeyID();
		Key key=keyMap.get(kid);
		JWSVerifier jwsVerifier= createJWSVerifier(key,signedJWT.getHeader());
		return signedJWT.verify(jwsVerifier);
	}
	
	/**
	 * Method gives JWSverifier like RSASSAVerifier, ECDSAVerifier etc as per implementation.
	 * @param key
	 * @return RSASSAVerifier, ECDSAVerifier etc
	 */
	public abstract JWSVerifier createJWSVerifier(Key key,Header header);
	
	public boolean verifyJWS(Key publicKey,SignedJWT signedJWT) throws JOSEException {
		JWSVerifier jwsVerifier= createJWSVerifier(publicKey,signedJWT.getHeader());
		return signedJWT.verify(jwsVerifier);
	}

}
