package com.capgemini.psd2.utilities;

import org.iban4j.CountryCode;
import org.iban4j.bban.BbanStructure;
import org.iban4j.bban.BbanStructureEntry;

import java.util.*;

/**
 * Class which represents bban structure
 */
public class PSD2BBANStructure{

    private final BbanStructureEntry[] entries;

    private PSD2BBANStructure(final BbanStructureEntry... entries) {
        this.entries = entries;
    }

    private static final EnumMap<CountryCode, PSD2BBANStructure> structures;

    static {
    	
    	structures = new EnumMap<CountryCode, PSD2BBANStructure>(CountryCode.class);
    	
    	BbanStructure.supportedCountries().forEach(countryCode -> {
    		BbanStructure structure = BbanStructure.forCountry(countryCode);
    		BbanStructureEntry[] bbanEntries = new BbanStructureEntry[structure.getEntries().size()];
    		structures.put(countryCode, new	PSD2BBANStructure(structure.getEntries().toArray(bbanEntries))); 
    	});
        
        //Pakistan : Modified
    	structures.put(CountryCode.PK,
                new PSD2BBANStructure(
                        BbanStructureEntry.bankCode(4, 'a'),
                        BbanStructureEntry.accountNumber(16, 'c')));
        

        //Costa Rica modified
        structures.put(CountryCode.CR,
                new PSD2BBANStructure(
                        BbanStructureEntry.bankCode(4, 'n'),
                        BbanStructureEntry.accountNumber(14, 'n')));
        
        //Turkey: modified
        structures.put(CountryCode.TR,
                new PSD2BBANStructure(
                        BbanStructureEntry.bankCode(5, 'n'),
                        BbanStructureEntry.nationalCheckDigit(1, 'n'),
                        BbanStructureEntry.accountNumber(16, 'c')));
        
        //Virgin Island: modified
        structures.put(CountryCode.VG,
                new PSD2BBANStructure(
                        BbanStructureEntry.bankCode(4, 'a'),
                        BbanStructureEntry.accountNumber(16, 'n')));
        //United Arab Emirates: modified
        structures.put(CountryCode.AE,
                new PSD2BBANStructure(
                        BbanStructureEntry.bankCode(3, 'n'),
                        BbanStructureEntry.accountNumber(16, 'n')));
        
        //Vatican City
        structures.put(CountryCode.VA, 
        		new PSD2BBANStructure(
        				BbanStructureEntry.bankCode(3, 'n'),
                        BbanStructureEntry.accountNumber(15, 'n')));
        
        //Belarus
        structures.put(CountryCode.BY, 
        		new PSD2BBANStructure(
        				BbanStructureEntry.bankCode(4, 'c'),
        				BbanStructureEntry.branchCode(4, 'n'),
                        BbanStructureEntry.accountNumber(16, 'c')));
        
        //Iraq
        structures.put(CountryCode.IQ, 
        		new PSD2BBANStructure(
        				BbanStructureEntry.bankCode(4, 'a'),
        				BbanStructureEntry.branchCode(3, 'n'),
                        BbanStructureEntry.accountNumber(12, 'n')));
        
        //Sao Tome
        structures.put(CountryCode.ST, 
        		new PSD2BBANStructure(
        				BbanStructureEntry.bankCode(4, 'n'),
        				BbanStructureEntry.branchCode(4, 'n'),
                        BbanStructureEntry.accountNumber(13, 'n')));
        
        //Seychelles
        structures.put(CountryCode.SC, 
        		new PSD2BBANStructure(
        				BbanStructureEntry.bankCode(6, 'c'),
        				BbanStructureEntry.branchCode(2, 'n'),
                        BbanStructureEntry.accountNumber(19, 'c')));
        
        //Saint Lucia
        structures.put(CountryCode.LC, 
        		new PSD2BBANStructure(
        				BbanStructureEntry.bankCode(4, 'a'),
                        BbanStructureEntry.accountNumber(24, 'c')));
        
        //El salvdaor
        structures.put(CountryCode.SV, 
        		new PSD2BBANStructure(
        				BbanStructureEntry.bankCode(4, 'a'),
                        BbanStructureEntry.accountNumber(20, 'n')));
        
       
        
    }

    /**
     * @param countryCode the country code.
     * @return PSD2BBANStructure for specified country or null if country is not supported.
     */
    public static PSD2BBANStructure forCountry(final CountryCode countryCode) {
        return structures.get(countryCode);
    }

    public List<BbanStructureEntry> getEntries() {
        return Collections.unmodifiableList(Arrays.asList(entries));
    }

    public static List<CountryCode> supportedCountries() {
        final List<CountryCode> countryCodes = new ArrayList<CountryCode>(structures.size());
        countryCodes.addAll(structures.keySet());
        return Collections.unmodifiableList(countryCodes);
    }

    /**
     * Returns the length of bban.
     *
     * @return int length
     */
    public int getBbanLength() {
        int length = 0;

        for (BbanStructureEntry entry : entries) {
            length += entry.getLength();
        }

        return length;
    }
}
