package com.capgemini.psd2.mock.foundationservice.account.transactions.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.capgemini.psd2.mock.foundationservice.account.transactions.domain.ValidationViolation;
import com.capgemini.psd2.mock.foundationservice.account.transactions.domain.ValidationViolations;


@ControllerAdvice
public class MockExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(InvalidParameterRequestException.class)
	public final ResponseEntity<ValidationViolations> invalidParameterRequestException(Exception ex) {

		ValidationViolations validationViolations = new ValidationViolations();
		ValidationViolation exceptionResponce = new ValidationViolation();

		exceptionResponce.setErrorCode("FS_ABTT_001");
		exceptionResponce.setErrorText("Bad Request. Please check your request");
		exceptionResponce.setErrorField("Bad Request. Please check your request");
		exceptionResponce.setErrorValue("Bad Request");

		validationViolations.getValidationViolation().add(exceptionResponce);

		return new ResponseEntity<ValidationViolations>(validationViolations, new HttpHeaders(), HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(MissingAuthenticationHeaderException.class)
	public final ResponseEntity<ValidationViolations> authenticationFailed(Exception ex) {

		ValidationViolations validationViolations = new ValidationViolations();
		ValidationViolation exceptionResponce = new ValidationViolation();

		exceptionResponce.setErrorCode("FS_ABTT_002");
		exceptionResponce.setErrorText("You are not authorised to use this service");
		exceptionResponce.setErrorField("You are not authorised to use this service");
		exceptionResponce.setErrorValue("Header Missing");

		validationViolations.getValidationViolation().add(exceptionResponce);

		return new ResponseEntity<ValidationViolations>(validationViolations, new HttpHeaders(), HttpStatus.UNAUTHORIZED);
	}

	@ExceptionHandler(RecordNotFoundException.class)
	public final ResponseEntity<ValidationViolations> recordNotFound(Exception ex) {

		ValidationViolations validationViolations = new ValidationViolations();
		ValidationViolation exceptionResponce = new ValidationViolation();
		exceptionResponce.setErrorCode("FS_ABTT_003");
		exceptionResponce.setErrorText ("No transaction details found for the requested account(s)");
		exceptionResponce.setErrorField("No transaction details found for the requested account(s)");
		exceptionResponce.setErrorValue("Not Found");

		validationViolations.getValidationViolation().add(exceptionResponce);

		return new ResponseEntity<ValidationViolations>(validationViolations, new HttpHeaders(), HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ValidationViolations> internalServerError(Exception ex) {

		ValidationViolations validationViolations = new ValidationViolations();
		ValidationViolation exceptionResponce = new ValidationViolation();

		exceptionResponce.setErrorCode("FS_ABTT_004");
		exceptionResponce.setErrorText("Technical Error. Please try again later");
		exceptionResponce.setErrorField("Technical Error. Please try again later");
		exceptionResponce.setErrorValue("Internal Server Error");

		validationViolations.getValidationViolation().add(exceptionResponce);

		return new ResponseEntity<ValidationViolations>(validationViolations, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
