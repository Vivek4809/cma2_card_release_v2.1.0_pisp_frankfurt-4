/*************************************************************************
 * 
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 */
package com.capgemini.psd2.security.consent.pisp.rest.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.capgemini.psd2.aisp.domain.OBReadAccount2;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.consent.domain.PSD2AccountsAdditionalInfo;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsValidationResponse;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPreAuthorizeAdditionalInfo;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentAuthorizationHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentHelper;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.models.DropOffResponse;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PFInstanceData;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.scaconsenthelper.services.SCAConsentHelperService;
import com.capgemini.psd2.security.consent.pisp.helpers.PispConsentCreationDataHelper;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.utilities.JSONUtilities;

/**
 * ConsentAuthorize Controller
 * 
 * @author Capgemini
 */

@RestController
public class PispConsentAuthorizeController {

	@Value("${app.isSinglePageConsent:#{false}}")
	private boolean isSinglePageConsent;

	@Autowired
	private TPPInformationAdaptor tppInformationAdaptor;

	@Autowired
	private PispConsentCreationDataHelper consentCreationDataHelper;

	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;

	@Autowired
	@Qualifier("pispConsentAuthorizationHelper")
	private ConsentAuthorizationHelper consentAuthorizationHelper;

	@Autowired
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;

	@Autowired
	private SCAConsentHelperService helperService;

	@Autowired
	private PFConfig pfConfig;

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private HttpServletResponse response;

	/**
	 * This method is used for creating the consent
	 * 
	 * @param contract-
	 *            ConsentTemplate
	 * @return ConsentResponse - Response after creating consent
	 * @throws NamingException
	 */
	@RequestMapping(value = "/pisp/consent", method = RequestMethod.POST)
	public String createConsent(ModelAndView model, @RequestBody PSD2AccountsAdditionalInfo accountAdditionalInfo,
			@RequestParam String resumePath) throws NamingException {

		PSD2Account customerAccount = null;
		String headers = accountAdditionalInfo.getHeaders();

		if (accountAdditionalInfo != null) {
			customerAccount = accountAdditionalInfo.getAccountdetails().get(0);
		}
		if (customerAccount == null) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNTS_SELECTED_BY_CUSTOMER);
		}

		PickupDataModel pickupDataModel = SCAConsentHelper.populatePickupDataModel(request);

		// Fix Scheme name validation failed. In MongoDB producing exception:
		// can't have. In field names [UK.OBIE.IBAN] while creating test data.
		// Hence, in mock data, we used "_" instead of "." character
		// Replacing "_" to "." to convert valid Scheme Name into mongo db
		// adapter only. there is no impact on CMA api flow. Its specifc to
		// Sandbox functionality
		String schemeName;
		if (customerAccount.getAccount().get(0).getSchemeName() != null) {
			schemeName = customerAccount.getAccount().get(0).getSchemeName().replace('.', '_');
		}
		OBReadAccount2 oBReadAccount2 = consentCreationDataHelper.retrieveCustomerAccountListInfo(
				pickupDataModel.getUserId(), pickupDataModel.getClientId(),
				IntentTypeEnum.PISP_INTENT_TYPE.getIntentType(), requestHeaderAttributes.getCorrelationId(),
				pickupDataModel.getChannelId(), customerAccount.getAccount().get(0).getSchemeName(),
				requestHeaderAttributes.getTenantId(), pickupDataModel.getIntentId());

		PSD2Account unmaskedAccount = (PSD2Account) consentAuthorizationHelper.matchAccountFromList(oBReadAccount2,
				customerAccount);

		Map<String, String> paramsMap = new HashMap<String, String>();
		paramsMap.put(PSD2Constants.CONSENT_FLOW_TYPE, pickupDataModel.getIntentTypeEnum().getIntentType());
		paramsMap.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, pickupDataModel.getChannelId());
		paramsMap.put(PSD2Constants.USER_IN_REQ_HEADER, pickupDataModel.getUserId());
		paramsMap.put(PSD2Constants.PARTY_IDENTIFIER,
                unmaskedAccount.getAdditionalInformation().get(PSD2Constants.PARTY_IDENTIFIER));


		if (isSinglePageConsent) {
			CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo = populatePreAuthAdditionalInfo(unmaskedAccount);
			OBCashAccountDebtor3 selectedDebtorDetails = consentCreationDataHelper
					.populateSelectedDebtorDetails(unmaskedAccount);

			/* Removed call for retrieve payment stage details */
			PaymentConsentsValidationResponse preAuthorisationResponse = consentCreationDataHelper
					.validatePreAuthorisation(selectedDebtorDetails, preAuthAdditionalInfo,
							pickupDataModel.getIntentId(), paramsMap);

			if (preAuthorisationResponse == null || OBTransactionIndividualStatus1Code.REJECTED
					.equals(preAuthorisationResponse.getPaymentSetupValidationStatus())) {
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PRE_AUTHORISATION_FAILED);
			}
			model.addObject(PSD2SecurityConstants.PRE_AUTHORISATION_RESPONSE,
					preAuthorisationResponse.getPaymentSetupValidationStatus());
		}

		Object tppInformationObj = tppInformationAdaptor.fetchTPPInformation(pickupDataModel.getClientId());
		String tppApplicationName = tppInformationAdaptor.fetchApplicationName(pickupDataModel.getClientId());

		CustomPaymentStageIdentifiers stageIdentifiers = paymentSetupPlatformAdapter
				.populateStageIdentifiers(pickupDataModel.getIntentId());

		consentCreationDataHelper.createConsent(unmaskedAccount, pickupDataModel.getUserId(),
				pickupDataModel.getClientId(), stageIdentifiers, pickupDataModel.getChannelId(), headers,
				tppApplicationName, tppInformationObj, requestHeaderAttributes.getTenantId());

		pickupDataModel.setPaymentType(stageIdentifiers.getPaymentTypeEnum().getPaymentType());
		DropOffResponse dropOffResponse = helperService.dropOffOnConsentSubmission(pickupDataModel,
				pfConfig.getPispinstanceId(), pfConfig.getPispinstanceusername(), pfConfig.getAispinstancepassword());

		resumePath = pfConfig.getTenantSpecificResumePathBaseUrl(requestHeaderAttributes.getTenantId())
				.concat(resumePath);

		String redirectURI = UriComponentsBuilder.fromHttpUrl(resumePath)
				.queryParam(PFConstants.REF, dropOffResponse.getRef()).toUriString();

		model.addObject(PSD2SecurityConstants.REDIRECT_URI_MODEL_ATTRIBUTE, redirectURI);
		SCAConsentHelper.invalidateCookie(response);
		return JSONUtilities.getJSONOutPutFromObject(model);
	}

	@RequestMapping(value = "/pisp/cancelConsent", method = RequestMethod.PUT)
	public String cancelConsent(ModelAndView model, @RequestParam Map<String, String> paramsMap) {

		String serverErrorFlag = paramsMap.get(PSD2Constants.SERVER_ERROR_FLAG_ATTR);

		PickupDataModel pickupDataModel = SCAConsentHelper.populatePickupDataModel(request);

		/*
		 * Setting channelId in reqHeader to be available in all adapter calls
		 */
		requestHeaderAttributes.setChannelId(pickupDataModel.getChannelId());

		paramsMap.put(PSD2Constants.USER_IN_REQ_HEADER, pickupDataModel.getUserId());
		consentCreationDataHelper.cancelPaymentSetup(pickupDataModel.getIntentId(), paramsMap);

		String resumePath = paramsMap.get(PFConstants.RESUME_PATH);

		resumePath = pfConfig.getTenantSpecificResumePathBaseUrl(requestHeaderAttributes.getTenantId())
				.concat(resumePath);

		if (serverErrorFlag != null && !serverErrorFlag.isEmpty() && Boolean.valueOf(serverErrorFlag)) {
			resumePath = UriComponentsBuilder.fromHttpUrl(resumePath).queryParam(PFConstants.REF, null).toUriString();
		} else {
			PFInstanceData pfInstanceData = new PFInstanceData();
			pfInstanceData.setPfInstanceId(pfConfig.getPispinstanceId());
			pfInstanceData.setPfInstanceUserName(pfConfig.getPispinstanceusername());
			pfInstanceData.setPfInstanceUserPwd(pfConfig.getAispinstancepassword());
			resumePath = helperService.cancelJourney(resumePath, pfInstanceData);
		}
		model.addObject(PSD2SecurityConstants.REDIRECT_URI_MODEL_ATTRIBUTE, resumePath);
		SCAConsentHelper.invalidateCookie(response);
		return JSONUtilities.getJSONOutPutFromObject(model);
	}

	@RequestMapping(value = "/pisp/preAuthorisation", method = RequestMethod.POST)
	public String preAuthorisation(ModelAndView model, @RequestBody PSD2Account account,
			@RequestParam Map<String, String> paramMap) {

		if (account == null) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNTS_SELECTED_BY_CUSTOMER);
		}
		PickupDataModel pickupDataModel = SCAConsentHelper.populatePickupDataModel(request);

		/*
		 * Setting channelId in reqHeader to be available in all adapter calls
		 */
		requestHeaderAttributes.setChannelId(pickupDataModel.getChannelId());

		OBReadAccount2 oBReadAccount2 = consentCreationDataHelper.retrieveCustomerAccountListInfo(
				pickupDataModel.getUserId(), pickupDataModel.getClientId(),
				IntentTypeEnum.PISP_INTENT_TYPE.getIntentType(), requestHeaderAttributes.getCorrelationId(),
				pickupDataModel.getChannelId(), account.getAccount().get(0).getSchemeName(),
				requestHeaderAttributes.getTenantId(), pickupDataModel.getIntentId());

		PSD2Account unmaskedAccount = (PSD2Account) consentAuthorizationHelper.matchAccountFromList(oBReadAccount2,
				account);
		Map<String, String> paramsMap = new HashMap<String, String>();
		paramsMap.put(PSD2Constants.CONSENT_FLOW_TYPE, pickupDataModel.getIntentTypeEnum().getIntentType());
		paramsMap.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, pickupDataModel.getChannelId());
		paramsMap.put(PSD2Constants.USER_IN_REQ_HEADER, pickupDataModel.getUserId());
		paramMap.put(PSD2Constants.TENANT_ID, requestHeaderAttributes.getTenantId());

		CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo = populatePreAuthAdditionalInfo(unmaskedAccount);
		OBCashAccountDebtor3 selectedDebtorDetails = consentCreationDataHelper
				.populateSelectedDebtorDetails(unmaskedAccount);

		/* Removed call for retrieve payment stage details */
		PaymentConsentsValidationResponse preAuthorisationResponse = consentCreationDataHelper.validatePreAuthorisation(
				selectedDebtorDetails, preAuthAdditionalInfo, pickupDataModel.getIntentId(), paramsMap);

		if (preAuthorisationResponse == null || OBTransactionIndividualStatus1Code.REJECTED
				.equals(preAuthorisationResponse.getPaymentSetupValidationStatus())) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PRE_AUTHORISATION_FAILED);
		}
		model.addObject(PSD2SecurityConstants.PRE_AUTHORISATION_RESPONSE,
				preAuthorisationResponse.getPaymentSetupValidationStatus());
		return JSONUtilities.getJSONOutPutFromObject(model);
	}

	private CustomPreAuthorizeAdditionalInfo populatePreAuthAdditionalInfo(PSD2Account selectedAccountUnmasked) {
		CustomPreAuthorizeAdditionalInfo additionalAuthInfo = new CustomPreAuthorizeAdditionalInfo();
		additionalAuthInfo.setPayerCurrency(selectedAccountUnmasked.getCurrency());

		Map<String, String> additionalInfo = selectedAccountUnmasked.getAdditionalInformation();

		additionalAuthInfo.setPayerJurisdiction(additionalInfo.get(PSD2Constants.PAYER_JURISDICTION));
		additionalAuthInfo.setAccountNumber(additionalInfo.get(PSD2Constants.ACCOUNT_NUMBER));
		additionalAuthInfo.setAccountNSC(additionalInfo.get(PSD2Constants.ACCOUNT_NSC));
		additionalAuthInfo.setAccountIban(additionalInfo.get(PSD2Constants.IBAN));
		additionalAuthInfo.setAccountBic(additionalInfo.get(PSD2Constants.BIC));
		additionalAuthInfo.setAccountPan(additionalInfo.get(PSD2Constants.PLAPPLID));
		return additionalAuthInfo;
	}
}
