package com.capgemini.psd2.security.consent.pisp.helpers;

import java.util.Map;

import javax.naming.NamingException;

import com.capgemini.psd2.aisp.domain.OBAccount2;
import com.capgemini.psd2.aisp.domain.OBReadAccount2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.PaymentConsentsValidationResponse;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomFraudSystemPaymentData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPreAuthorizeAdditionalInfo;

public interface PispConsentCreationDataHelper {

	public void cancelPaymentSetup(String paymentId, Map<String, String> paramsMap);

	public OBReadAccount2 retrieveCustomerAccountListInfo(String userId, String clientId, String flowType,
			String correlationId, String channelId, String schemeName, String tenantId, String intentId);

	public void createConsent(OBAccount2 account, String userId, String cid,
			CustomPaymentStageIdentifiers stageIdentifiers, String channelId, String header, String tppAppName,
			Object tppInformation, String tenantId) throws NamingException;

	public CustomConsentAppViewData retrieveConsentAppStagedViewData(String paymentConsentId);

	/* Will be called via consent controller pre-authorize request */
	public PaymentConsentsValidationResponse validatePreAuthorisation(OBCashAccountDebtor3 selectedDebtorDetails,
			CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo, String intentId, Map<String, String> paramsMap);

	/* Will be called via consent controller create request */
	public CustomFraudSystemPaymentData retrieveFraudSystemPaymentData(String paymentConsentId);

	/* Will be called via consentAuthorization an createConsent */
	public OBCashAccountDebtor3 populateSelectedDebtorDetails(OBAccount2 selectedAccount);
}
