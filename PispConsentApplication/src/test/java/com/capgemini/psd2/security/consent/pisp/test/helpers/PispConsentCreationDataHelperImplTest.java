//package com.capgemini.psd2.security.consent.pisp.test.helpers;
//
//import static org.junit.Assert.assertEquals;
//import static org.mockito.Matchers.any;
//import static org.mockito.Matchers.anyMap;
//import static org.mockito.Matchers.anyObject;
//import static org.mockito.Matchers.anyString;
//import static org.mockito.Mockito.doNothing;
//import static org.mockito.Mockito.when;
//
//import java.util.Base64;
//import java.util.HashMap;
//import java.util.Map;
//
//import javax.naming.NamingException;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.util.ReflectionTestUtils;
//
//import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
//import com.capgemini.psd2.aisp.domain.OBAccount2;
//import com.capgemini.psd2.consent.domain.PSD2Account;
//import com.capgemini.psd2.consent.domain.PispConsent;
//import com.capgemini.psd2.enums.ConsentStatusEnum;
//import com.capgemini.psd2.exceptions.PSD2Exception;
//import com.capgemini.psd2.fraudsystem.helper.FraudSystemHelper;
//import com.capgemini.psd2.logger.RequestHeaderAttributes;
//import com.capgemini.psd2.pisp.adapter.PaymentSetupAdapterHelper;
//import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
//import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
//import com.capgemini.psd2.pisp.domain.PaymentSetupResponse;
//import com.capgemini.psd2.pisp.domain.PaymentSetupResponse.StatusEnum;
//import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
//import com.capgemini.psd2.pisp.validation.PispUtilities;
//import com.capgemini.psd2.security.consent.pisp.helpers.PispConsentCreationDataHelperImpl;
//import com.capgemini.psd2.security.consent.pisp.test.mock.data.PispConsentApplicationMockdata;
//
//
//@RunWith(SpringJUnit4ClassRunner.class)
//public class PispConsentCreationDataHelperImplTest {
//	
//	@Mock
//	private CustomerAccountListAdapter customerAccountListAdapter;
//	 
//	@Mock
//	private PispConsentAdapter pispConsentAdapter;
//
//	@Mock
//	private FraudSystemHelper fraudSystemHelper;
//
//	@Mock
//	private PaymentSetupAdapterHelper paymentSetupAdapterHelper;
//	
//	@Mock
//	private RequestHeaderAttributes reqAttributes;
//
//	@InjectMocks
//	private PispConsentCreationDataHelperImpl consentCreationDataHelperImpl = new PispConsentCreationDataHelperImpl();
//
//	@Before
//	public void setUp() {
//		MockitoAnnotations.initMocks(this);
//		Map<String, String> paramMap = new HashMap<>();
//		paramMap.put("PISP", "PISP");
//		paramMap.put("client_id", "client123");
//		
//	}
//	
//	@Test
//	public void testRetrievePaymentRequestSetupData() {
//		String paymentId = "56323413c061485aaa622c195aa15e7c";
//		CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
//		PaymentSetupResponse data = new PaymentSetupResponse();
//		data .setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
//		data.setStatus(StatusEnum.ACCEPTEDCUSTOMERPROFILE);
//		paymentSetupPOSTResponse.data(data);
//		when(paymentSetupAdapterHelper.retrieveStagedPaymentSetup(anyString())).thenReturn(paymentSetupPOSTResponse);
//		ReflectionTestUtils.setField(consentCreationDataHelperImpl, "paymentSetupExpiryTime", Integer.parseInt("24"));
//		assertEquals(StatusEnum.ACCEPTEDCUSTOMERPROFILE,consentCreationDataHelperImpl.retrievePaymentSetupData(paymentId).getData().getStatus());
//	}
//	
//	@Test(expected = PSD2Exception.class)
//	public void testRetrievePaymentRequestSetupDataNullPaymentData() {
//		String paymentId = "56323413c061485aaa622c195aa15e7c";
//		when(paymentSetupAdapterHelper.retrieveStagedPaymentSetup(anyString())).thenReturn(null);
//		ReflectionTestUtils.setField(consentCreationDataHelperImpl, "paymentSetupExpiryTime", Integer.parseInt("24"));
//		assertEquals(StatusEnum.ACCEPTEDCUSTOMERPROFILE,consentCreationDataHelperImpl.retrievePaymentSetupData(paymentId).getData().getStatus());
//	}
//	
//	@Test
//	public void testRetrievePaymentRequestSetupDataException() {
//		String paymentId = "56323413c061485aaa622c195aa15e7c";
//		CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
//		PaymentSetupResponse data = new PaymentSetupResponse();
//		data .setCreationDateTime("2017-02-25T00:00:00+00:00");
//		data.setStatus(StatusEnum.ACCEPTEDCUSTOMERPROFILE);
//		paymentSetupPOSTResponse.data(data);
//		when(paymentSetupAdapterHelper.retrieveStagedPaymentSetup(anyString())).thenReturn(paymentSetupPOSTResponse);
//		ReflectionTestUtils.setField(consentCreationDataHelperImpl, "paymentSetupExpiryTime", Integer.parseInt("1"));
//		assertEquals(StatusEnum.ACCEPTEDCUSTOMERPROFILE,consentCreationDataHelperImpl.retrievePaymentSetupData(paymentId).getData().getStatus());
//	}
//	
//	@Test
//	public void testCreateConsent() throws NamingException {
//		String userId="12345"; 
//		String clientId="1234";
//		String intentId="12345";
//		String channelId="12345";
//		String headers=Base64.getEncoder().encodeToString(userId.getBytes());
//		String tppApplicationName = "Moneywise.com";
//		OBAccount2 account = PispConsentApplicationMockdata.getAccountData();
//		when(paymentSetupAdapterHelper.retrieveStagedPaymentSetup(anyString()))
//				.thenReturn(PispConsentApplicationMockdata.getPaymentSetupPOSTResponseInvalidStatus());
//		ReflectionTestUtils.setField(consentCreationDataHelperImpl, "consentExpiryTime", 24);
//		doNothing().when(pispConsentAdapter).createConsent(any(PispConsent.class));
//		doNothing().when(paymentSetupAdapterHelper).updatePaymentSetupDebtorDetails(any(OBAccount2.class),anyString());
//		consentCreationDataHelperImpl.createConsent(account,userId,clientId,intentId,channelId,headers,tppApplicationName,anyObject());
//	}
//	
//	@SuppressWarnings("unchecked")
//	@Test
//	public void testRetrieveCustomerAccountListInfoSuccessFlow() {
//		String userId="12345"; 
//		String clientId="1234";
//		String flowType="12345";
//		String correlationId="12345";
//		String channelId="12345";
//		when(reqAttributes.getCorrelationId()).thenReturn("123445");
//		when(customerAccountListAdapter.retrieveCustomerAccountList(anyString(), anyMap()))
//				.thenReturn(PispConsentApplicationMockdata.getCustomerAccountInfo());
//		assertEquals(PispConsentApplicationMockdata.getCustomerAccountInfo().getData().getAccount().get(1).getAccount().get(0).getIdentification(),
//				consentCreationDataHelperImpl.retrieveCustomerAccountListInfo(userId,clientId,flowType,correlationId,channelId,null).getData().getAccount().get(0).getAccount().get(0).getIdentification());
//	}
//	
//	@SuppressWarnings("unchecked")
//	@Test
//	public void testCancelAccountRequest() {
//		String intentId = "12345";
//		when(customerAccountListAdapter.retrieveCustomerAccountList(anyString(), anyMap()))
//		.thenReturn(PispConsentApplicationMockdata.getCustomerAccountInfo());
//		Map<String, String> paramsMap = new HashMap<>();
//		consentCreationDataHelperImpl.cancelPaymentSetup(intentId, paramsMap);
//	}
//	
//	@SuppressWarnings("unchecked")
//	@Test
//	public void testCancelAccountRequestConsentFound() {
//		String intentId = "12345";
//		PispConsent consent = new PispConsent();
//		consent.setConsentId("35476568");
//		consent.setPsuId("3345");
//		when(pispConsentAdapter.retrieveConsentByPaymentId(anyString(), any(ConsentStatusEnum.class))).thenReturn(consent );
//		doNothing().when(pispConsentAdapter).updateConsentStatus(anyString(), any(ConsentStatusEnum.class));
//		when(customerAccountListAdapter.retrieveCustomerAccountList(anyString(), anyMap()))
//		.thenReturn(PispConsentApplicationMockdata.getCustomerAccountInfo());
//		Map<String, String> paramsMap = new HashMap<>();
//		consentCreationDataHelperImpl.cancelPaymentSetup(intentId, paramsMap);
//	}
//	
//	@SuppressWarnings("unchecked")
//	@Test
//	public void testValidatePreAuthorisation(){
//		String paymentId = "56323413c061485aaa622c195aa15e7c";
//		Map<String, String> paramsMap = new HashMap<>();
//		paramsMap.put("channelId", "BOL");
//		paramsMap.put("x-user-id", "RMCUT001");
//		PaymentSetupValidationResponse paymentSetupValidationResponse = new PaymentSetupValidationResponse();
//		paymentSetupValidationResponse.setPaymentSetupValidationStatus("PASS");
//		when(paymentSetupAdapterHelper.preAuthorizationValidation(any(OBAccount2.class), anyString(), anyMap())).thenReturn(paymentSetupValidationResponse);
//		assertEquals("PASS", consentCreationDataHelperImpl.validatePreAuthorisation(PispConsentApplicationMockdata.getAccountData(),paymentId, paramsMap).getPaymentSetupValidationStatus());
//	}
//
//
//}
