"use strict";
describe("ConsentApp.PispReviewCtrl_test", function() {

    beforeEach(module("consentApp"));

    var $controller,
        $scope,
        state,
        mockConsentService,
        mockBlockUI,
        q,
        deferred,
        config,
        paymentDetails,
        tppInfo,
        $translate,
        translateDeferred,
        $timeout,
        windowObj = { location: { href: "" } },
        errorData = { errorCode: 999, correlationId: null },
        defaultErrorData = { errorCode: "800", correlationId: null };


    beforeEach(function() {
        mockConsentService = {
            cancelRequest: function() {
                deferred = q.defer();
                return deferred.promise;
            },
            accountRequest: function() {
                deferred = q.defer();
                return deferred.promise;
            },
            sessionLogout: function() { /**/ }
        };
        module(function($provide) {
            $provide.value("$window", windowObj);
        });
        paymentDetails = { "payInstBy": "Moneywise", "payeeName": "Test user", "payeeRef": "FRESCO-101", "amount": "777777777.0", "currency": "EUR", "accountDetails": { "AccountId": "e51f7007-23ff-4de4-b3b1-ef6c310e1e9e", "Currency": "GBP", "Nickname": "Bills", "Account": { "SchemeName": "BBAN", "Identification": "76528775", "Name": "Mr Kevin", "SecondaryIdentification": "0011" }, "Servicer": { "SchemeName": "UKSortCode", "Identification": "SC802001" }, "$$hashKey": "object:12" } };
        state = { "params": { "paymentDetails": paymentDetails } };
        mockBlockUI = { start: function() { /**/ }, stop: function() { /**/ } };
    });

    beforeEach(inject(function(_$controller_, $rootScope, ConsentService, $state, $q, _$window_, _$timeout_) {
        jasmine.getFixtures().fixturesPath = "base/tests/fixtures/";
        $scope = $rootScope.$new();
        q = $q;
        windowObj = _$window_;
        $timeout = _$timeout_;
        translateDeferred = q.defer();
        $translate = jasmine.createSpy("$translate").and.returnValue(translateDeferred.promise);
        var controller = _$controller_("PispReviewCtrl", { $scope: $scope, ConsentService: mockConsentService, $state: state, $window: windowObj, blockUI: mockBlockUI, $translate: $translate, $timeout: $timeout });
        config = {
            maskAccountNumberLength: 4,
            modelpopupConfig: {
                "open": function() { /**/ },
                "modelpopupTitle": "Title",
                "modelpopupBodyContent": "Content",
                "btn": {
                    "okbtn": { "visible": true, "label": "", "action": null },
                    "cancelbtn": { "visible": true, "label": "", "action": null }
                }
            }
        };
        tppInfo = "Moneywise";
        loadFixtures("index-pisp.html");
    }));

    afterEach(function() {
        $("body").empty();
    });

    describe("test init method", function() {
        it("initiate happy flow", function() {
            inject(function(config, $state) {
                $state.params.paymentDetails = paymentDetails;
                $scope.init();

                expect($scope.stopCnfirm).toBe(true);
                expect($scope.termsConditn).toBe(false);
                expect($scope.paymentDetails).toBe($state.params.paymentDetails);
                expect($scope.payInstBy).toBe(paymentDetails.payInstBy);
                expect($scope.payeeName).toBe(paymentDetails.payeeName);
                expect($scope.payeeRef).toBe(paymentDetails.payeeRef);
                expect($scope.amount).toBe(paymentDetails.amount);
                expect($scope.maskAccountNumberLength).toBe(config.maskAccountNumberLength);
                expect($scope.acountNumber).toBe(paymentDetails.accountDetails.Account.Identification);
                expect($scope.nickName).toBe(paymentDetails.accountDetails.Nickname);
                expect($scope.modelPopUpConf).toBe(config.modelpopupConfig);
            });
        });

        it("initiate session error flow", function() {
            appendSetFixtures("<input type='hidden' id='error' name='error' value='{&quot;errorCode&quot;:&quot;731&quot;}' />");
            appendSetFixtures("<input type='hidden' id='redirectUri' name='redirectUri' value='google.com' />");
            spyOn($scope, "openSessionOutModal");
            $scope.init();
            expect($scope.pageError.errorCode).toEqual("731");
            expect($scope.redirectUri).toEqual("google.com");
        });
    });

    describe("test createPayment happy flow", function() {
        it("createPayment", function() {
            inject(function(ConsentService, blockUI) {
                var authUrl = $("#oAuthUrl").val();
                var payload = paymentDetails.accountDetails;
                spyOn(blockUI, "start").and.returnValue({});
                spyOn(ConsentService, "accountRequest").and.returnValue({});
                spyOn($.fn, "submit").and.returnValue({});
                $scope.createPayment();
                //expect(ConsentService.cancelRequest).toHaveBeenCalledWith(null, authUrl);
                deferred.resolve({ status: 200, data: { redirectUri: "redirect-to" } });
                $scope.$digest();

                expect($("form").length).toEqual(1);
                expect($("form").attr("action")).toEqual("redirect-to");
                expect($("#user_oauth_approval", $("form")[0]).length).toEqual(1);
                expect($("#csrf", $("form")[0]).length).toEqual(1);
                expect($("form").submit).toHaveBeenCalledTimes(1);
            });
        });
    });

    describe("test createPayment error flow", function() {
        it("createPayment if error comes from server", function() {
            inject(function(ConsentService) {
                //expect($scope.errorData).toBe(null);
                spyOn(ConsentService, "accountRequest").and.returnValue({});
                var authUrl = $("#oAuthUrl").val();
                $scope.createPayment();
                //expect(ConsentService.cancelRequest).toHaveBeenCalledWith(null, authUrl);

                deferred.reject({ status: 200, data: { exception: { errorCode: 999 } }, headers: function() { /**/ } });
                $scope.$digest();
                expect($scope.errorData).toEqual(errorData);
            });
        });

        it("createPayment if no error comes", function() {
            inject(function(ConsentService) {
                spyOn($scope, "openSessionOutModal");
                spyOn(ConsentService, "accountRequest").and.returnValue({});
                var authUrl = $("#oAuthUrl").val();
                var error = { errorCode: "731", correlationId: null, redirectUri: "redirect-to" };
                $scope.createPayment();
                deferred.reject({ status: 200, data: { exception: { errorCode: "731" }, redirectUri: "redirect-to" }, headers: function() { /**/ } });
                $scope.$digest();

                // expect($scope.redirectUri).toEqual(error.redirectUri);
                expect($scope.openSessionOutModal).toHaveBeenCalled();
            });
        });
    });

    describe("test cancelSubmission happy flow", function() {
        it("cancelSubmission", function() {
            inject(function(ConsentService) {
                spyOn(ConsentService, "cancelRequest").and.returnValue({});
                var authUrl = $("#oAuthUrl").val();
                $scope.cancelSubmission();
                //expect(ConsentService.cancelRequest).toHaveBeenCalledWith(null, authUrl);

                deferred.resolve({ status: 200, data: { redirectUri: "redirect-to" } });
                $scope.$digest();
                expect(windowObj.location.href).toEqual("redirect-to");
            });
        });
    });

    describe("test cancelSubmission error flow", function() {
        it("cancelSubmission else flow", function() {
            inject(function(ConsentService) {
                spyOn(ConsentService, "cancelRequest").and.returnValue({});
                var authUrl = $("#oAuthUrl").val();
                $scope.cancelSubmission();

                deferred.reject({ status: 200, data: { exception: { errorCode: 999 } }, headers: function() { /**/ } });
                $scope.$digest();
                expect($scope.errorData).toEqual(errorData);
            });
        });
        it("cancelSubmission branch 800", function() {
            inject(function(ConsentService) {
                spyOn(ConsentService, "cancelRequest").and.returnValue({});
                var authUrl = $("#oAuthUrl").val();
                $scope.cancelSubmission();

                deferred.reject({ status: 200, data: { exception: { errorCode: "800" } }, headers: function() { return null } });
                $scope.$digest();
                expect($scope.errorData).toEqual(defaultErrorData);
            });
        });
        it("cancelSubmission", function() {
            inject(function(ConsentService) {
                spyOn(ConsentService, "cancelRequest").and.returnValue({});
                var authUrl = $("#oAuthUrl").val();
                $scope.cancelSubmission();

                spyOn($scope, "openSessionOutModal");
                deferred.reject({ status: 200, data: { exception: { errorCode: "731" }, redirectUri: "url" }, headers: function() { /**/ } });
                $scope.$digest();
                expect($scope.openSessionOutModal).toHaveBeenCalledTimes(1);
            });
        });
    });

    describe("test sessionSubmission", function() {
        it("sessionSubmission", function() {
            $scope.redirectUri = "redirect-to";
            $scope.sessionSubmission();
            expect(windowObj.location.href).toEqual("redirect-to");
        });
    });

    describe("test openCancelModal", function() {
        it("test openCancelModal happy flow", function() {
            spyOn($scope.modelPopUpConf, "open");
            $scope.cancelSubmission = "cancelSubmissionState";

            $scope.openCancelModal();
            expect($translate).toHaveBeenCalledWith(["CANCEL_POPUP_HEADER", "CANCEL_POPUP_BODY", "NO_BUTTON", "YES_BUTTON"]);
            expect($scope.modelPopUpConf.btn.okbtn.action).toBe("cancelSubmissionState");
            expect($scope.modelPopUpConf.btn.cancelbtn.visible).toBe(true);
            expect($scope.modelPopUpConf.open).toHaveBeenCalled();
            translateDeferred.resolve({
                CANCEL_POPUP_HEADER: "RESOLVED_CANCEL_POPUP_HEADER",
                CANCEL_POPUP_BODY: "RESOLVED_CANCEL_POPUP_BODY",
                NO_BUTTON: "RESOLVED_NO_BUTTON",
                YES_BUTTON: "RESOLVED_YES_BUTTON"
            });
            $scope.$digest();

            expect($scope.modelPopUpConf.modelpopupTitle).toBe("RESOLVED_CANCEL_POPUP_HEADER");
            expect($scope.modelPopUpConf.modelpopupBodyContent).toBe("RESOLVED_CANCEL_POPUP_BODY");
            expect($scope.modelPopUpConf.btn.okbtn.label).toBe("RESOLVED_YES_BUTTON");
            expect($scope.modelPopUpConf.btn.cancelbtn.label).toBe("RESOLVED_NO_BUTTON");
        });

        it("test openCancelModal error flow", function() {
            spyOn($scope.modelPopUpConf, "open");
            $scope.cancelSubmission = "cancelSubmissionState";

            $scope.openCancelModal();

            expect($translate).toHaveBeenCalledWith(["CANCEL_POPUP_HEADER", "CANCEL_POPUP_BODY", "NO_BUTTON", "YES_BUTTON"]);
            expect($scope.modelPopUpConf.btn.okbtn.action).toBe("cancelSubmissionState");
            expect($scope.modelPopUpConf.btn.cancelbtn.visible).toBe(true);
            expect($scope.modelPopUpConf.open).toHaveBeenCalled();

            translateDeferred.reject({
                CANCEL_POPUP_HEADER: "REJECTED_CANCEL_POPUP_HEADER",
                CANCEL_POPUP_BODY: "REJECTED_CANCEL_POPUP_BODY",
                NO_BUTTON: "REJECTED_NO_BUTTON",
                YES_BUTTON: "REJECTED_YES_BUTTON"
            });
            $scope.$digest();

            expect($scope.modelPopUpConf.modelpopupTitle).toBe("REJECTED_CANCEL_POPUP_HEADER");
            expect($scope.modelPopUpConf.modelpopupBodyContent).toBe("REJECTED_CANCEL_POPUP_BODY");
            expect($scope.modelPopUpConf.btn.okbtn.label).toBe("REJECTED_YES_BUTTON");
            expect($scope.modelPopUpConf.btn.cancelbtn.label).toBe("REJECTED_NO_BUTTON");
        });

    });

    describe("test openSessionOutModal", function() {
        it("test openSessionOutModal happy flow", function() {
            $scope.sessionSubmission = "sessionSubmissionState";
            spyOn($scope.modelPopUpConf, "open");
            $scope.openSessionOutModal();
            expect($translate).toHaveBeenCalledWith(["SESSION_TIMEOUT_POPUP_HEADER", "SESSION_TIMEOUT_POPUP_BODY", "OK_BUTTON"]);
            expect($scope.modelPopUpConf.btn.okbtn.visible).toBe(true);
            expect($scope.modelPopUpConf.btn.cancelbtn.visible).toBe(false);
            expect($scope.modelPopUpConf.btn.okbtn.action).toBe("sessionSubmissionState");
            expect($scope.modelPopUpConf.open).toHaveBeenCalled();
            translateDeferred.resolve({
                SESSION_TIMEOUT_POPUP_HEADER: "RESOLVED_SESSION_TIMEOUT_POPUP_HEADER",
                SESSION_TIMEOUT_POPUP_BODY: "RESOLVED_SESSION_TIMEOUT_POPUP_BODY",
                OK_BUTTON: "RESOLVED_OK_BUTTON"
            });
            $scope.$digest();
            expect($scope.modelPopUpConf.modelpopupTitle).toBe("RESOLVED_SESSION_TIMEOUT_POPUP_HEADER");
            expect($scope.modelPopUpConf.modelpopupBodyContent).toBe("RESOLVED_SESSION_TIMEOUT_POPUP_BODY");
            expect($scope.modelPopUpConf.btn.okbtn.label).toBe("RESOLVED_OK_BUTTON");
        });

        it("test openSessionOutModal error flow", function() {
            $scope.sessionSubmission = "sessionSubmissionState";
            spyOn($scope.modelPopUpConf, "open");
            $scope.openSessionOutModal();
            expect($translate).toHaveBeenCalledWith(["SESSION_TIMEOUT_POPUP_HEADER", "SESSION_TIMEOUT_POPUP_BODY", "OK_BUTTON"]);
            expect($scope.modelPopUpConf.btn.okbtn.visible).toBe(true);
            expect($scope.modelPopUpConf.btn.cancelbtn.visible).toBe(false);
            expect($scope.modelPopUpConf.btn.okbtn.action).toBe("sessionSubmissionState");
            expect($scope.modelPopUpConf.open).toHaveBeenCalled();
            translateDeferred.reject({
                SESSION_TIMEOUT_POPUP_HEADER: "REJECTED_SESSION_TIMEOUT_POPUP_HEADER",
                SESSION_TIMEOUT_POPUP_BODY: "REJECTED_SESSION_TIMEOUT_POPUP_BODY",
                OK_BUTTON: "REJECTED_OK_BUTTON"
            });
            $scope.$digest();
            expect($scope.modelPopUpConf.modelpopupTitle).toBe("REJECTED_SESSION_TIMEOUT_POPUP_HEADER");
            expect($scope.modelPopUpConf.modelpopupBodyContent).toBe("REJECTED_SESSION_TIMEOUT_POPUP_BODY");
            expect($scope.modelPopUpConf.btn.okbtn.label).toBe("REJECTED_OK_BUTTON");
        });
    });

});