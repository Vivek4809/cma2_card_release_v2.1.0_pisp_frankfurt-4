/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.consent.domain;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;


/**
 * The Class AccountMappingTest.
 */
public class AccountMappingTest {

	
	/**
	 * Test account mapping.
	 */
	@Test
	public void testAccountMapping() {
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setTppCID("tpp123");
		accountMapping.setPsuId("user123");
		accountMapping.setCorrelationId("123");
		List<AccountDetails> selectedAccounts = new ArrayList<>();
		AccountDetails accountRequest1 = new AccountDetails();
		accountRequest1.setAccountId("22289");
		accountRequest1.setAccountNSC("SC802001");
		accountRequest1.setAccountNumber("1022675");
		selectedAccounts.add(accountRequest1);
		accountMapping.setAccountDetails(selectedAccounts);
		assertEquals("123", accountMapping.getCorrelationId());
		assertEquals("tpp123", accountMapping.getTppCID());
		assertEquals("user123", accountMapping.getPsuId());
		assertEquals(selectedAccounts, accountMapping.getAccountDetails());
	}

}
