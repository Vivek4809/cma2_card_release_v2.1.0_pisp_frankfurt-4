/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.consent.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * The Class AccountDetailsTest.
 */
public class AccountDetailsTest {

	/**
	 * Account details test.
	 */
	@Test
	public void accountDetailsTest() {
		AccountDetails accountDetails = new AccountDetails();
		accountDetails.setAccountId("22289");
		accountDetails.setAccountNSC("SC802001");
		accountDetails.setAccountNumber("1022675");
		assertEquals("22289", accountDetails.getAccountId());
		assertEquals("SC802001", accountDetails.getAccountNSC());
		assertEquals("1022675", accountDetails.getAccountNumber());
	}
}
