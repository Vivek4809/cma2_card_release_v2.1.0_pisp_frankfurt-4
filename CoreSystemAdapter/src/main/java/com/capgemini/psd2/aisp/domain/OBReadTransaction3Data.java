package com.capgemini.psd2.aisp.domain;

import java.util.Objects;
import com.capgemini.psd2.aisp.domain.OBTransaction3;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * OBReadTransaction3Data
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-11-01T11:43:19.895+05:30")

public class OBReadTransaction3Data   {
  @JsonProperty("Transaction")
  @Valid
  private List<OBTransaction3> transaction = null;

  public OBReadTransaction3Data transaction(List<OBTransaction3> transaction) {
    this.transaction = transaction;
    return this;
  }

  public OBReadTransaction3Data addTransactionItem(OBTransaction3 transactionItem) {
    if (this.transaction == null) {
      this.transaction = new ArrayList<OBTransaction3>();
    }
    this.transaction.add(transactionItem);
    return this;
  }

  /**
   * Provides further details on an entry in the report.
   * @return transaction
  **/
  @ApiModelProperty(value = "Provides further details on an entry in the report.")

  @Valid

  public List<OBTransaction3> getTransaction() {
    return transaction;
  }

  public void setTransaction(List<OBTransaction3> transaction) {
    this.transaction = transaction;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OBReadTransaction3Data obReadTransaction3Data = (OBReadTransaction3Data) o;
    return Objects.equals(this.transaction, obReadTransaction3Data.transaction);
  }

  @Override
  public int hashCode() {
    return Objects.hash(transaction);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OBReadTransaction3Data {\n");
    
    sb.append("    transaction: ").append(toIndentedString(transaction)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

