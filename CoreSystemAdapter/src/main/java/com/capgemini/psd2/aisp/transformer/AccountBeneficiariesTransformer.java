package com.capgemini.psd2.aisp.transformer;

import java.util.Map;

import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBeneficiariesResponse;

public interface AccountBeneficiariesTransformer {
	
	public <T> PlatformAccountBeneficiariesResponse transformAccountBeneficiaries(T source, Map<String, String> params);

}
