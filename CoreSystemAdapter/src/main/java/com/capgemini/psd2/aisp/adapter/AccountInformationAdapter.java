/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.aisp.adapter;

import java.util.Map;

import com.capgemini.psd2.aisp.platform.domain.PlatformAccountInformationResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;

/**
 * The Interface AccountInformationAdapter.
 */
public interface AccountInformationAdapter {
	
	/**
	 * Retrieve account information.
	 *
	 * @param accountMapping the account mapping
	 * @param params the params
	 * @return the account GET response
	 */
	public PlatformAccountInformationResponse retrieveAccountInformation(AccountMapping accountMapping, Map<String, String> params);
	
	/**
	 * Retrieve multiple accounts information.
	 *
	 * @param accountMapping the account mapping
	 * @param params the params
	 * @return the account GET response
	 */
	public PlatformAccountInformationResponse retrieveMultipleAccountsInformation(AccountMapping accountMapping, Map<String, String> params);
}
