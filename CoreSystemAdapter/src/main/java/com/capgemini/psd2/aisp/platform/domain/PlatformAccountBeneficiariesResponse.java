package com.capgemini.psd2.aisp.platform.domain;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.OBReadBeneficiary2;

public class PlatformAccountBeneficiariesResponse {

	// CMA2 response
	private OBReadBeneficiary2 oBReadBeneficiary2;

	// Additional Information
	private Map<String, String> additionalInformation;

	public OBReadBeneficiary2 getoBReadBeneficiary2() {
		return oBReadBeneficiary2;
	}

	public void setoBReadBeneficiary2(OBReadBeneficiary2 oBReadBeneficiary2) {
		this.oBReadBeneficiary2 = oBReadBeneficiary2;
	}

	public Map<String, String> getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(Map<String, String> additionalInformation) {
		this.additionalInformation = additionalInformation;
	}
}