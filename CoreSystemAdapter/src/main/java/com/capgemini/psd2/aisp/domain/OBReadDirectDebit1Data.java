package com.capgemini.psd2.aisp.domain;

import java.util.Objects;
import com.capgemini.psd2.aisp.domain.OBDirectDebit1;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * OBReadDirectDebit1Data
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-11-01T11:43:19.895+05:30")

public class OBReadDirectDebit1Data   {
  @JsonProperty("DirectDebit")
  @Valid
  private List<OBDirectDebit1> directDebit = null;

  public OBReadDirectDebit1Data directDebit(List<OBDirectDebit1> directDebit) {
    this.directDebit = directDebit;
    return this;
  }

  public OBReadDirectDebit1Data addDirectDebitItem(OBDirectDebit1 directDebitItem) {
    if (this.directDebit == null) {
      this.directDebit = new ArrayList<OBDirectDebit1>();
    }
    this.directDebit.add(directDebitItem);
    return this;
  }

  /**
   * Account to or from which a cash entry is made.
   * @return directDebit
  **/
  @ApiModelProperty(value = "Account to or from which a cash entry is made.")

  @Valid

  public List<OBDirectDebit1> getDirectDebit() {
    return directDebit;
  }

  public void setDirectDebit(List<OBDirectDebit1> directDebit) {
    this.directDebit = directDebit;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OBReadDirectDebit1Data obReadDirectDebit1Data = (OBReadDirectDebit1Data) o;
    return Objects.equals(this.directDebit, obReadDirectDebit1Data.directDebit);
  }

  @Override
  public int hashCode() {
    return Objects.hash(directDebit);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OBReadDirectDebit1Data {\n");
    
    sb.append("    directDebit: ").append(toIndentedString(directDebit)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

