package com.capgemini.psd2.pisp.domain;

import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;

public class PaymentResponseInfo {

	private OBTransactionIndividualStatus1Code paymentValidationStatus;
	private OBExternalConsentStatus1Code consentValidationStatus;
	private OBExternalConsentStatus2Code payConsentValidationStatus;
	private String paymentConsentId;
	private String idempotencyRequest;
	private String paymentSubmissionId;
	private ProcessConsentStatusEnum consentProcessStatus;
	private String requestType;
	
	/**
	 * @return the PaymentConsentId
	 */
	public String getPaymentConsentId() {
		return paymentConsentId;
	}

	/**
	 * @param PaymentConsentId
	 *            the PaymentConsentId to set
	 */
	public void setPaymentConsentId(String PaymentConsentId) {
		this.paymentConsentId = PaymentConsentId;
	}
	

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	/**
	 * @return the paymentValidationStatus
	 */
	public OBTransactionIndividualStatus1Code getPaymentValidationStatus() {
		return paymentValidationStatus;
	}

	/**
	 * @param paymentValidationStatus
	 *            the paymentValidationStatus to set
	 */
	public void setPaymentValidationStatus(OBTransactionIndividualStatus1Code paymentValidationStatus) {
		this.paymentValidationStatus = paymentValidationStatus;
	}

	/**
	 * @return the idempotencyRequest
	 */
	public String getIdempotencyRequest() {
		return idempotencyRequest;
	}

	/**
	 * @param idempotencyRequest
	 *            the idempotencyRequest to set
	 */
	public void setIdempotencyRequest(String idempotencyRequest) {
		this.idempotencyRequest = idempotencyRequest;
	}

	/**
	 * @return the paymentSubmissionId
	 */
	public String getPaymentSubmissionId() {
		return paymentSubmissionId;
	}

	/**
	 * @param paymentSubmissionId
	 *            the paymentSubmissionId to set
	 */
	public void setPaymentSubmissionId(String paymentSubmissionId) {
		this.paymentSubmissionId = paymentSubmissionId;
	}

	public ProcessConsentStatusEnum getConsentProcessStatus() {
		return consentProcessStatus;
	}

	public void setConsentProcessStatus(ProcessConsentStatusEnum consentProcessStatus) {
		this.consentProcessStatus = consentProcessStatus;
	}

	public OBExternalConsentStatus1Code getConsentValidationStatus() {
		return consentValidationStatus;
	}

	public void setConsentValidationStatus(OBExternalConsentStatus1Code consentValidationStatus) {
		this.consentValidationStatus = consentValidationStatus;
	}

	public OBExternalConsentStatus2Code getPayConsentValidationStatus() {
		return payConsentValidationStatus;
	}

	public void setPayConsentValidationStatus(OBExternalConsentStatus2Code payConsentValidationStatus) {
		this.payConsentValidationStatus = payConsentValidationStatus;
	}

	@Override
	public String toString() {
		return "PaymentResponseInfo [paymentValidationStatus=" + paymentValidationStatus + ", consentValidationStatus="
				+ consentValidationStatus + ", payConsentValidationStatus=" + payConsentValidationStatus
				+ ", paymentConsentId=" + paymentConsentId + ", idempotencyRequest=" + idempotencyRequest
				+ ", paymentSubmissionId=" + paymentSubmissionId + ", consentProcessStatus=" + consentProcessStatus
				+ ", requestType=" + requestType + "]";
	}

}
