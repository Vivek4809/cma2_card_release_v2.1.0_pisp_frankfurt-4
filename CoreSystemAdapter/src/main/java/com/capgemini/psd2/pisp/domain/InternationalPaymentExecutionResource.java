package com.capgemini.psd2.pisp.domain;

import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

public class InternationalPaymentExecutionResource {
	private String paymentSubmissionId;
	private String submissionPlatformStatus;
	private CustomPaymentStageIdentifiers stageIdentifiers;
	
	public String getPaymentSubmissionId() {
		return paymentSubmissionId;
	}
	public void setPaymentSubmissionId(String paymentSubmissionId) {
		this.paymentSubmissionId = paymentSubmissionId;
	}
	public String getSubmissionPlatformStatus() {
		return submissionPlatformStatus;
	}
	public void setSubmissionPlatformStatus(String submissionPlatformStatus) {
		this.submissionPlatformStatus = submissionPlatformStatus;
	}
	public CustomPaymentStageIdentifiers getStageIdentifiers() {
		return stageIdentifiers;
	}
	public void setStageIdentifiers(CustomPaymentStageIdentifiers stageIdentifiers) {
		this.stageIdentifiers = stageIdentifiers;
	} 
}
