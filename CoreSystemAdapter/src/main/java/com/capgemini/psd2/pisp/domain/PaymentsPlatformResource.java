package com.capgemini.psd2.pisp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

//@Document(collection = "paymentSubmissionPlatformResources")
@Document(collection = "#{@CollectionNameLocator.getPaymentSubmissionCollectionName()}")
public class PaymentsPlatformResource {
	@Id
	private String id;	
	private String paymentId;
	private String paymentSubmissionId;	
	private String idempotencyKey;
	private String tppCID;
	private String status;
	private String createdAt;
	private String updatedAt;	
	private String idempotencyRequest;

	//new
	private String submissionId;
	private String tenantId;
	private String submissionCmaVersion;
	private String paymentType;
	//@Indexed(unique=true, sparse=true)
	private String paymentConsentId;
	private String statusUpdateDateTime;
	
	// new design fields ie completed/incomplete/abort
	private String proccessState;
	
	public String getIdempotencyRequest() {
		return idempotencyRequest;
	}
	public void setIdempotencyRequest(String idempotencyRequest) {
		this.idempotencyRequest = idempotencyRequest;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	public String getPaymentSubmissionId() {
		return paymentSubmissionId;
	}
	public void setPaymentSubmissionId(String paymentSubmissionId) {
		this.paymentSubmissionId = paymentSubmissionId;
	}
	public String getIdempotencyKey() {
		return idempotencyKey;
	}
	public void setIdempotencyKey(String idempotencyKey) {
		this.idempotencyKey = idempotencyKey;
	}
	public String getTppCID() {
		return tppCID;
	}
	public void setTppCID(String tppCID) {
		this.tppCID = tppCID;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}	
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	public String getSubmissionId() {
		return submissionId;
	}
	public void setSubmissionId(String submissionId) {
		this.submissionId = submissionId;
	}
	public String getSubmissionCmaVersion() {
		return submissionCmaVersion;
	}
	public void setSubmissionCmaVersion(String submissionCmaVersion) {
		this.submissionCmaVersion = submissionCmaVersion;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getPaymentConsentId() {
		return paymentConsentId;
	}
	public void setPaymentConsentId(String paymentConsentId) {
		this.paymentConsentId = paymentConsentId;
	}
	public String getStatusUpdateDateTime() {
		return statusUpdateDateTime;
	}
	public void setStatusUpdateDateTime(String statusUpdateDateTime) {
		this.statusUpdateDateTime = statusUpdateDateTime;
	}
	public String getProccessState() {
		return proccessState;
	}
	public void setProccessState(String proccessState) {
		this.proccessState = proccessState;
	}
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	@Override
	public String toString() {
		return "PaymentsPlatformResource [id=" + id + ", paymentId=" + paymentId + ", paymentSubmissionId="
				+ paymentSubmissionId + ", idempotencyKey=" + idempotencyKey + ", tppCID=" + tppCID + ", status="
				+ status + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + ", idempotencyRequest="
				+ idempotencyRequest + ", submissionId=" + submissionId + ", submissionCmaVersion="
				+ submissionCmaVersion + ", paymentType=" + paymentType + ", paymentConsentId=" + paymentConsentId
				+ ", statusUpdateDateTime=" + statusUpdateDateTime + ", proccessState=" + proccessState + "]";
	}
}
