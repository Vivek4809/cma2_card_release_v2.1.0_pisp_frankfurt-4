package com.capgemini.psd2.pisp.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Bad request
 */
@ApiModel(description = "Bad request")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-10T11:44:00.143+05:30")

public class OBErrorResponse   {
  @JsonProperty("Code")
  private BigDecimal code = null;

  @JsonProperty("Id")
  private String id = null;

  @JsonProperty("Message")
  private String message = null;

  @JsonProperty("Errors")
  @Valid
  private List<Error> errors = new ArrayList<>();

  public OBErrorResponse code(BigDecimal code) {
    this.code = code;
    return this;
  }

  /**
   * Mandatory, high level Textual error code, e.g., BAD_REQUEST
   * @return code
  **/
  @ApiModelProperty(required = true, value = "Mandatory, high level Textual error code, e.g., BAD_REQUEST")
  @NotNull

  @Valid

  public BigDecimal getCode() {
    return code;
  }

  public void setCode(BigDecimal code) {
    this.code = code;
  }

  public OBErrorResponse id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Optionally, a Unique reference for the error instance, for audit purposes, in case of unkown/unclassified errors
   * @return id
  **/
  @ApiModelProperty(value = "Optionally, a Unique reference for the error instance, for audit purposes, in case of unkown/unclassified errors")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public OBErrorResponse message(String message) {
    this.message = message;
    return this;
  }

  /**
   * Brief non contextual Error message, e.g., 'There is something wrong with the request parameters provided'
   * @return message
  **/
  @ApiModelProperty(required = true, value = "Brief non contextual Error message, e.g., 'There is something wrong with the request parameters provided'")
  @NotNull

@Size(min=1,max=40) 
  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public OBErrorResponse errors(List<Error> errors) {
    this.errors = errors;
    return this;
  }

  public OBErrorResponse addErrorsItem(Error errorsItem) {
    this.errors.add(errorsItem);
    return this;
  }

  /**
   * An array of detail error codes, and messages, if there are more than one error
   * @return errors
  **/
  @ApiModelProperty(required = true, value = "An array of detail error codes, and messages, if there are more than one error")
  @NotNull

  @Valid

  public List<Error> getErrors() {
    return errors;
  }

  public void setErrors(List<Error> errors) {
    this.errors = errors;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OBErrorResponse obErrorResponse = (OBErrorResponse) o;
    return Objects.equals(this.code, obErrorResponse.code) &&
        Objects.equals(this.id, obErrorResponse.id) &&
        Objects.equals(this.message, obErrorResponse.message) &&
        Objects.equals(this.errors, obErrorResponse.errors);
  }

  @Override
  public int hashCode() {
    return Objects.hash(code, id, message, errors);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OBErrorResponse {\n");
    
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("    errors: ").append(toIndentedString(errors)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

