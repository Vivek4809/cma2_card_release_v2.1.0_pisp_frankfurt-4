package com.capgemini.psd2.consent.domain;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "#{@CollectionNameLocator.getPispConsentCollectionName()}")
public class PispConsent extends Consent{

	/** The account details. */
	private AccountDetails accountDetails;
	
	private String paymentId;
    	
	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public AccountDetails getAccountDetails() {
		return accountDetails;
	}

	public void setAccountDetails(AccountDetails accountDetails) {
		this.accountDetails = accountDetails;
	}
	
}